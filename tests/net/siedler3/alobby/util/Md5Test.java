package net.siedler3.alobby.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Test;


public class Md5Test
{
    @Test
    public void testMd5Sum()
    {
        File md5File = new File("unrar/unrar.md5");
        File unrarExe = new File("unrar/UnRAR.exe");
        
        assertTrue(CryptoSupport.Md5Sum(unrarExe, md5File));
        
        File licenseFile = new File("unrar/license.txt");
        
        assertTrue(CryptoSupport.Md5Sum(licenseFile, md5File));
        
        assertTrue(CryptoSupport.md5Sum(md5File));

    }
    
    @Test
    public void testMd5()
    {
        String result;
        result = CryptoSupport.md5("");
        assertEquals("d41d8cd98f00b204e9800998ecf8427e".toUpperCase(), result);
        
        result = CryptoSupport.md5("a");
        assertEquals("0cc175b9c0f1b6a831c399e269772661".toUpperCase(), result);

        result = CryptoSupport.md5("abc");
        assertEquals("900150983cd24fb0d6963f7d28e17f72".toUpperCase(), result);

        result = CryptoSupport.md5("message digest");
        assertEquals("f96b697d7cb7938d525a2f31aaf161d0".toUpperCase(), result);

        result = CryptoSupport.md5("abcdefghijklmnopqrstuvwxyz");
        assertEquals("c3fcd3d76192e4007dfb496cca67e13b".toUpperCase(), result);

        result = CryptoSupport.md5(null);
        assertNull(result);
    }
}
