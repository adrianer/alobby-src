package net.siedler3.alobby.util;

import static org.junit.Assert.assertEquals;
import net.siedler3.alobby.controlcenter.ALobbyTheme.ALobbyThemes;

import org.junit.Test;

public class AlobbyThemeTest
{

    @Test
    public void testGetEnum()
    {
        assertEquals(ALobbyThemes.classicV1, ALobbyThemes.getEnum("classicV1"));
        assertEquals(ALobbyThemes.classicV1, ALobbyThemes.getEnum("classic V1"));
        assertEquals(ALobbyThemes.classicV2, ALobbyThemes.getEnum("classicV2"));
        assertEquals(ALobbyThemes.classicV2, ALobbyThemes.getEnum("classic V2"));
        assertEquals(ALobbyThemes.BB, ALobbyThemes.getEnum("BB"));
        assertEquals(ALobbyThemes.classicV2, ALobbyThemes.getEnum("ABC"));
    }

}
