package net.siedler3.alobby.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;


public class CryptoSupportTest
{
    private static final String[][] ircPwResults = {
        {"a",            "muyX1Qm7A60yGGw_Vda0"},
        {"BGVFAWER",     "HjiQkeTMrFnTF4PTkvtT"},
        {"abcde",        "2-NygIiYw2JNvdck1GQW"},
        {"aäöüß",        "PAvYgB6t1SXwCx1vIMcF"},
        {"!\"§$%&/()=?", "lZAf61UC4hI74QUH2Voi"},
        {"a,._#*+[]{}z", "-VVXawfrGoLxRwSBSTAO"},
        {"aA023987ysd",  "kZBfha6--yUa4WmZeM9u"},
    };

    @Test
    public void testEncodePasswordForIrcTransmission()
    {
        //this test must never fail, otherwise authentication
        //is no longer possible
        String password;
        String expectedResult;
        String result;
        try
        {
            for (String[] entry : ircPwResults)
            {
                password = entry[0];
                expectedResult = entry[1];
                result = CryptoSupport.encodePasswordForIrcTransmission(password);
                assertEquals(expectedResult, result);
            }
        }
        catch (Exception e) {
            fail(e.getMessage());
        }
    }

}
