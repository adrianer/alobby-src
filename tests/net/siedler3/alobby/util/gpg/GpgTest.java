package net.siedler3.alobby.util.gpg;

import java.io.File;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GpgTest
{

    @Before
    public void setUp() throws Exception
    {
    }

    @Test
    public void test() throws IOException
    {
        Gpg gpg = new Gpg();
        Assert.assertTrue(gpg.extractGpg());
        Assert.assertTrue(gpg.verify(new File("tests/net/siedler3/alobby/util/gpg/test.txt"), new File("tests/net/siedler3/alobby/util/gpg/test.asc")));
        Assert.assertFalse(gpg.verify(new File("tests/net/siedler3/alobby/util/gpg/test2.txt"), new File("tests/net/siedler3/alobby/util/gpg/test.asc")));
    }

}
