package net.siedler3.alobby.controlcenter;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;


public class ConfigurationTest
{
    
    /**
     * nur für Debugzwecke, daher mit Ignore flag
     */
    @Ignore @Test public void testConfiguration(SettlersLobby settlersLobby)
    {
        Configuration config = Configuration.getInstance();
        if (!config.loadDefaultConfig())
        {
            fail();
            return;
        }
        if (!config.tryToLoadConfigOrDie(settlersLobby))
        {
            fail();
            return;
        }
        config.getBuddys().add("user1");
        config.getBuddys().add("user2");
        config.getIgnoredUsers().add("bad1");
        config.getIgnoredUsers().add("bad2");
        
        assertTrue(config.saveCurrentConfiguration());
        
        config.getBuddys().remove("user1");
        config.getIgnoredUsers().remove("bad2");
        
        assertTrue(config.saveCurrentConfiguration());
        
        config.getBuddys().remove("user2");
        config.getIgnoredUsers().remove("bad1");
        
        assertTrue(config.saveCurrentConfiguration());

    }
    
    @Test
    public void testBuddy( SettlersLobby settlersLobby)
    {
        Configuration config = Configuration.getInstance();
        if (!config.loadDefaultConfig())
        {
            fail();
            return;
        }
        if (!config.tryToLoadConfigOrDie(settlersLobby))
        {
            fail();
            return;
        }
        
        String nick = "#asdf123";
        String nick2 = "#12345abc";
        config.addBuddy(nick);
        config.addBuddy(nick2);
        config.addIgnoredUser(nick);
        config.addIgnoredUser(nick2);
        
        //check buddy
        assertTrue(config.isBuddy(nick));
        config.removeBuddy(nick);
        assertFalse(config.isBuddy(nick));
        
        assertTrue(config.isBuddy(nick2));
        config.removeBuddy(nick2);
        assertFalse(config.isBuddy(nick2));
        
        config.addBuddy(nick);
        config.addBuddy(nick);
        assertTrue(config.isBuddy(nick));
        config.removeBuddy(nick);
        assertFalse(config.isBuddy(nick));
        config.removeBuddy(nick);
        assertFalse(config.isBuddy(nick));
        
        //check ignoredUser
        assertTrue(config.isIgnoredUser(nick));
        config.removeIgnoredUser(nick);
        assertFalse(config.isIgnoredUser(nick));
        
        assertTrue(config.isIgnoredUser(nick2));
        config.removeIgnoredUser(nick2);
        assertFalse(config.isIgnoredUser(nick2));
        
        config.addIgnoredUser(nick);
        config.addIgnoredUser(nick);
        assertTrue(config.isIgnoredUser(nick));
        config.removeIgnoredUser(nick);
        assertFalse(config.isIgnoredUser(nick));
        config.removeIgnoredUser(nick);
        assertFalse(config.isIgnoredUser(nick));
        

    }
}
