package net.siedler3.alobby.controlcenter;

import org.junit.Ignore;
import org.junit.Test;

public class IpViaUrlRetrieverTest
{

    /**
     * Hiermit kann man den IpViaUrlRetriever debuggen.
     * Dazu einfach das @Ignore entfernen und als Junit Test laufen lassen.
     */
    @Ignore @Test public void debugMapDownload()
    {
        SettlersLobby settlersLobby = new SettlersLobby();
        IpViaUrlRetriever ipViaUrl = new IpViaUrlRetriever(settlersLobby);
        ipViaUrl.start();
        try
        {
            ipViaUrl.join();
        }
        catch (InterruptedException e) {
        }
        System.exit(0); 
    }

}
