package net.siedler3.alobby.modules.login;

import net.siedler3.alobby.communication.IrcEventListener;
import net.siedler3.alobby.communication.NickServ;
import net.siedler3.alobby.communication.NickServ.IrcServer;
import net.siedler3.alobby.communication.ProprietaryReplyConstants;
import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.gui.GUI;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.modules.base.IrcTestHelper;
import net.siedler3.alobby.modules.base.ResultEventProvider;

import org.jibble.pircbot.ReplyConstants;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class IrcUserAuthenticatorTest extends IrcTestHelper
{
    private static final String ERROR_MESSAGE_NICK_NOT_REGISTERED = "IRCCommunicator.ERROR_NICK_NOT_REGISTERED"; //$NON-NLS-1$
    private static final String ERROR_MESSAGE_NICK_ALREADY_IN_USE = "IRCCommunicator.ERROR_NICK_ALREADY_IN_USE"; //$NON-NLS-1$
    private static final String ERROR_MESSAGE_WRONG_PASSWORD = "IRCCommunicator.ERROR_WRONG_PASSWORD"; //$NON-NLS-1$


    private IrcUserAuthenticator classUnderTest;

    @Override
    @Before
    public void setUp() throws Exception
    {
        super.setUp();
        isSendInfoCommandDuringQueryNickStatus = true;

        SettlersLobby settlersLobby = null;
        GUI gui = null;
        classUnderTest = new IrcUserAuthenticator(mockIrcInterface, settlersLobby, gui);
        classUnderTest.setListener(mockResultEventListener);
    }

    @After
    public void clearTimer()
    {
        //always clear the timeout timer at the end of the test, as not for all tests
        //a resultEvent is returned
        classUnderTest.cleanupBeforeNotifyResult();
    }

    @Test
    public void testLogin2()
    {
        String notice;
        NickServ.server = IrcServer.siedler3_net;

        //check doLogin()
        expectDoLogin(nick);
        ctrl.replay();
        classUnderTest.doLogin(nick, password);
        ctrl.verify();

        //check NickNotRegistered
        ctrl.reset();
        expectDoLogin(nick);
        expectOnNotice();
        expectHandleNickServeStatusReply(nick);
        expectOnResultFailed(I18n.getString(ERROR_MESSAGE_NICK_NOT_REGISTERED));
        ctrl.replay();
        classUnderTest.doLogin(nick, password);
        notice = NickServ.createStatus(nick, 0);
        onNickServeNotice(notice, nick);
        ctrl.verify();

        //check Login ok
        ctrl.reset();
        expectDoLogin(nick);
        expectTriggerTransmitCommand(nick, password);
        expectOnNotice();
        expectHandleNickServeStatusReply(nick);
        expectOnResultOk();
        ctrl.replay();
        classUnderTest.doLogin(nick, password);
        notice = NickServ.createStatus(nick, 1);
        onNickServeNotice(notice, nick);
        notice = NickServ.createStatus(nick, 3);
        onNickServeNotice(notice, nick);
        ctrl.verify();

        //check wrong password
        ctrl.reset();
        expectDoLogin(nick);
        expectTriggerTransmitCommand(nick, password);
        expectOnNotice();
        expectHandleWrongPasswordMessage();
        expectOnNotice();
        expectHandleNickServeStatusReply(nick);
        expectOnResultFailed(I18n.getString(ERROR_MESSAGE_WRONG_PASSWORD));
        ctrl.replay();
        classUnderTest.doLogin(nick, password);
        notice = NickServ.createStatus(nick, 1);
        onNickServeNotice(notice, nick);
        notice = NickServ.createWrongPassword();
        onNickServeNotice(notice, nick);
        notice = NickServ.createStatus(nick, 1);
        onNickServeNotice(notice, nick);
        ctrl.verify();


        //check login failed for unknown reason
        ctrl.reset();
        expectDoLogin(nick);
        expectTriggerTransmitCommand(nick, password);
        expectOnNotice();
        expectHandleNickServeStatusReply(nick);
        expectOnResultFailed(I18n.getString(ERROR_MESSAGE_WRONG_PASSWORD));
        ctrl.replay();
        classUnderTest.doLogin(nick, password);
        notice = NickServ.createStatus(nick, 1);
        onNickServeNotice(notice, nick);
        notice = NickServ.createStatus(nick, 1);
        onNickServeNotice(notice, nick);
        ctrl.verify();
    }

    @Test
    public void testServiceDown()
    {
        //service down is handled as OK
        int code = ProprietaryReplyConstants.ERR_SERVICESDOWN;
        String message = "empty";
        expectDoLogin(nick);
        expectOnServerResponse(code, message);
        expectOnResultOk();
        ctrl.replay();
        classUnderTest.doLogin(nick, password);
        onServerResponse(code, message);
        ctrl.verify();

        //with email should not have any influence
        ctrl.reset();
        NickServ.server = IrcServer.siedler3_net;
        ircSupportsEmailReg = true;
        isSendInfoCommandDuringQueryNickStatus = true;
        isSendRegisterCommandDuringQueryNickStatus = false;
        expectDoLogin(nick);
        expectOnServerResponse(code, message);
        expectOnResultOk();
        ctrl.replay();
        classUnderTest.doLogin(nick, password);
        onServerResponse(code, message);
        ctrl.verify();
    }


    @Test
    public void testNickInUse()
    {
        String nick2 = USER2;
        int code  = ReplyConstants.ERR_NICKNAMEINUSE;
        String message = "empty";
        expectDoLogin(nick2);
        expectOnServerResponse(code, message);
        expectOnResultFailed(I18n.getString(ERROR_MESSAGE_NICK_ALREADY_IN_USE));
        ctrl.replay();
        classUnderTest.doLogin(nick2, password);
        onServerResponse(code, message);
        ctrl.verify();

        //test erroneous nick, else clause
        ctrl.reset();
        message = nick2 + " :Erroneous nickname";
        code = ReplyConstants.ERR_ERRONEUSNICKNAME;
        expectDoLogin(nick2);
        expectOnServerResponse(code, message);
        expectGetNick();
        expectOnResultFailed(message);
        ctrl.replay();
        classUnderTest.doLogin(nick2, password);
        onServerResponse(code, message);
        ctrl.verify();

        //test erroneous nick, if clause
        ctrl.reset();
        message = nick2 + " :Erroneous nickname";
        String message1 = nick + " " + message; 
        code = ReplyConstants.ERR_ERRONEUSNICKNAME;
        expectDoLogin(nick2);
        expectOnServerResponse(code, message1);
        expectGetNick();
        expectGetNick();
        expectOnResultFailed(message);
        ctrl.replay();
        classUnderTest.doLogin(nick2, password);
        onServerResponse(code, message1);
        ctrl.verify();

    }


    @Test
    public void testNickChangeStartsRegistrationProcess()
    {
        String nick2 = USER2;
        String nick3 = "User3";
        expectDoLogin(nick2);
        expectOnNickChange(nick2);
        ctrl.replay();
        classUnderTest.doLogin(nick2, password);
        onNickChange(nick, nick2);
        ctrl.verify();

        //check that duplicate message does not trigger again
        ctrl.reset();
        expectDoLogin(nick2);
        expectOnNickChange(nick2);
        ctrl.replay();
        classUnderTest.doLogin(nick2, password);
        onNickChange(nick, nick2);
        onNickChange(nick, nick2);
        ctrl.verify();

        //test that wrong nickchange does not trigger anything
        ctrl.reset();
        expectDoLogin(nick2);
        expectOnWrongNickChange(nick3);
        ctrl.replay();
        classUnderTest.doLogin(nick2, password);
        classUnderTest.onNickChange(nick, "user", "abc.hostname.com", nick3);
        ctrl.verify();

        //somebody else takes the desired nick (hypothetical)
        ctrl.reset();
        expectDoLogin(nick2);
        expectOnWrongNickChange(nick2);
        ctrl.replay();
        classUnderTest.doLogin(nick2, password);
        classUnderTest.onNickChange(nick3, "user", "abc.hostname.com", nick2);
        ctrl.verify();
    }



    @Test
    public void testDisconnected()
    {
        isConnected = false;
        expectDoLogin(nick);
        expectOnResultAborted();
        ctrl.replay();
        classUnderTest.doLogin(nick, password);
        ctrl.verify();

        isConnected = true;
        ctrl.reset();
        expectDoLogin(nick);
        expectOnDisconnect();
        ctrl.replay();
        classUnderTest.doLogin(nick, password);
        classUnderTest.onDisconnect();
        ctrl.verify();

    }


    @Test
    public void testManualAbort()
    {
        expectStopListening();
        ctrl.replay();
        classUnderTest.manualAbort();
        ctrl.verify();
    }


    @Test
    public void timeoutTaskExecutedTest()
    {
        System.out.println("Timeout test, please wait...");
        expectDoLogin(nick);
        expectOnResultFailed("unexpected timeout");
        ctrl.replay();
        classUnderTest.doLogin(nick, password);
        sleep(11000);
        ctrl.verify();
        System.out.println("Timeout test finished");
    }



    private void expectTriggerTransmitCommand(String nick, String password)
    {
        expectOnNotice();
        expectHandleNickServeStatusReply(nick);
        expectTransmitLoginCommand(password);
    }

    private void expectTransmitLoginCommand(String password)
    {
        expectIdentify(password);
        expectQueryNickStatus();
    }


    private void expectIdentify(String password)
    {
        mockIrcInterface.setVerbose(false);
        mockIrcInterface.identify(password);
        mockIrcInterface.setVerbose(ALobbyConstants.IRC_DEBUG_ON);
    }

    private void onNickServeNotice(String notice, String target)
    {
        //System.out.println(notice);
        classUnderTest.onNotice("NickServ", "services", "irc.siedler3.net", target, notice);
    }

    private void onServerResponse(int code, String message)
    {
        classUnderTest.onServerResponse(code, message);
    }

    private void onNickChange(String oldNick, String newNick)
    {
        classUnderTest.onNickChange(oldNick, "user", "abc.hostname.com", newNick);
    }

    private void expectDoLogin(String nick)
    {
        expectIsConnected();
        if (this.isConnected)
        {
            expectStartListening();
            expectGetNick();
            if (nick.equals(this.nick))
            {
                expectQueryNickStatus();
            }
            else
            {
                expectChangeNick(nick);
            }
        }
    }


    @Override
    protected IrcEventListener getIrcEventListener()
    {
        return classUnderTest;
    }

    @Override
    protected ResultEventProvider getResultEventProvider()
    {
        return classUnderTest;
    }

    protected void expectHandleWrongPasswordMessage()
    {
    }

}
