package net.siedler3.alobby.modules.connect;

import static org.easymock.EasyMock.expectLastCall;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;

import net.siedler3.alobby.communication.IrcEventListener;
import net.siedler3.alobby.modules.base.IrcTestHelper;
import net.siedler3.alobby.modules.base.ResultCode;
import net.siedler3.alobby.modules.base.ResultEventProvider;

import org.jibble.pircbot.IrcException;
import org.junit.Before;
import org.junit.Test;

public class IrcConnectorTest extends IrcTestHelper
{
    private IrcConnector classUnderTest;
    private int port;
    private String server;

	@Override
    @Before
	public void setUp() throws Exception
	{
	    super.setUp();
        classUnderTest = new IrcConnector(mockIrcInterface);
        classUnderTest.setListener(mockResultEventListener);
        server = "irc.siedler3.net";
        port = 6667; //default irc port
	}

	@Test
	public void doConnectTest()
	{
        expectDoConnect();
        ctrl.replay();
        doConnect();
        ctrl.verify();

        //with answer
        ctrl.reset();
        expectDoConnect();
        expectOnResultOk();
        ctrl.replay();
        doConnect();
        onConnect();
        ctrl.verify();
	}

	@Test
	public void disconnectByServerTest()
	{
        expectDoConnect();
        expectOnResultOk();
        expectOnTerminated();
        ctrl.replay();
        doConnect();
        onConnect();
        onDisconnect();
        ctrl.verify();
	}

	@Test
    public void manualDisconnectByUserTest()
    {
        expectDoConnect();
        expectOnResultOk();
        expectOnManualDisconnect();
        expectOnResultAborted();
        ctrl.replay();
        doConnect();
        onConnect();
        doDisconnect();
        onDisconnect();
        ctrl.verify();

        //test that the killtask is not activated before 5000 ms
        ctrl.reset();
        expectDoConnect();
        expectOnResultOk();
        expectOnManualDisconnect();
        expectOnResultAborted();
        ctrl.replay();
        doConnect();
        onConnect();
        doDisconnect();
        sleep(4800);
        onDisconnect();
        sleep(1000); //wait again in case kill task triggers
        ctrl.verify();

    }

	@Test
	public void killTaskExecutedTest()
	{
	    //this test will log
	    //ERROR, cannot inform about Termination
	    //to System.out due to mocking limitations
        expectDoConnect();
        expectOnResultOk();
        expectOnManualDisconnect();
        expectOnKillTaskExecuted();
        ctrl.replay();
        doConnect();
        onConnect();
        System.out.println("Please ignore the following line: 'ERROR, cannot inform about Termination'");
        doDisconnect();
        sleep(5100);
        ctrl.verify();
	}


    @Test
	public void doConnectFailsTest() throws Exception
	{
	    try
	    {
    	    //do not log the exceptions
    	    net.siedler3.alobby.controlcenter.Test.stopLogging = true;
            expectDoConnectWithException(new java.net.ConnectException());
            ctrl.replay();
            doConnect();
            ctrl.verify();

            ctrl.reset();
            expectDoConnectWithException(new java.net.SocketException());
            ctrl.replay();
            doConnect();
            ctrl.verify();

            ctrl.reset();
            expectDoConnectWithException(new IOException("The PircBot is already connected to an IRC server.  Disconnect first."));
            ctrl.replay();
            doConnect();
            ctrl.verify();

            ctrl.reset();
            expectDoConnectWithException(new IrcException("Could not log into the IRC server: k-lined"));
            ctrl.replay();
            doConnect();
            ctrl.verify();
	    }
	    finally
	    {
	        net.siedler3.alobby.controlcenter.Test.stopLogging = false;
	    }
	}


    private void doDisconnect()
    {
        boolean result = classUnderTest.doDisconnect();
        assertTrue(result);
    }

    private void expectOnManualDisconnect()
    {
        expectIsConnected();
        mockIrcInterface.disconnect();
    }

    private void expectOnKillTaskExecuted()
    {
        mockIrcInterface.dispose();
        mockIrcInterface.isConnected();
        expectLastCall().andReturn(true).times(2).andReturn(false);
        //TODO mockIrcInterface is not an instance of IRcCommunicator, so
        //the onDisconnect call will not be triggered
        //expectOnResultAborted();
    }


    private void expectOnTerminated()
    {
        expectStopListening();
        mockResultEventListener.onResultEvent(createResult(ResultCode.TERMINATED));
    }

    private void onConnect()
    {
        classUnderTest.onConnect();
    }

    private void onDisconnect()
    {
        classUnderTest.onDisconnect();
    }

    private void expectDoConnectWithException(Exception e) throws Exception
    {
        expectSetAutoNickChange(true);
        expectStartIdentServer();
        expectStartListening();
        mockIrcInterface.connect(server, port);
        expectLastCall().andThrow(e);
        if (e instanceof IOException)
        {
            mockIrcInterface.dispose();
        }
        expectOnResultFailed(e.toString());
    }

    private void doConnect()
    {
        classUnderTest.doConnect(server, port);
        //connection is done in a different thread, need to sleep so that
        //the other thread gets active
        sleep();
    }

    private void sleep()
    {
        sleep(50);
    }

    private void expectDoConnect()
    {
        expectSetAutoNickChange(true);
        expectStartIdentServer();
        expectStartListening();
        expectConnect(server, port);
    }

    private void expectConnect(String server, int port)
    {
        try
        {
            mockIrcInterface.connect(server, port);
        }
        catch (Exception e)
        {
            fail(e.getMessage());
        }
    }

    private void expectStartIdentServer()
    {
        mockIrcInterface.startIdentServer();
    }

    private void expectSetAutoNickChange(boolean b)
    {
        mockIrcInterface.setAutoNickChange(b);
    }

    @Override
    protected void expectOnResultOk()
    {
        mockResultEventListener.onResultEvent(createResult(ResultCode.OK));
    }

    @Override
    protected IrcEventListener getIrcEventListener()
    {
        return classUnderTest;
    }

    @Override
    protected ResultEventProvider getResultEventProvider()
    {
        return classUnderTest;
    }

}
