package net.siedler3.alobby.modules.register;

import net.siedler3.alobby.communication.IrcEventListener;
import net.siedler3.alobby.communication.NickServ;
import net.siedler3.alobby.communication.NickServ.IrcServer;
import net.siedler3.alobby.communication.ProprietaryReplyConstants;
import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.modules.base.IrcTestHelper;
import net.siedler3.alobby.modules.base.ResultCode;
import net.siedler3.alobby.modules.base.ResultEventProvider;

import org.jibble.pircbot.ReplyConstants;
import org.junit.Before;
import org.junit.Test;

public class IrcUserRegistratorTest extends IrcTestHelper
{

    private static final String ERROR_MESSAGE_NICK_ALREADY_REGISTERED = "IRCCommunicator.ERROR_NICK_ALREADY_REGISTERED"; //$NON-NLS-1$
    private static final String ERROR_MESSAGE_NICK_NOT_REGISTERED = "IRCCommunicator.ERROR_NICK_NOT_REGISTERED"; //$NON-NLS-1$
    private static final String ERROR_MESSAGE_NICK_ALREADY_IN_USE = "IRCCommunicator.ERROR_NICK_ALREADY_IN_USE"; //$NON-NLS-1$
    private static final String ERROR_MESSAGE_PASSWORD_TOO_SHORT = "IRCCommunicator.ERROR_PASSWORD_TOO_SHORT"; //$NON-NLS-1$
    private static final String ERROR_MESSAGE_INVALID_EMAIL = "IRCCommunicator.ERROR_INVALID_EMAIL"; //$NON-NLS-1$;
    private static final String ERROR_MESSAGE_SERVICE_DOWN = "IRCCommunicator.ERROR_SERVICE_DOWN"; //$NON-NLS-1$;
    private IrcUserRegistrator classUnderTest;
    private String email;

    @Before
    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        classUnderTest = new IrcUserRegistrator(mockIrcInterface);
        classUnderTest.setListener(mockResultEventListener);
        email = "user@domain.com";
    }

    @Test
    public void testNotifyCalls()
    {
        expectOnResultOk();
        ctrl.replay();
        classUnderTest.testOnly_notifyRegistrationSuccessful();
        ctrl.verify();

        ctrl.reset();
        expectStopListening();
        mockResultEventListener.onResultEvent(createResult(ResultCode.SPECIAL_AUTH_CODE_SENT));
        ctrl.replay();
        classUnderTest.testOnly_notifyRegistrationSuccessfulAuthCodeSent();
        ctrl.verify();

        ctrl.reset();
        expectStopListening();
        mockResultEventListener.onResultEvent(createResultFailed("error"));
        expectStopListening();
        mockResultEventListener.onResultEvent(createResultFailed("error2"));
        expectOnResultFailed(I18n.getString(ERROR_MESSAGE_NICK_NOT_REGISTERED));
        ctrl.replay();
        classUnderTest.testOnly_notifyRegistrationFailed("error");
        classUnderTest.testOnly_setErrorMessage("error2");
        classUnderTest.testOnly_notifyRegistrationFailed();
        classUnderTest.testOnly_setErrorMessage(null);
        classUnderTest.testOnly_notifyRegistrationFailed();
        ctrl.verify();
    }

    @Test
    public void testRegister2()
    {
        String notice;
        NickServ.server = IrcServer.siedler3_net;

        //check doRegister()
        expectDoRegister(nick);
        ctrl.replay();
        classUnderTest.doRegister(nick, password, email);
        ctrl.verify();

        //check NickAlreadyRegistered
        expectNickAlreadyRegistered(nick, password, email, 1);
        expectNickAlreadyRegistered(nick, password, email, 3);

        //check Registration ok
        ctrl.reset();
        expectDoRegister(nick);
        expectTriggerTransmitCommand(nick, password, email);
        expectOnNotice();
        expectHandleNickServeStatusReply(nick);
        expectOnResultOk();
        ctrl.replay();
        classUnderTest.doRegister(nick, password, email);
        notice = NickServ.createStatus(nick, 0);
        onNickServeNotice(notice, nick);
        notice = NickServ.createStatus(nick, 3);
        onNickServeNotice(notice, nick);
        ctrl.verify();

        //check password too short
        ctrl.reset();
        expectDoRegister(nick);
        expectTriggerTransmitCommand(nick, password, email);
        expectOnNotice();
        expectHandlePasswordTooShortMessage();
        expectOnNotice();
        expectHandleNickServeStatusReply(nick);
        expectOnResultFailed(I18n.getString(ERROR_MESSAGE_PASSWORD_TOO_SHORT));
        ctrl.replay();
        classUnderTest.doRegister(nick, password, email);
        notice = NickServ.createStatus(nick, 0);
        onNickServeNotice(notice, nick);
        notice = NickServ.createPasswordTooShort();
        onNickServeNotice(notice, nick);
        notice = NickServ.createStatus(nick, 0);
        onNickServeNotice(notice, nick);
        ctrl.verify();

        //check invalid email
        ctrl.reset();
        expectDoRegister(nick);
        expectTriggerTransmitCommand(nick, password, email);
        expectOnNotice();
        expectHandleInvalidEmailMessage();
        expectOnNotice();
        expectHandleNickServeStatusReply(nick);
        expectOnResultFailed(I18n.getString(ERROR_MESSAGE_INVALID_EMAIL));
        ctrl.replay();
        classUnderTest.doRegister(nick, password, email);
        notice = NickServ.createStatus(nick, 0);
        onNickServeNotice(notice, nick);
        notice = NickServ.createInvalidEmail();
        onNickServeNotice(notice, nick);
        notice = NickServ.createStatus(nick, 0);
        onNickServeNotice(notice, nick);
        ctrl.verify();

        //check registration failed for unknown reason
        ctrl.reset();
        expectDoRegister(nick);
        expectTriggerTransmitCommand(nick, password, email);
        expectOnNotice();
        expectHandleNickServeStatusReply(nick);
        expectOnResultFailed(I18n.getString(ERROR_MESSAGE_NICK_NOT_REGISTERED));
        ctrl.replay();
        classUnderTest.doRegister(nick, password, email);
        notice = NickServ.createStatus(nick, 0);
        onNickServeNotice(notice, nick);
        notice = NickServ.createStatus(nick, 0);
        onNickServeNotice(notice, nick);
        ctrl.verify();

    }

    @Test
    public void testRegister1()
    {
        String notice;
        NickServ.server = IrcServer.IRC_Mania;

        //check password too short
        ctrl.reset();
        expectDoRegister(nick);
        expectTriggerTransmitCommand(nick, password, email);
        expectOnNotice();
        expectHandlePasswordTooShortMessage();
        expectOnNotice();
        expectHandleNickServeStatusReply(nick);
        expectOnResultFailed(I18n.getString(ERROR_MESSAGE_PASSWORD_TOO_SHORT));
        ctrl.replay();
        classUnderTest.doRegister(nick, password, email);
        notice = NickServ.createStatus(nick, 0);
        onNickServeNotice(notice, nick);
        notice = NickServ.createPasswordTooShort();
        onNickServeNotice(notice, nick);
        notice = NickServ.createStatus(nick, 0);
        onNickServeNotice(notice, nick);
        ctrl.verify();

        //check invalid email
        ctrl.reset();
        expectDoRegister(nick);
        expectTriggerTransmitCommand(nick, password, email);
        expectOnNotice();
        expectHandleInvalidEmailMessage();
        expectOnNotice();
        expectHandleNickServeStatusReply(nick);
        expectOnResultFailed(I18n.getString(ERROR_MESSAGE_INVALID_EMAIL));
        ctrl.replay();
        classUnderTest.doRegister(nick, password, email);
        notice = NickServ.createStatus(nick, 0);
        onNickServeNotice(notice, nick);
        notice = NickServ.createInvalidEmail();
        onNickServeNotice(notice, nick);
        notice = NickServ.createStatus(nick, 0);
        onNickServeNotice(notice, nick);
        ctrl.verify();
    }

    @Test
    public void testRegisterEmailAuth2()
    {
        String notice;
        NickServ.server = IrcServer.siedler3_net;
        ircSupportsEmailReg = true;

        //check doRegister()
        expectDoRegister(nick);
        ctrl.replay();
        classUnderTest.doRegister(nick, password, email);
        ctrl.verify();

        //check NickAlreadyRegistered
        expectNickAlreadyRegistered(nick, password, email, 1);
        expectNickAlreadyRegistered(nick, password, email, 3);

        //check standard email Registration ok
        ctrl.reset();
        expectDoRegister(nick);
        expectTriggerTransmitCommand(nick, password, email);
        expectOnNotice();
        expectHandleEmailSent();
        expectOnNotice();
        expectRequiresAuthCode();
        expectOnNotice();
        expectHandleNickServeStatusReply(nick);
        expectOnResultOkAuthCodeSent();
        ctrl.replay();
        classUnderTest.doRegister(nick, password, email);
        notice = NickServ.createStatus(nick, 0);
        onNickServeNotice(notice, nick);
        notice = NickServ.createEmailSent(email);
        onNickServeNotice(notice, nick);
        notice = NickServ.createRequiresAuthCode(email);
        onNickServeNotice(notice, nick);
        notice = NickServ.createStatus(nick, 0);
        onNickServeNotice(notice, nick);
        ctrl.verify();

        //check standard email Registration, email sent not recognised, no error recognised
        ctrl.reset();
        expectDoRegister(nick);
        expectTriggerTransmitCommand(nick, password, email);
        expectOnNotice();
        expectRequiresAuthCode();
        expectOnNotice();
        expectHandleNickServeStatusReply(nick);
        expectOnResultOk();
        ctrl.replay();
        classUnderTest.doRegister(nick, password, email);
        notice = NickServ.createStatus(nick, 0);
        onNickServeNotice(notice, nick);
        notice = NickServ.createRequiresAuthCode(email);
        onNickServeNotice(notice, nick);
        notice = NickServ.createStatus(nick, 0);
        onNickServeNotice(notice, nick);
        ctrl.verify();


        //check password too short
        ctrl.reset();
        expectDoRegister(nick);
        expectTriggerTransmitCommand(nick, password, email);
        expectOnNotice();
        expectHandlePasswordTooShortMessage();
        expectOnNotice();
        expectRequiresAuthCode();
        expectOnNotice();
        expectHandleNickServeStatusReply(nick);
        expectOnResultFailed(I18n.getString(ERROR_MESSAGE_PASSWORD_TOO_SHORT));
        ctrl.replay();
        classUnderTest.doRegister(nick, password, email);
        notice = NickServ.createStatus(nick, 0);
        onNickServeNotice(notice, nick);
        notice = NickServ.createPasswordTooShort();
        onNickServeNotice(notice, nick);
        notice = NickServ.createRequiresAuthCode(email);
        onNickServeNotice(notice, nick);
        notice = NickServ.createStatus(nick, 0);
        onNickServeNotice(notice, nick);
        ctrl.verify();

        //check invalid email
        ctrl.reset();
        expectDoRegister(nick);
        expectTriggerTransmitCommand(nick, password, email);
        expectOnNotice();
        expectHandleInvalidEmailMessage();
        expectOnNotice();
        expectRequiresAuthCode();
        expectOnNotice();
        expectHandleNickServeStatusReply(nick);
        expectOnResultFailed(I18n.getString(ERROR_MESSAGE_INVALID_EMAIL));
        ctrl.replay();
        classUnderTest.doRegister(nick, password, email);
        notice = NickServ.createStatus(nick, 0);
        onNickServeNotice(notice, nick);
        notice = NickServ.createInvalidEmail();
        onNickServeNotice(notice, nick);
        notice = NickServ.createRequiresAuthCode(email);
        onNickServeNotice(notice, nick);
        notice = NickServ.createStatus(nick, 0);
        onNickServeNotice(notice, nick);
        ctrl.verify();
    }

    @Test
    public void testServiceDown()
    {
        int code  = ProprietaryReplyConstants.ERR_SERVICESDOWN;
        String message = "empty";
        expectDoRegister(nick);
        expectOnServerResponse(code, message);
        expectOnResultFailed(I18n.getString(ERROR_MESSAGE_SERVICE_DOWN));
        ctrl.replay();
        classUnderTest.doRegister(nick, password, email);
        onServerResponse(code, message);
        ctrl.verify();

        //with email should not have any influence
        ctrl.reset();
        NickServ.server = IrcServer.siedler3_net;
        ircSupportsEmailReg = true;
        expectDoRegister(nick);
        expectOnServerResponse(code, message);
        expectOnResultFailed(I18n.getString(ERROR_MESSAGE_SERVICE_DOWN));
        ctrl.replay();
        classUnderTest.doRegister(nick, password, email);
        onServerResponse(code, message);
        ctrl.verify();
    }

    @Test
    public void testNickInUse()
    {
        String nick2 = USER2;
        int code  = ReplyConstants.ERR_NICKNAMEINUSE;
        String message = "empty";
        expectDoRegister(nick2);
        expectOnServerResponse(code, message);
        expectOnResultFailed(I18n.getString(ERROR_MESSAGE_NICK_ALREADY_IN_USE));
        ctrl.replay();
        classUnderTest.doRegister(nick2, password, email);
        onServerResponse(code, message);
        ctrl.verify();

        //test erroneous nick, else clause
        ctrl.reset();
        message = nick2 + " :Erroneous nickname";
        code = ReplyConstants.ERR_ERRONEUSNICKNAME;
        expectDoRegister(nick2);
        expectOnServerResponse(code, message);
        expectGetNick();
        expectOnResultFailed(message);
        ctrl.replay();
        classUnderTest.doRegister(nick2, password, email);
        onServerResponse(code, message);
        ctrl.verify();
        
        //test erroneous nick, if clause
        ctrl.reset();
        message = nick2 + " :Erroneous nickname";
        String message1 = nick + " " + message; 
        code = ReplyConstants.ERR_ERRONEUSNICKNAME;
        expectDoRegister(nick2);
        expectOnServerResponse(code, message1);
        expectGetNick();
        expectGetNick();
        expectOnResultFailed(message);
        ctrl.replay();
        classUnderTest.doRegister(nick2, password, email);
        onServerResponse(code, message1);
        ctrl.verify();

    }

    @Test
    public void testNickChangeStartsRegistrationProcess()
    {
        String nick2 = USER2;
        String nick3 = "User3";
        expectDoRegister(nick2);
        expectOnNickChange(nick2);
        ctrl.replay();
        classUnderTest.doRegister(nick2, password, email);
        onNickChange(nick, nick2);
        ctrl.verify();

        //check that duplicate message does not trigger again
        ctrl.reset();
        expectDoRegister(nick2);
        expectOnNickChange(nick2);
        ctrl.replay();
        classUnderTest.doRegister(nick2, password, email);
        onNickChange(nick, nick2);
        onNickChange(nick, nick2);
        ctrl.verify();

        //test that wrong nickchange does not trigger anything
        ctrl.reset();
        expectDoRegister(nick2);
        expectOnWrongNickChange(nick3);
        ctrl.replay();
        classUnderTest.doRegister(nick2, password, email);
        classUnderTest.onNickChange(nick, "user", "abc.hostname.com", nick3);
        ctrl.verify();

        //somebody else takes the desired nick (hypothetical)
        ctrl.reset();
        expectDoRegister(nick2);
        expectOnWrongNickChange(nick2);
        ctrl.replay();
        classUnderTest.doRegister(nick2, password, email);
        classUnderTest.onNickChange(nick3, "user", "abc.hostname.com", nick2);
        ctrl.verify();
    }

    @Test
    public void testDisconnected()
    {
        isConnected = false;
        expectDoRegister(nick);
        expectOnResultAborted();
        ctrl.replay();
        classUnderTest.doRegister(nick, password, email);
        ctrl.verify();

        isConnected = true;
        ctrl.reset();
        expectDoRegister(nick);
        expectOnDisconnect();
        ctrl.replay();
        classUnderTest.doRegister(nick, password, email);
        classUnderTest.onDisconnect();
        ctrl.verify();

    }


    @Test
    public void testManualAbort()
    {
        expectStopListening();
        ctrl.replay();
        classUnderTest.manualAbort();
        ctrl.verify();
    }

    private void expectRequiresAuthCode()
    {
    }

    private void expectHandleEmailSent()
    {
    }

    private void expectHandleInvalidEmailMessage()
    {
    }

    private void expectTriggerTransmitCommand(String nick, String password, String email)
    {
        expectOnNotice();
        expectHandleNickServeStatusReply(nick);
        expectTransmitRegistrationCommand(password, email);
    }


    private void expectOnResultOkAuthCodeSent()
    {
        expectStopListening();
        mockResultEventListener.onResultEvent(createResult(ResultCode.SPECIAL_AUTH_CODE_SENT));
    }

    private void expectTransmitRegistrationCommand(String password, String email)
    {
        expectRegisterNickToServer(password, email);
        expectQueryNickStatus();
    }

    private void expectRegisterNickToServer(String password, String email)
    {
        mockIrcInterface.setVerbose(false);
        mockIrcInterface.registerNickToServer(password, email);
        mockIrcInterface.setVerbose(ALobbyConstants.IRC_DEBUG_ON);
    }

    private void expectNickAlreadyRegistered(String nick, String password, String email, int status)
    {
        String notice;
        ctrl.reset();
        expectDoRegister(nick);
        expectOnNotice();
        expectHandleNickServeStatusReply(nick);
        expectOnResultFailed(I18n.getString(ERROR_MESSAGE_NICK_ALREADY_REGISTERED));
        ctrl.replay();
        classUnderTest.doRegister(nick, password, email);
        notice = NickServ.createStatus(nick, status);
        onNickServeNotice(notice, nick);
        ctrl.verify();
    }

    private void onNickServeNotice(String notice, String target)
    {
        //System.out.println(notice);
        classUnderTest.onNotice("NickServ", "services", "irc.siedler3.net", target, notice);
    }

    private void onServerResponse(int code, String message)
    {
        classUnderTest.onServerResponse(code, message);
    }

    private void onNickChange(String oldNick, String newNick)
    {
        classUnderTest.onNickChange(oldNick, "user", "abc.hostname.com", newNick);
    }

    private void expectDoRegister(String nick)
    {
        expectIsConnected();
        if (this.isConnected)
        {
            expectStartListening();
            expectGetNick();
            if (nick.equals(this.nick))
            {
                expectQueryNickStatus();
            }
            else
            {
                expectChangeNick(nick);
            }
        }
    }

    @Override
    protected IrcEventListener getIrcEventListener()
    {
        return classUnderTest;
    }

    @Override
    protected ResultEventProvider getResultEventProvider()
    {
        return classUnderTest;
    }

    protected void expectHandlePasswordTooShortMessage()
    {
    }
}
