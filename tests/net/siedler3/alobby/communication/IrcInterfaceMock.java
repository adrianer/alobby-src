package net.siedler3.alobby.communication;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.util.ArrayList;

import org.jibble.pircbot.ConnectionSettings;
import org.jibble.pircbot.DccChat;
import org.jibble.pircbot.DccFileTransfer;
import org.jibble.pircbot.IrcException;
import org.jibble.pircbot.NickAlreadyInUseException;
import org.jibble.pircbot.User;

public class IrcInterfaceMock implements IrcInterface
{

	public ArrayList<Object> calledMethods;
	ArrayList<IrcEventListener> eventListeners;
	String nick = "";

	public IrcInterfaceMock()
	{
		calledMethods = new ArrayList<Object>();
		eventListeners = new ArrayList<IrcEventListener>();
	}

	@Override
	public void addEventListener(IrcEventListener anEventHandler) {
		eventListeners.add(anEventHandler);
	}

	@Override
	public boolean removeEventListener(IrcEventListener anEventHandler) {
		return eventListeners.remove(anEventHandler);
	}

	public void triggerOnConnect() {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onConnect();
		}
	}


	public void triggerOnDisconnect() {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onDisconnect();
		}
	}


	public void triggerOnServerResponse(int code, String response) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onServerResponse(code, response);
		}
	}


	public void triggerOnUserList(String channel, User[] users) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onUserList(channel, users);
		}
	}


	public void triggerOnMessage(String channel, String sender, String login, String hostname, String message) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onMessage(channel, sender, login, hostname, message);
		}
	}


	public void triggerOnPrivateMessage(String sender, String login, String hostname, String message) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onPrivateMessage(sender, login, hostname, message);
		}
	}


	public void triggerOnAction(String sender, String login, String hostname, String target, String action) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onAction(sender, login, hostname, target, action);
		}
	}


	public void triggerOnNotice(String sourceNick, String sourceLogin, String sourceHostname, String target, String notice) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onNotice(sourceNick, sourceLogin, sourceHostname, target, notice);
		}
	}


	public void triggerOnJoin(String channel, String sender, String login, String hostname) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onJoin(channel, sender, login, hostname);
		}
	}


	public void triggerOnPart(String channel, String sender, String login, String hostname) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onPart(channel, sender, login, hostname);
		}
	}


	public void triggerOnNickChange(String oldNick, String login, String hostname, String newNick) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onNickChange(oldNick, login, hostname, newNick);
		}
	}


	public void triggerOnKick(String channel, String kickerNick, String kickerLogin, String kickerHostname, String recipientNick, String reason) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onKick(channel, kickerNick, kickerLogin, kickerHostname, recipientNick, reason);
		}
	}


	public void triggerOnQuit(String sourceNick, String sourceLogin, String sourceHostname, String reason) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onQuit(sourceNick, sourceLogin, sourceHostname, reason);
		}
	}


	public void triggerOnTopic(String channel, String topic, String setBy, long date, boolean changed) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onTopic(channel, topic, setBy, date, changed);
		}
	}


	public void triggerOnChannelInfo(String channel, int userCount, String topic) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onChannelInfo(channel, userCount, topic);
		}
	}


	public void triggerOnMode(String channel, String sourceNick, String sourceLogin, String sourceHostname, String mode) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onMode(channel, sourceNick, sourceLogin, sourceHostname, mode);
		}
	}


	public void triggerOnUserMode(String targetNick, String sourceNick, String sourceLogin, String sourceHostname, String mode) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onUserMode(targetNick, sourceNick, sourceLogin, sourceHostname, mode);
		}
	}


	public void triggerOnOp(String channel, String sourceNick, String sourceLogin, String sourceHostname, String recipient) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onOp(channel, sourceNick, sourceLogin, sourceHostname, recipient);
		}
	}


	public void triggerOnDeop(String channel, String sourceNick, String sourceLogin, String sourceHostname, String recipient) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onDeop(channel, sourceNick, sourceLogin, sourceHostname, recipient);
		}
	}


	public void triggerOnVoice(String channel, String sourceNick, String sourceLogin, String sourceHostname, String recipient) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onVoice(channel, sourceNick, sourceLogin, sourceHostname, recipient);
		}
	}


	public void triggerOnDeVoice(String channel, String sourceNick, String sourceLogin, String sourceHostname, String recipient) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onDeVoice(channel, sourceNick, sourceLogin, sourceHostname, recipient);
		}
	}


	public void triggerOnSetChannelKey(String channel, String sourceNick, String sourceLogin, String sourceHostname, String key) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onSetChannelKey(channel, sourceNick, sourceLogin, sourceHostname, key);
		}
	}


	public void triggerOnRemoveChannelKey(String channel, String sourceNick, String sourceLogin, String sourceHostname, String key) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onRemoveChannelKey(channel, sourceNick, sourceLogin, sourceHostname, key);
		}
	}


	public void triggerOnSetChannelLimit(String channel, String sourceNick, String sourceLogin, String sourceHostname, int limit) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onSetChannelLimit(channel, sourceNick, sourceLogin, sourceHostname, limit);
		}
	}


	public void triggerOnRemoveChannelLimit(String channel, String sourceNick, String sourceLogin, String sourceHostname) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onRemoveChannelLimit(channel, sourceNick, sourceLogin, sourceHostname);
		}
	}


	public void triggerOnSetChannelBan(String channel, String sourceNick, String sourceLogin, String sourceHostname, String hostmask) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onSetChannelBan(channel, sourceNick, sourceLogin, sourceHostname, hostmask);
		}
	}


	public void triggerOnRemoveChannelBan(String channel, String sourceNick, String sourceLogin, String sourceHostname, String hostmask) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onRemoveChannelBan(channel, sourceNick, sourceLogin, sourceHostname, hostmask);
		}
	}


	public void triggerOnSetTopicProtection(String channel, String sourceNick, String sourceLogin, String sourceHostname) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onSetTopicProtection(channel, sourceNick, sourceLogin, sourceHostname);
		}
	}


	public void triggerOnRemoveTopicProtection(String channel, String sourceNick, String sourceLogin, String sourceHostname) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onRemoveTopicProtection(channel, sourceNick, sourceLogin, sourceHostname);
		}
	}


	public void triggerOnSetNoExternalMessages(String channel, String sourceNick, String sourceLogin, String sourceHostname) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onSetNoExternalMessages(channel, sourceNick, sourceLogin, sourceHostname);
		}
	}


	public void triggerOnRemoveNoExternalMessages(String channel, String sourceNick, String sourceLogin, String sourceHostname) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onRemoveNoExternalMessages(channel, sourceNick, sourceLogin, sourceHostname);
		}
	}


	public void triggerOnSetInviteOnly(String channel, String sourceNick, String sourceLogin, String sourceHostname) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onSetInviteOnly(channel, sourceNick, sourceLogin, sourceHostname);
		}
	}


	public void triggerOnRemoveInviteOnly(String channel, String sourceNick, String sourceLogin, String sourceHostname) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onRemoveInviteOnly(channel, sourceNick, sourceLogin, sourceHostname);
		}
	}


	public void triggerOnSetModerated(String channel, String sourceNick, String sourceLogin, String sourceHostname) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onSetModerated(channel, sourceNick, sourceLogin, sourceHostname);
		}
	}


	public void triggerOnRemoveModerated(String channel, String sourceNick, String sourceLogin, String sourceHostname) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onRemoveModerated(channel, sourceNick, sourceLogin, sourceHostname);
		}
	}


	public void triggerOnSetPrivate(String channel, String sourceNick, String sourceLogin, String sourceHostname) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onSetPrivate(channel, sourceNick, sourceLogin, sourceHostname);
		}
	}


	public void triggerOnRemovePrivate(String channel, String sourceNick, String sourceLogin, String sourceHostname) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onRemovePrivate(channel, sourceNick, sourceLogin, sourceHostname);
		}
	}


	public void triggerOnSetSecret(String channel, String sourceNick, String sourceLogin, String sourceHostname) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onSetSecret(channel, sourceNick, sourceLogin, sourceHostname);
		}
	}


	public void triggerOnRemoveSecret(String channel, String sourceNick, String sourceLogin, String sourceHostname) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onRemoveSecret(channel, sourceNick, sourceLogin, sourceHostname);
		}
	}


	public void triggerOnInvite(String targetNick, String sourceNick, String sourceLogin, String sourceHostname, String channel)  {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onInvite(targetNick, sourceNick, sourceLogin, sourceHostname, channel);
		}
	}


	public void triggerOnIncomingFileTransfer(DccFileTransfer transfer) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onIncomingFileTransfer(transfer);
		}
	}


	public void triggerOnFileTransferFinished(DccFileTransfer transfer, Exception e) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onFileTransferFinished(transfer, e);
		}
	}


	public void triggerOnIncomingChatRequest(DccChat chat) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onIncomingChatRequest(chat);
		}
	}


	public void triggerOnVersion(String sourceNick, String sourceLogin, String sourceHostname, String target) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onVersion(sourceNick, sourceLogin, sourceHostname, target);
		}
	}


	public void triggerOnPing(String sourceNick, String sourceLogin, String sourceHostname, String target, String pingValue) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onPing(sourceNick, sourceLogin, sourceHostname, target, pingValue);
		}
	}


	public void triggerOnServerPing(String response) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onServerPing(response);
		}
	}


	public void triggerOnTime(String sourceNick, String sourceLogin, String sourceHostname, String target) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onTime(sourceNick, sourceLogin, sourceHostname, target);
		}
	}


	public void triggerOnFinger(String sourceNick, String sourceLogin, String sourceHostname, String target) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onFinger(sourceNick, sourceLogin, sourceHostname, target);
		}
	}


	public void triggerOnUnknown(String line) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onUnknown(line);
		}
	}

	@Override
	public void setNick(String newNick)
	{
		nick = newNick;
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("setNick");
		methodNameAndParameters.add(newNick);
		calledMethods.add(methodNameAndParameters);
	}

	@Override
	public void _setVersion(String version)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("_setVersion");
		methodNameAndParameters.add(version);
		calledMethods.add(methodNameAndParameters);
	}

	@Override
	public void connect(String hostname) throws IOException, IrcException,
			NickAlreadyInUseException
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("connect");
		methodNameAndParameters.add(hostname);
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void connect(String hostname, int port) throws IOException, IrcException,
			NickAlreadyInUseException
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("connect");
		methodNameAndParameters.add(hostname);
		methodNameAndParameters.add(new Integer(port));
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void connect(String hostname, int port, String password) throws IOException,
			IrcException, NickAlreadyInUseException
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("connect");
		methodNameAndParameters.add(hostname);
		methodNameAndParameters.add(new Integer(port));
		methodNameAndParameters.add(password);
		calledMethods.add(methodNameAndParameters);
	}
	
	@Override
	public void connect(ConnectionSettings cs) throws IOException,
			IrcException, NickAlreadyInUseException
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("connect");
		methodNameAndParameters.add(cs.server);
		methodNameAndParameters.add(new Integer(cs.port));
		methodNameAndParameters.add(cs.password);
		methodNameAndParameters.add(cs.useSSL);
		calledMethods.add(methodNameAndParameters);
	}

	@Override
	public void reconnect() throws IOException, IrcException, NickAlreadyInUseException
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("reconnect");
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void disconnect()
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("disconnect");
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void setAutoNickChange(boolean autoNickChange)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("setAutoNickChange");
		methodNameAndParameters.add(new Boolean(autoNickChange));
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void startIdentServer()
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("startIdentServer");
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void joinChannel(String channel)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("joinChannel");
		methodNameAndParameters.add(channel);
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void joinChannel(String channel, String key)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("joinChannel");
		methodNameAndParameters.add(channel);
		methodNameAndParameters.add(key);
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void partChannel(String channel)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("partChannel");
		methodNameAndParameters.add(channel);
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void partChannel(String channel, String reason)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("partChannel");
		methodNameAndParameters.add(channel);
		methodNameAndParameters.add(reason);
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void quitServer()
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("quitServer");
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void quitServer(String reason)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("quitServer");
		methodNameAndParameters.add(reason);
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void sendRawLine(String line)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("sendRawLine");
		methodNameAndParameters.add(line);
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void sendRawLineViaQueue(String line)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("sendRawLineViaQueue");
		methodNameAndParameters.add(line);
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void sendMessage(String target, String message)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("sendMessage");
		methodNameAndParameters.add(target);
		methodNameAndParameters.add(message);
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void sendAction(String target, String action)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("sendAction");
		methodNameAndParameters.add(target);
		methodNameAndParameters.add(action);
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void sendNotice(String target, String notice)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("sendNotice");
		methodNameAndParameters.add(target);
		methodNameAndParameters.add(notice);
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void sendCTCPCommand(String target, String command)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("sendCTCPCommand");
		methodNameAndParameters.add(target);
		methodNameAndParameters.add(command);
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void changeNick(String newNick)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("changeNick");
		methodNameAndParameters.add(newNick);
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void identify(String password)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("identify");
		methodNameAndParameters.add(password);
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void setMode(String channel, String mode)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("setMode");
		methodNameAndParameters.add(channel);
		methodNameAndParameters.add(mode);
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void sendInvite(String nick, String channel)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("sendInvite");
		methodNameAndParameters.add(nick);
		methodNameAndParameters.add(channel);
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void ban(String channel, String hostmask)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("ban");
		methodNameAndParameters.add(channel);
		methodNameAndParameters.add(hostmask);
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void unBan(String channel, String hostmask)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("unBan");
		methodNameAndParameters.add(channel);
		methodNameAndParameters.add(hostmask);
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void op(String channel, String nick)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("op");
		methodNameAndParameters.add(channel);
		methodNameAndParameters.add(nick);
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void deOp(String channel, String nick)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("deOp");
		methodNameAndParameters.add(channel);
		methodNameAndParameters.add(nick);
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void voice(String channel, String nick)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("voice");
		methodNameAndParameters.add(channel);
		methodNameAndParameters.add(nick);
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void deVoice(String channel, String nick)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("deVoice");
		methodNameAndParameters.add(channel);
		methodNameAndParameters.add(nick);
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void setTopic(String channel, String topic)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("setTopic");
		methodNameAndParameters.add(channel);
		methodNameAndParameters.add(topic);
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void kick(String channel, String nick)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("kick");
		methodNameAndParameters.add(channel);
		methodNameAndParameters.add(nick);
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void kick(String channel, String nick, String reason)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("kick");
		methodNameAndParameters.add(channel);
		methodNameAndParameters.add(nick);
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void listChannels()
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("listChannels");
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void listChannels(String parameters)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("listChannels");
		methodNameAndParameters.add(parameters);
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public DccFileTransfer dccSendFile(File file, String nick, int timeout)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("dccSendFile");
		methodNameAndParameters.add(file);
		methodNameAndParameters.add(nick);
		methodNameAndParameters.add(new Integer(timeout));
		calledMethods.add(methodNameAndParameters);
		return null;
	}


	@Override
	public DccChat dccSendChatRequest(String nick, int timeout)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("dccSendChatRequest");
		methodNameAndParameters.add(nick);
		methodNameAndParameters.add(new Integer(timeout));
		calledMethods.add(methodNameAndParameters);
		return null;
	}


	@Override
	public void log(String line)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("log");
		methodNameAndParameters.add(line);
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public void setVerbose(boolean verbose)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("setVerbose");
		methodNameAndParameters.add(new Boolean(verbose));
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public String getName()
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("getName");
		calledMethods.add(methodNameAndParameters);
		return null;
	}


	@Override
	public String getNick()
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("getNick");
		calledMethods.add(methodNameAndParameters);
		return nick;
	}


	@Override
	public String getLogin()
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("getLogin");
		calledMethods.add(methodNameAndParameters);
		return null;
	}


	@Override
	public String getVersion()
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("getVersion");
		calledMethods.add(methodNameAndParameters);
		return null;
	}


	@Override
	public String getFinger()
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("getFinger");
		calledMethods.add(methodNameAndParameters);
		return null;
	}


	@Override
	public boolean isConnected()
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("isConnected");
		calledMethods.add(methodNameAndParameters);
		return false;
	}


	@Override
	public void setMessageDelay(long delay)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("setMessageDelay");
		methodNameAndParameters.add(new Long(delay));
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public long getMessageDelay()
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("getMessageDelay");
		calledMethods.add(methodNameAndParameters);
		return 0;
	}


	@Override
	public int getMaxLineLength()
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("getMaxLineLength");
		calledMethods.add(methodNameAndParameters);
		return 0;
	}


	@Override
	public int getOutgoingQueueSize()
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("getOutgoingQueueSize");
		calledMethods.add(methodNameAndParameters);
		return 0;
	}


	@Override
	public String getServer()
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("getServer");
		calledMethods.add(methodNameAndParameters);
		return null;
	}


	@Override
	public int getPort()
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("getPort");
		calledMethods.add(methodNameAndParameters);
		return 0;
	}
	
	@Override
	public boolean useSSL()
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("useSSL");
		calledMethods.add(methodNameAndParameters);
		return false;
	}

	@Override
	public String getPassword()
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("getPassword");
		calledMethods.add(methodNameAndParameters);
		return null;
	}


	@Override
	public int[] longToIp(long address)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("longToIp");
		methodNameAndParameters.add(new Long(address));
		calledMethods.add(methodNameAndParameters);
		return null;
	}


	@Override
	public long ipToLong(byte[] address)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("ipToLong");
		ArrayList<Byte> byteArray = new ArrayList<Byte>();
		for (byte currentByte : address) {
			byteArray.add(new Byte(currentByte));
		}
		methodNameAndParameters.add(byteArray);
		calledMethods.add(methodNameAndParameters);
		return 0;
	}


	@Override
	public void setEncoding(String charset) throws UnsupportedEncodingException
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("setEncoding");
		methodNameAndParameters.add(charset);
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public String getEncoding()
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("getEncoding");
		calledMethods.add(methodNameAndParameters);
		return null;
	}


	@Override
	public InetAddress getInetAddress()
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("getInetAddress");
		calledMethods.add(methodNameAndParameters);
		return null;
	}


	@Override
	public void setDccInetAddress(InetAddress dccInetAddress)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("setDccInetAddress");
		methodNameAndParameters.add(dccInetAddress);
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public InetAddress getDccInetAddress()
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("getDccInetAddress");
		calledMethods.add(methodNameAndParameters);
		return null;
	}


	@Override
	public int[] getDccPorts()
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("getDccPorts");
		calledMethods.add(methodNameAndParameters);
		return null;
	}


	@Override
	public void setDccPorts(int[] ports)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("setDccPorts");
		ArrayList<Integer> integerArray = new ArrayList<Integer>();
		for (int currentInteger : ports) {
			integerArray.add(new Integer(currentInteger));
		}
		methodNameAndParameters.add(integerArray);
		calledMethods.add(methodNameAndParameters);
	}


	@Override
	public User[] getUsers(String channel)
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("getUsers");
		methodNameAndParameters.add(channel);
		calledMethods.add(methodNameAndParameters);
		return null;
	}


	@Override
	public String[] getChannels()
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("getChannels");
		calledMethods.add(methodNameAndParameters);
		return null;
	}


	@Override
	public void dispose()
	{
		ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
		methodNameAndParameters.add("dispose");
		calledMethods.add(methodNameAndParameters);
	}

	public void resetFlags()
	{
		calledMethods.clear();
	}

    @Override
    public void queryNickStatus()
    {
        ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
        methodNameAndParameters.add("queryNickStatus");
        calledMethods.add(methodNameAndParameters);
    }

    @Override
    public boolean isNickServe(String sourceNick, String sourceLogin, String sourceHostname)
    {
        if (sourceNick.equalsIgnoreCase("NickServ") && sourceLogin.equalsIgnoreCase("services") && sourceHostname.toLowerCase().startsWith("irc."))
        {
            return true;
        }
        return false;
    }

    @Override
    public void registerNickToServer(String password, String eMail)
    {
        ArrayList<Object> methodNameAndParameters = new ArrayList<Object>();
        methodNameAndParameters.add("registerNickToServer");
        methodNameAndParameters.add(password);
        methodNameAndParameters.add(eMail);
        calledMethods.add(methodNameAndParameters);
    }

    @Override
    public IrcServerInfo getIrcServerInfo()
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public void sendNickServCommand(NickServCommand command, String options)
    {
        throw new UnsupportedOperationException();
    }

}
