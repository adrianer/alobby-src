package net.siedler3.alobby.communication.protocol;

import static org.junit.Assert.assertEquals;
import net.siedler3.alobby.controlcenter.GoodsInStock;
import net.siedler3.alobby.controlcenter.OpenGame;
import net.siedler3.alobby.util.Base64Coder;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

public class ProgramMessageProtocolTest
{
    private static final String MAP_2 = "USER\\schöner siedeln ;).map";
    private static final String MAP_MULTI = "MULTI\\A256-2-Quick3.map";
    private static final String MAP_1 = "USER\\alien.map";
    private static final String NAME_1 = "xy's game";
    private static final String NAME_2 = "xy's game ;) ;))#äöüß";
    private static final String RANDOM_1 = "RANDOM\\384_1_1";
    private static final String SAVEGAME_1 = "Savegame\\ea152b27-c4b1-452f-a71b-a9ef5ed625b8.mps";
    private final static char SEPARATOR = ';';

    private ProgramMessageProtocol protocol;
    private String token;
    private String userName;
    private String hostUserName;
    private String mapName;
    private String encodedMapName;
    private String ip;
    private String name;
    private String encodedName;
    private int goods;
    private int maxPlayerNumber;
    private int currentPlayerNumber;
    private boolean amazons;
    private boolean isLeagueGame;
    private OpenGame game;
    private String expectedResult;
    private String result;
    private boolean wimo;
    private int tournamentid;
    private String tournamentname;
	private int matchnumber;
	private int round;
	private int groupnumber;

    @Before
    public void setUp() throws Exception
    {
        protocol = new ProgramMessageProtocol();
        userName = "Testuser";
        hostUserName = "GameHost";
        ip = "1.2.3.4";
        setName(NAME_1);
        setMapName(MAP_1);
        goods = GoodsInStock.Viel.getIndex();
        maxPlayerNumber = 12;
        currentPlayerNumber = 1;
        amazons = true;
        isLeagueGame = false;
        game = newGame();
        tournamentid = 1;
        tournamentname = "Example-Tournament";
    }

    private OpenGame newGame()
    {
        return new OpenGame(ip, hostUserName, name, mapName, amazons, goods, maxPlayerNumber, currentPlayerNumber, isLeagueGame, wimo, tournamentid, tournamentname, round, groupnumber, matchnumber, new ArrayList<>());
    }

    @Test
    public void testGameMessages()
    {
        token = "1";
        result = protocol.createMsgNewCurrentPlayerNumber(game);
        expectedResult = format(token, ip, hostUserName, currentPlayerNumber);
        assertEquals(expectedResult, result);

        token = "2";
        result = protocol.createMsgNewMaxPlayerNumber(game);
        expectedResult = format(token, ip, hostUserName, maxPlayerNumber);
        assertEquals(expectedResult, result);

        token = "3";
        result = protocol.createMsgCloseGame(game);
        expectedResult = format(token, ip, hostUserName);
        assertEquals(expectedResult, result);

        token = "4";
        result = protocol.createMsgQueryOpenGames(userName);
        expectedResult = format(token, userName);
        assertEquals(expectedResult, result);

        token = "5";
        result = protocol.createMsgIncrementCurrentPlayerNumber(hostUserName);
        expectedResult = format(token);
        assertEquals(expectedResult, result);

        token = "6";
        result = protocol.createMsgDecrementCurrentPlayerNumber(hostUserName);
        expectedResult = format(token);
        assertEquals(expectedResult, result);

        token = "8";
        result = protocol.createMsgNewGameName(game);
        expectedResult = format(token, ip, hostUserName, encodedName);
        assertEquals(expectedResult, result);
    }

    @Test
    public void testNewGame()
    {
        result = protocol.createMsgGameInfo(game);
        expectedResult = expectedNewGamePlain();
        assertEquals(expectedResult, result);

        amazons = false;
        goods = GoodsInStock.Wenig.getIndex();
        game = newGame();
        result = protocol.createMsgGameInfo(game);
        expectedResult = expectedNewGamePlain();
        assertEquals(expectedResult, result);

        amazons = true;
        setMapName(MAP_2);
        goods = GoodsInStock.Viel.getIndex();
        game = newGame();
        result = protocol.createMsgGameInfo(game);
        expectedResult = expectedNewGameEncoded();
        assertEquals(expectedResult, result);

        amazons = false;
        setName(NAME_2);
        game = newGame();
        result = protocol.createMsgGameInfo(game);
        expectedResult = expectedNewGameEncoded();
        assertEquals(expectedResult, result);

        setMapName(MAP_MULTI);
        game = newGame();
        result = protocol.createMsgGameInfo(game);
        expectedResult = expectedNewGameEncoded();
        assertEquals(expectedResult, result);

        amazons = true;
        setName(NAME_1);
        setMapName(RANDOM_1);
        game = newGame();
        result = protocol.createMsgGameInfo(game);
        expectedResult = expectedNewGamePlain();
        assertEquals(expectedResult, result);

        setName(NAME_2);
        game = newGame();
        result = protocol.createMsgGameInfo(game);
        expectedResult = expectedNewGameEncoded();
        assertEquals(expectedResult, result);


        setName(NAME_1);
        setMapName(SAVEGAME_1);
        game = newGame();
        result = protocol.createMsgGameInfo(game);
        expectedResult = expectedNewGamePlain();
        assertEquals(expectedResult, result);

        setName(NAME_2);
        setMapName(SAVEGAME_1);
        game = newGame();
        result = protocol.createMsgGameInfo(game);
        expectedResult = expectedNewGameEncoded();
        assertEquals(expectedResult, result);


        setName(NAME_1);
        amazons = false;
        goods = GoodsInStock.Viel.getIndex();
        isLeagueGame = true;
        game = newGame();
        result = protocol.createMsgGameInfo(game);
        expectedResult = expectedNewGamePlain();
        assertEquals(expectedResult, result);


        setName(NAME_2);
        isLeagueGame = true;
        game = newGame();
        result = protocol.createMsgGameInfo(game);
        expectedResult = expectedNewGameEncoded();
        assertEquals(expectedResult, result);


    }

    private String expectedNewGamePlain()
    {
        if (isLeagueGame)
        {
            return format(0, ip, hostUserName, name, mapName, maxPlayerNumber, currentPlayerNumber, getAmazonsString(), goods, isLeagueGameString());
        }
        else
        {
            return format(0, ip, hostUserName, name, mapName, maxPlayerNumber, currentPlayerNumber, getAmazonsString(), goods);
        }
    }

    private String expectedNewGameEncoded()
    {
        if (isLeagueGame)
        {
            return format(9, ip, hostUserName, encodedName, encodedMapName, maxPlayerNumber, currentPlayerNumber, getAmazonsString(), goods, isLeagueGameString());
        }
        else
        {
            return format(9, ip, hostUserName, encodedName, encodedMapName, maxPlayerNumber, currentPlayerNumber, getAmazonsString(), goods);
        }
    }


    private String getAmazonsString()
    {
        return amazons ? "true" : "false";
    }

    private String isLeagueGameString()
    {
        return isLeagueGame ? "1" : "0";
    }

    private void setName(String name)
    {
        this.name = name;
        this.encodedName = Base64Coder.encodeString(name);
    }

    private void setMapName(String name)
    {
        this.mapName = name;
        this.encodedMapName = Base64Coder.encodeString(name);
    }


    private static String format(Object... entries)
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < entries.length; i++)
        {
            sb.append(entries[i].toString());
            if (i < entries.length - 1)
            {
                sb.append(SEPARATOR);
            }
        }
        return sb.toString();
    }

}
