package net.siedler3.alobby.util;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.gui.GUI;
import net.siedler3.alobby.gui.MessageDialog;

/**
 * Tetris-Application
 * 
 * @source https://gist.github.com/DataWraith/5236083
 * 
 * Advanced through Zwirni regarding the official rules for tetris
 * from The Tetris Company (http://tetris.wikia.com/wiki/Tetris_Guideline) 
 *
 */

public class Tetris extends JPanel {

	public GUI gui;
	public boolean gameover = false;
	private JPanel pnlGameOver = new JPanel();

	private final Point[][][] Tetraminos = {
		// I-Piece
		{
			{ new Point(0, 1), new Point(1, 1), new Point(2, 1), new Point(3, 1) },
			{ new Point(1, 0), new Point(1, 1), new Point(1, 2), new Point(1, 3) },
			{ new Point(0, 1), new Point(1, 1), new Point(2, 1), new Point(3, 1) },
			{ new Point(1, 0), new Point(1, 1), new Point(1, 2), new Point(1, 3) }
		},
		
		// J-Piece
		{
			{ new Point(0, 1), new Point(1, 1), new Point(2, 1), new Point(2, 0) },
			{ new Point(1, 0), new Point(1, 1), new Point(1, 2), new Point(2, 2) },
			{ new Point(0, 1), new Point(1, 1), new Point(2, 1), new Point(0, 2) },
			{ new Point(1, 0), new Point(1, 1), new Point(1, 2), new Point(0, 0) }
		},
		
		// L-Piece
		{
			{ new Point(0, 1), new Point(1, 1), new Point(2, 1), new Point(2, 2) },
			{ new Point(1, 0), new Point(1, 1), new Point(1, 2), new Point(0, 2) },
			{ new Point(0, 1), new Point(1, 1), new Point(2, 1), new Point(0, 0) },
			{ new Point(1, 0), new Point(1, 1), new Point(1, 2), new Point(2, 0) }
		},
		
		// O-Piece
		{
			{ new Point(0, 0), new Point(0, 1), new Point(1, 0), new Point(1, 1) },
			{ new Point(0, 0), new Point(0, 1), new Point(1, 0), new Point(1, 1) },
			{ new Point(0, 0), new Point(0, 1), new Point(1, 0), new Point(1, 1) },
			{ new Point(0, 0), new Point(0, 1), new Point(1, 0), new Point(1, 1) }
		},
		
		// S-Piece
		{
			{ new Point(1, 0), new Point(2, 0), new Point(0, 1), new Point(1, 1) },
			{ new Point(0, 0), new Point(0, 1), new Point(1, 1), new Point(1, 2) },
			{ new Point(1, 0), new Point(2, 0), new Point(0, 1), new Point(1, 1) },
			{ new Point(0, 0), new Point(0, 1), new Point(1, 1), new Point(1, 2) }
		},
		
		// T-Piece
		{
			{ new Point(1, 0), new Point(0, 1), new Point(1, 1), new Point(2, 1) },
			{ new Point(1, 0), new Point(0, 1), new Point(1, 1), new Point(1, 2) },
			{ new Point(0, 1), new Point(1, 1), new Point(2, 1), new Point(1, 2) },
			{ new Point(1, 0), new Point(1, 1), new Point(2, 1), new Point(1, 2) }
		},
		
		// Z-Piece
		{
			{ new Point(0, 0), new Point(1, 0), new Point(1, 1), new Point(2, 1) },
			{ new Point(1, 0), new Point(0, 1), new Point(1, 1), new Point(0, 2) },
			{ new Point(0, 0), new Point(1, 0), new Point(1, 1), new Point(2, 1) },
			{ new Point(1, 0), new Point(0, 1), new Point(1, 1), new Point(0, 2) }
		}
	};
	
	private final String[] tetraminoImages = {
		ALobbyConstants.PACKAGE_PATH + "/gui/img/glocke02.png",
		ALobbyConstants.PACKAGE_PATH + "/gui/img/kugel_rot2.png",
		ALobbyConstants.PACKAGE_PATH + "/gui/img/kugel_blau.png",
		ALobbyConstants.PACKAGE_PATH + "/gui/img/kugel_gruen.png",
		ALobbyConstants.PACKAGE_PATH + "/gui/img/glocke03.png",
		ALobbyConstants.PACKAGE_PATH + "/gui/img/kugel_blau.png",
		ALobbyConstants.PACKAGE_PATH + "/gui/img/kugel_rot2.png"
	};
	
	private final Color[] tetraminoColors = {
		Color.cyan, Color.blue, Color.orange, Color.yellow, Color.green, Color.pink, Color.red
	};
	
	private Point pieceOrigin;
	private int currentPiece;
	private int rotation;
	private ArrayList<Integer> nextPieces = new ArrayList<Integer>();

	public long score;
	private Color[][] well;
	private String[][] wellImages;
	private int clearedRows = 0;
	// set the variables for window-width
	private int windowSizeWidth = 0;
	// set the field size (width and height)
	private int playfieldHeight = 26;
	private int playfieldWidth = 12;
	// set the block-size (width and height are the same)
	private int blockWidthHeight = 26;
	private boolean printPauseOnScreen = false;
	private int holdPiece = -1;
	// Panel for hold-position including the draw-function for the piece in it
	private JPanel pnlHold = new JPanel() {
		@Override 
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			if( holdPiece >= 0 ) {
				g.setColor(tetraminoColors[holdPiece]);
				for (Point p : Tetraminos[holdPiece][0]) {
					g.fillRect(p.x*blockWidthHeight+blockWidthHeight, 
							   p.y*blockWidthHeight+blockWidthHeight, 
							   blockWidthHeight-1, blockWidthHeight-1);
				}
			}
		}
	};
	private JPanel pnlNext = new JPanel();
	private boolean previewPanelVisible = false;
	private int previewTetranimosCount = 0;
	private int pieceIndex;
	private boolean itsChristmastime;

	// Creates a border around the well and initializes the dropping piece
	public void init() {
		
		// calculate window-size depending on block-size
		// and how much blocks should be on one line
		windowSizeWidth = 10*this.blockWidthHeight;
		
		// set the layout for the following panel-components
		setLayout(new BorderLayout());
		
		// create the left panel in size of 4 block-elements
		// which will show the hold-panel
		JPanel pnlLeft = new JPanel();
		pnlLeft.setSize(windowSizeWidth/2, getPlayfieldHeight()*getBlockWidthHeight());
		pnlLeft.setPreferredSize(pnlLeft.getSize());
		pnlLeft.setLayout(new BorderLayout());
		pnlHold.setSize(4*getBlockWidthHeight(), 4*getBlockWidthHeight());
		pnlHold.setPreferredSize(pnlHold.getSize());
		pnlLeft.add(pnlHold, BorderLayout.NORTH);
		add(pnlLeft, BorderLayout.WEST);
		
		// create the right panel in size of 4 block-elements
		// which will show the next blocks in list
		pnlNext = new JPanel();
		pnlNext.setLayout(new BoxLayout(pnlNext, BoxLayout.Y_AXIS));
		pnlNext.setSize(windowSizeWidth/2, getPlayfieldHeight()*getBlockWidthHeight());
		pnlNext.setPreferredSize(pnlNext.getSize());
		add(pnlNext, BorderLayout.EAST);
		
		// create the game-over-panel which will only be visible
		// if the game is over
		JLabel lblGameOver = new JLabel();
		lblGameOver.setText("Game Over");
		pnlGameOver.setOpaque(false);
		pnlGameOver.setVisible(false);
		pnlGameOver.setSize(200, 40);
		pnlGameOver.setPreferredSize(pnlGameOver.getSize());
		pnlGameOver.add(lblGameOver, BorderLayout.CENTER);
		add(pnlGameOver, BorderLayout.NORTH);
		
		// create the wall for the tetranimos
		// in the defined sizes
		well = new Color[this.playfieldWidth][this.playfieldHeight];
		for (int i = 0; i < this.playfieldWidth; i++) {
			for (int j = 0; j < this.playfieldHeight-1; j++) {
				if (i == 0 || i == this.playfieldWidth-1 || j == this.playfieldHeight-2) {
					well[i][j] = Color.GRAY;
				} else {
					well[i][j] = Color.BLACK;
				}
			}
		}
		wellImages = new String[this.playfieldWidth][this.playfieldHeight];
		newPiece( -1 );
	}
	
	/**
	 * Put a new, random piece into the dropping position
	 * 
	 * @param usePiece	value >= 0 if a specific piece should be used
	 */
	public void newPiece( int usePiece ) {
		// fill the nextPieces-list with up to 6 random pieces
		if( nextPieces.isEmpty() ) {
			Collections.addAll(nextPieces, 0, 1, 2, 3, 4, 5, 6);
			Collections.shuffle(nextPieces);
		}
		// if less than 6 pieces in the nextPieces-list
		// add more to fill the list with up to 6 pieces
		// but never add two identical pieces after each other
		while( nextPieces.size() <= Tetraminos.length - 1 ) {
			int randomPiece = (int)((Math.random()) * Tetraminos.length);
			if( randomPiece != nextPieces.get(nextPieces.size()-1) ) {
				nextPieces.add(randomPiece);
			}
		}
		// select the new piece
		// -> choose given piece
		if( usePiece >= 0 ) {
			currentPiece = usePiece;
		}
		else {
			// -> or select the first entry from the nextPieces-list
			currentPiece = nextPieces.get(0);
			// -> and remove it from the nextPieces-list
			nextPieces.remove(0);
		}
		// update the preview-Panel if it is visible
		if( this.previewPanelVisible && false == this.gameover ) {
			pnlNext.removeAll();
			pnlNext.revalidate();
			pieceIndex = 0;
			for (Integer piece: nextPieces) {
				if( pnlNext.getComponentCount() < this.previewTetranimosCount ) {
					pieceIndex++;
				    JPanel pnlPiece = new JPanel() {
				    	@Override 
						public void paintComponent(Graphics g) {
							super.paintComponent(g);
							g.setColor(tetraminoColors[piece]);
							for (Point p : Tetraminos[piece][0]) {
								g.fillRect(p.x*blockWidthHeight+blockWidthHeight, 
										   p.y*blockWidthHeight+blockWidthHeight, 
										   blockWidthHeight-1, blockWidthHeight-1);
							}							
						}
				    };
				    pnlPiece.setSize(5*getBlockWidthHeight(), 5*getBlockWidthHeight());
				    pnlPiece.setPreferredSize(pnlPiece.getSize());
				    // mark the first piece as it will be the next which will be used in game
				    if( pieceIndex == 1 ) {
				    	pnlPiece.setBackground(Color.black);
				    }
				    pnlPiece.repaint();
				    pnlNext.add(pnlPiece);
				}
			}
			repaint();
		}
		// start-position depends on which piece will be drawn
		pieceOrigin = new Point(4, 2);
		if( currentPiece == 0 ) {
			pieceOrigin = new Point(4, 2);
		}
		if( currentPiece == 3 ) {
			pieceOrigin = new Point(5, 2);
		}
		// set the initial rotation
		rotation = 0;
		// game ends if collision is detected
		if( collidesAt(pieceOrigin.x, pieceOrigin.y, rotation) ) {
			gameover = true;
			pnlGameOver.setVisible(true);
			pnlHold.setVisible(false);
			pnlNext.setVisible(false);
			repaint();
		}
	}
	
	/**
	 * Collision test for the dropping piece
	 * 
	 * @param x
	 * @param y
	 * @param rotation
	 * @return boolean	true if collision is detected
	 */
	private boolean collidesAt(int x, int y, int rotation) {
		for (Point p : Tetraminos[currentPiece][rotation]) {
			if (well[p.x + x][p.y + y] != Color.BLACK) {
				return true;
			}
		}
		return false;
	}
	
	/** 
	 * Rotate the piece clockwise or counterclockwise
	 * 
	 * @param i
	 * @param simulate	true if rotation should only be simulated, not really done
	 * @return boolean	true if rotation is possible
	 */
	public boolean rotate(int i, boolean simulate ) {
		int newRotation = (rotation + i) % 4;
		if (newRotation < 0) {
			newRotation = 3;
		}
		boolean rotationDone = false;
		if (!collidesAt(pieceOrigin.x, pieceOrigin.y, newRotation)) {
			rotation = newRotation;
			rotationDone = true;
		}
		if( false == simulate ) {
			repaint();
		}
		return rotationDone;
	}
	
	/**
	 * Move the piece left or right
	 * 
	 * @param i
	 * @param simulate	true if move should only be simulated, not really done
	 * @return boolean	true if move is possible
	 */
	public boolean move(int i, boolean simulate ) {
		boolean moveDone = false;
		if (!collidesAt(pieceOrigin.x + i, pieceOrigin.y, rotation)) {
			pieceOrigin.x += i;
			moveDone = true;
		}
		if( false == simulate ) {
			repaint();
		}
		return moveDone;
	}
	
	/**
	 * Soft drops the piece one line or fixes it to the well if it can't drop
	 * 
	 * @return boolean	true if a the soft drop following rotate or move is possible
	 * 
	 */
	public boolean dropDown() {
		if (!collidesAt(pieceOrigin.x, pieceOrigin.y + 1, rotation)) {
			pieceOrigin.y += 1;
		} else {
			fixToWell();
			// simulate rotate and moving and check if the piece could do it
			// -> if one is possible send true back as the user could change something before finalizing the position 
			if( rotate(-1, true) || rotate(+1, true) || move(-1, true) || move(+1, true) ) {
				return true;
			}
		}
		repaint();
		return false;
	}
	
	/**
	 * Hard drops the piece to the bottom in one shot and fix is to well
	 * 
	 * @return int	the y-coodinate where the piece was dropped
	 */
	public int hardDropDown() {
		int droppoints = pieceOrigin.y;
		for( int i=pieceOrigin.y;i<this.playfieldHeight;i++ ) {
			if (!collidesAt(pieceOrigin.x, i, rotation)) {
				pieceOrigin.y = i;
			} else {
				score += i - droppoints;
				fixToWell();
				i=this.playfieldHeight;
			}
			repaint();
		}
		return droppoints;
	}
	
	/**
	 * Make the dropping piece part of the well, so it is available for
	 * collision detection.
	 */
	public void fixToWell() {
		for (Point p : Tetraminos[currentPiece][rotation]) {
			well[pieceOrigin.x + p.x][pieceOrigin.y + p.y] = tetraminoColors[currentPiece];
			if( false != this.itsChristmastime ) {
				wellImages[pieceOrigin.x + p.x][pieceOrigin.y + p.y] = tetraminoImages[currentPiece];
			}
		}
		clearRows();
		newPiece( -1 );
	}
	
	/**
	 * Removes a given row from wall.
	 * 
	 * @param row
	 */
	public void deleteRow(int row) {
		for (int j = row-1; j > 0; j--) {
			for (int i = 1; i < 11; i++) {
				well[i][j+1] = well[i][j];
				if( false != this.itsChristmastime ) {
					wellImages[i][j+1] = wellImages[i][j];
				}
			}
		}
	}
	
	/**
	 * Calculate the actual level depending on removed rows.
	 * -> Variable Goal System: user must clear 10 more lines for each level
	 *   (1 => 10, 2 => 20, 3 => 30, 4 => 40 ..)
	 * @return
	 */
	public int getLevel() {
		return this.clearedRows/10+1;
	}
	
	/**
	 * Clear completed rows from the field and award score according to
	 * the number of simultaneously cleared rows.
	 */
	public void clearRows() {
		boolean gap;
		int numClears = 0;
		
		for (int j = this.playfieldHeight-3; j > 0; j--) {
			gap = false;
			for (int i = 1; i < this.playfieldWidth-1; i++) {
				if (well[i][j] == Color.BLACK) {
					gap = true;
					break;
				}
			}
			if (!gap) {
				deleteRow(j);
				j += 1;
				numClears += 1;
			}
		}
		
		// count removed rows
		clearedRows += numClears;
		
		// add cleared rows to score multiplied with the actual level
		switch (numClears) {
			case 1:
				score += 100*this.getLevel();
				break;
			case 2:
				score += 300*this.getLevel();
				break;
			case 3:
				score += 500*this.getLevel();
				break;
			case 4:
				score += 800*this.getLevel();
				break;
		}

	}
	
	/**
	 * Draw the falling piece
	 * 
	 * @param g
	 */
	private void drawPiece(Graphics g) {
		g.setColor(tetraminoColors[currentPiece]);
		for (Point p : Tetraminos[currentPiece][rotation]) {
			if( false == this.itsChristmastime ) {
				g.fillRect((p.x + pieceOrigin.x) * this.blockWidthHeight+windowSizeWidth/2, 
						   (p.y + pieceOrigin.y) * this.blockWidthHeight, 
						   this.blockWidthHeight-1, this.blockWidthHeight-1);
			}
			else {
				Graphics2D g2 = (Graphics2D)g.create();
				Image img;
				try {
					img = ImageIO.read(GUI.class.getResourceAsStream(tetraminoImages[currentPiece]));
					g2.drawImage(img, (p.x + pieceOrigin.x) * this.blockWidthHeight+windowSizeWidth/2, (p.y + pieceOrigin.y) * this.blockWidthHeight, img.getWidth(this), img.getHeight(this), this);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Paint the wall an its components new depending on actual values. 
	 */
	@Override 
	public void paintComponent(Graphics g)
	{
		
		if (this.isOpaque()) {
	        Color color = (this.isEnabled()) ? this.getBackground() : this.getBackground().darker().darker();
	        g.setColor(color);
	        g.fillRect(0, 0, this.getWidth(), this.getHeight());
	    }
		
		// Paint the well
		g.fillRect(0, 0, this.blockWidthHeight*this.playfieldWidth, this.blockWidthHeight*this.playfieldHeight-1);
		for (int i = 0; i < this.playfieldWidth; i++) {
			for (int j = 0; j < this.playfieldHeight-1; j++) {
				g.setColor(well[i][j]);
				if( false == this.itsChristmastime ) {
					g.fillRect(this.blockWidthHeight*i+windowSizeWidth/2, this.blockWidthHeight*j, this.blockWidthHeight-1, this.blockWidthHeight-1);
				}
				else {
					if( null != wellImages[i][j] ) {
						Graphics2D g2 = (Graphics2D)g.create();
						Image img;
						try {
							img = ImageIO.read(GUI.class.getResourceAsStream(wellImages[i][j]));
							g2.drawImage(img, this.blockWidthHeight*i+windowSizeWidth/2, this.blockWidthHeight*j, img.getWidth(this), img.getHeight(this), this);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					else {
						g.fillRect(this.blockWidthHeight*i+windowSizeWidth/2, this.blockWidthHeight*j, this.blockWidthHeight-1, this.blockWidthHeight-1);
					}
				}
				
			}
		}
		
		// Display the score
		g.setColor(Color.WHITE);
		g.drawString("" + score, 19*this.playfieldWidth+windowSizeWidth/2, 25);
		
		// Display the level
		g.drawString("Lvl " + this.getLevel(), 19*this.playfieldWidth+windowSizeWidth/2, 50);
		
		// Display the cleared rows
		g.drawString("Rows " + this.clearedRows, 19*this.playfieldWidth+windowSizeWidth/2, 75);
		
		// Display pause on screen if game is paused
		if( false != this.printPauseOnScreen ) {
			g.setColor(Color.YELLOW);
			g.drawString("Pause", 19*this.playfieldWidth+windowSizeWidth/2, 100);
		}
		
		// Draw the currently falling piece
		drawPiece(g);
	}
	
	/**
	 * Hide or show a "pause"-hint in JLabel.
	 * 
	 * @param printPauseOnScreen	true if pause should be visible, false otherwise
	 */
	public void printPauseOnScreen( boolean printPauseOnScreen ) {
		this.printPauseOnScreen = printPauseOnScreen;
		repaint();
	}
	
	/** 
	 * Return the actual score
	 * 
	 * @return long	the score
	 */
	public long getScore() {
		return score;
	}

	/**
	 * Return the playfield-height.
	 * 
	 * @return int	the height
	 */
	public int getPlayfieldHeight() {
		return playfieldHeight;
	}

	/**
	 * Return the playfield-width.
	 * 
	 * @return int	the width
	 */
	public int getPlayfieldWidth() {
		return playfieldWidth;
	}
	
	/**
	 * Return the block-width and -height
	 * 
	 * @return int	the block-size
	 */
	public int getBlockWidthHeight() {
		return blockWidthHeight;
	}

	/**
	 * Return the sleep-Time between piece moves.
	 * 
	 * @return long
	 */
	public long getSleepTime() {
		// calculate the time depending on actual level:
		// (0.8 - ((level - 1) * 0.007))^(level-1)
		double part1 = 0.8 - ( ( this.getLevel() - 1 ) * 0.007);
		double part2 = this.getLevel()-1;
		double result = Math.pow(part1, part2);
		double returnvalue = result*1000; 
		return (long)returnvalue;
	}

	/**
	 * Return the number of removed rows.
	 * 
	 * @return int
	 */
	public int getRows() {
		return this.clearedRows;
	}

	/**
	 * Set a delay as pause.
	 * 
	 * @param delta
	 */
	public void pause( long delta ) {
		try{Thread.sleep(delta);}catch(InterruptedException e){e.printStackTrace();}
	}

	/**
	 * Return the calculated window-size
	 * 
	 * @return int
	 */
	public int getWindowSizeWidth() {
		return this.windowSizeWidth;
	}

	/**
	 * Set the actucal falling piece into the hold-position.
	 * Set an existing holded piece in this position into game.
	 * Two identical pieces could not be switched in hold-position.
	 */
	public void setPieceInHold() {
		if( currentPiece != this.holdPiece || this.holdPiece == -1 ) {
			// secure the last hold piece
			int oldholdedPiece = this.holdPiece;
			// secure the actual piece in hold-position
			this.holdPiece  = currentPiece;
			// draw the actual piece in hold-position
			pnlHold.repaint();
			// if an piece actual existed in the hold-position use it now in the game
			if( oldholdedPiece >= 0 ) {
				newPiece( oldholdedPiece );
			}
			else {
				// otherwise use the next peace as normal
				newPiece( -1 );
			}
			repaint();
		}
	}

	/**
	 * Show of hide the preview of next tetranimos in right panel.
	 * 
	 * @param int	number of preview-tetranimos to show in panel,
	 * 				zero if no preview should be visible,
	 * 				max. 6
	 */
	public void showPreview(int count) {
		if( count > 0 && count <= 6 ) {
			this.previewPanelVisible  = true;
			this.previewTetranimosCount  = count;
			pnlNext.setVisible(true);
		}
		else {
			this.previewPanelVisible = true;
			pnlNext.setVisible(false);
		}
	}

	/**
	 * Set marker if its actucal christmastime.
	 * 
	 * @param itsChristmastime
	 */
	public void setChristmas(boolean itsChristmastime) {
		this.itsChristmastime = itsChristmastime;
	}
	
}
