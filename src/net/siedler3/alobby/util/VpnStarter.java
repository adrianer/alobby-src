/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.util;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.i18n.I18n;


public class VpnStarter extends Thread
{
    private SettlersLobby settlersLobby;
    private Process process;
    private File configDir;
    private boolean isStarting = false;
    private boolean wasStarted = false;
    private boolean wasKilled = false;

    public VpnStarter(SettlersLobby settlersLobby) throws IOException
    {
        isStarting = true;
        wasStarted = true;
        wasKilled = false;
        Test.output("VpnStarter started.");
        try {
            this.settlersLobby = settlersLobby;
            setName(getName() + "-VPNStarter"); //$NON-NLS-1$
            String openvpnExePath = settlersLobby.getOpenVpnPath();
            File openvpnExe = new File(openvpnExePath);
            if (!openvpnExe.exists()) {
                String msg = I18n.getString("SettlersLobby.VPN_NO_OPENVPN");
                settlersLobby.getGUI().displayError(msg);
                // disable vpn-autoconnect because without vpnkey it is unnecessary
                settlersLobby.config.setAutoconnectVpn(false);
                throw new IOException(msg);
            }
            configDir = new File(SettlersLobby.configDir);
            File configFile = null;
            File[] configFiles = configDir.listFiles();
            for (File file : configFiles) {
                if (file.getName().toLowerCase().endsWith(".ovpn") && file.getName().toLowerCase().contains(settlersLobby.getNick().toLowerCase())) {
                    configFile = file;
                    break;
                }
            }
            if (configFile == null) {
                // load textcolor of actualy used theme as Color-Object
                @SuppressWarnings("static-access")
                String hexcolor = SettlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor());
                // set hexcolor as color for links in errortext
                String msg = MessageFormat.format(I18n.getString("SettlersLobby.VPN_NO_CONFIG"), hexcolor);
                settlersLobby.getGUI().displayError(msg);
                // disable vpn-autoconnect because without vpnkey it is unnecessary
                settlersLobby.config.setAutoconnectVpn(false);
                throw new IOException(msg);
            }
            List<String> command = new ArrayList<>();
            if (settlersLobby.IS_WINE) {
                command.add("cmd");
                command.add("/c");
                command.add("start");
                command.add("/b");
                command.add("/wait");
                command.add("/d");
                // Using backslashes in the path does not work with Wine 8.16 and newer anymore
                command.add(configDir.getAbsolutePath().replace("\\", "/"));  // cwd
                command.add(openvpnExe.getAbsolutePath().replace("\\", "/"));
                command.add(configDir.getAbsolutePath().replace("\\", "/"));  // config dir
                command.add(configFile.getName());
            } else {
                command.add(openvpnExe.getAbsolutePath());
                command.add("--verb");
                command.add("4");
                command.add("--log");
                command.add(SettlersLobby.configDir + File.separator + "vpn.log");
                command.add("--config");
                command.add(configFile.getName());
                command.add("--remote");
                command.add(settlersLobby.config.getVpnDomain());
                command.add("--rport");
                command.add("13947");
                // Allow connecting to Servers that use 1024 bit TLS-CAs, which is not permitted anymore with newer OpenSSL versions
                // by default (which are used by OpenVPN 2.5+).
                //command.add("--tls-cipher");
                //command.add("\"DEFAULT:@SECLEVEL=0\"");
            }
            ProcessBuilder builder = new ProcessBuilder(command);
            builder.directory(configDir);
            builder.inheritIO();
            Test.output("starting VPN: " + builder.command());
            process = builder.start();

            if (process != null)
            {
                Test.output("using class " + process.getClass().getName()); //$NON-NLS-1$
            }
        } finally {
            String ipVpn = settlersLobby.getVpnIpFromSystem();
            if (ipVpn != null) {
                settlersLobby.getGUI().setIpVpn(ipVpn);
                settlersLobby.sendProgramMessageVPNState(true);
                settlersLobby.ircCommunicator.onVPNStateChange(settlersLobby.getNick(), true);
            }
            isStarting = false;
            Test.output("VpnStarter finished.");
        }
    }

    public static Path findSystemOpenVpnPath(){
        String path = System.getenv("PATH");
        String[] dirs = path.split(";");
        for (String dir: dirs){
          Path realPath = Paths.get(dir, SettlersLobby.OPENVPN_EXE_SYSTEM);
          File file = new File(realPath.toString());
          if (file.canExecute()) {
              return realPath;
          }
        }
        return null;
      }

    @Override
    public void run()
    {
        try
        {
            int rc = process.waitFor();
            Test.output("OpenVPN exited: " + rc);
        }
        catch (Exception ex)
        {
            Test.outputException(ex);
            //something unexpected happened
        }
        finally
        {
            int exit = process.exitValue();
            if (!settlersLobby.IS_WINE) {
                // On wine the process does immediately exit
                destroyVpn();
            }
            process = null;
            //from the point of the alobby VPN has exited
            String ipVpn = settlersLobby.getVpnIpFromSystem();
            if (ipVpn != null) {
                settlersLobby.getGUI().setIpVpn(ipVpn);
            } else if (exit == 0) {
                settlersLobby.getGUI().setVpnText("\uf127", Color.red, I18n.getString("SettlersLobby.VPN_OFF"));
            } else if (settlersLobby.IS_WINE) {
                settlersLobby.getGUI().setVpnText("\uf127", Color.yellow, I18n.getString("SettlersLobby.VPN_STARTING"));
            } else {
                settlersLobby.getGUI().setVpnText("\uf127", Color.red, I18n.getString("SettlersLobby.VPN_ERROR"));
            }
        }
    }

    public void destroyVpn()
    {
        if (process != null)
        {
            process.destroy();
        }
        if (settlersLobby.IS_WINE) {
            //'cmd /c start /unix /bin/kill -2'
            File pidFile = new File(SettlersLobby.configDir + File.separator + "vpn.pid");
            if (pidFile.exists() && wasStarted && !wasKilled) {
                try(BufferedReader br = new BufferedReader(new FileReader(pidFile.getAbsolutePath()))) {
                    StringBuilder sb = new StringBuilder();
                    String line = br.readLine();
                    if (line != null) {
                        sb.append(line);
                    }
                    String pid = sb.toString();
                    if (pid.length() > 0) {
                        List<String> command = new ArrayList<>();
                        command.add("cmd");
                        command.add("/c");
                        command.add("start");
                        command.add("/b");
                        command.add("/wait");
                        // Using backslashes in the path does not work with Wine 8.16 and newer anymore
                        command.add(new File(settlersLobby.OPENVPN_KILL_WRAPPER).getAbsolutePath().replace("\\", "/"));
                        command.add(pid);
                        ProcessBuilder builder = new ProcessBuilder(command);
                        builder.inheritIO();
                        Test.output("killing Unix VPN: " + builder.command());
                        Process killProcess = builder.start();
                        killProcess.waitFor();
                        if (killProcess != null) {
                            killProcess.destroy();
                        }
                    }
                } catch (FileNotFoundException e) {
                    Test.outputException(e);
                } catch (IOException e) {
                    Test.outputException(e);
                } catch (InterruptedException e) {
                    Test.outputException(e);
                }
            }
        }
        wasKilled = true;
        wasStarted = false;
    }

    public boolean isVpnRunning()
    {
        return process != null;
    }

    public boolean isVpnStarting()
    {
        return isStarting;
    }
}
