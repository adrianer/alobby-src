package net.siedler3.alobby.util.s3sm;
import java.io.File;

public class SaveManagerAlgo
{
   public static void Quicksort(File[] Dateiliste)
   {
      Quicksort(Dateiliste, 0, Dateiliste.length-1);
   }
   public static void Quicksort(File[] Dateiliste, int IndexA, int IndexE)                    //Quicksort mit Duplikattoleranz von 10 
   {
      if (IndexA<IndexE)
      {
         File Zwischenspeicher;
         int PE = IndexA+(IndexE-IndexA)/2, A=IndexA, E=IndexE, Duplikatanzahl=0, Duplikatpositionen[] = new int[10];
         Boolean Duplikatzählen=true;
         while(A!=E || Duplikatanzahl!=0)
         {
            if (A==E)
            {
               Duplikatzählen=false;
               PE=Duplikatpositionen[--Duplikatanzahl];
               A=IndexA;
               E=IndexE;
            }
            while(Dateiliste[A].lastModified()>Dateiliste[PE].lastModified() || (Dateiliste[A].lastModified()==Dateiliste[PE].lastModified() && Dateiliste[A]!=Dateiliste[PE]))
            {
               if (Dateiliste[A].lastModified()==Dateiliste[PE].lastModified() && Dateiliste[A]!=Dateiliste[PE] && Duplikatzählen)
                  Duplikatpositionen[Duplikatanzahl++]=A;
               ++A;
            }
            while(Dateiliste[E].lastModified()<Dateiliste[PE].lastModified() || (Dateiliste[E].lastModified()==Dateiliste[PE].lastModified() && Dateiliste[E]!=Dateiliste[PE]))
            {
               if (Dateiliste[E].lastModified()==Dateiliste[PE].lastModified() && Dateiliste[E]!=Dateiliste[PE] && Duplikatzählen)
                  Duplikatpositionen[Duplikatanzahl++]=E;
               --E;
            }
            if (PE==A)
               PE=E;
            else if (PE==E)
               PE=A;
            Zwischenspeicher = Dateiliste[A];
            Dateiliste[A]=Dateiliste[E];
            Dateiliste[E]=Zwischenspeicher;
         }
         Quicksort(Dateiliste, IndexA, PE-1);
         Quicksort(Dateiliste, PE+1, IndexE);
      }
   }
}