/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;

import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;

/**
 * Prepare launch with s3-patch.
 * 
 * @author Zwirni
 */
public class S3CEGeneration extends S3Support {

    // define list of files which are used for the patch
    private static final List<String> ceResourceFiles = Arrays.asList(
            ALobbyConstants.PACKAGE_PATH + "/util/s3support/s3ce.exe",
            ALobbyConstants.PACKAGE_PATH + "/util/s3support/crashpad_handler.exe",
            ALobbyConstants.PACKAGE_PATH + "/util/s3support/s3.dll",
            ALobbyConstants.PACKAGE_PATH + "/util/s3support/s3ce.de.po",
            ALobbyConstants.PACKAGE_PATH + "/util/s3support/s3ce.fr.po",
            ALobbyConstants.PACKAGE_PATH + "/util/s3support/s3ce.it.po",
            ALobbyConstants.PACKAGE_PATH + "/util/s3support/s3ce.pl.po"
    );

    private static final String mfc140dllUtilPath = ALobbyConstants.PACKAGE_PATH + "/util/commonsupport/mfc140.dll"; //$NON-NLS-1$
    private static final String msvcp140dllUtilPath = ALobbyConstants.PACKAGE_PATH + "/util/commonsupport/msvcp140.dll"; //$NON-NLS-1$
    private static final String ucrtbasedllUtilPath = ALobbyConstants.PACKAGE_PATH + "/util/commonsupport/ucrtbase.dll"; //$NON-NLS-1$
    private static final String vcruntime140dllUtilPath = ALobbyConstants.PACKAGE_PATH + "/util/commonsupport/vcruntime140.dll"; //$NON-NLS-1$

	private RandomAccessFile file;
	private String s3ExePath;
	private SettlersLobby settlersLobby;

	/**
	 * Constructor
	 * 
	 * @param file
	 * @param s3ExePath
	 * @param settlersLobby
	 */
	public S3CEGeneration(RandomAccessFile file, String s3ExePath, SettlersLobby settlersLobby) {
		this.file = file;
		this.s3ExePath = s3ExePath;
		this.settlersLobby = settlersLobby;
	}

	/**
	 * Check if the files for the patch should be recreated.
	 * 
	 * @return boolean true if they should be recreated
	 * @throws IOException
	 */
    private boolean shouldReCreate() throws IOException {
        for (String resource : ceResourceFiles) {
            File s3File = getTargetCeFile(resource);
            if (!Files.exists(s3File.toPath())) {
                Test.output("The file " + s3File.getAbsolutePath() + " does not exist yet, re-generating files.");
                return true;
            }

            InputStream resourceAsStream = S3CEGeneration.class.getResourceAsStream(resource + ".md5");
            String md5Bytes = new String(resourceAsStream.readAllBytes(), StandardCharsets.UTF_8);
            resourceAsStream.close();
            String s3Checksum = getMD5Checksum(s3File).toLowerCase();
            if (!md5Bytes.startsWith(s3Checksum)) {
                Test.output("The checksum for file " + s3File.getAbsolutePath() + " changed, re-generating files.");
                return true;
            }

            Test.output("File " + s3File.getAbsolutePath() + " already generated, skipping.");
        }

        return false;
    }

	/**
	 * Get the file to patch.
	 * 
	 * @param resource
	 * @return
	 */
    private File getTargetCeFile(String resource)
    {
        String parent = new File(s3ExePath).getParent();
        if (resource.endsWith(".po")) {
            parent += "/res";
        }

        return new File(parent, new File(resource).getName());
    }

	/**
	 * Prepare environment to run s3 with s3-patch.
	 * 
	 * @return boolean	true if launcher is ready
	 */
	public boolean doGenerate() {
        try {
            // First generate the s3_alobby.exe-file in its latest version 1.73
            S3ExeGeneration173 s3AlobbyExeGenerator = new S3ExeGeneration173(file, s3ExePath, settlersLobby);
            if (!s3AlobbyExeGenerator.doGenerate()) {
                return false;
            }

            // set s3path
            String s3Path = new File(s3ExePath).getParentFile().getAbsolutePath();

            // If everything is already in the expected state, we don't need to recreate anything
            if (!shouldReCreate()) {
                return true;
            }

            // The directory "res" must exist
            if (!new File(s3Path, "res").exists()) {
                Files.createDirectories(new File(s3Path, "res").toPath());
            }

            // Extract our own files
            for (String resource : ceResourceFiles) {
                InputStream resourceAsStream = S3CEGeneration.class.getResourceAsStream(resource);
                File targetFile = getTargetCeFile(resource);
                ExtractFile.extract(
                        resourceAsStream,
                        targetFile.getName(),
                        targetFile.getParentFile()
                );
                resourceAsStream.close();
            }

            // Extract the needed Microsoft VC DLL files
            InputStream mfc140dllAsStream = S3CEGeneration.class.getResourceAsStream(mfc140dllUtilPath);
            ExtractFile.extract(mfc140dllAsStream, new File(mfc140dllUtilPath).getName(), new File(s3Path));
            mfc140dllAsStream.close();
            InputStream msvcp140dllAsStream = S3CEGeneration.class.getResourceAsStream(msvcp140dllUtilPath);
            ExtractFile.extract(msvcp140dllAsStream, new File(msvcp140dllUtilPath).getName(), new File(s3Path));
            msvcp140dllAsStream.close();
            InputStream ucrtbaseAsStream = S3CEGeneration.class.getResourceAsStream(ucrtbasedllUtilPath);
            ExtractFile.extract(ucrtbaseAsStream, new File(ucrtbasedllUtilPath).getName(), new File(s3Path));
            ucrtbaseAsStream.close();
            InputStream vcruntime140AsStream = S3CEGeneration.class.getResourceAsStream(vcruntime140dllUtilPath);
            ExtractFile.extract(vcruntime140AsStream, new File(vcruntime140dllUtilPath).getName(), new File(s3Path));
            vcruntime140AsStream.close();

            Test.output("s3ce.exe is ready to launch");
            return true;
        }
        catch(Exception e)
        {
            Test.outputException(e);
            if (file != null) {
                try {
                    file.close();
                } catch (IOException e2) {
                    Test.outputException(e2);
                }
            }
            return false;
        }
    }
}
