package net.siedler3.alobby.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.util.List;

import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.nativesupport.NativeFunctions;
import net.siedler3.alobby.s3gameinterface.IProcessWindowSupport;

import com.sun.jna.platform.win32.WinDef.HWND;


public class JavaProcessWrapper extends Process implements IProcessWindowSupport, IProvidesProcessIdAndWindowHandle
{

    private Process process;
    private int processId;
    private HWND processWindowHandle;
    private long hProcess;
    private boolean isInitialised = false;

    private JavaProcessWrapper(Process process)
    {
        this.process = process;
        retrieveProcessIdAndWindowHandle();
    }

    private void retrieveProcessIdAndWindowHandle()
    {
        if (process.getClass().getName().equals("java.lang.ProcessImpl") ||
            process.getClass().getName().equals("java.lang.Win32Process"))
        {
            try
            {
            	// TODO replace this with other method to get the pid via NativeFunctions 
            	// as Java 11 does not support handle-field anymore on ProcessImpl
                Field f = process.getClass().getDeclaredField("handle");
                f.setAccessible(true);
                hProcess = f.getLong(process);

                isInitialised = NativeFunctions.getInstance().deriveProcessIdAndWindowHandleFromProcessHandle(this, hProcess);
                if (!isInitialised)
                {
                    //revert to error-prone method using FindWindow
                    isInitialised = NativeFunctions.getInstance().findS3WindowHandleUsingFindWindow(this, hProcess);
                }
            }
            catch (Throwable e)
            {
                Test.outputException(e);
            }
        }
    }

    @Override
    public int hashCode()
    {
        return process.hashCode();
    }

    @Override
    public OutputStream getOutputStream()
    {
        return process.getOutputStream();
    }

    @Override
    public InputStream getInputStream()
    {
        return process.getInputStream();
    }

    @Override
    public InputStream getErrorStream()
    {
        return process.getErrorStream();
    }
    
    @Override
    public boolean equals(Object obj)
    {
        return process.equals(obj);
    }

    @Override
    public int waitFor() throws InterruptedException
    {
        return process.waitFor();
    }

    @Override
    public int exitValue()
    {
        return process.exitValue();
    }

    @Override
    public void destroy()
    {
        process.destroy();
    }

    @Override
    public String toString()
    {
        return process.toString();
    }

    @Override
    public boolean isForegroundWindow()
    {
        return NativeFunctions.getInstance().isForegroundWindow(processWindowHandle);
    }

    public static Process createProcess(String cmdLine) throws IOException
    {
        ProcessBuilder builder = new ProcessBuilder(cmdLine);
        return startProcessAndCreateWrapper(builder);
    }

    public static Process createProcess(String cmdLine, File directory) throws IOException
    {
        ProcessBuilder builder = new ProcessBuilder(cmdLine);
        builder.directory(directory);
        return startProcessAndCreateWrapper(builder);
    }
    
    public static Process createProcessWithParam(List<String> args) throws IOException
    {
        ProcessBuilder builder = new ProcessBuilder(args);
        return startProcessAndCreateWrapper(builder);
    }  

    public static Process createProcessWithParam(String cmdLine, String param) throws IOException
    {
        ProcessBuilder builder = new ProcessBuilder(cmdLine, param);
        return startProcessAndCreateWrapper(builder);
    }

    public static Process createProcessWithParam(String cmdLine, String param, File directory) throws IOException
    {
        ProcessBuilder builder = new ProcessBuilder(cmdLine, param);
        builder.directory(directory);
        return startProcessAndCreateWrapper(builder);
    }

    public static Process createProcessUsingNewShell(String cmdLine, File directory) throws IOException
    {
        ProcessBuilder builder = new ProcessBuilder("cmd", "/c", cmdLine);
        builder.directory(directory);
        return startProcessAndCreateWrapper(builder);
    }

    public static Process createProcessUsingNewShell(String cmdLine) throws IOException
    {
        ProcessBuilder builder = new ProcessBuilder("cmd", "/c", cmdLine);
        return startProcessAndCreateWrapper(builder);
    }

    public static Process startProcessAndCreateWrapper(ProcessBuilder builder) throws IOException
    {
        builder.redirectErrorStream(true);
       	builder.inheritIO();
        JavaProcessWrapper javaProcessWrapper = null;
        Process process = builder.start();
        try
        {
            javaProcessWrapper = new JavaProcessWrapper(process);
        }
        catch (Exception e)
        {
            Test.outputException(e);
        }
        if (javaProcessWrapper != null && javaProcessWrapper.isInitialised())
        {
            return javaProcessWrapper;
        }
        Test.output("reverting to default Java process");
        return process;
    }




    @Override
    public HWND getProcessWindowHandle()
    {
        return processWindowHandle;
    }

    @Override
    public void setProcessWindowHandle(HWND processWindowHandle)
    {
        this.processWindowHandle = processWindowHandle;
    }

    @Override
    public void setProcessId(int processId)
    {
        this.processId = processId;
    }

    @Override
    public int getProcessId()
    {
        return processId;
    }

    public long getProcessHandle()
    {
        return hProcess;
    }

    public boolean isInitialised()
    {
        return isInitialised;
    }
}
