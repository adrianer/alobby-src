/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.util.s4support;

import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.ISMap;
import net.siedler3.alobby.controlcenter.SettlersLobby;

public class S4Map implements ISMap
{
    private String mapPath;
    private int player = 0;
    private int widthHeight = 0;
    
    protected boolean isValid;
    
    public S4Map() {
    	mapPath = null;
        isValid = false;
	}
    
    protected S4Map( SettlersLobby settlersLobby )
    {
        mapPath = null;
        isValid = false;
    }
    
    private S4Map(String mapPath)
    {
        this.mapPath = mapPath;
        isValid = mapPath.startsWith(ALobbyConstants.PATH_S4MULTI);
    }
    
    private S4Map(String mapPath, SettlersLobby settlersLobby)
    {
        this.mapPath = mapPath;
        isValid = mapPath.startsWith(ALobbyConstants.PATH_S4MULTI);
    }

	public String toDisplayString()
    {
        return toString();
    }
    
    public String toString()
    {
        return mapPath;
    }
    
    /**
     * Liefert true, falls die Karte in einem korrekten Format angegeben ist.
     * Es wird nicht geprüft, ob die Mapdatei tatsächlich existiert.
     * 
     * @return true, falls die Karte in einem korrekten Format angegeben ist.
     */
    public boolean isValid()
    {
        return isValid;
    }
    
    public static ISMap newInstance(String mapPath)
    {
        return new S4Map(mapPath);
    }
    
    public static ISMap newInstance(String mapPath, SettlersLobby settlersLobby)
    {
        return new S4Map(mapPath, settlersLobby);
    }
    
    public static String getDisplayName(String mapPath, SettlersLobby settlersLobby)
    {
        ISMap map = newInstance(mapPath, settlersLobby);
        if (map.isValid())
        {
            return map.toDisplayString();
        }
        //eventuell ein savgame, einfach den Wert zurückgeben
        return mapPath;
    }
    
    public static String getDisplayName(String mapPath)
    {
    	ISMap map = newInstance(mapPath);
        if (map.isValid())
        {
            return map.toDisplayString();
        }
        //eventuell ein savgame, einfach den Wert zurückgeben
        return mapPath;
    }
    
	@Override
	public int getPlayer() {
		return this.player;
	}

	@Override
	public int getWidthHeight() {
		return this.widthHeight;
	}
}
