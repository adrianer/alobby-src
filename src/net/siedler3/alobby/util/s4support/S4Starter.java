/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.util.s4support;

import java.io.File;
import java.io.IOException;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.s3gameinterface.IProcessWindowSupport;
import net.siedler3.alobby.s3gameinterface.ProcessWindowSupportHandler;
import net.siedler3.alobby.util.JavaProcessWrapper;

public class S4Starter extends Thread
{
	private SettlersLobby settlersLobby;
	private Process process;
	private boolean cleanup = true;

	public S4Starter(SettlersLobby settlersLobby) throws IOException
	{
		this.settlersLobby = settlersLobby;
		if( false == settlersLobby.isS4GamePathValid() )
		{
		    throw new IOException(I18n.getString("S4Starter.ERROR_S4_NOT_FOUND")); //$NON-NLS-1$
		}
		setName(getName() + "-S4Starter"); //$NON-NLS-1$

		Test.output("starting: " + settlersLobby.config.getGamePathS4() + " netgame=1");
		File directory = new File(settlersLobby.config.getGamePathS4()).getParentFile();
		process = JavaProcessWrapper.createProcessWithParam(settlersLobby.config.getGamePathS4(), "netgame=1", directory);
		if( process != null )
		{
		    Test.output("using class " + process.getClass().getName()); //$NON-NLS-1$
		}

        IProcessWindowSupport processWindowSupport = null;
        if (process instanceof IProcessWindowSupport)
        {
            processWindowSupport = (IProcessWindowSupport)process;
        }
        ProcessWindowSupportHandler.getInstance().setProcessWindowSupport(processWindowSupport);

	}

	@Override
    public void run()
	{
		try
		{
			int rc = process.waitFor();
			// possible rc-values (only for the S4_alobby.exe and S4_Main.exe, as the S4_alobby_starter.exe does
			// not forward the values it gets, yet. If we want to use them, we need to change the S4_alobby_starter.exe
			// first, so it will use the upstream exit code as its own):
			// -> 0 => all ok
			// -> 1 => unknown
			// -> 3 => netgame.ini was defect (single-player-error?)
			// -> negative int-value => SE
			Test.output("S4 exited: " + rc);
			//remove the process window support if it is still used
	        if (process instanceof IProcessWindowSupport)
	        {
	            ProcessWindowSupportHandler.getInstance().removeProcessWindowSupport((IProcessWindowSupport)process);
	        }
		}
		catch (InterruptedException e)
		{
		    //the gamestarter may interrupt this thread, in this case
		    //cleanup is false
			Test.outputException(e);
			Thread.currentThread().interrupt();
		}
		catch (Exception ex)
		{
		    Test.outputException(ex);
		    //something unexpected happened
		    //clear the link to the process
		}
		finally
		{
		    process = null;
		    // from the point of the alobby S4 has exited,
		    // so call onS4ShutDown() to cleanup all related tasks, e.g. the gamestarter
		    Test.output("1111: " + cleanup + " -> " + settlersLobby);
            if (cleanup && settlersLobby != null)
            {
            	Test.output("2222");
                settlersLobby.onS4ShutDown();
            }
		}
	}

}
