/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.util.s4support;

import java.io.File;

import net.siedler3.alobby.util.LocaleChanger;

/**
 * This class consists of different language-specific words
 * for S4.
 */
public class S4Language
{
    public enum Lang
    {
        DE
//      EN,
//      PL,
//      FR,
//      IT,
//      ES,
//      KO,
//      JA
        ;

        public static Lang getLangByIndex(int index)
        {
            return values()[index];
        }

    }

    public String languageIndicator;
    public String map;
    public String[] goodsInStock;
    public String[] goodsInStockMiniChat;

    public S4Language()
    {

    }

    private static final S4Language LANG_DE = new S4Language();
    private static final S4Language LANG_EN = new S4Language();
    private static final S4Language LANG_PL = new S4Language();
    private static final S4Language LANG_FR = new S4Language();
    private static final S4Language LANG_IT = new S4Language();
    private static final S4Language LANG_ES = new S4Language();

    static
    {
        //------ German ------

        LANG_DE.goodsInStock = new String[]{
                "Wenig",
                "Mittel",
                "Viel"
        };

        //------ English ------

        LANG_EN.goodsInStock = new String[]{
                "Low",
                "Medium",
                "High"
        };

        //------ Polish ------

        LANG_PL.goodsInStock = new String[]{
                "Ma\u0142o",     //Malo
                "\u015Arednia",  //Srednia
                "Du\u017Co"      //Duzo
        };

        //------ French ------

        LANG_FR.goodsInStock = new String[]{
                "Peu",
                "Normal",
                "Beaucoup"
        };

        //------ Italian ------

        LANG_IT.goodsInStock = new String[]{
                "Basso",
                "Medio",
                "Alto"
        };

        //------ Spanish ------

        LANG_ES.goodsInStock = new String[]{
                "Bajo",
                "Medio",
                "Alto"
        };

        //-----------------------

        //muss dieselbe Reihenfolge haben wie die Sprachreihenfolge in Lang
        languageCheck = new String[] {
                LANG_DE.languageIndicator,
                LANG_EN.languageIndicator,
                LANG_PL.languageIndicator,
                LANG_FR.languageIndicator,
                LANG_IT.languageIndicator,
                LANG_ES.languageIndicator
        };

    }

    public static final String[] languageCheck;

    // TODO: how to check which local language has the S4-Installation?
    public static Lang getCurrentS3Language(File s4Path)
    {
    	int locale = LocaleChanger.readLocale(s4Path);
        switch(locale)
        {
/*            case 1:
                return Lang.EN;
            case 2:
                return Lang.IT;
            case 3:
                return Lang.FR;
            case 4:
                return Lang.PL;
            case 5:
                return Lang.ES;
            case 6:
                return Lang.KO;
            case 7:
                return Lang.JA;*/
            case 0:
            case -1:
            default:
                return Lang.DE;
        }
    }
    

    private static Lang currentLang = getCurrentS3Language(null);

    /**
     * gebe das zu {@code lang} gehörende Language Objekt zurück.
     *
     * @param lang
     * @return
     */
    public static S4Language getInstance(Lang lang)
    {
        switch (lang)
        {
/*            case EN:
                return LANG_EN;
            case PL:
                return LANG_PL;
            case FR:
                return LANG_FR;
            case IT:
                return LANG_IT;
            case ES:
                return LANG_ES;
            case DE:*/
            default:
                return LANG_DE;
        }
    }

    /**
     * Benutzt den Wert in currentLang als Sprachversion
     * für Siedler3. Default ist DE.
     * Nach dem Starten von Siedler3 wird eine Versionerkennung
     * durchgeführt, die den Wert von currentLang ändern kann.
     *
     * @return Language Objekt
     */
    public static S4Language getInstance()
    {
        return getInstance(currentLang);
    }

    /**
     * Bestimmt die Sprachversion von S3 und gibt das zugehörige Language
     * Objekt zurück.
     * Default ist DE.
     * @return Language Objekt
     */
    public static S4Language getInstance(boolean useRegistryValue)
    {
        return getInstance(getCurrentS3Language(null));
    }

    public static Lang getCurrentLanguage()
    {
        return currentLang;
    }

    public static void setCurrentLanguage(Lang lang)
    {
        currentLang = lang;
    }

    // Polish characters:
    // Small a with acute: \u0105
    // Small c with acute: \u0107
    // Small e with acute: \u0119
    // Small l with dash:  \u0142
    // Small o with acute: \u00F3
    // Small s with acute: \u015B
    // Small z with dot:   \u017C
    // Small z with acute: \u017a

    // French characters:
    // Small e with acute: \u00e9

    // Spanish characters:
    // Small i with acute: \u00ED
}
