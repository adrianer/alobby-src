package net.siedler3.alobby.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UrlHelper
{
    private static final Pattern charsetPattern = Pattern.compile("(?i)\\bcharset=\\s*\"?([^\\s;\"]*)");
    
    /**
     * does url encoding using %20 instead of '+'
     * @param input
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String urlEncode(String input) throws UnsupportedEncodingException
    {
        String result = URLEncoder.encode(input, "UTF-8");
        result = result.replaceAll("\\+", "%20");
        return result;
    }

    /**
     * Parse out a charset from a content type header.
     * 
     * @param contentType
     *            e.g. "text/html; charset=EUC-JP"
     * @return "EUC-JP", or null if not found. Charset is trimmed and
     *         uppercased.
     */
    private static String getCharsetFromContentType(String contentType) 
    {
        if (contentType == null)
            return null;

        Matcher m = charsetPattern.matcher(contentType);
        if (m.find())
        {
            return m.group(1).trim().toUpperCase();
        }
        return null;
    }
    
    /**
     * 
     * @param connection
     * @param defaultValue charset to be returned if it cannot be determined from the connection
     * @return
     * @throws IOException
     */
    public static String getCharsetFromUrlConnection(URLConnection connection, String defaultValue) throws IOException
    {
        String result = getCharsetFromUrlConnection(connection);
        return (result == null) ? defaultValue : result;
    }
    
    public static String getCharsetFromUrlConnection(URLConnection connection) throws IOException
    {
        connection.connect();
        String contentType = connection.getContentType();
        return getCharsetFromContentType(contentType);
    }
    
    public static BufferedReader createBufferedReaderFromUrl(URL url) throws IOException
    {
        URLConnection connection = url.openConnection();
        String charset = getCharsetFromUrlConnection(connection, "UTF-8");
        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), charset));
        return reader;
    }


}
