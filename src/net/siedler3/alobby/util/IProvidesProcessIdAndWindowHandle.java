package net.siedler3.alobby.util;

import com.sun.jna.platform.win32.WinDef.HWND;

public interface IProvidesProcessIdAndWindowHandle
{
    public void setProcessId(int processId);
    public int getProcessId();
    public void setProcessWindowHandle(HWND processWindowHandle);
    public HWND getProcessWindowHandle();
}
