package net.siedler3.alobby.util;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.nio.file.Files;

import net.siedler3.alobby.controlcenter.Test;

public class FileLockWrapper
{
    private class DeleteFileLockOnExit implements Runnable
    {
        @Override
        public void run()
        {
            Test.output("deleteFileLockOnExit " + (file != null ? file.getAbsolutePath() : ""));
            releaseLock();
        }
    }
    private FileLock fileLock;
    private FileChannel fileLockChannel;
    private RandomAccessFile fileLockFile;
    private File file;
    private Thread deleteOnExit;
    
    public FileLockWrapper(File file)
    {
        this.file = file;
    }

    public boolean aquireLock()
    {
        try
        {
            //Delete the file, if it was left over e.g. after a crash.
            //If this fails, then the file is currently already locked.
            Files.deleteIfExists(file.toPath());
        }
        catch (SecurityException e) 
        {
            // File is already locked in the OS
            return false;
        }
        catch (IOException e) 
        {
            // File is already locked in the OS
            return false;
        }
        
        try
        {
            fileLockFile = new RandomAccessFile(file, "rw");
            fileLockChannel = fileLockFile.getChannel();
        }
        catch (Exception e)
        {
            return false;
        }
        
        try 
        {
            fileLock = fileLockChannel.tryLock();
        } 
        catch (OverlappingFileLockException e) 
        {
            fileLock = null;
        }
        catch (IOException e)
        {
            fileLock = null;
        }
        
        if (fileLock != null)
        {
            try
            {
                deleteOnExit = new Thread(this.new DeleteFileLockOnExit());
                deleteOnExit.setName("deleteOnExitThread");
                Runtime.getRuntime().addShutdownHook(deleteOnExit);
            }
            catch (Exception e)
            {
                Test.outputException(e);
            }
        }
        else
        {
            try
            {
                fileLockChannel.close();
                fileLockChannel = null;
                fileLockFile.close();
                fileLockFile = null;
            }
            catch (Exception e)
            {
                Test.outputException(e);
            }
        }
        return fileLock != null;
    }

    public void releaseLock()
    {
        boolean deleteFile = false;
        try
        {
            if (deleteOnExit != null)
            {
                if (!deleteOnExit.isAlive())
                {
                    //deleteOnExit can only be alive, if the shutdown hooks were triggered by the JVM
                    //only try to remove the shutdownhook, if the thread is not yet alive
                    try
                    {
                        Runtime.getRuntime().removeShutdownHook(deleteOnExit);
                        deleteOnExit = null;
                    }
                    catch (Exception e)
                    {
                        Test.outputException(e);
                    }
                }
            }
            if (fileLock != null)
            {
                deleteFile = fileLock.isValid();
                fileLock.release();
                fileLock = null;
            }
            if (fileLockChannel != null)
            {
                fileLockChannel.close();
                fileLockChannel = null;
            }
            if (fileLockFile != null)
            {
                fileLockFile.close();
                fileLockFile = null;
            }
            if (file != null && deleteFile)
            {
                Files.deleteIfExists(file.toPath());
            }
        }
        catch (Exception e)
        {
            Test.outputException(e);
        }
    }
}
