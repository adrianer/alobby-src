package net.siedler3.alobby.util;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;

import net.siedler3.alobby.nativesupport.NativeFunctions;
import net.siedler3.alobby.s3gameinterface.IProcessWindowSupport;

import com.sun.jna.platform.win32.WinDef.HWND;

public class NativeProcess extends Process implements IProcessWindowSupport, IProvidesProcessIdAndWindowHandle
{
	private NativeFunctions natives = NativeFunctions.getInstance();

    private Integer exitValue;
    private int processId;
    private HWND processWindowHandle;

    @Override
    public OutputStream getOutputStream()
    {
        //not supported
        return null;
    }

    @Override
    public InputStream getInputStream()
    {
        //not supported
        return null;
    }

    @Override
    public InputStream getErrorStream()
    {
        //not supported
        return null;
    }

    @Override
    public int waitFor() throws InterruptedException
    {
        //call the native function only once
        if (this.exitValue == null)
        {
            this.exitValue = natives.waitForImpl(processId);
        }
        return exitValue();
    }

    @Override
    public int exitValue()
    {
        //only valid after waitFor() has been called
        return exitValue;
    }

    @Override
    public void destroy()
    {
        natives.destroyImpl(processId);
    }

    /**
     * returns true if any of the windows belonging to the process is the
     * active window.
     * @return
     */
    @Override
    public boolean isForegroundWindow() {
    	return natives.isForegroundWindow(processWindowHandle);
    }

    /**
     * Creates a Process by executing the cmdLine and returns
     * a Process-Object representing the lowLevel Process.
     * @param cmdLine
     * @return
     */
    public static NativeProcess createProcess(String cmdLine)
    {
    	return createProcess(cmdLine, null);
    }
    
    public static NativeProcess createProcess(String cmdLine, File directory)
    {
        NativeProcess result = new NativeProcess();
        if (!result.natives.createProcessImpl(result, cmdLine, directory))
        {
            //something was not ok during process creation,
            //return null to indicate this
            return null;
        }
        return result;
    }

	@Override
    public void setProcessId(int processId) {
		this.processId = processId;
	}

	@Override
    public void setProcessWindowHandle(HWND processWindowHandle) {
		this.processWindowHandle = processWindowHandle;
	}

	@Override
    public HWND getProcessWindowHandle() {
		return processWindowHandle;
	}

    @Override
    public int getProcessId()
    {
        return processId;
    }
}
