/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.util;

import javax.swing.JOptionPane;

import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.i18n.I18n;

public class DllLoader
{
	//Versuche einfach beide Dlls zu laden, entweder
	//die 32Bit Version oder die 64Bit Version funktioniert
	//hoffentlich. Falls keine funktioniert, breche ab
	private static boolean loaded = false;
	private static String message = "dll not loaded";
	static
	{
	    try
	    {
	        System.loadLibrary("alobby"); //$NON-NLS-1$
	        loaded = true;
	        message = "alobby.dll loaded"; //$NON-NLS-1$
	        Test.output(message);
	    }
	    catch (UnsatisfiedLinkError e)
	    {
	    }

	    try
	    {
	        System.loadLibrary("alobby64"); //$NON-NLS-1$
	        loaded = true;
            message = "alobby64.dll loaded"; //$NON-NLS-1$
            Test.output(message);
	    }
	    catch (UnsatisfiedLinkError e)
	    {
	    }

	    if(!loaded)
	    {
	        JOptionPane.showMessageDialog(null,
	                I18n.getString("SettlersLobby.ERROR_LOADING_DLL"),  //$NON-NLS-1$
	                I18n.getString("SettlersLobby.ERROR_LOADING_DLL_TITLE"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$
	        System.exit(1);
        }
	}

	public static void loadDll()
	{
		//leer, dient nur zum Aufruf aus anderen Klassen
		//beim erstmaligen Aufruf wird der static-initialiser ausgeführt
	}

	public static String getLoadedDllName()
	{
	    return message;
	}
}
