/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.util;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;

import net.siedler3.alobby.controlcenter.ReqQuery;
import net.siedler3.alobby.controlcenter.Test;


public class LocaleChanger
{
	public static final String[] LOCALES = new String[] {"Deutsch", "English", "Italiano", "Français", "Polski", "Español", "Korean", "Japanese"};
	
	// corresponding ISO-3166-country-codes
	public static final String[] LOCALES_ISO_3166 = new String[] {"de", "en", "it", "fr", "pl", "es", "kr", "jp"};
	
    public static void main(String args[])
    {
    	changeLocale(7, ReqQuery.getS3ExeDirectory());
    }
    

    public static boolean changeLocale(int locale, File s3Path)
    {
        RandomAccessFile file = null;

        try
        {
            if (s3Path == null || !Files.exists(s3Path.toPath()))
            {
            	s3Path = ReqQuery.getS3ExeDirectory();
                if (s3Path == null || !Files.exists(s3Path.toPath()))
                {
                    // Test.output(ReqQuery.s3ExeFilePath);
                    return false;
                }
            }

            File Siedler3_01Path = new File(s3Path, "SND" + File.separator + "Siedler3_01.dat");
            // The file may be set to the Windows-like "Read-only".
            Files.setAttribute(Siedler3_01Path.toPath(), "dos:readonly", false);
            file = new RandomAccessFile(Siedler3_01Path, "rw");

            int offsetMapRead = 0x14;
            file.seek(offsetMapRead);
            
            file.writeByte(locale);

            if (file != null) {
            	try {
    				file.close();
    			} catch (IOException e) {
    				Test.outputException(e);
    			}
            }
            return true;
        }
        catch(Exception e)
        {
            Test.outputException(e);
            if (file != null) {
            	try {
    				file.close();
    			} catch (IOException e2) {
    				Test.outputException(e2);
    			}
            }
            return false;
        }
    }

    public static int readLocale(File s3Path)
    {
        RandomAccessFile file = null;

        try
        {
            if (s3Path == null || !Files.exists(s3Path.toPath()))
            {
            	s3Path = ReqQuery.getS3ExeDirectory();
                if (s3Path == null || !Files.exists(s3Path.toPath()))
                {
                    // Test.output(ReqQuery.s3ExeFilePath);
                    return -1;
                }
            }

            File Siedler3_01Path = new File(s3Path, "SND" + File.separator + "Siedler3_01.dat"); 
            file = new RandomAccessFile(Siedler3_01Path, "r");

            int offsetMapRead = 0x14;
            file.seek(offsetMapRead);
            
            int result = file.read();

            if (file != null) {
            	try {
    				file.close();
    			} catch (IOException e) {
    				Test.outputException(e);
    			}
            }
            return result;
        }
        catch(Exception e)
        {
            Test.outputException(e);
            if (file != null) {
            	try {
    				file.close();
    			} catch (IOException e2) {
    				Test.outputException(e2);
    			}
            }
            return -1;
        }
    }
    
}