package net.siedler3.alobby.util;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.FlavorEvent;
import java.awt.datatransfer.FlavorListener;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.util.Calendar;
import java.util.TimeZone;

import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.nativesupport.NativeFunctions;

/**
 * When other program takes ownership of the clipboard wait 250 ms and take back clipboard's ownership with updated content.
 * Through this function we try to prevent cheat-software which takes
 * screenshots of s3.
 * 
 * @author Zwirni
 * @source https://stackoverflow.com/questions/14226064/calling-a-method-when-content-of-clipboard-is-changed
 *
 */

public class ClipBoardListener extends Thread implements ClipboardOwner, FlavorListener {
	private Clipboard sysClip; 
	private boolean isRunning = true;
	private long lastClipBoardChangeTime = 0;
	private long lastPrintKeyPressedTime = 0;

	@Override
    public void run() {
    	sysClip = Toolkit.getDefaultToolkit().getSystemClipboard();
    	takeOwnership();
    	
    	//call this once to delete any result between the last call to isPressed and upcoming ones.
        //if someone presses space while chatting between to savepoints, this might be visible
        //in the first call to isPressed (LSB set to one)
    	NativeFunctions.getInstance().isPressed(44);
		
        boolean printPressed = false;
        
    	while (isRunning())
    	{
    		printPressed = NativeFunctions.getInstance().isPressed(44);
    		if( false != printPressed ) {
    			Calendar calendar = Calendar.getInstance();
    	    	calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
    			long keytime = calendar.getTimeInMillis();
    			this.setLastPrintKeyPressedTime(keytime);
    			printPressed = false;
    		}
    		
    		// only run every 0,01 seconds to check if keys are pressed, not between
    		try {
    			Thread.sleep(10);
    		} catch (InterruptedException e) {
    			e.printStackTrace();
    		}
    	}
    	
    }
    
    /**
     * Check for signs for cheating by comparing the 
     * the time where the print key was last used with the last screenshot-time. 
     * If the print key was pressed and the screenshot-time is set, than it is not cheating.
     * If the screenshot-time is set, but not print key was pressed it seems to be a cheater.
     */
	private void checkForCheating() {
		Test.output("checkForCheating: " + this.lastPrintKeyPressedTime + " >= " + this.lastClipBoardChangeTime);
		if( this.lastClipBoardChangeTime > 0 && this.lastPrintKeyPressedTime == 0 ) {
			Test.output("CHEATER!!!!");
		}
	}

	private synchronized boolean isRunning()
    {
        return this.isRunning;
    }

    public synchronized void setRunning(boolean isRunning)
    {
        this.isRunning = isRunning;
    }
   
    @Override
    public void lostOwnership(Clipboard c, Transferable t) {
    	if( isRunning() ) {
			checkOwnership();
			checkForCheating();
		}
    }

    private void takeOwnership() {
    	try {
		    Thread.sleep(20);
		} catch(Exception e) {
			System.out.println("Exception: " + e);
		}
    	Transferable contents = new Transferable() {
    	    public DataFlavor[] getTransferDataFlavors() {
    	      return new DataFlavor[0];
    	    }

    	    public boolean isDataFlavorSupported(DataFlavor flavor) {
    	      return false;
    	    }

    	    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException {
    	      throw new UnsupportedFlavorException(flavor);
    	    }
    	};
    	if( sysClip.getFlavorListeners().length > 0 ) {
    		sysClip.removeFlavorListener(this);
    	}
    	sysClip.setContents(contents, this); 
    	sysClip.addFlavorListener(this);
    }  
    
    /**
     * Check if the new value in clipboard is not text.
     * => if it is not text than is seems to be a screenshot => possible cheat-tool running and active.
     * 
     * @param t
     * @param c
     */
    public boolean processClipboard(Transferable t, Clipboard c) {
    	Transferable trans = t;
    	try {
    		if( trans != null ) {
    			if( trans.isDataFlavorSupported(DataFlavor.imageFlavor) ) {
	    			Calendar calendar = Calendar.getInstance();
	    	    	calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
	    	    	// save the last changed clipboard-time
	    			setLastClipBoardChange(calendar.getTimeInMillis());
	    			return true;
    			}
    		}
    	} catch (Exception e) {
    		
    	}
    	return false;
    }
    
    /**
	 * Set the last time clipboard has changed.
	 * 
	 * @param mylastChangeTime
	 */
	private void setLastClipBoardChange(long mylastChangeTime) {
		Test.output("lastclipboardchange: " + mylastChangeTime);
		lastClipBoardChangeTime = mylastChangeTime;
	}
	
	/**
	 * Set the last time print-key was pressed.
	 * 
	 * @param mylastChangeTime
	 */
	private void setLastPrintKeyPressedTime(long mylastChangeTime) {
		Test.output("lastPrintKeyPressedTime: " + mylastChangeTime);
		lastPrintKeyPressedTime  = mylastChangeTime;
	}

	@Override
	public void flavorsChanged(FlavorEvent e) {
		if( isRunning() ) {
			checkOwnership();
			checkForCheating();
		}
	}
	
	private void checkOwnership() {
		Transferable contents = sysClip.getContents(null);
		if( processClipboard(contents, sysClip) ) {
			takeOwnership();
		}
	}

}