package net.siedler3.alobby.modules.connect;

import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_REGEX_REQUIRES_AUTH_CODE_2;

import java.net.InetAddress;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import net.siedler3.alobby.communication.IrcEventListener;
import net.siedler3.alobby.communication.IrcInterface;
import net.siedler3.alobby.communication.IrcServerInfo;
import net.siedler3.alobby.communication.NickServCommand;
import net.siedler3.alobby.communication.NickServPatterns;
import net.siedler3.alobby.communication.ProprietaryReplyConstants;
import net.siedler3.alobby.controlcenter.Test;

public class IrcServerCapabilities extends IrcEventListener implements IrcServerInfo
{

    private static final Pattern PATTERN_CONFIRM = Pattern.compile("\\s+CONFIRM\\s+.*");
    private static final Pattern PATTERN_RESEND= Pattern.compile("\\s+RESEND\\s+.*");
    private static final Pattern PATTERN_RESET_PASSWORD = Pattern.compile("^\\s+RESETPASS\\s+.*$");
    private static final Pattern PATTERN_REGEX_EMAIL_SENT_2 = Pattern.compile(NickServPatterns.PATTERN_EMAIL_SENT_2);
    private static final Pattern PATTERN_DISABLEREGISTRATION = Pattern.compile("DISABLEALOBBYREG");
    private int maxTargetsForCommand = 1;
    private boolean registrationRequiresEmailAuthentication;
    private boolean isResetPasswordSupported;
	private boolean hideRegistrationInFrontend = false;

    public IrcServerCapabilities(IrcInterface ircInterface)
	{
		super(ircInterface);
        //initialise with empty listener
	}

    /* (non-Javadoc)
     * @see net.siedler3.alobby.modules.connect.IrcServerInfo#getMaxTargetsForCommand()
     */
    @Override
    public int getMaxTargetsForCommand()
    {
        return maxTargetsForCommand;
    }

    /* (non-Javadoc)
     * @see net.siedler3.alobby.modules.connect.IrcServerInfo#isRegistrationRequiresEmailAuthentication()
     */
    @Override
    public boolean isRegistrationRequiresEmailAuthentication()
    {
        return registrationRequiresEmailAuthentication;
    }

	public void startObserveServerCapabilites()
	{
		startListening();
	}

	@Override
	protected void onConnect()
	{

        //userMode invisible, dadurch erscheint der Nick nicht bei /WHO
        // wenn man nicht im selben channel ist
        ircInterface.setMode(ircInterface.getNick(), "+ix"); //$NON-NLS-1$

        //sobald verbunden, whois auf den eigenen Nick
        //um dadurch die externe IP zu ermitteln
        //zuvor die bisher gespeicherte verwerfen
        ircInterface.setDccInetAddress(null);
        ircInterface.sendRawLine("WHOIS " + ircInterface.getNick()); //$NON-NLS-1$

        checkRegistrationCapabilities();
	}

    private void checkRegistrationCapabilities()
    {
        registrationRequiresEmailAuthentication = false;
        sendNickServCommand(NickServCommand.HELP);
        sendNickServCommand(NickServCommand.ALOBBY);
    }

	private void sendNickServCommand(NickServCommand command)
	{
	    ircInterface.sendNickServCommand(command, null);
	}

	@Override
    public void onServerResponse(int code, String response)
	{
        switch (code)
        {
            case ProprietaryReplyConstants.RPL_PROTOCOL:
                //server response indicating some of the server settings
                //scan for MAXTARGETS, which is needed for the ircWhoisTask
                try
                {
                    StringTokenizer st = new StringTokenizer(response);
                    if (st.hasMoreTokens())
                    {
                        //the first element is the nick, ignore it
                        st.nextToken();
                    }
                    while(st.hasMoreTokens())
                    {
                        String element = st.nextToken();
                        if (element.startsWith("MAXTARGETS="))
                        {
                            try
                            {
                                maxTargetsForCommand  = Integer.parseInt(element.substring(11));
                            }
                            catch (NumberFormatException e)
                            {
                                maxTargetsForCommand = 1;
                            }
                        }
                    }
                }
                catch (Exception e)
                {}
                break;

            case ProprietaryReplyConstants.RPL_WHOISHOST:
            {
                if (ircInterface.getDccInetAddress() == null)
                {
                    int firstSpace = response.indexOf(' ');
                    int secondSpace = response.indexOf(' ', firstSpace + 1);
                    int lastSpace = response.lastIndexOf(' ');

                    String sourceNick = response.substring(0, firstSpace);
                    String whoisNick = response.substring(firstSpace + 1, secondSpace);
                    if (sourceNick.equals(ircInterface.getNick()) && whoisNick.equals(sourceNick) && lastSpace > 0)
                    {
                        //hinter dem letzten Space steht eine IPv4 IP
                        String ip = response.substring(lastSpace + 1);
                        if (ip.length() > 0)
                        {
                            try
                            {
                                InetAddress address = InetAddress.getByName(ip);
                                ircInterface.setDccInetAddress(address); //als DccInetAddress ablegen
                            }
                            catch (Exception e)
                            {}
                        }
                    }
                }
                break;
            }
        }
	}

    @Override
    protected void onNotice(String sourceNick, String sourceLogin, String sourceHostname, String target, String notice)
    {
        if (ircInterface.isNickServe(sourceNick, sourceLogin, sourceHostname))
        {
            checkForEmailRegistrationMode(notice);
            checkForResetPassOption(notice);
        }
    }

    /**
     * Process each single line from server-answer and check if it contains
     * specific marker to enable or disable aLobby-functions regarding registration.
     * 
     * Hint: serverside Anope needs the modules ns_alobby.cpp for some functions.
     * 
     * @param notice
     */
    private void checkForEmailRegistrationMode(String notice)
    {
    	if( PATTERN_DISABLEREGISTRATION.matcher(notice).matches() ) {
        	hideRegistrationInFrontend = true;
        }
        if (registrationRequiresEmailAuthentication == true)
        {
            return;
        }
        if (PATTERN_CONFIRM.matcher(notice).matches() ||
            PATTERN_RESEND.matcher(notice).matches())
        {
            registrationRequiresEmailAuthentication = true;
            Test.output("Server requires Email Auth"); //$NON-NLS-1$
        }
        //Eventually NickServ was offline during server start, therefore also observe
        //some typical message indicating email authenticaton
        if (PATTERN_REGEX_REQUIRES_AUTH_CODE_2.matcher(notice).matches() ||
            PATTERN_REGEX_EMAIL_SENT_2.matcher(notice).matches())
        {
            registrationRequiresEmailAuthentication = true;
            Test.output("Server requires Email Auth"); //$NON-NLS-1$
        }
    }
    
    /**
     * Process each single line to check for a marker which shows that the
     * IRC server support passwort-reset.
     * 
     * @param notice
     */
    private void checkForResetPassOption(String notice)
    {
        if (isResetPasswordSupported == true)
        {
            return;
        }
        if (PATTERN_RESET_PASSWORD.matcher(notice).matches())
        {
            isResetPasswordSupported = true;
            Test.output("Server supports RESETPASS"); //$NON-NLS-1$
        }
    }

    /**
     * Return if password-reset is supported by IRC or not.
     * 
     * @return boolean
     */
    public boolean isResetPasswordSupported()
    {
        return isResetPasswordSupported;
    }
    
    /**
     * Return if the Registration in frontend should be hidden or not.
     * 
     * @return boolean
     */
    public boolean hideRegistrationInFrontend() {
    	return this.hideRegistrationInFrontend;
    }

}
