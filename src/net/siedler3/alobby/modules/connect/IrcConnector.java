package net.siedler3.alobby.modules.connect;

import static net.siedler3.alobby.modules.base.ResultCode.ABORTED;
import static net.siedler3.alobby.modules.base.ResultCode.FAILED;
import static net.siedler3.alobby.modules.base.ResultCode.OK;
import static net.siedler3.alobby.modules.base.ResultCode.TERMINATED;
import static net.siedler3.alobby.modules.base.ResultCode.UNEXPECTED_ERROR_MESSAGE;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.MessageFormat;

import javax.swing.UIManager;

import org.jibble.pircbot.ConnectionSettings;

import net.siedler3.alobby.communication.IrcEventDispatcher;
import net.siedler3.alobby.communication.IrcInterface;
import net.siedler3.alobby.controlcenter.ALobbyTheme;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.gui.MessageDialog;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.modules.base.IrcResultProvider;
import net.siedler3.alobby.modules.base.ResultCode;
import net.siedler3.alobby.util.TimerTaskCompatibility;

public class IrcConnector extends IrcResultProvider
{
    private static final ResultCode[] supportedResultCodes = {
        OK, //connection established
        FAILED, //initial connection failed
        ABORTED, //user has closed the connection
        TERMINATED, //the server closed an existing connection unexpectedly
        UNEXPECTED_ERROR_MESSAGE
    };

	private TimerTaskCompatibility killConnectionTask;
	private boolean doDisconnect = false;

	public IrcConnector(IrcInterface ircInterface)
	{
		super(ircInterface);
	}

    @Override
    public ResultCode[] supportedResultCodes()
    {
        return supportedResultCodes;
    }

	public void doConnect(final String serverUrl, final int port)
	{
	    internalReset();
		ircInterface.setAutoNickChange(true);
		ircInterface.startIdentServer();
		startObservation();
		//connect may block, so do it in a separate thread
		SettlersLobby.executor.execute(new Runnable() {
            @Override
            public void run()
            {
                rawConnect(serverUrl, port);
            }
        });
	}

	/**
	 * if the Server does not close our connection in a specific time,
	 * the connection is closed from our side by closing the Socket.
	 */
	private void scheduleKillTask()
	{
	    cancelKillTask();
        killConnectionTask = new TimerTaskCompatibility() {
            @Override
            public void run()
            {
                ircInterface.dispose();
                //make sure that we are really disconnected,
                //even if this may block the timer-Thread
                while (ircInterface.isConnected())
                {
                    try
                    {
                        Thread.sleep(10);
                    }
                    catch (Exception e)
                    {
                    }
                }
                //dispose does not call onDisconnect, so do this now manually
                //so that all listeners (including ourselves) are informed about the disconnect
                if (ircInterface instanceof IrcEventDispatcher)
                {
                    ((IrcEventDispatcher) ircInterface).onDisconnect();
                }
                else
                {
                    Test.output("ERROR, cannot inform about Termination"); //$NON-NLS-1$
                }
            }
        };
        final int timeLimitBeforeKill = 5000;
        killConnectionTask.schedule(timeLimitBeforeKill);
	}

    private void cancelKillTask()
    {
        if (killConnectionTask != null)
	    {
	        killConnectionTask.cancel();
	    }
    }

    private void internalReset()
    {
        doDisconnect = false;
        cancelKillTask();
    }

    /**
     *
     * @return true if disconnection process started, false if the interface is already disconnected
     */
	public boolean doDisconnect()
	{
	    if (ircInterface.isConnected())
	    {
	        doDisconnect = true;
	        ircInterface.disconnect();
	        scheduleKillTask();
	        return true;
	    }
	    return false;
	}

	public void manualAbort()
	{
	    doDisconnect();
	}

	private void rawConnect(String serverUrl, int port)
	{
        try
        {
        	// connect to irc-server with help of ConnectionSettings
        	ConnectionSettings cs = new ConnectionSettings(serverUrl);
            cs.port = port;
            cs.useSSL = false;
            // if port = 9999 than use SSL to build connection to the irc-server
            if( cs.port == 9999 ) {
            	cs.useSSL = true;
            }
            ircInterface.connect(cs);
        }
        catch (IOException e)
        {
            //in case an IOException was received during connect,
            //the InputThread of the PIrcbot is not properly deleted.
            //Try to dispose the IrcInterface, hopefully this
            //helps for the next connect
            ircInterface.dispose(); //dispose does not trigger anything
            Test.outputException(e);
            //onConnectionFailed(e.toString());
            
            stopListening();
            
            String errorText = e.toString(); 
            
            // load textcolor of actually used theme as Color-Object
        	String hexcolor = SettlersLobby.getHexColorStringByColor(ALobbyTheme.getStaticTextColor());
        	
            // use MessageDialog to show hint that IRC-Server is not available
 			MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
         	messagewindow.addMessage(
         		I18n.getString("IRCCommunicator.SERVER_NOT_AVAILABLE_1"),
         		UIManager.getIcon("OptionPane.errorIcon")
         	);
         	messagewindow.addMessage(
         		"<html>" + MessageFormat.format(I18n.getString("IRCCommunicator.SERVER_NOT_AVAILABLE_2"), hexcolor) + "</html>",
         		UIManager.getIcon("OptionPane.informationIcon")
         	);
         	messagewindow.addButton(
         		UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
         		new ActionListener() {
         		    @Override
         		    public void actionPerformed(ActionEvent e) {
         		    	// close this window
    			    	messagewindow.dispose();
    			    	onConnectionFailed(errorText);
         		    }
         		}
         	);
         	messagewindow.setVisible(true);
            
        }
        catch (Exception e)
        {
            Test.outputException(e);
            onConnectionFailed(e.toString());
        }
	}

	private void onConnectionFailed(String errorMessage)
	{
	    resultFailed(errorMessage);
	}


	@Override
    protected void resultOk()
    {
	    //only notify, do not stop listening
	    notifyResultOk();
    }

    @Override
	protected void onConnect()
	{
		resultOk();
	}

	@Override
	protected void onDisconnect()
	{
	    cancelKillTask();
	    if (doDisconnect)
	    {
	        //we were the ones that triggered the disconnect
	        resultAborted();
	    }
	    else
	    {
	        //the server terminated our connection unexpectedly
	        result(ResultCode.TERMINATED);
	    }
	}

    @Override
    protected void onUnknown(String line)
    {
        if (doDisconnect == false && line.startsWith("ERROR"))
        {
            notifyResult(ResultCode.UNEXPECTED_ERROR_MESSAGE, line);
        }
    }




}
