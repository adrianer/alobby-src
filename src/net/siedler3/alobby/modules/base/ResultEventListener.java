package net.siedler3.alobby.modules.base;

import java.util.EventListener;

public interface ResultEventListener extends EventListener
{
    public void onResultEvent(ResultEvent event);
}
