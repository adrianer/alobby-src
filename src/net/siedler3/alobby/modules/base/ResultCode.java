package net.siedler3.alobby.modules.base;

public enum ResultCode
{
    //standard events
    OK,
    FAILED,
    ABORTED,
    TERMINATED,
    //specific events
    SPECIAL_AUTH_CODE_SENT,
    SPECIAL_REQUIRES_AUTH_CODE,
    UNEXPECTED_ERROR_MESSAGE,
    USER_REQUESTS_REGISTRATION,
    RESET_AUTHENTIFICATION_SUCCESS, 
    RESET_AUTHENTIFICATION_FAILED, 
    RESET_PASSWORD_FAILED, 
    RESET_PASSWORD_SUCCESS, 
    RESET_AUTHENTIFICATION_EXPIRED
}