package net.siedler3.alobby.modules.base;

import net.siedler3.alobby.communication.IrcEventListener;
import net.siedler3.alobby.communication.IrcInterface;

abstract public class IrcResultProvider extends IrcEventListener implements ResultEventProvider
{
    protected ResultEventListener listener;
    protected String errorMessage;
    protected String defaultErrorMessage;

    protected IrcResultProvider(IrcInterface ircInterface)
    {
        super(ircInterface);
        setListener(null);
    }

    @Override
    public void setListener(ResultEventListener listener)
    {
        if (listener == null)
        {
            this.listener = new ResultEventAdapter();
        }
        else
        {
            this.listener = listener;
        }
    }

    protected void startObservation()
    {
        errorMessage = null;
        startListening();
    }

    protected void resultOk()
    {
        stopListening();
        notifyResultOk();
    }

    protected void resultFailed()
    {
        stopListening();
        notifyResultFailed();
    }

    protected void resultFailed(String errorMessage)
    {
        stopListening();
        notifyResultFailed(errorMessage);
    }


    protected void resultAborted()
    {
        stopListening();
        notifyResultAborted();
    }
    
    protected void resultResetAuthentification() {
    	stopListening();
    	notifyResultResetAuthentification();
    }
    
    protected void resultResetAuthentificationFailed(String error) {
    	stopListening();
    	notifyResultResetAuthentificationFailed(error);
    }
    
    protected void resultResetAuthentificationExpired() {
    	stopListening();
    	notifyResultResetAuthentificationExpired();
    }

	protected void resultResetPasswordSuccess() {
    	stopListening();
    	notifyResultResetPasswordSuccess();
    }
    
	protected void resultResetPasswordFailed(String error) {
    	stopListening();
    	notifyResultResetPasswordFailed(error);
    }

	protected void result(ResultCode result)
    {
        stopListening();
        notifyResult(result);
    }

    protected void notifyResult(ResultCode result)
    {
        cleanupBeforeNotifyResult();
        ResultEvent.notifyResult(listener, this, result);
    }

    protected void notifyResult(ResultCode result, String errorMessage)
    {
        cleanupBeforeNotifyResult();
        ResultEvent.notifyResult(listener, this, result, errorMessage);
    }

    protected void notifyResultOk()
    {
        notifyResult(ResultCode.OK);
    }

    protected void notifyResultFailed(String errorMessage)
    {
        this.errorMessage = errorMessage;
        notifyResultFailed();
    }

    protected void notifyResultFailed()
    {
        if (errorMessage == null)
        {
            errorMessage = defaultErrorMessage;
        }
        notifyResult(ResultCode.FAILED, errorMessage);
        errorMessage = null;
    }

    protected void notifyResultAborted()
    {
        notifyResult(ResultCode.ABORTED);
    }
    

    private void notifyResultResetAuthentification() {
		notifyResult(ResultCode.RESET_AUTHENTIFICATION_SUCCESS);
	}
    
    private void notifyResultResetAuthentificationFailed(String error) {
		notifyResult(ResultCode.RESET_AUTHENTIFICATION_FAILED, error);
	}

    private void notifyResultResetAuthentificationExpired() {
		notifyResult(ResultCode.RESET_AUTHENTIFICATION_EXPIRED);
	}
    
    private void notifyResultResetPasswordSuccess() {
		notifyResult(ResultCode.RESET_PASSWORD_SUCCESS);
		
	}

	private void notifyResultResetPasswordFailed(String error) {
		notifyResult(ResultCode.RESET_PASSWORD_FAILED, error);
	}

    /**
     * this is a callback function.
     * It is called just before notifying the listener of a result.
     */
    protected void cleanupBeforeNotifyResult()
    {

    }

}
