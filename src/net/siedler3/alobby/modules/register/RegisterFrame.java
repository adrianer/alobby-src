package net.siedler3.alobby.modules.register;

import static net.siedler3.alobby.modules.base.ResultCode.ABORTED;
import static net.siedler3.alobby.modules.base.ResultCode.FAILED;
import static net.siedler3.alobby.modules.base.ResultCode.OK;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.MessageFormat;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import net.siedler3.alobby.communication.IrcEventDispatcher;
import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.Configuration;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.gui.GUI;
import net.siedler3.alobby.gui.LinkLabel;
import net.siedler3.alobby.gui.additional.AlphaNumericAndUnderlinePlainDocument;
import net.siedler3.alobby.gui.additional.CapsLockFocusListener;
import net.siedler3.alobby.gui.additional.addCursorChangesToButton;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.modules.base.ResultCode;
import net.siedler3.alobby.modules.base.ResultEvent;
import net.siedler3.alobby.modules.base.ResultEventAdapter;
import net.siedler3.alobby.modules.base.ResultEventListener;
import net.siedler3.alobby.modules.base.ResultEventProvider;

public class RegisterFrame extends JFrame implements ResultEventListener, ResultEventProvider
{
    private static final ResultCode[] supportedResultCodes = {
        OK,
        FAILED,
        ABORTED
    };

	private final GUI gui;
	private SettlersLobby settlersLobby;
	private final Configuration config = Configuration.getInstance();
	private IrcUserRegistrator ircUserRegistrator;

    private final JButton btnBackToLogin = new JButton(I18n.getString("GUI.BACK_TO_LOGIN")); //$NON-NLS-1$
    private final JButton btnRegister = new JButton(I18n.getString("GUI.REGISTER")); //$NON-NLS-1$
    private final JButton btnFromRegToSettings = new JButton(I18n.getString("GUI.SETUP")); //$NON-NLS-1$
    private final JTextField inputRegisterMail = new JTextField();
    private final JPasswordField inputRegisterPassword = new JPasswordField();
    private final JPasswordField inputRegisterRepeatPassword = new JPasswordField();
    private final JTextField inputRegisterUser = new JTextField();
    private final JLabel lblRegisterMail = new JLabel(I18n.getString("GUI.EMAIL") + ":"); //$NON-NLS-1$ //$NON-NLS-2$
    private final JLabel lblRegisterPassword = new JLabel(I18n.getString("GUI.PASSWORD") + ":"); //$NON-NLS-1$ //$NON-NLS-2$
    private final JLabel lblRegisterRepeatPassword = new JLabel(
            I18n.getString("GUI.RETYPE_PASSWORD") + ":"); //$NON-NLS-1$  //$NON-NLS-2$
    private final JLabel lblRegisterUser = new JLabel(I18n.getString("GUI.USERNAME") + ":"); //$NON-NLS-1$ //$NON-NLS-2$
    private final JPanel pnlRegister = new JPanel();
    private final JCheckBox chkPrivacyPolicy = new JCheckBox(I18n.getString("GUI.CONSENT"));

	private ResultEventListener listener;

    private String userName = "";
    private String password = "";
    private String passwordRepeated = "";
    private String email = "";

	public RegisterFrame(GUI gui, SettlersLobby settlersLobby)
	{
	    super(ALobbyConstants.PROGRAM_NAME + " > " + I18n.getString("GUI.REGISTER"));
		this.gui = gui;
		this.settlersLobby = settlersLobby;
		registerListeners();
		buildGuiElements();
		createNewIrcUserRegistrator();
		setListener(null);
		// save window-position on specific screen on closing
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent evt)
			{
				settlersLobby.config.setScreen(evt.getComponent().getGraphicsConfiguration().getDevice().getIDstring());
			}			
		});
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}

    @Override
    public void setListener(ResultEventListener listener)
    {
        if (listener == null)
        {
            this.listener = new ResultEventAdapter();
        }
        else
        {
            this.listener = listener;
        }
    }

    @Override
    public ResultCode[] supportedResultCodes()
    {
        return supportedResultCodes;
    }


	private void registerListeners()
	{
        // Beim Schließen des Fensters
        addWindowListener(gui.windowAdapterToCloseWindow);

        // Nach Klicken auf "Registrieren", sende Inhalte der Eingabefelder
        // weiter an SettlersLobby.actionWantsToEditSettings()
        btnRegister.addActionListener(checkValuesAndRegister);
        btnRegister.addMouseListener(new addCursorChangesToButton());
        inputRegisterMail.addActionListener(checkValuesAndRegister);
        inputRegisterPassword.addActionListener(checkValuesAndRegister);
        inputRegisterRepeatPassword
                .addActionListener(checkValuesAndRegister);
        inputRegisterUser.addActionListener(checkValuesAndRegister);

        // Beim Klicken auf "Zurück zum Login" rufe
        // settlersLobby.actionWantsToLogin() auf
        btnBackToLogin.addActionListener(actionListenerToOpenLogin);
        btnBackToLogin.addMouseListener(new addCursorChangesToButton());

        // Beim Klicken auf "Einstellungen" rufe
        // settlersLobby.actionWantsToEditSettings() auf
        btnFromRegToSettings.addActionListener(gui.actionListenerToOpenSettings);
        btnFromRegToSettings.addMouseListener(new addCursorChangesToButton());

	}

	private final ActionListener checkValuesAndRegister = new ActionListener()
	{
		@Override
		public void actionPerformed(ActionEvent evt)
		{
			if (valuesAreValid())
			{
			    doRegister();
			}
			else
			{
				//error already shown in valuesAreValid()
			}
		}
	};

    public final ActionListener actionListenerToOpenLogin = new ActionListener()
    {
        @Override
        public void actionPerformed(ActionEvent evt)
        {
            onRegistrationAbortedByUser();
        }
    };

	private boolean valuesAreValid()
	{
	    readInternalValuesFromInputFields();

        if (userName.isEmpty() ||
		    password.isEmpty() ||
		    passwordRepeated.isEmpty() ||
		    email.isEmpty())
		{
            gui.displayError(I18n.getString("SettlersLobby.ERROR_ALL_INPUT_FIELDS")); //$NON-NLS-1$
		    return false;
		}
        if (!userName.matches(ALobbyConstants.NICKNAME_PATTERN_STRING))
        {
            gui.displayError(I18n.getString("SettlersLobby.ERROR_WRONG_CHARACTERS")); //$NON-NLS-1$
            return false;
        }
        //password must not contain whitespaces
        if (!password.matches("\\S+")) //$NON-NLS-1$
        {
            gui.displayError(I18n.getString("SettlersLobby.ERROR_WRONG_CHARACTERS_PASSWORD")); //$NON-NLS-1$
            return false;
        }
        if (!password.equals(passwordRepeated))
        {
            gui.displayError(I18n.getString("SettlersLobby.ERROR_REPEAT_PASSWORD")); //$NON-NLS-1$
            return false;
        }
        // check the email-format via regex
        if( !email.matches(ALobbyConstants.EMAIL_VALIDATION_STRING) ) {
        	gui.displayError(I18n.getString("SettlersLobby.ERROR_EMAIL_VALIDATION"));
        	return false;
        }
        // check privacy protection-checkbox
        if( !chkPrivacyPolicy.isSelected() ) {
        	gui.displayError(I18n.getString("SettlersLobby.ERROR_PRIVACY_POLICY"));
        	return false;
        }
		return true;

	}

    private void readInternalValuesFromInputFields()
    {
        userName = inputRegisterUser.getText();
        password = new String(inputRegisterPassword.getPassword());
        passwordRepeated = new String(inputRegisterRepeatPassword.getPassword());
        email = inputRegisterMail.getText();
    }

    public void askUserForCredentials(String userName)
    {
        this.userName = userName;
        //use previously set email, but reset the shown password
        this.password = ""; //$NON-NLS-1$
        this.passwordRepeated = ""; //$NON-NLS-1$
        writeInternalValuesToInputFields();
        inputRegisterUser.requestFocus();
        gui.setNewCurrentActiveFrame(this);
    }

    private void writeInternalValuesToInputFields()
    {
        inputRegisterUser.setText(userName);
        inputRegisterMail.setText(email);
        inputRegisterPassword.setText(password);
        inputRegisterRepeatPassword.setText(passwordRepeated);
    }


	private void doRegister()
	{
	    readInternalValuesFromInputFields();

		gui.displayWait(I18n.getString("SettlersLobby.MSG_LOGIN_WAIT_2")); //$NON-NLS-1$
		ircUserRegistrator.doRegister(userName, password, email);
	}

    private void createNewIrcUserRegistrator()
    {
        ircUserRegistrator = new IrcUserRegistrator(IrcEventDispatcher.getInstance());
        ircUserRegistrator.setListener(this);
    }


    @Override
    public void onResultEvent(final ResultEvent event)
    {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run()
            {
                onResultEventEdt(event);
            }
        });
    }

    private void onResultEventEdt(ResultEvent event)
    {
        assert(SwingUtilities.isEventDispatchThread() == true);
        if (!isVisible())
        {
            //only react if we are active
            return;
        }

        switch (event.getResult())
        {
            case OK:
                doResultOk();
                break;
            case FAILED:
                hideFrame();
                gui.displayError(event.getErrorMessage());
                ResultEvent.notifyResultFailed(listener, this, event.getErrorMessage());
                break;
            case ABORTED:
                hideFrame();
                ResultEvent.notifyResultAborted(listener, this);
                break;
            case SPECIAL_AUTH_CODE_SENT:
                gui.displayInformation(I18n.getString("GUI.REGISTER"), I18n.getString("RegisterFrame.INFO_AUTH_CODE_EMAIL_SENT")); //$NON-NLS-1$ //$NON-NLS-2$
                doResultOk();
                break;
            default:
                Test.output("unexpected event"  + event); //$NON-NLS-1$
                return;
        }
    }

    private void doResultOk()
    {
        hideFrame();
        config.setUserName(userName);
        config.setPassword(password);
        ResultEvent.notifyResultOk(listener, this);
    }

    private void hideFrame()
    {
        gui.closeWait();
        setVisible(false);
    }

    private void onRegistrationAbortedByUser()
    {
        if (isVisible())
        {
            //user aborted, manually stop the Registrator
            ircUserRegistrator.manualAbort();
            hideFrame();
            ResultEvent.notifyResultAborted(listener, this);
        }
    }

    private void buildGuiElements()
	{
        gui.setIcon(this);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getContentPane().setBackground(settlersLobby.theme.backgroundColor);
        setResizable(false);

        btnRegister.setMultiClickThreshhold(500);

        // Hinweis bei CapsLock:
        inputRegisterRepeatPassword.addFocusListener(new CapsLockFocusListener(settlersLobby));
        inputRegisterPassword.addFocusListener(new CapsLockFocusListener(settlersLobby));


        pnlRegister.setBackground(settlersLobby.theme.backgroundColor);
        pnlRegister.setBorder(javax.swing.BorderFactory.createTitledBorder(
                null, " " + I18n.getString("GUI.REGISTER") + " ", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
                javax.swing.border.TitledBorder.DEFAULT_POSITION,
                new java.awt.Font("Trebuchet MS italic", 1, 17), settlersLobby.theme.getTextColor())); //$NON-NLS-1$

        inputRegisterUser.setDocument(new AlphaNumericAndUnderlinePlainDocument());
        inputRegisterUser.setBackground(settlersLobby.theme.inputBoxBackgroundColor);
        inputRegisterRepeatPassword.setBackground(settlersLobby.theme.inputBoxBackgroundColor);
        inputRegisterPassword.setBackground(settlersLobby.theme.inputBoxBackgroundColor);

        String hexcolor = SettlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor());
        
        LinkLabel hintForAlternativeRegisterWay = new LinkLabel();
        hintForAlternativeRegisterWay.setText(MessageFormat.format("<html>" + I18n.getString("GUI.HINT_FOR_REGISTRATION") + "</html>", hexcolor));
        LinkLabel hintForPrivacyPolicy = new LinkLabel();
        hintForPrivacyPolicy.setText(MessageFormat.format("<html>" + I18n.getString("GUI.HINT_FOR_PRIVACY_POLICY") + "</html>", hexcolor));

        javax.swing.GroupLayout pnlRegisterLayout = new javax.swing.GroupLayout(
                pnlRegister);
        pnlRegister.setLayout(pnlRegisterLayout);
		pnlRegisterLayout
                .setHorizontalGroup(pnlRegisterLayout
                        .createParallelGroup(
                                javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(
                                pnlRegisterLayout
                                        .createSequentialGroup()
                                        .addContainerGap()
                                        .addGroup(
                                                pnlRegisterLayout
                                                        .createParallelGroup(
                                                                javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(
                                                                lblRegisterRepeatPassword)
                                                        .addComponent(
                                                                lblRegisterMail)
                                                        .addComponent(
                                                                lblRegisterUser)
                                                        .addComponent(
                                                                lblRegisterPassword))
                                        .addGap(12, 12, 12)
                                        .addGroup(
                                                pnlRegisterLayout
                                                        .createParallelGroup(
                                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(
                                                                javax.swing.GroupLayout.Alignment.TRAILING,
                                                                pnlRegisterLayout
                                                                        .createSequentialGroup()
                                                                        .addComponent(
                                                                                btnRegister,
                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                117,
                                                                                Short.MAX_VALUE)
                                                                        .addPreferredGap(
                                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(
                                                                                btnBackToLogin,
                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                147,
                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addPreferredGap(
                                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(
                                                                                btnFromRegToSettings,
                                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                114,
                                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addPreferredGap(
                                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                                                        .addComponent(
                                                                inputRegisterPassword,
                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                390,
                                                                Short.MAX_VALUE)
                                                        .addComponent(
                                                                inputRegisterMail,
                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                390,
                                                                Short.MAX_VALUE)
                                                        .addComponent(
                                                                inputRegisterUser,
                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                390,
                                                                Short.MAX_VALUE)
                                                        .addComponent(
                                                                inputRegisterRepeatPassword,
                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                390,
                                                                Short.MAX_VALUE)
                                                        .addComponent(
                                                                chkPrivacyPolicy,
                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                390,
                                                                Short.MAX_VALUE)
                                                        .addComponent(hintForPrivacyPolicy)
                                                        .addComponent(hintForAlternativeRegisterWay))
                                        .addContainerGap()));
        pnlRegisterLayout
                .setVerticalGroup(pnlRegisterLayout
                        .createParallelGroup(
                                javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(
                                pnlRegisterLayout
                                        .createSequentialGroup()
                                        .addContainerGap()
                                        .addComponent(hintForAlternativeRegisterWay)
                                        .addGap(20, 20, 20)
                                        .addGroup(
                                                pnlRegisterLayout
                                                        .createParallelGroup(
                                                                javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(
                                                                inputRegisterUser,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(
                                                                lblRegisterUser))
                                        .addPreferredGap(
                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(
                                                pnlRegisterLayout
                                                        .createParallelGroup(
                                                                javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(
                                                                inputRegisterMail,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(
                                                                lblRegisterMail))
                                        .addPreferredGap(
                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(
                                                pnlRegisterLayout
                                                        .createParallelGroup(
                                                                javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(
                                                                inputRegisterPassword,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(
                                                                lblRegisterPassword))
                                        .addPreferredGap(
                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(
                                                pnlRegisterLayout
                                                        .createParallelGroup(
                                                                javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(
                                                                lblRegisterRepeatPassword)
                                                        .addComponent(
                                                                inputRegisterRepeatPassword,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(
                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(chkPrivacyPolicy)
                                        .addPreferredGap(
                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(hintForPrivacyPolicy)
                                        .addGap(20, 20, 20)
                                        .addGroup(
                                                pnlRegisterLayout
                                                        .createParallelGroup(
                                                                javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(
                                                                btnBackToLogin)
                                                        .addComponent(
                                                                btnRegister)
                                                        .addComponent(
                                                                btnFromRegToSettings))
                                        .addContainerGap(
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                Short.MAX_VALUE)));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(
                javax.swing.GroupLayout.Alignment.LEADING).addGroup(
                layout.createSequentialGroup().addContainerGap().addComponent(
                        pnlRegister, javax.swing.GroupLayout.DEFAULT_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(
                javax.swing.GroupLayout.Alignment.LEADING).addGroup(
                layout.createSequentialGroup().addContainerGap().addComponent(
                        pnlRegister, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE,
                        javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE,
                                Short.MAX_VALUE)));
        pack();

        setLocationRelativeTo(gui.getMainWindow());
	}
}
