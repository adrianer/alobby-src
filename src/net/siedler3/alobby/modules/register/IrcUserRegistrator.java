package net.siedler3.alobby.modules.register;

import static net.siedler3.alobby.communication.NickServCommand.REGISTER;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_EMAIL_SENT_2;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_EMAIL_SENT_3_freenode;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_INVALID_EMAIL_1;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_INVALID_EMAIL_2;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_LOGGED_IN_NICK_1_freenode;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_LOGGED_IN_NICK_2_freenode;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_NOT_LOGGED_IN_1_freenode;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_NOT_LOGGED_IN_2_freenode;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_PASSWORD_1;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_PASSWORD_2;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_REGEX_REQUIRES_AUTH_CODE_2;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_TIME_DELAY_1;
import static net.siedler3.alobby.modules.base.ResultCode.ABORTED;
import static net.siedler3.alobby.modules.base.ResultCode.FAILED;
import static net.siedler3.alobby.modules.base.ResultCode.OK;
import static net.siedler3.alobby.modules.base.ResultCode.SPECIAL_AUTH_CODE_SENT;
import net.siedler3.alobby.communication.IrcInterface;
import net.siedler3.alobby.communication.NickServPatterns;
import net.siedler3.alobby.communication.ProprietaryReplyConstants;
import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.modules.base.IrcResultProvider;
import net.siedler3.alobby.modules.base.ResultCode;
import net.siedler3.alobby.util.TimerTaskCompatibility;

import org.jibble.pircbot.ReplyConstants;

public class IrcUserRegistrator extends IrcResultProvider
{

    private static final String ERROR_MESSAGE_NICK_ALREADY_REGISTERED = "IRCCommunicator.ERROR_NICK_ALREADY_REGISTERED"; //$NON-NLS-1$
    private static final String ERROR_MESSAGE_NICK_ALREADY_IN_USE = "IRCCommunicator.ERROR_NICK_ALREADY_IN_USE"; //$NON-NLS-1$
	private static final String ERROR_MESSAGE_NICK_NOT_REGISTERED = "IRCCommunicator.ERROR_NICK_NOT_REGISTERED"; //$NON-NLS-1$
	private static final String ERROR_MESSAGE_PASSWORD_TOO_SHORT = "IRCCommunicator.ERROR_PASSWORD_TOO_SHORT"; //$NON-NLS-1$
	private static final String ERROR_MESSAGE_INVALID_EMAIL = "IRCCommunicator.ERROR_INVALID_EMAIL"; //$NON-NLS-1$;
	private static final String ERROR_MESSAGE_NICK_ALREADY_REGISTERED_AUTH_CODE_PENDING =
	                "IRCCommunicator.ERROR_NICK_ALREADY_REGISTERED_AUTH_CODE_PENDING"; //$NON-NLS-1$
	private static final String ERROR_MESSAGE_SERVICE_DOWN = "IRCCommunicator.ERROR_SERVICE_DOWN"; //$NON-NLS-1$;

    private static final ResultCode[] supportedResultCodes = {
        OK,
        FAILED,
        ABORTED,
        SPECIAL_AUTH_CODE_SENT
    };


	private String password;
	private String initialNick;
	private String requiredNick;
    private String email;

    private int retryCounter;
    private TimerTaskCompatibility task;
    private State state = new State();

    private static class State
    {
        private boolean registrationCommandSent;
        private boolean requiresAuthCode;
        private boolean emailSent;
        private boolean skipNextStatusReplyEvaluation;

        private State()
        {
            reset();
        }

        void reset()
        {
            registrationCommandSent = false;
            requiresAuthCode = false;
            emailSent = false;
            skipNextStatusReplyEvaluation = false;

        }
    }

	public IrcUserRegistrator(IrcInterface ircInterface)
	{
		super(ircInterface);
		defaultErrorMessage = I18n.getString(ERROR_MESSAGE_NICK_NOT_REGISTERED);
	}

    @Override
    public ResultCode[] supportedResultCodes()
    {
        return supportedResultCodes;
    }

	public void manualAbort()
	{
	    stopListening();
	    if (task != null)
	    {
	        task.cancel();
	    }
	    //do not inform the listener
	}

	private void resultAuthCodeSent()
    {
        result(SPECIAL_AUTH_CODE_SENT);
    }


    /**
     *
     * @param nick
     * @param password
     * @param email
     * @return true if the registration process started, false otherwise
     */
	public void doRegister(String nick, String password, String email)
	{
        if (!ircInterface.isConnected())
        {
            resultAborted();
            return;
        }
	    super.startObservation();
	    this.retryCounter = 10;
	    this.requiredNick = nick;
	    this.initialNick = ircInterface.getNick();
		this.password = password;
		this.email = email;
		this.errorMessage = null;
		state.reset();
		if (!initialNick.equals(nick))
		{
		    //first try to get the desired nick
		    //if this succeeds, the registerProcedure
		    //will be started afterwards
		    //see onNickChange()
		    ircInterface.changeNick(nick);
		}
		else
		{
            startRegisterProcedure();
		}
	}

	private void startRegisterProcedure()
    {
        //by querying the status, transmitting of password will be triggered
        queryNickStatus();
    }


	@Override
    protected void onDisconnect()
    {
	    resultAborted();
    }

    @Override
    protected void onNickChange(String oldNick, String login, String hostname, String newNick)
    {
	    if (oldNick.equals(initialNick) && newNick.equals(requiredNick))
	    {
	        initialNick = requiredNick; //react only once to the nick change
	        ircInterface.setNick(requiredNick); //TODO is this really necessary? the "login" entry in IRC cannot be changed anymore
	        //the user has now the required nick, start IRC login procedure
	        startRegisterProcedure();
	    }
    }

    @Override
	protected void onNotice(String sourceNick, String sourceLogin, String sourceHostname,
			String target, String notice)
	{
	    if (ircInterface.isNickServe(sourceNick, sourceLogin, sourceHostname))
	    {
	        Test.output("!!IrcUserRegistrator notice: " + notice);
	        handleEmailSent(notice);
	        handleRequiresAuthCode(notice);
	        handlePasswordTooShortMessage(notice);
	        handleInvalidEmailMessage(notice);
	        handleTimeDelayMessage(notice);
	        String[] splittedNotice = notice.split(" "); //$NON-NLS-1$
            handleNickServStatusReply(target, splittedNotice);
	    }
	}

    private void handleEmailSent(String notice)
    {
        //Ein Password wurde zu a@b.com gesendet, bitte tippe /msg NickServ confirm <passcode> um die Registrierung abzuschließen.
        if (notice.contains(PATTERN_EMAIL_SENT_2) || notice.contains(PATTERN_EMAIL_SENT_3_freenode))
        {
            state.requiresAuthCode = true;
            state.emailSent = true;
            Test.output("stateRequiresAuthCode: " + state.requiresAuthCode); //$NON-NLS-1$
        }
    }

    private void handleRequiresAuthCode(String notice)
    {
        if (PATTERN_REGEX_REQUIRES_AUTH_CODE_2.matcher(notice).matches())
        {
            state.requiresAuthCode = true;
            Test.output("stateRequiresAuthCode: " + state.requiresAuthCode); //$NON-NLS-1$
        }
    }

    private void handlePasswordTooShortMessage(String notice)
    {
        // Liest aus, ob das Passwort zu kurz ist:
        // NOTICE test :Versuchen Sie es erneut mit einem ungewöhnlicheren
        // Passwort. Passwörter sollten
        // mindestens fünf Zeichen lang sein und nicht zu leicht zu erraten
        // sein.

        if (notice.contains(PATTERN_PASSWORD_1) || notice.contains(PATTERN_PASSWORD_2))
        {
            errorMessage = I18n.getString(ERROR_MESSAGE_PASSWORD_TOO_SHORT);
        }
    }

    private void handleInvalidEmailMessage(String notice)
    {
        //
        // NickServ!Services@IRC-Mania.net NOTICE nick :E-Mail-Adressen müssen der Form Benutzer@Domain sein. Es dürfen keine Kontrollzeichen (Farbwechsel, etc) und keine der folgenden Zeichen benutzt werden: , : ; | \ " ( ) < > [ ]
        //
        if (notice.contains(PATTERN_INVALID_EMAIL_1) || notice.contains(PATTERN_INVALID_EMAIL_2))
        {
            errorMessage = I18n.getString(ERROR_MESSAGE_INVALID_EMAIL);
        }
    }

    private void handleTimeDelayMessage(String notice)
    {
        //
        // NickServ!Services@IRC-Mania.net NOTICE nick :Bitte 29 Sekunden vor der Benutzung des REGISTER-Befehls warten.
        //
        if (notice.contains(PATTERN_TIME_DELAY_1))
        {
            //each transmitRegistration is followed
            //by a Status query
            //as the transmit was to early, the
            //following status evaluation would return an error
            //so we skip (the command has already been sent to the
            //server, so we cannot skip the sending)
            state.skipNextStatusReplyEvaluation = true;
            transmitRegistrationCommandDelayed();
        }
    }


    private void handleNickServStatusReply(String target, String[] splittedNotice)
    {
        if (isNickServStatusReply(splittedNotice))
        {
            if (state.skipNextStatusReplyEvaluation)
            {
                //workaround for delayed sending, see handleTimeDelayMessage()
                state.skipNextStatusReplyEvaluation = false;
                return;
            }

            try
            {
                int status = Integer.parseInt(splittedNotice[2]);
                if (state.registrationCommandSent == true)
                {
                    switch (status)
                    {
                        case 0:
                            if (state.requiresAuthCode == true)
                            {
                                if (state.emailSent == true)
                                {
                                    //email was sent, auth-code handling is done
                                    //in IrcUserAuthenticator
                                    resultAuthCodeSent();
                                }
                                else
                                {
                                    //an email should have been sent, but this was not recognised or
                                    //there was an error.
                                    if (errorMessage != null)
                                    {
                                        resultFailed();
                                    }
                                    else
                                    {
                                        //No error found, continue as if the registration was successful
                                        resultOk();
                                    }
                                }
                            }
                            else
                            {
                                //nick still not registered
                                resultFailed();
                            }
                            break;
                        case 3:
                        resultOk();
                            break;
                        default:
                            Test.output("Error, unknown status " + status); //$NON-NLS-1$
                    }
                }
                else
                {
                    switch (status)
                    {
                        case 0:
                            if (state.requiresAuthCode == true)
                            {
                                //email has been sent before
                                resultFailed(I18n.getString(ERROR_MESSAGE_NICK_ALREADY_REGISTERED_AUTH_CODE_PENDING));
                            }
                            else
                            {
                                //nick not registered
                                transmitRegistrationCommand();
                            }
                            break;
                        case 1:
                        case 2:
                        case 3:
                        resultFailed(I18n.getString(ERROR_MESSAGE_NICK_ALREADY_REGISTERED));
                            break;
                        default:
                            Test.output("Error, unknown status " + status); //$NON-NLS-1$
                    }
                }
            }
            catch (NumberFormatException e) {
                Test.outputException(e);
            }
        } else {
            String notice = String.join(" ", splittedNotice);
            if (notice.startsWith(PATTERN_NOT_LOGGED_IN_1_freenode) || notice.startsWith(PATTERN_NOT_LOGGED_IN_2_freenode)) {
                if (state.registrationCommandSent == false)
                {
                    transmitRegistrationCommand();
                }
                else
                {
                    resultFailed();
                }
            } else if (notice.startsWith(PATTERN_LOGGED_IN_NICK_1_freenode) || notice.startsWith(PATTERN_LOGGED_IN_NICK_2_freenode)) {
                resultOk();
            } else {
                Test.output("Unhandled notice: " + notice);
            }
        }
    }
    
    private boolean isNickServStatusReply(String[] splittedNotice)
    {
        return NickServPatterns.isNickServStatusReply(ircInterface.getNick(), splittedNotice);
    }


    @Override
    protected void onServerResponse(int code, String response)
    {
	    switch (code)
	    {
	        case ReplyConstants.ERR_ERRONEUSNICKNAME:
                //response has the form
                // 432    ERR_ERRONEUSNICKNAME
                //  "<nick> :Erroneous nickname"
                //on unreal irc it is in fact
                // "<nick> <erroneous nick> :Erroneous nickname: <error message>
                if (response.startsWith(ircInterface.getNick() + " "))
                {
                    resultFailed(response.substring(ircInterface.getNick().length() + 1));
                }
                else
                {
                    resultFailed(response);
                }
                break;
	        case ReplyConstants.ERR_NICKNAMEINUSE:
	            resultFailed(I18n.getString(ERROR_MESSAGE_NICK_ALREADY_IN_USE));
	            break;
	        case ProprietaryReplyConstants.ERR_SERVICESDOWN:
                //Message: Services are currently down. Please try again later
                //show error
	            resultFailed(I18n.getString(ERROR_MESSAGE_SERVICE_DOWN));
                break;
	    }
    }

	/**
	 * sendet das Passwort an den Irc-Server, bzw. genauer an den NickServ. Das
	 * Passwort erscheint nicht im logfile. Sollte das Passwort nicht gesetzt
	 * sein, passiert gar nichts.
	 */
	private void transmitRegistrationCommand()
	{
		if (password != null && !password.isEmpty() && email != null)
		{
			ircInterface.setVerbose(false);
			Test.output("sending register information");
			ircInterface.registerNickToServer(password, email);
			ircInterface.setVerbose(ALobbyConstants.IRC_DEBUG_ON);
			state.registrationCommandSent = true;
			queryNickStatus();
		}
	}

    private void queryNickStatus()
    {
        if (ircInterface.getIrcServerInfo().isRegistrationRequiresEmailAuthentication())
        {
            REGISTER.transmitUsing(ircInterface, null);
        }
        ircInterface.queryNickStatus();
    }

    private void transmitRegistrationCommandDelayed()
    {
        retryCounter--;
        if (retryCounter > 0)
        {
            state.reset();
            task = new TimerTaskCompatibility() {
                @Override
                public void run()
                {
                    transmitRegistrationCommand();
                }
            };
            task.schedule(5000);
        }
        else
        {
            queryNickStatus();
        }
    }

    void testOnly_setErrorMessage(String errorMessage)
    {
        this.errorMessage = errorMessage;
    }

    void testOnly_notifyRegistrationFailed()
    {
        resultFailed();
    }

    void testOnly_notifyRegistrationFailed(String errorMessage)
    {
        resultFailed(errorMessage);
    }

    void testOnly_notifyRegistrationSuccessful()
    {
        resultOk();
    }

    void testOnly_notifyRegistrationSuccessfulAuthCodeSent()
    {
        resultAuthCodeSent();
    }

}
