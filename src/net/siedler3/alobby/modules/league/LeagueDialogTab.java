package net.siedler3.alobby.modules.league;

import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.gui.MessageDialog;
import net.siedler3.alobby.gui.OpenNewGameDialogTableModel;
import net.siedler3.alobby.gui.additional.addCursorChangesToButton;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.util.httpRequests;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import org.json.JSONArray;
import org.json.JSONObject;

public class LeagueDialogTab extends JPanel {
	
	private JFrame parentFrame;
	private JButton btnReload;
	private JButton btnClose;
	protected JTable listing;
	protected String sourceURL;
	private JPopupMenu tableGamesPopup;
	private List<ListEntryElement> listentries;

	private static class ListEntryElement
	{
		public String nickname;

		ListEntryElement(String nickname)
		{
			super();
			this.nickname = nickname;
		}
	}
	
	// initialize the object
	public LeagueDialogTab( SettlersLobby settlersLobby, LeagueTournamentDialog myLeagueTournamentDialog ) {
		/** 
		 * This dialog-window was created with Windows Builder.
		 * After install Window Builder right-click on this file 
		 * and choose Open With > Window Builder to edit this settings.
		 */
		
		// set the size for the form
		setSize(new Dimension(530, 360));
		setPreferredSize(new Dimension(640, 400));
		setMinimumSize(new Dimension(530, 360));
		setLocale(Locale.GERMANY);
		setAutoscrolls(true);
		setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		
		// set properties for this panel
		this.setAlignmentX(LEFT_ALIGNMENT);
		this.setAlignmentY(TOP_ALIGNMENT);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		
		// create the reload-button
		btnReload = new JButton(I18n.getString("LeagueDialogTab.BUTTON_RELOAD"));
		btnReload.addMouseListener(new addCursorChangesToButton());
		
		// create the close-Button
		btnClose = new JButton(I18n.getString("LeagueDialogTab.BUTTON_CLOSE"));
		btnClose.addMouseListener(new addCursorChangesToButton());
		
		// add repeating backgroundImage to the buttons, if defined for actual theme
		if( settlersLobby.theme.isRepeatingButtonStyleAvailable() ) {
			btnReload = settlersLobby.theme.getButtonWithStyle(btnReload.getText());
			btnClose = settlersLobby.theme.getButtonWithStyle(btnClose.getText());
		}
		
		// add specific backgroundImage to the buttons, if defined for actual theme
		// -> all together for consistent view
		if( settlersLobby.theme.isButtonStyleAvailable() ) {
			btnReload = settlersLobby.theme.getButtonWithStyle(btnReload.getText(), settlersLobby.theme.buttonBackgroundImage1, settlersLobby.theme.buttonBackgroundImage1Hover);
			btnClose = settlersLobby.theme.getButtonWithStyle(btnClose.getText(), settlersLobby.theme.buttonBackgroundImage1, settlersLobby.theme.buttonBackgroundImage1Hover);
		}
		
		// add ActionListener for reload-Button
		btnReload.addActionListener(actionListenerToReloadLeagueList);
		
		// add ActionListener to close this dialog
		btnClose.addActionListener(actionListenerToCloseDialog);
		
		// create the button-panel-layout
		GroupLayout gl_buttonPanel = new GroupLayout(buttonPanel);
		gl_buttonPanel.setHorizontalGroup(
			gl_buttonPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_buttonPanel.createSequentialGroup()
					.addComponent(btnReload, GroupLayout.DEFAULT_SIZE, 306, Short.MAX_VALUE)
					.addGap(18)
					.addComponent(btnClose, GroupLayout.PREFERRED_SIZE, 306, GroupLayout.PREFERRED_SIZE))
		);
		gl_buttonPanel.setVerticalGroup(
			gl_buttonPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_buttonPanel.createSequentialGroup()
					.addGroup(gl_buttonPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnReload, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(btnClose))
					.addContainerGap())
		);
		buttonPanel.setLayout(gl_buttonPanel);
		
		// generate panel for the league-table
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setName("");
		scrollPane.setAlignmentY(Component.TOP_ALIGNMENT);
		scrollPane.setAlignmentX(Component.LEFT_ALIGNMENT);
		scrollPane.setInheritsPopupMenu(true);
		scrollPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		
		// generate the league-table itself including headers
		listing = new JTable();
		listing.setName("listingTable");
		listing.setAutoCreateRowSorter(true);
		listing.getTableHeader().setReorderingAllowed(false);
		listing.getTableHeader().setResizingAllowed(false);
		listing.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listing.setModel(new OpenNewGameDialogTableModel(
			new Object[][] {
			},
			new String[] {
				I18n.getString("LeagueDialogTab.TAB_COL_POSITION"),
				I18n.getString("LeagueDialogTab.TAB_COL_PLAYER"),
				I18n.getString("LeagueDialogTab.TAB_COL_POINTS"),
				I18n.getString("LeagueDialogTab.TAB_COL_WON"),
				I18n.getString("LeagueDialogTab.TAB_COL_GAMECOUNT")
			}
		));
		listing.getColumnModel().getColumn(0).setResizable(false);
		listing.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		
		// set table unsortable
		TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(listing.getModel()) {
	        @Override
	        public boolean isSortable(int column) {
	        	return false;
	        };
	    };
	    listing.setRowSorter(sorter);

		scrollPane.setViewportView(listing);
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(scrollPane, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 640, Short.MAX_VALUE)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(10)
					.addComponent(buttonPanel, GroupLayout.DEFAULT_SIZE, 620, Short.MAX_VALUE)
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 343, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(buttonPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(12))
		);
		setLayout(groupLayout);
		
		// style the table
		DefaultTableCellRenderer headerRendererLeft = new DefaultTableCellRenderer();
		headerRendererLeft.setHorizontalAlignment( JLabel.LEFT );
    	
    	DefaultTableCellRenderer headerRendererCenter = new DefaultTableCellRenderer();
    	headerRendererCenter.setHorizontalAlignment( JLabel.CENTER );
    	
    	DefaultTableCellRenderer headerRendererRight = new DefaultTableCellRenderer();
    	headerRendererRight.setHorizontalAlignment( JLabel.RIGHT );
    	
    	// set alignment for each column
		listing.getColumnModel().getColumn(0).setCellRenderer(headerRendererLeft);
		listing.getColumnModel().getColumn(1).setCellRenderer(headerRendererCenter);
		listing.getColumnModel().getColumn(2).setCellRenderer(headerRendererRight);
		listing.getColumnModel().getColumn(3).setCellRenderer(headerRendererRight);
		listing.getColumnModel().getColumn(4).setCellRenderer(headerRendererRight);
		
		// define each column width
		listing.getColumnModel().getColumn(0).setPreferredWidth(25);
		listing.getColumnModel().getColumn(1).setPreferredWidth(150);
		listing.getColumnModel().getColumn(2).setPreferredWidth(50);
		listing.getColumnModel().getColumn(3).setPreferredWidth(50);
		listing.getColumnModel().getColumn(4).setPreferredWidth(50);
		
		// disable the lines
		listing.setShowGrid(false);
		
		// add theme-specific properties
		if (settlersLobby.config.isClassicV1Theme())
		{
		    //classic V1 theme keeps the default table header renderer
		}
		else
		{
		    //for the new themes the table header should look identical to the cells
			listing.getTableHeader().setDefaultRenderer(headerRendererCenter);
		    // add border-bottom to table-header
			listing.getTableHeader().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, settlersLobby.theme.lobbyBackgroundColor));
		    // add background-Color which will only visible behind the tableheader
		    if( settlersLobby.theme.tableheadercolor != null && settlersLobby.theme.tableheadercolor.toString().length() > 0 ) {
		    	
		    	DefaultTableCellRenderer headerRendererCenterColored = new DefaultTableCellRenderer();
		    	headerRendererCenterColored.setBackground(settlersLobby.theme.tableheadercolor);
		    	headerRendererCenterColored.setHorizontalAlignment( JLabel.CENTER );
		    	
		    	for (int i = 0; i < listing.getModel().getColumnCount(); i++) {
		    		listing.getColumnModel().getColumn(i).setHeaderRenderer(headerRendererCenterColored);
			    }
		    }
		}
		
		// show player-info-options on right-click
        listing.addMouseListener(mouseAdapterToGetPlayerInfo);
		
		listentries = new Vector<ListEntryElement>();
		
		// adding popup-menu to get more options for each entry
		tableGamesPopup = new JPopupMenu();
		
		// add a menu-entry which will open the preview-image of the selected map
	    // if the selected map is not a random-map or save-game
	    JMenuItem menuItemGetProfil = new JMenuItem("Profil aufrufen");  //$NON-NLS-1$
	    menuItemGetProfil.addActionListener(new ActionListener() {
            @SuppressWarnings("static-access")
			@Override
            public void actionPerformed(ActionEvent e)
            {
                // when the popup was activated, the selected row was also adjusted.
                int selectedRow = listing.getSelectedRow();
                if (selectedRow != -1)
                {
                	String nickname = listentries.get(selectedRow).nickname;
                	if( nickname.length() > 0 ) {
                		String link;
						try {
							link = settlersLobby.config.getLeagueUrl() + "s3player/show/" + URLEncoder.encode(nickname, "UTF-8");
							settlersLobby.getGUI().openURL(link);
						} catch (UnsupportedEncodingException e1) {
							e1.printStackTrace();
						}
                	}
                }
            }
        });
	    menuItemGetProfil.setVisible(true);
	    tableGamesPopup.add(menuItemGetProfil);
		
	}
	
	/**
	 * Get the content for the table-listing depending on source-url.
	 * 
	 * Only if the table is empty.
	 */
	public void getListing() {
		if( this.sourceURL.length() > 0 && listing.getRowCount() == 0 ) {
			// get the content for this table from league-server
			httpRequests httpRequests = new httpRequests();
			httpRequests.setUrl(this.sourceURL);
			httpRequests.addParameter("getdata", 1);
			// no token-security for this request
			httpRequests.setIgnorePhpSessId(true);
			String response = "";
			// -> get the response-String
			try {
				response = httpRequests.doRequest();
			} catch (IOException e1) {
			}
			if( !httpRequests.isError() && response.length() > 0 ) {
				JSONObject obj = new JSONObject(response);
				JSONArray listArray = obj.getJSONArray("liste");
				DefaultTableModel model = (DefaultTableModel) listing.getModel();
				if( listArray.length() > 0 ) {
					// go through the list and add each entry to the table for this tab
					for (int i = 0; i < listArray.length(); i++) {
						// get the position
						String position = "";
						if( !listArray.getJSONObject(i).isNull("position") ) {
							position = listArray.getJSONObject(i).get("position").toString();
						}
						
						// get the nickname
						String player = "";
						if( !listArray.getJSONObject(i).isNull("nickname") ) {
							player = listArray.getJSONObject(i).get("nickname").toString();
						}
						
						// get the points
						String points = "";
						if( !listArray.getJSONObject(i).isNull("points") ) {
							points = listArray.getJSONObject(i).get("points").toString();
						}
						
						// get the gamecount
						String gamecount = "";
						if( !listArray.getJSONObject(i).isNull("gamecount") ) {
							gamecount = listArray.getJSONObject(i).get("gamecount").toString();
						}
						
						// get the wincount
						String wincount = "";
						if( !listArray.getJSONObject(i).isNull("wincount") ) {
							wincount = listArray.getJSONObject(i).get("wincount").toString();
						}
						
						// add this user to the table
						if( model.getColumnCount() == 5 ) {
							listentries.add(new ListEntryElement(player));
							model.addRow(new Object[]{
								position,
								player, 
								points,
								wincount,
								gamecount
							});
						}
					}
				}
			}
			else {
				Test.output("List could not be loaded #2: " + this.sourceURL);
				// show hint that the list could not be loaded
				MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
	        	// -> add message
	    		messagewindow.addMessage(
    				I18n.getString("LeagueDialogTab.ERROR_LISTLOADING"), 
	    			UIManager.getIcon("OptionPane.errorIcon")
	    		);
	    		// -> add ok-button
	    		messagewindow.addButton(
	    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
	    			new ActionListener() {
	    			    @Override
	    			    public void actionPerformed(ActionEvent e) {
	    			    	messagewindow.dispose();
	    			    }
	    			}
	    		);
	    		// show it
	    		messagewindow.setVisible(true);
			}
		}
	}
	
	/**
	 * Get the parentFrame, necessary for closing the window.
	 * 
	 * @param parentFrame
	 */
	public void setParentObject( JFrame parentFrame ) {
		this.parentFrame = parentFrame;
	}
	
	/**
	 * Close this window.
	 */
	private final Action actionListenerToCloseDialog = new AbstractAction() {
		@Override
        public void actionPerformed(java.awt.event.ActionEvent evt) {
        	parentFrame.dispose();
        }
    };
    
    /**
     * Reload table.
     */
    private final Action actionListenerToReloadLeagueList = new AbstractAction() {
		
		@Override
        public void actionPerformed(java.awt.event.ActionEvent evt) {
        	// empty the table
			DefaultTableModel dm = (DefaultTableModel) listing.getModel();
			if( listing.getRowCount() > 0 ) {
				for (int i = listing.getRowCount() - 1; i >= 0; i--) {
					dm.removeRow(i);
				}
			}
			// reload the list
			getListing();
			// generate messagewindow
        	MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
        	// -> add message
    		messagewindow.addMessage(
				I18n.getString("LeagueDialogTab.OK_LISTLOADING"),
    			UIManager.getIcon("OptionPane.informationIcon")
    		);
    		// -> add ok-button
    		messagewindow.addButton(
    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
    			new ActionListener() {
    			    @Override
    			    public void actionPerformed(ActionEvent e) {
    			    	messagewindow.dispose();
    			    }
    			}
    		);
    		// show it
    		messagewindow.setVisible(true);
        }
    };
    
    private final MouseAdapter mouseAdapterToGetPlayerInfo = new MouseAdapter() {

		//the methods below are needed only for the popup menu
		@Override
		public void mousePressed(MouseEvent e)
		{
		    showPopup(e);
		}

		@Override
		public void mouseReleased(MouseEvent e)
		{
		    showPopup(e);
		}

		private void showPopup(MouseEvent e)
		{
            if (e.isPopupTrigger())
            {
            	JTable clickedTable = listing;
                if( clickedTable != null ) {
                    Point p = e.getPoint();
                    // get the row index that contains that coordinate
                    int rowNumber = clickedTable.rowAtPoint( p );
                    // Get the ListSelectionModel of the JTable
                    ListSelectionModel model = clickedTable.getSelectionModel();
                    // set the selected interval of rows. Using the "rowNumber"
                    // variable for the beginning and end selects only that one row.
                    model.setSelectionInterval( rowNumber, rowNumber );
                }
                
                for (int i = 0; i < tableGamesPopup.getComponents().length; ++i) {
					tableGamesPopup.getComponent(i).setVisible(true);
				}
                
                // show the popup
                tableGamesPopup.show(e.getComponent(), e.getX(), e.getY());
                
            }
		}
	};
    
}