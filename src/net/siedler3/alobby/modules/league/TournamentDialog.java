package net.siedler3.alobby.modules.league;

import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Locale;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.json.JSONArray;
import org.json.JSONObject;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.gui.LinkLabel;
import net.siedler3.alobby.gui.MessageDialog;
import net.siedler3.alobby.gui.OpenNewGameDialog;
import net.siedler3.alobby.gui.OpenNewS3GameDialog;
import net.siedler3.alobby.gui.additional.addCursorChangesToButton;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.util.httpRequests;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.UIManager;
import javax.swing.JComboBox;
import java.awt.Font;

public class TournamentDialog extends JPanel {
	
	private JFrame parentFrame;
	private SettlersLobby settlersLobby;
	private LeagueTournamentDialog leagueTournamentDialog;
	private JButton btnReload;
	private JButton btnClose;
	private JPanel hintPanel;
	private JComboBox<String> cmbTournamentList;
	private String[][] tournamentlist;
	private JTextPane txtGroup;
	private JTextPane txtMatchnumber;
	private JTextPane txtLink;
	private JTextPane txtPlayerlist;
	private JPanel tournamentPanel;
    private JTextPane txtRound;
    private String tournamentid;
    private String tournamentname = "";
    private String matchnumber;
	private JLabel lblGroup;
	private String maptypes;

	// initialize the object
	public TournamentDialog( SettlersLobby settlersLobby, LeagueTournamentDialog myLeagueTournamentDialog ) {
		/** 
		 * This dialog-window was created with Windows Builder.
		 * After install Window Builder right-click on this file 
		 * and choose Open With > Window Builder to edit this settings.
		 */
		
		// set the size for the form
		setSize(new Dimension(530, 360));
		setPreferredSize(new Dimension(640, 400));
		setMinimumSize(new Dimension(530, 360));
		setLocale(Locale.GERMANY);
		setAutoscrolls(true);
		setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		
		this.settlersLobby = settlersLobby;
		this.leagueTournamentDialog = myLeagueTournamentDialog;
		
		// set properties for this panel
		this.setAlignmentX(LEFT_ALIGNMENT);
		this.setAlignmentY(TOP_ALIGNMENT);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setBounds(10, 362, 620, 34);
		buttonPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		
		// create the reload-button
		btnReload = new JButton(I18n.getString("LeagueDialogTab.BUTTON_RELOAD"));
		btnReload.addMouseListener(new addCursorChangesToButton());
		
		// create the close-Button
		btnClose = new JButton(I18n.getString("LeagueDialogTab.BUTTON_CLOSE"));
		btnClose.addMouseListener(new addCursorChangesToButton());
		
		// add repeating backgroundImage to the buttons, if defined for actual theme
		if( settlersLobby.theme.isRepeatingButtonStyleAvailable() ) {
			btnReload = settlersLobby.theme.getButtonWithStyle(btnReload.getText());
			btnClose = settlersLobby.theme.getButtonWithStyle(btnClose.getText());
		}
		
		// add specific backgroundImage to the buttons, if defined for actual theme
		// -> all together for consistent view
		if( settlersLobby.theme.isButtonStyleAvailable() ) {
			btnReload = settlersLobby.theme.getButtonWithStyle(btnReload.getText(), settlersLobby.theme.buttonBackgroundImage1, settlersLobby.theme.buttonBackgroundImage1Hover);
			btnClose = settlersLobby.theme.getButtonWithStyle(btnClose.getText(), settlersLobby.theme.buttonBackgroundImage1, settlersLobby.theme.buttonBackgroundImage1Hover);
		}
		
		// add ActionListener for reload-Button
		btnReload.addActionListener(actionListenerToReloadTournamentData);
		
		// add ActionListener to close this dialog
		btnClose.addActionListener(actionListenerToCloseDialog);
		
		// create the button-panel-layout
		GroupLayout gl_buttonPanel = new GroupLayout(buttonPanel);
		gl_buttonPanel.setHorizontalGroup(
			gl_buttonPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_buttonPanel.createSequentialGroup()
					.addComponent(btnReload, GroupLayout.DEFAULT_SIZE, 306, Short.MAX_VALUE)
					.addGap(18)
					.addComponent(btnClose, GroupLayout.PREFERRED_SIZE, 306, GroupLayout.PREFERRED_SIZE))
		);
		gl_buttonPanel.setVerticalGroup(
			gl_buttonPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_buttonPanel.createSequentialGroup()
					.addGroup(gl_buttonPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnReload, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(btnClose))
					.addContainerGap())
		);
		buttonPanel.setLayout(gl_buttonPanel);
		
		hintPanel = new JPanel();
		hintPanel.setEnabled(false);
		hintPanel.setVisible(false);
		hintPanel.setBounds(10, 11, 620, 340);
		setLayout(null);
		
		tournamentPanel = new JPanel();
		tournamentPanel.setBounds(10, 11, 620, 340);
		add(tournamentPanel);
		
		cmbTournamentList = new JComboBox<String>();
		cmbTournamentList.addActionListener(new ActionListener () {
		    public void actionPerformed(ActionEvent e) {
		        changeTournamentDataThroughComboBox();
		    }
		});
		
		txtRound = new JTextPane();
		
		JLabel lblRound = new JLabel(I18n.getString("TournamentDialog.ROUND"));
		lblRound.setFont(lblRound.getFont().deriveFont(Font.BOLD));
		
		JLabel lblMatchnumber = new JLabel(I18n.getString("TournamentDialog.MATCHNUMBER"));
		lblMatchnumber.setFont(lblMatchnumber.getFont().deriveFont(Font.BOLD));
		
		txtGroup = new JTextPane();
		
		txtMatchnumber = new JTextPane();
		
		txtLink = new JTextPane();
		
		lblGroup = new JLabel(I18n.getString("TournamentDialog.GROUPNUMBER"));
		lblGroup.setFont(lblGroup.getFont().deriveFont(Font.BOLD));
		
		JLabel lblLink = new JLabel(I18n.getString("TournamentDialog.LINK"));
		lblLink.setFont(lblLink.getFont().deriveFont(Font.BOLD));
		
		JButton btnStart = new JButton(I18n.getString("TournamentDialog.BUTTON_CHOOSE_MAP"));
		// add repeating backgroundImage to the buttons, if defined for actual theme
		if( settlersLobby.theme.isRepeatingButtonStyleAvailable() ) {
			btnStart = settlersLobby.theme.getButtonWithStyle(btnStart.getText());
		}
		// add specific backgroundImage to the buttons, if defined for actual theme
		// -> all together for consistent view
		if( settlersLobby.theme.isButtonStyleAvailable() ) {
			btnStart = settlersLobby.theme.getButtonWithStyle(btnStart.getText(), settlersLobby.theme.buttonBackgroundImage1, settlersLobby.theme.buttonBackgroundImage1Hover);
		}
		btnStart.addActionListener(actionListenerToCreateGame);
		
		JTextPane txtHintForTournament = new JTextPane();
		txtHintForTournament.setFont(txtHintForTournament.getFont().deriveFont(Font.BOLD));
		txtHintForTournament.setText(I18n.getString("TournamentDialog.LABEL_CHOOSE_GAME"));
		
		JLabel lblPlayerlist = new JLabel(I18n.getString("TournamentDialog.PLAYERLIST"));
		lblPlayerlist.setFont(lblPlayerlist.getFont().deriveFont(Font.BOLD));
		
		txtPlayerlist = new JTextPane();
		GroupLayout gl_tournamentPanel = new GroupLayout(tournamentPanel);
		gl_tournamentPanel.setHorizontalGroup(
			gl_tournamentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_tournamentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_tournamentPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_tournamentPanel.createSequentialGroup()
							.addComponent(lblPlayerlist, GroupLayout.PREFERRED_SIZE, 147, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(txtPlayerlist, GroupLayout.DEFAULT_SIZE, 270, Short.MAX_VALUE)
						)
						.addComponent(btnStart, GroupLayout.PREFERRED_SIZE, 460, GroupLayout.PREFERRED_SIZE)
						.addGroup(Alignment.TRAILING, gl_tournamentPanel.createSequentialGroup()
							.addGroup(gl_tournamentPanel.createParallelGroup(Alignment.TRAILING)
								.addComponent(txtHintForTournament, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 588, Short.MAX_VALUE)
								.addComponent(cmbTournamentList, Alignment.LEADING, 0, 588, Short.MAX_VALUE)
								.addGroup(Alignment.LEADING, gl_tournamentPanel.createSequentialGroup()
									.addGroup(gl_tournamentPanel.createParallelGroup(Alignment.LEADING)
										.addComponent(lblRound, GroupLayout.PREFERRED_SIZE, 147, GroupLayout.PREFERRED_SIZE)
										.addComponent(lblMatchnumber, GroupLayout.PREFERRED_SIZE, 147, GroupLayout.PREFERRED_SIZE)
										.addComponent(lblGroup, GroupLayout.PREFERRED_SIZE, 147, GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(gl_tournamentPanel.createParallelGroup(Alignment.LEADING)
										.addComponent(txtGroup, GroupLayout.PREFERRED_SIZE, 149, GroupLayout.PREFERRED_SIZE)
										.addComponent(txtMatchnumber, GroupLayout.PREFERRED_SIZE, 149, GroupLayout.PREFERRED_SIZE)
										.addComponent(txtRound, GroupLayout.PREFERRED_SIZE, 149, GroupLayout.PREFERRED_SIZE)))
								.addGroup(Alignment.LEADING, gl_tournamentPanel.createSequentialGroup()
									.addComponent(lblLink, GroupLayout.PREFERRED_SIZE, 147, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(txtLink, GroupLayout.PREFERRED_SIZE, 433, GroupLayout.PREFERRED_SIZE)))
							.addGap(22))))
		);
		gl_tournamentPanel.setVerticalGroup(
			gl_tournamentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_tournamentPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(txtHintForTournament, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(cmbTournamentList, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_tournamentPanel.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblLink)
						.addComponent(txtLink, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_tournamentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblRound, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
						.addComponent(txtRound, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_tournamentPanel.createParallelGroup(Alignment.LEADING)
							.addComponent(lblMatchnumber, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
							.addComponent(txtMatchnumber, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_tournamentPanel.createParallelGroup(Alignment.LEADING, false)
						.addComponent(lblGroup, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(txtGroup, GroupLayout.DEFAULT_SIZE, 21, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_tournamentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(txtPlayerlist, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblPlayerlist))
					.addComponent(btnStart)
					.addGap(18))
		);
		tournamentPanel.setLayout(gl_tournamentPanel);
		
		@SuppressWarnings("static-access")
		String hexcolor = settlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor());
		LinkLabel txtHintForNoneTournament = new LinkLabel();
		txtHintForNoneTournament.setText(MessageFormat.format("<html>" + I18n.getString("TournamentDialog.MESSAGE_NO_TOURNAMENT") + "</html>", hexcolor));
		GroupLayout gl_hintPanel = new GroupLayout(hintPanel);
		gl_hintPanel.setHorizontalGroup(
			gl_hintPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_hintPanel.createSequentialGroup()
					.addGap(56)
					.addComponent(txtHintForNoneTournament, GroupLayout.DEFAULT_SIZE, 554, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_hintPanel.setVerticalGroup(
			gl_hintPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_hintPanel.createSequentialGroup()
					.addGap(5)
					.addComponent(txtHintForNoneTournament, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(309, Short.MAX_VALUE))
		);
		hintPanel.setLayout(gl_hintPanel);
		add(hintPanel);
		add(buttonPanel);
	}

	/**
	 * Fill the text-fields with data from selected tournament.
	 */
	private void changeTournamentDataThroughComboBox() {
		if( tournamentlist.length > 0 ) { 
			// loop through the array and find the according entry for this selection
			String selectedTournamentname = cmbTournamentList.getSelectedItem().toString();
			String hexcolor = SettlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor());
			for( int t=0;t<tournamentlist.length;t++ ) {
				if( tournamentlist[t][0] != null ) {
					String tournamentnameToCheck = tournamentlist[t][7];
					if( tournamentnameToCheck.equals(selectedTournamentname) ) {
						// match => fill the fields and break loop
						// -> round
						txtRound.setText(tournamentlist[t][2]);
						// -> matchnumber
						txtMatchnumber.setText(tournamentlist[t][3]);
						// -> groupnumber
						// ---> if 0 hide it
						int matchnumber = Integer.parseInt(tournamentlist[t][4]);
						if( matchnumber == 0 ) {
							lblGroup.setVisible(false);
							txtGroup.setVisible(false);
						}
						else {
							lblGroup.setVisible(true);
							txtGroup.setVisible(true);
						}
						txtGroup.setText(tournamentlist[t][4]);
						// -> link as clickable link
						String link = tournamentlist[t][5];
						LinkLabel linklabel = new LinkLabel();
						linklabel.setText(MessageFormat.format("<html><a href=''" + link + "'' style=''color: {0}''>" + link + "</a></html>", hexcolor));
						linklabel.setBorder(BorderFactory.createCompoundBorder(linklabel.getBorder(), BorderFactory.createEmptyBorder(3, 3, 3, 3)));
						linklabel.setBounds(0, 0, 0, 0);
						linklabel.setSize(linklabel.getPreferredSize());
						linklabel.setLocation(0, 0);
						txtLink.add(linklabel);
						// -> playerlist
						txtPlayerlist.setText(tournamentlist[t][6].replace(",", ", "));
						// -> tournamentid
						this.tournamentid = tournamentlist[t][0];
						// -> tournament-name
						this.tournamentname = tournamentlist[t][1];
						// -> matchnumber
						this.matchnumber = tournamentlist[t][3];
						// -> allowed maptypes
						this.maptypes = tournamentlist[t][8];
						break;
					}
				}
			}
		}
	}

	/**
	 * Set the parentObject (the window). We need this to close the window via button in this tab.
	 * 
	 * @param parentFrame
	 */
	public void setParentObject( JFrame parentFrame ) {
		this.parentFrame = parentFrame;
	}
	
	/**
	 * Get the list of open matches for the active user.
	 */
	public void getOpenTournaments() {
		// create the url
		String sourceURL = settlersLobby.config.getTournamentPlayerList() + settlersLobby.getNick();
		// get the content for this table from league-server
		httpRequests httpRequests = new httpRequests();
		httpRequests.setUrl(sourceURL);
		httpRequests.addParameter("getdata", 1);
		// no token-security for this request
		httpRequests.setIgnorePhpSessId(true);
		String response = "";
		// -> get the response-String
		try {
			response = httpRequests.doRequest();
		} catch (IOException e1) {
		}
		// empty the arraylist
		tournamentlist = new String[0][0];
		if( !httpRequests.isError() && response.length() > 0 ) {
			JSONObject obj = new JSONObject(response);
			JSONArray listArray = obj.getJSONArray("liste");
			if( listArray.length() > 0 ) {
				// clear the combobox
				if( cmbTournamentList.getComponentCount() > 0 ) {
					cmbTournamentList.removeAllItems();
				}
				// init the arraylist
				tournamentlist = new String[listArray.length()][9];
				int count = 0;
				// go through the list and add each entry to the table for this tab
				for (int i = 0; i < listArray.length(); i++) {
					// get the tournamentid
					int tournamentid = 0;
					if( !listArray.getJSONObject(i).isNull("tournamentid") ) {
						tournamentid = listArray.getJSONObject(i).getInt("tournamentid");
					}
					// get the tournament-title
					String tournamentname = "";
					if( !listArray.getJSONObject(i).isNull("tournamentname") ) {
						tournamentname = listArray.getJSONObject(i).getString("tournamentname");
					}
					// get the round
					int round = 0;
					if( !listArray.getJSONObject(i).isNull("round") ) {
						round = listArray.getJSONObject(i).getInt("round");
					}
					// get the matchnumber
					int matchnumber = 0;
					if( !listArray.getJSONObject(i).isNull("matchnumber") ) {
						matchnumber = listArray.getJSONObject(i).getInt("matchnumber");
					}
					// get the groupnumber
					int groupnumber = 0;
					if( !listArray.getJSONObject(i).isNull("groupnumber") ) {
						groupnumber = listArray.getJSONObject(i).getInt("groupnumber");
					}
					// get the allowed maptypes
					String maptypes = "";
					if( !listArray.getJSONObject(i).isNull("maptypes") ) {
						maptypes = listArray.getJSONObject(i).getString("maptypes");
					}
					// get the link
					String link = "";
					if( !listArray.getJSONObject(i).isNull("link") ) {
						link = listArray.getJSONObject(i).getString("link");
					}
					// get the playerlist
					String playerlist = "";
					if( !listArray.getJSONObject(i).isNull("playerlist") ) {
						JSONArray playerArray = listArray.getJSONObject(i).getJSONArray("playerlist"); 
						for( int p = 0; p < playerArray.length(); p++ ) {
							if( playerlist.length() > 0 ) {
								playerlist = playerlist + ",";
							}
							playerlist = playerlist + playerArray.getJSONObject(p).getString("nickname");
						}
					}
					
					if( tournamentname.length() > 0 && tournamentid > 0 ) {
						// add this tournament with information about round, group and match to combobox
						String tournamentListName = tournamentname + " (" + I18n.getString("TournamentDialog.ROUND") + " " + round + ", " + I18n.getString("TournamentDialog.MATCHNUMBER") + " " + matchnumber + ")"; 
						cmbTournamentList.addItem(tournamentListName);
						// and add all data to an array
						tournamentlist[count][0] = Integer.toString(tournamentid);
						tournamentlist[count][1] = tournamentname;
						tournamentlist[count][2] = Integer.toString(round);
						tournamentlist[count][3] = Integer.toString(matchnumber);
						tournamentlist[count][4] = Integer.toString(groupnumber);
						tournamentlist[count][5] = link;
						tournamentlist[count][6] = playerlist;
						tournamentlist[count][7] = tournamentListName;
						tournamentlist[count][8] = maptypes;
						count++;
					}
				}
				
				if( cmbTournamentList.getComponentCount() > 0 ) {
					// hide hintPanel
					hintPanel.setVisible(false);	
					tournamentPanel.setVisible(true);
					// select the first entry and load its contents into the fields
					cmbTournamentList.setSelectedIndex(0);
					changeTournamentDataThroughComboBox();
				}
				else {
					// hide hintPanel
					hintPanel.setVisible(true);
					tournamentPanel.setVisible(false);
				}
			}
			else {
				hintPanel.setVisible(true);
				tournamentPanel.setVisible(false);
			}
		}
		else {
			// empty response => show hint to join tournaments on website
			hintPanel.setVisible(true);
			tournamentPanel.setVisible(false);
		}
	}
	
	/**
	 * Close this window.
	 */
	private final Action actionListenerToCloseDialog = new AbstractAction() {
		@Override
        public void actionPerformed(java.awt.event.ActionEvent evt) {
        	parentFrame.dispose();
        }
    };
    
    /**
     * Reload tournament-data.
     */
    private final Action actionListenerToReloadTournamentData = new AbstractAction() {
		
		@Override
        public void actionPerformed(java.awt.event.ActionEvent evt) {
			getOpenTournaments();
			// generate messagewindow
        	MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
        	// -> add message
    		messagewindow.addMessage(
				I18n.getString("TournamentDialog.MESSAGE_RELOADED"),
    			UIManager.getIcon("OptionPane.informationIcon")
    		);
    		// -> add ok-button
    		messagewindow.addButton(
    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
    			new ActionListener() {
    			    @Override
    			    public void actionPerformed(ActionEvent e) {
    			    	messagewindow.dispose();
    			    }
    			}
    		);
    		// show it
    		messagewindow.setVisible(true);
        }
    };
    
    // Open newGame-Dialog with tournament-parameters after click on "start game"
 	private final Action actionListenerToCreateGame = new AbstractAction()
 	{
 		@Override
 		public void actionPerformed(ActionEvent evt)
 		{
 			settlersLobby.getGUI().setMapChoosingDialog(createOpenNewGameDialog());
 			leagueTournamentDialog.dispose();
 			settlersLobby.actionWantsToCreateGame();
 		}
 	};
    
    /**
     * Create the OpenNewGameDialog.
     * 
     * @return OpenNewGameDialog
     */
    public OpenNewGameDialog createOpenNewGameDialog() {
    	// get the dialog
    	OpenNewGameDialog openNewGameDialog = settlersLobby.getGUI().createOpenNewGameDialog();
    	// set another title for this window
    	openNewGameDialog.setTitle(I18n.getString("TournamentDialog.TITLE"));
    	// get the available tabs
    	JTabbedPane tabListe = openNewGameDialog.getTabListe();
    	// get the S3-tab (first one)
    	OpenNewS3GameDialog openNewS3GameDialog = (OpenNewS3GameDialog) tabListe.getComponentAt(0);
    	if( openNewS3GameDialog instanceof OpenNewS3GameDialog ) {
    		tabListe.setSelectedIndex(0);
    		openNewS3GameDialog.setForTournament(
    			tournamentid, 
				tournamentname, 
				txtRound.getText(), 
				txtGroup.getText(), 
				matchnumber,
				txtPlayerlist.getText(),
				maptypes
    		);
    		openNewGameDialog.resetOnClose(true);
	    }
	    return openNewGameDialog;
    }
}