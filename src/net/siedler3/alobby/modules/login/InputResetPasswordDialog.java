package net.siedler3.alobby.modules.login;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.gui.GUI;
import net.siedler3.alobby.i18n.I18n;

public class InputResetPasswordDialog extends JDialog implements PropertyChangeListener {
	
	private GUI gui;
	private JTextField nicknameField;
	private JTextField emailField;
	private JOptionPane optionPane;
	private String nickname;
	private String email;
	
	public InputResetPasswordDialog(JFrame owner, GUI gui, SettlersLobby settlersLobby) {
		super(owner, true);
		this.gui = gui;
		
		setIconImage(gui.getIcon());
		
		nicknameField = new JTextField();
		nicknameField.setBackground(settlersLobby.theme.inputBoxBackgroundColor);
		
		emailField = new JTextField();
		emailField.setBackground(settlersLobby.theme.inputBoxBackgroundColor);
		
		String labelUsername = I18n.getString("GUI.USERNAME"); //$NON-NLS-1$
        String labelEmail = I18n.getString("GUI.EMAIL"); //$NON-NLS-1$
		Object[] components = {labelUsername, nicknameField, labelEmail, emailField};
		
		optionPane = new JOptionPane(components, JOptionPane.PLAIN_MESSAGE, JOptionPane.OK_CANCEL_OPTION, null, null,
                null);
		
		this.setContentPane(optionPane);
		
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we)
            {
                /*
                 * Instead of directly closing the window, we're going to change
                 * the JOptionPane's value property.
                 */
                optionPane.setValue(JOptionPane.CANCEL_OPTION);
            }
        });
        
        // Ensure the text field always gets the first focus.
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent ce)
            {
                nicknameField.requestFocusInWindow();
            }
        });
        
        optionPane.addPropertyChangeListener(this);
		
		pack();
	}
	
	@Override
    public void setVisible(boolean b)
    {
        if (b)
        {
            setLocationRelativeTo(getParent());
        }
        super.setVisible(b);
    }

	@Override
	public void propertyChange(PropertyChangeEvent e) {
		String prop = e.getPropertyName();
		
        if (isVisible() && (e.getSource() == optionPane)
                        && (JOptionPane.VALUE_PROPERTY.equals(prop) || JOptionPane.INPUT_VALUE_PROPERTY.equals(prop)))
        {
            Object value = optionPane.getValue();

            if (value == JOptionPane.UNINITIALIZED_VALUE)
            {
                // ignore reset
                return;
            }

            // Reset the JOptionPane's value.
            // If you don't do this, then if the user
            // presses the same button next time, no
            // property change event will be fired.
            optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

            if (value.equals(JOptionPane.OK_OPTION))
            {
            	nickname = nicknameField.getText();
            	email = emailField.getText();
            	
            	// validate the email-format via regex, which we get vom alobbyconstants
            	if( !email.isEmpty() && !email.matches(ALobbyConstants.EMAIL_VALIDATION_STRING) ) {
            		// if this is not a valid email-format, than show a hint
            		gui.displayError(I18n.getString("LoginFrame.MSG_FAILED_EMAIL_VALIDATION"));
            		emailField.requestFocusInWindow();
            	}
            	// if both fields not empty, than initialize changing
            	else if (nickname != null && !nickname.isEmpty() 
                	&& email != null && !email.isEmpty()) {
                	clearAndHide();       	
                }
            	// if name-field is empty, than show a hint
                else if( nickname != null && nickname.isEmpty() ) {
                	gui.displayError(I18n.getString("LoginFrame.MSG_NO_USERNAME"));
                	nicknameField.requestFocusInWindow();
                }
                // if email-field is empty, than show a hint
                else if( email != null && email.isEmpty() ) {
                	gui.displayError(I18n.getString("LoginFrame.MSG_NO_EMAILADDRESS"));
                	emailField.requestFocusInWindow();
                }
                else
                {
                	gui.displayError(I18n.getString("InputAuthCodeDialog.ERROR_INVALID_FORMAT"));
                	nicknameField.requestFocusInWindow();
                }
            }
            else
            { // user closed dialog or clicked cancel
            	nickname = null;
            	email = null;
                clearAndHide();
            }
        }
	}

	private void clearAndHide() {
		nicknameField.setText(null);
		emailField.setText(null);
		setVisible(false);
	}

	public String getNickname() {
		return nickname;
	}

	public String getEmail() {
		return email;
	}

	public void setNickname(String text) {
		nicknameField.setText(text);
	}
}
