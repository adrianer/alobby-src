package net.siedler3.alobby.modules.login;

import static net.siedler3.alobby.modules.base.ResultCode.ABORTED;
import static net.siedler3.alobby.modules.base.ResultCode.FAILED;
import static net.siedler3.alobby.modules.base.ResultCode.OK;
import net.siedler3.alobby.communication.IrcEventDispatcher;
import net.siedler3.alobby.communication.IrcInterface;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.gui.GUI;
import net.siedler3.alobby.modules.base.IrcResultProvider;
import net.siedler3.alobby.modules.base.ResultCode;
import net.siedler3.alobby.modules.base.ResultEvent;
import net.siedler3.alobby.modules.base.ResultEventListener;
import net.siedler3.alobby.util.TimerTaskCompatibility;

public abstract class IrcNickServAccessorBase extends IrcResultProvider implements ResultEventListener
{

    private static final ResultCode[] supportedResultCodes = {
        OK,
        FAILED,
        ABORTED,
    };

    protected IrcUserAuthenticator ircUserAuthenticator;
    private TimerTaskCompatibility timeoutTask;
    private GUI gui;
	private SettlersLobby settlersLobby;
    
    public IrcNickServAccessorBase(IrcInterface ircInterface, SettlersLobby settlersLobby, GUI gui)
    {
        super(ircInterface);
        this.gui = gui;
        this.settlersLobby = settlersLobby;
        createIrcUserAuthenticator();
    }

    @Override
    public ResultCode[] supportedResultCodes()
    {
        return supportedResultCodes;
    }

    private void createIrcUserAuthenticator()
    {
        ircUserAuthenticator = new IrcUserAuthenticator(IrcEventDispatcher.getInstance(), this.settlersLobby, this.gui);
        ircUserAuthenticator.setListener(this);
    }

    @Override
    public void onResultEvent(final ResultEvent event)
    {
        switch (event.getResult())
        {
            case OK:
                onInternalResultEventOkAction();
                break;
            case FAILED:
                resultFailed(event.getErrorMessage());
                break;
            case ABORTED:
                ResultEvent.notifySameResult(listener, this, event);
                break;
            case SPECIAL_REQUIRES_AUTH_CODE:
                resultFailed("not yet authorized");
                break;
            default:
                Test.output("unexpected event"  + event); //$NON-NLS-1$
                return;
        }

    }
    
    protected void doLogin(String nick, String password)
    {
        ircUserAuthenticator.doLogin(nick, password);
    }

    abstract protected void onInternalResultEventOkAction();
    
    protected void scheduleTimeoutTask()
    {
        cancelTimeoutTask();
        timeoutTask = new TimerTaskCompatibility() {
            @Override
            public void run()
            {
                resultFailed("unexpected timeout");  //$NON-NLS-1$
            }
        };
        final int timeLimitBeforeAbort = 10000;
        timeoutTask.schedule(timeLimitBeforeAbort);
    }
    
    protected void cancelTimeoutTask()
    {
        if (timeoutTask != null)
        {
            timeoutTask.cancel();
        }
    }

    @Override
    protected void cleanupBeforeNotifyResult()
    {
        super.cleanupBeforeNotifyResult();
        cancelTimeoutTask();
    }

}