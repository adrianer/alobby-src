package net.siedler3.alobby.i18n;
/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */


import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class I18nAwayMsg
{
    public static ResourceBundle awayMsg = ResourceBundle.getBundle(
            "net.siedler3.alobby.i18n.awayMsg", I18n.currentLocale); //$NON-NLS-1$

    /**
     * liefert den zum key gehörigen Wert aus dem Bundle
     * "awayMsg".
     * @param key
     * @return
     */
    public static String getString(String key)
    {
        try
        {
            return I18nAwayMsg.awayMsg.getString(key);
        }
        catch (MissingResourceException e)
        {
            return e.getKey();
        }
    }

    /**
     * liefert den zum key gehörigen Wert für die übergebene Locale.
     * Referenziert das Bundle "awayMsg".
     * @param key
     * @param locale
     * @return
     */
    public static String getString(String key, Locale locale)
    {
        ResourceBundle bundle = ResourceBundle.getBundle(
                "net.siedler3.alobby.i18n.awayMsg", locale); //$NON-NLS-1$
        try
        {
            return bundle.getString(key);
        }
        catch (MissingResourceException e)
        {
            return e.getKey();
        }
        
    }

}
