/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui.additional;

import java.awt.FileDialog;
import java.awt.Frame;
import java.awt.Image;

public class FileChooser
{
    //use FileDialog instead of JFileChooser due to this strange bug:
    //http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=8003395
    //http://stackoverflow.com/questions/17644390/what-causes-an-internalerror-to-be-thrown-by-sun-awt-shell-win32shellfolder2-ini
    //It seems to happen on some PCs simply by calling the Constructor
    private FileDialog fileDialog;
    private String path;

    public FileChooser()
    {
        this(null, null);
    }

    public FileChooser(Frame frame, Image icon)
    {
        fileDialog = new FileDialog(frame, null, FileDialog.LOAD);
        //neither setIconImage nor setLocationRealtiveTo seem to work on windows
        //nevertheless keep them
        fileDialog.setIconImage(icon);
        fileDialog.setLocationRelativeTo(frame);
    }

    public String getPath()
    {
    	return this.path;
    }

    public void show()
    {
        initComponents();
    }

    public void show(String filter)
    {
        fileDialog.setFile(filter);
        initComponents();
    }

    private void initComponents() {
        fileDialog.setVisible(true);

        if (fileDialog.getFile() != null)
        {
            path = fileDialog.getDirectory();
            if (!path.endsWith(System.getProperty("file.separator")))
            {
                path += System.getProperty("file.separator");
            }
            path += fileDialog.getFile();
        }
        else
        {
            path = null;
        }
    }

}
