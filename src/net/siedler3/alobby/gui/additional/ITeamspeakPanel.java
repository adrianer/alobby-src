package net.siedler3.alobby.gui.additional;

import javax.swing.JPanel;

public interface ITeamspeakPanel
{
    public void showUrl();
    public void stopLoading();
    public JPanel getPanel();
	public void startLoading();

}
