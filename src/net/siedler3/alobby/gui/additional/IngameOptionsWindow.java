package net.siedler3.alobby.gui.additional;
/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */

import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.gui.GUI;
import net.siedler3.alobby.gui.MessageDialog;
import net.siedler3.alobby.i18n.I18n;



public class IngameOptionsWindow extends IngameOptionsConfigWindow implements ItemListener
{
    private static final long serialVersionUID = 1L;

    /**
     * Listener der den eigegebenen Wert für den Autosave timer
     * in der Configuration speichert.
     *
     */
    private final class InputChangedListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            if (!stopActions)
            {
                settlersLobby.setAutoSaveTimer((Integer) comboBoxAutoSaveTimer.getSelectedItem());
                //falls das Spiel schon läuft, muß die Änderung
                //sofort in den Task übernommen werden
                settlersLobby.updateAutoSaveState();
            }
        }
    }

    /**
     * Action für die Autosave Einstellung. Die JCheckbox wird mit
     * dieser Action initialisiert.
     */
    @SuppressWarnings("serial")
    private final class AutosaveCheckboxAction extends AbstractAction
    {
        public AutosaveCheckboxAction()
        {
            super(I18n.getString("IngameOptionsWindow.AUTOSAVE_ACTION_NAME")); //$NON-NLS-1$
            //ALT+a
            putValue(MNEMONIC_KEY, KeyEvent.VK_A);
            putValue(SELECTED_KEY, getIsAutosave());
        }
        @Override
        public void actionPerformed(ActionEvent e)
        {
            if (!stopActions)
            {
                if (e.getID() == ActionEvent.ACTION_PERFORMED)
                {
                    boolean b = Boolean.TRUE.equals(getValue(Action.SELECTED_KEY));
                    settlersLobby.setAutoSave(b);
                    comboBoxAutoSaveTimer.setEnabled(b && isEnabled());
                    settlersLobby.updateAutoSaveState();
                }
            }
        }
    }

    protected boolean stopActions = false;

    public IngameOptionsWindow(GUI gui, SettlersLobby settlersLobby)
    {
        super(settlersLobby);

        comboBoxAutoSaveTimer.setEnabled(checkBoxAutosave.isSelected());
        comboBoxAutoSaveTimer.addActionListener(new InputChangedListener());

        checkBoxStopStoringTools.addItemListener(this);

        this.fontSize = settlersLobby.config.getDpiAwareFontSize();
        double dpiScaling = MessageDialog.getDpiScalingFactor();
        setFont(new Font("Tahoma", Font.PLAIN, (int) (this.fontSize * dpiScaling))); //$NON-NLS-1$
        updateFonts();
        updateView();
    }


    /**
     * Die dargestellten Werte werden an die Werte in {@code settlersLobby}
     * angepasst.
     */
    @Override
    public void updateView()
    {
        stopActions = true;
        try
        {
            super.updateView();
            boolean b = getIsAutosave();
            comboBoxAutoSaveTimer.setEnabled(b && autosaveAction.isEnabled());
        }
        finally
        {
            stopActions = false;
        }
    }

    @Override
    public void itemStateChanged(ItemEvent e)
    {
        if (!stopActions)
        {
            boolean value = e.getStateChange() == ItemEvent.SELECTED;
            if (e.getSource() == checkBoxStopStoringTools)
            {
                settlersLobby.setStopStoringTools(value);
                settlersLobby.updateStopStoringToolsState();
            }
        }
    }

    @Override
    protected AbstractAction createAutosaveAction()
    {
        return new AutosaveCheckboxAction();
    }

    @Override
    protected boolean getIsAutosave()
    {
        return settlersLobby.isAutoSave();
    }
    @Override
    protected int getAutoSaveTimer()
    {
        return settlersLobby.getAutoSaveTimer();
    }

    @Override
    protected boolean getIsStopStoringTools()
    {
        return settlersLobby.isStopStoringTools();
    }


    @Override
    public void setOpaque(boolean isOpaque)
    {
        super.setOpaque(isOpaque);
        if (checkBoxAutosave != null && checkBoxStopStoringTools != null)
        {
            pnlAutosave.setOpaque(isOpaque);
            checkBoxAutosave.setOpaque(isOpaque);
            checkBoxStopStoringTools.setOpaque(isOpaque);
        }
    }

    @Override
    public void setForeground(Color fg)
    {
        super.setForeground(fg);
        if (checkBoxAutosave != null && checkBoxStopStoringTools != null)
        {
            checkBoxAutosave.setForeground(fg);
            checkBoxStopStoringTools.setForeground(fg);
            lblAutoSave.setForeground(fg);
            lblAutoSaveTimer.setForeground(fg);
        }
    }



}
