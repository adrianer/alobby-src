package net.siedler3.alobby.gui.additional;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.MessageFormat;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.UIDefaults;
import javax.swing.UIManager;

import net.siedler3.alobby.controlcenter.OpenGame.MiscType;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.state.UserInGame;
import net.siedler3.alobby.i18n.I18n;

public class MapUserListCellRenderer extends DefaultListCellRenderer {

	private SettlersLobby settlersLobby = null;
	private boolean s4style = false;
	
	public MapUserListCellRenderer( SettlersLobby settlersLobby, boolean s4style ) {
		this.settlersLobby = settlersLobby;
	}
	
	@Override
    public Component getListCellRendererComponent (JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus)
    {
        this.setBackground(this.settlersLobby.theme.backgroundColor);
        if (value instanceof UserInGame)
        {
            UserInGame user = (UserInGame) value;
            this.setText(user.getNick());
            this.setForeground(this.settlersLobby.theme.getTextColor());
            this.setFont(this.getFont().deriveFont(this.settlersLobby.config.getDpiAwareFontSize()));
            this.setBorder(BorderFactory.createEmptyBorder(0,4,0,4));
        }
        
        if( false == s4style ) {
        	
	        UIDefaults uiDefaults = UIManager.getDefaults();
	        
	        if (isSelected)
	        {
	            Color c = (Color) uiDefaults.get("List.selectionBackground");
	            this.setBackground(c);
	        }
	        
	        list.addMouseListener(new MouseAdapter()
	        {
	            public void mousePressed(MouseEvent e)
	            {
	            	
	            	Object user = list.getModel().getElementAt(list.locationToIndex(e.getPoint()));
	            	
	            	// do nothing if the user clicked on himself
	            	if( user.toString().equals(settlersLobby.getNick()) ) {
	            		return;
	            	}
	            	
	            	// for right-click-event
	                if (SwingUtilities.isRightMouseButton(e))
					{
	                	// create a popup
						JPopupMenu popupMenu = new JPopupMenu();
						
						// create an entry to call user into game
						JMenuItem callofduty = new JMenuItem(I18n.getString("GUI.POPUP_CALL_OF_DUTY"));
						callofduty.addActionListener(new ActionListener() {
							@Override
							public void actionPerformed(ActionEvent e) {
								settlersLobby.actionChatMessage("/me " + MessageFormat.format(I18n.getString("IRCCommunicator.MSG_GAME_ACTIVITY"), user.toString()));
							}
						});						
						// add this entry to the menu
						popupMenu.add(callofduty);
						
						// create an entry to whisper to user
						JMenuItem whisperEntry = new JMenuItem(I18n.getString("PopupBuilder.ENTRY_PRIVATE_MESSAGE"));
						whisperEntry.addActionListener(new ActionListener() {
							@Override
							public void actionPerformed(ActionEvent e) {
								settlersLobby.getGUI().getChatPane().selectMessageRecipient(user.toString());
							}
						});
						// add this entry to the menu
						popupMenu.add(whisperEntry);
						
						// create an entry to kick user from game
						// if this is the host of an s4-game
						if( settlersLobby.getOpenHostGame() != null ) {
							if( settlersLobby.getOpenHostGame().getMisc() == MiscType.S4 ) {
								JMenuItem kickEntry = new JMenuItem(I18n.getString("PopupBuilder.ENTRY_KICK"));
								kickEntry.addActionListener(new ActionListener() {
									@Override
									public void actionPerformed(ActionEvent e) {
										settlersLobby.getGameState().onPart(new UserInGame(user.toString()), settlersLobby.getOpenHostGame().getMap());
									}
								});
								// add this entry to the menu
								popupMenu.add(kickEntry);
							}
						}
						
						// make popup visible
						popupMenu.show(e.getComponent(), e.getX(), e.getY());
					}
	            }
	        });
        }
        
        return this;
    }
}