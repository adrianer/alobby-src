/* This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui.additional;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.gui.GUI;
import net.siedler3.alobby.gui.MessageDialog;
import net.siedler3.alobby.gui.ShadowForJPanel;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.xmlcommunication.XMLCommunicator;
import net.siedler3.alobby.xmlcommunication.XMLFiles;

/**
 * Creates a window with an classic adventcalender
 * configured by entries in alobbys config.xml-file.
 * 
 * Usage:
 * 		JFrame adventcalendar = new adventcalendar(this);
 *
 * @author Zwirni
 */
public class AdventCalendar extends JDialog {
	
	private SettlersLobby settlersLobby;
	private JLabel lblclose;
	private ShadowForJPanel pnlAdventCalendar;
	public GregorianCalendar calendar;
	public String hexcolor;
	private boolean showit = false;

	/**
	 * Get and show the single window with adventcalendar.
	 * 
	 * @param settlersLobby
	 */
	@SuppressWarnings("static-access")
	public AdventCalendar( SettlersLobby settlersLobby ) {
				
		this.settlersLobby = settlersLobby;
		
		// get the calendar
		calendar = new GregorianCalendar();
		
		// define the dimension for the panel with the calendar
		int width = 990;
		int height = 669;
		
		// get the main text color
		hexcolor = SettlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor());
		
		// set the size and some other parameters
		setSize(new Dimension(width, height));
		setPreferredSize(new Dimension(width, height));
		setMinimumSize(new Dimension(width, height));
		setLocale(Locale.GERMANY);
		setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		setResizable(false);
		setUndecorated(true);
		setBackground(new Color(0, 255, 0, 0));
		setVisible(false);
		setAlwaysOnTop(true);
		getContentPane().setLayout(null);
		// close this window really
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		// get fontAweSome
		Font font = settlersLobby.config.getFontAweSome((float) (24 * MessageDialog.getDpiScalingFactor()));
			
		/**
		 * Create a label where the "X" for closing the layer will be placed
		 * and set width, height and paddings.
		 */
		lblclose = new JLabel();
		lblclose.setLayout(null);
		lblclose.setFont(font);
		lblclose.setText("\uf00d");
		lblclose.setBackground(new Color(0, 0, 0, 0));
		lblclose.setBorder(new EmptyBorder(0, 0, 0, 0));
		lblclose.setBounds(width - 36, 12, 44, 44);
		lblclose.setSize(new Dimension(44, 44));
		lblclose.setPreferredSize(new Dimension(44, 44));
		lblclose.setMinimumSize(new Dimension(44, 44));
		lblclose.setOpaque(true);
		// set the mouse-listener which will wait for click or mouse-moving
		lblclose.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// click the jFrame on click
				dispose();
			}
			// if mouse enters the button change the cursor and the font-color
            public void mouseEntered(java.awt.event.MouseEvent evt) {
            	// set the cursor to a hand-cursor
            	lblclose.setForeground(Color.red);
            	lblclose.setCursor(new Cursor(Cursor.HAND_CURSOR));
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
            	// set the cursor back to standard-cursor
            	lblclose.setForeground(settlersLobby.theme.getTextColor());
            	lblclose.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }
		});
		getContentPane().add(lblclose);
		
		// define and add the label for the title "Advent Calendar"
		JLabel lblAdventCalendar = new JLabel();
		lblAdventCalendar.setBorder(new EmptyBorder(0, 0, 0, 0));
		lblAdventCalendar.setBounds(12, 8, 0, 0);
		lblAdventCalendar.setSize(new Dimension(320, 42));
		lblAdventCalendar.setOpaque(true);
		lblAdventCalendar.setHorizontalAlignment(SwingConstants.CENTER);
		lblAdventCalendar.setVerticalAlignment(SwingConstants.CENTER);
		lblAdventCalendar.setText(MessageFormat.format("<html><span style=''color: {0};font-weight: bold;font-size: 16px''>" + I18n.getString("AdventsCalendar.TITLE") + " " + calendar.get(Calendar.YEAR) + "</span></html>", hexcolor));
		getContentPane().add(lblAdventCalendar);
		
		// define and add the panel for the calendar-image
		pnlAdventCalendar = new ShadowForJPanel(8);
		pnlAdventCalendar.setSize(new Dimension(width, height));
		pnlAdventCalendar.setPreferredSize(new Dimension(width, height));
		pnlAdventCalendar.setMinimumSize(new Dimension(width, height));
		pnlAdventCalendar.setBorder(new EmptyBorder(0, 0, 0, 0));
		pnlAdventCalendar.setOpaque(true);
		getContentPane().add(pnlAdventCalendar);
		
		// read image
		BufferedImage originalBufferedImage;
		try {
			originalBufferedImage = ImageIO.read(GUI.class.getResourceAsStream(ALobbyConstants.PACKAGE_PATH + "/gui/img/adventskalender.png"));

			// set the designated width for the image
			// -> do not set this to the width and height of the wrapping panel
			//    because the resulting image will be bigger than the image itself
			// -> height will be calculated depending on size of the original image
	        int thumbnailWidth = 920;
	        int thumbnailHeight = 640;
	        
	        // calculate the width of the resulting image on-the-fly
	        int widthToScale = (int)(thumbnailWidth);
	        int heightToScale = (int)((widthToScale * originalBufferedImage.getHeight() ) / originalBufferedImage.getWidth());
	        
	        if( originalBufferedImage.getWidth() > thumbnailWidth ) {
	        	widthToScale = (int)(thumbnailWidth);
	        	heightToScale = (int)((widthToScale * originalBufferedImage.getHeight() ) / originalBufferedImage.getWidth());
	        }
	        
	        if( originalBufferedImage.getHeight() > thumbnailHeight ) {
	        	widthToScale = (int)((heightToScale * originalBufferedImage.getWidth()) / originalBufferedImage.getHeight());
	        	heightToScale = (int)(thumbnailHeight);
	        }
	        
	        // create the target image without contents
	        BufferedImage resizedImage = new BufferedImage(widthToScale, heightToScale, originalBufferedImage.getType());
	    	Graphics2D g = resizedImage.createGraphics();
	    	// -> set alphacomposite 
	    	g.setComposite(AlphaComposite.Src);
	    	// -> set rendering-conditions
	    	g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	    	g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
	    	g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	    	// -> draw the image
	    	g.drawImage(originalBufferedImage, 0, 0, widthToScale, heightToScale, null);
	    	// -> forget the result :]
	    	g.dispose();
	    	
	    	// add the bufferedImage as image to the panel
	    	pnlAdventCalendar.add(new JLabel(new ImageIcon(resizedImage)));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
				
		// get the aLobby-config.xml to load the day-config
		InputStream configFile = getClass().getResourceAsStream(ALobbyConstants.PACKAGE_PATH + "/xmlcommunication/config.xml");
		XMLFiles xmlFiles = XMLFiles.getInstance();
        XMLCommunicator xmlConfig;
		try {
			xmlConfig = xmlFiles.getXMLCommunicatorOfStream(configFile);
			
			// get year
			int year = calendar.get(Calendar.YEAR);
			
			// loop through the days and load settings for each day if the specific day has been reached
			for (int day = 1; day <= 24; day++) {
				AdventCalendarDay dayObj = new AdventCalendarDay(this, this.settlersLobby, year, day);
				if( dayObj.hasBeenReached() ) {
					dayObj.setCoordinats(
						xmlConfig.getByPath("adventcalendar/year" + year + "/day" + day + "/coordinates/x1"),
						xmlConfig.getByPath("adventcalendar/year" + year + "/day" + day + "/coordinates/y1"),
						xmlConfig.getByPath("adventcalendar/year" + year + "/day" + day + "/coordinates/x2"),
						xmlConfig.getByPath("adventcalendar/year" + year + "/day" + day + "/coordinates/y2")
					);
					dayObj.setType(xmlConfig.getByPath("adventcalendar/year" + year + "/day" + day + "/type"));
					dayObj.setContent(xmlConfig.getByPath("adventcalendar/year" + year + "/day" + day + "/content_" + settlersLobby.config.getLanguage()));
					dayObj.setTitleIcon(xmlConfig.getByPath("adventcalendar/year" + year + "/day" + day + "/titleicon"));
					if (day == calendar.get(Calendar.DAY_OF_MONTH)
							&& Calendar.getInstance().get(Calendar.DAY_OF_MONTH) <= 24
							&& false == settlersLobby.config.getAdventCalendarDayShow(year, day) ) {
						this.showit = true;
					}
					dayObj.show();
				}
			}
		} catch (ParserConfigurationException e1) {
			e1.printStackTrace();
		} catch (SAXException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		getContentPane().repaint();
		
	}

	/**
	 * Return info if calendar has been already shown today.
	 * 
	 * @return boolean	true if the calendar has been shown today
	 */
	public boolean alreadyShowToday() {
		return this.showit;
	}
	
}