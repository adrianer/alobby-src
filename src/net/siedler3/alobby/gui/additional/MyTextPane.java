package net.siedler3.alobby.gui.additional;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JTextPane;

import net.siedler3.alobby.controlcenter.Test;

/*
 * own JTextPane-class to set additional properties for JTextPane-Elements
 * 
 * @author Zwirni
 */
public class MyTextPane extends JTextPane {
	
	// path to the background-image
	private String _BackgroundImageFileName = "";
	
	// target-x-position of the background-image
	private int _BackgroundImagePositionX = 0;
	
	// target-Y-position of the background-image
	private int _BackgroundImagePositionY = 0;
	
	// relative position ("left", "right", "full" or "bottom")
	private String _BackgroundImageRelativePosition = "";
	
	// repeat the image
	private boolean _BackgroundImageRepeating = false;
	
	// content-min-width, initialized with zero (than it would be ignored) 
	private int _contentMinWidth = 0;

	public MyTextPane() {
        super();
    }
	
	/**
	 * adds a optional background-image
	 */
	public void addBackgroundImageFile( String BackgroundImageFileName ) {
		this.addBackgroundImageFile( BackgroundImageFileName, this._BackgroundImagePositionX, this._BackgroundImagePositionY, "left", false );
	}
	
	/**
	 * adds a optional background-image including position
	 */
	public void addBackgroundImageFile( String BackgroundImageFileName, int x, int y ) {
		this.addBackgroundImageFile( BackgroundImageFileName, x, y, "left", false );
	}
	
	/**
	 * adds a optional background-image including position with relative position
	 */
	public void addBackgroundImageFile( String BackgroundImageFileName, int x, int y, String relativeposition, boolean repeating ) {
		this._BackgroundImageFileName = BackgroundImageFileName;
		this._BackgroundImagePositionX = x;
		this._BackgroundImagePositionY = y;
		this._BackgroundImageRelativePosition = relativeposition;
		this._BackgroundImageRepeating = repeating;
	}
	
	/**
	 * Set the content-min-width. If the background-image could overlapp this value
	 * then it is hidden.  
	 */
	public void setContentMinWidth( int contentMinWidth ) {
		this._contentMinWidth = contentMinWidth;
	}
	
    @Override
    protected void paintComponent(Graphics g) {
    	
    	// clear area for painting
        super.paintComponent( g );
    	
        // do only override if background-pic for actual theme is available
    	if( this._BackgroundImageFileName.length() > 0 )
    	{
	        // generate 2d-graphic-object for painting transparent files
	        Graphics2D g2d = ( Graphics2D ) g;
	        // get the file
	        Image img;
			try {
				InputStream imgstream = ListCellRenderer.class.getResourceAsStream(this._BackgroundImageFileName);
				BufferedImage imgio = ImageIO.read(imgstream);
				img = new ImageIcon(imgio).getImage();
				
				int imagewidth = img.getWidth(this);
				int imageheight = img.getHeight(this);
				float ratio = (float)imagewidth / (float)imageheight;
				
				// calculate the position from the right of the component where the image will be painted
				Integer posx = this._BackgroundImagePositionX;
				
		        // set distance to top
				Integer posy = this._BackgroundImagePositionY;
				
				// if image should be positioned from the right
				// calculate the coordinates
				if( this._BackgroundImageRelativePosition == "right" ) {
					// posx = width of component - width of image - defined padding from right
					posx = getWidth() - img.getWidth(this) - this._BackgroundImagePositionX;
					// posy = y-position (at top of component)
					posy = this._BackgroundImagePositionY;
				}
				
				// if image should be positioned from the bottom
				// calculate the coordinates
				if( this._BackgroundImageRelativePosition == "bottom" ) {
					// calculate new img-size
					if ( ratio < 1 ) {
						imagewidth = (int) (this.getHeight() * ratio);
						imageheight = (int) (imagewidth * ratio);
            		} else {
            			imageheight = (int) (this.getWidth() / ratio);
            			imagewidth = (int) (imageheight * ratio);
            		}
					// posx = 0 (zero :])
					posx = 0;
					// posy = component-height - imageheight
					posy = this.getHeight() - imageheight - this._BackgroundImagePositionY;
				}
				
				// if image should be positioned from the bottom
				// calculate the coordinates
				if( this._BackgroundImageRelativePosition == "bottomright" ) {
					// calculate new img-size
					//imagewidth = this.getWidth();
					//imageheight = (int) (this.getWidth() / ratio);
					
					// posx = 0 (zero :])
					posx = this.getWidth() - imagewidth;
					// posy = component-height - imageheight
					posy = this.getHeight() - imageheight;
				}
				
				// if image should be positioned as full screen
				// calculate the coordinates
				if( this._BackgroundImageRelativePosition == "full" ) {
					// posx = 0
					posx = 0;
					// posy = 0
					posy = 0;
					imagewidth = this.getWidth();
					imageheight = this.getHeight();
				}
				
		        // if calculated posx could overlap the the text in the chat
		        // than hide image
				// -> only if contentminwidth is bigger than zero
		        if( ( this._contentMinWidth > 0 && posx > this._contentMinWidth ) || this._contentMinWidth == 0 ) {
		        	if( this._BackgroundImageRepeating && this._BackgroundImageRelativePosition == "right" ) {
		        		for (posy = 0; posy < this.getHeight(); posy += imageheight) {
		        			g2d.drawImage(img, posx, posy, imagewidth, imageheight, this);
		        		}
		        	}
		        	else {
		        		// draw the file
		        		g2d.drawImage(img, posx, posy, imagewidth, imageheight, this);
		        	}
		        }
			} catch (IOException e) {
				Test.outputException(e);
			}
    	}        
    }
}