package net.siedler3.alobby.gui.additional;
/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.MessageFormat;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.Timer;

import net.siedler3.alobby.controlcenter.OpenGame.MiscType;
import net.siedler3.alobby.controlcenter.S3Map;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.controlcenter.state.UserInGame;
import net.siedler3.alobby.controlcenter.state.UserInGameList;
import net.siedler3.alobby.i18n.I18n;

/**
 * Creates and handle the Window with the list of users in a S3 game
 * and its options.
 */
public class S3MapUserListWindow extends JPanel
{
   
    private JList<UserInGame> mapUserJList;
    private JScrollPane scrollpaneMapUserList;
    private JLabel label;
    private UserInGameList users;
    private SettlersLobby settlersLobby;
    private JButton pnlButton = new JButton("<html><div style='text-align: center;'>" + I18n.getString("GUI.CALL_OF_DUTY") + "</div></html>");

    public S3MapUserListWindow(SettlersLobby settlersLobby)
    {
        super(new BorderLayout());
        this.settlersLobby = settlersLobby;
        
        
        mapUserJList = new JList<UserInGame>();
        mapUserJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        mapUserJList.setLayoutOrientation(JList.VERTICAL);
        mapUserJList.setVisibleRowCount(8);
        mapUserJList.setCellRenderer(new MapUserListCellRenderer(this.settlersLobby, false));
        //wichtig für die korrekte Grössenberechnung (speziell die Höhe)
        mapUserJList.setPrototypeCellValue(new UserInGame("prototypeUsername"));
        scrollpaneMapUserList = new JScrollPane(mapUserJList);
        
        label = new JLabel(" ");
        add(label, BorderLayout.PAGE_START);
        add(scrollpaneMapUserList);
        
        // define a timer which will enable the disabled button 
        // 30 seconds after every click
        int delay = 30000; //milliseconds
        Timer timer = new Timer(delay, new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
            	if( mapUserJList.getModel().getSize() > 1 ) {
            		pnlButton.setEnabled(true);
            	}
            }
        });
        pnlButton.setEnabled(false);
        pnlButton.setHorizontalAlignment(SwingConstants.CENTER);
        pnlButton.addActionListener(new ActionListener() {
        	@Override
		    public void actionPerformed(ActionEvent e) {
        		// collect the user as string, to call them in one message
        		List<String> userList = settlersLobby.getGameState().getNameList( true );
        		if( settlersLobby.getOpenHostGame() != null && settlersLobby.getOpenHostGame().getMisc() == MiscType.S3CE ) {
        			userList = settlersLobby.getOpenHostGame().getPlayerList();
        		}
        		String outputList = "";
        		int i = 0;
        		for (String user : userList) {
        			i++;
        			if( !settlersLobby.getNick().equals(user) ) {
        				// add user to string-list
        				if( outputList.length() > 0 ) {
        					// find a delimiter
        					if( i == userList.size() ) {
        						outputList = outputList + " " + I18n.getString("GUI.AND") + " ";
        					}
        					else {
        						outputList = outputList + ", ";
        					}
        				}
        				outputList = outputList + user;
        			}
        		}
        		// post the result, if its length is greater than zero
        		if( outputList.length() > 0 ) {
        			settlersLobby.actionChatMessage("/me " + MessageFormat.format(I18n.getString("IRCCommunicator.MSG_GAME_ACTIVITY"), outputList));
        			// disable the button, will be enabled by timer
       				pnlButton.setEnabled(false);
       				// start the timer
       				timer.start();
        		}
        	}
        });
    }

    /**
     * Fill and show the list of users which are in the actual game.
     * 
     * @param userList
     */
    public void displayUsers(UserInGameList userList)
    {
        try
        {
            users = userList.clone();
        }
        catch (CloneNotSupportedException e) {
            Test.outputException(e);
            users = new UserInGameList();
        }
        MapUserListModel model = new MapUserListModel(users.getUserList());
        mapUserJList.setModel(model);
        String text = " " + S3Map.getDisplayName(users.getMapName()) + "  " + users.getHost();
        label.setText(text);
        label.setToolTipText(text);
        
        // show list only if it has entries.
        if (model.getSize() > 0)
        {
            setVisible(true);
        }
        else
        {
            setVisible(false);
        }
        if( model.getSize() > 1 ) {
        	pnlButton.setEnabled(true);
        }
        else {
        	pnlButton.setEnabled(false);
        }
    }

    /**
     * Add an button where the host can click
     * to inform all players that they should switch to S3.
     */
	public void addButton() {
        add(pnlButton, BorderLayout.SOUTH);		
	}

	/**
	 * Remove this button.
	 */
	public void removeButton() {
		remove(pnlButton);
	}

}
