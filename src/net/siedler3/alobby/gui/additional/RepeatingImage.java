/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui.additional;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

/*
 * Own JPanel-class to add a repeating image as backgroundImage
 * to the panel. Optional the image could repeat on x- AND y-axis
 * or only on x-axis.
 * 
 * @author Zwirni
 */

public class RepeatingImage extends JPanel {
    private Image _image;
    private String _direction = "xy";
    private String _imagePosition = "bottom";

    public RepeatingImage(Image image) {
        this._image = image;
    };
    
    public RepeatingImage(Image image, String direction) {
        this._image = image;
        this._direction = direction;
    };

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        int iw = _image.getWidth(this);
        int ih = _image.getHeight(this);
        int imagepositiony = 0;
        if (iw > 0 && ih > 0) {
            for (int x = 0; x < getWidth(); x += iw) {
            	if( this._direction == "xy" ) {
            		// repeat picture also in y-axis
            		for (int y = 0; y < getHeight(); y += ih) {
            			g.drawImage(_image, x, y, iw, ih, this);
            		}
            	} else {
            		
            		// calculate the image-dimension depending on Panel-width
            		int ratio = iw / ih;
            		if ( ratio < 1 ) {
            		    iw = this.getHeight() * ratio;
            		} else {
            		    ih = this.getWidth() / ratio;
            		}
            		
            		if( this._imagePosition == "bottom" ) {
                    	imagepositiony = this.getHeight() - ih;
                    }
            		
            		// do not repeat image, just print it
            		g.drawImage(_image, x, imagepositiony, iw, ih, this);
            	}
            }
        }
    }
}