package net.siedler3.alobby.gui.additional;

/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Action;
import javax.swing.InputMap;
import javax.swing.JList;
import javax.swing.KeyStroke;

/**
 * see http://forum.java.sun.com/thread.jspa?threadID=631008&messageID=3638390
 * 
 * Fügt einer JList eine Action hinzu, die bei Enter oder Doppelklick
 * ausgeführt wird. So als hätte JList einen ActionListener.
 * 
 *
 */
public class ListUtility
{
    private static final KeyStroke ENTER = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0);
 
    public static void addAction(JList<?> source, Action action)
    {
        //  Handle enter key
 
        InputMap im = source.getInputMap();
        im.put(ENTER, ENTER);
        source.getActionMap().put(ENTER, action);
 
        //  Handle mouse double click
 
        source.addMouseListener( new ActionMouseListener() );
    }
 
    //  Implement Mouse Listener
 
    static class ActionMouseListener extends MouseAdapter
    {
        public void mouseClicked(MouseEvent e)
        {
            if (e.getClickCount() == 2)
            {
                JList<?> list = (JList<?>)e.getSource();
                Action action = list.getActionMap().get(ENTER);
 
                if (action != null)
                {
                    ActionEvent event = new ActionEvent(
                        list,
                        ActionEvent.ACTION_PERFORMED,
                        "");
                    action.actionPerformed(event);
                }
            }
        }
    }
}