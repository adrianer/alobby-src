/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui.additional;

import java.util.List;
import java.util.Vector;

/**
 * Die Klasse repräsentiert eine Zeichenkette. Man kann einer Instanz
 * Wörter übergeben, nach denen in der Zeichenkette gesucht wird. Ihre
 * Start und Endpositionen werden in offsetsStart und offsetsEnd
 * gespeichert.
 * 
 * @author Stephan Bauer
 */
public class OffsetFinder {
	private String text;
	private Vector<Integer> offsetsStart;
	private Vector<Integer> offsetsEnd;
	private boolean isWordsMarked;
	
	/**
	 * Konstruktor
	 * @param text Die in der zu suchenden Zeichenkette.
	 */
	public OffsetFinder(String text) {
		this.text = text.toUpperCase();
		isWordsMarked = false;
		offsetsStart = new Vector<Integer>();
		offsetsEnd = new Vector<Integer>();
	}

	/**
	 * Fügt den offsets eine neue Start und Endmarke hinzu.
	 * @param start Die neue Startmarke
	 * @param end Die neue Endmarke
	 */
	private void addOffset(int start, int end) {
		int position = 0;
		while (offsetsStart.size() > position && offsetsStart.elementAt(position) < start) {
			position++;
		}
		offsetsStart.insertElementAt(start, position);
		offsetsEnd.insertElementAt(end, position);
	}

	/**
	 * Überprüft auf Überschneidungen und entfernt diese
	 */
	private void checkOffsets() {
		for (int i = 0; i < offsetsStart.size()-1; i++) {
			if (offsetsStart.elementAt(i + 1) <= offsetsEnd.elementAt(i)) {
			    //der Start des Folgeelements ist kleiner als das Ende des aktuellen
			    //daher muss der Start des Folgeelments nicht extra markiert werden
				offsetsStart.remove(i + 1);
				//Jetzt muss noch überprüft werden, ob das Ende des aktuellen
				// oder das Ende des Folgeelements weiter hinten liegt 
				// (also die Frage ob sich die Elemente überlappen oder ob das hintere
				// komplett im vorderen liegt)
				if (offsetsEnd.elementAt(i) <= offsetsEnd.elementAt(i + 1))
				{
				    offsetsEnd.remove(i);
				}
				else
				{
				    offsetsEnd.remove(i+1);
				}
				i--;
			}
		}
	}
	
	/**
	 * Sucht nach dem übergebenen Wort und merkt sich die Start- und End-
	 * positionen in den Offsets.
	 * @param word Das Wort, nach dem gesucht wird.
	 */
	public void markWord(String word) {
		word = word.toUpperCase();
		int startIndex = 0;
		while (true) {
			int matchedIndex = text.indexOf(word, startIndex);
			if (matchedIndex != -1) {
				addOffset(matchedIndex, matchedIndex+word.length());
				isWordsMarked = true;
				startIndex = matchedIndex + word.length();
			} else {
				break;
			}
		}
		checkOffsets();
	}
	
	/**
	 * Wie markWord, nur mit einer Liste von Worten.
	 * @param words Die Liste von Worten.
	 */
	public void markWords(List<String> words) {
		for (String word : words) {
			markWord(word);
		}
	}
	
	/**
	 * Liefert einen Vector mit den Startmarken der gefundenen Wörter.
	 * @return Liste von den Startmarken der gefundenen Wörter.
	 */
	public Vector<Integer> getOffsetsStart() {
		return offsetsStart;
	}
	
	/**
	 * Liefert einen Vector mit den Endmarken der gefundenen Wörter.
	 * @return Liste von den Endmarken der gefundenen Wörter.
	 */
	public Vector<Integer> getOffsetsEnd() {
		return offsetsEnd;
	}
	
	/**
	 * Gibt an, ob mindestens ein Wort gefunden wurde.
	 * @return true, wenn mindestens ein Wort im Offset markiert wurde.
	 */
	public boolean isWordsMarked() {
		return isWordsMarked;
	}
}
