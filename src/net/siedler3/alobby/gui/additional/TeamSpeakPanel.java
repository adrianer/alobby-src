/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui.additional;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.math.BigInteger;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.gui.LinkLabel;
import net.siedler3.alobby.gui.MessageDialog;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.util.CryptoSupport;
import net.siedler3.alobby.util.httpRequests;

import org.json.JSONArray;
import org.json.JSONObject;

import javax.swing.JLabel;
import javax.swing.JMenuItem;

/**
 * Get the actual teamspeak-channel- and -user-list via json-request
 * and show the resulting contents as simple AWT/SWING-panel.
 * 
 * @author Zwirni
 */
public class TeamSpeakPanel implements ITeamspeakPanel
{
    private SettlersLobby settlersLobby;
    private JPanel panel;
	protected boolean interrupted = false;
	private JSONObject icons = null;
	private Runnable teamSpeakThread;
	private JPopupMenu ts3Popup;
	
	// Refresh-rate for the list in seconds
	private Integer refreshRate = 30;

	/**
	 * Generate the TeamSpeakPanel.
	 * 
	 * @param settlersLobby
	 * @param interrupted
	 */
    public TeamSpeakPanel(SettlersLobby settlersLobby, boolean interrupted)
    {
    	this.settlersLobby = settlersLobby;
    	if( false != interrupted ) {
    		this.stopLoading();
    	}
    	
    	// define popup for right-click-event
		ts3Popup = new JPopupMenu();
	    JMenuItem menuItemReload = new JMenuItem(I18n.getString("TeamSpeakPanel.RELOAD"));  //$NON-NLS-1$
	    menuItemReload.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                // initiate reloading of the ts3-panel
            	teamSpeakThread.run();
            }
        });
	    menuItemReload.setVisible(true);
	    ts3Popup.add(menuItemReload);
	    	    
	    // show the panel
        initSwingComponents();
                
    }

    /**
     * Initialize the output-components and start the JSON-request.
     */
    private void initSwingComponents()
    {
        panel = new JPanel();
        panel.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        showUrl();
    }

    /**
     * Return the ts-panel-object
     */
    @Override
    public JPanel getPanel()
    {
        return panel;
    }
    
    /**
     * Show on right click an option to reload this panel.
     */
	private final MouseAdapter mouseAdapterForTSPanel = new MouseAdapter()
	{
		@Override
		public void mouseClicked(MouseEvent evt)
		{
			if (SwingUtilities.isRightMouseButton(evt))
			{
				ts3Popup.show(evt.getComponent(), evt.getX(), evt.getY());
			}
		}
	};
	private ScheduledFuture<?> tsTask;

    /**
     * Load the TeamSpeak-list in a fixed interval via scheduler.
     * Do not load if interrupt is set.
     */
	@Override
    public void showUrl()
    {
    	teamSpeakThread = new Runnable() {
			@Override
            public void run()
            {
				try {
					if( !interrupted  ) {
						loadAndCreatePanel();
					}
					else {
						JLabel hint = new JLabel();
						hint.setText("<html>Die TeamSpeak-Ansicht<br>ist angehalten<br>während Du spielst!</html>");
						
						panel.removeAll();
						panel.removeMouseListener(mouseAdapterForTSPanel);
						
						// ScrollPane
						JScrollPane scrollPane = new JScrollPane();
						scrollPane.add(hint);

						// Helper-Pane for ScrollPane
						JPanel scrollPaneHelper = new JPanel();
						scrollPaneHelper.setLayout(new BorderLayout());

						// add channel-list to scrollpane
						scrollPane.setViewportView(hint);
						scrollPane.getViewport().setBackground(settlersLobby.theme.backgroundColor);
						scrollPane.getVerticalScrollBar().setUnitIncrement(16);
						scrollPane.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
						scrollPaneHelper.add(scrollPane);
						
						panel.add(scrollPaneHelper);
						panel.validate();
				        panel.repaint();
					}
            	} catch (Exception ex) {
            		Test.outputException(ex);
            		stopLoading();
            	}
            }
        };
        changeReadInterval(this.refreshRate);
    }
    
    /**
     * This method will reschedule "tsTask" with the new time interval.
     */
    @SuppressWarnings("static-access")
	private void changeReadInterval(long time)
    {
        if(time > 0 && teamSpeakThread instanceof Runnable)
        {       
            if (tsTask != null)
            {
            	tsTask.cancel(true);
            }

            tsTask = this.settlersLobby.scheduler.scheduleAtFixedRate(teamSpeakThread, 1, time, TimeUnit.SECONDS);
        }
    }
    
    /**
     * Generate a hash from the keys inside the icon-Object.
     * 
     * @return hashcode
     */
    private String getIconObjectListHash() {
    	String hashcode = "";
    	if( this.icons != null ) {
    		// get the keys from the object as ArrayList
    		Iterator<?> keys = this.icons.keys();
    		ArrayList<String> iconnames = new ArrayList<String>();
    		List<BigInteger> intList = new ArrayList<BigInteger>();
    	    List<String> strList = new ArrayList<String>();
    		while( keys.hasNext() ) {
    		    String key = (String)keys.next();
    		    if ( this.icons.get(key) instanceof JSONObject ) {
    		    	if (isInteger((String) key)) {
        	            intList.add(new BigInteger(key));
        	        } else {
        	            strList.add(key.toLowerCase());
        	        }
    		    }
    		}

    	    // Sort the lists
    	    Collections.sort(intList);
    	    Collections.sort(strList);
   	    
    		// Convert the int-list into string-list
    		List<String> intListAsString = new ArrayList<String>(intList.size());
    		for (BigInteger myInt : intList) { 
    			intListAsString.add(String.valueOf(myInt)); 
    		}

    		// combine the lists into one list (user order as PHP does for natural sorting)
    		// -> first the ex-integer-list
    	    iconnames.addAll(intListAsString);
    	    // -> as next the string-list
    	    iconnames.addAll(strList);
    	    
       		// collect the names from the resulting list as simple string-list
    	    // from which we will get the hash-value
    		StringBuilder sb = new StringBuilder();
            if (iconnames.size() > 0)
            {
                for (String name : iconnames)
                {
                    sb.append(name);
                }
            }
    		// generate the md5-hash from the string-list
    		hashcode = CryptoSupport.md5(sb.toString()).toLowerCase();
    	}
    	return hashcode;
    }
    
    /**
     * Check if a string represents a integer-value by checking
     * char for char.
     * 
     * @param str
     * @return true if it is a integer-value
     */
    private boolean isInteger(String str) {
        if( str == null ) {
            return false;
        }
        int length = str.length();
        if( length == 0 ) {
            return false;
        }
        int i = 0;
        if( str.charAt(0) == '-' ) {
            if (length == 1) {
                return false;
            }
            i = 1;
        }
        for( ; i < length; i++ ) {
            char c = str.charAt(i);
            if( c < '0' || c > '9' ) {
                return false;
            }
        }
        return true;
    }

    /**
     * Load and create the TeamSpeak-panel.
     */
    private void loadAndCreatePanel() {
    	String response = "";
    	// define the request
    	httpRequests httpRequests = new httpRequests();
    	// -> set parameter to configure the JSON-response
    	httpRequests.addParameter("skey", "0");
    	httpRequests.addParameter("sid", "1");
   		httpRequests.addParameter("iconhash", this.getIconObjectListHash());
    	// -> ignore security-token
    	httpRequests.setIgnorePhpSessId(true);
    	// -> set the url
		httpRequests.setUrl(settlersLobby.config.getTeamSpeakViewUrl());
		// -> get the response-String
		try {
			response = httpRequests.doRequest();
		} catch (IOException e1) {
			Test.outputException(e1);
		}

		// remove all from panel
		panel.removeAll();
		panel.removeMouseListener(mouseAdapterForTSPanel);

		String hexcolor = SettlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor());

		// Link to TS-Info-forum-entry
		JPanel pnlTsInfo = new JPanel();
		LinkLabel lblTsInfo = new LinkLabel();
		lblTsInfo.setText("<html>" + MessageFormat.format("<a href=''https://" + settlersLobby.config.getMainDomain() + "/teamspeak-3-f120.html'' style=''color:{0};font-size:{1}px;''>TeamSpeak 3 Server</a>", hexcolor, settlersLobby.config.getFontSize()) + "</html>");
		lblTsInfo.setBorder(new EmptyBorder((int) (12 * MessageDialog.getDpiScalingFactor()), 0, (int) (4 * MessageDialog.getDpiScalingFactor()), 0));
		lblTsInfo.setFont(lblTsInfo.getFont().deriveFont(Font.BOLD, settlersLobby.config.getDpiAwareFontSize()));
		pnlTsInfo.add(lblTsInfo);
		panel.add(pnlTsInfo);

		// User-Info
		JPanel pnlUserInfo = new JPanel();
		JTextPane lblUserInfo = new JTextPane();
		lblUserInfo.setContentType("text/html");
		lblUserInfo.setText(MessageFormat.format("<html><div style=''font-size:{0}px;''>{1}</div></html>", settlersLobby.config.getFontSize(), I18n.getString("TeamSpeakPanel.LOGINDATA")));
		lblUserInfo.setBorder(new EmptyBorder((int) (4 * MessageDialog.getDpiScalingFactor()), 0, 0, 0));
		lblUserInfo.setFont(lblUserInfo.getFont().deriveFont(Font.PLAIN, settlersLobby.config.getDpiAwareFontSize()));
		lblUserInfo.setEditable(false);
		lblUserInfo.setMaximumSize( lblUserInfo.getPreferredSize() );
		pnlUserInfo.add(lblUserInfo);
		panel.add(pnlUserInfo);

		CopyPastePopup chatPopup = new CopyPastePopup(lblUserInfo, settlersLobby);
		// ---> disable cut, paste and translate
        chatPopup.setVisibleCut(false);
        chatPopup.setVisiblePaste(false);
        chatPopup.setVisibleTranslate(false);
        // ---> hide popup if no text is selected
        chatPopup.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
			@Override
			public void popupMenuCanceled(javax.swing.event.PopupMenuEvent arg0) {}
			@Override
			public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent arg0) {}
			@Override
			public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent arg0) {
				if( null == lblUserInfo.getSelectedText() ) {
					chatPopup.setVisibleCopy(false);
				}
				
			}
        });
        lblUserInfo.setComponentPopupMenu(chatPopup);

		// ScrollPane
		JScrollPane scrollPane = new JScrollPane();

		// Helper-Pane for ScrollPane
		JPanel scrollPaneHelper = new JPanel();
		scrollPaneHelper.setLayout(new BorderLayout());

		// create wrapping panel for channel-list
		JPanel pnlChannelliste = new JPanel();
		pnlChannelliste.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		if( !httpRequests.isError() ) {
			if( response.length() > 0 ) {
				JSONArray channellist = null;
				JSONObject obj = null;
				try {
					obj = new JSONObject(response);
				} catch (Exception ex) {
					Test.outputException(ex);
				}
				if( obj != null ) {
					if( !obj.isNull("icons") ) {
						icons = obj.getJSONObject("icons");
					}
					if( !obj.isNull("channelList") ) {
						channellist = obj.getJSONArray("channelList");
					}
				}
				
				int gridy = 0;
				
				// go through the channel-list from json-response
				if( channellist != null ) {
					for (int i = 0 ; i < channellist.length(); i++) {
						// get the channel-object
				        JSONObject channel = channellist.getJSONObject(i);
				        
				        // get the channel-name
				        String channel_name = channel.getString("channel_name");
				        String channel_name_shortened = getShortenString(channel_name, 22); 
				        
				        // get the user-list as array
				        JSONArray userlist = channel.getJSONArray("userlist");
				        
				        // get the icon-id
				        String channel_iconid = channel.getString("icon");
				        
				        // create panel for this channel
				        JPanel pnlChannel = new JPanel();
				        pnlChannel.setLayout(new BorderLayout());
				        
				        // create label for the channel-name
				        JLabel lbltitle = new JLabel(channel_name_shortened);
				        lbltitle.setBorder(new EmptyBorder(0, 8, 0, 8));
				        lbltitle.addMouseListener(mouseAdapterForTSPanel);
				        lbltitle.setFont(lbltitle.getFont().deriveFont(Font.BOLD, settlersLobby.config.getDpiAwareFontSize()));
				        lbltitle.setToolTipText(channel_name);
				        
				        // add the label with the channel-name
				        pnlChannel.add(lbltitle, BorderLayout.WEST);
				        
				        // create panel for the channel-icon
				        JPanel pnlchannelicon = new JPanel();
				        
				        // add the password-icon, if flag is set
				        if( !channel.isNull("channel_flag_default") && channel.getString("channel_flag_default").equals("1") ) {
				        	JLabel lblIcon = this.getIconLabel(icons.getJSONObject("default_icon"));
			        		if( lblIcon instanceof JLabel ) {
			        			pnlchannelicon.add(lblIcon);
			        		}
				        }
				        
				        // add the password-icon, if flag is set
				        if( !channel.isNull("channel_flag_password") && channel.getString("channel_flag_password").equals("1") ) {
				        	JLabel lblIcon = this.getIconLabel(icons.getJSONObject("password_icon"));
			        		if( lblIcon instanceof JLabel ) {
			        			pnlchannelicon.add(lblIcon);
			        		}
				        }
				        
				        // add the password-icon, if flag is set
				        if( !channel.isNull("channel_needed_talk_power") && channel.getInt("channel_needed_talk_power") > 0 ) {
				        	JLabel lblIcon = this.getIconLabel(icons.getJSONObject("moderated_icon"));
			        		if( lblIcon instanceof JLabel ) {
			        			pnlchannelicon.add(lblIcon);
			        		}
				        }
				        
				        // add the music-icon, if flag for codec is set
				        if( !channel.isNull("channel_codec") && channel.getInt("channel_codec") > 0 ) {
				        	JLabel lblIcon = this.getIconLabel(icons.getJSONObject("music_icon"));
			        		if( lblIcon instanceof JLabel ) {
			        			pnlchannelicon.add(lblIcon);
			        		}
				        }
				        
				        // add the channel-icon if one exists
			        	if( !icons.isNull(channel_iconid) ) {
			        		JLabel lblIcon = this.getIconLabel(icons.getJSONObject(channel_iconid));
			        		if( lblIcon instanceof JLabel ) {
			        			pnlchannelicon.add(lblIcon);
			        		}
			        	}
				        
				        // add the panel with the channel-icon
				        pnlChannel.add(pnlchannelicon, BorderLayout.EAST);
				        
				        // define the layout-settings
				        gbc.fill = GridBagConstraints.HORIZONTAL;
				        gridy = gridy + i;
				        gbc.gridy = gridy;
				        gbc.weightx = 1.0;
				        
				        // add the channel into the channel-list
				        pnlChannelliste.add(pnlChannel, gbc);
				        			        
				        // if users in this channel exists, add them to the panel
				        if( userlist != null && userlist.length() > 0 ) {
				        	for (int u = 0 ; u < userlist.length(); u++) {
				        		// get the user-object
						        JSONObject user = userlist.getJSONObject(u);
						        					        
						        // get the user-name
						        String username = "";
						        if( !user.isNull("name") ) {
						        	username = user.getString("name");
						        }
						        String username_shortened = getShortenString(username, 22);
						        
						        // get the icon-id
						        String user_iconid = user.getString("client_icon_id");
						        if( !user.isNull("client_icon_id") ) {
						        	user_iconid = user.getString("client_icon_id");
						        }
						        
						        // get the group-icon-id
						        JSONArray user_group_iconids = null;
						        if( !user.isNull("client_icon_group_ids") ) {
						        	user_group_iconids = user.getJSONArray("client_icon_group_ids");
						        }
						        
						        // get the country-string
						        String user_country = "";
						        if( !user.isNull("country") ) {
						        	user_country = user.getString("country");
						        }
						        
						        // get the talking-setting
						        String user_talking = "";
						        if( !user.isNull("client_flag_talking") ) {
						        	user_talking = user.getString("client_flag_talking");
						        }
						        
						        // get the output-muted-setting
						        String user_output_muted = "";
						        if( !user.isNull("client_output_muted") ) {
						        	user_output_muted = user.getString("client_output_muted");
						        }
						        
						        // get the input-muted-setting
						        String user_input_muted = "";
						        if( !user.isNull("client_input_muted") ) {
						        	user_input_muted = user.getString("client_input_muted");
						        }
						        
						        // get the hardware-muted-setting
						        String user_hw_muted = "";
						        if( !user.isNull("client_input_hardware") ) {
						        	user_hw_muted = user.getString("client_input_hardware");
						        }
						        					        
						        // create panel for this user
						        JPanel pnlUser = new JPanel();
						        pnlUser.setLayout(new BorderLayout());
						        
						        // decide which player-icon should be visible
						        String player_icon = "player_icon";
						        if( user_talking.equals("1") ) {
						        	player_icon = "player_on_icon";
						        }
						        if( user_input_muted.equals("1") ) {
						        	player_icon = "player_input_muted";
						        }
						        if( user_output_muted.equals("1") ) {
						        	player_icon = "player_muted";
						        }
						        if( user_hw_muted.length() == 1 && user_hw_muted.equals("0") ) {
						        	player_icon = "player_hw_muted";
						        }
						        
						        // create label for the username
						        JLabel lblusername = this.getIconLabel(icons.getJSONObject(player_icon));
						        lblusername.setFont(new Font(lblusername.getFont().getFamily(), Font.PLAIN, settlersLobby.config.getDpiAwareFontSize()));
						        lblusername.setText(username_shortened);
						        lblusername.setOpaque(true);
						        lblusername.setBorder(new EmptyBorder(0, 18, 0, 8));
						        lblusername.addMouseListener(mouseAdapterForTSPanel);
						        lblusername.setToolTipText(username);
						        
						        // add the label with the username
						        pnlUser.add(lblusername, BorderLayout.WEST);
						        
						        // create panel for the user-icons
						        JPanel pnlusericons = new JPanel();
						        					        
						        // add the language-flag-icon if one exists
					        	if( !icons.isNull(user_country) ) {
					        		JLabel lblIcon = this.getIconLabel(icons.getJSONObject(user_country));
					        		if( lblIcon instanceof JLabel ) {
					        			pnlusericons.add(lblIcon);
					        		}
					        	}
					        	
					        	// add the user-icon if one exists
					        	if( !icons.isNull(user_iconid) ) {
					        		JLabel lblIcon = this.getIconLabel(icons.getJSONObject(user_iconid));
					        		if( lblIcon instanceof JLabel ) {
					        			pnlusericons.add(lblIcon);
					        		}
					        	}
					        	
					        	// add the group-icons if they exists
					        	if( user_group_iconids != null ) {
						        	for( int g = 0;g<user_group_iconids.length();g++ ) {
						        		String icongroup = user_group_iconids.getString(g);
							        	if( !icons.isNull(icongroup) ) {
							        		JLabel lblIcon = this.getIconLabel(icons.getJSONObject(icongroup));
							        		if( lblIcon instanceof JLabel ) {
							        			pnlusericons.add(lblIcon);
							        		}
							        	}
						        	}
					        	}
					        
						        // add the panel with the user-icons
						        pnlUser.add(pnlusericons, BorderLayout.EAST);
						        					        
						        // define the layout-settings
						        gbc.fill = GridBagConstraints.HORIZONTAL;
						        // sum up for the next horizontal line
						        gridy = gridy + u + 1;
						        gbc.gridy = gridy;
						        gbc.weightx = 1.0;
	
						        // set the preferred size
						        pnlUser.setFont(pnlUser.getFont().deriveFont(Font.PLAIN, settlersLobby.config.getDpiAwareFontSize()));
						        pnlUser.setPreferredSize(new Dimension((int) pnlUser.getPreferredSize().getWidth(), (int) (21 * MessageDialog.getDpiScalingFactor())));
						        
						        // add the resulting user-panel to the channel-list
						        pnlChannelliste.add(pnlUser, gbc);
						        
				        	}
				        }
				    }
				}
				else {
					// got an empty response
					// -> show a hint to the user
					JLabel lblErrorInfo = new JLabel();
					lblErrorInfo.setText("<html>" + I18n.getString("TeamSpeakPanel.OFFLINE_HINT") + "</html>");
					lblErrorInfo.setBorder(new EmptyBorder((int) (4 * MessageDialog.getDpiScalingFactor()), 0, (int) (12 * MessageDialog.getDpiScalingFactor()), 0));
					lblErrorInfo.setFont(new Font(lblUserInfo.getFont().getName(), Font.PLAIN, settlersLobby.config.getDpiAwareFontSize()));
					
					// add hint to helper-panel
					pnlChannelliste.add(lblErrorInfo);
				}
				
				// add an helper-panel with weighty of 1.0
				// which will push the scrollpane-content to the top of the viewport
				gbc.fill = GridBagConstraints.HORIZONTAL;
		        gridy = gridy + 1;
		        gbc.gridy = gridy;
		        gbc.weighty = 1.0;
		        pnlChannelliste.add(new JPanel(), gbc);
		        
			}
			else {
				// got an empty response
				// -> show a hint to the user
				JLabel lblErrorInfo = new JLabel();
				lblErrorInfo.setText("<html>" + I18n.getString("TeamSpeakPanel.OFFLINE_HINT") + "</html>");
                lblErrorInfo.setBorder(new EmptyBorder((int) (4 * MessageDialog.getDpiScalingFactor()), 0, (int) (12 * MessageDialog.getDpiScalingFactor()), 0));
				lblErrorInfo.setFont(new Font(lblUserInfo.getFont().getName(), Font.PLAIN, settlersLobby.config.getDpiAwareFontSize()));
				
				// add hint to helper-panel
				pnlChannelliste.add(lblErrorInfo);
				
				// add an helper-panel with weighty of 1.0
				// which will push the scrollpane-content to the top of the viewport
				gbc.fill = GridBagConstraints.HORIZONTAL;
		        gbc.gridy = 1;
		        gbc.weighty = 1.0; 
		        pnlChannelliste.add(new JPanel(), gbc);
			}
		}
		else {
			// team2speak war not reached
			// -> show a hint to the user
			JLabel lblErrorInfo = new JLabel();
			lblErrorInfo.setText("<html>" + I18n.getString("TeamSpeakPanel.OFFLINE_HINT") + "</html>");
            lblErrorInfo.setBorder(new EmptyBorder((int) (4 * MessageDialog.getDpiScalingFactor()), 0, (int) (12 * MessageDialog.getDpiScalingFactor()), 0));
			lblErrorInfo.setFont(new Font(lblUserInfo.getFont().getName(), Font.PLAIN, settlersLobby.config.getDpiAwareFontSize()));
			
			// add hint to helper-panel
			pnlChannelliste.add(lblErrorInfo);
			
			// add an helper-panel with weighty of 1.0
			// which will push the scrollpane-content to the top of the viewport
			gbc.fill = GridBagConstraints.HORIZONTAL;
	        gbc.gridy = 1;
	        gbc.weighty = 1.0; 
	        pnlChannelliste.add(new JPanel(), gbc);
		}
				
		// add channel-list to scrollpane
		scrollPane.setViewportView(pnlChannelliste);
		scrollPane.getViewport().setBackground(settlersLobby.theme.backgroundColor);
		scrollPane.getVerticalScrollBar().setUnitIncrement(16);
		scrollPane.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
		scrollPaneHelper.add(scrollPane);
		panel.add(scrollPaneHelper);
		panel.addMouseListener(mouseAdapterForTSPanel);
		panel.validate();
        panel.repaint();
                
	}

	/**
     * Generate a JLabel with icon from a base64-string.
     * 
     * @param jsonObject
     * @return
     */
    private JLabel getIconLabel(JSONObject jsonObject) {
    	byte[] imageBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(jsonObject.getString("base64"));
		JLabel lblIcon = new JLabel();
		lblIcon.setIcon(new ImageIcon(imageBytes));
		lblIcon.addMouseListener(mouseAdapterForTSPanel);
		return lblIcon;
	}
    
    /**
     * Start reloading of TeamSpeak-panel-content.
     */
    @Override
    public void startLoading()
    {
        interrupted = false;
        changeReadInterval(this.refreshRate);
    }

    /**
     * Stop reloading of TeamSpeak-panel-content
     * and check for new state in faster intervals.
     */
	@Override
    public void stopLoading()
    {
        interrupted = true;
        changeReadInterval(5);
    }
	
	/**
	 * Shorten a string to optimal length.
	 */
	private String getShortenString( String myString, int maxLength ) {
		String resultingString = myString;
		if( myString.length() >= maxLength ) {
			resultingString = myString.substring(0, maxLength);
			if( resultingString.length() != myString.length() ) {
				resultingString = resultingString + "..";
			}
		}
		return resultingString;
	}
	
}
