/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui.additional;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.LinkedList;
import java.util.ListIterator;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

public class MessageHistory implements ActionListener
{
    private static final String MESSAGE_HISTORY_DOWN_ACTION = "MessageHistoryDownAction";
    private static final String MESSAGE_HISTORY_UP_ACTION = "MessageHistoryUpAction";
    private JTextField input;
    private LinkedList<String> history = new LinkedList<String>();
    private ListIterator<String> iter = history.listIterator();
    private long lastMessageTime;

    private MessageHistory(JTextField input)
    {
        this.input = input;
        installKeyBindings();
        addAsActionListener();
    }

    private void add(String text)
    {
        if (text != null && text.length() > 0)
        {
            history.addFirst(text);
            if (history.size() > 50)
            {
                history.removeLast();
            }
            lastMessageTime = System.currentTimeMillis();
            iter = history.listIterator();
        }
    }

    private void performUpAction()
    {
        if (iter.hasNext())
        {
            String result = iter.next();
            if (result != null)
            {
                input.setText(result);
            }
        }
    }

    private void performDownAction()
    {
        if (iter.hasPrevious())
        {
            String result = iter.previous();
            if (result != null)
            {
                input.setText(result);
            }
        }
    }


    private void installKeyBindings()
    {
        //keybinding for up arrow
        @SuppressWarnings("serial")
        Action upAction = new AbstractAction()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                performUpAction();
            }
        };
        input.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0),
                                MESSAGE_HISTORY_UP_ACTION);
        input.getActionMap().put(MESSAGE_HISTORY_UP_ACTION,
                                 upAction);

        @SuppressWarnings("serial")
        Action downAction = new AbstractAction()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                performDownAction();
            }
        };
        input.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0),
                                MESSAGE_HISTORY_DOWN_ACTION);
        input.getActionMap().put(MESSAGE_HISTORY_DOWN_ACTION,
                                 downAction);
    }

    private void addAsActionListener()
    {
        input.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        add(input.getText());
    }

    public static MessageHistory attachToInput(JTextField input)
    {
        return new MessageHistory(input);
    }

    /**
     * Flood-protection: check if new message from input-field equals the last 5 entries.
     * Only if last message was less than 30 seconds ago. 
     * 
     * @param text
     * @return
     */
	public boolean checkIfStringEqualsLastEntries(String text) {
		// if last message was 30 seconds ago always return false
		long time = System.currentTimeMillis();
		if( lastMessageTime <= time - 30*1000 ) {
			return false;
		}
		int countMatches = 0;
		int maxEntriesToCheck = history.size();
		if( maxEntriesToCheck > 5 ) {
			maxEntriesToCheck = 5;
		}
		for (int i = 0; i < maxEntriesToCheck; i++) {
            if( text.equals(history.get(i)) ) {
          		countMatches++;
            }
        }
		if( countMatches == 5 ) {
    		return true;
    	}
		return false;
	}

}
