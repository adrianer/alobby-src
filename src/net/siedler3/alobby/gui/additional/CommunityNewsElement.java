package net.siedler3.alobby.gui.additional;

import java.util.Observable;

/**
 * Representation of single news-element.
 * 
 * @author Zwirni
 *
 */
public class CommunityNewsElement extends Observable {

	// Date and title in this Object
	private String prio = "";
	private String md5 = "";
	private String date = "";
	private String title = "";
	private String text = "";
	private String link = "";
	private String type;
	
	/**
	 * Returns the date of the element as string.
	 * 
	 * @return String
	 */
	public String getDateAsString() {
		return this.date;
	}

	/**
	 * Returns the title of the element as string.
	 * 
	 * @return String
	 */
	public String getTitle() {
		return this.title;
	}
	
	/**
	 * Returns the md5-hash for this news
	 * 
	 * @return String
	 */
	public String getMd5() {
		return this.md5;
	}
	
	/**
	 * Return the text for this news
	 * 
	 * @return String
	 */
	public String getText() {
		return this.text;
	}
	
	/**
	 * Return the link for this news
	 * 
	 * @return String
	 */
	public String getLink() {
		return this.link;
	}
	
	/**
	 * Return the priority for this news
	 * 
	 * @return String
	 */
	public String getPriority() {
		return this.prio;
	}
	
	/**
	 * Return the type of the news.
	 * 
	 * @return String
	 */
	public String getType() {
		return this.type;
	}

	/**
	 * Observ changes on this object.
	 * 
	 * @param defaultTableModelForCommunityNews
	 */
	public void addObserver(DefaultTableModelForCommunityNews defaultTableModelForCommunityNews) {
		
	}

	/**
	 * Set the date.
	 * 
	 * @param date
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * Set the title
	 * 
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Set the md5-hash for this news.
	 * 
	 * @param md5
	 */
	public void setMd5(String md5) {
		this.md5  = md5;
	}

	/**
	 * Set the text for this news.
	 * 
	 * @param text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Set the link for this news.
	 * 
	 * @param link
	 */
	public void setLink(String link) {
		this.link = link;
	}

	/**
	 * Set the priority for this news.
	 * 
	 * @param prio
	 */
	public void setPriority(String prio) {
		this.prio = prio;
	}

	/**
	 * Set the type of news.
	 * 
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}

}
