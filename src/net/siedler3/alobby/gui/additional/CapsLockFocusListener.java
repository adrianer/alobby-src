package net.siedler3.alobby.gui.additional;
/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */

import java.awt.Color;
import java.awt.Component;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Popup;
import javax.swing.PopupFactory;
import javax.swing.SwingUtilities;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.i18n.I18n;

/**
 * Wird eine Instanz dieser Klasse einer Component als FocusListener
 * hinzugefügt, erzeugt sie über der Component ein Hinweispopup, wenn
 * die Umschalttaste aktiv ist.
 * @author Stephan Bauer
 */
public class CapsLockFocusListener implements FocusListener {

	private Popup popup;
	private Component observedComponent;
	private JPanel popupPanel;
	private PopupShowHideThread thread;
	
	/**
	 * Konstruktor.
	 */
	public CapsLockFocusListener(SettlersLobby settlersLobby) {
		popupPanel = new JPanel();
		popupPanel.setBackground(settlersLobby.theme.backgroundColor);
		popupPanel.setForeground(settlersLobby.theme.getTextColor());
		popupPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		popupPanel.add(new JLabel(I18n.getString("CapsLockFocusListener.MSG_CAPSLOCK_ACTIVE"))); //$NON-NLS-1$
		// Das popupPanel muss einmal von einem Layoutmanager berechnet
		// werden, damit es bei showPopup die richtige Höhe liefert.
		// Es wird in einem HilfeDialog eingebettet, von dessen LayoutManager
		// berechnet und danach der HilfeDialog beseitigt:
		JDialog invisibleHelpDialog = new JDialog();
		invisibleHelpDialog.add(popupPanel);
		invisibleHelpDialog.pack();
		invisibleHelpDialog.dispose();
	}
	
	/**
	 * Startet die Umschalttasten-Überwachung, was zur Folge hat, dass
	 * der Hinweis je nach Zustand ein- oder ausgeblendet ist.
	 */
	@Override
	public void focusGained(FocusEvent e) {
		observedComponent = e.getComponent();
		thread = new PopupShowHideThread();
		thread.start();
	}

	/**
	 * Stoppt die Umschalttasten-Überwachung und blendet den eventuell
	 * angezeigten Hinweis aus.
	 */
	@Override
	public void focusLost(FocusEvent e) {
		if (thread != null) {
			if (thread.isAlive()) {
				thread.interrupt();
			}
			thread = null;
		}
		hidePopup();
	}

	/**
	 * Blendet den Hinweis aus, so einer angezeigt wird.
	 */
	private void showPopup() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				if (popup == null) {
					Point point = observedComponent.getLocationOnScreen();
					PopupFactory factory = PopupFactory.getSharedInstance();
					popup = factory.getPopup(null, popupPanel, point.x, point.y - popupPanel.getHeight());
					popup.show();
				}
			}
		});		
	}
	
	/**
	 * Blendet den Hinweis aus, so einer angezeigt wird.
	 */
	private void hidePopup() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				if (popup != null) {
					popup.hide();
					popup = null;
				}
			}
		});	
	}
	
	/**
	 * Umschalttasten-Überwachung 
	 * @author Stephan Bauer
	 */
	private class PopupShowHideThread extends Thread {
		/**
		 * Prueft im Sekundentakt, ob die Umschalttaste ein- oder aus-
		 * geschaltet ist. Zeigt dementsprechend den Hinweis an oder blendet
		 * ihn aus.
		 */
		public void run() {
			while (!isInterrupted()) {
				if (Toolkit.getDefaultToolkit().getLockingKeyState(KeyEvent.VK_CAPS_LOCK)) {
					showPopup();
				} else {
					hidePopup();
				}
				try {
					sleep(1000);
				} catch (InterruptedException e) {
					interrupt();
				}
			}
		}
	}
}