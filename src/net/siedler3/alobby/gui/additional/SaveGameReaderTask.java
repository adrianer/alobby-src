/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui.additional;

import net.siedler3.alobby.controlcenter.SaveGame;
import net.siedler3.alobby.controlcenter.Test;

/**
 * Task that reads the contents of the savegames.
 *
 * @author Jim
 */
public class SaveGameReaderTask implements Runnable
{

	private SaveGame[] saveGames;

	public SaveGameReaderTask(SaveGame[] saveGames) {
		this.saveGames = saveGames;
	}

	/**
	 * reads the content of the savegames.
	 */
	@Override
    public void run() {
		for (int i = 0; i < saveGames.length; i++)
		{
		    if (Thread.currentThread().isInterrupted())
		    {
		        Test.output("savegames read interrupted");
		        return;
		    }
			saveGames[i].readValuesFromSavegameIfUnread();
		}
		Test.output("savegames read");
	}

}
