/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui.additional;

import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JTextPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.Element;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import net.siedler3.alobby.gui.GUI;


/**
 * Bei einem Klick im Chatfenster wird überprüft, ob der darunter
 * liegende Text eine URL ist. Falls ja wird sie im System Browser geöffnet.
 * 
 * Generell wird überprüft, ob unter dem Cursor eine URL steht, in dem Fall
 * wird ein HandCursor angezeigt, wie man es vom Browser gewöhnt ist.
 *  
 * @author jim
 *
 */
public class UrlListener extends MouseAdapter
{

    private SimpleAttributeSet urlStyle;
    
    public UrlListener(SimpleAttributeSet urlStyle)
    {
        super();
        this.urlStyle = urlStyle;
    }
    
    @Override
    public void mouseClicked(MouseEvent e)
    {
        int clickCount = e.getClickCount();
        if (e.getButton() == MouseEvent.BUTTON1 && clickCount == 1)
        {
            Object src = e.getSource();
            if (src instanceof JTextPane)
            {
                JTextPane textPane = (JTextPane) src;
                int offset = textPane.viewToModel(e.getPoint());
                findUrlAndOpen(textPane, offset);
            }
        }
    }
       
    @Override
    public void mouseMoved(MouseEvent e)
    {
        Object src = e.getSource();
        if (src instanceof JTextPane)
        {
            JTextPane textPane = (JTextPane) src;
            int offset = textPane.viewToModel(e.getPoint());
            Element element = textPane.getStyledDocument().getCharacterElement(offset);
            AttributeSet set = element.getAttributes();
            // wenn die Maus über einer URL steht, soll der HandCursor erscheinen
            // dazu wird überprüft, ob der Text unter dem Cursor dem urlStyle 
            // entspricht. Aktuell wird dies nur anhand der Schriftfarbe
            // und des Underlines ermittelt
            if (StyleConstants.getForeground(set).equals(StyleConstants.getForeground(urlStyle)) &&
            		StyleConstants.isUnderline(set) == StyleConstants.isUnderline(urlStyle))
            {
                textPane.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            }
            else
            {
                textPane.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            }
        }
    }



    /**
     * Sucht im textPane an der Stelle offset nach einer URL.
     * Falls eine erkannt wird, wird sie geöffnet.
     * Abgeleitet vom Code aus dem Artikel
     * http://www.javaworld.com/javaworld/jw-10-2005/jw-1031-jepane.html
     * 
     * @param textPane
     * @param offset
     */
    private void findUrlAndOpen(JTextPane textPane, int offset)
    {
        StyledDocument document = textPane.getStyledDocument();
        Element e = document.getCharacterElement(offset);
        
    
        int startOffset = e.getStartOffset();
        int length = e.getEndOffset() - e.getStartOffset();
        

        String text;
        try
        {
            text = document.getText(startOffset, length);
            text += ' '; //brauchen ein space am Ende für den matcher,
            //falls die URL von Anfang bis Ende geht
        }
        catch (Exception ex) 
        {
            return;
        }

        int relativeOffset = offset - startOffset;

        Matcher matcher = Pattern.compile(
                "(?i)(\\b(http://|https://|www.|ftp://|file:/)\\S+)(\\s+)")
                .matcher(text);

        while (matcher.find()) 
        {
            String url = matcher.group(1);
            
            if (matcher.start() <= relativeOffset && matcher.end() >= relativeOffset)
            {

                Matcher dotEndMatcher = Pattern.compile("([\\W&&[^/]]+)$")
                        .matcher(url);

                //Ending non alpha characters like [.,?%] shouldn't be included
                // in the url.
                if (dotEndMatcher.find()) 
                {
                    url = dotEndMatcher.replaceFirst("");
                }
                textPane.setSelectionStart(startOffset + matcher.start());
                textPane.setSelectionEnd(startOffset + matcher.end(1));
                GUI.openURL(url);
            }
        }
    }
}
