package net.siedler3.alobby.gui.additional;
/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
import java.awt.Toolkit;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class IntegerPlainDocument extends PlainDocument {

	public void insertString(int offset, String s, AttributeSet attributeSet)
			throws BadLocationException {
		try {
			Integer.parseInt(s);
		} catch (Exception ex) // only allow integer values
		{
			Toolkit.getDefaultToolkit().beep(); // macht ein DÜT
			return;
		}
		super.insertString(offset, s, attributeSet);
	}
}
