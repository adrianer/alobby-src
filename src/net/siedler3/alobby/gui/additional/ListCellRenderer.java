/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui.additional;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;

import net.siedler3.alobby.controlcenter.Configuration;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.i18n.I18n;

public class ListCellRenderer extends DefaultListCellRenderer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8402038935114558765L;
	private static ImagePanel iconAway;
	private static ImagePanel iconOperator;
	private static ImagePanel iconBuddy;
	private static ImagePanel iconIgnored;
	private static ImagePanel iconGame;
	private static ImagePanel iconNoAway;
	private static ImagePanel iconNoOperator;
	private static ImagePanel iconNoBuddy;
	private static ImagePanel iconNoIgnored;
	private static boolean isIconsLoaded = false;
	
	private SettlersLobby settlersLobby;

	public ListCellRenderer(SettlersLobby settlerslobby) {
		super();
		this.settlersLobby = settlerslobby;
		// Load the icons for irc-userlist only one time.
		loadIcons();
	}

	/**
	 * Get the list of icons for irc-userlist, load this only one time.
	 */
	private void loadIcons() {
		if (!isIconsLoaded) {
			try {
				iconAway = new ImagePanel(ImageIO.read(
						ListCellRenderer.class.getResourceAsStream(settlersLobby.theme.iconAway)));
				iconOperator = new ImagePanel(ImageIO.read(
						ListCellRenderer.class.getResourceAsStream(settlersLobby.theme.iconOperator)));
				iconBuddy = new ImagePanel(ImageIO.read(
						ListCellRenderer.class.getResourceAsStream(settlersLobby.theme.iconBuddy)));
				iconIgnored = new ImagePanel(ImageIO.read(
						ListCellRenderer.class.getResourceAsStream(settlersLobby.theme.iconIgnored)));
				iconGame = new ImagePanel(ImageIO.read(
						ListCellRenderer.class.getResourceAsStream(settlersLobby.theme.iconGame)));
				iconNoAway = new ImagePanel(ImageIO.read(
						ListCellRenderer.class.getResourceAsStream(settlersLobby.theme.iconNoAway)));
				iconNoOperator = new ImagePanel(ImageIO.read(
						ListCellRenderer.class.getResourceAsStream(settlersLobby.theme.iconNoOperator)));
				iconNoBuddy = new ImagePanel(ImageIO.read(
						ListCellRenderer.class.getResourceAsStream(settlersLobby.theme.iconNoBuddy)));
				iconNoIgnored = new ImagePanel(ImageIO.read(
						ListCellRenderer.class.getResourceAsStream(settlersLobby.theme.iconNoIgnored)));
			} catch (Exception e) {
				// Dann gibt es eben kein Icon
			}
			isIconsLoaded = true;
		}
	}

	/**
	 * Define the look and feel for the irc-userlist.
	 */
	
	@Override
    public Component getListCellRendererComponent (JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus)
	{
		S3User user = (S3User) value;

		JPanel panel = new JPanel();
		panel.setBackground(settlersLobby.theme.backgroundColor);
		panel.setLayout(new GridBagLayout());

		Vector<ImagePanel> icons = new Vector<ImagePanel>();
		Vector<ImagePanel> noIcons = new Vector<ImagePanel>();
		if (user.isIgnored()) {
			icons.add(iconIgnored);
		} else {
			noIcons.add(iconNoIgnored);
		}
		if (user.isAdmin()) {
			icons.add(iconOperator);
		} else {
			noIcons.add(iconNoOperator);
		}
		// Besonderheit: Da Away und Away in Game nicht gleichzeitig angezeigt wird
		// Gibt es nur eine Platzhaltergrafik für beide Zustände.
		if (user.isInGame()) {
			icons.add(iconGame);
		} else if (user.isAway()) {
			icons.add(iconAway);
		} else {
			noIcons.add(iconNoAway);
		}
		if (user.isBuddy()) {
			icons.add(iconBuddy);
		} else {
			noIcons.add(iconNoBuddy);
		}
		//reserve at most 2 no-icons, otherwise too much space is wasted
		//for the no-icons, as it is rather unlikely that a person
		//has all 4 possible icons..3 may occur, if he is for example
		//friend, operator and away, but even this should apply only
		//for a small amount of people
		int numberOfNoIconsPrinted = 2 - icons.size();
		for (int i = 0; i < noIcons.size() && i < numberOfNoIconsPrinted; i++) {
			panel.add(noIcons.elementAt(i), new GridBagConstraints(i, 0, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		}
		for (int i = 0; i < icons.size(); i++) {
			panel.add(icons.elementAt(i), new GridBagConstraints(noIcons.size() + i, 0, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		}
		JLabel lblNick = new JLabel(user.getNick());
		lblNick.setFont(lblNick.getFont().deriveFont((float) Configuration.getInstance().getDpiAwareFontSize()));
		// color the name depending on its vpn-state and if they are beta-user or not
		// and set tooltip
		String tooltip = null;
		Color color = settlersLobby.theme.getTextColor();
		if( false != user.isBetaUser() ) {
			color = new Color(144,123,254);
		}
		if( user.getVpnState() ) {
			// vpn is active: full color
			lblNick.setForeground(color);
			tooltip = I18n.getString("ListCellRenderer.VPN_ONLINE");
		}
		else {
			// vpn inactive: alpha-transparent color
			lblNick.setForeground(new Color(color.getRed(), color.getGreen(), color.getBlue(), 170));
			tooltip = I18n.getString("ListCellRenderer.VPN_OFFLINE");;
		}
		panel.add(lblNick, new GridBagConstraints(noIcons.size() + icons.size(), 0, 1, 1, 1, 0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));

        if (!user.getAwayMsg().isEmpty())
        {
        	if( tooltip != null ) {
        		tooltip = "<html>" + tooltip + "<br>" + user.getAwayMsg() + "</html>";
        	}
        	else {
        		tooltip = user.getAwayMsg();
        	}
        }
        panel.setToolTipText(tooltip);

		return panel;
	}
}
