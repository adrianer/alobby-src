package net.siedler3.alobby.gui.additional;

import java.util.List;

import javax.swing.AbstractListModel;

import net.siedler3.alobby.controlcenter.state.UserInGame;

/**
 * Define the userlist-model which is used from S3- and S4-playerlists.
 * 
 * @author Zwirni
 *
 */

public class MapUserListModel extends AbstractListModel<UserInGame> {
    private List<UserInGame> userList;

    public MapUserListModel(List<UserInGame> userList)
    {
        super();
        this.userList = userList;
    }

    @Override
    public UserInGame getElementAt(int index)
    {
        return userList.get(index); 
    }

    @Override
    public int getSize()
    {
        return userList.size();
    }
    
}