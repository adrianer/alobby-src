/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui.additional;

import java.util.Observable;
import java.util.Observer;

import javax.swing.table.DefaultTableModel;

import net.siedler3.alobby.i18n.I18n;

/**
 * Extends TableModel for streams.
 *
 * @author Zwirni
 */
public class DefaultTableModelForStreams extends DefaultTableModel implements Observer
{

	/**
	 * Liefert eine Instanz
	 * @param data			Daten für das Tablemodel.
	 * @param columnNames	Überschriften für das Tablemodel.
	 */
	public DefaultTableModelForStreams() {
		super(null, new String[] { 	I18n.getString("GUI.STREAM_PLAYER"), //$NON-NLS-1$
									I18n.getString("GUI.STREAM_LINK"), //$NON-NLS-1$
									I18n.getString("GUI.STREAM_ONLINESINCE") //$NON-NLS-1$
					             });
	}

	/**
	 * Adds a stream to the stream-table
	 * 
	 * @param onlinesince 
	 * @param link 
	 * @param user 
	 */
	public synchronized void addStream(String user, String link, String onlinesince)
	{
		addRow(createTableEntry(user, link, onlinesince));
	}

	/**
	 * Create a table-entry for stream-table with given parameters.
	 * 
	 * @param user
	 * @param link
	 * @param onlinesince
	 * @return
	 */
    private static String[] createTableEntry(String user, String link, String onlinesince)
    {
               
        String[] tableEntry = {
        		user,
                link,
                onlinesince
        };
        return tableEntry;
    }

	@Override
	public void update(Observable observable, Object change) {
	}

	/**
	 * Remove a stream from a given nickname.
	 * 
	 * @param user
	 */
	public void removeStream(String user) {
		for(int row = 0;row < this.getRowCount();row++) {
			if( this.getValueAt(row, 0).toString().equalsIgnoreCase(user) ) {
				this.removeRow(row);
			}
		}		
	}

}
