/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.text.AbstractDocument;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.gui.chat.DocumentSizeFilter;

/**
 * Creates a window with message for user
 * including the possibility to use different 
 * buttons for different tasks to interact.
 * 
 * Usage:
 * 		// generate MessageDialog-Object
 * 		MessageDialog messagewindow = new MessageDialog(null, "my windows title", true);
 * 		// add message to display
 * 		messagewindow.addMessage(message);
 * 		// finally set window visible
 *      messagewindow.setVisible(true);
 *
 * Hints:
 * * To get line breaks in message-texts use HTML-code (<br> or <br />) in addMessage()
 *
 * @author Zwirni
 */
public class MessageDialog extends JDialog
{
	
	private JPanel _contentpanel;
	private JPanel _buttonpanel;
	private Container _container;
	
	// set the inner padding for all components
	private Integer _padding = 8;
	private boolean closingState = false;
	private SettlersLobby settlersLobby = null;
		
	/**
	 * Creates a window with message for user
	 * including different buttons to interact
	 * 
	 * @param parent 		owner of the new window
	 * @param title 		window-title
	 * @param modal 		block parent window or not
	 */
	public MessageDialog(JFrame parent, String title, boolean modal){
		super(parent,title,modal);
		
		// set layout to FlowLayout
		getContentPane().setLayout(new FlowLayout());
		
		// create the wrapping container
		_container = new Container();
		// let the content display from north to south
		_container.setLayout( new BoxLayout( _container, BoxLayout.Y_AXIS ) );
		
		// container #1: the content (e.g. the message)
		_contentpanel = new JPanel();
		// let this JPanel-content display from north to south
		_contentpanel.setLayout( new GridLayout(0,1) );
		_contentpanel.setBorder(new EmptyBorder(_padding, _padding, _padding, _padding));
		
		// container #2: the buttons
		_buttonpanel = new JPanel();
		
		// global settings:
        // -> suppress icon to resize the window
        this.setResizable(false);
        
        // -> set position relative to parent-window
        this.setLocationRelativeTo(this.getParent());
    }

    public static double getDpiScalingFactor() {
        return Toolkit.getDefaultToolkit().getScreenResolution() / 96.0;
    }

	/**
	 * add a button to the window
	 * could be called multiple times, all buttons will be displayed side by side
	 * 
	 * @param title value for the button-title
	 */
	public void addButton( String title ) {
		addButton( title, null );
	}
	
	/**
	 * add a input-field with checkbox
	 * 
	 * @param title		value for the label
	 * @param value		value for the textfield
	 */
	public JCheckBox addPanelWithLabelAndCheckbox( String title, String value ) {
		// only if title is set
		if( title.length() > 0 ) {
			JPanel panel = new JPanel();
			JLabel label = new JLabel();
			JCheckBox input = new JCheckBox();
			label.setText(title);
			input.setText(value);
			input.setHorizontalAlignment(SwingConstants.RIGHT);
			panel.add(label);
			panel.add(input);
			
			// add the panel to the wrapping panel
			_contentpanel.add(panel);
			
			return input;
		}
		return null;
	}
	
	/**
	 * add a input-field with label
	 * 
	 * @param title		value for the label
	 * @param maxlength	max length of the textfield
	 * @param value		value for the textfield
	 */
	public JTextField addPanelWithLabelAndTextField( String title, int maxlength, String value ) {
		// only if title is set
		if( title.length() > 0 && maxlength > 0 ) {
			JPanel panel = new JPanel();
			JLabel label = new JLabel();
			JTextField input = new JTextField();
			label.setText(title);
			input.setColumns(maxlength);
			input.setText(value);
			input.setHorizontalAlignment(SwingConstants.RIGHT);
			((AbstractDocument)input.getDocument()).setDocumentFilter(new DocumentSizeFilter(2));
			panel.add(label);
			panel.add(input);
			
			// add the panel to the wrapping panel
			_contentpanel.add(panel);
			
			return input;
		}
		return null;
	}
	
	/**
	 * add a button to the window
	 * could be called multiple times, all buttons will be displayed side by side
	 * 
	 * @param title 		value for the button-title
	 * @param listener 		add an ActionListener to the button
	 * @param keyListener	add an KeyListener to the button
	 */
	public JButton addButton( String title, ActionListener listener, KeyListener keyListener ) {
		// only add buttons with title
		if( title.length() > 0 ) {
			
			// create the button
			JButton button = new JButton( title );
			
			// add the ActionLister to the button if it really exists
			if( listener instanceof ActionListener ) {
				button.addActionListener(listener);
			}
			
			// add the MouseListener to the button if it really exists
			if( keyListener instanceof KeyListener ) {
				button.addKeyListener(keyListener);
			}
			
			// add the button to panel
			_buttonpanel.add(button);
			
			return button;
		}
		return null;
	}
	
	/**
	 * add a simple button to the window
	 * could be called multiple times, all buttons will be displayed side by side
	 * 
	 * @param title 		value for the button-title
	 * @param listener 		add an ActionListener to the button
	 */
	public JButton addButton( String title, ActionListener listener ) {
		return addButton( title, listener, null);
	}
	
	/**
	 * add simple-message for user to display
	 * could be called multiple time, all messages will be display one after the other
	 *  
	 * @param message 		the message to display
	 */
	public LinkLabel addMessage( String message ) {
		return addMessage( message, "" );
	}
	
	/**
	 * add full-message for user to display
	 * could be called multiple time, all messages will be display one after the other
	 * including all options for display
	 * 
	 * @param message 		the message to display
	 * @param icon			optional icon to display before the message
	 */
	public LinkLabel addMessage( String message, String imageFile ) {
		// add image as icon to JLabel if it exists
		if( imageFile instanceof String && imageFile.length() > 0 ) {
			try {
				// get the stream of the image and ..
				InputStream imagestream = this.getClass().getResourceAsStream(imageFile);
				// .. check it is ok
				if( imagestream instanceof InputStream ) {
					// if yes, get the image as icon
					ImageIcon icon = new ImageIcon(ImageIO.read(imagestream));
					// and print it as icon to the message
					return addMessage(message, icon);
				}
			} catch(IOException e) {
				// if something went wrong ..
				e.printStackTrace();
			}
		}
		return addMessage( message, new ImageIcon("") );
	}
	
	public LinkLabel addMessage(String message, Icon icon) {
		// only add messages with length
		if( message != null && message.length() > 0 ) {
			
			// create a message-object
			LinkLabel messageToAdd = new LinkLabel();
		
			// add image as icon to LinkLabel if it exists as Icon-Object
			if( icon instanceof Icon && icon.getIconWidth() > 0 && icon.getIconHeight() > 0 ) {
				if( icon.getIconWidth() > 0 && icon.getIconHeight() > 0 ) {
					// set icon to label
					messageToAdd.setIcon(icon);
					// and add some gap between icon and message
					messageToAdd.setIconTextGap(_padding);
				}
			}
			
			// if message doesn't contain <html>,
			// than wrap the message with <html></html> to allow e.g. linebreak via <br>
			if( !message.contains("<html>") ) {
				message = "<html>" + message + "</html>";
			}
			
			// add the text
			messageToAdd.setText(message);
			messageToAdd.setFont(messageToAdd.getFont().deriveFont(Font.BOLD));
			
			// add some padding in form of border because its the only way to add space between JLabels
			messageToAdd.setBorder(new EmptyBorder(0, 0, _padding, 0));
			
			// add the message-object
			_contentpanel.add(messageToAdd);
			
			return messageToAdd;
		}
		return null;
	}
	
	/**
	 * resize window to content size
	 */
	private void resizeWindowToContentSize() {
		this.pack();
	}
	
	/**
	 * Set if this window should not be closed by click on closing-button top right.
	 * 
	 * @param closingState 	true if the window should not be closed by click on the button
	 */
	public void preventClosing( boolean closingState ) {
		this.closingState = closingState;
	}
	
	/**
	 * Get the option for closing-button depending on closingState.
	 * 
	 * @return Integer 
	 */
	private int getMessageWindowDefaultCloseOperation() {
		if( false != this.closingState ) {
			return WindowConstants.DO_NOTHING_ON_CLOSE;
		}
		return JDialog.DISPOSE_ON_CLOSE;
	}
	
	/**
	 * (non-Javadoc)
	 * @see java.awt.Dialog#setVisible(boolean)
	 */
	public void setVisible( boolean visibility ) {		
		// add collected content to window
		// -> only if there is any content
		if( _contentpanel.getComponentCount() > 0 ) {
			_container.add(_contentpanel);
		}
		// the same with the buttons
		if( _buttonpanel.getComponentCount() > 0 ) {
			_container.add(_buttonpanel);
		}
		
		// add the wrapping container (with its label(s) an button(s) to the window
		add(_container);
		
		// resize window to content size
		this.resizeWindowToContentSize();
		
		// set the dialog-window-position relativ to its parent-window
		this.setLocationRelativeTo(this.getParent());
		
		// set the window on configured screen
		if( this.settlersLobby != null ) {
			this.settlersLobby.showOnSpecifiedScreen(this.settlersLobby.config.getScreen(), this);
		}
		
		// set the default-action on close
		this.setDefaultCloseOperation(this.getMessageWindowDefaultCloseOperation());
		
		// set resulting window to visible
		super.setVisible(visibility);
	}

	/**
	 * Add SettlersLobby as object to MessageDialog
	 * 
	 * @param settlersLobby
	 */
	public void setSettlersLobby(SettlersLobby settlersLobby) {
		this.settlersLobby = settlersLobby;
	}
	
}
