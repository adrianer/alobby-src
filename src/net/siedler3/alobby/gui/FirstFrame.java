/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.MessageFormat;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.gui.additional.ImagePanel;
import net.siedler3.alobby.gui.additional.addCursorChangesToButton;
import net.siedler3.alobby.i18n.I18n;

public class FirstFrame extends JFrame {


    private static final long serialVersionUID = -408716755319172760L;

    private static final int ABSTAND = 5;

	public static BufferedImage backgroundImage;
	static
	{
        try
        {
            backgroundImage = ImageIO.read(FirstFrame.class.getResourceAsStream(
                    ALobbyConstants.PACKAGE_PATH + "/gui/img/StartScreen.png")); //$NON-NLS-1$
        } catch (IOException e)
        {
            backgroundImage = null;
        }
	}

	private SettlersLobby settlersLobby;

	public FirstFrame(SettlersLobby settlersLobby, String title) {
		super(title);
		this.settlersLobby = settlersLobby;

		//There is a bug with setResizable(false), sometimes additional 10 pixels are added
		//to the right and to the bottom. Therefore create a JPanel that has the same background
		//Color as the image to take up this extra space.
		JPanel bugWorkaroundPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		if (backgroundImage != null)
		{
		    bugWorkaroundPanel.setBackground(settlersLobby.theme.startBackgroundColor);
		}
		add(bugWorkaroundPanel);
	    // background-image-helper-component
        JPanel backgroundPanel = new ImagePanel(backgroundImage);

        backgroundPanel.setLayout(new GridBagLayout());
        //add(backgroundPanel);
        bugWorkaroundPanel.add(backgroundPanel);
        bugWorkaroundPanel.setBackground(settlersLobby.theme.startBackgroundColor);

        // create a panel for the linklist on the bottom
        String hexcolor = SettlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor());
        JPanel pnlLinkList = new JPanel();
        LinkLabel lnkForum = new LinkLabel();
        lnkForum.setText(MessageFormat.format("<html>" + I18n.getString("GUI.FORUM") + "</html>", hexcolor));
        lnkForum.setFont(lnkForum.getFont().deriveFont(Font.BOLD));
        LinkLabel lnkFAQ = new LinkLabel();
        lnkFAQ.setText(MessageFormat.format("<html>" + I18n.getString("GUI.FAQ") + "</html>", hexcolor));
        lnkFAQ.setFont(lnkFAQ.getFont().deriveFont(Font.BOLD));
        pnlLinkList.add(lnkForum);
        pnlLinkList.add(lnkFAQ);
        
		// Buttons bauen
		JButton btnSettings = new JButton(I18n.getString("FirstFrame.SETUP")); //$NON-NLS-1$
		btnSettings.addMouseListener(new addCursorChangesToButton());
		btnSettings.setFont(btnSettings.getFont().deriveFont(Font.BOLD));
		JButton btnChat = new JButton(I18n.getString("FirstFrame.LOBBY")); //$NON-NLS-1$
		btnChat.addMouseListener(new addCursorChangesToButton());
		btnChat.setFont(btnChat.getFont().deriveFont(Font.BOLD));
		JButton btnStandalone = new JButton(I18n.getString("FirstFrame.STANDALONE")); //$NON-NLS-1$
		btnStandalone.addMouseListener(new addCursorChangesToButton());
		btnStandalone.setFont(btnStandalone.getFont().deriveFont(Font.BOLD));
		JLabel btnConfigDir = new JLabel("\uf188"); //$NON-NLS-1$
		btnConfigDir.setToolTipText(I18n.getString("FirstFrame.GOTO_LOGFILES"));
		btnConfigDir.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnConfigDir.setFont(this.settlersLobby.config.getFontAweSome((float) (14 * MessageDialog.getDpiScalingFactor())));

		// Set ActionListener for each button
		// -> settings-Button
		btnSettings.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(ActionEvent arg0) {
			    startOpenSettings();
			}
		});
		btnSettings.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
			}
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode()==KeyEvent.VK_ENTER){
					startOpenSettings();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) {
			}
		});
		// -> login-Button
		btnChat.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(ActionEvent arg0) {
				startConnecting();
			}
		});
		btnChat.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
			}
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode()==KeyEvent.VK_ENTER){
					startConnecting();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) {
			}
		});
		// -> Standalone-Button
		btnStandalone.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(ActionEvent arg0) {
				startOpenStandalone();
			}
		});
		btnStandalone.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
			}
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode()==KeyEvent.VK_ENTER){
					startOpenStandalone();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) {
			}
		});
		// -> config-dir-link, only mouseadapter 
		btnConfigDir.addMouseListener(new MouseAdapter()  
		{  
		    @Override
            public void mouseClicked(MouseEvent e)  
		    {
		    	// show bugtracker-window on click
	    		Bugtracker bugTrackerWindow = new Bugtracker(settlersLobby, "Bugtracker");
	    		bugTrackerWindow.setVisible(true);
		    }  
		}); 

		// form the layout with the buttons
		backgroundPanel.add(btnSettings, new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(2*ABSTAND, 2*ABSTAND, 0, 0), 0, 0));
		backgroundPanel.add(btnChat, new GridBagConstraints(0, 1, 1, 1, 0, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(ABSTAND, 2*ABSTAND, 0, 0), 0, 0));
		backgroundPanel.add(btnStandalone, new GridBagConstraints(0, 2, 1, 1, 0, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(ABSTAND, 2*ABSTAND, 2*ABSTAND, 0), 0, 0));
		// add a fill element that takes all space below the last button to the bottom
		Component filler1 = Box.createRigidArea(new Dimension(0,0));
		Component filler2 = Box.createRigidArea(new Dimension(210,1));
		backgroundPanel.add(filler1, new GridBagConstraints(0, 3, 1, 1, 0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0, 0));
		// add a fill element that takes 210 pixels to the right and gets all additional vertical and horizontal space
		backgroundPanel.add(filler2, new GridBagConstraints(1, 0, 1, 4, 1.0, 1.0, GridBagConstraints.NORTHEAST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0), 0, 0));
		backgroundPanel.add(pnlLinkList, new GridBagConstraints(0, 4, 1, 1, 0, 0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0,0,0,0), 4, 0));
		backgroundPanel.add(btnConfigDir, new GridBagConstraints(1, 4, 1, 1, 0, 0, GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE, new Insets(0,0,0,0), 4, 4));

		// prevent resizing
		setResizable(false);
		pack();
		
		// set the focus to the login-button
		btnChat.requestFocus();
	}
	
	/**
	 * Open the Standalone-dialog.
	 */
	private void startOpenStandalone() {
		setVisible(false);
		FirstFrame.this.settlersLobby.actionIngameOptionsStandalone();
	}

	/**
	 * Open the aLobby-settings.
	 */
	private void startOpenSettings() {
		FirstFrame.this.settlersLobby.actionWantsToEditSettings();
	}

	/**
	 * Start to connect to aLobby.
	 */
	private void startConnecting() {
		FirstFrame.this.settlersLobby.actionWantsToConnect();
		// enable the register-button after 5 seconds
		settlersLobby.getLoginframe().enableRegisterButtonAfterMilliseconds(5000);
	    // remove firstframe-window as we don't need it anymore
		dispose();
	}

}
