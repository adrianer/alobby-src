/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui;

import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.CopyOption;
import java.nio.file.Files;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.i18n.I18n;

/**
 * Provides a single window where the portchecker can started
 * and results of the check will be visible in a table.
 * 
 * @author Zwirni
 */
public class PortcheckerWindow  extends JFrame {

	private SettlersLobby settlersLobby;
	private DefaultTableModel tableModel;
	private PortcheckerWindow portcheckerWindow;

	@SuppressWarnings("static-access")
	public PortcheckerWindow(SettlersLobby settlersLobby) {
		
		this.settlersLobby  = settlersLobby;
		this.portcheckerWindow = this;
		
		// set the window-size
		this.setSize(500, 500);
		
		// set always on top so user must use it
		//this.setAlwaysOnTop(true);
		
		// set the default text-orientation in this window (possible language-depending)
		this.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		
		// if window is closed reset the dialog in GUI if requested
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		// set layout for the window
		this.setLayout(new GridLayout());
		
		// set Window-title
		this.setTitle(String.format("%s > %s", settlersLobby.getGUI().PROGRAM_NAME, I18n.getString("PortcheckerWindow.WINDOW_TITEL")));
		
		// add panel
		JPanel pnlWrapper = new JPanel();
		pnlWrapper.setLayout(new BoxLayout(pnlWrapper, BoxLayout.PAGE_AXIS));
		
		JPanel pnlButtons = new JPanel();
		
		// add button which starts the portchecker with complete scan
		JButton btnStart = new JButton();
		btnStart.setText(I18n.getString("PortcheckerWindow.BTN_START"));
		btnStart.addActionListener(actionListenerToStartPortChecker);
		pnlButtons.add(btnStart);
		
		// add button which starts the portchecker with minimal scan
		JButton btnStartMinimal = new JButton();
		btnStartMinimal.setText(I18n.getString("PortcheckerWindow.BTN_START_MINIMAL"));
		btnStartMinimal.addActionListener(actionListenerToStartPortCheckerMinimal);
		pnlButtons.add(btnStartMinimal);
		
		// add the button-panel to the wrapper-panel
		pnlWrapper.add(pnlButtons);
		
		// add scrollpane for the table
		JScrollPane scrollPane = new JScrollPane();
		
		// define a table-model for the results
		// -> incl. other return value for column 1 (which is a JLabel)
		//    -> will be rendered via tableCellRenderer
		tableModel = new DefaultTableModel() {
			@SuppressWarnings({ "unchecked", "rawtypes" })
			@Override
		    public Class getColumnClass(int columnIndex) {
		        if(columnIndex == 0) {
		        	return getValueAt(0, columnIndex).getClass();
		        }
		        else {
		        	return super.getColumnClass(columnIndex);
		        }
		    }
		};
		tableModel.addColumn("");
		tableModel.addColumn(I18n.getString("PortcheckerWindow.ROW_TYPE"));
		tableModel.addColumn(I18n.getString("PortcheckerWindow.ROW_PORT"));
		tableModel.addColumn(I18n.getString("PortcheckerWindow.ROW_DIRECTION"));
		
		// add table for the results
		JTable tblResults = new JTable()
		{
			@Override
			public boolean isCellEditable(int row, int column)
			{
				return false;
			}
		};
		tblResults.setModel(tableModel);
		tblResults.setDefaultRenderer(JLabel.class, new DefaultTableCellRenderer() {
			public void fillColor(JTable t,JLabel l,boolean isSelected ){
		        //setting the background and foreground when JLabel is selected
		        if(isSelected){
		            l.setBackground(t.getSelectionBackground());
		            //l.setForeground(t.getSelectionForeground());
		        }
		 
		        else{
		            l.setBackground(t.getBackground());
		            //l.setForeground(t.getForeground());
		        }
		 
		    }
			
			// render the JLabel-cell as JLabel incl. coloring
			@Override
		    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
		         boolean hasFocus, int row, int column)
		     {
		 
		        if(value instanceof JLabel){
		        	JLabel label = (JLabel)value;
		            // to make label foreground and background visible:
		            label.setOpaque(true);
		            fillColor(table,label,isSelected);
		            return label;
		        }
		        else {
		            return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		        }		 
		     }
		});
		tblResults.setFont(tblResults.getFont().deriveFont(Font.PLAIN, settlersLobby.config.getDpiAwareFontSize()));
		tblResults.setRowHeight(tblResults.getFont().getSize() + 6);
		tblResults.getTableHeader().setReorderingAllowed(false);
		tblResults.setShowGrid(false);
		tblResults.getColumnModel().getColumn(0).setPreferredWidth(28);
		
		scrollPane.setViewportView(tblResults);
		scrollPane.getViewport().setBackground(settlersLobby.theme.backgroundColor);
		pnlWrapper.add(scrollPane);
		
		// set icon
		this.settlersLobby.getGUI().setIcon(this);
		
		// add wrapper to frame
		this.add(pnlWrapper);
		
		// shrink the size of the window to the only necessary size
		this.pack();
		
		// show the dialog
		this.setVisible(true);
		
		// center this window
		this.setLocationRelativeTo(settlersLobby.getGUI().getMainWindow());
	}
	
	/**
	 * Get the tableModel e.g. to add rows
	 * 
	 * @return DefaultTableModel
	 */
	public DefaultTableModel getTableModel() {
		return this.tableModel;
	}
	
	/**
	 * ActionListener which start the hole portchecker-program.
	 */
	private ActionListener actionListenerToStartPortChecker = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			startPortchecker(false);
		}
	};
	
	/**
	 * ActionListener which start the hole portchecker-program.
	 */
	private ActionListener actionListenerToStartPortCheckerMinimal = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			startPortchecker(true);
		}
	};
	
	/**
	 * Start the portchecker with parameter.
	 * 
	 * @param minimalCheck	true if minimal portcheck should be done
	 */
	@SuppressWarnings("resource")
	private void startPortchecker( boolean minimalCheck ) {
		// get the install directory of aLobby
		String installDir = System.getProperty("user.dir");
		
		// do not run the portchecker if s3 is already running
		if( !(settlersLobby.getOpenHostGame() == null && settlersLobby.getGameStarter().isReady() && settlersLobby.getOpenJoinGame() == null ) ) {
			// show hint that s3 is running
        	MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
        	// -> add message
    		messagewindow.addMessage(
    			"<html>" + I18n.getString("PortcheckerWindow.WARNING_S3_RUNNING") + "</html>", 
    			UIManager.getIcon("OptionPane.errorIcon")
    		);
    		// -> add ok-button
    		messagewindow.addButton(
    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
    			new ActionListener() {
    			    @Override
    			    public void actionPerformed(ActionEvent e) {
    			    	messagewindow.dispose();
    			    }
    			}
    		);
    		// show it
    		messagewindow.setVisible(true);
			return;
		}
		
		// do not run the portchecker if aLobby uses the LAN-mode
		if( settlersLobby.config.isLocalMode() ) {
			// show hint that s3 is running
        	MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
        	// -> add message
    		messagewindow.addMessage(
    			"<html>" + I18n.getString("PortcheckerWindow.WARNING_LAN_MODE") + "</html>", 
    			UIManager.getIcon("OptionPane.errorIcon")
    		);
    		// -> add ok-button
    		messagewindow.addButton(
    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
    			new ActionListener() {
    			    @Override
    			    public void actionPerformed(ActionEvent e) {
    			    	messagewindow.dispose();
    			    }
    			}
    		);
    		// show it
    		messagewindow.setVisible(true);
			return;
		}
		
		// do not run the portchecker if user is not connected
		if( false == settlersLobby.isConnectedWithServer() ) {
			// show hint that s3 is running
        	MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
        	// -> add message
    		messagewindow.addMessage(
    			"<html>" + I18n.getString("PortcheckerWindow.WARNING_NOT_CONNECTED") + "</html>", 
    			UIManager.getIcon("OptionPane.errorIcon")
    		);
    		// -> add ok-button
    		messagewindow.addButton(
    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
    			new ActionListener() {
    			    @Override
    			    public void actionPerformed(ActionEvent e) {
    			    	messagewindow.dispose();
    			    }
    			}
    		);
    		// show it
    		messagewindow.setVisible(true);
			return;
		}
		
		File portcheckerExe = new File(installDir, "portchecker.exe");
		// do not run the portchecker if portchecker.exe is not available
		if( !portcheckerExe.exists() ) {
			// show hint that s3 is running
        	MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
        	// -> add message
    		messagewindow.addMessage(
    			"<html>" + I18n.getString("PortcheckerWindow.WARNING_PORTCHECKER_EXE_NOT_FOUND") + "</html>", 
    			UIManager.getIcon("OptionPane.errorIcon")
    		);
    		// -> add ok-button
    		messagewindow.addButton(
    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
    			new ActionListener() {
    			    @Override
    			    public void actionPerformed(ActionEvent e) {
    			    	messagewindow.dispose();
    			    }
    			}
    		);
    		// show it
    		messagewindow.setVisible(true);
			return;
		}
		
		String s3Exe = settlersLobby.config.getGamePathS3();
		if( s3Exe != null ) {
			
			// clear table
			if (getTableModel().getRowCount() > 0) {
			    for (int i = getTableModel().getRowCount() - 1; i > -1; i--) {
			    	getTableModel().removeRow(i);
			    }
			}
			
			// step 1: copy jre-directory from aLobby- to s3-dir
			// if it does not exists there
			File jreDirectoryDest = new File(new File(s3Exe).getParent(), "jre");
			File jreDirectorySrc = new File(new File(installDir).getAbsolutePath(), "jre");
			if( false == jreDirectoryDest.exists() && false != jreDirectorySrc.exists() ) {
				try {
					copyFileOrFolder(jreDirectorySrc, jreDirectoryDest);
				} catch (IOException e) {
					Test.outputException(e);
				}
			}
			
			// step 2: secure s3_alobby.exe by renaming								
			File originalFile = new File(s3Exe);
			File fileSicherung = new File(s3Exe + ".original");
			originalFile.renameTo(fileSicherung);
			
			// step 3: copy portchecker.exe from alobby-install-dir to s3_alobby.exe
	        FileChannel inChannel = null;
	        FileChannel outChannel = null;
			try {
				inChannel = new FileInputStream(portcheckerExe).getChannel();
				outChannel = new FileOutputStream(originalFile).getChannel();
				inChannel.transferTo(0, inChannel.size(), outChannel);
			} catch (FileNotFoundException e) {
				Test.outputException(e);
			} catch (IOException e) {
				Test.outputException(e);
			}
			finally {
				try {
					inChannel.close();
					outChannel.close();
				} catch (IOException e) {
					Test.outputException(e);
				}
			}
            
			// step 4: call the new s3_alobby.exe with arguments in new thread (to make it independent from aLobby)
			// and process the results
			new Thread() {
	            @Override
	            public void run()
	            {
					try {
						PortcheckerStarter portcheckerStarter = new PortcheckerStarter(settlersLobby, s3Exe, portcheckerWindow, minimalCheck);
						portcheckerStarter.setName("portcheckerStarter");
	            		portcheckerStarter.start();
					} catch (IOException e) {
						Test.outputException(e);
					}
	            }
			}.start();

		}
	}
	
	/**
	 * Copy a complete folder from src to dest.
	 * 
	 * @param src
	 * @param dest
	 * @throws IOException
	 */
	private void copyFileOrFolder(File source, File dest, CopyOption...  options) throws IOException {
	    if (source.isDirectory())
	        copyFolder(source, dest, options);
	    else {
	        ensureParentFolder(dest);
	        copyFile(source, dest, options);
	    }
	}

	private void copyFolder(File source, File dest, CopyOption... options) throws IOException {
	    if (!dest.exists())
	        dest.mkdirs();
	    File[] contents = source.listFiles();
	    if (contents != null) {
	        for (File f : contents) {
	            File newFile = new File(dest.getAbsolutePath() + File.separator + f.getName());
	            if (f.isDirectory())
	                copyFolder(f, newFile, options);
	            else
	                copyFile(f, newFile, options);
	        }
	    }
	}

	private void copyFile(File source, File dest, CopyOption... options) throws IOException {
	    Files.copy(source.toPath(), dest.toPath(), options);
	}

	private void ensureParentFolder(File file) {
	    File parent = file.getParentFile();
	    if (parent != null && !parent.exists())
	        parent.mkdirs();
	} 
	
}
