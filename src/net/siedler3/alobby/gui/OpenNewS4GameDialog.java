/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.MessageFormat;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.ISMap;
import net.siedler3.alobby.controlcenter.OpenGame;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.controlcenter.OpenGame.MiscType;
import net.siedler3.alobby.gui.additional.IntegerPlainDocument;
import net.siedler3.alobby.gui.additional.MouseWheelListenerForJTabbedPane;
import net.siedler3.alobby.gui.additional.TableCellCenterRenderer;
import net.siedler3.alobby.gui.additional.addCursorChangesToButton;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.util.httpRequests;
import net.siedler3.alobby.util.s4support.GoodsInStock;
import net.siedler3.alobby.util.s4support.S4Map;
import net.siedler3.alobby.util.s4support.S4Support;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.Component;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicTabbedPaneUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;

import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.JTable;
import javax.swing.JCheckBox;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

import org.eclipse.wb.swing.FocusTraversalOnArray;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTextPane;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;

/**
 * Create the S4-OpenGameDialog as JPanel which will be insert into the OpenNewGameDialog-JFrame.
 * Includes the following options for user who will create a new S4-game:
 * - define a game-name (visible in aLobby)
 * - choose the goods
 * - ?
 * 
 * @author Zwirni
 *
 */
public class OpenNewS4GameDialog extends JPanel {
	
	private SettlersLobby settlersLobby;
	private JTextField gamenameTextfield;
	private JTable multiplayerTable;
	private JTable usermapsTable;
	private JTable savegameTable;
	private JTextField searchTextField;
	private JTextField searchTextFieldUserMap;
	private JTable favoritesTable;
	private JFrame parentFrame;
	private JPanel gamenamePane;
	private JTable mapbaseResultsTable;
	// disable this line for using windowbuilder in eclipse
	private JComboBox<GoodsInStock> goodsInStock = new JComboBox<GoodsInStock>(GoodsInStock.values());
	// enable disable this line for using windowbuilder in eclipse
	//private JComboBox<GoodsInStock> goodsInStock;
	private JTabbedPane tabbedPane;
	private OpenNewGameDialog openNewGameDialog;
	private JCheckBox chckBxWiMo;
	private JCheckBox chckBxTrojans;
	private JLabel lblPlayers;
	private JLabel lblTeams;
	private JTextField txtPlayers;
	private JTextField txtTeams;
	private JPanel pnlPlayers;
	private JPanel pnlTeams;
	private JButton searchBtn;
	private String chosenMapName;
	private JScrollPane mapbaseResultsScrollPane;
	private JScrollPane favoritesScrollPane;
	private JScrollPane multiplayerScrollPane;
	private JScrollPane usermapsScrollPane;
	private JScrollPane savegameScrollPane;
	private JScrollPane randomScrollPane;
	private ArrayList<JTable> tableList;
	private JPanel pnlCreator;
	private JButton btnStartGame;
	private JButton btnCancel;
	private JPanel previewPanel;
	private JTextPane txtMapSize;
	private JTextPane txtMapDate;
	private JTextPane txtMapPlayer;
	private JPanel pnlLink;
	private JPanel pnlRating;
	private JPanel pnlPreviewImage;
	private int defaultWindowHeight = 400;
	private JPanel randomForm;
	private JComboBox<String> randomMapSize;
	private JComboBox<String> randomLandmass;
	private JTextField randomCodeField;
	private JComboBox<String> randomAxes;
	private JComboBox<String> randomMinerals;
	private JCheckBox chckBxCoop;
		
	// initialize the object
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public OpenNewS4GameDialog( SettlersLobby settlersLobby, OpenNewGameDialog openNewGameDialog) {
		
		/** 
		 * This dialog-window was created with Windows Builder.
		 * After install Window Builder right-click on this file 
		 * and choose Open With > Window Builder to edit this settings.
		 */
		
		// set the size for the form
		setSize(new Dimension(530, 360));
		setPreferredSize(new Dimension(640, 400));
		setMinimumSize(new Dimension(530, 360));
		setLocale(Locale.GERMANY);
		setAutoscrolls(true);
		setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		
		this.settlersLobby = settlersLobby;
		this.openNewGameDialog = openNewGameDialog;
		
		// set properties for this panel
		this.setAlignmentX(LEFT_ALIGNMENT);
		this.setAlignmentY(TOP_ALIGNMENT);
		
		gamenamePane = new JPanel();
		gamenamePane.setAlignmentX(0.0f);
		gamenamePane.setAlignmentY(Component.TOP_ALIGNMENT);
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.addMouseWheelListener(new MouseWheelListenerForJTabbedPane());
		
		JPanel optionsPanel = new JPanel();
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		
		btnStartGame = new JButton(I18n.getString("OpenNewS4GameDialog.BUTTON_STARTGAME"));
		btnCancel = new JButton(I18n.getString("OpenNewS4GameDialog.BUTTON_CANCEL"));
		// add repeating backgroundImage to the buttons, if defined for actual theme
		if( settlersLobby.theme.isRepeatingButtonStyleAvailable() ) {
			btnStartGame = settlersLobby.theme.getButtonWithStyle(btnStartGame.getText());
			btnCancel = settlersLobby.theme.getButtonWithStyle(btnCancel.getText());
		}
		
		// add specific backgroundImage to the buttons, if defined for actual theme
		// -> all together for consistent view
		if( settlersLobby.theme.isButtonStyleAvailable() ) {
			btnStartGame = settlersLobby.theme.getButtonWithStyle(btnStartGame.getText(), settlersLobby.theme.buttonBackgroundImage1, settlersLobby.theme.buttonBackgroundImage1Hover);
			btnCancel = settlersLobby.theme.getButtonWithStyle(btnCancel.getText(), settlersLobby.theme.buttonBackgroundImage1, settlersLobby.theme.buttonBackgroundImage1Hover);
		}
		btnStartGame.addActionListener(actionListenerToCreateGame);
		btnCancel.addActionListener(actionListenerToCloseNewGameDialog);
		
		previewPanel = new JPanel();
		previewPanel.setVisible(false);
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(previewPanel, GroupLayout.PREFERRED_SIZE, 612, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, 612, Short.MAX_VALUE))
						.addComponent(gamenamePane, GroupLayout.DEFAULT_SIZE, 612, Short.MAX_VALUE))
					.addGap(20))
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(optionsPanel, 0, 0, Short.MAX_VALUE)
					.addGap(22))
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(buttonPanel, GroupLayout.PREFERRED_SIZE, 608, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(22, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(gamenamePane, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, 239, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(previewPanel, GroupLayout.PREFERRED_SIZE, 263, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(optionsPanel, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(buttonPanel, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(181, Short.MAX_VALUE))
		);
		
		JLabel lblMapSize = new JLabel(I18n.getString("OpenNewS4GameDialog.MAPSIZE"));
		
		txtMapSize = new JTextPane();
		txtMapSize.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		txtMapSize.setAutoscrolls(false);
		
		JLabel lblMapPlayers = new JLabel(I18n.getString("OpenNewS4GameDialog.PLAYERS"));
		lblMapPlayers.setAlignmentY(Component.TOP_ALIGNMENT);
		
		txtMapPlayer = new JTextPane();
		txtMapPlayer.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		txtMapPlayer.setAutoscrolls(false);
		
		JLabel lblMapCreator = new JLabel(I18n.getString("OpenNewS4GameDialog.MAPCREATOR"));
		
		JLabel lblMapDate = new JLabel(I18n.getString("OpenNewS4GameDialog.DATE"));
		
		txtMapDate = new JTextPane();
		txtMapDate.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		txtMapDate.setAutoscrolls(false);
		txtMapDate.setFocusable(false);
		
		JLabel lblRating = new JLabel(I18n.getString("OpenNewS4GameDialog.RATING"));
		
		pnlRating = new JPanel();
		pnlRating.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		
		pnlPreviewImage = new JPanel();
		// add the mouse-listener to the image-preview-panel
    	// -> if user clicks on it, the detail-view of the map (in new window) will be displayed
    	pnlPreviewImage.addMouseListener(actionListenerZoomPreview);
    	pnlPreviewImage.addMouseListener(new addCursorChangesToButton());
		
		JLabel lblLink = new JLabel(I18n.getString("OpenNewS4GameDialog.LINK_TO_MAPBASE"));
		
		pnlLink = new JPanel();
		
		pnlCreator = new JPanel();
		pnlCreator.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		
		GroupLayout gl_previewPanel = new GroupLayout(previewPanel);
		gl_previewPanel.setHorizontalGroup(
			gl_previewPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_previewPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_previewPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_previewPanel.createSequentialGroup()
							.addComponent(pnlLink, GroupLayout.DEFAULT_SIZE, 592, Short.MAX_VALUE)
							.addContainerGap())
						.addGroup(gl_previewPanel.createSequentialGroup()
							.addGroup(gl_previewPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(lblMapCreator, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblMapPlayers, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblMapSize, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblMapDate, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblRating, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_previewPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(pnlRating, GroupLayout.DEFAULT_SIZE, 186, Short.MAX_VALUE)
								.addComponent(pnlCreator, GroupLayout.DEFAULT_SIZE, 186, Short.MAX_VALUE)
								.addComponent(txtMapDate, GroupLayout.DEFAULT_SIZE, 186, Short.MAX_VALUE)
								.addComponent(txtMapSize, GroupLayout.DEFAULT_SIZE, 186, Short.MAX_VALUE)
								.addComponent(txtMapPlayer, GroupLayout.DEFAULT_SIZE, 186, Short.MAX_VALUE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(pnlPreviewImage, GroupLayout.PREFERRED_SIZE, 294, GroupLayout.PREFERRED_SIZE)
							.addContainerGap())
						.addGroup(gl_previewPanel.createSequentialGroup()
							.addComponent(lblLink, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addGap(520))))
		);
		gl_previewPanel.setVerticalGroup(
			gl_previewPanel.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, gl_previewPanel.createSequentialGroup()
					.addGroup(gl_previewPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_previewPanel.createSequentialGroup()
							.addGroup(gl_previewPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(lblMapSize, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtMapSize, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_previewPanel.createParallelGroup(Alignment.TRAILING)
								.addComponent(lblMapPlayers, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtMapPlayer, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
							.addGap(6)
							.addGroup(gl_previewPanel.createParallelGroup(Alignment.LEADING, false)
								.addComponent(pnlCreator, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(lblMapCreator, GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_previewPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(lblMapDate, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtMapDate, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_previewPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(lblRating, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
								.addComponent(pnlRating, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_previewPanel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_previewPanel.createSequentialGroup()
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addGap(26))
								.addGroup(Alignment.TRAILING, gl_previewPanel.createSequentialGroup()
									.addPreferredGap(ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
									.addComponent(lblLink, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)))
							.addPreferredGap(ComponentPlacement.RELATED))
						.addGroup(gl_previewPanel.createSequentialGroup()
							.addComponent(pnlPreviewImage, GroupLayout.PREFERRED_SIZE, 205, GroupLayout.PREFERRED_SIZE)
							.addGap(20)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(pnlLink, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		GroupLayout gl_pnlRating = new GroupLayout(pnlRating);
		gl_pnlRating.setHorizontalGroup(
			gl_pnlRating.createParallelGroup(Alignment.LEADING)
				.addGap(0, 184, Short.MAX_VALUE)
		);
		gl_pnlRating.setVerticalGroup(
			gl_pnlRating.createParallelGroup(Alignment.LEADING)
				.addGap(0, 29, Short.MAX_VALUE)
		);
		pnlRating.setLayout(gl_pnlRating);
		GroupLayout gl_pnlCreator = new GroupLayout(pnlCreator);
		gl_pnlCreator.setHorizontalGroup(
			gl_pnlCreator.createParallelGroup(Alignment.LEADING)
				.addGap(0, 184, Short.MAX_VALUE)
		);
		gl_pnlCreator.setVerticalGroup(
			gl_pnlCreator.createParallelGroup(Alignment.LEADING)
				.addGap(0, 29, Short.MAX_VALUE)
		);
		pnlCreator.setLayout(gl_pnlCreator);
		GroupLayout gl_pnlLink = new GroupLayout(pnlLink);
		gl_pnlLink.setHorizontalGroup(
			gl_pnlLink.createParallelGroup(Alignment.LEADING)
				.addGap(0, 372, Short.MAX_VALUE)
		);
		gl_pnlLink.setVerticalGroup(
			gl_pnlLink.createParallelGroup(Alignment.LEADING)
				.addGap(0, 29, Short.MAX_VALUE)
		);
		pnlLink.setLayout(gl_pnlLink);
		previewPanel.setLayout(gl_previewPanel);
		groupLayout.setAutoCreateContainerGaps(true);
				
		GroupLayout gl_buttonPanel = new GroupLayout(buttonPanel);
		gl_buttonPanel.setHorizontalGroup(
			gl_buttonPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_buttonPanel.createSequentialGroup()
					.addComponent(btnStartGame, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE)
					.addGap(14)
					.addComponent(btnCancel, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		gl_buttonPanel.setVerticalGroup(
			gl_buttonPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_buttonPanel.createSequentialGroup()
					.addGroup(gl_buttonPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(btnStartGame, GroupLayout.DEFAULT_SIZE, 24, Short.MAX_VALUE)
						.addComponent(btnCancel))
					.addContainerGap())
		);
		buttonPanel.setLayout(gl_buttonPanel);
		buttonPanel.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{btnStartGame, btnCancel}));
		
		lblPlayers = new JLabel(I18n.getString("OpenNewS4GameDialog.GAME_PLAYERCOUNT"));
		lblTeams = new JLabel(I18n.getString("OpenNewS4GameDialog.GAME_TEAMCOUNT"));
		txtPlayers = new JTextField();
		txtPlayers.setDocument(new IntegerPlainDocument());
		txtPlayers.setPreferredSize( new Dimension( 32, 24 ) );
		txtPlayers.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		txtPlayers.setAutoscrolls(false);
		txtPlayers.setHorizontalAlignment(SwingConstants.LEFT);
		txtPlayers.setColumns(2);
		txtPlayers.setBorder(BorderFactory.createCompoundBorder(txtPlayers.getBorder(), BorderFactory.createEmptyBorder(3, 3, 2, 3)));
		txtPlayers.setText(Integer.toString(settlersLobby.config.getnewS4GamePlayers()));
		txtTeams = new JTextField();
		txtTeams.setDocument(new IntegerPlainDocument());
		txtTeams.setPreferredSize( new Dimension( 32, 24 ) );
		txtTeams.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		txtTeams.setAutoscrolls(false);
		txtTeams.setHorizontalAlignment(SwingConstants.LEFT);
		txtTeams.setColumns(2);
		txtTeams.setBorder(BorderFactory.createCompoundBorder(txtTeams.getBorder(), BorderFactory.createEmptyBorder(3, 3, 2, 3)));
		txtTeams.setText(Integer.toString(settlersLobby.config.getnewS4GameTeams()));
		pnlPlayers = new JPanel();
		pnlPlayers.add(lblPlayers);
		pnlPlayers.add(txtPlayers);
		pnlTeams = new JPanel();
		pnlTeams.add(lblTeams);
		pnlTeams.add(txtTeams);
		
		chckBxWiMo = new JCheckBox(I18n.getString("OpenNewS4GameDialog.WIMO"));
		chckBxWiMo.setToolTipText(I18n.getString("OpenNewS4GameDialog.WIMO_TOOLTIP"));
		chckBxWiMo.setSelected(settlersLobby.config.getnewS4GameWimo());
		chckBxWiMo.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				setStateOfCoop();
			}
		});

		chckBxTrojans = new JCheckBox(I18n.getString("OpenNewS4GameDialog.TROJANS"));
		chckBxTrojans.setToolTipText(I18n.getString("OpenNewS4GameDialog.TROJANS_TOOLTIP"));
		chckBxTrojans.setSelected(settlersLobby.config.getnewS4GameTrojans());
		
		chckBxCoop = new JCheckBox(I18n.getString("OpenNewS4GameDialog.COOP"));
		chckBxCoop.setToolTipText(I18n.getString("OpenNewS4GameDialog.COOP_TOOLTIP"));
		chckBxCoop.setSelected(settlersLobby.config.getnewS4GameCoop());
		chckBxCoop.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				setStateOfWimo();
			}
		});
		
		setStateOfCoop();
		setStateOfWimo();

		GroupLayout gl_optionsPanel = new GroupLayout(optionsPanel);
		gl_optionsPanel.setHorizontalGroup(
			gl_optionsPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_optionsPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(pnlPlayers, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(pnlTeams, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(chckBxWiMo, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(chckBxTrojans, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(chckBxCoop, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
				)
		);
		gl_optionsPanel.setVerticalGroup(
			gl_optionsPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_optionsPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_optionsPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(pnlPlayers)
						.addComponent(pnlTeams)
						.addComponent(chckBxWiMo)
						.addComponent(chckBxTrojans)
						.addComponent(chckBxCoop)
					)
					.addContainerGap(9, Short.MAX_VALUE))
		);
		optionsPanel.setLayout(gl_optionsPanel);
		
		JPanel favoritesPane = new JPanel();
		favoritesPane.setPreferredSize(new Dimension(450, 400));
		favoritesPane.setName("favoriteS4Maps");
		favoritesPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		tabbedPane.addTab(I18n.getString("OpenNewS4GameDialog.TAB_FAVORITES"), null, favoritesPane, null);
		
		favoritesScrollPane = new JScrollPane();
		favoritesScrollPane.setName("");
		favoritesScrollPane.setAlignmentY(Component.TOP_ALIGNMENT);
		favoritesScrollPane.setAlignmentX(Component.LEFT_ALIGNMENT);
		favoritesScrollPane.setInheritsPopupMenu(true);
		favoritesScrollPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		
		favoritesTable = new JTable();
		favoritesTable.addMouseListener(mouseAdapterToCreateGame);
		favoritesTable.setName("favoritesTable");
		favoritesTable.setAutoCreateRowSorter(true);
		favoritesTable.getTableHeader().setReorderingAllowed(false);
		favoritesTable.getTableHeader().setResizingAllowed(false);
		favoritesTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		favoritesTable.setModel(new OpenNewGameDialogTableModel(
			new Object[][] {
			},
			new Object[] {
				I18n.getString("OpenNewS4GameDialog.TAB_ROW_MAP")
			}
			
		));
		favoritesTable.getColumnModel().getColumn(0).setResizable(false);
		favoritesTable.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		favoritesScrollPane.setViewportView(favoritesTable);
		GroupLayout gl_favoritesPane = new GroupLayout(favoritesPane);
		gl_favoritesPane.setHorizontalGroup(
			gl_favoritesPane.createParallelGroup(Alignment.LEADING)
				.addComponent(favoritesScrollPane, GroupLayout.DEFAULT_SIZE, 605, Short.MAX_VALUE)
		);
		gl_favoritesPane.setVerticalGroup(
			gl_favoritesPane.createParallelGroup(Alignment.LEADING)
				.addComponent(favoritesScrollPane, GroupLayout.DEFAULT_SIZE, 211, Short.MAX_VALUE)
		);
		favoritesPane.setLayout(gl_favoritesPane);
		
		JPanel mapbasePane = new JPanel();
		mapbasePane.setPreferredSize(new Dimension(450, 400));
		mapbasePane.setName("S4mapbase");
		mapbasePane.setAlignmentY(Component.TOP_ALIGNMENT);
		mapbasePane.setAlignmentX(Component.LEFT_ALIGNMENT);
		// TODO disabled until MapBase supports S4-Maps
		//tabbedPane.addTab(I18n.getString("OpenNewS4GameDialog.TAB_MAPBASE"), null, mapbasePane, null);
		
		JPanel searchPane = new JPanel();
		
		mapbaseResultsScrollPane = new JScrollPane();
		mapbaseResultsScrollPane.setName("mapbaseTable");
		mapbaseResultsScrollPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		mapbaseResultsScrollPane.setAlignmentY(Component.TOP_ALIGNMENT);
		mapbaseResultsScrollPane.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		mapbasePane.add(mapbaseResultsScrollPane);
		
		GroupLayout gl_mapbasePane = new GroupLayout(mapbasePane);
		gl_mapbasePane.setHorizontalGroup(
			gl_mapbasePane.createParallelGroup(Alignment.LEADING)
				.addComponent(searchPane, GroupLayout.DEFAULT_SIZE, 605, Short.MAX_VALUE)
				.addComponent(mapbaseResultsScrollPane, GroupLayout.DEFAULT_SIZE, 605, Short.MAX_VALUE)
		);
		gl_mapbasePane.setVerticalGroup(
			gl_mapbasePane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_mapbasePane.createSequentialGroup()
					.addComponent(searchPane, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(mapbaseResultsScrollPane, GroupLayout.DEFAULT_SIZE, 127, Short.MAX_VALUE))
		);
		
		mapbaseResultsTable = new JTable();
		mapbaseResultsTable.setName("mapbaseTable");
		mapbaseResultsTable.addMouseListener(mouseAdapterToCreateGame);
		mapbaseResultsTable.setAutoCreateRowSorter(true);
		mapbaseResultsTable.getTableHeader().setReorderingAllowed(false);
		mapbaseResultsTable.getTableHeader().setResizingAllowed(false);
		mapbaseResultsTable.setModel(new OpenNewGameDialogTableModel(
			new Object[][] {
			},
			new Object[] {
				I18n.getString("OpenNewS4GameDialog.TAB_ROW_MAP"), 
				I18n.getString("OpenNewS4GameDialog.TAB_ROW_PLAYER"), 
				I18n.getString("OpenNewS4GameDialog.TAB_ROW_MAPSIZE")
			}
		));
		mapbaseResultsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		mapbaseResultsScrollPane.setViewportView(mapbaseResultsTable);
		searchPane.setLayout(new BorderLayout(0, 0));
		
		searchTextField = new JTextField();
		searchTextField.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		searchTextField.setAutoscrolls(false);
		searchTextField.setHorizontalAlignment(SwingConstants.LEFT);
		searchTextField.setText(I18n.getString("OpenNewS4GameDialog.MAPBASE_INPUT_PLACEHOLDER"));
		searchTextField.setFont(new Font("Tahoma", Font.ITALIC, settlersLobby.config.getDpiAwareFontSize()));
		searchPane.add(searchTextField);
		
		searchTextFieldUserMap = new JTextField();
		searchTextFieldUserMap.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		searchTextFieldUserMap.setAutoscrolls(false);
		searchTextFieldUserMap.setHorizontalAlignment(SwingConstants.LEFT);
		searchTextFieldUserMap.setText(I18n.getString("OpenNewS4GameDialog.MAPBASE_INPUT_PLACEHOLDER"));
		searchTextFieldUserMap.setFont(new Font("Tahoma", Font.ITALIC, settlersLobby.config.getDpiAwareFontSize()));
		searchTextFieldUserMap.setColumns(10);
		
		searchBtn = new JButton(I18n.getString("OpenNewS4GameDialog.BUTTON_SEARCH"));
		searchPane.add(searchBtn, BorderLayout.EAST);
		
		mapbasePane.setLayout(gl_mapbasePane);
		
		JPanel multiplayerPane = new JPanel();
		multiplayerPane.setPreferredSize(new Dimension(450, 400));
		multiplayerPane.setName("multiplayer");
		multiplayerPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		tabbedPane.addTab(I18n.getString("OpenNewS4GameDialog.TAB_CDMAPS"), null, multiplayerPane, null);
		
		multiplayerScrollPane = new JScrollPane();
		multiplayerScrollPane.setName("");
		multiplayerScrollPane.setInheritsPopupMenu(true);
		multiplayerScrollPane.setAlignmentY(Component.TOP_ALIGNMENT);
		multiplayerScrollPane.setAlignmentX(Component.LEFT_ALIGNMENT);
		multiplayerScrollPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		
		multiplayerTable = new JTable();
		multiplayerTable.setName("multiplayerTable");
		multiplayerTable.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		multiplayerTable.setAutoCreateRowSorter(true);
		multiplayerTable.getTableHeader().setReorderingAllowed(false);
		multiplayerTable.getTableHeader().setResizingAllowed(false);
		multiplayerTable.setModel(new OpenNewGameDialogTableModel(
			new Object[][] {
			},
			new Object[] {
				I18n.getString("OpenNewS4GameDialog.TAB_ROW_MAP")
			}
		));
		multiplayerTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		multiplayerTable.addMouseListener(mouseAdapterToCreateGame);
		multiplayerScrollPane.setViewportView(multiplayerTable);
				
		GroupLayout gl_multiplayerPane = new GroupLayout(multiplayerPane);
		gl_multiplayerPane.setHorizontalGroup(
			gl_multiplayerPane.createParallelGroup(Alignment.TRAILING)
				.addComponent(multiplayerScrollPane, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 605, Short.MAX_VALUE)
		);
		gl_multiplayerPane.setVerticalGroup(
			gl_multiplayerPane.createParallelGroup(Alignment.LEADING)
				.addComponent(multiplayerScrollPane, GroupLayout.DEFAULT_SIZE, 211, Short.MAX_VALUE)
		);
		multiplayerPane.setLayout(gl_multiplayerPane);
		
		usermapsScrollPane = new JScrollPane();
		usermapsScrollPane.setName("");
		usermapsScrollPane.setInheritsPopupMenu(true);
		usermapsScrollPane.setAlignmentY(Component.TOP_ALIGNMENT);
		usermapsScrollPane.setAlignmentX(Component.LEFT_ALIGNMENT);
		usermapsScrollPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
				
		JPanel usermapsPane = new JPanel();
		usermapsPane.setPreferredSize(new Dimension(450, 400));
		usermapsPane.setName("usermaps");
		usermapsPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		tabbedPane.addTab(I18n.getString("OpenNewS4GameDialog.TAB_USERMAPS"), null, usermapsPane, null);
				
		usermapsTable = new JTable();
		usermapsTable.setName("usermapsTable");
		usermapsTable.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		usermapsTable.setAutoCreateRowSorter(true);
		usermapsTable.getTableHeader().setReorderingAllowed(false);
		usermapsTable.getTableHeader().setResizingAllowed(false);
		usermapsTable.setModel(new OpenNewGameDialogTableModel(
			new Object[][] {
			},
			new Object[] {
				I18n.getString("OpenNewS4GameDialog.TAB_ROW_MAP")
			}
		));
		usermapsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		usermapsTable.addMouseListener(mouseAdapterToCreateGame);
		usermapsScrollPane.setViewportView(usermapsTable);
		GroupLayout gl_usermapsPane = new GroupLayout(usermapsPane);
		gl_usermapsPane.setHorizontalGroup(
				gl_usermapsPane.createParallelGroup(Alignment.TRAILING)
				.addComponent(usermapsScrollPane, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 605, Short.MAX_VALUE)
		);
		gl_usermapsPane.setVerticalGroup(
				gl_usermapsPane.createParallelGroup(Alignment.LEADING)
				.addComponent(usermapsScrollPane, GroupLayout.DEFAULT_SIZE, 211, Short.MAX_VALUE)
		);
		usermapsPane.setLayout(gl_usermapsPane);
				
		randomScrollPane = new JScrollPane();
		randomScrollPane.setName("");
		randomScrollPane.setInheritsPopupMenu(true);
		randomScrollPane.setAlignmentY(Component.TOP_ALIGNMENT);
		randomScrollPane.setAlignmentX(Component.LEFT_ALIGNMENT);
		randomScrollPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		
		JPanel randomFormPane = new JPanel();
		randomFormPane.setPreferredSize(new Dimension(450, 400));
		randomFormPane.setName("s4random");
		randomFormPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		tabbedPane.addTab(I18n.getString("OpenNewS4GameDialog.TAB_RANDOM"), null, randomFormPane, null);
		
		// form for random-configuration
		randomForm = new JPanel();
		randomForm.setName("s4randomform");

		// set JLabel-sizes for the following components
		Dimension jLblSizes = new Dimension(120, 24);
		
		// set sizes for the other components
		Dimension compSizes = new Dimension(180, 24);
				
		// add components to this form which are necessary to generate the random-map-string
		// -> mapsize
		ArrayList<String> randomMapSizes = new ArrayList<String>();
		randomMapSizes.add("256x256");
		randomMapSizes.add("320x320");
		randomMapSizes.add("384x384");
		randomMapSizes.add("448x448");
		randomMapSizes.add("512x512");
		randomMapSizes.add("576x576");
		randomMapSizes.add("640x640");
		randomMapSizes.add("704x740");
		randomMapSizes.add("768x768");
		randomMapSizes.add("832x832");
		randomMapSizes.add("896x896");
		randomMapSizes.add("960x960");
		randomMapSizes.add("1024x1024");
		randomMapSize  = new JComboBox(randomMapSizes.toArray());
		randomMapSize.setMinimumSize(compSizes);
		randomMapSize.setPreferredSize(compSizes);
		randomMapSize.setMaximumSize(compSizes);
		randomMapSize.setSelectedIndex(settlersLobby.config.getnewS4GameRandomMapSize());
		// TODO disable until we know how to calculate the random-code-string
		randomMapSize.setEnabled(false);
		JLabel lblRandomMapSize = new JLabel(I18n.getString("OpenNewS4GameDialog.RANDOM_MAPSIZE"));
		lblRandomMapSize.setMinimumSize(jLblSizes);
		lblRandomMapSize.setPreferredSize(jLblSizes);
		lblRandomMapSize.setMaximumSize(jLblSizes);
		JPanel pnlRandomMapSize = new JPanel();
		pnlRandomMapSize.add(lblRandomMapSize);
		pnlRandomMapSize.add(randomMapSize);
				
		// -> landmass
		ArrayList<String> randomLandmasses = new ArrayList<String>();
		randomLandmasses.add("10%");
		randomLandmasses.add("20%");
		randomLandmasses.add("30%");
		randomLandmasses.add("40%");
		randomLandmasses.add("50%");
		randomLandmasses.add("60%");
		randomLandmasses.add("70%");
		randomLandmasses.add("80%");
		randomLandmasses.add("90%");
		randomLandmass  = new JComboBox(randomLandmasses.toArray());
		randomLandmass.setMinimumSize(compSizes);
		randomLandmass.setPreferredSize(compSizes);
		randomLandmass.setMaximumSize(compSizes);
		randomLandmass.setSelectedIndex(settlersLobby.config.getnewS4GameRandomLandmass());
		// TODO disable until we know how to calculate the random-code-string
		randomLandmass.setEnabled(false);
		JLabel lblRandomLandmass = new JLabel(I18n.getString("OpenNewS4GameDialog.RANDOM_LANDMASS"));
		lblRandomLandmass.setMinimumSize(jLblSizes);
		lblRandomLandmass.setPreferredSize(jLblSizes);
		lblRandomLandmass.setMaximumSize(jLblSizes);
		JPanel pnlRandomLandmass = new JPanel();
		pnlRandomLandmass.add(lblRandomLandmass);
		pnlRandomLandmass.add(randomLandmass);
				
		// -> random-code-field
		randomCodeField = new JTextField();
		randomCodeField.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		randomCodeField.setAutoscrolls(false);
		randomCodeField.setHorizontalAlignment(SwingConstants.LEFT);
		randomCodeField.setText(settlersLobby.config.getnewS4GameRandomCode());
		randomCodeField.setFont(new Font("Tahoma", Font.PLAIN, settlersLobby.config.getDpiAwareFontSize()));
		randomCodeField.setColumns(10);
		randomCodeField.setMinimumSize(compSizes);
		randomCodeField.setPreferredSize(compSizes);
		randomCodeField.setMaximumSize(compSizes);
		JLabel lblRandomCodeField = new JLabel(I18n.getString("OpenNewS4GameDialog.RANDOM_CODE"));
		lblRandomCodeField.setMinimumSize(jLblSizes);
		lblRandomCodeField.setPreferredSize(jLblSizes);
		lblRandomCodeField.setMaximumSize(jLblSizes);
		JPanel pnlRandomCodeField = new JPanel();
		pnlRandomCodeField.add(lblRandomCodeField);
		pnlRandomCodeField.add(randomCodeField);
		
		// -> axes
		ArrayList<String> randomAxesValues = new ArrayList<String>();
		randomAxesValues.add(I18n.getString("OpenNewS4GameDialog.RANDOM_NO_AXES"));
		randomAxesValues.add(I18n.getString("OpenNewS4GameDialog.RANDOM_SHORT_AXES"));
		randomAxesValues.add(I18n.getString("OpenNewS4GameDialog.RANDOM_LONG_AXES"));
		randomAxesValues.add(I18n.getString("OpenNewS4GameDialog.RANDOM_BOTH_AXES"));
		randomAxes  = new JComboBox(randomAxesValues.toArray());
		randomAxes.setMinimumSize(compSizes);
		randomAxes.setPreferredSize(compSizes);
		randomAxes.setMaximumSize(compSizes);
		randomAxes.setSelectedIndex(settlersLobby.config.getnewS4GameRandomAxes());
		// TODO disable until we know how to calculate the random-code-string
		randomAxes.setEnabled(false);
		JLabel lblRandomAxes = new JLabel(I18n.getString("OpenNewS4GameDialog.RANDOM_AXES"));
		lblRandomAxes.setMinimumSize(jLblSizes);
		lblRandomAxes.setPreferredSize(jLblSizes);
		lblRandomAxes.setMaximumSize(jLblSizes);
		JPanel pnlRandomAxes = new JPanel();
		pnlRandomAxes.add(lblRandomAxes);
		pnlRandomAxes.add(randomAxes);
				
		// -> mineral resources
		ArrayList<String> randomMineralsValues = new ArrayList<String>();
		randomMineralsValues.add(I18n.getString("OpenNewS4GameDialog.MINERAL_RESOURCES_LESS"));
		randomMineralsValues.add(I18n.getString("OpenNewS4GameDialog.MINERAL_RESOURCES_MEDIUM"));
		randomMineralsValues.add(I18n.getString("OpenNewS4GameDialog.MINERAL_RESOURCES_HIGH"));
		randomMinerals  = new JComboBox(randomMineralsValues.toArray());
		randomMinerals.setMinimumSize(compSizes);
		randomMinerals.setPreferredSize(compSizes);
		randomMinerals.setMaximumSize(compSizes);
		randomMinerals.setSelectedIndex(settlersLobby.config.getnewS4GameRandomMineralResources());
		// TODO disable until we know how to calculate the random-code-string
		randomMinerals.setEnabled(false);
		JLabel lblRandomMinerals = new JLabel(I18n.getString("OpenNewS4GameDialog.RANDOM_MINERAL_RESOURCES"));
		lblRandomMinerals.setMinimumSize(jLblSizes);
		lblRandomMinerals.setPreferredSize(jLblSizes);
		lblRandomMinerals.setMaximumSize(jLblSizes);
		JPanel pnlRandomMinerals = new JPanel();
		pnlRandomMinerals.add(lblRandomMinerals);
		pnlRandomMinerals.add(randomMinerals);
		
		// -> create the layout for the random-form
		GroupLayout gl_randomForm = new GroupLayout(randomForm);
		gl_randomForm.setHorizontalGroup(
			gl_randomForm.createParallelGroup(Alignment.LEADING)
        		.addGroup(gl_randomForm.createSequentialGroup()
        			.addGroup(gl_randomForm.createParallelGroup(Alignment.LEADING)
        				.addComponent(pnlRandomMapSize, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        				.addComponent(pnlRandomLandmass, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        				.addComponent(pnlRandomAxes, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        				.addComponent(pnlRandomMinerals, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        				.addComponent(pnlRandomCodeField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        			)
        			.addContainerGap())
        );
		gl_randomForm.setVerticalGroup(
    		gl_randomForm.createParallelGroup(Alignment.LEADING)
    		.addGroup(gl_randomForm.createSequentialGroup()
    			.addComponent(pnlRandomMapSize, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
    			.addGap(6)
    			.addComponent(pnlRandomLandmass, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
    			.addGap(6)
    			.addComponent(pnlRandomAxes, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
    			.addGap(6)
    			.addComponent(pnlRandomMinerals, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
    			.addGap(6)
    			.addComponent(pnlRandomCodeField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
    		)
        );
				
		// -> set the layout
		randomForm.setLayout(gl_randomForm);
		
		randomScrollPane.setViewportView(randomForm);
		GroupLayout gl_randomPane = new GroupLayout(randomFormPane);
		gl_randomPane.setHorizontalGroup(
				gl_randomPane.createParallelGroup(Alignment.TRAILING)
				.addComponent(randomScrollPane, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 605, Short.MAX_VALUE)
		);
		gl_randomPane.setVerticalGroup(
				gl_randomPane.createParallelGroup(Alignment.LEADING)
				.addComponent(randomScrollPane, GroupLayout.DEFAULT_SIZE, 211, Short.MAX_VALUE)
		);
		randomFormPane.setLayout(gl_randomPane);
		
		JPanel savegamePane = new JPanel();
		savegamePane.setPreferredSize(new Dimension(450, 400));
		savegamePane.setName("s4savegames");
		savegamePane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		tabbedPane.addTab(I18n.getString("OpenNewS4GameDialog.TAB_SAVEGAMES"), null, savegamePane, null);
		
		savegameScrollPane = new JScrollPane();
		savegameScrollPane.setName("");
		savegameScrollPane.setInheritsPopupMenu(true);
		savegameScrollPane.setAlignmentY(Component.TOP_ALIGNMENT);
		savegameScrollPane.setAlignmentX(Component.LEFT_ALIGNMENT);
		savegameScrollPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
				
		savegameTable = new JTable();
		savegameTable.setName("savegameTable");
		savegameTable.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		savegameTable.setAutoCreateRowSorter(true);
		savegameTable.getTableHeader().setReorderingAllowed(false);
		savegameTable.getTableHeader().setResizingAllowed(false);
		savegameTable.setModel(new OpenNewGameDialogTableModel(
			new Object[][] {
			},
			new Object[] {
				I18n.getString("OpenNewS4GameDialog.TAB_ROW_MAP")
			}
		));
		
		savegameTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		savegameTable.addMouseListener(mouseAdapterToCreateGame);
		savegameScrollPane.setViewportView(savegameTable);
		GroupLayout gl_savegamePane = new GroupLayout(savegamePane);
		gl_savegamePane.setHorizontalGroup(
				gl_savegamePane.createParallelGroup(Alignment.TRAILING)
				.addComponent(savegameScrollPane, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 605, Short.MAX_VALUE)
		);
		gl_savegamePane.setVerticalGroup(
				gl_savegamePane.createParallelGroup(Alignment.LEADING)
				.addComponent(savegameScrollPane, GroupLayout.DEFAULT_SIZE, 211, Short.MAX_VALUE)
		);
		savegamePane.setLayout(gl_savegamePane);
		
		gamenameTextfield = new JTextField();
		gamenameTextfield.setToolTipText(I18n.getString("OpenNewS4GameDialog.GAMENAME_TOOLTIP"));
		gamenameTextfield.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		gamenameTextfield.setAutoscrolls(false);
		gamenameTextfield.setHorizontalAlignment(SwingConstants.LEFT);
		gamenameTextfield.setColumns(10);
		gamenameTextfield.setBorder(BorderFactory.createCompoundBorder(gamenameTextfield.getBorder(), BorderFactory.createEmptyBorder(3, 3, 2, 3)));
		
		JPanel pGoodsInStock = new JPanel();
        pGoodsInStock.setBackground(settlersLobby.theme.lobbyBackgroundColor);
		pGoodsInStock.setLayout(new GridLayout(1,2));
		goodsInStock.setRenderer(new GoodsInStockComboBoxListRenderer());
        goodsInStock.setBackground(settlersLobby.theme.backgroundColor);
        goodsInStock.setSelectedIndex(settlersLobby.config.getnewS4GameGoods());
        pGoodsInStock.add(goodsInStock);
		GroupLayout gl_gamenamePane = new GroupLayout(gamenamePane);
		gl_gamenamePane.setHorizontalGroup(
			gl_gamenamePane.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_gamenamePane.createSequentialGroup()
					.addGap(5)
					.addComponent(gamenameTextfield, GroupLayout.DEFAULT_SIZE, 244, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(pGoodsInStock, GroupLayout.PREFERRED_SIZE, 169, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		gl_gamenamePane.setVerticalGroup(
			gl_gamenamePane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_gamenamePane.createSequentialGroup()
					.addGap(5)
					.addGroup(gl_gamenamePane.createParallelGroup(Alignment.BASELINE)
						.addComponent(gamenameTextfield, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(pGoodsInStock, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gamenamePane.setLayout(gl_gamenamePane);
		setLayout(groupLayout);
				
		/////////////////////////////////////////////////////////////
		// THE FOLLOWING CODE ARE NOT GENERATED VIA WINDOW BUILDER //
		/////////////////////////////////////////////////////////////
		
		// create an array of the different tables (used for multiple style and actions)
		tableList = new ArrayList<JTable>();
		tableList.add(mapbaseResultsTable);
		tableList.add(favoritesTable);
		tableList.add(multiplayerTable);
		tableList.add(usermapsTable);
		tableList.add(savegameTable);
		
		// define a game-name
		// -> default-text in English because the game-name is visible 
		//    for all users no matter which language they use
		String gamenameTextfieldContent = I18n.getString("OpenNewS4GameDialog.GAMENAME");
		boolean gamenameTextfieldFontItalic = false;
		if (settlersLobby.getNick() == null ||
		    settlersLobby.getNick().length() == 0)
		{
			gamenameTextfieldContent = "game"; //$NON-NLS-1$
			gamenameTextfieldFontItalic = true;
		}
		else if (settlersLobby.getNick().endsWith("s") || settlersLobby.getNick().endsWith("x") )  //$NON-NLS-1$
		{
			gamenameTextfieldContent = String.format("%s' game", settlersLobby.getNick()); //$NON-NLS-1$
		}
		else
		{
			gamenameTextfieldContent = String.format("%s's game", settlersLobby.getNick());; //$NON-NLS-1$
		}
		gamenameTextfield.setText(gamenameTextfieldContent);
		if( gamenameTextfieldFontItalic ) {
			gamenameTextfield.setFont(new Font("Tahoma", Font.ITALIC, settlersLobby.config.getDpiAwareFontSize()));
		}
		
		// Add Listener for field with the game-name
		// which will check whether there is something entered or not
		// and fill in a placeholder or not.
		gamenameTextfield.addFocusListener(new FocusListener(){
            @Override
            public void focusGained(FocusEvent  e){
            	if( gamenameTextfield.getText().equals(I18n.getString("OpenNewS4GameDialog.GAMENAME")) ) {
            		gamenameTextfield.setText("");
            		gamenameTextfield.setFont(new Font("Tahoma", Font.PLAIN, settlersLobby.config.getDpiAwareFontSize()));
            	}
            }

			@Override
			public void focusLost(FocusEvent e) {
				if( gamenameTextfield.getText().length() == 0 ) {
					gamenameTextfield.setText(I18n.getString("OpenNewS4GameDialog.GAMENAME"));
					gamenameTextfield.setFont(new Font("Tahoma", Font.ITALIC, settlersLobby.config.getDpiAwareFontSize()));
				}
			}
        });
		
		// add ChangeListener for tab-navigation for the moment when user change the tab
		// -> loads the content for the active tab (if not preloaded at aLobby-start)
		// -> and removes the preview-part
		tabbedPane.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JTabbedPane sourceTabbedPane = (JTabbedPane) e.getSource();
				Component activeTab = sourceTabbedPane.getSelectedComponent();
				settlersLobby.config.setnewS4GameTab(sourceTabbedPane.indexOfComponent(activeTab));
				// if new activeTab is the savegame-tab
				// than make player and team- and the option-fields unchangeable
				if( activeTab.getName().equals("s4savegames") ) {
					txtPlayers.setEnabled(false);
					txtTeams.setEnabled(false);
					chckBxTrojans.setEnabled(false);
					chckBxCoop.setEnabled(false);
				}
				else {
					txtPlayers.setEnabled(true);
					txtTeams.setEnabled(true);
					chckBxTrojans.setEnabled(true);
					setStateOfCoop();
					setStateOfWimo();
				}
				loadMapsIntoTables(activeTab);
			}
		});
		
		// add focusListener to searchTextField which will start the search for
		// the entered text in MapBase
		searchTextField.addFocusListener(new FocusListener(){
            @Override
            public void focusGained(FocusEvent  e){
            	if( searchTextField.getText().equals(I18n.getString("OpenNewS4GameDialog.MAPBASE_INPUT_PLACEHOLDER")) ) {
            		searchTextField.setText("");
            		searchTextField.setFont(new Font("Tahoma", Font.PLAIN, settlersLobby.config.getDpiAwareFontSize()));
            	}
            }

			@Override
			public void focusLost(FocusEvent e) {
				if( searchTextField.getText().length() == 0 ) {
					searchTextField.setText(I18n.getString("OpenNewS4GameDialog.MAPBASE_INPUT_PLACEHOLDER"));
					searchTextField.setFont(new Font("Tahoma", Font.ITALIC, settlersLobby.config.getDpiAwareFontSize()));
				}
			}
        });
		
		// add focusListener to searchTextField which will start the search for
		// the entered text in userMap-List
		searchTextFieldUserMap.addFocusListener(new FocusListener(){
            @Override
            public void focusGained(FocusEvent  e){
            	if( searchTextFieldUserMap.getText().equals(I18n.getString("OpenNewS4GameDialog.MAPBASE_INPUT_PLACEHOLDER")) ) {
            		searchTextFieldUserMap.setText("");
            		searchTextFieldUserMap.setFont(new Font("Tahoma", Font.PLAIN, settlersLobby.config.getDpiAwareFontSize()));
            	}
            }

			@Override
			public void focusLost(FocusEvent e) {
				if( searchTextFieldUserMap.getText().length() == 0 ) {
					searchTextFieldUserMap.setText(I18n.getString("OpenNewS4GameDialog.MAPBASE_INPUT_PLACEHOLDER"));
					searchTextFieldUserMap.setFont(new Font("Tahoma", Font.ITALIC, settlersLobby.config.getDpiAwareFontSize()));
				}
			}
        });
		
		AbstractAction searchHandler = new AbstractAction(){
            @Override
            public void actionPerformed(ActionEvent   e){
            	if( searchTextField.getText().equals(I18n.getString("OpenNewS4GameDialog.MAPBASE_INPUT_PLACEHOLDER")) ) {
            		searchTextField.setText("");
            		searchTextField.setFont(new Font("Tahoma", Font.PLAIN, settlersLobby.config.getDpiAwareFontSize()));
            	}
            	else {
            		getMapsFromMapBaseBySearchString();
            	}
            }
		};
		
		// add actionListener to searchTextField and button
		// which will start the search for the entered text in mapBase
		searchTextField.addActionListener(searchHandler);
		searchBtn.addActionListener(searchHandler);
		
		// set active tab depending on actual setting
		int tabFromSetting = settlersLobby.config.getnewS4GameTab();
		if( tabFromSetting < 0 ) {
			tabFromSetting = 0;
		}
		tabbedPane.setSelectedIndex(tabFromSetting);

		// add ancestorListener
		this.addAncestorListener(new AncestorListener ()
	    {
	        public void ancestorAdded ( AncestorEvent event )
	        {
	        	// hide the ingameoptions meant for s3
	        	if( false == settlersLobby.isIngameOptionsStandalone() ) {
	        		settlersLobby.getGUI().displayIngameOptions(false);
	        	}
	        }

			public void ancestorRemoved(AncestorEvent event) {
			}

			public void ancestorMoved(AncestorEvent event) {
			}
	    } );
		
		// set layout
		// -> disable this function to activate windowBuilder in eclipse since
		//    windowBuilder crash with this ui-settings.
		setLayoutStyle();
	}
	
	/**
	 * Set the state of the coop-checkbox depending on wimo-checkbox. 
	 */
	private void setStateOfCoop() {
		if( chckBxWiMo.isSelected() ) {
			chckBxCoop.setEnabled(false);
		}
		else {
			chckBxCoop.setEnabled(true);
		}
	}
	
	/**
	 * Set the state of the wimo-checkbox depending on coop-checkbox. 
	 */
	private void setStateOfWimo() {
		if( chckBxCoop.isSelected() ) {
			chckBxWiMo.setEnabled(false);
		}
		else {
			chckBxWiMo.setEnabled(true);
		}
	}

	// do some styling
	private void setLayoutStyle() {
		
		// -> set column-widths for each column in all tables
		//    -> they are slightly different
		mapbaseResultsTable.getColumnModel().getColumn(0).setPreferredWidth(275);
		mapbaseResultsTable.getColumnModel().getColumn(1).setPreferredWidth(100);
		mapbaseResultsTable.getColumnModel().getColumn(2).setPreferredWidth(75);
		favoritesTable.getColumnModel().getColumn(0).setPreferredWidth(225);
		multiplayerTable.getColumnModel().getColumn(0).setPreferredWidth(450);
		usermapsTable.getColumnModel().getColumn(0).setPreferredWidth(450);
		savegameTable.getColumnModel().getColumn(0).setPreferredWidth(450);
		
		// -> set background-color for the tables
		mapbaseResultsScrollPane.setViewportView(mapbaseResultsTable);
		mapbaseResultsScrollPane.getViewport().setBackground(settlersLobby.theme.backgroundColor);
		favoritesScrollPane.setViewportView(favoritesTable);
		favoritesScrollPane.getViewport().setBackground(settlersLobby.theme.backgroundColor);
		multiplayerScrollPane.setViewportView(multiplayerTable);
		multiplayerScrollPane.getViewport().setBackground(settlersLobby.theme.backgroundColor);
		usermapsScrollPane.setViewportView(usermapsTable);
		usermapsScrollPane.getViewport().setBackground(settlersLobby.theme.backgroundColor);
		savegameScrollPane.setViewportView(savegameTable);
		savegameScrollPane.getViewport().setBackground(settlersLobby.theme.backgroundColor);
		
		TableCellCenterRenderer tableCellCenterRenderer = new TableCellCenterRenderer();
		for(JTable object: tableList){
			// remove grids for each cell
			object.setShowGrid(false);
			// set the default style for the table-header
			object.getTableHeader().setDefaultRenderer(tableCellCenterRenderer);
		    // add border-bottom to table-header
			object.getTableHeader().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, settlersLobby.theme.lobbyBackgroundColor));
			// add background-Color which will only visible behind the tableHeader
			// and only if the actual theme does have this value
			if( settlersLobby.theme.tableheadercolor != null && settlersLobby.theme.tableheadercolor.toString().length() > 0 ) {
		    	
		    	// define the cellRenderer
		    	DefaultTableCellRenderer headerRendererLeft = new DefaultTableCellRenderer();
		    	headerRendererLeft.setBackground(settlersLobby.theme.tableheadercolor);
		    	headerRendererLeft.setHorizontalAlignment( JLabel.LEFT );
		    	object.getColumnModel().getColumn(0).setHeaderRenderer(headerRendererLeft);
		    	
		    	DefaultTableCellRenderer headerRenderer = new DefaultTableCellRenderer();
		    	headerRenderer.setBackground(settlersLobby.theme.tableheadercolor);
		    	headerRenderer.setHorizontalAlignment( JLabel.CENTER );		    	
		    	// set it to each column except the first
		    	for (int i = 1; i < object.getModel().getColumnCount(); i++) {
		    		object.getColumnModel().getColumn(i).setHeaderRenderer(headerRenderer);
			    }
		    }
				
		}
		
		// -> override the ui-settings for the tabbed-pane to style the tabs in identical way
		tabbedPane.setUI(new BasicTabbedPaneUI() {
			// new border around whole tabbedPane
	        private final Insets borderInsets = new Insets(0, 0, 0, 0);
	        @Override
	        protected void paintContentBorder(Graphics g, int tabPlacement, int selectedIndex) {
	        }
	        @Override
	        protected Insets getContentBorderInsets(int tabPlacement) {
	            return borderInsets;
	        }
	        // new border around tabs
	        @Override
	        protected void paintTabBorder(Graphics g, int tabPlacement, int tabIndex, int x, int y, int w, int h, boolean isSelected ) {
	        	g.setColor(new Color(122,138,138));
	            g.drawLine(x, y+1, x, y+h+1); // left highlight
	        	g.drawLine(x+1, y+1, x+1, y+1); // top-left highlight
	        	g.drawLine(x+1, y+1, x+w-1, y+1); // top highlight
	
	            g.setColor(new Color(122,138,138));
	            g.drawLine(x+w-2, y+1, x+w-2, y+h+1); // right shadow
	        }
	        
	    });
		
	}
	
	/**
	 * Get the Favorite-Maps as list.
	 * 
	 * @return void
	 */
	private Object[] getFavoriteMaps() {
		return this.settlersLobby.getFavoriteS4Maps();
	}
	
	/**
	 * Get the multiplayerMaps as list.
	 * 
	 * @return void
	 */
	private String[] getMultiplayerMaps() {
		return this.settlersLobby.getS4MultiplayerMaps();
	}
	
	/**
	 * Get the usermaps as list.
	 * 
	 * @return void
	 */
	private String[] getUserMaps() {
		return this.settlersLobby.getS4UserMaps();
	}
	
	/**
	 * Get the savegames as list.
	 * 
	 * @return void
	 */
	private String[] getSavegames() {
		return this.settlersLobby.getS4Savegames();
	}
	
	/**
	 * Set a list of maps into a table including loading of map-data.
	 * Show a progress-bar during loading is in progress.
	 */
	private void setMapsIntoTable( Object[] userMapList, JTable tableComponent, String path ) {
		if( userMapList.length > 0 ) {
			// go through the maps
			for (int i = 0; i < userMapList.length; i++) {
				
				// get map-data (like map-name)
				ISMap map = S4Map.newInstance(path + userMapList[i], settlersLobby);
				
				// if map-instance is ok, go further
				if( map instanceof ISMap ) {
					// add map to table with output-values (like map-name)
					DefaultTableModel model = (DefaultTableModel) tableComponent.getModel();
					if( model.getColumnCount() == 1 ) {
						model.addRow(new Object[]{
							map.toDisplayString().replace(path, "").replace(".exe", "")
						});
					}
				}
				
				// free memory from now unused ISMap-object
				map = null;
				System.gc();
				
			}

		}
	}
	
	/**
	 * Set a list of maps into a JTable.
	 * 
	 * @param maps
	 * @param tableComponent
	 */
	private void setMapsIntoTable( Object[] maps, JTable tableComponent ) {
		if( maps.length > 0 ) {
			// go through the map-list
			for ( Object mapString: maps ) {
				
				// get map-data (like map-name)
				ISMap map = S4Map.newInstance(mapString.toString(), this.settlersLobby);
		
				// add this map to table with output-values (like map-name)
				DefaultTableModel model = (DefaultTableModel) tableComponent.getModel();
				if( model.getColumnCount() == 1 ) {
					model.addRow(new Object[]{
						map.toDisplayString()
					});
				}
				
				// free memory from unused ISMap-object
				map = null;
				System.gc();
				
			}
		}
	}
	
	/**
	 * Set a list of maps from a JSON-Result into a JTable.
	 * 
	 * @param maps
	 * @param tableComponent
	 */
	private void setMapsIntoTable(JSONArray maps, JTable tableComponent) {
		if( maps.length() > 0 ) {
			// go through the map-list
			for (int i = 0; i < maps.length(); i++) {
				
				// get the mapname
				String mapname = "";
				if( !maps.getJSONObject(i).isNull("filename") ) {
					mapname = maps.getJSONObject(i).get("filename").toString();
				}
				
				// get the player
				String player = "";
				if( !maps.getJSONObject(i).isNull("player") ) {
					player = maps.getJSONObject(i).get("player").toString();
				}
				
				// get the map-size
				String size = "";
				if( !maps.getJSONObject(i).isNull("size") ) {
					size = maps.getJSONObject(i).get("size").toString();
				}
				
				// add this map to table with output-values (like map-name)
				DefaultTableModel model = (DefaultTableModel) tableComponent.getModel();
				if( model.getColumnCount() == 3 ) {
					model.addRow(new Object[]{
						mapname,
						player, 
						size
					});
				}
			}
		}
	}
	
	/**
	 * Get the background-color from active style
	 * 
	 * @return Color
	 */
	/*
	private Color getBackgroundColor() {
		return this.settlersLobby.theme.backgroundColor;
	}*/
	
	public void setParentObject( JFrame parentFrame ) {
		this.parentFrame = parentFrame;
	}
	
	/**
	 * Get the mapdata by mapname and insert it into previewPanel
	 * 
	 * @param mapname
	 * 			Map-Name
	 * @param previewPanel
	 * 			The Preview-Panel which consists of the following fields. 
	 * @param pnlLink 
	 * @param txtMapPlayer 
	 * @param txtMapDate 
	 * @param pnlCreator 
	 * @param txtMapSize 
	 * @param previewPanel 
	 * @param pnlPreviewImage
	 */
	/*private void getMapDataByName( String mapname, JPanel previewPanel, JTextPane txtMapSize, JPanel pnlCreator, JTextPane txtMapDate, JTextPane txtMapPlayer, JPanel pnlLink, JPanel pnlRating, JPanel pnlPreviewImage ) {
		if( mapname.length() > 0 && Toolkit.getDefaultToolkit().getScreenSize().getHeight() > 620 ) {
        				
			// get a window with waiting-message
			LoadingJFrame waitingWindow = new LoadingJFrame(this.parentFrame);
			
			// get settlersLobby to use it in swingWorker
			SettlersLobby settlersLobby = this.settlersLobby;
			
			// define a swingWorker which will show a loading-window while http-request is not done
			SwingWorker<?, ?> swingWorker = new SwingWorker<Object, Object>() {

				@Override
				protected Object doInBackground() throws Exception {
					
					String response = "";
					
					// define the request
		        	httpRequests httpRequests = new httpRequests();
		        	// -> set parameter to configure the JSON-response
		        	httpRequests.addParameter("json", "1");
		        	// -> ignore security-token
		        	httpRequests.setIgnorePhpSessId(true);
		        	// -> show a progress-monitor
		        	httpRequests.setShowProgressMonitor(true);
		        	// -> set the url
		        	String url = "";
					try {
						// encode the mapname and add it to the url
						url = settlersLobby.config.getDefaultGetMapData() + URLEncoder.encode(mapname, "UTF-8");
					} catch (UnsupportedEncodingException e2) {
						e2.printStackTrace();
					}
					httpRequests.setUrl(url);
					// -> get the response-String
					try {
						response = httpRequests.doRequest();
					} catch (IOException e) {
					}
					
					// show only informations for this map if no error happend
					// during the http-request
					// -> if an error came up, show no hint therefore because it would be an endless windows-messaging-loop
					//    if users clicks again and again ..
					boolean previewPanelVisibility = false;
					if( !httpRequests.isError() ) {
					
						String hexcolor = SettlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor());
				        
						String size = "";
						String player = "";
						String creator = "";
						String creator_link = "";
						String insertdate = "";
						String link = "";
						String rating = "";
						byte[] img = null;
						
						if( response.length() > 0 ) {
							JSONObject obj = new JSONObject(response);
							if( !obj.isNull("size") ) {
								size = obj.getString("size");
								previewPanelVisibility = true;
							}
							if( !obj.isNull("player") ) {
								player = obj.getString("player");
								previewPanelVisibility = true;
							}
							if( !obj.isNull("creator") ) {
								creator = obj.getString("creator");
								previewPanelVisibility = true;
							}
							if( !obj.isNull("creator_link") ) {
								if( obj.getString("creator_link").length() > 0 ) {
									creator_link = obj.getString("creator_link");
									previewPanelVisibility = true;
								}
							}
							if( !obj.isNull("insertdate") ) {
								insertdate = obj.getString("insertdate");
								previewPanelVisibility = true;
							}
							if( !obj.isNull("rating") ) {
								rating = obj.getString("rating");
								previewPanelVisibility = true;
							}
							if( !obj.isNull("link") ) {
								link = obj.getString("link");
								previewPanelVisibility = true;
							}
							if( !obj.isNull("img") ) {
								String imgBase64 = obj.getString("img");
								img = Base64.getDecoder().decode(imgBase64);
								previewPanelVisibility = true;
							}
						}
						
						// set the resulting data (if available, otherwise remove data)
						txtMapSize.setText(size);
						txtMapPlayer.setText(player);
						txtMapDate.setText(insertdate);
						
						pnlCreator.removeAll();
						pnlCreator.validate();
						pnlCreator.repaint();
						
						// set creator-info for this map depending on available link to it's mapbase-profile
						if( creator_link.length() > 0 ) {
							LinkLabel creatorLinkLabel = new LinkLabel();
							creatorLinkLabel.setText(MessageFormat.format("<html><a href=''" + creator_link + "'' style=''color: {0}''>" + creator + "</a></html>", hexcolor));
							creatorLinkLabel.setBorder(BorderFactory.createCompoundBorder(creatorLinkLabel.getBorder(), BorderFactory.createEmptyBorder(3, 3, 3, 3)));
							creatorLinkLabel.setBounds(0, 0, 0, 0);
							creatorLinkLabel.setSize(creatorLinkLabel.getPreferredSize());
							creatorLinkLabel.setLocation(0, 0);
							pnlCreator.add(creatorLinkLabel);
						}
						else {
							JTextField creatorTextField = new JTextField();
							creatorTextField.setText(creator);
							pnlCreator.add(creatorTextField);
						}
						pnlCreator.validate();
						pnlCreator.repaint();
						
						// remove all components from linkPanel
						pnlLink.removeAll();
						pnlLink.validate();
						pnlLink.repaint();
						
						// add the text as linked text via linkLabel
						if( link.length() > 0 ) {
							LinkLabel linklabel = new LinkLabel();
							linklabel.setText(MessageFormat.format("<html><a href=''" + link + "'' style=''color: {0}''>" + link + "</a></html>", hexcolor));
							linklabel.setBorder(BorderFactory.createCompoundBorder(linklabel.getBorder(), BorderFactory.createEmptyBorder(3, 3, 3, 3)));
							linklabel.setBounds(0, 0, 0, 0);
							linklabel.setSize(linklabel.getPreferredSize());
							linklabel.setLocation(0, 0);
							pnlLink.add(linklabel);
							chosenMapLink = link;
						}
						else {
							chosenMapLink = "";
						}
						pnlLink.validate();
						pnlLink.repaint();
			
						// remove all components in previewPanel
						pnlPreviewImage.removeAll();
						// and reset its graphic
						pnlPreviewImage.repaint();
						
						// show image only if an image is available
						if( img == null || img.length == 0 ) {
							// image for this map is not available
							// -> set this panel invisible (blank space)
							pnlPreviewImage.setVisible(false);
						}
						else {
							// get image from blob as byte-Array
							ByteArrayInputStream bais = new ByteArrayInputStream(img);
						    BufferedImage originalBufferedImage;
							try {
								
								// read image from Byte-Array
								originalBufferedImage = ImageIO.read(bais);
								
								// set the designated width for the image
								// -> do not set this to the width and height of the wrapping panel
								//    because this will be bigger than the image itself
								// -> height will be calculated depending on size of the original image
						        int thumbnailWidth = 240;
						        
						        // calculate the width of the resulting image on-the-fly
						        int widthToScale = (int)(1.1 * thumbnailWidth);
						        int heightToScale = (int)((widthToScale * 1.0) / originalBufferedImage.getWidth() 
						                            * originalBufferedImage.getHeight());
						        
						        // create the target image without contents
						        BufferedImage resizedImage = new BufferedImage(widthToScale, heightToScale, originalBufferedImage.getType());
					        	Graphics2D g = resizedImage.createGraphics();
					        	// -> set alpha-composite 
					        	g.setComposite(AlphaComposite.Src);
					        	// -> set rendering-conditions
					        	g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
					        	g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
					        	g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
					        	// -> draw the image
					        	g.drawImage(originalBufferedImage, 0, 0, widthToScale, heightToScale, null);
					        	// -> forget the result :]
					        	g.dispose();
					        	
					        	// add the bufferedImage as image to the panel
					        	pnlPreviewImage.add(new JLabel(new ImageIcon(resizedImage)));
					        	
					        	// set the name of the preview-image-Panel to the map-name
					        	// -> will be used in onclick-event
					        	pnlPreviewImage.setName(mapname);
					        	
						    } catch (IOException e) {
						        throw new RuntimeException(e);
						    }
							
							// set it visible
							pnlPreviewImage.setVisible(true);
						}
						
						// check if this map has been rated
						// -> if yes, than show the stars
						// -> if not, than hide this panel
						pnlRating.removeAll();
						pnlRating.validate();
						pnlRating.repaint();
						if( rating.length() > 0 ) {
							Font font = settlersLobby.config.getFontAweSome((float) (12 * MessageDialog.getDpiScalingFactor()));
							
							// generate the list of stars depending of actual rating-value
							// -> fontAweSome is used to display the stars
							String stars = "";
							for( int r=0;r<Integer.parseInt(rating);r++ ) {
								stars = stars + "\uf005";
							}
							
							// create a label where the stars will be added
							JLabel label = new JLabel();
							label.setFont(font);
							label.setText(stars);
							label.setBorder(new EmptyBorder(0, 0, 0, 0));
							label.setBounds(0, 0, 0, 0);
							label.setSize(label.getPreferredSize());
							label.setLocation(0, 0);
							label.setVisible(true);
							pnlRating.add(label);
							pnlRating.setVisible(true);
						}
						else {
							pnlRating.setVisible(false);
						}
						pnlRating.validate();
						pnlRating.repaint();
						
						// set previewPanel visible or not (depending on results above)
						previewPanel.setVisible(previewPanelVisibility);
						
					}
					
					// resize the wrapping panel and the window accordingly
					int newHeight = defaultWindowHeight;
					if( previewPanelVisibility ) {
						newHeight = defaultWindowHeight + 250;
					}
					setPreferredSize(new Dimension(getPreferredSize().width, newHeight));
					openNewGameDialog.pack();
					return previewPanelVisibility;
				}
		         //Executed in event dispatching thread
		        @Override
		        public void done() {
		        	// close the waiting window
		        	waitingWindow.dispatchEvent(new WindowEvent(waitingWindow, WindowEvent.WINDOW_CLOSING));
		        }
				
			};
			
			swingWorker.execute();
		}
	}*/
	
	/**
	 * Create the game with configured parameters
	 * including check if all necessary parameters are configured.
	 * "All" means:
	 * - Name of the Map
	 * - Name for the Game
	 * - Player-count
	 * - Team-count
	 * 
	 * The chosen options for
	 * - goods
	 * - WiMo
	 * - trojans
	 * - coop
	 * will be respected. 
	 * 
	 * @return void
	 */
	public void createGame( String mapname ) {

		saveGameSettings();
				
		// create a window for hints if something is missing
		MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
		
		// check for necessary parameters
		boolean startGame = true;
		
		// -> map-name available?
		if( mapname.length() == 0 ) {
			startGame = false;
			messagewindow.addMessage(
				I18n.getString("OpenNewS4GameDialog.ERROR_CHOOSEMAP"),
        		UIManager.getIcon("OptionPane.errorIcon")
        	);
		}
		
		// -> if this is a random-map check if the given mapcode has exact 8 chars
		if( mapname.startsWith(ALobbyConstants.PATH_RANDOM + File.separator) && mapname.replace(ALobbyConstants.PATH_RANDOM + File.separator, "").length() != 8 ) {
			startGame = false;
			messagewindow.addMessage(
				I18n.getString("OpenNewS4GameDialog.ERROR_RANDOM_MAPNAME"),
        		UIManager.getIcon("OptionPane.errorIcon")
        	);
		}
		// TODO Syntax-check for random-map-codes
		
		// -> check for game-name
		String gameName = getGameName();
		String gamenameTextfieldContent = I18n.getString("OpenNewS4GameDialog.GAMENAME");
		if( gameName.length() == 0 ) {
			startGame = false;
			messagewindow.addMessage(
				I18n.getString("OpenNewS4GameDialog.ERROR_MAPNAME"),
        		UIManager.getIcon("OptionPane.errorIcon")
        	);
		}
		if( gameName.length() > 0 && gameName.equals(gamenameTextfieldContent) ) {
			startGame = false;
			messagewindow.addMessage(
				I18n.getString("OpenNewS4GameDialog.ERROR_MAPNAME"),
        		UIManager.getIcon("OptionPane.errorIcon")
        	);
		}
		
		// -> check for player and team
		int players = getPlayers();
		int teams = getTeams();
		if( txtPlayers.isEnabled() && players <= 1 ) {
			startGame = false;
			messagewindow.addMessage(
				I18n.getString("OpenNewS4GameDialog.ERROR_PLAYERCOUNT"),
        		UIManager.getIcon("OptionPane.errorIcon")
        	);
		}
		if( txtTeams.isEnabled() && teams <= 0 ) {
			startGame = false;
			messagewindow.addMessage(
				I18n.getString("OpenNewS4GameDialog.ERROR_TEAMCOUNT"),
        		UIManager.getIcon("OptionPane.errorIcon")
        	);
		}
		if( txtPlayers.isEnabled() && txtTeams.isEnabled() && teams > players ) {
			startGame = false;
			messagewindow.addMessage(
				I18n.getString("OpenNewS4GameDialog.ERROR_TEAMCOUNT_2"),
        		UIManager.getIcon("OptionPane.errorIcon")
        	);
		}
		
		if( txtPlayers.isEnabled() && players > 8 ) {
			startGame = false;
			messagewindow.addMessage(
				I18n.getString("OpenNewS4GameDialog.ERROR_PLAYERCOUNT_2"),
        		UIManager.getIcon("OptionPane.errorIcon")
        	);
		}
		
		// -> is tetris running?
		if( settlersLobby.isPlayingTetris() ) {
			startGame = false;
			messagewindow.addMessage(
				I18n.getString("OpenNewS4GameDialog.ERROR_TETRIS"),
        		UIManager.getIcon("OptionPane.errorIcon")
        	);
		}
		
		// start game if all necessary parameters are available 
		if( startGame ) {
			
			/*			
			CheckAndDownloadMapFile mapFile = new CheckAndDownloadMapFile(this.settlersLobby);
			mapFile.setMap(mapname);
			if( mapFile.checkMapFile() ) {
			*/
		
				// add this map to the user-favorites
				settlersLobby.config.addFavoriteS4Map(mapname);
				settlersLobby.config.setFavoriteS4Maps(settlersLobby.config.getFavoriteS4Maps());
				
				// reload the favoriteMaps
				loadFavoriteMaps();
				
				// create the game-conditions
				OpenGame newGame = new OpenGame(
					settlersLobby.ipToUse(), 
					settlersLobby.getNick(), 
					gameName, 
					mapname,
	                false, 
	                getGoodsInStock().getIndex(), 
	                getPlayers(), 
	                1, 
	                false, 
	                isWiMo(), 
	                0, 
	                "", 
	                0, 
	                0, 
	                0,
					new ArrayList<>()
				);
				// -> set game-type to S4
				newGame.setMisc(MiscType.S4);
				// -> set Trojans
				newGame.setTrojans(withTrojans());
				// -> is coop
				newGame.setCoop(isCoop());
				// -> set teamcount
				newGame.setTeamCount(getTeams());
				// -> add the creation-time
		        Calendar calendar = Calendar.getInstance();
		    	calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
		        newGame.setCreationTime(calendar.getTimeInMillis());
				
				checkMapAndStartGame(newGame);
			
			/*}
			else {
				String hexcolor = SettlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor());
				messagewindow.addMessage(
					MessageFormat.format("<html>" + I18n.getString("OpenNewS4GameDialog.MAP_DOES_NOT_EXISTS_ON_MAPBASE") + "<br>" + I18n.getString("OpenNewS4GameDialog.MAP_DOES_NOT_EXISTS_ON_MAPBASE_LINE_2") + "</html>", hexcolor),
	        		UIManager.getIcon("OptionPane.errorIcon")
	        	);
	        	messagewindow.addButton(
	        			I18n.getString("OpenNewS4GameDialog.VPN_HINT_AT_START_BUTTON"),
	        		new ActionListener() {
	        		    @Override
	        		    public void actionPerformed(ActionEvent e) {
	        	        	// and close the hint-window
	        		    	messagewindow.dispose();
	        		    	// create the game-conditions
							OpenGame newGame = new OpenGame(
								settlersLobby.ipToUse(), 
								settlersLobby.getUserName(), 
								gameName, 
								mapname,
				                false, 
				                getGoodsInStock().toString(), 
				                getPlayers(), 
				                1, 
				                false, 
				                isWiMo(), 
				                0, 
				                "", 
				                0, 
				                0, 
				                0
							);
							// -> set game-type to S4
							newGame.setMisc(MiscType.S4);
							// -> set Trojans
							newGame.setTrojans(withTrojans());
							// -> is Coop
							newGame.setCoop(isCoop());
							// -> set teamcount
							newGame.setTeamCount(getTeams());
							// -> add the creation-time
					        Calendar calendar = Calendar.getInstance();
					    	calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
					        newGame.setCreationTime(calendar.getTimeInMillis());
							
							checkMapAndStartGame(newGame);
	        		    }
	        		}
	        	);
	        	messagewindow.addButton(
	        		I18n.getString("OpenNewS4GameDialog.VPN_HINT_AT_START_BUTTON_2"),
	        		new ActionListener() {
	        		    @Override
	        		    public void actionPerformed(ActionEvent e) {
	        	        	// and close the hint-window
	        		    	messagewindow.dispose();
	        		    	// set the openNewGameDialog to visible
	        		    	OpenNewGameDialog openNewGameDialog = getOpenNewGameDialog();
	        		    	openNewGameDialog.setVisible(true);
	        		    }
	        		}
	        	);
	        	// set the openNewGameDialog to invisible
	        	OpenNewGameDialog openNewGameDialog = getOpenNewGameDialog();
	        	openNewGameDialog.setVisible(false);
	        	messagewindow.setVisible(true);
			}*/
		}
		else {
			// otherwise show the hint
        	messagewindow.addButton(
        		UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
        		new ActionListener() {
        		    @Override
        		    public void actionPerformed(ActionEvent e) {
        	        	// and close the hint-window
        		    	messagewindow.dispose();
        		    	// set the openNewGameDialog to visible
        		    	OpenNewGameDialog openNewGameDialog = getOpenNewGameDialog();
        		    	openNewGameDialog.setVisible(true);
        		    }
        		}
        	);
        	// set the openNewGameDialog to invisible
        	OpenNewGameDialog openNewGameDialog = getOpenNewGameDialog();
        	openNewGameDialog.setVisible(false);
        	messagewindow.setVisible(true);
		}
	}
	
	private void checkMapAndStartGame( OpenGame newGame ) {
        // Check if a S4 CD is mounted
        boolean mounted = S4Support.isCdMounted();
        if (mounted != true) {
            MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
            messagewindow.addMessage(
                I18n.getString("SettlersLobby.ERROR_S4_CD_Missing"),
                UIManager.getIcon("OptionPane.errorIcon")
            );
            messagewindow.addButton(
                UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        // and close the hint-window
                        messagewindow.dispose();
                    }
                }
            );
            messagewindow.setVisible(true);
            return;
        }

		// check if user is connected via vpn
		// -> if not warn the user that he needs vpn to play
		//    with others
		if( !settlersLobby.isLocalMode() && ( settlersLobby.getVpnIpFromSystem() == null || !settlersLobby.getVpnIpFromSystem().startsWith(ALobbyConstants.VPN_IP_RANGE) ) ) {
			String hexcolor = SettlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor());
			MessageDialog messagewindow1 = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
			messagewindow1.addMessage(
				I18n.getString("OpenNewS4GameDialog.VPN_HINT_AT_START"),
				UIManager.getIcon("OptionPane.errorIcon")
			);
			messagewindow1.addMessage(
					"<html>" + MessageFormat.format(I18n.getString("SettlersLobby.VPN_NOT_ACTIVE_INFO"),hexcolor) + "</html>",
				UIManager.getIcon("OptionPane.informationIcon")
			);
			messagewindow1.addButton(
				I18n.getString("OpenNewS4GameDialog.VPN_HINT_AT_START_BUTTON"),
				new ActionListener() {
				    @Override
				    public void actionPerformed(ActionEvent e) {
				    	// generate a userlist in gameState
						settlersLobby.getGameState().userWantsToCreateGame(newGame.getMap(), true);
						// activate the game options-window with initial userlist as host
						Test.output("setS4GameOptions iiii");
						settlersLobby.getGUI().setS4GameOptions(settlersLobby.getGameState().getUserList(), true, true);
						// set this game as host game which is open
						settlersLobby.setAsOpenHostGame(newGame);
						// send info about the new game to all users in the channel
						settlersLobby.sendOpenHostGame(null);
				    	// close the open new game dialog
				    	closeDialog();
				    	messagewindow1.dispose();
				    }
				}
			);
			messagewindow1.addButton(
				I18n.getString("OpenNewS4GameDialog.VPN_HINT_AT_START_BUTTON_2"),
				new ActionListener() {
				    @Override
				    public void actionPerformed(ActionEvent e) {
				    	// close this window
				    	messagewindow1.dispose();
				    }
				}
			).addActionListener(actionListenerToCloseNewGameDialog);
			messagewindow1.setVisible(true);
		}
		else {
			// generate a userlist in gameState
			settlersLobby.getGameState().userWantsToCreateGame(newGame.getMap(), true);
			// activate the game options-window with initial userlist as host
			Test.output("setS4GameOptions jjjj");
			settlersLobby.getGUI().setS4GameOptions(settlersLobby.getGameState().getUserList(), true, true);
			// set this game as host game which is open
			settlersLobby.setAsOpenHostGame(newGame);
			// send info about the new game to all users in the channel
			settlersLobby.sendOpenHostGame(null);
			// set user as playing
			settlersLobby.userWasActive();
			settlersLobby.getAwayStatus().onGameStarted();
	    	// close the open new game dialog
			closeDialog();
		}
	}
	
	private void closeDialog() {
		// remove the object
		settlersLobby.getGUI().resetMapChoosingDialog();
		// close the open new game dialog
		this.openNewGameDialog.setVisible(false);
		this.openNewGameDialog.dispose();
	}

	/**
	 * Check whether this will be a wimo or not
	 *
	 * @return boolean
	 */
	private boolean isWiMo() {
		if( chckBxWiMo.isEnabled() ) {
			return chckBxWiMo.isSelected();
		}
		return false;
	}
	
	/**
	 * Check whether this will be a game with trojans or not
	 *
	 * @return boolean
	 */
	private boolean withTrojans() {
		if( chckBxTrojans.isEnabled() ) {
			return chckBxTrojans.isSelected();
		}
		return false;
	}
	
	/**
	 * Check whether this will be a coop-game
	 *
	 * @return boolean
	 */
	private boolean isCoop() {
		if( chckBxCoop.isEnabled() ) {
			return chckBxCoop.isSelected();
		}
		return false;
	}
	
	/**
	 * Get the selected tools-setting.
	 * 
	 * @return GoodsInStock
	 */
	private GoodsInStock getGoodsInStock() {
		return (GoodsInStock)goodsInStock.getSelectedItem();
	}
	
	/**
	 * Get the Game-name which will be show to all users in the aLobby
	 * 
	 * @return String
	 */
	private String getGameName() {
		return gamenameTextfield.getText();
	}
	
	/**
	 * Get the player-count,
	 * 
	 * @return int
	 */
	private Integer getPlayers() {
		return Integer.parseInt(txtPlayers.getText());
	}
	
	/**
	 * Get the team-name.
	 * 
	 * @return int
	 */
	private Integer getTeams() {
		return Integer.parseInt(txtTeams.getText());
	}

	/**
	 * Define mouseAdapter for mouse-events on tables, e.g. to start a game via double click.
	 * 
	 * Use this as Listener for appropriated panes.
	 * 
	 * @return MouseAdapter
	 */
	private final MouseAdapter mouseAdapterToCreateGame = new MouseAdapter()
	{
		
		@Override
		public void mouseClicked(MouseEvent e)
		{
			
			if (e.getClickCount() > 2 || e.isConsumed())
	        {
		        // prevent execution if user clicks more than 2 times
		        return;
	        }
			
			e.consume();
						
	    	// get the table-object
        	JTable table = (JTable) e.getSource();
        	        	
        	// get the name of the table
        	// -> essential to send the correct s4-foldername to start the game
        	String tableName = table.getName();

        	// get the number of the clicked row
        	int i = table.getSelectedRow();
        	
			// left-mouse-click
		    if( SwingUtilities.isLeftMouseButton(e) )
		    {
		    	
		    	// if single-click load the map-data
		    	// TODO: reactivate until mapbase supports S4-maps
		    	/*if( e.getClickCount() == 1 ) {
		    			    		
					// get map-name from first column of the selected row
		    		// and show map-data from mapBase
					String mapname = "";
					if( table.getSelectedRow() >= 0) {
						mapname = getMapNameFromTableWithRowSorter( table, table.getSelectedRow());
						// -> not for random Maps
						if( mapname.indexOf(ALobbyConstants.PATH_RANDOM + File.separator) >= 0 ) {
							mapname = "";
						}
						// remove map-type from mapName if user clicked in favoritesTable
						if( table == favoritesTable ) {
							mapname = mapname.replace(ALobbyConstants.PATH_MAP + File.separator, "").replace(ALobbyConstants.PATH_S4MULTI + File.separator, "");
						}
					}
					if( mapname.length() > 0 ) {
						// get data for previewPanel
						getMapDataByName(mapname, previewPanel, txtMapSize, pnlCreator, txtMapDate, txtMapPlayer, pnlLink, pnlRating, pnlPreviewImage);
					}
					else {
						// set the previewPanel invisible and resize wrapping panel and window accordingly
						previewPanel.setVisible(false);
						setPreferredSize(new Dimension(getPreferredSize().width, defaultWindowHeight));
						openNewGameDialog.pack();
					}
					
		    	}*/
		    	
		    	// if double-clicked start the game with actual setting
		        if( e.getClickCount() == 2 )
		        {
		        			        	
		        	// get the content of the first column from clicked row
		        	String mapname = getMapNameFromTableWithRowSorter( table, i);
		        	
		        	// depending on tableName search for the clicked row/map
		        	// -> except for favorites-table because the first column already consists the settler 3-foldername
		        	switch( tableName ) {
		        		case "mapbaseTable":
		        			mapname = ALobbyConstants.PATH_USER + File.separator + mapname;
		        			break;
		        		case "multiplayerTable":
		        			mapname = ALobbyConstants.PATH_S4MULTI + File.separator + mapname;
		        			break;
		        		case "usermapsTable":
		        			mapname = ALobbyConstants.PATH_S4USER + File.separator + mapname;
		        			break;
		        		case "savegameTable":
		        			mapname = ALobbyConstants.PATH_S4SAVEGAMES + File.separator + mapname;
		        			break;
		        	}
		        			        			        	        	
		        	// start game
		        	createGame(mapname);		        		
		        }
		    }
		    
		    // right-mouse-click
		    if( SwingUtilities.isRightMouseButton(e) ) {
		    	// check whether a row is selected
		    	// -> if yes, than copy the content of the first row
	        	String mapname = table.getModel().getValueAt(i, 0).toString();
	        	Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
				if( clipboard != null ) {
					clipboard.setContents(new StringSelection(mapname), null);
				}
		    }
        }
	};
	
	/**
	 * Define actionListener to start a game e.g. if the "start"-button is clicked.
	 * 
	 * @return Action
	 */
	private final Action actionListenerToCreateGame = new AbstractAction()
	{
		@Override
		public void actionPerformed(ActionEvent evt)
		{
			
			// get the name of the active tab
			String activeTabName = tabbedPane.getSelectedComponent().getName();
			
			// prepare String for map-name
			String mapname = "";
			
			// depending from this name, get the actual marked game
			// -> if nothing marked show a hint
			// -> if marked start the game
			switch (activeTabName) {
			
				// favorites-tab
				case "favoriteS4Maps":
					if( favoritesTable.getRowCount() > 0 ) {
						int i = favoritesTable.getSelectedRow();
						if( i >= 0 ) {
							mapname = getMapNameFromTableWithRowSorter( favoritesTable, i );
						}
					}
					break;
				
				// mapbase-tab 
				case "mapbase":
					if( mapbaseResultsTable.getRowCount() > 0 ) {
						int i = mapbaseResultsTable.getSelectedRow();
						if( i >= 0 ) {
							mapname = ALobbyConstants.PATH_USER + File.separator + getMapNameFromTableWithRowSorter( mapbaseResultsTable, i );
						}
					}
					break;
					
				// multiplayer-tab
				case "multiplayer":
					if( multiplayerTable.getRowCount() > 0 ) {
						int i = multiplayerTable.getSelectedRow();
						if( i >= 0 ) {
							mapname = ALobbyConstants.PATH_S4MULTI + File.separator + getMapNameFromTableWithRowSorter( multiplayerTable, i );
						}
					}
					break;
				// usermaps-tab
				case "usermaps":
					if( usermapsTable.getRowCount() > 0 ) {
						int i = usermapsTable.getSelectedRow();
						if( i >= 0 ) {
							mapname = ALobbyConstants.PATH_S4USER + File.separator + getMapNameFromTableWithRowSorter( usermapsTable, i );
						}
					}
					break;
				// savegame-tab
				case "s4savegames":
					if( savegameTable.getRowCount() > 0 ) {
						int i = savegameTable.getSelectedRow();
						if( i >= 0 ) {
							mapname = ALobbyConstants.PATH_S4SAVEGAMES + File.separator + getMapNameFromTableWithRowSorter( savegameTable, i );
						}
					}
					break;
				// random-tab
				case "s4random":
					mapname = getRandomCode();
					if( !"".equals(mapname) ) {
						mapname = ALobbyConstants.PATH_RANDOM + File.separator + mapname;
					}
					break;
			}
			
			// start game if mapname is available
			createGame(mapname);
		}
	};
	
	/**
	 * Zoom the preview image.
	 */
	private final MouseAdapter actionListenerZoomPreview = new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e)
			{
				// left-mouse-click
			    if( SwingUtilities.isLeftMouseButton(e) &&  e.getClickCount() == 1 )
			    {
			    	// get the map-name which is actually visible in the previewPanel
			    	String mapname = ((JPanel) e.getSource()).getName();
			    	
			    	// show the window with detailed map-preview
			    	new ShowMapDetail(settlersLobby.getGUI().getMainWindow(), settlersLobby, mapname, false);
			    }
			}
	};
	
	/**
	 * Close this dialog.
	 */
	private final Action actionListenerToCloseNewGameDialog = new AbstractAction() {
		
		@Override
        public void actionPerformed(java.awt.event.ActionEvent evt) {
			saveGameSettings();
			
			// remove the object
			settlersLobby.getGUI().resetMapChoosingDialog();
			
			// remove the ingameoptions
			if( false == settlersLobby.isIngameOptionsStandalone() ) {
				settlersLobby.getGUI().displayIngameOptions(false);
			}
			
        	// close the window
        	parentFrame.dispose();
        }
    };
    
    /**
     * Suggest the actual map in chat.
     */
    /*private final Action actionListenerToSuggestMap = new AbstractAction() {

		@Override
		public void actionPerformed(ActionEvent e) {
			settlersLobby.actionChatMessage(chosenMapLink);
		}
    	
    };*/
    	
    /**
     * Save the actual settings for next game.
     */
    private void saveGameSettings() {
    	// save the actual settings
		settlersLobby.config.setnewS4GameGoods(getGoodsInStock().getIndex());
		settlersLobby.config.setnewS4GameWimo(isWiMo());
		settlersLobby.config.setnewS4GameTab(tabbedPane.getSelectedIndex());
		settlersLobby.config.setnewS4GamePlayers(getPlayers());
		settlersLobby.config.setnewS4GameTeams(getTeams());
		settlersLobby.config.setnewS4GameCoop(isCoop());
		settlersLobby.config.setnewS4GameTrojans(withTrojans());
		settlersLobby.config.setnewS4GameRandomMapSize(getRandomMapSize());
		settlersLobby.config.setnewS4GameRandomLandmass(getRandomLandmass());
		settlersLobby.config.setnewS4GameRandomAxes(getRandomAxes());
		settlersLobby.config.setnewS4GameRandomMineralResources(getMineralResources());
		settlersLobby.config.setnewS4GameRandomCode(getRandomCode());
    }
    
    /**
     * Return the actual inserted random-code.
     * 
     * @return String
     */
	private String getRandomCode() {
		return this.randomCodeField.getText().toUpperCase();
	}

	/**
	 * Return the actual selected mineral resources for random-games.
	 * 
	 * @return int
	 */
	private int getMineralResources() {
		return this.randomMinerals.getSelectedIndex();
	}

	/**
	 * Return the actual selected axes for random-games.
	 * 
	 * @return int
	 */
	private int getRandomAxes() {
		return this.randomAxes.getSelectedIndex();
	}

	/**
	 * Return the actual selected landmass for random-games.
	 * 
	 * @return int
	 */
	private int getRandomLandmass() {
		return this.randomLandmass.getSelectedIndex();
	}

	/**
	 * Return the actucal selected mapsize for random-games.
	 * 
	 * @return int
	 */
	private int getRandomMapSize() {
		return this.randomMapSize.getSelectedIndex();
	}

	/**
	 * Get the wrapping dialog-window
	 * 
	 * @return OpenNewGameDialog
	 */
	public OpenNewGameDialog getOpenNewGameDialog() {
		return this.openNewGameDialog;
	}
	
	/**
	 * Start search for S4-Maps in MapBase on base of a search-keyword.
	 * 
	 * @return void
	 */
	private void getMapsFromMapBaseBySearchString() {
		
		// get the search-string from input-field
		String searchString = "";
		if( searchTextField.getText().length() > 0  ) {
			searchString = searchTextField.getText();
			if( searchString.equals(I18n.getString("OpenNewS4GameDialog.MAPBASE_INPUT_PLACEHOLDER")) ) {
				searchString = "";
			}
		}
		
		// reset the filter for the result-table
		mapbaseResultsTable.setRowSorter(null);
		
		// get the table-model
		DefaultTableModel dm = (DefaultTableModel) mapbaseResultsTable.getModel();
		
		// remove all previous results from table-model
		this.removeRowsFromTable(mapbaseResultsTable, dm);
		
		// check for minimum-size for search-string: 3 or more
		if( searchString.length() > 2 ) {
			
			final String mySearchString = searchString;
			
			// show a waiting-text in first column of this table
			dm.addRow(new Object[]{
				I18n.getString("GUI.PLEASE_WAIT")
			});
			
			// get a separate window with waiting-message
			LoadingJFrame waitingWindow = new LoadingJFrame(this.parentFrame);
			
			SwingWorker<?, ?> swingWorker = new SwingWorker<Object, Object>() {

				@Override
				protected Object doInBackground() throws Exception {
					String response = "";
					
					// define the request
		        	httpRequests httpRequests = new httpRequests();
		        	// -> set parameter to configure the JSON-response
		        	httpRequests.addParameter("json", "1");
		        	// -> ignore security-token
		        	httpRequests.setIgnorePhpSessId(true);
		        	// -> show a progess-minitor
		        	httpRequests.setShowProgressMonitor(true);
		        	// -> set the url
		        	String url = "";
					try {
						// encode the search-string and add it to the url
						url = settlersLobby.config.getDefaultMapSearch() + URLEncoder.encode(mySearchString, "UTF-8");
					} catch (UnsupportedEncodingException e2) {
						e2.printStackTrace();
					}
					httpRequests.setUrl(url);
					// -> get the response-String
					try {
						response = httpRequests.doRequest();
					} catch (IOException e) {
					}
					
					if( httpRequests.isError() ) {
						
						// remove all previous results from table-model
						removeRowsFromTable(mapbaseResultsTable, dm);
						
						// show a hint that no results where found
						dm.addRow(new Object[]{
							I18n.getString("OpenNewS4GameDialog.ERROR_CONNECTION_LINE_1")
						});
						dm.addRow(new Object[]{
							I18n.getString("OpenNewS4GameDialog.ERROR_CONNECTION_LINE_2")
						});
					}
								
					// if response contains contents, then decrypt the JSON-format
					// and show the resulting map-data in the mapbase-table
					if( response.length() > 0 ) {
						
						// remove all previous results from table-model
						removeRowsFromTable(mapbaseResultsTable, dm);
						
						JSONObject obj = new JSONObject(response);
						if( !obj.isNull("maps") ) {
							mapbaseResultsTable.addMouseListener(mouseAdapterToCreateGame);
							// insert the results in table
							JSONArray mapArray = obj.getJSONArray("maps");
							setMapsIntoTable(mapArray, mapbaseResultsTable);
						}
						else {
							// show a hint that no results where found
							dm.addRow(new Object[]{
								I18n.getString("OpenNewS4GameDialog.ERROR_NORESULTS")
							});
						}
					}
					else {
						
						if( !httpRequests.isError() ) {
							// remove all previous results from table-model
							removeRowsFromTable(mapbaseResultsTable, dm);
							
							// show a hint that there was a connection-error
							dm.addRow(new Object[]{
								I18n.getString("OpenNewS4GameDialog.ERROR_NORESULTS_2")
							});
						}
					}
					
					return null;
				}
				
				/**
		         * Executed in event dispatching thread
		         */
		        @Override
		        public void done() {
		        	// close the waiting window
		        	waitingWindow.dispatchEvent(new WindowEvent(waitingWindow, WindowEvent.WINDOW_CLOSING));
		        }
				
			};
			
			swingWorker.execute();
			
		}
		else if( searchString.length() > 0 ) {
			// show a hint that the user must insert a searchstring
			dm.addRow(new Object[]{
				I18n.getString("OpenNewS4GameDialog.ERROR_MINCHARS")
			});
		}
		else {
			// show a hint that the user must insert a searchstring
			dm.addRow(new Object[]{
				I18n.getString("OpenNewS4GameDialog.ERROR_NOSEARCHSTRING")
			});
		}
	}
	
	/**
	 * Returns the actual chosen mapname.
	 * 
	 * @return String
	 */
	public String getChosenMapName() {
		return chosenMapName;
	}
	
	/**
	 * Load the table-contents depending on active tab in navigation. 
	 * 
	 * @param activeTab
	 * @return void
	 */
	private void loadMapsIntoTables( Component activeTab ) {
		
		// go through each table and unselect rows, if some is selected
		for( JTable object: tableList )
		{
			if( object.getRowCount() > 0 && object.getSelectedRowCount() > 0 ) {
				object.clearSelection();
			}
		}
		
		this.previewPanel.setVisible(false);
		
		// depending on actual tab-name load its contents
		switch( activeTab.getName() ) {
		
			// favorites-tab
			case "favoriteMaps":
				loadFavoriteMaps();
				break;
				
			// mapbase-tab 
			case "mapbase":
				mapbaseResultsTable.setRowSorter(null);
				// show a hint to the searchfield if table is empty
				if( mapbaseResultsTable.getRowCount() == 0 ) {
					// remove mouse-listener to prevent irritations
					mapbaseResultsTable.removeMouseListener(mouseAdapterToCreateGame);
					// add this map to table with output-values (like map-name)
					DefaultTableModel model = (DefaultTableModel) mapbaseResultsTable.getModel();
					model.addRow(new Object[]{
							I18n.getString("OpenNewS4GameDialog.MAPBASE_HINT_SEARCHFIELD")
					});
				}
				// set cursor in searchfield, so that the user can directly input his search-key
				searchTextField.requestFocusInWindow();
				break;
				
			// multiplayer-tab
			case "multiplayer":
				loadMultiplayerMaps();
				break;
				
			// multiplayer-tab
			case "usermaps":
				loadUserMaps();
				break;
				
			// savegame-tab
			case "s4savegames":
				loadSavegames();
				break;
				
		}
		
		// make preview-panel invisible
		previewPanel.setVisible(false);
		
		setPreferredSize(new Dimension(getPreferredSize().width, defaultWindowHeight));
		openNewGameDialog.pack();	
	}
	
	/**
	 * Load the favorite Maps into table.
	 */
	private void loadFavoriteMaps() {
		favoritesTable.setRowSorter(null);
		// if table is empty get the favorite maps and put them into the table
		if( favoritesTable.getRowCount() == 0 ) {
			// get the maps
			Object[] favoriteMapList = getFavoriteMaps();
			// if no favoriteMaps available the user must have a new installation or is completly new
			// -> show a nice hint :-)
			if( favoriteMapList.length == 0 ) {
				// remove mouse-listener to prevent irritations
				favoritesTable.removeMouseListener(mouseAdapterToCreateGame);
				DefaultTableModel model = (DefaultTableModel) favoritesTable.getModel();
				model.addRow(new Object[]{
					I18n.getString("OpenNewS4GameDialog.NOFAVORITEMAPS_LINE_1")
				});
				model.addRow(new Object[]{
					I18n.getString("OpenNewS4GameDialog.NOFAVORITEMAPS_LINE_2")
				});
			}
			else {
				// set the mouse-listener to start games via click
				favoritesTable.addMouseListener(mouseAdapterToCreateGame);

				// put maps into table
				setMapsIntoTable( favoriteMapList, favoritesTable );
			}
		}
	}
	
	/**
	 * Load multiPlayer maps.
	 */
	private void loadMultiplayerMaps() {
		multiplayerTable.setRowSorter(null);
		if( multiplayerTable.getRowCount() == 0 ) {
			setMapsIntoTable( getMultiplayerMaps(), multiplayerTable, ALobbyConstants.PATH_S4MULTI + File.separator );
		}
	}
	
	/**
	 * Load usermaps.
	 */
	private void loadUserMaps() {
		usermapsTable.setRowSorter(null);
		if( usermapsTable.getRowCount() == 0 ) {
			setMapsIntoTable( getUserMaps(), usermapsTable, ALobbyConstants.PATH_S4USER + File.separator );
		}
	}
	
	/**
	 * Load savegames.
	 */
	private void loadSavegames() {
		savegameTable.setRowSorter(null);
		if( savegameTable.getRowCount() == 0 ) {
			setMapsIntoTable( getSavegames(), savegameTable, ALobbyConstants.PATH_S4SAVEGAMES + File.separator );
		}
	}
	
	/**
	 * Remove all rows from a JTable-Model.
	 * @param model 
	 * @param table 
	 * 
	 * @param JTable
	 */
	private void removeRowsFromTable(JTable table, DefaultTableModel model) {
		if( table.getRowCount() > 0 ) {
			for (int i = table.getRowCount() - 1; i >= 0; i--) {
				model.removeRow(i);
			}
		}
	}
	
	/**
	 * Wrapper to load local existing maps for each tab and their data on aLobby-Start.
	 * 
	 * Without this preloading the new-game-dialog should be unusable for players with many maps
	 * in their s4-directory.
	 */
	public void loadMaps() {
		Runnable runnable = new Runnable(){
			public void run(){
				// load the maps for each table
				loadFavoriteMaps();
				loadMultiplayerMaps();
				loadSavegames();
				loadUserMaps();
			}
		};
		Thread thread = new Thread(runnable);
		thread.setName("loadS4MapsThread");
		thread.start();
	}
	
	/**
	 * Show the S4-Language specific string to choose the goods
	 * at gamestart as combobox.
	 *
	 * @author Zwirni
	 *
	 */
	private static class GoodsInStockComboBoxListRenderer extends DefaultListCellRenderer
	{
        @Override
        public Component getListCellRendererComponent(JList<?> list, Object value,
                int index, boolean isSelected, boolean cellHasFocus)
        {
            Component result = super.getListCellRendererComponent(
                    list, value, index, isSelected, cellHasFocus);
            if (value instanceof GoodsInStock &&
                result instanceof DefaultListCellRenderer)
            {
                ((DefaultListCellRenderer)result).setText(
                        ((GoodsInStock)value).getLanguageString());
            }
            return result;
        }
	}
	
	// get Mapname from a JTable-object with added rowsorter (or without)
	private String getMapNameFromTableWithRowSorter(JTable object, int i) {
		if( object.getRowSorter() != null ) {
			i = object.convertRowIndexToModel(i);
		}
		return object.getModel().getValueAt(i, 0).toString();
	}

}
