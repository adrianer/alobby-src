/* This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.Base64;
import java.util.Locale;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.json.JSONObject;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.util.httpRequests;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;


/**
 * Creates a window with informations to a single S3-Map
 * including a very big preview-picture.
 * All visible data will be loaded via http from mapbase. Without connection to mapbase
 * nothing will be visible.
 * 
 * Usage:
 * 		// generate ShowMapDetail-Object
 * 		JFrame mapdetail = new ShowMapDetail(mapname);
 *
 * @author Zwirni
 */
public class ShowMapDetail extends JDialog {
	
	private ShadowForJPanel pnlPreviewImage;
	private LinkLabel lblmapname;
	private SettlersLobby settlersLobby;
	private JLabel lblclose;
	private JLabel lblopenMap = null;
	private JFrame parentFrame;
	private boolean showMapChoosingDialogOption = false;
	
	/**
	 * Get and show the single window with map-informations.
	 * 
	 * @param mainWindow				the main-window-object
	 * @param settlersLobby				the settlersLobby-object
	 * @param mapname					the name of the map as plain-name
	 * @param openInMapChoosingDialog	if the option to open the map in mapchoosingdialog should be visible or not
	 */
	public ShowMapDetail( JFrame parentFrame, SettlersLobby settlersLobby, String mapname, boolean openInMapChoosingDialog ) {
				
		this.settlersLobby = settlersLobby;
		this.parentFrame = parentFrame;
		this.showMapChoosingDialogOption = openInMapChoosingDialog;
		
		// define the dimension for the preview-image-panel
		int width = (int) (850 * MessageDialog.getDpiScalingFactor());
		int height = (int) (574 * MessageDialog.getDpiScalingFactor());
		
		// set the size and some other parameters
		setSize(new Dimension(width, height));
		setPreferredSize(new Dimension(width, height));
		setMinimumSize(new Dimension(width, height));
		setLocale(Locale.GERMANY);
		setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		setResizable(false);
		setUndecorated(true);
		setBackground(new Color(0, 255, 0, 0));
		setVisible(false);
		setAlwaysOnTop(true);
		getContentPane().setLayout(null);
		// close this window really
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		// get fontAweSome
		Font font = settlersLobby.config.getFontAweSome((int) (24 * MessageDialog.getDpiScalingFactor()));
			
		/**
		 * Create a label where the "X" for closing the layer will be placed
		 * and set width, height and paddings.
		 */
		lblclose = new JLabel();
		lblclose.setLayout(null);
		lblclose.setFont(font);
		lblclose.setText("\uf00d");
		lblclose.setBackground(new Color(0, 0, 0, 0));
		lblclose.setBorder(new EmptyBorder(0, 0, 0, 0));
		lblclose.setBounds(width - (int) (36 * MessageDialog.getDpiScalingFactor()), (int) (12 * MessageDialog.getDpiScalingFactor()), (int) (18 * MessageDialog.getDpiScalingFactor()), (int) (18 * MessageDialog.getDpiScalingFactor()));
		lblclose.setSize(new Dimension((int) (24 * MessageDialog.getDpiScalingFactor()), (int) (24 * MessageDialog.getDpiScalingFactor())));
		lblclose.setPreferredSize(new Dimension((int) (24 * MessageDialog.getDpiScalingFactor()), (int) (24 * MessageDialog.getDpiScalingFactor())));
		lblclose.setMinimumSize(new Dimension((int) (24 * MessageDialog.getDpiScalingFactor()), (int) (24 * MessageDialog.getDpiScalingFactor())));
		lblclose.setOpaque(true);
		// set the mouse-listener which will wait for click or mouse-moving
		lblclose.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// click the jFrame on click
				settlersLobby.getGUI().showMapDetail = null;
				dispose();
			}
			// if mouse enters the button change the cursor and the font-color
            public void mouseEntered(java.awt.event.MouseEvent evt) {
            	// set the cursor to a hand-cursor
            	lblclose.setForeground(Color.red);
            	lblclose.setCursor(new Cursor(Cursor.HAND_CURSOR));
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
            	// set the cursor back to standard-cursor
            	lblclose.setForeground(settlersLobby.theme.getTextColor());
            	lblclose.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }
		});
		getContentPane().add(lblclose);

		if( false != this.showMapChoosingDialogOption ) {
			lblopenMap = new JLabel();
			lblopenMap.setBorder(new EmptyBorder(0, 0, 0, 0));
			lblopenMap.setBounds(12, 42, (int) (120 * MessageDialog.getDpiScalingFactor()), (int) (32 * MessageDialog.getDpiScalingFactor()));
			lblopenMap.setSize(new Dimension((int) (240 * MessageDialog.getDpiScalingFactor()), (int) (32 * MessageDialog.getDpiScalingFactor())));
			lblopenMap.setOpaque(true);
			lblopenMap.setHorizontalAlignment(SwingConstants.CENTER);
			lblopenMap.setVerticalAlignment(SwingConstants.CENTER);
			lblopenMap.setText(I18n.getString("ShowMapDetail.TITLE_OPEN"));
			lblopenMap.addMouseListener(new MouseAdapter() {
				@Override
 	            public void mouseClicked(MouseEvent e) {
					// enable s3-tab in openmapchoosingdialog-window
					settlersLobby.getGUI().displayMapChoosingDialog();
 	            	OpenNewGameDialog mapChoosingDialog = settlersLobby.getGUI().getMapChoosingDialog();
 	            	mapChoosingDialog.getTabListe().setSelectedIndex(0);
 	            	// and force it into the front
			    	java.awt.EventQueue.invokeLater(new Runnable() {
			    	    @Override
			    	    public void run() {
			    	    	mapChoosingDialog.toFront();
			    	    	mapChoosingDialog.repaint();
			    	    }
			    	});
			    	// get the s3-dialog-object
			    	OpenNewS3GameDialog s3PanelDialogObject = settlersLobby.getGUI().s3PanelDialogObject;
			    	// enable the mapbase-tab there
			    	s3PanelDialogObject.getTabbedPane().setSelectedIndex(2);
			    	// get the searchfield
			    	JTextField searchfield = s3PanelDialogObject.getMapBaseSearchField();
			    	// and add the mapname
			    	searchfield.setText(mapname);
			    	// start search
			    	s3PanelDialogObject.getMapsFromMapBaseBySearchString();
			    	// close the detail-window
			    	dispose();
 	            }
				// if mouse enters the button change the cursor and the font-color
	            public void mouseEntered(java.awt.event.MouseEvent evt) {
	            	// set the cursor to a hand-cursor
	            	lblopenMap.setForeground(Color.red);
	            	lblopenMap.setCursor(new Cursor(Cursor.HAND_CURSOR));
	            }

	            public void mouseExited(java.awt.event.MouseEvent evt) {
	            	// set the cursor back to standard-cursor
	            	lblopenMap.setForeground(settlersLobby.theme.getTextColor());
	            	lblopenMap.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
	            }
 	        });
			getContentPane().add(lblopenMap);
		}
		
		// define and add the label for the mapname
		lblmapname = new LinkLabel();
		lblmapname.setBorder(new EmptyBorder(0, 0, 0, 0));
		lblmapname.setBounds(12, 8, 0, 0);
		lblmapname.setSize(new Dimension((int) (240 * MessageDialog.getDpiScalingFactor()), (int) (32 * MessageDialog.getDpiScalingFactor())));
		lblmapname.setOpaque(true);
		lblmapname.setHorizontalAlignment(SwingConstants.CENTER);
		lblmapname.setVerticalAlignment(SwingConstants.CENTER);
		getContentPane().add(lblmapname);
		
		// define and add the panel for the preview-image
		pnlPreviewImage = new ShadowForJPanel(8);
		pnlPreviewImage.setSize(new Dimension(width, height));
		pnlPreviewImage.setPreferredSize(new Dimension(width, height));
		pnlPreviewImage.setMinimumSize(new Dimension(width, height));
		pnlPreviewImage.setBorder(new EmptyBorder(0, 0, 0, 0));
		pnlPreviewImage.setOpaque(true);
		getContentPane().add(pnlPreviewImage);
		
		// get and show the map-data
		getMapDataByName( mapname );
		
	}

	/**
	 * Load the map-data and insert it in the window-panels.
	 * 
	 * @param mapname
	 */
	private void getMapDataByName( String mapname ) {
		if( mapname.length() > 0 ) {
			
			String hexcolor = SettlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor());
			
			// get a window with waiting-message
			LoadingJFrame waitingWindow = new LoadingJFrame(this.parentFrame);
			
			// define a swingWorker which will show a loading-window while http-request is not done
			SwingWorker<?, ?> swingWorker = new SwingWorker<Object, Object>() {
			
				// save the link for this map
				String link = "";
				
				@Override
				protected Object doInBackground() throws Exception {
				
					// get the informations for the map
					String response = "";
					
					// define the request
			    	httpRequests httpRequests = new httpRequests();
			    	// -> set parameter to configure the JSON-response
			    	httpRequests.addParameter("json", "1");
			    	// -> ignore security-token
			    	httpRequests.setIgnorePhpSessId(true);
			    	// -> set the url
			    	String url = "";
					try {
						// encode the mapname and add it to the url
						url = settlersLobby.config.getDefaultGetMapData() + URLEncoder.encode(mapname, "UTF-8");
					} catch (UnsupportedEncodingException e2) {
						e2.printStackTrace();
					}
					httpRequests.setUrl(url);
					// -> get the response-String
					try {
						response = httpRequests.doRequest();
					} catch (IOException e) {
                        Test.outputException(e);
					}
					
					// show only informations for this map if no error happend
					// during the http-request
					// -> if an error came up, show no hint therefore because it would be an endless windows-messaging-loop
					//    if users clicks again and again ..
					boolean previewPanelVisibility = false;
					if( !httpRequests.isError() ) {
						        
						byte[] img = null;
						
						if( response.length() > 0 ) {
							JSONObject obj = new JSONObject(response);
							if( !obj.isNull("link") ) {
								link = obj.getString("link");
								previewPanelVisibility = true;
							}
							if( !obj.isNull("img") ) {
								String imgBase64 = obj.getString("img");
								img = Base64.getDecoder().decode(imgBase64);
								previewPanelVisibility = true;
							}
						}
					
						// get image from blob as byte-Array
						ByteArrayInputStream bais = new ByteArrayInputStream(img);
					    BufferedImage originalBufferedImage;
						try {
		
							// read image from Byte-Array
							originalBufferedImage = ImageIO.read(bais);
							
							// set the designated width for the image
							// -> do not set this to the width and height of the wrapping panel
							//    because the resulting image will be bigger than the image itself
							// -> height will be calculated depending on size of the original image
					        int thumbnailWidth = (int) (740 * MessageDialog.getDpiScalingFactor());
					        int thumbnailHeight = (int) (540 * MessageDialog.getDpiScalingFactor());
					        
					        // calculate the width of the resulting image on-the-fly
					        int widthToScale = (int)(thumbnailWidth);
					        int heightToScale = (int)((widthToScale * originalBufferedImage.getHeight() ) / originalBufferedImage.getWidth());
					        
					        if( originalBufferedImage.getWidth() > thumbnailWidth ) {
					        	widthToScale = (int)(thumbnailWidth);
					        	heightToScale = (int)((widthToScale * originalBufferedImage.getHeight() ) / originalBufferedImage.getWidth());
					        }
					        
					        if( originalBufferedImage.getHeight() > thumbnailHeight ) {
					        	widthToScale = (int)((heightToScale * originalBufferedImage.getWidth()) / originalBufferedImage.getHeight());
					        	heightToScale = (int)(thumbnailHeight);
					        }
					        
					        // create the target image without contents
					        BufferedImage resizedImage = new BufferedImage(widthToScale, heightToScale, originalBufferedImage.getType());
				        	Graphics2D g = resizedImage.createGraphics();
				        	// -> set alphacomposite 
				        	g.setComposite(AlphaComposite.Src);
				        	// -> set rendering-conditions
				        	g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
				        	g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
				        	g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				        	// -> draw the image
				        	g.drawImage(originalBufferedImage, 0, 0, widthToScale, heightToScale, null);
				        	// -> forget the result :]
				        	g.dispose();
				        	
				        	// add the bufferedImage as image to the panel
				        	pnlPreviewImage.add(new JLabel(new ImageIcon(resizedImage)));
			        	
					    } catch (IOException e) {
					        throw new RuntimeException(e);
					    }
						
			        	// set the mapname including link
			        	lblmapname.setText(MessageFormat.format("<html><a href=''" + link + "'' style=''color: {0}''>" + mapname + "</a></html>", hexcolor));
			        				        	
					} else {
                        Test.output("requests returned an error!");
                        done();
                        throw new RuntimeException("requests returned an error!");
                    }
					return previewPanelVisibility;
				}

			
				/**
		         * Executed in event dispatching thread
		         */
		        @Override
		        public void done() {
		        	
		        	pnlPreviewImage.setVisible(true);
		        	lblmapname.setVisible(true);
		        	if( lblopenMap != null ) {
		        		lblopenMap.setVisible(true);
		        	}
		        	lblclose.setVisible(true);
		        	
		        	// show the preview-window if map and preview-image could be found on mapbase
		        	if( pnlPreviewImage.getComponentCount() > 0 ) {
		        		pack();
		        		setLocationRelativeTo(parentFrame);
		        		setVisible(true);
		        	}
		        	else {
		        		// show a hint that this map could not be found on mapbase
		        		
		        		// load text-color of actually used theme as Color-Object
		            	String hexcolor = SettlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor());
		            	
		            	String text = I18n.getString("GUI.MAP_IMAGE_DOES_NOT_EXISTS_ON_MAPBASE");
		            	if( link.length() > 0 ) {
		            		text = text + "<br>" + I18n.getString("GUI.MAP_IMAGE_DOES_NOT_EXISTS_ON_MAPBASE_LINE_2");
		            	}
		            	
		                // use MessageDialog to show the hin
		     			MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
		     			if( link.length() > 0 ) {
			             	messagewindow.addMessage(
			             		MessageFormat.format("<html>" + text + "</html>", mapname, link, hexcolor),
			             		UIManager.getIcon("OptionPane.errorIcon")
			             	);
		     			}
		     			else {
		     				messagewindow.addMessage(
			             		MessageFormat.format("<html>" + text + "</html>", mapname),
			             		UIManager.getIcon("OptionPane.errorIcon")
			             	);
		     			}
		             	messagewindow.addButton(
		             		UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
		             		new ActionListener() {
		             		    @Override
		             		    public void actionPerformed(ActionEvent e) {
		             		    	// close this window
		        			    	messagewindow.dispose();
		             		    }
		             		}
		             	);
		             	messagewindow.setVisible(true);
		        	}
		        	
		        	// close the waiting window
		        	waitingWindow.dispatchEvent(new WindowEvent(waitingWindow, WindowEvent.WINDOW_CLOSING));
		        }
				
			};
			
			swingWorker.execute();
							
		}
	}
	
	@Override
	public void paint(Graphics g) {
	    super.paint(g);
	    Rectangle screen = this.getGraphicsConfiguration().getBounds();
	    this.setLocation(
	        screen.x + (screen.width - this.getWidth()) / 2,
	        screen.y + (screen.height - this.getHeight()) / 2
	    );
	}
	
}