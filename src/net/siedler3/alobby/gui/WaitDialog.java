package net.siedler3.alobby.gui;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Label;

import javax.swing.JFrame;
import javax.swing.JLabel;

import net.siedler3.alobby.controlcenter.SettlersLobby;

public class WaitDialog extends Dialog {

	private SettlersLobby settlersLobby;

	public WaitDialog(Dialog owner) {
		super(owner);
	}
	
	@SuppressWarnings("static-access")
	public WaitDialog(SettlersLobby settlersLobby, JFrame currentActiveFrame, String string, boolean b, String label) {
		super(currentActiveFrame, string, b);
		this.settlersLobby = settlersLobby;
		Label realLabel = new Label(label);
		// AWT Label does not use the JLabel size set by the UIManager - we have to set it manually
		realLabel.setFont(new JLabel().getFont());
		add(realLabel);
		setIconImage(this.settlersLobby.getGUI().getIcon());
		setBackground(this.settlersLobby.theme.lobbyBackgroundColor);
		setForeground(this.settlersLobby.theme.getTextColor());
        setAlwaysOnTop(true);
        setMinimumSize(new Dimension((int) (200 * MessageDialog.getDpiScalingFactor()), (int) (70 * MessageDialog.getDpiScalingFactor())));
		pack();
		setLocation(this.settlersLobby.getGUI().getCenterLocationForWindow(this));
	}
	
}
