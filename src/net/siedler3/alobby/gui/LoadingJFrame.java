/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */

package net.siedler3.alobby.gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRootPane;

import net.siedler3.alobby.controlcenter.ALobbyConstants;

/**
 * Define a JFrame which can be used as window with loading-message
 * 
 * @author Zwirni
 *
 */
public class LoadingJFrame extends JFrame {

	// optimize borderline
    protected int strokeSize = 1;
    
    // shadow-color
    protected Color shadowColor = Color.black;
    
    // drop shadow exists?
    protected boolean shady = true;
    
    // set the radius
    protected Dimension radius = new Dimension(24, 24);
    
    // distance between shadow and border
    protected int shadowGap = 2;
    
    // shadow-offset
    protected int shadowOffset = 6;
    
    // shadow transparency
    protected int shadowAlpha = 120;
    
    // parent Frame
    protected JFrame parentFrame = null;
	
	/**
	 * Open a JFrame with an animated loading.gif and minimal window-decorations.
	 * 
	 * @param parentFrame 
	 */
	public LoadingJFrame( JFrame parentFrame ) {
	
		// set the parentFrame		
		this.parentFrame = parentFrame;
		
		// get the loading.gif als ImageIcon for a JLabel
		ImageIcon icon = new ImageIcon(this.getClass().getResource(ALobbyConstants.PACKAGE_PATH + "/s3gameinterface/img/loading.gif"));
		JLabel hint = new JLabel();
		hint.setIcon(icon);
		
		// define a wrapper for this JFrame
		// and overwrite its paintComponent to show rounded corners surrounding the loading.gif
		JPanel hintWrapper = new JPanel() {
			protected void paintComponent(Graphics g) {
			       super.paintComponent(g);
			       
			       int width = getWidth();
			       int height = getHeight();
			       Color shadowColorA = new Color(shadowColor.getRed(), shadowColor.getGreen(), shadowColor.getBlue(), shadowAlpha);
			       Graphics2D graphics = (Graphics2D) g;
			
		           graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			
			       // draw the shadow border
			       if (shady) {
			           graphics.setColor(shadowColorA);
			           graphics.fillRoundRect(
		        		   shadowOffset,// X position
		                   shadowOffset,// Y position
		                   width - strokeSize - shadowOffset, // width
		                   height - strokeSize - shadowOffset, // height
		                   radius.width, radius.height);// radius Dimension
			       } else {
			           shadowGap = 1;
			       }
			
			       // draw rounded corners
			       graphics.setColor(getBackground());
			       graphics.fillRoundRect(0, 0, width - shadowGap, height - shadowGap, radius.width, radius.height);
			       graphics.setColor(getForeground());
			       graphics.setStroke(new BasicStroke(strokeSize));
			       graphics.drawRoundRect(0, 0, width - shadowGap, height - shadowGap, radius.width, radius.height);
			
			       //Sets strokes to default, is better.
			       graphics.setStroke(new BasicStroke());
			}
		};
		hintWrapper.setOpaque(false);
		hintWrapper.add(hint);
		hintWrapper.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
		hintWrapper.setBackground( Color.white );
		
		// define the window itself
		dispose();
		setSize(80,40);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setUndecorated(true);
		getRootPane().setWindowDecorationStyle(JRootPane.NONE);
		add(hintWrapper);
		pack();
		setLocationRelativeTo(this.parentFrame);
		
		// show it
		setVisible(true);

	}
	
	@Override
	public void paint(Graphics g) {
	    super.paint(g);
	    Rectangle screen = this.getGraphicsConfiguration().getBounds();
	    this.setLocation(
	        screen.x + (screen.width - this.getWidth()) / 2,
	        screen.y + (screen.height - this.getHeight()) / 2
	    );
	}
	
}