/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.MessageFormat;

import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;

import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.Configuration;
import net.siedler3.alobby.controlcenter.GoodsInStock;
import net.siedler3.alobby.controlcenter.ISMap;
import net.siedler3.alobby.controlcenter.OpenGame.MiscType;
import net.siedler3.alobby.controlcenter.S3Map;
import net.siedler3.alobby.controlcenter.S3RandomMap;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.controlcenter.download.CheckAndDownloadMapFile;
import net.siedler3.alobby.gui.additional.MouseWheelListenerForJTabbedPane;
import net.siedler3.alobby.gui.additional.TableCellCenterRenderer;
import net.siedler3.alobby.gui.additional.addCursorChangesToButton;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.s3gameinterface.GameStarter;
import net.siedler3.alobby.s3gameinterface.GameStarter.StartOptions;
import net.siedler3.alobby.util.ArraySupport;
import net.siedler3.alobby.util.httpRequests;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.Component;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicTabbedPaneUI;

import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;

import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.RowFilter;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import javax.swing.JCheckBox;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

import org.eclipse.wb.swing.FocusTraversalOnArray;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JTextPane;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.JScrollPane;

/**
 * Create the S3-OpenGameDialog as JPanel which will be insert into the OpenNewGameDialog-JFrame.
 * Includes the following options for user who will create a new S3-game:
 * - define a game-name (visible in aLobby)
 * - choose the goods
 * - set the tools-property
 * - set random-position
 * - set property to navigate to minichat on game-opening
 * - choose map from local S3-directory
 * -> including considering the different sub-directories
 * - search and choose a map in MapBase (https://mapbase.DOMAIN)
 * -> including download of map if necessary
 * - show preview-image of chosen map (local and from MapBase)
 * -> including basic information about the map presented vom MapBase
 *
 * @author Zwirni
 *
 */
public class OpenNewS3GameDialog extends JPanel {

	private SettlersLobby settlersLobby;
	private JTextField gamenameTextfield;
	private JTable multiplayerTable;
	private JTextField searchTextField;
	private JTextField searchTextFieldUserMap;
	private JTable usermapsTable;
	private JTable favoritesTable;
	private JFrame parentFrame;
	private JPanel gamenamePane;
	private JTable randommapTable;
	private JTable mapbaseResultsTable;
	// disable this line for using windowbuilder in eclipse
	private JComboBox<GoodsInStock> goodsInStock = new JComboBox<GoodsInStock>(GoodsInStock.values());
	// enable disable this line for using windowbuilder in eclipse
	//private JComboBox<GoodsInStock> goodsInStock;
	private JCheckBox chckbxRandomPosition;
	private JCheckBox chckbxGoToMiniChat;
	private JCheckBox chckbxLeague;
	private JTabbedPane tabbedPane;
	private OpenNewGameDialog openNewGameDialog;
	private JCheckBox chckBxWiMo;
	private JButton searchBtn;
	private JButton searchBtnUserMap;
	private JButton resetBtnUserMap;
	private String chosenMapName;
	private JScrollPane mapbaseResultsScrollPane;
	private JScrollPane favoritesScrollPane;
	private JScrollPane randommapScrollPane;
	private JScrollPane usermapsScrollPane;
	private JScrollPane multiplayerScrollPane;
	private ArrayList<JTable> tableList;
	private JPanel pnlCreator;
	private JButton btnStartGame;
	private JButton btnSuggestMap;
	private JButton btnStartRandomGame;
	private JButton btnCancel;
	private JPanel previewPanel;
	private JTextPane txtMapSize;
	private JTextPane txtMapDate;
	private JTextPane txtMapPlayer;
	private JPanel pnlLink;
	private JPanel pnlRating;
	private JPanel pnlPreviewImage;
	private int defaultWindowHeight;
	private JPanel pnlLeagueTournament;
	private JCheckBox chckbxTournament;
	private String tournamentid = "";
	private String tournamentname;
	private String round;
	private String groupnumber;
	private String matchnumber;
	private String playerlist;
	private JCheckBox leagueRules;
	private JButton leagueRulesOkButton;
	private LinkLabel messageWithLeagueRulesLink;
	private String chosenMapLink = "";
	private JPanel usermapsPane = null;
    private JRadioButton radioS3VersionCDorGOG;
    private JRadioButton radioS3VersionGE;
    private JRadioButton radioS3VersionCE;

	// initialize the object
	public OpenNewS3GameDialog( SettlersLobby settlersLobby, OpenNewGameDialog openNewGameDialog) {

		/**
		 * This dialog-window was created with Windows Builder.
		 * After install Window Builder right-click on this file
		 * and choose Open With > Window Builder to edit this settings.
		 */

		// set the size for the form
		setSize(new Dimension((int) (530 * MessageDialog.getDpiScalingFactor()), (int) (380 * MessageDialog.getDpiScalingFactor())));
		setPreferredSize(new Dimension((int) (640 * MessageDialog.getDpiScalingFactor()), (int) (420 * MessageDialog.getDpiScalingFactor())));
		setMinimumSize(new Dimension((int) (530 * MessageDialog.getDpiScalingFactor()), (int) (380 * MessageDialog.getDpiScalingFactor())));
		setLocale(Locale.GERMANY);
		setAutoscrolls(true);
		setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		this.defaultWindowHeight = (int) (410 * MessageDialog.getDpiScalingFactor());
		this.settlersLobby = settlersLobby;
		this.openNewGameDialog = openNewGameDialog;

		// set properties for this panel
		this.setAlignmentX(LEFT_ALIGNMENT);
		this.setAlignmentY(TOP_ALIGNMENT);

		gamenamePane = new JPanel();
		gamenamePane.setAlignmentX(0.0f);
		gamenamePane.setAlignmentY(Component.TOP_ALIGNMENT);

		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.addMouseWheelListener(new MouseWheelListenerForJTabbedPane());

		JPanel optionsPanel = new JPanel();

		JPanel buttonPanel = new JPanel();
		buttonPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		btnStartGame = new JButton(I18n.getString("OpenNewS3GameDialog.BUTTON_STARTGAME"));
		btnStartRandomGame = new JButton(I18n.getString("OpenNewS3GameDialog.BUTTON_STARTRANDOMGAME"));
		btnCancel = new JButton(I18n.getString("OpenNewS3GameDialog.BUTTON_CANCEL"));
		btnSuggestMap = new JButton(I18n.getString("OpenNewS3GameDialog.BUTTON_SUGGEST"));
		// add repeating backgroundImage to the buttons, if defined for actual theme
		if( settlersLobby.theme.isRepeatingButtonStyleAvailable() ) {
			btnStartGame = settlersLobby.theme.getButtonWithStyle(btnStartGame.getText());
			btnStartRandomGame = settlersLobby.theme.getButtonWithStyle(btnStartRandomGame.getText());
			btnCancel = settlersLobby.theme.getButtonWithStyle(btnCancel.getText());
			btnSuggestMap = settlersLobby.theme.getButtonWithStyle(btnSuggestMap.getText());
		}

		// add specific backgroundImage to the buttons, if defined for actual theme
		// -> all together for consistent view
		if( settlersLobby.theme.isButtonStyleAvailable() ) {
			btnStartGame = settlersLobby.theme.getButtonWithStyle(btnStartGame.getText(), settlersLobby.theme.buttonBackgroundImage1, settlersLobby.theme.buttonBackgroundImage1Hover);
			btnStartRandomGame = settlersLobby.theme.getButtonWithStyle(btnStartRandomGame.getText(), settlersLobby.theme.buttonBackgroundImage1, settlersLobby.theme.buttonBackgroundImage1Hover);
			btnCancel = settlersLobby.theme.getButtonWithStyle(btnCancel.getText(), settlersLobby.theme.buttonBackgroundImage1, settlersLobby.theme.buttonBackgroundImage1Hover);
			btnSuggestMap = settlersLobby.theme.getButtonWithStyle(btnSuggestMap.getText(), settlersLobby.theme.buttonBackgroundImage1, settlersLobby.theme.buttonBackgroundImage1Hover);
		}
		btnStartGame.addActionListener(actionListenerToCreateGame);
		btnStartRandomGame.addActionListener(actionListenerToCreateRandomGame);
		btnCancel.addActionListener(actionListenerToCloseNewGameDialog);
		btnSuggestMap.addActionListener(actionListenerToSuggestMap);

		previewPanel = new JPanel();
		previewPanel.setVisible(false);
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(previewPanel, GroupLayout.PREFERRED_SIZE, (int) (612 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, (int) (612 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE))
						.addComponent(gamenamePane, GroupLayout.DEFAULT_SIZE, (int) (612 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE))
					.addGap(20))
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(optionsPanel, 0, 0, Short.MAX_VALUE)
					.addGap(22))
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(buttonPanel, GroupLayout.PREFERRED_SIZE, (int) (608 * MessageDialog.getDpiScalingFactor()), GroupLayout.PREFERRED_SIZE)
					.addContainerGap(22, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(gamenamePane, GroupLayout.PREFERRED_SIZE, (int) (31 * MessageDialog.getDpiScalingFactor()), GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, (int) (239 * MessageDialog.getDpiScalingFactor()), GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(previewPanel, GroupLayout.PREFERRED_SIZE, (int) (263 * MessageDialog.getDpiScalingFactor()), GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(optionsPanel, GroupLayout.PREFERRED_SIZE, (int) (64 * MessageDialog.getDpiScalingFactor()), GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(buttonPanel, GroupLayout.PREFERRED_SIZE, (int) (26 * MessageDialog.getDpiScalingFactor()), GroupLayout.PREFERRED_SIZE)
					.addContainerGap((int) (181 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE))
		);

		JLabel lblMapSize = new JLabel(I18n.getString("OpenNewS3GameDialog.MAPSIZE"));

		txtMapSize = new JTextPane();
		txtMapSize.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		txtMapSize.setAutoscrolls(false);

		JLabel lblMapPlayers = new JLabel(I18n.getString("OpenNewS3GameDialog.PLAYERS"));
		lblMapPlayers.setAlignmentY(Component.TOP_ALIGNMENT);

		txtMapPlayer = new JTextPane();
		txtMapPlayer.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		txtMapPlayer.setAutoscrolls(false);

		JLabel lblMapCreator = new JLabel(I18n.getString("OpenNewS3GameDialog.MAPCREATOR"));

		JLabel lblMapDate = new JLabel(I18n.getString("OpenNewS3GameDialog.DATE"));

		txtMapDate = new JTextPane();
		txtMapDate.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		txtMapDate.setAutoscrolls(false);
		txtMapDate.setFocusable(false);

		JLabel lblRating = new JLabel(I18n.getString("OpenNewS3GameDialog.RATING"));

		pnlRating = new JPanel();
		pnlRating.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		pnlPreviewImage = new JPanel();
		// add the mouse-listener to the image-preview-panel
    	// -> if user clicks on it, the detail-view of the map (in new window) will be displayed
    	pnlPreviewImage.addMouseListener(actionListenerZoomPreview);
    	pnlPreviewImage.addMouseListener(new addCursorChangesToButton());

		JLabel lblLink = new JLabel(I18n.getString("OpenNewS3GameDialog.LINK_TO_MAPBASE"));

		pnlLink = new JPanel();

		pnlCreator = new JPanel();
		pnlCreator.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		GroupLayout gl_previewPanel = new GroupLayout(previewPanel);
		gl_previewPanel.setHorizontalGroup(
			gl_previewPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_previewPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_previewPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_previewPanel.createSequentialGroup()
							.addComponent(pnlLink, GroupLayout.DEFAULT_SIZE, (int) (592 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE)
							.addContainerGap())
						.addGroup(gl_previewPanel.createSequentialGroup()
							.addGroup(gl_previewPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(lblMapCreator, GroupLayout.PREFERRED_SIZE, (int) (102 * MessageDialog.getDpiScalingFactor()), GroupLayout.PREFERRED_SIZE)
								.addComponent(lblMapPlayers, GroupLayout.PREFERRED_SIZE, (int) (102 * MessageDialog.getDpiScalingFactor()), GroupLayout.PREFERRED_SIZE)
								.addComponent(lblMapSize, GroupLayout.PREFERRED_SIZE, (int) (102 * MessageDialog.getDpiScalingFactor()), GroupLayout.PREFERRED_SIZE)
								.addComponent(lblMapDate, GroupLayout.PREFERRED_SIZE, (int) (102 * MessageDialog.getDpiScalingFactor()), GroupLayout.PREFERRED_SIZE)
								.addComponent(lblRating, GroupLayout.PREFERRED_SIZE, (int) (102 * MessageDialog.getDpiScalingFactor()), GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_previewPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(pnlRating, GroupLayout.DEFAULT_SIZE, (int) (186 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE)
								.addComponent(pnlCreator, GroupLayout.DEFAULT_SIZE, (int) (186 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE)
								.addComponent(txtMapDate, GroupLayout.DEFAULT_SIZE, (int) (186 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE)
								.addComponent(txtMapSize, GroupLayout.DEFAULT_SIZE, (int) (186 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE)
								.addComponent(txtMapPlayer, GroupLayout.DEFAULT_SIZE, (int) (186 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE)
								.addComponent(btnSuggestMap))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(pnlPreviewImage, GroupLayout.PREFERRED_SIZE, (int) (294 * MessageDialog.getDpiScalingFactor()), GroupLayout.PREFERRED_SIZE)
							.addContainerGap())
						.addGroup(gl_previewPanel.createSequentialGroup()
							.addComponent(lblLink, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addGap((int) (520 * MessageDialog.getDpiScalingFactor())))))
		);
		gl_previewPanel.setVerticalGroup(
			gl_previewPanel.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, gl_previewPanel.createSequentialGroup()
					.addGroup(gl_previewPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_previewPanel.createSequentialGroup()
							.addGroup(gl_previewPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(lblMapSize, GroupLayout.PREFERRED_SIZE, (int) (27 * MessageDialog.getDpiScalingFactor()), GroupLayout.PREFERRED_SIZE)
								.addComponent(txtMapSize, GroupLayout.PREFERRED_SIZE, (int) (27 * MessageDialog.getDpiScalingFactor()), GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_previewPanel.createParallelGroup(Alignment.TRAILING)
								.addComponent(lblMapPlayers, GroupLayout.PREFERRED_SIZE, (int) (27 * MessageDialog.getDpiScalingFactor()), GroupLayout.PREFERRED_SIZE)
								.addComponent(txtMapPlayer, GroupLayout.PREFERRED_SIZE, (int) (27 * MessageDialog.getDpiScalingFactor()), GroupLayout.PREFERRED_SIZE))
							.addGap((int) (6 * MessageDialog.getDpiScalingFactor()))
							.addGroup(gl_previewPanel.createParallelGroup(Alignment.LEADING, false)
								.addComponent(pnlCreator, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(lblMapCreator, GroupLayout.DEFAULT_SIZE, (int) (27 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_previewPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(lblMapDate, GroupLayout.PREFERRED_SIZE, (int) (27 * MessageDialog.getDpiScalingFactor()), GroupLayout.PREFERRED_SIZE)
								.addComponent(txtMapDate, GroupLayout.PREFERRED_SIZE, (int) (27 * MessageDialog.getDpiScalingFactor()), GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_previewPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(lblRating, GroupLayout.PREFERRED_SIZE, (int) (27 * MessageDialog.getDpiScalingFactor()), GroupLayout.PREFERRED_SIZE)
								.addComponent(pnlRating, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_previewPanel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_previewPanel.createSequentialGroup()
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(btnSuggestMap)
									.addGap((int) (26 * MessageDialog.getDpiScalingFactor())))
								.addGroup(Alignment.TRAILING, gl_previewPanel.createSequentialGroup()
									.addPreferredGap(ComponentPlacement.RELATED, (int) (29 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE)
									.addComponent(lblLink, GroupLayout.PREFERRED_SIZE, (int) (27 * MessageDialog.getDpiScalingFactor()), GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)))
							.addPreferredGap(ComponentPlacement.RELATED))
						.addGroup(gl_previewPanel.createSequentialGroup()
							.addComponent(pnlPreviewImage, GroupLayout.PREFERRED_SIZE, (int) (205 * MessageDialog.getDpiScalingFactor()), GroupLayout.PREFERRED_SIZE)
							.addGap((int) (20 * MessageDialog.getDpiScalingFactor()))))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(pnlLink, GroupLayout.PREFERRED_SIZE, (int) (27 * MessageDialog.getDpiScalingFactor()), GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		GroupLayout gl_pnlRating = new GroupLayout(pnlRating);
		gl_pnlRating.setHorizontalGroup(
			gl_pnlRating.createParallelGroup(Alignment.LEADING)
				.addGap(0, (int) (184 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE)
		);
		gl_pnlRating.setVerticalGroup(
			gl_pnlRating.createParallelGroup(Alignment.LEADING)
				.addGap(0, (int) (29 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE)
		);
		pnlRating.setLayout(gl_pnlRating);
		GroupLayout gl_pnlCreator = new GroupLayout(pnlCreator);
		gl_pnlCreator.setHorizontalGroup(
			gl_pnlCreator.createParallelGroup(Alignment.LEADING)
				.addGap(0, (int) (184 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE)
		);
		gl_pnlCreator.setVerticalGroup(
			gl_pnlCreator.createParallelGroup(Alignment.LEADING)
				.addGap(0, (int) (29 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE)
		);
		pnlCreator.setLayout(gl_pnlCreator);
		GroupLayout gl_pnlLink = new GroupLayout(pnlLink);
		gl_pnlLink.setHorizontalGroup(
			gl_pnlLink.createParallelGroup(Alignment.LEADING)
				.addGap(0, (int) (372 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE)
		);
		gl_pnlLink.setVerticalGroup(
			gl_pnlLink.createParallelGroup(Alignment.LEADING)
				.addGap(0, (int) (29 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE)
		);
		pnlLink.setLayout(gl_pnlLink);
		previewPanel.setLayout(gl_previewPanel);
		groupLayout.setAutoCreateContainerGaps(true);

		GroupLayout gl_buttonPanel = new GroupLayout(buttonPanel);
		gl_buttonPanel.setHorizontalGroup(
			gl_buttonPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_buttonPanel.createSequentialGroup()
					.addComponent(btnStartGame, GroupLayout.PREFERRED_SIZE, (int) (220 * MessageDialog.getDpiScalingFactor()), GroupLayout.PREFERRED_SIZE)
					.addGap((int) (14 * MessageDialog.getDpiScalingFactor()))
					.addComponent(btnStartRandomGame, GroupLayout.PREFERRED_SIZE, (int) (220 * MessageDialog.getDpiScalingFactor()), GroupLayout.PREFERRED_SIZE)
					.addGap((int) (14 * MessageDialog.getDpiScalingFactor()))
					.addComponent(btnCancel, GroupLayout.PREFERRED_SIZE, (int) (140 * MessageDialog.getDpiScalingFactor()), GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		gl_buttonPanel.setVerticalGroup(
			gl_buttonPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_buttonPanel.createSequentialGroup()
					.addGroup(gl_buttonPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(btnStartGame, GroupLayout.DEFAULT_SIZE, (int) (24 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE)
						.addComponent(btnStartRandomGame, GroupLayout.DEFAULT_SIZE, (int) (24 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE)
						.addComponent(btnCancel))
					.addContainerGap())
		);
		buttonPanel.setLayout(gl_buttonPanel);
		buttonPanel.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{btnStartGame, btnCancel}));

        // create the options to select the edition which should be used to start the game
        // -> CD or GOG 1.60 version
        String cdOrGogText;
        if (settlersLobby.isS3Gog) {
            cdOrGogText = I18n.getString("OpenNewS3GameDialog.S3_GOG_EDITION");
        } else {
            cdOrGogText = I18n.getString("OpenNewS3GameDialog.S3_CD_EDITION");
        }
        radioS3VersionCDorGOG = new JRadioButton(cdOrGogText, false);
        radioS3VersionCDorGOG.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JRadioButton button = (JRadioButton) e.getSource();
                if( button.isSelected() ) {
                    settlersLobby.config.setS3Versionnumber(173);
                    settlersLobby.config.setGameCompability(false);
                    radioS3VersionGE.setSelected(false);
                    radioS3VersionCE.setSelected(false);
                    chckbxGoToMiniChat.setEnabled(true);
                    chckBxWiMo.setEnabled(true);
                }
            }
        });
        // -> classic-edition
        radioS3VersionGE = new JRadioButton(I18n.getString("OpenNewS3GameDialog.S3_CLASSIC_EDITION"), false);
        radioS3VersionGE.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JRadioButton button = (JRadioButton) e.getSource();
                if( button.isSelected() ) {
                    settlersLobby.config.setS3Versionnumber(173);
                    settlersLobby.config.setGameCompability(true);
                    radioS3VersionCDorGOG.setSelected(false);
                    radioS3VersionCE.setSelected(false);
                    chckbxGoToMiniChat.setEnabled(true);
                    chckBxWiMo.setEnabled(true);
                }
            }
        });
        // -> S3CE
        radioS3VersionCE = new JRadioButton(I18n.getString("OpenNewS3GameDialog.S3_COMMUNITY_EDITION"), false);
        radioS3VersionCE.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JRadioButton button = (JRadioButton) e.getSource();
                if( button.isSelected() ) {
                    settlersLobby.config.setS3Versionnumber(300);
                    settlersLobby.config.setGameCompability(true);
                    radioS3VersionCDorGOG.setSelected(false);
                    radioS3VersionGE.setSelected(false);
                    chckbxGoToMiniChat.setEnabled(false);
                    chckbxGoToMiniChat.setSelected(false);
                    chckBxWiMo.setEnabled(false);
                    chckBxWiMo.setSelected(false);
                }
            }
        });

        // mark one of the options as selected depending on chosen version 
        if( settlersLobby.config.getS3Versionnumber() >= 300 ) {
            radioS3VersionCE.setSelected(true);
            settlersLobby.config.setGameCompability(true);
        } else if( settlersLobby.config.getS3Versionnumber() >= 170 ) {
            radioS3VersionGE.setSelected(true);
            settlersLobby.config.setGameCompability(true);
        } else {
            radioS3VersionCDorGOG.setSelected(true);
            settlersLobby.config.setGameCompability(false);
        }

		chckbxRandomPosition = new JCheckBox(I18n.getString("OpenNewS3GameDialog.RANDOMPOSITIONS"));
		chckbxRandomPosition.setToolTipText(I18n.getString("OpenNewS3GameDialog.RANDOMPOSITIONS_TOOLTIP"));
		chckbxRandomPosition.setSelected(settlersLobby.config.getnewS3GameRandomPositon());

		chckbxGoToMiniChat = new JCheckBox(I18n.getString("OpenNewS3GameDialog.NAVIGATETOMINICHAT"));
		chckbxGoToMiniChat.setToolTipText(I18n.getString("OpenNewS3GameDialog.NAVIGATETOMINICHAT_TOOLTIP"));
		chckbxGoToMiniChat.setSelected(settlersLobby.config.getnewS3GameMinichat());
		// disable this option for S3 CE-hosts
		if(  radioS3VersionCE.isSelected() ) {
			chckbxGoToMiniChat.setEnabled(false);
			chckbxGoToMiniChat.setSelected(false);
		}

		chckBxWiMo = new JCheckBox(I18n.getString("OpenNewS3GameDialog.WIMO"));
		chckBxWiMo.setToolTipText(I18n.getString("OpenNewS3GameDialog.WIMO_TOOLTIP"));
		chckBxWiMo.setSelected(settlersLobby.config.getnewS3GameWimo());
		// disable this option for S3 CE-hosts
		if( radioS3VersionCE.isSelected() ) {
			chckBxWiMo.setEnabled(false);
			chckBxWiMo.setSelected(false);
		}

		pnlLeagueTournament = new JPanel();
		pnlLeagueTournament.setLayout(null);

		chckbxLeague = new JCheckBox(I18n.getString("OpenNewS3GameDialog.LEAGUEGAME"));
		chckbxLeague.setBounds(6, 0, (int) (108 * MessageDialog.getDpiScalingFactor()), (int) (23 * MessageDialog.getDpiScalingFactor()));
		chckbxLeague.setToolTipText(I18n.getString("OpenNewS3GameDialog.LEAGUEGAME_TOOLTIP"));
		chckbxLeague.setSelected(settlersLobby.config.getnewS3GameLeague());
		chckbxLeague.setVisible(true);
		chckbxTournament = new JCheckBox("Turnier-Spiel");
		chckbxTournament.setBounds(6, 0, (int) (108 * MessageDialog.getDpiScalingFactor()), (int) (23 * MessageDialog.getDpiScalingFactor()));
		chckbxTournament.setToolTipText("Dies ist ein Turnierspiel");
		chckbxTournament.setSelected(settlersLobby.config.getnewS3GameLeague());
		chckbxTournament.setVisible(false);
		chckbxTournament.setEnabled(false);
		pnlLeagueTournament.add(chckbxLeague);
		pnlLeagueTournament.add(chckbxTournament);

		GroupLayout gl_optionsPanel = new GroupLayout(optionsPanel);
		gl_optionsPanel.setHorizontalGroup(
			gl_optionsPanel.createParallelGroup(Alignment.LEADING)
			    .addGroup(gl_optionsPanel.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(radioS3VersionCDorGOG)
                    .addGap((int) (18 * MessageDialog.getDpiScalingFactor()))
                    .addComponent(radioS3VersionGE)
                    .addGap((int) (18 * MessageDialog.getDpiScalingFactor()))
                    .addComponent(radioS3VersionCE)
                )
				.addGroup(gl_optionsPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(chckbxRandomPosition)
					.addGap((int) (18 * MessageDialog.getDpiScalingFactor()))
					.addComponent(chckbxGoToMiniChat)
					.addGap((int) (18 * MessageDialog.getDpiScalingFactor()))
					.addComponent(chckBxWiMo)
                    .addGap((int) (18 * MessageDialog.getDpiScalingFactor()))
					.addComponent(pnlLeagueTournament)
				)
		);
		gl_optionsPanel.setVerticalGroup(
			gl_optionsPanel.createParallelGroup(Alignment.LEADING)
			    .addGroup(gl_optionsPanel.createSequentialGroup()
			        .addGroup(gl_optionsPanel.createParallelGroup(Alignment.BASELINE)
                        .addComponent(radioS3VersionCDorGOG)
                        .addComponent(radioS3VersionGE)
                        .addComponent(radioS3VersionCE)
	                )
		        )
				.addGroup(gl_optionsPanel.createSequentialGroup()
                    .addGap((int) (28 * MessageDialog.getDpiScalingFactor()))
					.addGroup(gl_optionsPanel.createParallelGroup(Alignment.TRAILING)
						.addComponent(chckbxRandomPosition)
						.addComponent(chckbxGoToMiniChat)
						.addComponent(chckBxWiMo)
						.addComponent(pnlLeagueTournament)
					)
					.addContainerGap((int) (9 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE))
		);
		optionsPanel.setLayout(gl_optionsPanel);

		JPanel favoritesPane = new JPanel();
		favoritesPane.setPreferredSize(new Dimension((int) (450 * MessageDialog.getDpiScalingFactor()), (int) (400 * MessageDialog.getDpiScalingFactor())));
		favoritesPane.setName("favoriteS3Maps");
		favoritesPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		tabbedPane.addTab(I18n.getString("OpenNewS3GameDialog.TAB_FAVORITES"), null, favoritesPane, null);

		favoritesScrollPane = new JScrollPane();
		favoritesScrollPane.setName("");
		favoritesScrollPane.setAlignmentY(Component.TOP_ALIGNMENT);
		favoritesScrollPane.setAlignmentX(Component.LEFT_ALIGNMENT);
		favoritesScrollPane.setInheritsPopupMenu(true);
		favoritesScrollPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		favoritesTable = new JTable();
		favoritesTable.addMouseListener(mouseAdapterToCreateGame);
		favoritesTable.setName("favoritesTable");
		favoritesTable.setAutoCreateRowSorter(true);
		favoritesTable.getTableHeader().setReorderingAllowed(false);
		favoritesTable.getTableHeader().setResizingAllowed(false);
		favoritesTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		favoritesTable.setRowHeight((int) (favoritesTable.getFont().getSize() * MessageDialog.getDpiScalingFactor()) + favoritesTable.getRowMargin());
		favoritesTable.setModel(new OpenNewGameDialogTableModel(
			new Object[][] {
			},
			new Object[] {
				I18n.getString("OpenNewS3GameDialog.TAB_ROW_MAP"),
				I18n.getString("OpenNewS3GameDialog.TAB_ROW_PLAYERDIRECTION"),
				I18n.getString("OpenNewS3GameDialog.TAB_ROW_MAPSIZE")
			}

		));
		favoritesTable.getColumnModel().getColumn(0).setResizable(false);
		favoritesTable.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		favoritesScrollPane.setViewportView(favoritesTable);
		GroupLayout gl_favoritesPane = new GroupLayout(favoritesPane);
		gl_favoritesPane.setHorizontalGroup(
			gl_favoritesPane.createParallelGroup(Alignment.LEADING)
				.addComponent(favoritesScrollPane, GroupLayout.DEFAULT_SIZE, (int) (605 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE)
		);
		gl_favoritesPane.setVerticalGroup(
			gl_favoritesPane.createParallelGroup(Alignment.LEADING)
				.addComponent(favoritesScrollPane, GroupLayout.DEFAULT_SIZE, (int) (211 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE)
		);
		favoritesPane.setLayout(gl_favoritesPane);

		usermapsPane  = new JPanel();
		usermapsPane.setPreferredSize(new Dimension((int) (450 * MessageDialog.getDpiScalingFactor()), (int) (400 * MessageDialog.getDpiScalingFactor())));
		usermapsPane.setName("userS3Maps");
		tabbedPane.addTab(I18n.getString("OpenNewS3GameDialog.TAB_USERMAPS"), null, usermapsPane, null);

		usermapsScrollPane = new JScrollPane();
		usermapsScrollPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		usermapsScrollPane.setInheritsPopupMenu(true);
		usermapsScrollPane.setAlignmentY(Component.TOP_ALIGNMENT);
		usermapsScrollPane.setAlignmentX(Component.LEFT_ALIGNMENT);

		JPanel searchPaneUserMap = new JPanel();

		usermapsTable = new JTable();
		usermapsTable.addMouseListener(mouseAdapterToCreateGame);
		usermapsTable.setAutoCreateRowSorter(true);
		usermapsTable.getTableHeader().setReorderingAllowed(false);
		usermapsTable.getTableHeader().setResizingAllowed(false);
		usermapsTable.setRowHeight((int) (usermapsTable.getFont().getSize() * MessageDialog.getDpiScalingFactor()) + usermapsTable.getRowMargin());
		usermapsTable.setModel(new OpenNewGameDialogTableModel(
			new Object[][] {
			},
			new Object[] {
				I18n.getString("OpenNewS3GameDialog.TAB_ROW_MAP"),
				I18n.getString("OpenNewS3GameDialog.TAB_ROW_PLAYER"),
				I18n.getString("OpenNewS3GameDialog.TAB_ROW_MAPSIZE")
			}
		));
		usermapsTable.setName("userTable");
		usermapsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		usermapsScrollPane.setViewportView(usermapsTable);
		GroupLayout gl_usermapsPane = new GroupLayout(usermapsPane);
		gl_usermapsPane.setHorizontalGroup(
			gl_usermapsPane.createParallelGroup(Alignment.LEADING)
				.addComponent(searchPaneUserMap, GroupLayout.DEFAULT_SIZE, (int) (605 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE)
				.addComponent(usermapsScrollPane, GroupLayout.DEFAULT_SIZE,(int) ( 605 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE)
		);
		gl_usermapsPane.setVerticalGroup(
			gl_usermapsPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_usermapsPane.createSequentialGroup()
						.addComponent(searchPaneUserMap, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(usermapsScrollPane, GroupLayout.DEFAULT_SIZE, (int) (211 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE))
		);
		usermapsPane.setLayout(gl_usermapsPane);

		JPanel mapbasePane = new JPanel();
		mapbasePane.setPreferredSize(new Dimension((int) (450 * MessageDialog.getDpiScalingFactor()), (int) (400 * MessageDialog.getDpiScalingFactor())));
		mapbasePane.setName("S3mapbase");
		mapbasePane.setAlignmentY(Component.TOP_ALIGNMENT);
		mapbasePane.setAlignmentX(Component.LEFT_ALIGNMENT);
		tabbedPane.addTab(I18n.getString("OpenNewS3GameDialog.TAB_MAPBASE"), null, mapbasePane, null);

		JPanel searchPane = new JPanel();

		mapbaseResultsScrollPane = new JScrollPane();
		mapbaseResultsScrollPane.setName("mapbaseTable");
		mapbaseResultsScrollPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		mapbaseResultsScrollPane.setAlignmentY(Component.TOP_ALIGNMENT);
		mapbaseResultsScrollPane.setAlignmentX(Component.LEFT_ALIGNMENT);

		mapbasePane.add(mapbaseResultsScrollPane);

		GroupLayout gl_mapbasePane = new GroupLayout(mapbasePane);
		gl_mapbasePane.setHorizontalGroup(
			gl_mapbasePane.createParallelGroup(Alignment.LEADING)
				.addComponent(searchPane, GroupLayout.DEFAULT_SIZE, (int) (605 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE)
				.addComponent(mapbaseResultsScrollPane, GroupLayout.DEFAULT_SIZE, (int) (605 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE)
		);
		gl_mapbasePane.setVerticalGroup(
			gl_mapbasePane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_mapbasePane.createSequentialGroup()
					.addComponent(searchPane, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(mapbaseResultsScrollPane, GroupLayout.DEFAULT_SIZE, (int) (127 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE))
		);

		mapbaseResultsTable = new JTable();
		mapbaseResultsTable.setName("mapbaseTable");
		mapbaseResultsTable.addMouseListener(mouseAdapterToCreateGame);
		mapbaseResultsTable.setAutoCreateRowSorter(true);
		mapbaseResultsTable.getTableHeader().setReorderingAllowed(false);
		mapbaseResultsTable.getTableHeader().setResizingAllowed(false);
		mapbaseResultsTable.setRowHeight((int) (mapbaseResultsTable.getFont().getSize() * MessageDialog.getDpiScalingFactor()) + mapbaseResultsTable.getRowMargin());
		mapbaseResultsTable.setModel(new OpenNewGameDialogTableModel(
			new Object[][] {
			},
			new Object[] {
				I18n.getString("OpenNewS3GameDialog.TAB_ROW_MAP"),
				I18n.getString("OpenNewS3GameDialog.TAB_ROW_PLAYER"),
				I18n.getString("OpenNewS3GameDialog.TAB_ROW_MAPSIZE")
			}
		));
		mapbaseResultsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		mapbaseResultsScrollPane.setViewportView(mapbaseResultsTable);
		searchPane.setLayout(new BorderLayout(0, 0));
		searchPaneUserMap.setLayout(new BorderLayout(0, 0));

		searchTextField = new JTextField();
		searchTextField.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		searchTextField.setAutoscrolls(false);
		searchTextField.setHorizontalAlignment(SwingConstants.LEFT);
		searchTextField.setText(I18n.getString("OpenNewS3GameDialog.MAPBASE_INPUT_PLACEHOLDER"));
		searchTextField.setFont(new Font("Tahoma", Font.ITALIC, settlersLobby.config.getDpiAwareFontSize()));
		searchPane.add(searchTextField);

		searchTextFieldUserMap = new JTextField();
		searchTextFieldUserMap.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		searchTextFieldUserMap.setAutoscrolls(false);
		searchTextFieldUserMap.setHorizontalAlignment(SwingConstants.LEFT);
		searchTextFieldUserMap.setText(I18n.getString("OpenNewS3GameDialog.MAPBASE_INPUT_PLACEHOLDER"));
		searchTextFieldUserMap.setFont(new Font("Tahoma", Font.ITALIC, settlersLobby.config.getDpiAwareFontSize()));
		searchPaneUserMap.add(searchTextFieldUserMap);
		searchTextFieldUserMap.setColumns(10);

		searchBtn = new JButton(I18n.getString("OpenNewS3GameDialog.BUTTON_SEARCH"));
		searchPane.add(searchBtn, BorderLayout.EAST);

		searchBtnUserMap = new JButton(I18n.getString("OpenNewS3GameDialog.BUTTON_SEARCH"));
		resetBtnUserMap = new JButton("Reset");
		resetBtnUserMap.setEnabled(false);
		resetBtnUserMap.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// get the table-model
				DefaultTableModel dm = (DefaultTableModel) usermapsTable.getModel();
				// load the full list
				// remove all previous results from table-model
				removeRowsFromTable(usermapsTable, dm);
				// load maps
				loadUserMaps();
				searchTextFieldUserMap.setText(I18n.getString("OpenNewS3GameDialog.MAPBASE_INPUT_PLACEHOLDER"));
				searchTextFieldUserMap.setFont(new Font("Tahoma", Font.ITALIC, settlersLobby.config.getDpiAwareFontSize()));
				resetBtnUserMap.setEnabled(false);
			}
		});
		JPanel searchPaneUserMapButtons = new JPanel();
		searchPaneUserMapButtons.setLayout(new BorderLayout(0,0));
		searchPaneUserMapButtons.add(searchBtnUserMap, BorderLayout.WEST);
		searchPaneUserMapButtons.add(resetBtnUserMap, BorderLayout.EAST);
		searchPaneUserMap.add(searchPaneUserMapButtons, BorderLayout.EAST);
		mapbasePane.setLayout(gl_mapbasePane);

		JPanel randommapPane = new JPanel();
		randommapPane.setPreferredSize(new Dimension((int) (450 * MessageDialog.getDpiScalingFactor()), (int) (400 * MessageDialog.getDpiScalingFactor())));
		tabbedPane.addTab(I18n.getString("OpenNewS3GameDialog.TAB_RANDOMMAPS"), null, randommapPane, null);
		randommapPane.setName("S3randommaps");
		randommapPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		randommapScrollPane = new JScrollPane();
		randommapScrollPane.setName("");
		randommapScrollPane.setInheritsPopupMenu(true);
		randommapScrollPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		randommapScrollPane.setAlignmentY(0.0f);
		randommapScrollPane.setAlignmentX(0.0f);
		GroupLayout gl_randommapPane = new GroupLayout(randommapPane);
		gl_randommapPane.setHorizontalGroup(
			gl_randommapPane.createParallelGroup(Alignment.TRAILING)
				.addComponent(randommapScrollPane, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, (int) (605 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE)
		);
		gl_randommapPane.setVerticalGroup(
			gl_randommapPane.createParallelGroup(Alignment.LEADING)
				.addComponent(randommapScrollPane, GroupLayout.DEFAULT_SIZE, (int) (211 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE)
		);

		randommapTable = new JTable();
		randommapTable.setName("randomTable");
		randommapTable.addMouseListener(mouseAdapterToCreateGame);
		randommapTable.setAutoCreateRowSorter(true);
		randommapTable.getTableHeader().setReorderingAllowed(false);
		randommapTable.getTableHeader().setResizingAllowed(false);
		randommapTable.setRowHeight((int) (randommapTable.getFont().getSize() * MessageDialog.getDpiScalingFactor()) + randommapTable.getRowMargin());
		randommapTable.setModel(new OpenNewGameDialogTableModel(
			new Object[][] {
			},
			new Object[] {
				I18n.getString("OpenNewS3GameDialog.TAB_ROW_MAP"),
				I18n.getString("OpenNewS3GameDialog.TAB_ROW_DIRECTION"),
				I18n.getString("OpenNewS3GameDialog.TAB_ROW_MAPSIZE")
			}
		));
		randommapTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		randommapTable.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		randommapScrollPane.setViewportView(randommapTable);
		randommapPane.setLayout(gl_randommapPane);

		JPanel multiplayerPane = new JPanel();
		multiplayerPane.setPreferredSize(new Dimension((int) (450 * MessageDialog.getDpiScalingFactor()), (int) (400 * MessageDialog.getDpiScalingFactor())));
		multiplayerPane.setName("multiplayer");
		multiplayerPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		tabbedPane.addTab(I18n.getString("OpenNewS3GameDialog.TAB_CDMAPS"), null, multiplayerPane, null);

		multiplayerScrollPane = new JScrollPane();
		multiplayerScrollPane.setName("");
		multiplayerScrollPane.setInheritsPopupMenu(true);
		multiplayerScrollPane.setAlignmentY(Component.TOP_ALIGNMENT);
		multiplayerScrollPane.setAlignmentX(Component.LEFT_ALIGNMENT);
		multiplayerScrollPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		multiplayerTable = new JTable();
		multiplayerTable.setName("multiplayerTable");
		multiplayerTable.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		multiplayerTable.setAutoCreateRowSorter(true);
		multiplayerTable.getTableHeader().setReorderingAllowed(false);
		multiplayerTable.getTableHeader().setResizingAllowed(false);
		multiplayerTable.setRowHeight((int) (multiplayerTable.getFont().getSize() * MessageDialog.getDpiScalingFactor()) + multiplayerTable.getRowMargin());
		multiplayerTable.setModel(new OpenNewGameDialogTableModel(
			new Object[][] {
			},
			new Object[] {
				I18n.getString("OpenNewS3GameDialog.TAB_ROW_MAP"),
				I18n.getString("OpenNewS3GameDialog.TAB_ROW_PLAYER"),
				I18n.getString("OpenNewS3GameDialog.TAB_ROW_MAPSIZE")
			}
		));
		multiplayerTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		multiplayerTable.addMouseListener(mouseAdapterToCreateGame);
		multiplayerScrollPane.setViewportView(multiplayerTable);
		GroupLayout gl_multiplayerPane = new GroupLayout(multiplayerPane);
		gl_multiplayerPane.setHorizontalGroup(
			gl_multiplayerPane.createParallelGroup(Alignment.TRAILING)
				.addComponent(multiplayerScrollPane, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, (int) (605 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE)
		);
		gl_multiplayerPane.setVerticalGroup(
			gl_multiplayerPane.createParallelGroup(Alignment.LEADING)
				.addComponent(multiplayerScrollPane, GroupLayout.DEFAULT_SIZE, (int) (211 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE)
		);
		multiplayerPane.setLayout(gl_multiplayerPane);

		gamenameTextfield = new JTextField();
		gamenameTextfield.setToolTipText(I18n.getString("OpenNewS3GameDialog.GAMENAME_TOOLTIP"));
		gamenameTextfield.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		gamenameTextfield.setAutoscrolls(false);
		gamenameTextfield.setHorizontalAlignment(SwingConstants.LEFT);
		gamenameTextfield.setColumns(10);
		gamenameTextfield.setBorder(BorderFactory.createCompoundBorder(gamenameTextfield.getBorder(), BorderFactory.createEmptyBorder(3, 3, 2, 3)));

		JPanel pGoodsInStock = new JPanel();
        pGoodsInStock.setBackground(settlersLobby.theme.lobbyBackgroundColor);
		pGoodsInStock.setLayout(new GridLayout(1,2));
		goodsInStock.setRenderer(new GoodsInStockComboBoxListRenderer());
        goodsInStock.setBackground(settlersLobby.theme.backgroundColor);
        goodsInStock.setSelectedIndex(settlersLobby.config.getnewS3GameGoods());
        pGoodsInStock.add(goodsInStock);
		GroupLayout gl_gamenamePane = new GroupLayout(gamenamePane);
		gl_gamenamePane.setHorizontalGroup(
			gl_gamenamePane.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_gamenamePane.createSequentialGroup()
					.addGap((int) (5 * MessageDialog.getDpiScalingFactor()))
					.addComponent(gamenameTextfield, GroupLayout.DEFAULT_SIZE, (int) (244 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(pGoodsInStock, GroupLayout.PREFERRED_SIZE, (int) (169 * MessageDialog.getDpiScalingFactor()), GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		gl_gamenamePane.setVerticalGroup(
			gl_gamenamePane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_gamenamePane.createSequentialGroup()
					.addGap((int) (5 * MessageDialog.getDpiScalingFactor()))
					.addGroup(gl_gamenamePane.createParallelGroup(Alignment.BASELINE)
						.addComponent(gamenameTextfield, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(pGoodsInStock, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gamenamePane.setLayout(gl_gamenamePane);
		setLayout(groupLayout);

		////////////////////////////////////////////////////////////
		// THE FOLLOWING CODE IS NOT GENERATED VIA WINDOW BUILDER //
		////////////////////////////////////////////////////////////

		// create an array of the different tables (used for multiple style and actions)
		tableList = new ArrayList<JTable>();
		tableList.add(mapbaseResultsTable);
		tableList.add(favoritesTable);
		tableList.add(randommapTable);
		tableList.add(usermapsTable);
		tableList.add(multiplayerTable);

		// define a game-name
		// -> default-text in English because the game-name is visible
		//    for all users no matter which language they use
		String gamenameTextfieldContent = I18n.getString("OpenNewS3GameDialog.GAMENAME");
		boolean gamenameTextfieldFontItalic = false;
		if (settlersLobby.getNick() == null ||
		    settlersLobby.getNick().length() == 0)
		{
			gamenameTextfieldContent = "game"; //$NON-NLS-1$
			gamenameTextfieldFontItalic = true;
		}
		else if (settlersLobby.getNick().endsWith("s") || settlersLobby.getNick().endsWith("x") )  //$NON-NLS-1$
		{
			gamenameTextfieldContent = String.format("%s' game", settlersLobby.getNick()); //$NON-NLS-1$
		}
		else
		{
			gamenameTextfieldContent = String.format("%s's game", settlersLobby.getNick());; //$NON-NLS-1$
		}
		gamenameTextfield.setText(gamenameTextfieldContent);
		if( gamenameTextfieldFontItalic ) {
			gamenameTextfield.setFont(new Font("Tahoma", Font.ITALIC, settlersLobby.config.getDpiAwareFontSize()));
		}

		// Add Listener for field with the game-name
		// which will check whether there is something entered or not
		// and fill in a placeholder or not.
		gamenameTextfield.addFocusListener(new FocusListener(){
            @Override
            public void focusGained(FocusEvent  e){
            	if( gamenameTextfield.getText().equals(I18n.getString("OpenNewS3GameDialog.GAMENAME")) ) {
            		gamenameTextfield.setText("");
            		gamenameTextfield.setFont(new Font("Tahoma", Font.PLAIN, settlersLobby.config.getDpiAwareFontSize()));
            	}
            }

			@Override
			public void focusLost(FocusEvent e) {
				if( gamenameTextfield.getText().length() == 0 ) {
					gamenameTextfield.setText(I18n.getString("OpenNewS3GameDialog.GAMENAME"));
					gamenameTextfield.setFont(new Font("Tahoma", Font.ITALIC, settlersLobby.config.getDpiAwareFontSize()));
				}
			}
        });

		// add ChangeListener for tab-navigation for the moment when user change the tab
		// -> loads the content for the active tab (if not preloaded at aLobby-start)
		// -> and removes the preview-part
		tabbedPane.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JTabbedPane sourceTabbedPane = (JTabbedPane) e.getSource();
				Component activeTab = sourceTabbedPane.getSelectedComponent();
				settlersLobby.config.setnewS3GameTab(sourceTabbedPane.indexOfComponent(activeTab));
				// if new activeTab is the random-tab
				// than make random-position- and minichat-checkboxes unusable
				if( activeTab.getName().equals("S3randommaps") ) {
					chckbxRandomPosition.setEnabled(false);
					chckbxGoToMiniChat.setEnabled(false);
				}
                else {
                    chckbxRandomPosition.setEnabled(true);
                    if ( radioS3VersionCDorGOG.isSelected() || radioS3VersionGE.isSelected()) {
                        chckbxGoToMiniChat.setEnabled(true);
                    }
                    if ( radioS3VersionCE.isSelected() ) {
                        chckbxGoToMiniChat.setEnabled(false);
                    }
                }
                loadMapsIntoTables(activeTab);
            }
        });

		// add focusListener to searchTextField which will start the search for
		// the entered text in MapBase
		searchTextField.addFocusListener(new FocusListener(){
            @Override
            public void focusGained(FocusEvent  e){
            	if( searchTextField.getText().equals(I18n.getString("OpenNewS3GameDialog.MAPBASE_INPUT_PLACEHOLDER")) ) {
            		searchTextField.setText("");
            		searchTextField.setFont(new Font("Tahoma", Font.PLAIN, settlersLobby.config.getDpiAwareFontSize()));
            	}
            }

			@Override
			public void focusLost(FocusEvent e) {
				if( searchTextField.getText().length() == 0 ) {
					searchTextField.setText(I18n.getString("OpenNewS3GameDialog.MAPBASE_INPUT_PLACEHOLDER"));
					searchTextField.setFont(new Font("Tahoma", Font.ITALIC, settlersLobby.config.getDpiAwareFontSize()));
				}
			}
        });

		// add focusListener to searchTextField which will start the search for
		// the entered text in userMap-List
		searchTextFieldUserMap.addFocusListener(new FocusListener(){
            @Override
            public void focusGained(FocusEvent  e){
            	if( searchTextFieldUserMap.getText().equals(I18n.getString("OpenNewS3GameDialog.MAPBASE_INPUT_PLACEHOLDER")) ) {
            		searchTextFieldUserMap.setText("");
            		searchTextFieldUserMap.setFont(new Font("Tahoma", Font.PLAIN, settlersLobby.config.getDpiAwareFontSize()));
            	}
            }

			@Override
			public void focusLost(FocusEvent e) {
				if( searchTextFieldUserMap.getText().length() == 0 ) {
					searchTextFieldUserMap.setText(I18n.getString("OpenNewS3GameDialog.MAPBASE_INPUT_PLACEHOLDER"));
					searchTextFieldUserMap.setFont(new Font("Tahoma", Font.ITALIC, settlersLobby.config.getDpiAwareFontSize()));
				}
			}
        });

		AbstractAction searchHandler = new AbstractAction(){
            @Override
            public void actionPerformed(ActionEvent   e){
            	if( searchTextField.getText().equals(I18n.getString("OpenNewS3GameDialog.MAPBASE_INPUT_PLACEHOLDER")) ) {
            		searchTextField.setText("");
            		searchTextField.setFont(new Font("Tahoma", Font.PLAIN, settlersLobby.config.getDpiAwareFontSize()));
            	}
            	else {
            		getMapsFromMapBaseBySearchString();
            	}
            }
		};

		AbstractAction searchHandlerUserMap = new AbstractAction(){
            @Override
            public void actionPerformed(ActionEvent   e){
            	if( searchTextFieldUserMap.getText().equals(I18n.getString("OpenNewS3GameDialog.MAPBASE_INPUT_PLACEHOLDER")) ) {
            		searchTextFieldUserMap.setText("");
            		searchTextFieldUserMap.setFont(new Font("Tahoma", Font.PLAIN, settlersLobby.config.getDpiAwareFontSize()));
            		resetBtnUserMap.setEnabled(false);
            	}
            	else {
            		resetBtnUserMap.setEnabled(true);
            		getMapsFromUserMapsBySearchString();
            	}
            }
		};

		// add actionListener to searchTextField and button
		// which will start the search for the entered text in mapBase
		searchTextField.addActionListener(searchHandler);
		searchBtn.addActionListener(searchHandler);

		// add actionListener to searchTextField and button
		// which will start the search for the entered text in mapBase
		searchTextFieldUserMap.addActionListener(searchHandlerUserMap);
		searchBtnUserMap.addActionListener(searchHandlerUserMap);

		// set active tab depending on actual setting
		int tabFromSetting = settlersLobby.config.getnewS3GameTab();
		if( tabFromSetting < 0 ) {
			tabFromSetting = 0;
		}
		tabbedPane.setSelectedIndex(tabFromSetting);

		// hide league- and tournament-panel if league-interface is disabled
		if (Configuration.getInstance().isHideLeagueInterface())
	    {
			pnlLeagueTournament.setVisible(false);
	    }

		// add ancestorListener
		this.addAncestorListener(new AncestorListener ()
	    {
	        public void ancestorAdded ( AncestorEvent event )
	        {
	        	// check for League-State if this Panel is visible
	    		// -> if League is disabled (no active cycle) than disable the league-checkbox
	        	// -> only if league-interface is enabled
	        	if ( false == Configuration.getInstance().isHideLeagueInterface() ) {
        			if( false == checkIfLeagueIsActive() ) {
        				chckbxLeague.setSelected(false);
        				chckbxLeague.setEnabled(false);
        			}
        			else {
        				chckbxLeague.setEnabled(true);
        			}
				}
	        	// display the ingameoptions if this is not a standalone-game and not S3CE
	        	if( false == settlersLobby.isIngameOptionsStandalone() ) {
	        		settlersLobby.getGUI().displayIngameOptions((settlersLobby.config.getS3Versionnumber() <= 300) ? true : false);
	        	}
	        	else {
	        		chckbxLeague.setSelected(false);
	        		chckbxLeague.setVisible(false);
	        	}
	        }

			public void ancestorRemoved(AncestorEvent event) {
				// if panel was removed (e.g. window closed or active tab changed)
				// remove the ingameoptions if no game was started
				if( false == settlersLobby.isIngameOptionsStandalone() ) {
					if( false == settlersLobby.getGameStarter().isRunning() ) {
						settlersLobby.getGUI().displayIngameOptions(false);
					}
				}
			}

			public void ancestorMoved(AncestorEvent event) {
			}
	    } );

		// set layout
		// -> disable this function to activate windowBuilder in eclipse since
		//    windowBuilder crash with this ui-settings.
		setLayoutStyle();
	}

	/**
	 * Check if the League is active or not (depending on league-cycles managed in league-framework)
	 *
	 * @return boolean (false if league is offline or not active)
	 */
	private boolean checkIfLeagueIsActive() {
		// get file-info from mapBase independently whether the map-file is local available or not
		String response = "";

		// check whether the md5 of the local file is the same as on mapbase
    	httpRequests httpRequests = new httpRequests();
    	// -> set an parameter
    	httpRequests.addParameter("json", 1);
    	// -> ignore security-token
    	httpRequests.setIgnorePhpSessId(true);
    	// -> set the url
    	String url = "";
		try {
			// encode the searchstring and add it to the url
			url = settlersLobby.config.getLeagueUrlState();
			httpRequests.setUrl(url);
			// -> get the response-String
			response = httpRequests.doRequest();
    		if( !httpRequests.isError() ) {
    			if( response.trim().length() > 0 ) {
    				if( Integer.parseInt(response.trim()) > 0 ) {
    					return true;
    				}
    			}
    		}
		} catch (UnsupportedEncodingException e2) {
			e2.getStackTrace();
		} catch (IOException e) {
			e.getStackTrace();
		}
		return false;
	}

	/**
	 * Search for userMaps in local directory by entered searchstring.
	 */
	protected void getMapsFromUserMapsBySearchString() {
		// get the search-string from input-field
		String searchString = "";
		if( searchTextFieldUserMap.getText().length() > 0  ) {
			searchString = searchTextFieldUserMap.getText();
			if( searchString.equals("selbsterstellte Maps durchsuchen") ) {
				searchString = "";
			}
		}

		// reset the filter for the result-table
		usermapsTable.setRowSorter(null);

		// get the table-model
		DefaultTableModel dm = (DefaultTableModel) usermapsTable.getModel();

		// remove all previous results from table-model
		this.removeRowsFromTable(usermapsTable, dm);

		// check for minimum-size for search-string: 3 or more
		if( searchString.length() > 2 ) {

			final String mySearchString = searchString;

			// show a waiting-text in first column of this table
			dm.addRow(new Object[]{
				I18n.getString("GUI.PLEASE_WAIT")
			});

			// get a separate window with waiting-message
			LoadingJFrame waitingWindow = new LoadingJFrame(this.parentFrame);

			SwingWorker<?, ?> swingWorker = new SwingWorker<Object, Object>() {

				@Override
				protected Object doInBackground() throws Exception {
					// remove all previous results from table-model
					removeRowsFromTable(usermapsTable, dm);
					// load maps
					loadUserMaps(mySearchString);
					return null;
				}

				/**
		         * Executed in event dispatching thread
		         */
		        @Override
		        public void done() {
		        	// close the waiting window
		        	waitingWindow.dispatchEvent(new WindowEvent(waitingWindow, WindowEvent.WINDOW_CLOSING));
		        }

			};

			swingWorker.execute();

		}
		else if( searchString.length() > 0 ) {
			// show a hint that the user must insert a searchstring
			dm.addRow(new Object[]{
				I18n.getString("OpenNewS3GameDialog.ERROR_MINCHARS")
			});
		}
		else {
			// load the full list
			// remove all previous results from table-model
			removeRowsFromTable(usermapsTable, dm);
			// load maps
			loadUserMaps();
		}
	}

	// do some styling
	private void setLayoutStyle() {

		// -> set column-widths for each column in all tables
		//    -> they are slightly different
		mapbaseResultsTable.getColumnModel().getColumn(0).setPreferredWidth(275);
		mapbaseResultsTable.getColumnModel().getColumn(1).setPreferredWidth(100);
		mapbaseResultsTable.getColumnModel().getColumn(2).setPreferredWidth(75);
		favoritesTable.getColumnModel().getColumn(0).setPreferredWidth(225);
		favoritesTable.getColumnModel().getColumn(1).setPreferredWidth(150);
		favoritesTable.getColumnModel().getColumn(2).setPreferredWidth(75);
		randommapTable.getColumnModel().getColumn(0).setPreferredWidth(275);
		randommapTable.getColumnModel().getColumn(1).setPreferredWidth(100);
		randommapTable.getColumnModel().getColumn(2).setPreferredWidth(75);
		usermapsTable.getColumnModel().getColumn(0).setPreferredWidth(275);
		usermapsTable.getColumnModel().getColumn(1).setPreferredWidth(100);
		usermapsTable.getColumnModel().getColumn(2).setPreferredWidth(75);
		multiplayerTable.getColumnModel().getColumn(0).setPreferredWidth(275);
		multiplayerTable.getColumnModel().getColumn(1).setPreferredWidth(100);
		multiplayerTable.getColumnModel().getColumn(2).setPreferredWidth(75);

		// -> set background-color for the tables
		mapbaseResultsScrollPane.setViewportView(mapbaseResultsTable);
		mapbaseResultsScrollPane.getViewport().setBackground(settlersLobby.theme.backgroundColor);
		favoritesScrollPane.setViewportView(favoritesTable);
		favoritesScrollPane.getViewport().setBackground(settlersLobby.theme.backgroundColor);
		randommapScrollPane.setViewportView(randommapTable);
		randommapScrollPane.getViewport().setBackground(settlersLobby.theme.backgroundColor);
		usermapsScrollPane.setViewportView(usermapsTable);
		usermapsScrollPane.getViewport().setBackground(settlersLobby.theme.backgroundColor);
		multiplayerScrollPane.setViewportView(multiplayerTable);
		multiplayerScrollPane.getViewport().setBackground(settlersLobby.theme.backgroundColor);

		TableCellCenterRenderer tableCellCenterRenderer = new TableCellCenterRenderer();
		for(JTable object: tableList){
			// remove grids for each cell
			object.setShowGrid(false);
			// set the default style for the table-header
			object.getTableHeader().setDefaultRenderer(tableCellCenterRenderer);
		    // add border-bottom to table-header
			object.getTableHeader().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, settlersLobby.theme.lobbyBackgroundColor));
			// add background-Color which will only visible behind the tableHeader
			// and only if the actual theme does have this value
			if( settlersLobby.theme.tableheadercolor != null && settlersLobby.theme.tableheadercolor.toString().length() > 0 ) {

		    	// define the cellRenderer
		    	DefaultTableCellRenderer headerRendererLeft = new DefaultTableCellRenderer();
		    	headerRendererLeft.setBackground(settlersLobby.theme.tableheadercolor);
		    	headerRendererLeft.setHorizontalAlignment( JLabel.LEFT );
		    	object.getColumnModel().getColumn(0).setHeaderRenderer(headerRendererLeft);

		    	DefaultTableCellRenderer headerRenderer = new DefaultTableCellRenderer();
		    	headerRenderer.setBackground(settlersLobby.theme.tableheadercolor);
		    	headerRenderer.setHorizontalAlignment( JLabel.CENTER );
		    	// set it to each column except the first
		    	for (int i = 1; i < object.getModel().getColumnCount(); i++) {
		    		object.getColumnModel().getColumn(i).setHeaderRenderer(headerRenderer);
			    }
		    }
			// set alignment of contents in 2. and 3. column to right
			DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
			rightRenderer.setHorizontalAlignment(SwingConstants.RIGHT);
			object.getColumnModel().getColumn(1).setCellRenderer(rightRenderer);
			object.getColumnModel().getColumn(2).setCellRenderer(rightRenderer);

			// define the filter which will be available via JPopupMenu
			object.getTableHeader().addMouseListener(new MouseAdapter() {

				// show the popup on right click on table-header
				public void mouseClicked(MouseEvent e) {
					if (SwingUtilities.isRightMouseButton(e)) {
						showPopup(e);
					}
			    }

				// define the popup
				public void showPopup(MouseEvent mouseEvent) {
					// get the table-object
					JTableHeader tableHeader = (JTableHeader) mouseEvent.getSource();
					int index = tableHeader.columnAtPoint(mouseEvent.getPoint());
					if( index >= 1 ) {
						JPopupMenu popup = new JPopupMenu();

						// if filter for this row is active,
						// than show only one option name "show all entries"
						if( tableHeader.getTable().getRowSorter() != null && tableHeader.getTable().getRowSorter().getModelRowCount() != tableHeader.getTable().getRowSorter().getViewRowCount() ) {
							JMenuItem entry = new JMenuItem(I18n.getString("OpenNewS3GameDialog.SHOWALLOPTIONS"));
							entry.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									JPopupMenu menu = (JPopupMenu) ((JMenuItem) e.getSource()).getParent();
									JTableHeader tableHeader = (JTableHeader) menu.getInvoker();
									TableModel model = tableHeader.getTable().getModel();
									TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>();
									tableHeader.getTable().setRowSorter(sorter);
						            sorter.setModel(model);
						            // set regexp for filter
						            String text = "(?i)(.*)";
						            sorter.setRowFilter(RowFilter.regexFilter(text));
								}
							});
							popup.add(entry);
							popup.show( tableHeader, mouseEvent.getX(), mouseEvent.getY() );
						}
						else {

							// Go through all values of the chosen column and create for each
							// value an JMenuItem. Each JMenuItem gets a clickEvent which
							// starts the filtering of the table by JMenuItem-title.
							Map<String, String> aMap = new HashMap<String, String>();
							for( int i = tableHeader.getTable().getRowCount() - 1; i >= 0; i-- ) {
								String value = tableHeader.getTable().getModel().getValueAt(i, index).toString();
								aMap.put(value, value);
							}

							// sort the values (string-sort, so "10" will be before "2" in case of player-counts)
							aMap = ArraySupport.sortByValue(aMap);

							// add each value as menu-item to the popup-menu
							Iterator<Entry<String, String>> it = aMap.entrySet().iterator();
							while( it.hasNext() ) {
								Entry<String, String> pair = it.next();
								JMenuItem entry = new JMenuItem(pair.getValue().toString());
								entry.addActionListener(new ActionListener() {
									public void actionPerformed(ActionEvent e) {
										JMenuItem menuItem = (JMenuItem) e.getSource();
										JPopupMenu menu = (JPopupMenu) ((JMenuItem) e.getSource()).getParent();
										JTableHeader tableHeader = (JTableHeader) menu.getInvoker();
										TableModel model = tableHeader.getTable().getModel();
										TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>();
										tableHeader.getTable().setRowSorter(sorter);
							            sorter.setModel(model);
							            // set regexp for filter
							            String text = "(?i)" + menuItem.getText();
							            sorter.setRowFilter(RowFilter.regexFilter(text));
									}
								});
								popup.add(entry);
							}
							// show popup-menu if more than one item exists in it
							if( popup.getComponentCount() > 1 ) {
								popup.show( tableHeader, mouseEvent.getX(), mouseEvent.getY() );
							}
						}

					}
				};
			});

		}

		// -> override the ui-settings for the tabbed-pane to style the tabs in identical way
		tabbedPane.setUI(new BasicTabbedPaneUI() {
			// new border around whole tabbedPane
	        private final Insets borderInsets = new Insets(0, 0, 0, 0);
	        @Override
	        protected void paintContentBorder(Graphics g, int tabPlacement, int selectedIndex) {
	        }
	        @Override
	        protected Insets getContentBorderInsets(int tabPlacement) {
	            return borderInsets;
	        }
	        // new border around tabs
	        @Override
	        protected void paintTabBorder(Graphics g, int tabPlacement, int tabIndex, int x, int y, int w, int h, boolean isSelected ) {
	        	g.setColor(new Color(122,138,138));
	            g.drawLine(x, y+1, x, y+h+1); // left highlight
	        	g.drawLine(x+1, y+1, x+1, y+1); // top-left highlight
	        	g.drawLine(x+1, y+1, x+w-1, y+1); // top highlight

	            g.setColor(new Color(122,138,138));
	            g.drawLine(x+w-2, y+1, x+w-2, y+h+1); // right shadow
	        }

	    });

	}

	/**
	 * Get the Favorite-Maps as list
	 *
	 * @return void
	 */
	private Object[] getFavoriteMaps() {
		return this.settlersLobby.getFavoriteS3Maps();
	}

	/**
	 * Get the userMaps as list by string
	 *
	 * @return void
	 */
	private Object[] getUserMaps( String filter ) {
		return this.settlersLobby.getUserMaps( filter );
	}

	/**
	 * Get the multiplayerMaps as list
	 *
	 * @return void
	 */
	private String[] getMultiplayerMaps() {
		return this.settlersLobby.getMultiS3Maps();
	}

	/**
	 * Get the Random-Map as list
	 *
	 * @return void
	 */
	private S3RandomMap[] getRandomMaps() {
		return S3RandomMap.randomMaps;
	}

	/**
	 * Set a list of maps into a table including loading of map-data.
	 * Show a progress-bar during loading is in progress.
	 */
	private void setMapsIntoTable( Object[] userMapList, JTable tableComponent, String path ) {
		if( userMapList.length > 0 ) {
			// go through the maps
			for (int i = 0; i < userMapList.length; i++) {

				// get map-data (like map-name)
				ISMap map = S3Map.newInstance(path + userMapList[i], settlersLobby);

				// if map-instance is ok, go further
				if( map instanceof ISMap ) {
					// add map to table with output-values (like map-name)
					DefaultTableModel model = (DefaultTableModel) tableComponent.getModel();
					if( model.getColumnCount() == 3 ) {
						model.addRow(new Object[]{
							map.toDisplayString().replace(path, ""),
							map.getPlayer() + " " + I18n.getString("OpenNewS3GameDialog.PLAYERS"),
							map.getWidthHeight()
						});
					}
				}

				// free memory from now unused IS3Map-object
				map = null;
				System.gc();

			}

		}
	}

	/**
	 * set a list of random-maps into a table
	 */
	private void setMapsIntoTable( S3RandomMap[] maps, JTable tableComponent ) {
		if( maps.length > 0 ) {
			// go through the maps
			for (int i = 0; i < maps.length; i++) {
				// get the values for this map
				String[] mapNameParts = maps[i].toString().split("_");
				// -> mapSize
				String mapSize = mapNameParts[0];
				// -> direction (depending on modulo 4 from index)
				String direction = S3RandomMap.RANDOM_MAP_MIRROR_0;
				if( i % 4 == 1 ) {
					direction = S3RandomMap.RANDOM_MAP_MIRROR_1;
				}
				if( i % 4 == 2 ) {
					direction = S3RandomMap.RANDOM_MAP_MIRROR_2;
				}
				if( i % 4 == 3 ) {
					direction = S3RandomMap.RANDOM_MAP_MIRROR_3;
				}

				// add map to table with output-values (like map-name)
				DefaultTableModel model = (DefaultTableModel) tableComponent.getModel();
				if( model.getColumnCount() == 3 ) {
					model.addRow(new Object[]{
						maps[i].toDisplayString(),
						direction,
						mapSize
					});
				}
			}
		}
	}

	/**
	 * Set a list of maps into a JTable.
	 *
	 * @param maps
	 * @param tableComponent
	 */
	private void setMapsIntoTable( Object[] maps, JTable tableComponent ) {
		if( maps.length > 0 ) {
			// go through the map-list
			for ( Object mapString: maps ) {

				// get map-data (like map-name)
				ISMap map = S3Map.newInstance(mapString.toString(), this.settlersLobby);

				String secondRow = map.getPlayer() + " " + I18n.getString("OpenNewS3GameDialog.PLAYERS");
				String thirdRow = "" + map.getWidthHeight();

				if( thirdRow.equals("0") && secondRow.equals("0 " + I18n.getString("OpenNewS3GameDialog.PLAYERS")) ) {
					S3RandomMap randomMap = new S3RandomMap(mapString.toString());
					if( randomMap.isValid() ) {
						thirdRow = randomMap.getSize();
						secondRow = "";
						if( randomMap.isMirrorNone() )
						{
							secondRow = S3RandomMap.RANDOM_MAP_MIRROR_0;
						} else if( randomMap.isMirrorXAxis() )
						{
							secondRow = S3RandomMap.RANDOM_MAP_MIRROR_1;
						} else if( randomMap.isMirrorYAxis() )
						{
							secondRow = S3RandomMap.RANDOM_MAP_MIRROR_2;
						} else if( randomMap.isMirrorBoth() )
						{
							secondRow = S3RandomMap.RANDOM_MAP_MIRROR_3;
						}
					}
				}

				// add this map to table with output-values (like map-name)
				DefaultTableModel model = (DefaultTableModel) tableComponent.getModel();
				if( model.getColumnCount() == 3 ) {
					model.addRow(new Object[]{
						map.toDisplayString(),
						secondRow,
						thirdRow,
					});
				}

				// free memory from unused IS3Map-object
				map = null;
				System.gc();

			}
		}
	}

	/**
	 * Set a list of maps from a JSON-Result into a JTable.
	 *
	 * @param maps
	 * @param tableComponent
	 */
	private void setMapsIntoTable(JSONArray maps, JTable tableComponent) {
		if( maps.length() > 0 ) {
			// go through the map-list
			for (int i = 0; i < maps.length(); i++) {

				// get the mapname
				String mapname = "";
				if( !maps.getJSONObject(i).isNull("filename") ) {
					mapname = maps.getJSONObject(i).get("filename").toString();
				}

				// get the player
				String player = "";
				if( !maps.getJSONObject(i).isNull("player") ) {
					player = maps.getJSONObject(i).get("player").toString();
				}

				// get the map-size
				String size = "";
				if( !maps.getJSONObject(i).isNull("size") ) {
					size = maps.getJSONObject(i).get("size").toString();
				}

				// add this map to table with output-values (like map-name)
				DefaultTableModel model = (DefaultTableModel) tableComponent.getModel();
				if( model.getColumnCount() == 3 ) {
					model.addRow(new Object[]{
						mapname,
						player,
						size
					});
				}
			}
		}
	}

	/**
	 * Get the background-color from active style
	 *
	 * @return Color
	 */
	/*
	private Color getBackgroundColor() {
		return this.settlersLobby.theme.backgroundColor;
	}*/

	public void setParentObject( JFrame parentFrame ) {
		this.parentFrame = parentFrame;
	}

	/**
	 * Get the mapdata by mapname and insert it into previewPanel
	 *
	 * @param mapname
	 * 			Map-Name
	 * @param previewPanel
	 * 			The Preview-Panel which consists of the following fields.
	 * @param pnlLink
	 * @param txtMapPlayer
	 * @param txtMapDate
	 * @param pnlCreator
	 * @param txtMapSize
	 * @param previewPanel
	 * @param pnlPreviewImage
	 */
	private void getMapDataByName( String mapname, JPanel previewPanel, JTextPane txtMapSize, JPanel pnlCreator, JTextPane txtMapDate, JTextPane txtMapPlayer, JPanel pnlLink, JPanel pnlRating, JPanel pnlPreviewImage ) {
		if( mapname.length() > 0 && Toolkit.getDefaultToolkit().getScreenSize().getHeight() > 620 ) {

			// get a window with waiting-message
			LoadingJFrame waitingWindow = new LoadingJFrame(this.parentFrame);

			// get settlersLobby to use it in swingWorker
			SettlersLobby settlersLobby = this.settlersLobby;

			// define a swingWorker which will show a loading-window while http-request is not done
			SwingWorker<?, ?> swingWorker = new SwingWorker<Object, Object>() {

				@Override
				protected Object doInBackground() throws Exception {

					String response = "";

					// define the request
		        	httpRequests httpRequests = new httpRequests();
		        	// -> set parameter to configure the JSON-response
		        	httpRequests.addParameter("json", "1");
		        	// -> ignore security-token
		        	httpRequests.setIgnorePhpSessId(true);
		        	// -> show a progress-monitor
		        	httpRequests.setShowProgressMonitor(true);
		        	// -> set the url
		        	String url = "";
					try {
						// encode the mapname and add it to the url
						url = settlersLobby.config.getDefaultGetMapData() + URLEncoder.encode(mapname, "UTF-8");
					} catch (UnsupportedEncodingException e2) {
						e2.printStackTrace();
					}
					httpRequests.setUrl(url);
					// -> get the response-String
					try {
						response = httpRequests.doRequest();
					} catch (IOException e) {
					    Test.outputException(e);
					}

					// show only informations for this map if no error happend
					// during the http-request
					// -> if an error came up, show no hint therefore because it would be an endless windows-messaging-loop
					//    if users clicks again and again ..
					boolean previewPanelVisibility = false;
					if( !httpRequests.isError() ) {

						String hexcolor = SettlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor());

						String size = "";
						String player = "";
						String creator = "";
						String creator_link = "";
						String insertdate = "";
						String link = "";
						String rating = "";
						byte[] img = null;

						if( response.length() > 0 ) {
							JSONObject obj = new JSONObject(response);
							if( !obj.isNull("size") ) {
								size = obj.getString("size");
								previewPanelVisibility = true;
							}
							if( !obj.isNull("player") ) {
								player = obj.getString("player");
								previewPanelVisibility = true;
							}
							if( !obj.isNull("creator") ) {
								creator = obj.getString("creator");
								previewPanelVisibility = true;
							}
							if( !obj.isNull("creator_link") ) {
								if( obj.getString("creator_link").length() > 0 ) {
									creator_link = obj.getString("creator_link");
									previewPanelVisibility = true;
								}
							}
							if( !obj.isNull("insertdate") ) {
								insertdate = obj.getString("insertdate");
								previewPanelVisibility = true;
							}
							if( !obj.isNull("rating") ) {
								rating = obj.getString("rating");
								previewPanelVisibility = true;
							}
							if( !obj.isNull("link") ) {
								link = obj.getString("link");
								previewPanelVisibility = true;
							}
							if( !obj.isNull("img") ) {
								String imgBase64 = obj.getString("img");
								img = Base64.getDecoder().decode(imgBase64);
								previewPanelVisibility = true;
							}
						}

						// set the resulting data (if available, otherwise remove data)
						txtMapSize.setText(size);
						txtMapPlayer.setText(player);
						txtMapDate.setText(insertdate);

						pnlCreator.removeAll();
						pnlCreator.validate();
						pnlCreator.repaint();

						// set creator-info for this map depending on available link to it's mapbase-profile
						if( creator_link.length() > 0 ) {
							LinkLabel creatorLinkLabel = new LinkLabel();
							creatorLinkLabel.setText(MessageFormat.format("<html><a href=''" + creator_link + "'' style=''color: {0}''>" + creator + "</a></html>", hexcolor));
							creatorLinkLabel.setBorder(BorderFactory.createCompoundBorder(creatorLinkLabel.getBorder(), BorderFactory.createEmptyBorder(3, 3, 3, 3)));
							creatorLinkLabel.setBounds(0, 0, 0, 0);
							creatorLinkLabel.setSize(creatorLinkLabel.getPreferredSize());
							creatorLinkLabel.setLocation(0, 0);
							pnlCreator.add(creatorLinkLabel);
						}
						else {
							JTextField creatorTextField = new JTextField();
							creatorTextField.setText(creator);
							pnlCreator.add(creatorTextField);
						}
						pnlCreator.validate();
						pnlCreator.repaint();

						// remove all components from linkPanel
						pnlLink.removeAll();
						pnlLink.validate();
						pnlLink.repaint();

						// add the text as linked text via linkLabel
						if( link.length() > 0 ) {
							LinkLabel linklabel = new LinkLabel();
							linklabel.setText(MessageFormat.format("<html><a href=''" + link + "'' style=''color: {0}''>" + link + "</a></html>", hexcolor));
							linklabel.setBorder(BorderFactory.createCompoundBorder(linklabel.getBorder(), BorderFactory.createEmptyBorder(3, 3, 3, 3)));
							linklabel.setBounds(0, 0, 0, 0);
							linklabel.setSize(linklabel.getPreferredSize());
							linklabel.setLocation(0, 0);
							pnlLink.add(linklabel);
							chosenMapLink = link;
						}
						else {
							chosenMapLink = "";
						}
						pnlLink.validate();
						pnlLink.repaint();

						// remove all components in previewPanel
						pnlPreviewImage.removeAll();
						// and reset its graphic
						pnlPreviewImage.repaint();

						// show image only if an image is available
						if( img == null || img.length == 0 ) {
							// image for this map is not available
							// -> set this panel invisible (blank space)
							pnlPreviewImage.setVisible(false);
						}
						else {
							// get image from blob as byte-Array
							ByteArrayInputStream bais = new ByteArrayInputStream(img);
						    BufferedImage originalBufferedImage;
							try {

								// read image from Byte-Array
								originalBufferedImage = ImageIO.read(bais);

								// set the designated width for the image
								// -> do not set this to the width and height of the wrapping panel
								//    because this will be bigger than the image itself
								// -> height will be calculated depending on size of the original image
						        int thumbnailWidth = 240;

						        // calculate the width of the resulting image on-the-fly
						        int widthToScale = (int)(1.1 * thumbnailWidth);
						        int heightToScale = (int)((widthToScale * 1.0) / originalBufferedImage.getWidth()
						                            * originalBufferedImage.getHeight());

						        // create the target image without contents
						        BufferedImage resizedImage = new BufferedImage(widthToScale, heightToScale, originalBufferedImage.getType());
					        	Graphics2D g = resizedImage.createGraphics();
					        	// -> set alpha-composite
					        	g.setComposite(AlphaComposite.Src);
					        	// -> set rendering-conditions
					        	g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
					        	g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
					        	g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
					        	// -> draw the image
					        	g.drawImage(originalBufferedImage, 0, 0, widthToScale, heightToScale, null);
					        	// -> forget the result :]
					        	g.dispose();

					        	// add the bufferedImage as image to the panel
					        	pnlPreviewImage.add(new JLabel(new ImageIcon(resizedImage)));

					        	// set the name of the preview-image-Panel to the map-name
					        	// -> will be used in onclick-event
					        	pnlPreviewImage.setName(mapname);

						    } catch (IOException e) {
						        throw new RuntimeException(e);
						    }

							// set it visible
							pnlPreviewImage.setVisible(true);
						}

						// check if this map has been rated
						// -> if yes, than show the stars
						// -> if not, than hide this panel
						pnlRating.removeAll();
						pnlRating.validate();
						pnlRating.repaint();
						if( rating.length() > 0 ) {
							Font font = settlersLobby.config.getFontAweSome((float) (12 * MessageDialog.getDpiScalingFactor()));

							// generate the list of stars depending of actual rating-value
							// -> fontAweSome is used to display the stars
							String stars = "";
							for( int r=0;r<Integer.parseInt(rating);r++ ) {
								stars = stars + "\uf005";
							}

							// create a label where the stars will be added
							JLabel label = new JLabel();
							label.setFont(font);
							label.setText(stars);
							label.setBorder(new EmptyBorder(0, 0, 0, 0));
							label.setBounds(0, 0, 0, 0);
							label.setSize(label.getPreferredSize());
							label.setLocation(0, 0);
							label.setVisible(true);
							pnlRating.add(label);
							pnlRating.setVisible(true);
						}
						else {
							pnlRating.setVisible(false);
						}
						pnlRating.validate();
						pnlRating.repaint();

						// set previewPanel visible or not (depending on results above)
						previewPanel.setVisible(previewPanelVisibility);

					} else {
					    Test.output("requests returned an error!");
					    done();
	                    throw new RuntimeException("requests returned an error!"); 
					}

					// resize the wrapping panel and the window accordingly
					int newHeight = defaultWindowHeight;
					if( previewPanelVisibility ) {
						newHeight = defaultWindowHeight + (int) (270 * MessageDialog.getDpiScalingFactor());
					}
					setPreferredSize(new Dimension(getPreferredSize().width, newHeight));
					openNewGameDialog.pack();
					return previewPanelVisibility;
				}
				/**
		         * Executed in event dispatching thread
		         */
		        @Override
		        public void done() {
		        	// close the waiting window
		        	waitingWindow.dispatchEvent(new WindowEvent(waitingWindow, WindowEvent.WINDOW_CLOSING));
		        }

			};

			swingWorker.execute();
		}
	}

	/**
	 * Create the game with configured parameters
	 * including check if all necessary parameters are configured.
	 *
	 * "All" means:
	 * - Name of the Map
	 * - Name for the Game
	 *
	 * The chosen options for
	 * - goods
	 * - minichat
	 * - randomposition
	 * - WiMo
	 * will be respected.
	 *
	 * @return void
	 */
	public void createGame( String mapname ) {

		saveGameSettings();

		// create a window for hints if something is missing
		MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);

		// check for necessary parameters
		boolean startGame = true;

		// -> map-name available?
		if( mapname.length() == 0 ) {
			startGame = false;
			messagewindow.addMessage(
				I18n.getString("OpenNewS3GameDialog.ERROR_CHOOSEMAP"),
        		UIManager.getIcon("OptionPane.errorIcon")
        	);
		}

		// -> check for game-name
		String gameName = getGameName();
		String gamenameTextfieldContent = I18n.getString("OpenNewS3GameDialog.GAMENAME");
		if( gameName.length() == 0 ) {
			startGame = false;
			messagewindow.addMessage(
				I18n.getString("OpenNewS3GameDialog.ERROR_MAPNAME"),
        		UIManager.getIcon("OptionPane.errorIcon")
        	);
		}
		if( gameName.length() > 0 && gameName.equals(gamenameTextfieldContent) ) {
			startGame = false;
			messagewindow.addMessage(
				I18n.getString("OpenNewS3GameDialog.ERROR_MAPNAME"),
        		UIManager.getIcon("OptionPane.errorIcon")
        	);
		}

		// -> is tetris running?
		if( settlersLobby.isPlayingTetris() ) {
			startGame = false;
			messagewindow.addMessage(
				I18n.getString("OpenNewS3GameDialog.ERROR_TETRIS"),
        		UIManager.getIcon("OptionPane.errorIcon")
        	);
		}
		
		// save the chosen s3-edition for this game in the settings
        int versionnumber = 173;
        if( this.radioS3VersionCE.isSelected() ) {
            versionnumber = 300;
        }
        settlersLobby.config.setS3Versionnumber(versionnumber);

		// start game if all necessary parameters are available
		if( startGame ) {

			CheckAndDownloadMapFile mapFile = new CheckAndDownloadMapFile(this.settlersLobby);
			mapFile.setMap(mapname);
			if( mapFile.checkMapFile() ) {

				// add this map to the user-favorites
				settlersLobby.config.addFavoriteS3Map(mapname);
				settlersLobby.config.setFavoriteS3Maps(settlersLobby.config.getFavoriteS3Maps());

				// reload the favoriteMaps
				loadFavoriteMaps();

				// create the game-conditions
				GameStarter.StartOptions startOptions = new GameStarter.StartOptions(
					settlersLobby.config.getUserName(),
					gameName,
					mapname,
					getGoodsInStock(),
					isRandomPosis(),
					isNavigationToMiniChatActive(),
					isLeagueGame(),
					isWiMo(),
					getTournamentId(),
					getTournamentName(),
					getTournamentRound(),
					getTournamentGroupnumber(),
					getTournamentMatchnumber(),
					getTournamentPlayerlist(),
					mapname,
					2,
					getMiscType()
				);

				// -> if this will be a league-game
				//    check if the user has player one league-game before
				//    -> if not or if he does not exists in league at this moment
				//       ask the user if he is sure to play a league-game with the league-rules
				if( !Configuration.getInstance().isHideLeagueInterface() ) {
					if( this.isLeagueGame() && !mapname.toLowerCase().contains("savegame") ) {
						if( !settlersLobby.checkIfUserIsInLeague() ) {
							String hexcolor = SettlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor());
				 			MessageDialog messagewindowleague = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
				 			messageWithLeagueRulesLink = messagewindowleague.addMessage(
			         			"<html>" + MessageFormat.format(I18n.getString("OpenNewS3GameDialog.LEAGUE_HINT"), hexcolor) + "</html>",
				         		UIManager.getIcon("OptionPane.informationIcon")
				         	);
				 			messageWithLeagueRulesLink.addMouseListener(new MouseAdapter() {
				 	            @Override
				 	            public void mouseClicked(MouseEvent e) {
				 	            	if( messageWithLeagueRulesLink.getLinkClicked() ) {
				 	            		leagueRules.setEnabled(true);
				 	            		leagueRulesOkButton.setEnabled(true);
				 	            	}
				 	            }
				 	        });
				 			//messageWithLeagueRulesLink.getLinkClicked()
				 			leagueRules = messagewindowleague.addPanelWithLabelAndCheckbox(I18n.getString("OpenNewS3GameDialog.LEAGUE_ACCEPT"), "");
				 			leagueRules.setEnabled(false);
				 			leagueRulesOkButton = messagewindowleague.addButton(
				         		I18n.getString("OpenNewS3GameDialog.LEAGUE_OK"),
				         		new ActionListener() {
				         		    @Override
				         		    public void actionPerformed(ActionEvent e) {
				         		    	if( false == leagueRules.isSelected() ) {
				         		    		MessageDialog messagewindowleague_error = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
				         		    		messagewindowleague_error.addMessage(
				         		    				I18n.getString("OpenNewS3GameDialog.LEAGUE_HINT2"),
								         		UIManager.getIcon("OptionPane.errorIcon")
								         	);
				         		    		messagewindowleague_error.addButton(
			    				         		I18n.getString("ErrorDialog.OK"),
			    				         		new ActionListener() {
			    				         		    @Override
			    				         		    public void actionPerformed(ActionEvent e) {
			    				         		    	messagewindowleague_error.dispose();
			    				         		    }
			    				         		}
			    				         	);
				         		    		messagewindowleague_error.setVisible(true);
				         		    	}
				         		    	else {
				         		    		// start the game and close this window
				         		    		checkMapAndStartGame(startOptions);
				         		    		messagewindowleague.dispose();
				         		    	}
				         		    }
				         		}
				         	);
				 			leagueRulesOkButton.setEnabled(false);
				 			messagewindowleague.addButton(
				         		I18n.getString("OpenNewS3GameDialog.LEAGUE_NOT_OK"),
				         		new ActionListener() {
				         		    @Override
				         		    public void actionPerformed(ActionEvent e) {
				         		    	// close this window
				         		    	messagewindowleague.dispose();
				         		    }
				         		}
				         	);
				 			messagewindowleague.setVisible(true);
						}
						else {
							checkMapAndStartGame(startOptions);
						}
					}
					else {
						// if this is a tournament-game first check, if the selected map
						// is allowed for this tournament
						if( this.tournamentid != "" ) {
							boolean allowedMap = true;
							if( this.maptypes.equals("setmaps") ) {
								if( mapname.contains("RANDOM") ) {
									allowedMap = false;
									// show hint, that randoms not allowed for this tournament
									MessageDialog messagewindowtournament = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
									messagewindowtournament.addMessage(
					         			"<html>In diesem Turnier sind keine Random-Maps zugelassen. Bitte wähle eine Setmap aus.</html>",
						         		UIManager.getIcon("OptionPane.errorIcon")
						         	);
									messagewindowtournament.addButton(
						         		"OK",
						         		new ActionListener() {
						         		    @Override
						         		    public void actionPerformed(ActionEvent e) {
						         		    	// close this window
						         		    	messagewindowtournament.dispose();
						         		    }
						         		}
						         	);
									messagewindowtournament.setVisible(true);
								}
							}
							if( allowedMap ) {
								checkMapAndStartGame(startOptions);
							}
						}
						else {
							checkMapAndStartGame(startOptions);
						}
					}
				}
				else {
					checkMapAndStartGame(startOptions);
				}

			}
			else {
			    // Hint if chosen Map does not exists in mapbase
				String hexcolor = SettlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor());
				messagewindow.addMessage(
					MessageFormat.format("<html>" + I18n.getString("OpenNewS3GameDialog.MAP_DOES_NOT_EXISTS_ON_MAPBASE") + "<br>" + I18n.getString("OpenNewS3GameDialog.MAP_DOES_NOT_EXISTS_ON_MAPBASE_LINE_2") + "</html>", hexcolor),
	        		UIManager.getIcon("OptionPane.errorIcon")
	        	);
	        	messagewindow.addButton(
	        			I18n.getString("OpenNewS3GameDialog.VPN_HINT_AT_START_BUTTON"),
	        		new ActionListener() {
	        		    @Override
	        		    public void actionPerformed(ActionEvent e) {
	        	        	// and close the hint-window
	        		    	messagewindow.dispose();
	        		    	// create the game-conditions
	        				GameStarter.StartOptions startOptions = new GameStarter.StartOptions(
	        					settlersLobby.config.getUserName(),
	        					gameName,
	        					mapname,
	        					getGoodsInStock(),
	        					isRandomPosis(),
	        					isNavigationToMiniChatActive(),
	        					isLeagueGame(),
	        					isWiMo(),
	        					getTournamentId(),
	        					getTournamentName(),
	        					getTournamentRound(),
	        					getTournamentGroupnumber(),
	        					getTournamentMatchnumber(),
	        					getTournamentPlayerlist(),
	        					mapname,
	        					2,
	        					getMiscType()
	        				);
	        		    	settlersLobby.actionWantsToCreateMapGame(startOptions);
	        		    }
	        		}
	        	);
	        	messagewindow.addButton(
	        		I18n.getString("OpenNewS3GameDialog.VPN_HINT_AT_START_BUTTON_2"),
	        		new ActionListener() {
	        		    @Override
	        		    public void actionPerformed(ActionEvent e) {
	        	        	// and close the hint-window
	        		    	messagewindow.dispose();
	        		    	// set the openNewGameDialog to visible
	        		    	OpenNewGameDialog openNewGameDialog = getOpenNewGameDialog();
	        		    	openNewGameDialog.setVisible(true);
	        		    }
	        		}
	        	);
	        	// set the openNewGameDialog to invisible
	        	OpenNewGameDialog openNewGameDialog = getOpenNewGameDialog();
	        	openNewGameDialog.setVisible(false);
	        	messagewindow.setVisible(true);
			}
		}
		else {
			// otherwise show the hint
        	messagewindow.addButton(
        		UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
        		new ActionListener() {
        		    @Override
        		    public void actionPerformed(ActionEvent e) {
        	        	// and close the hint-window
        		    	messagewindow.dispose();
        		    	// set the openNewGameDialog to visible
        		    	OpenNewGameDialog openNewGameDialog = getOpenNewGameDialog();
        		    	openNewGameDialog.setVisible(true);
        		    }
        		}
        	);
        	// set the openNewGameDialog to invisible
        	OpenNewGameDialog openNewGameDialog = getOpenNewGameDialog();
        	openNewGameDialog.setVisible(false);
        	messagewindow.setVisible(true);
		}
	}

	/**
	 * Get the Matchnumber for a tournament-game.
	 *
	 * @return int
	 */
	private int getTournamentMatchnumber() {
		if( isTournamentGame() ) {
			return Integer.parseInt(this.matchnumber);
		}
		return 0;
	}

	/**
	 * Get the groupnumber for a tournament-game.
	 *
	 * @return String
	 */
	private int getTournamentGroupnumber() {
		if( isTournamentGame() ) {
			return Integer.parseInt(this.groupnumber);
		}
		return 0;
	}

	/**
	 * Get the round for a tournament-game.
	 *
	 * @return String
	 */
	private int getTournamentRound() {
		if( isTournamentGame() ) {
			return Integer.parseInt(this.round);
		}
		return 0;
	}

	/**
	 * Get the playerlist for a tournament-game.
	 *
	 * @return String
	 */
	private String getTournamentPlayerlist() {
		if( isTournamentGame() ) {
			return this.playerlist;
		}
		return "";
	}
	
	/**
	 * Get the configured S3 edition as miscType.
	 * 
	 * @return
	 */
    private MiscType getMiscType() {
        MiscType miscType = settlersLobby.config.getS3VersionAsMiscType();
        if( this.radioS3VersionCE.isSelected() ) {
            miscType = MiscType.S3CE;
        }
        return miscType;
    }

	private void checkMapAndStartGame( StartOptions startOptions ) {
		// check if user is connected via vpn
		// -> if not warn the user that he needs vpn to play
		//    with others
		if( !settlersLobby.isLocalMode() && ( settlersLobby.getVpnIpFromSystem() == null || !settlersLobby.getVpnIpFromSystem().startsWith(ALobbyConstants.VPN_IP_RANGE) ) ) {
			String hexcolor = SettlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor());
			MessageDialog messagewindow1 = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
			messagewindow1.addMessage(
				I18n.getString("OpenNewS3GameDialog.VPN_HINT_AT_START"),
				UIManager.getIcon("OptionPane.errorIcon")
			);
			messagewindow1.addMessage(
					"<html>" + MessageFormat.format(I18n.getString("SettlersLobby.VPN_NOT_ACTIVE_INFO"),hexcolor) + "</html>",
				UIManager.getIcon("OptionPane.informationIcon")
			);
			messagewindow1.addButton(
				I18n.getString("OpenNewS3GameDialog.VPN_HINT_AT_START_BUTTON"),
				new ActionListener() {
				    @Override
				    public void actionPerformed(ActionEvent e) {
				    	// close this window
				    	settlersLobby.actionWantsToCreateMapGame(startOptions);
				    	// close the open new game dialog
				    	closeDialog();
				    	messagewindow1.dispose();
				    }
				}
			);
			messagewindow1.addButton(
				I18n.getString("OpenNewS3GameDialog.VPN_HINT_AT_START_BUTTON_2"),
				new ActionListener() {
				    @Override
				    public void actionPerformed(ActionEvent e) {
				    	// close this window
				    	messagewindow1.dispose();
				    }
				}
			).addActionListener(actionListenerToCloseNewGameDialog);
			messagewindow1.setVisible(true);
		}
		else {
			settlersLobby.actionWantsToCreateMapGame(startOptions);
			closeDialog();
		}
	}

	private void closeDialog() {
		// remove the object
		settlersLobby.getGUI().resetMapChoosingDialog();
		// close the open new game dialog
		this.openNewGameDialog.setVisible(false);
		this.openNewGameDialog.dispose();
	}

	/**
	 * Check whether this should be a league-game or not
	 *
	 * @return boolean
	 */
	private boolean isLeagueGame() {
		if( pnlLeagueTournament.isEnabled() && chckbxLeague.isVisible() ) {
			return chckbxLeague.isSelected();
		}
		return false;
	}

	/**
	 * Check whether this will be a wimo or not
	 *
	 * @return boolean
	 */
	private boolean isWiMo() {
		if( chckBxWiMo.isEnabled() ) {
			return chckBxWiMo.isSelected();
		}
		return false;
	}

	/**
	 * Get the chosen tournamentid.
	 *
	 * @return int
	 */
	private int getTournamentId() {
		if( isTournamentGame() ) {
			return Integer.parseInt(this.tournamentid);
		}
		return 0;
	}

	/**
	 * Get the chosen tournament-name.
	 *
	 * @return int
	 */
	private String getTournamentName() {
		if( isTournamentGame() ) {
			return this.tournamentname;
		}
		return "";
	}

	/**
	 * Check if this is a Tournament-Game.
	 *
	 * @return boolean
	 */
	private boolean isTournamentGame() {
		return this.tournamentid == "" ? false: true;
	}

	/**
	 * Get the selected tools-setting.
	 *
	 * @return GoodsInStock
	 */
	private GoodsInStock getGoodsInStock() {
		return (GoodsInStock)goodsInStock.getSelectedItem();
	}

	/**
	 * Get the Game-name which will be show to all users in the aLobby
	 *
	 * @return String
	 */
	private String getGameName() {
		return gamenameTextfield.getText();
	}

	/**
	 * Check whether random position should be used
	 *
	 * @return boolean
	 */
	private boolean isRandomPosis() {
		if( chckbxRandomPosition.isEnabled() ) {
			return chckbxRandomPosition.isSelected();
		}
		return false;
	}

	/**
	 *  Check whether navigation to minichat should be used
	 *
	 *  @return boolean
	 */
	private boolean isNavigationToMiniChatActive() {
		if( chckbxGoToMiniChat.isEnabled() ) {
			return chckbxGoToMiniChat.isSelected();
		}
		return false;
	}

	/**
	 * Define mouseAdapter for mouse-events on tables.
	 *
	 * Use this as Listener for appropriated panes.
	 *
	 * @return MouseAdapter
	 */
	private final MouseAdapter mouseAdapterToCreateGame = new MouseAdapter()
	{

		@Override
		public void mouseClicked(MouseEvent e)
		{

			if (e.getClickCount() > 2 || e.isConsumed())
	        {
		        // prevent execution if user clicks more than 2 times
		        return;
	        }

			e.consume();

	    	// get the table-object
        	JTable table = (JTable) e.getSource();

        	// get the name of the table
        	// -> essential to send the correct s3-foldername to start the game
        	String tableName = table.getName();

        	// get the number of the clicked row
        	int i = table.getSelectedRow();

			// left-mouse-click
		    if( SwingUtilities.isLeftMouseButton(e) )
		    {

		    	// if single-click load the map-data
		    	if( e.getClickCount() == 1 ) {

					// get map-name from first column of the selected row
		    		// and show map-data from mapBase
					String mapname = "";
					if( table.getSelectedRow() >= 0) {
						mapname = getMapNameFromTableWithRowSorter( table, table.getSelectedRow());
						// -> not for random Maps
						if( mapname.indexOf(ALobbyConstants.PATH_RANDOM + File.separator) >= 0 || table == randommapTable ) {
							mapname = "";
							// If clicked map is a random-map
							// than disable the random-position- and minichat-checkboxes
							// but not if this will be a tournament-game
							if( tournamentid == "" ) {
								chckbxRandomPosition.setEnabled(false);
								chckbxGoToMiniChat.setEnabled(false);
							}
						}
						else {
							if( tournamentid == "" ) {
								chckbxRandomPosition.setEnabled(true);
								chckbxGoToMiniChat.setEnabled(true);
							}
						}
						// remove map-type from mapName if user clicked in favoritesTable
						if( table == favoritesTable ) {
							mapname = mapname.replace(ALobbyConstants.PATH_MAP + File.separator, "").replace(ALobbyConstants.PATH_S3MULTI + File.separator, "").replace(ALobbyConstants.PATH_RANDOM + File.separator, "").replace(ALobbyConstants.PATH_USER + File.separator, "");
						}
					}
					if( mapname.length() > 0 ) {
						// get data for previewPanel
						getMapDataByName(mapname, previewPanel, txtMapSize, pnlCreator, txtMapDate, txtMapPlayer, pnlLink, pnlRating, pnlPreviewImage);
					}
					else {
						hidePreviewPanel();
					}

		    	}

		    	// if double-clicked start the game with actual setting
		        if( e.getClickCount() == 2 )
		        {

		        	// get the content of the first column from clicked row
		        	String mapname = getMapNameFromTableWithRowSorter( table, i);

		        	// depending on tableName search for the clicked row/map
		        	// -> except for favorites-table because the first column already consists the settler 3-foldername
		        	switch( tableName ) {
		        		case "userTable":
		        			mapname = ALobbyConstants.PATH_USER + File.separator + mapname;
		        			break;
		        		case "mapbaseTable":
		        			mapname = ALobbyConstants.PATH_USER + File.separator + mapname;
		        			break;
		        		case "randomTable":
		        			mapname = getRandomMapName(mapname);
		        			break;
		        		case "multiplayerTable":
		        			mapname = ALobbyConstants.PATH_S3MULTI + File.separator + mapname;
		        			break;
		        		case "favoritesTable":
		        			if( mapname.indexOf(ALobbyConstants.PATH_RANDOM + File.separator) >= 0 ) {
		        				mapname = getRandomMapName(mapname.replace(ALobbyConstants.PATH_RANDOM + File.separator, ""));
		        			}
		        			break;
		        	}

		        	// start game
		        	createGame(mapname);
		        }
		    }

		    // right-mouse-click
		    if( SwingUtilities.isRightMouseButton(e) ) {
		    	// check whether a row is selected
		    	// -> if yes, than copy the content of the first row
	        	String mapname = table.getModel().getValueAt(i, 0).toString();
	        	Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
				if( clipboard != null ) {
					clipboard.setContents(new StringSelection(mapname), null);
				}

				// build a popup with some options which is only shown if min. 1 option is visible
				JPopupMenu mapOptionsPopup = new JPopupMenu();
				boolean showPopup = false;
				// show option to delete the map from local map-directory if a single map is selected
				if( tabbedPane.getComponentAt(tabbedPane.getSelectedIndex()) == usermapsPane && usermapsTable.getSelectedRow() >= 0 ) {
					JMenuItem deleteMapsOptions = new JMenuItem(I18n.getString("OpenNewS3GameDialog.POPUP_TITLE_DELETE_MAP"));  //$NON-NLS-1$
					deleteMapsOptions.addActionListener(new ActionListener() {
			            @Override
			            public void actionPerformed(ActionEvent e)
			            {
			            	// hide the preview panel
			            	hidePreviewPanel();

			                // get the map-file in user-directory
			                File userMapDir = new File(new File(new File(settlersLobby.config.getGamePathS3()).getParent(), "Map"), "User");
			                File map = new File(userMapDir, mapname);

			                // delete this file if it exists
			                if( map.exists() ) {
			                	if( map.delete() ) {
			                		// -> show hint to user that file has been deleted
						        	MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
						        	// -> add message
						    		messagewindow.addMessage(
						    			"<html>" + I18n.getString("OpenNewS3GameDialog.MAP_DELETED") + "</html>",
						    			UIManager.getIcon("OptionPane.infoIcon")
						    		);
						    		// -> add ok-button
						    		messagewindow.addButton(
						    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
						    			new ActionListener() {
						    			    @Override
						    			    public void actionPerformed(ActionEvent e) {
						    			    	loadMaps();
						    			    	messagewindow.dispose();
						    			    }
						    			}
						    		);
						    		// show it
						    		messagewindow.setVisible(true);
			                	}
			                	else {
			                		// -> show hint to user that file could not be deleted
						        	MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
						        	// -> add message
						    		messagewindow.addMessage(
						    			"<html>" + I18n.getString("OpenNewS3GameDialog.MAP_NOT_DELETED") + "</html>",
						    			UIManager.getIcon("OptionPane.errorIcon")
						    		);
						    		// -> add ok-button
						    		messagewindow.addButton(
						    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
						    			new ActionListener() {
						    			    @Override
						    			    public void actionPerformed(ActionEvent e) {
						    			    	messagewindow.dispose();
						    			    }
						    			}
						    		);
						    		// show it
						    		messagewindow.setVisible(true);
			                	}
			                }

			            }
			        });
					deleteMapsOptions.setVisible(true);
					showPopup = true;
					mapOptionsPopup.add(deleteMapsOptions);
		    	}
				// show the popup if one option is available
				if( showPopup ) {
					mapOptionsPopup.show(e.getComponent(), e.getX(), e.getY());
				}
		    }
        }
	};

	/**
	 * Hide the preview panel.
	 */
	private void hidePreviewPanel() {
		// set the previewPanel invisible and resize wrapping panel and window accordingly
		previewPanel.setVisible(false);
		setPreferredSize(new Dimension(getPreferredSize().width, defaultWindowHeight));
		openNewGameDialog.pack();
	}

	/**
	 * Get a random map name.
	 *
	 * @param OriginalMapName
	 * @return
	 */
	private String getRandomMapName(String OriginalMapName) {
		S3RandomMap[] randommaps = getRandomMaps();

		String mapname = "";

		// go through the maps and search for the clicked map
		// to get its internal name
		for (int mapnr = 0; mapnr < randommaps.length; mapnr++) {
			if( randommaps[mapnr].toDisplayString().equals(OriginalMapName) ) {
				mapname = randommaps[mapnr].toString();
			}
		}

		if( mapname.length() > 0 ) {
			mapname = ALobbyConstants.PATH_RANDOM + File.separator + mapname;
		}
		return mapname;
	}

	private JTextField FieldPlayercount;


	/**
	 * Define actionListener to get a random map after user insert his preferred player-count.
	 *
	 * @return Action
	 */
	private final Action actionListenerToCreateRandomGame = new AbstractAction() {
		@Override
		public void actionPerformed(ActionEvent evt)
		{
			@SuppressWarnings("static-access")
			String hexcolor = settlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor());
			MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
        	// -> add message
    		messagewindow.addMessage(
				MessageFormat.format(I18n.getString("OpenNewS3GameDialog.RANDOMMAP_DESCRIPTION"), hexcolor),
    			UIManager.getIcon("JOptionPane.INFORMATION_MESSAGE")
    		);
    		FieldPlayercount = messagewindow.addPanelWithLabelAndTextField(I18n.getString("OpenNewS3GameDialog.RANDOMMAP_PLAYERCOUNT"), 2, "");
    		// -> add ok-button
    		messagewindow.addButton(
    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
    			new ActionListener() {
    			    @Override
    			    public void actionPerformed(ActionEvent e) {
    			    	boolean ok = false;
    			    	if( FieldPlayercount.getText().length() > 0 ) {
    			    		int playercount = Integer.parseInt(FieldPlayercount.getText());
    			    		if( playercount > 0 && playercount <= 20 ) {
    			    			ok = true;

    			    			// get a separate window with waiting-message
			    				LoadingJFrame waitingWindow = new LoadingJFrame(parentFrame);

    			    			// get a random map from mapbase
    			    			// define a swingWorker which will show a loading-window while http-request is not done
    			    			SwingWorker<?, ?> swingWorker = new SwingWorker<Object, Object>() {

    			    				@Override
    			    				protected Object doInBackground() throws Exception {

    			    					String response = "";

    			    					// define the request
    			    		        	httpRequests httpRequests = new httpRequests();
    			    		        	// -> set parameter to configure the JSON-response
    			    		        	httpRequests.addParameter("json", "1");
    			    		        	// -> ignore security-token
    			    		        	httpRequests.setIgnorePhpSessId(true);
    			    		        	// -> show a progress-monitor
    			    		        	httpRequests.setShowProgressMonitor(true);
    			    		        	// -> set the url
    			    		        	String url = "";
    			    					// encode the mapname and add it to the url
										url = settlersLobby.config.getDefaultGetRandomMap() + playercount;
    			    					httpRequests.setUrl(url);
    			    					// -> get the response-String
    			    					try {
    			    						response = httpRequests.doRequest().trim();
    			    					} catch (IOException e) {
    			    					}
    			    					String mapname = ALobbyConstants.PATH_USER + File.separator + response;
    			    					// enable the navigation to minichat, so the user cannot change anything before it
                                        if (radioS3VersionCE.isSelected()) {
                                            chckbxGoToMiniChat.setEnabled(false);
                                        } else {
                                            chckbxGoToMiniChat.setEnabled(true);
                                        }
    			    					// start the game
    			    					createGame(mapname);
										return null;
    			    				}

    			    				/**
    			    		         * Executed in event dispatching thread
    			    		         */
    			    		        @Override
    			    		        public void done() {
    			    		        	// close the waiting window
    			    		        	waitingWindow.dispatchEvent(new WindowEvent(waitingWindow, WindowEvent.WINDOW_CLOSING));
    			    		        }

    			    			};

    			    			swingWorker.execute();
    			    		}
    			    	}
    			    	if( false == ok ) {
    			    		MessageDialog messagewindow2 = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
    			        	// -> add message
    			    		messagewindow2.addMessage(
			    				I18n.getString("OpenNewS3GameDialog.RANDOMMAP_WARNING"),
    			    			UIManager.getIcon("OptionPane.errorIcon")
    			    		);
    			    		// -> add ok-button
    			    		messagewindow2.addButton(
    			    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
    			    			new ActionListener() {
    			    			    @Override
    			    			    public void actionPerformed(ActionEvent e) {
    			    			    	messagewindow2.dispose();
    			    			    }
    			    			}
    			    		);
    			    		// show it
    			    		messagewindow2.setVisible(true);
    			    	}
    			    	messagewindow.dispose();
    			    }
    			}
    		);
    		// show it
    		messagewindow.setVisible(true);
		}
	};

	/**
	 * Define actionListener to start a game e.g. if the "start"-button is clicked
	 *
	 * @return Action
	 */
	private final Action actionListenerToCreateGame = new AbstractAction()
	{
		@Override
		public void actionPerformed(ActionEvent evt)
		{

			// get the name of the active tab
			String activeTabName = tabbedPane.getSelectedComponent().getName();

			// prepare String for map-name
			String mapname = "";

			// depending from this name, get the actual marked game
			// -> if nothing marked show a hint
			// -> if marked start the game
			switch (activeTabName) {

				// favorites-tab
				case "favoriteS3Maps":
					if( favoritesTable.getRowCount() > 0 ) {
						int i = favoritesTable.getSelectedRow();
						if( i >= 0 ) {
							mapname = getMapNameFromTableWithRowSorter( favoritesTable, i );
							if( mapname.indexOf(ALobbyConstants.PATH_RANDOM + File.separator) >= 0 ) {
								mapname = getRandomMapName(mapname.replace(ALobbyConstants.PATH_RANDOM + File.separator, ""));
							}
						}
					}
					break;

				// user-tab
				case "userS3Maps":
					if( usermapsTable.getRowCount() > 0 ) {
						int i = usermapsTable.getSelectedRow();
						if( i >= 0 ) {
							mapname = ALobbyConstants.PATH_USER + File.separator + getMapNameFromTableWithRowSorter( usermapsTable, i );
						}
					}
					break;

				// mapbase-tab
				case "S3mapbase":
					if( mapbaseResultsTable.getRowCount() > 0 ) {
						int i = mapbaseResultsTable.getSelectedRow();
						if( i >= 0 ) {
							mapname = ALobbyConstants.PATH_USER + File.separator + getMapNameFromTableWithRowSorter( mapbaseResultsTable, i );
						}
					}
					break;

				// multiplayer-tab
				case "S3multiplayer":
					if( multiplayerTable.getRowCount() > 0 ) {
						int i = multiplayerTable.getSelectedRow();
						if( i >= 0 ) {
							mapname = ALobbyConstants.PATH_S3MULTI + File.separator + getMapNameFromTableWithRowSorter( multiplayerTable, i );
						}
					}
					break;

				// random-tab
				case "S3randommaps":
					if( randommapTable.getRowCount() > 0 ) {
						int i = randommapTable.getSelectedRow();
						if( i >= 0 ) {
							String mapnamePrepare = randommapTable.getModel().getValueAt(i, 0).toString();

							// get the list of random-maps
							S3RandomMap[] randommaps = getRandomMaps();

		        			// go through the maps and search for the clicked map
		        			// to get its internal name
		        			for (int mapnr = 0; mapnr < randommaps.length; mapnr++) {
		        				if( randommaps[mapnr].toDisplayString().equals(mapnamePrepare) ) {
		        					mapname = ALobbyConstants.PATH_RANDOM + File.separator + randommaps[mapnr].toString();
		        				}
		        			}
						}
					}
					break;
			}

			// start game if mapname is available
			createGame(mapname);
		}
	};

	/**
	 * Zoom the preview image.
	 */
	private final MouseAdapter actionListenerZoomPreview = new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e)
			{
				// left-mouse-click
			    if( SwingUtilities.isLeftMouseButton(e) &&  e.getClickCount() == 1 )
			    {
			    	// get the map-name which is actually visible in the previewPanel
			    	String mapname = ((JPanel) e.getSource()).getName();

			    	// show the window with detailed map-preview
			    	new ShowMapDetail(settlersLobby.getGUI().getMainWindow(), settlersLobby, mapname, false);
			    }
			}
	};

	/**
	 * Close this dialog.
	 */
	private final Action actionListenerToCloseNewGameDialog = new AbstractAction() {

		@Override
        public void actionPerformed(java.awt.event.ActionEvent evt) {
			saveGameSettings();

			// remove the object
			settlersLobby.getGUI().resetMapChoosingDialog();

			// remove the ingameoptions
			if( false == settlersLobby.isIngameOptionsStandalone() ) {
				settlersLobby.getGUI().displayIngameOptions(false);
			}

        	// close the window
        	parentFrame.dispose();
        }
    };

    /**
     * Suggest the actual map in chat.
     */
    private final Action actionListenerToSuggestMap = new AbstractAction() {

		@Override
		public void actionPerformed(ActionEvent e) {
			settlersLobby.actionChatMessage(chosenMapLink);
		}

    };

	private String maptypes;

    /**
     * Save the actual settings for next game.
     */
    private void saveGameSettings() {
    	// save the actual settings
		settlersLobby.config.setnewS3GameGoods(getGoodsInStock().getIndex());
		settlersLobby.config.setnewS3GameRandomPositon(isRandomPosis());
		settlersLobby.config.setnewS3GameMinichat(isNavigationToMiniChatActive());
		settlersLobby.config.setnewS3GameWimo(isWiMo());
		settlersLobby.config.setnewS3GameLeague(isLeagueGame());
		settlersLobby.config.setnewS3GameTab(tabbedPane.getSelectedIndex());
    }

	/**
	 * Get the wrapping dialog-window
	 *
	 * @return OpenNewGameDialog
	 */
	public OpenNewGameDialog getOpenNewGameDialog() {
		return this.openNewGameDialog;
	}

	/**
	 * Start search for S3-Maps in MapBase on base of a search-keyword.
	 *
	 * @return void
	 */
	public void getMapsFromMapBaseBySearchString() {

		// get the search-string from input-field
		String searchString = "";
		if( searchTextField.getText().length() > 0  ) {
			searchString = searchTextField.getText();
			if( searchString.equals(I18n.getString("OpenNewS3GameDialog.MAPBASE_INPUT_PLACEHOLDER")) ) {
				searchString = "";
			}
		}

		// reset the filter for the result-table
		mapbaseResultsTable.setRowSorter(null);

		// get the table-model
		DefaultTableModel dm = (DefaultTableModel) mapbaseResultsTable.getModel();

		// remove all previous results from table-model
		this.removeRowsFromTable(mapbaseResultsTable, dm);

		// check for minimum-size for search-string: 3 or more
		if( searchString.length() > 2 ) {

			final String mySearchString = searchString;

			// show a waiting-text in first column of this table
			dm.addRow(new Object[]{
				I18n.getString("GUI.PLEASE_WAIT")
			});

			// get a separate window with waiting-message
			LoadingJFrame waitingWindow = new LoadingJFrame(this.parentFrame);

			SwingWorker<?, ?> swingWorker = new SwingWorker<Object, Object>() {

				@Override
				protected Object doInBackground() throws Exception {
					String response = "";

					// define the request
		        	httpRequests httpRequests = new httpRequests();
		        	// -> set parameter to configure the JSON-response
		        	httpRequests.addParameter("json", "1");
		        	// -> ignore security-token
		        	httpRequests.setIgnorePhpSessId(true);
		        	// -> show a progess-minitor
		        	httpRequests.setShowProgressMonitor(true);
		        	// -> set the url
		        	String url = "";
					try {
						// encode the search-string and add it to the url
						url = settlersLobby.config.getDefaultMapSearch() + URLEncoder.encode(mySearchString, "UTF-8");
					} catch (UnsupportedEncodingException e2) {
						e2.printStackTrace();
					}
					httpRequests.setUrl(url);
					// -> get the response-String
					try {
						response = httpRequests.doRequest();
					} catch (IOException e) {
					}

					if( httpRequests.isError() ) {

						// remove all previous results from table-model
						removeRowsFromTable(mapbaseResultsTable, dm);

						// show a hint that no results where found
						dm.addRow(new Object[]{
							I18n.getString("OpenNewS3GameDialog.ERROR_CONNECTION_LINE_1")
						});
						dm.addRow(new Object[]{
							I18n.getString("OpenNewS3GameDialog.ERROR_CONNECTION_LINE_2")
						});
					}

					// if response contains contents, then decrypt the JSON-format
					// and show the resulting map-data in the mapbase-table
					if( response.length() > 0 ) {

						// remove all previous results from table-model
						removeRowsFromTable(mapbaseResultsTable, dm);

						JSONObject obj = new JSONObject(response);
						if( !obj.isNull("maps") ) {
							mapbaseResultsTable.addMouseListener(mouseAdapterToCreateGame);
							// insert the results in table
							JSONArray mapArray = obj.getJSONArray("maps");
							setMapsIntoTable(mapArray, mapbaseResultsTable);
						}
						else {
							// show a hint that no results where found
							dm.addRow(new Object[]{
								I18n.getString("OpenNewS3GameDialog.ERROR_NORESULTS")
							});
						}
					}
					else {

						if( !httpRequests.isError() ) {
							// remove all previous results from table-model
							removeRowsFromTable(mapbaseResultsTable, dm);

							// show a hint that there was a connection-error
							dm.addRow(new Object[]{
								I18n.getString("OpenNewS3GameDialog.ERROR_NORESULTS_2")
							});
						}
					}

					return null;
				}

				/**
		         * Executed in event dispatching thread
		         */
		        @Override
		        public void done() {
		        	// close the waiting window
		        	waitingWindow.dispatchEvent(new WindowEvent(waitingWindow, WindowEvent.WINDOW_CLOSING));
		        }

			};

			swingWorker.execute();

		}
		else if( searchString.length() > 0 ) {
			// show a hint that the user must insert a searchstring
			dm.addRow(new Object[]{
				I18n.getString("OpenNewS3GameDialog.ERROR_MINCHARS")
			});
		}
		else {
			// show a hint that the user must insert a searchstring
			dm.addRow(new Object[]{
				I18n.getString("OpenNewS3GameDialog.ERROR_NOSEARCHSTRING")
			});
		}
	}

	/**
	 * Returns the actual chosen mapname.
	 *
	 * @return String
	 */
	public String getChosenMapName() {
		return chosenMapName;
	}

	/**
	 * Load the table-contents depending on active tab in navigation.
	 *
	 * @param activeTab
	 * @return void
	 */
	private void loadMapsIntoTables( Component activeTab ) {

		// go through each table and unselect rows, if some is selected
		for( JTable object: tableList )
		{
			if( object.getRowCount() > 0 && object.getSelectedRowCount() > 0 ) {
				object.clearSelection();
			}
		}

		this.previewPanel.setVisible(false);

		// depending on actual tab-name load its contents
		switch( activeTab.getName() ) {

			// favorites-tab
			case "favoriteMaps":
				loadFavoriteMaps();
				break;

			// user-tab
			case "userMaps":
				// only load userMaps at this point if the table is empty
				// e.g. if the window was opened as tournament-window
				if( usermapsTable.getRowCount() == 0 ) {
					Runnable runnable = new Runnable(){
						public void run(){
							loadUserMaps();
						}
					};
					Thread thread = new Thread(runnable);
					thread.setName("userMapTableThread");
					thread.start();
				}
				break;

			// mapbase-tab
			case "mapbase":
				mapbaseResultsTable.setRowSorter(null);
				// show a hint to the searchfield if table is empty
				if( mapbaseResultsTable.getRowCount() == 0 ) {
					// remove mouse-listener to prevent irritations
					mapbaseResultsTable.removeMouseListener(mouseAdapterToCreateGame);
					// add this map to table with output-values (like map-name)
					DefaultTableModel model = (DefaultTableModel) mapbaseResultsTable.getModel();
					model.addRow(new Object[]{
							I18n.getString("OpenNewS3GameDialog.MAPBASE_HINT_SEARCHFIELD")
					});
				}
				// set cursor in searchfield, so that the user can directly input his search-key
				searchTextField.requestFocusInWindow();
				break;

			// multiplayer-tab
			case "multiplayer":
				loadMultiplayerMaps();
				break;

			// random-tab
			case "S3randommaps":
				randommapTable.setRowSorter(null);
				if( randommapTable.getRowCount() == 0 ) {
					S3RandomMap[] randomMapList = getRandomMaps();
					setMapsIntoTable( randomMapList, randommapTable );
				}
				break;
		}

		// make preview-panel invisible
		previewPanel.setVisible(false);

		setPreferredSize(new Dimension(getPreferredSize().width, defaultWindowHeight));
		openNewGameDialog.pack();
	}

	/**
	 * Load the favorite Maps into table.
	 */
	private void loadFavoriteMaps() {
		favoritesTable.setRowSorter(null);
		// if table is empty get the favorite maps and put them into the table
		if( favoritesTable.getRowCount() == 0 ) {
			// get the maps
			Object[] favoriteMapList = getFavoriteMaps();
			// if no favoriteMaps available the user must have a new installation or is completly new
			// -> show a nice hint :-)
			if( favoriteMapList.length == 0 ) {
				// remove mouse-listener to prevent irritations
				favoritesTable.removeMouseListener(mouseAdapterToCreateGame);
				DefaultTableModel model = (DefaultTableModel) favoritesTable.getModel();
				model.addRow(new Object[]{
					I18n.getString("OpenNewS3GameDialog.NOFAVORITEMAPS_LINE_1"),
					"",
					""
				});
				model.addRow(new Object[]{
					I18n.getString("OpenNewS3GameDialog.NOFAVORITEMAPS_LINE_2"),
					"",
					""
				});
			}
			else {
				// set the mouse-listener to start games via click
				favoritesTable.addMouseListener(mouseAdapterToCreateGame);

				// put maps into table
				setMapsIntoTable( favoriteMapList, favoritesTable );
			}
		}
	}

	/**
	 * Load user maps into table, optional filtered by searchstring.
	 */
	private void loadUserMaps() {
		loadUserMaps(null);
	}
	private void loadUserMaps( String filter ) {
		usermapsTable.setRowSorter(null);
		String path = ALobbyConstants.PATH_USER + File.separator;
		// if table is empty load the user-maps and put them into the table
		if( usermapsTable.getRowCount() == 0 ) {
			Object[] userMapList = getUserMaps(filter);
			if( userMapList.length == 0 ) {
				// remove mouse-listener to prevent irritations
				usermapsTable.removeMouseListener(mouseAdapterToCreateGame);
				DefaultTableModel model = (DefaultTableModel) usermapsTable.getModel();
				model.addRow(new Object[]{
					I18n.getString("OpenNewS3GameDialog.NOUSERMAPS_LINE_1"),
					"",
					""
				});
				model.addRow(new Object[]{
					I18n.getString("OpenNewS3GameDialog.NOUSERMAPS_LINE_1"),
					"",
					""
				});
			}
			else {
				// set the mouse-listener to start games via click
				usermapsTable.addMouseListener(mouseAdapterToCreateGame);

				// put maps into table
				setMapsIntoTable( userMapList, usermapsTable, path );
			}

			// free memory
			userMapList = null;
			System.gc();
		}
	}

	/**
	 * Load multiPlayer maps.
	 */
	private void loadMultiplayerMaps() {
		multiplayerTable.setRowSorter(null);
		if( multiplayerTable.getRowCount() == 0 ) {
			setMapsIntoTable( getMultiplayerMaps(), multiplayerTable, ALobbyConstants.PATH_S3MULTI + File.separator );
		}
	}

	/**
	 * Remove all rows from a JTable-Model.
	 * @param model
	 * @param table
	 *
	 * @param JTable
	 */
	private void removeRowsFromTable(JTable table, DefaultTableModel model) {
		if( table.getRowCount() > 0 ) {
			for (int i = table.getRowCount() - 1; i >= 0; i--) {
				model.removeRow(i);
			}
		}
	}

	/**
	 * Wrapper to load local existing maps for each tab and their data on aLobby-Start.
	 *
	 * Without this preloading the new-game-dialog should be unusable for players with many maps
	 * in their s3-directory.
	 */
	public void loadMaps() {
		Runnable runnable = new Runnable(){
			public void run(){
				// remove all maps from usertable
				DefaultTableModel dm = (DefaultTableModel) usermapsTable.getModel();
				removeRowsFromTable(usermapsTable, dm);
				// load the maps for each table
				loadFavoriteMaps();
				loadUserMaps();
				loadMultiplayerMaps();
			}
		};
		Thread thread = new Thread(runnable);
		thread.setName("loadS3MapsThread");
		thread.start();
	}

	/**
	 * zeigt den S3-Language spezifischen String für die Auswahl des
	 * Warenbestandes in der ComboBox an.
	 *
	 * @author jim
	 *
	 */
	private static class GoodsInStockComboBoxListRenderer extends DefaultListCellRenderer
	{
        @Override
        public Component getListCellRendererComponent(JList<?> list, Object value,
                int index, boolean isSelected, boolean cellHasFocus)
        {
            Component result = super.getListCellRendererComponent(
                    list, value, index, isSelected, cellHasFocus);
            //es soll der S3-Language spezifische Text angezeigt werden,
            //nicht einfach nur toString(), was immer den deutschen String liefern
            //würde. Daher wird einfach nur der Text-String verändert,
            //der zuvor im DefaulListCellRenderer gesetzt wurde
            if (value instanceof GoodsInStock &&
                result instanceof DefaultListCellRenderer)
            {
                ((DefaultListCellRenderer)result).setText(
                        ((GoodsInStock)value).getLanguageString());
            }
            return result;
        }
	}

	/**
	 * Get Mapname from a JTable-object with added rowsorter (or without)
	 *
	 * @param object	table which should be used
	 * @param i			index of entry which should be used
	 * @return
	 */
	private String getMapNameFromTableWithRowSorter(JTable object, int i) {
		if( object.getRowSorter() != null ) {
			i = object.convertRowIndexToModel(i);
		}
		return object.getModel().getValueAt(i, 0).toString();
	}

	/**
	 * Switches the visibility of League- and Tournament-Checkboxes
	 * and sets the tournament-parameter.
	 */
	public void setForTournament( String tournamentid, String tournamentname, String round, String groupnumber, String matchnumber, String playerlist, String maptypes ) {
		this.tournamentid = tournamentid;
		this.tournamentname = tournamentname;
		this.round = round;
		this.groupnumber = groupnumber;
		this.matchnumber = matchnumber;
		this.playerlist = playerlist;
		this.maptypes = maptypes;
		chckbxRandomPosition.setSelected(false);
		chckbxRandomPosition.setEnabled(false);
		chckbxLeague.setVisible(false);
		chckbxTournament.setVisible(true);
		chckbxTournament.setSelected(true);
		btnStartGame.setText("Turnierspiel starten");
		chckbxGoToMiniChat.setEnabled(false);
		chckbxGoToMiniChat.setSelected(true);
		// if only random-maps are allowed disable setmap-tabs and set random-map-tab active
		if( maptypes.equals("randoms") ) {
			tabbedPane.setEnabledAt(0, false);
			tabbedPane.setEnabledAt(1, false);
			tabbedPane.setEnabledAt(2, false);
			tabbedPane.setEnabledAt(3, true);
			tabbedPane.setEnabledAt(4, false);
			tabbedPane.setSelectedIndex(3);
		}
		// if only setmap-maps are allowed disable random-tab and set mapbase-tab active
		if( maptypes.equals("setmaps") ) {
			tabbedPane.setEnabledAt(0, true);
			tabbedPane.setEnabledAt(1, true);
			tabbedPane.setEnabledAt(2, true);
			tabbedPane.setEnabledAt(3, false);
			tabbedPane.setEnabledAt(4, true);
			tabbedPane.setSelectedIndex(2);
		}
	}

	/**
	 * Return the tabbed-pane
	 *
	 * @return
	 */
	public JTabbedPane getTabbedPane() {
		return tabbedPane;
	}

	/**
	 * Return the text-searchfield for mapbase.
	 *
	 * @return
	 */
	public JTextField getMapBaseSearchField() {
		return searchTextField;
	}

}
