/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.CellEditor;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.OverlayLayout;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.plaf.basic.BasicTabbedPaneUI;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import org.json.JSONObject;

import net.siedler3.alobby.communication.IrcUser;
import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.Configuration;
import net.siedler3.alobby.controlcenter.ConfigurationListener;
import net.siedler3.alobby.controlcenter.GoodsInStock;
import net.siedler3.alobby.controlcenter.OpenGame;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.controlcenter.download.CheckAndDownloadMapFile;
import net.siedler3.alobby.controlcenter.state.UserInGameList;
import net.siedler3.alobby.gui.additional.CommunityNewsElement;
import net.siedler3.alobby.gui.additional.DefaultTableModelForCommunityNews;
import net.siedler3.alobby.gui.additional.DefaultTableModelForOpenGames;
import net.siedler3.alobby.gui.additional.DefaultTableModelForRunningGames;
import net.siedler3.alobby.gui.additional.DefaultTableModelForStreams;
import net.siedler3.alobby.gui.additional.ITeamspeakPanel;
import net.siedler3.alobby.gui.additional.ImagePanel;
import net.siedler3.alobby.gui.additional.IngameOptionsWindow;
import net.siedler3.alobby.gui.additional.ListCellRenderer;
import net.siedler3.alobby.gui.additional.ListModel;
import net.siedler3.alobby.gui.additional.MouseWheelListenerForJTabbedPane;
import net.siedler3.alobby.gui.additional.S3MapUserListWindow;
import net.siedler3.alobby.gui.additional.PopupBuilder;
import net.siedler3.alobby.gui.additional.RepeatingImage;
import net.siedler3.alobby.gui.additional.S3User;
import net.siedler3.alobby.gui.additional.TableCellCenterRenderer;
import net.siedler3.alobby.gui.additional.TeamSpeakPanel;
import net.siedler3.alobby.gui.additional.addCursorChangesToButton;
import net.siedler3.alobby.gui.chat.IChatPane;
import net.siedler3.alobby.gui.chat.SingleChatPane;
import net.siedler3.alobby.gui.chat.TabbedChatPaneSwing;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.modules.league.BestRandomersTab;
import net.siedler3.alobby.modules.league.BestSetmapersTab;
import net.siedler3.alobby.modules.league.LeagueTournamentDialog;
import net.siedler3.alobby.modules.league.TournamentDialog;
import net.siedler3.alobby.nativesupport.NativeFunctions;
import net.siedler3.alobby.s3gameinterface.GameStarterVanilla;
import net.siedler3.alobby.s3gameinterface.S3Starter;
import net.siedler3.alobby.util.BlinkWorker;
import net.siedler3.alobby.util.httpRequests;
import net.siedler3.alobby.util.StatisticsManager.StatisticsManager;
import net.siedler3.alobby.util.s4support.S4GameOptions;

/**
 * Die GUI Klasse dient als Ansprechpartner der Klasse SettlersLobby. Sie setzt
 * alle Aufrufe der SettlersLobby um und meldet Benutzeraktionen an die
 * SettlersLobby zurück.
 *
 *
 * Die GUI stellt 5 verschiedene Anzeigen zur Verfügung:
 *
 * In der Anzeige "Einstellungen" kann der Benutzer den Pfad zur s3.exe, die URL
 * zum IRC-Server und das Programminterne Timeout in Sekunden festlegen.
 *
 * In der Anzeige "Login" kann sich der Benutzer durch Eingabe des
 * Benutzernamens und des Passwortes im IRC-Server anmelden.
 *
 * In der Anzeige "Registrieren" kann sich der Benutzer am IRC-Server
 * registrieren.
 *
 * In der Anzeigen "Chat" läuft der Chat des Programms und die Anzeige der
 * offenen Spiele ab.
 *
 * Die Anzeige "Host" wird dargestellt, wenn der Benutzer im Chat ein Spiel
 * erzeut. Tut er dies, wird von SettlersLobby eine Überwachung von Siedler3
 * gestartet, die die aktuellen Veränderungen im Minichat (im Spiel teilnehmende
 * Spieler, freie Plätze, Spielstart) überwacht und meldet. Diese überwachung
 * wird erst beendet, wenn das Spiel tatsächlich startet, oder der Benutzer in
 * dieser Ansicht auf einen Button bspspw. "Spiel abgebrochen" klickt.
 *
 * Zusätzlich stellt die GUI noch einen Fehlerdialog zur Verfügung.
 *
 * Schließt der Benutzer das Programm (X-Button, [Alt][F4]), meldet das die GUI
 * an SettlersLobby.actionWantsToKillProgramm() weiter.
 *
 * @author Alexander Gruner (ruler)
 * @author Stephan Bauer (aka maximilius)
 * @author Jim
 */
public class GUI implements ConfigurationListener
{
	public static BufferedImage backgroundImage;
	private static Image s3SmImageGreen;
	private static Image s3SmImageRed;
	private static ImageIcon s3SmIconGreen;
	private static ImageIcon s3SmIconRed;
	
	static
	{
        try
        {
            backgroundImage = ImageIO.read(GUI.class.getResourceAsStream(
                    ALobbyConstants.PACKAGE_PATH + "/gui/img/GameOptionsScreen.png")); //$NON-NLS-1$
            s3SmImageGreen = ImageIO.read(GUI.class.getResourceAsStream(
                    ALobbyConstants.PACKAGE_PATH + "/gui/img/s3SmImageGreen.png")); //$NON-NLS-1$
            s3SmImageRed = ImageIO.read(GUI.class.getResourceAsStream(
                    ALobbyConstants.PACKAGE_PATH + "/gui/img/s3SmImageRed.png")); //$NON-NLS-1$
            s3SmIconGreen = new ImageIcon(s3SmImageGreen);
            s3SmIconRed = new ImageIcon(s3SmImageRed);
            
        } catch (IOException e)
        {
            backgroundImage = null;
            Test.outputException(e);
        }
	}

	//Icon
	private Image icon;
	private final PopupBuilder popupBuilder;

	public final static String PROGRAM_NAME = "aLobby " //$NON-NLS-1$
			+ ALobbyConstants.VERSION + SettlersLobby.getBetaVersionMarker();

	private final OptionsFrame optionsFrame;
	private final FirstFrame firstFrame;

	// Komponenten für das Hauptfenster (Chat, offene Spiele, online Users)
	private final JFrame windowMain = new JFrame(PROGRAM_NAME);
	private JPanel wrapperPanel = new JPanel();
	private JButton btnCreateGame = new JButton(I18n.getString("GUI.NEW_GAME")); //$NON-NLS-1$
	private JButton btnLeague = new JButton(I18n.getString("GUI.COMPETITIONS")); //$NON-NLS-1$
	private JButton btnSaveManager = new JButton(I18n.getString("GUI.SAVEMANAGER"), s3SmIconRed); //$NON-NLS-1$
	private JButton btnStatisticsManager = new JButton(I18n.getString("GUI.BUTTON_STATISTICS_MANAGER"));
	private JTextField lblIp = new JTextField();
	private JButton btnVpnState = new JButton();
	private JButton btnLogout = new JButton(); //$NON-NLS-1$
	private JButton btnSettings = new JButton(); //$NON-NLS-1$

	private JTabbedPane tabbedPane = new JTabbedPane();
	private final JScrollPane scrollPaneRunningGames = new JScrollPane();
	private JTable tableRunningGames = new JTable()
	{
		@Override
		public boolean isCellEditable(int row, int column)
		{
			return false;
		}
		@Override
        public String getToolTipText(MouseEvent e)
        {
            //für jede Zeile wird ist intern ein ToolTip gespeichert
            //dieser wird ermittelt und angezeigt.
            String toolTip = null;
            java.awt.Point p = e.getPoint();
            int rowIndex = rowAtPoint(p);
            int realRowIndex = convertRowIndexToModel(rowIndex);
            try
            {
                toolTip = ((DefaultTableModelForRunningGames) getModel()).getToolTipAt(realRowIndex);
            }
            catch (Exception e2)
            {
            }
            return toolTip;
        }

        @Override
        public Point getToolTipLocation(MouseEvent event)
        {
            //durch den folgenden Code wandert der Tooltip mit dem
            //Mauszeiger mit, so wie bei der listUsersOnline
            if (event.getID() == MouseEvent.MOUSE_MOVED)
            {
                return new Point(event.getX() + (int) (10 * MessageDialog.getDpiScalingFactor()), event.getY() + (int) (20 * MessageDialog.getDpiScalingFactor()));
            }
            return super.getToolTipLocation(event);
        }
	};
	private final JScrollPane scrollPaneStreamedGames = new JScrollPane();
	private JTable tableStreamedGames = new JTable()
	{
		@Override
		public boolean isCellEditable(int row, int column)
		{
			return false;
		}
	};
	private final JScrollPane scrollPaneGames = new JScrollPane();
	private final JScrollPane scrollCommunityNews = new JScrollPane();
	
	public OpenNewS3GameDialog s3PanelDialogObject;
	public OpenNewS4GameDialog s4PanelDialogObject;

	private JTable tableOpenGames = new JTable()
	{
		@Override
		public boolean isCellEditable(int row, int column)
		{
		    if (convertColumnIndexToModel(column) == 0)
		    {
		        //Das Namensfeld vom eigenen Spiel soll editierbar sein,
		        //alle anderen nicht
		        int selectedRow = convertRowIndexToModel(row);
		        if (isOwnOpenHostGameAt(selectedRow))
		        {
                    return true;
                }
		    }
			return false;
		}

        @Override
        public void editingStopped(ChangeEvent e)
        {
            super.editingStopped(e);
            //der JTable ist ein Listener für sein eigenes Model und für die
            //CellEditors. Nur das Feld mit dem Namen des eigenen Spiels ist
            //editierbar, daher kann das ChangeEvent nur von diesem Editor
            //ausgelöst worden sein. Daher kann man direkt
            //settlersLobby.newGameName() aufrufen
            //eigentlich soll man sich hier nicht reinklinken, aber es klappt
            //wunderbar
            CellEditor editor = (CellEditor)e.getSource();
            settlersLobby.newGameName(editor.getCellEditorValue().toString());
        }

        @Override
        public String getToolTipText(MouseEvent e)
        {
            //für jede Zeile wird ist intern ein ToolTip gespeichert
            //dieser wird ermittelt und angezeigt.
            String toolTip = null;
            java.awt.Point p = e.getPoint();
            int rowIndex = rowAtPoint(p);
            int realRowIndex = convertRowIndexToModel(rowIndex);
            try
            {
                toolTip = ((DefaultTableModelForOpenGames) getModel()).getToolTipAt(realRowIndex);
            }
            catch (Exception e2)
            {
            }
            return toolTip;
        }

        @Override
        public Point getToolTipLocation(MouseEvent event)
        {
            //durch den folgenden Code wandert der Tooltip mit dem
            //Mauszeiger mit, so wie bei der listUsersOnline
            if (event.getID() == MouseEvent.MOUSE_MOVED)
            {
                return new Point(event.getX() + (int) (10 * MessageDialog.getDpiScalingFactor()), event.getY() + (int) (20 * MessageDialog.getDpiScalingFactor()));
            }
            return super.getToolTipLocation(event);
        }

	};
	private final JPopupMenu tableGamesPopup;
	
	private JTable tableCommunityNews = new JTable()
	{
		@Override
		public boolean isCellEditable(int row, int column)
		{
			return false;
		}

        @Override
        public String getToolTipText(MouseEvent e)
        {
            //für jede Zeile wird ist intern ein ToolTip gespeichert
            //dieser wird ermittelt und angezeigt.
            String toolTip = null;
            java.awt.Point p = e.getPoint();
            int rowIndex = rowAtPoint(p);
            int realRowIndex = convertRowIndexToModel(rowIndex);
            try
            {
                toolTip = ((DefaultTableModelForCommunityNews) getModel()).getToolTipAt(realRowIndex);
            }
            catch (Exception e2)
            {
            }
            return toolTip;
        }

        @Override
        public Point getToolTipLocation(MouseEvent event)
        {
            //durch den folgenden Code wandert der Tooltip mit dem
            //Mauszeiger mit, so wie bei der listUsersOnline
            if (event.getID() == MouseEvent.MOUSE_MOVED)
            {
                return new Point(event.getX() + (int) (10 * MessageDialog.getDpiScalingFactor()), event.getY() + (int) (20 * MessageDialog.getDpiScalingFactor()));
            }
            return super.getToolTipLocation(event);
        }

	};

	private final JList<S3User> listUsersOnline = new JList<S3User>() {
		@Override
	    public Point getToolTipLocation(MouseEvent event)
	    {
	        if (event.getID() == MouseEvent.MOUSE_MOVED)
	        {
	            return new Point(event.getX() + 10, event.getY() + 20);
	        }
	        return super.getToolTipLocation(event);
	    }
	};
	private final JPanel pnlEast = new JPanel();
	private final JPanel pnlSouth = new JPanel();
	private final JPanel pnlWest = new JPanel();
	private final JPanel pnlWestSpace = new JPanel();
	private final JScrollPane scrollPaneUsersOnline = new JScrollPane();
	private IChatPane chatPane;


	/**
	 * Referenz auf das Objekt der Klasse SettlersLobby, um Benutzeraktionen an
	 * diese zu melden.
	 */
	private SettlersLobby settlersLobby;

	// Komponenten für das Kartenauswahlfenster
	private OpenNewGameDialog mapChoosingDialog;
	
	private LeagueTournamentDialog leagueTournamentDialog;

	private S3MapUserListWindow mapUserListWindow;
	private S4GameOptions s4gameOptions;
	private IngameOptionsWindow ingameOptionsWindow;
	private final JTabbedPane onDemandWindow = new JTabbedPane();

	// Komponenten für das "Bitte warten"-Fenster
	/*
	 * Auskommentiert weil: "Bitte warten" - Fenster wird nicht richtig
	 * gezeichnet.
	 *
	 * private JFrame windowPleaseWait = new JFrame("Please wait"); private
	 * JLabel lblPleaseWait = new JLabel();
	 */
	private Dialog waitDialog;

	private ListModel usersOnline; // enthält alle user
	
	/**
	 * Get all necessary data to view community news
	 */
	private DefaultTableModelForCommunityNews tableOfCommunityNews;
	
	/**
	 * Get all necessary data to view open games.
	 */
	private DefaultTableModelForOpenGames tableOfOpenGames;
	
	/**
	 * Get all necessary data to view running games.
	 */
	private DefaultTableModelForRunningGames tableOfRunningGames;
	
	/**
	 * Get all necessary data to view Streams
	 */
	private DefaultTableModelForStreams tableOfStreams;

	// Pointer auf das aktuell sichtbare Fenster
	protected JFrame currentActiveFrame;

    private final JTextField topic = new JTextField(" "); //$NON-NLS-1$

    // Komponenten für das Spiele Options Fenster
    private final JFrame windowIngameOptions = new JFrame(PROGRAM_NAME
            + " > " + I18n.getString("GUI.GAME_OPTIONS")); //$NON-NLS-1$ //$NON-NLS-2$
    private final JButton btnCreateGameStandalone = new JButton(I18n.getString("GUI.NEW_GAME")); //$NON-NLS-1$
    private IngameOptionsWindow pnlIngameOptions;
    private final JButton btnDefaultIngameSettings = new JButton(I18n.getString("GUI.DEFAULTS")); //$NON-NLS-1$
    private JDialog waitMapDialog;

    private ITeamspeakPanel teamSpeakPanel;
    private boolean isHideTeamspeakWindowOriginal;


	/***************************************************************************
	 * EREIGNISSE:
	 **************************************************************************/

	// Beim Schließen eines Fensters
	public WindowAdapter windowAdapterToCloseWindow = new WindowAdapter()
	{
		@Override
		public void windowClosing(WindowEvent evt)
		{
			settlersLobby.config.setScreen(evt.getComponent().getGraphicsConfiguration().getDevice().getIDstring());
			settlersLobby.actionWantsToKillProgramm();
		}
	};
	
	// Nach Klicken auf "Eigenes Spiel", rufe
	// SettlersLobby.actionWantsToCreateGame() auf
	private final Action actionListenerToCreateGame = new AbstractAction()
	{
		@Override
		public void actionPerformed(ActionEvent evt)
		{
			settlersLobby.actionWantsToCreateGame();
		}
	};

    private final Action actionListenerToOpenLeagueWindow = new AbstractAction()
	{
        @Override
        public void actionPerformed(ActionEvent e)
        {
            displayLeagueGamesDialog();
        }
    };

    private final Action actionListenerToCreateGameStandalone = new AbstractAction()
    {
        @Override
		public void actionPerformed(ActionEvent evt)
        {
            settlersLobby.actionWantsToCreateGameStandalone();
        }
    };

	// Beim Klicken auf auf "Einstellungen", rufe
	// settlersLobby.actionWantsToEditSettings() auf
	public ActionListener actionListenerToOpenSettings = new ActionListener()
	{
		@Override
		public void actionPerformed(ActionEvent evt)
		{
			settlersLobby.actionWantsToEditSettings();
		}
	};

	/**
	 * After click on "Save Manager" opens the Save Manager.
	 */
	private final ActionListener actionListenerToSaveManager = new ActionListener()
	{
		@Override
		public void actionPerformed(ActionEvent evt)
		{
			try
			{
				settlersLobby.saveManager.showConfigWindow(); //$NON-NLS-1$
			} catch (Exception e) {
				displayError(I18n.getString("GUI.ERROR_LOADING_SAVEMANAGER"));
				Test.outputException(e);
			}	
		}
	};
	
	// After click on "Statistics Manager",
	// opens the Statistics Manager
	private final ActionListener actionListenerToStatisticsManager = new ActionListener()
	{
		@Override
		public void actionPerformed(ActionEvent evt)
		{
			try
			{
				Test.output("statistik manager starten");
				new StatisticsManager(settlersLobby);
			} catch (Exception e) {
				displayError(I18n.getString("GUI.ERROR_LOADING_STATIMANAGER"));
				Test.outputException(e);
			}	
		}
	};

    private final ActionListener actionListenerToSetDefaultIngameOptions = new ActionListener()
    {
        @Override
		public void actionPerformed(ActionEvent e)
        {
            settlersLobby.actionDefaultIngameOptionsTasks();
            //die Default Werte müssen auch im Fenster angezeigt werden
            //dazu ist ein manueller Aufruf nötig, da keine Listener benutzt werden
            updateSelectedIngameOptions();
        }
    };
    
    /**
     * Define mouseListener for click on Community-News.
     */
 	private final MouseAdapter mouseAdapterCommunityNews = new MouseAdapter()
 	{
 		@Override
		public void mouseClicked(MouseEvent e)
		{
 			if (SwingUtilities.isLeftMouseButton(e))
		    {
		        if (e.getClickCount() > 1)
		        {
    		        //nur der erste Klick löst eine Aktion aus, alle weiteren
    		        //nicht, bis dazwischen eine entsprechend grosse Pause liegt,
    		        //damit es wieder als erster Klick gewertet wird
    		        return;
		        }
    			int selectedRow = tableCommunityNews.getSelectedRow(); // returns index of
    			// row, else -1
    			if (selectedRow != -1) {
    				int realSelectedRow = tableCommunityNews.convertRowIndexToModel(selectedRow);
    				CommunityNewsElement newsElement = tableOfCommunityNews.getCommunityNewsElementAt(realSelectedRow);
    				if( newsElement.getLink().length() > 0 ) {
    					openURL(newsElement.getLink());
    				}
    			}
		    }
		}
 	};

	/**
	 * Call game start-function after click on a game incl. all checks regarding league and vpn-key.
	 */
	private final MouseAdapter mouseAdapterToJoinGame = new MouseAdapter()
	{
		private JCheckBox leagueRules;
		private LinkLabel messageWithLeagueRulesLink;
		private JButton leagueRulesOkButton;

		@Override
		public void mouseClicked(MouseEvent e)
		{
		    if (SwingUtilities.isLeftMouseButton(e))
		    {
		        if (e.getClickCount() > 1)
		        {
    		        //nur der erste Klick löst eine Aktion aus, alle weiteren
    		        //nicht, bis dazwischen eine entsprechend grosse Pause liegt,
    		        //damit es wieder als erster Klick gewertet wird
    		        return;
		        }
    			int selectedRow = tableOpenGames.getSelectedRow(); // returns index of
    			// row, else -1
    			if (selectedRow != -1)
    			{
    			    OpenGame game = tableOfOpenGames.getOpenGameAt(selectedRow);
    			    // if this is an tournament-game, than check if the user exists 
    			    // in the tournament-playerlist
    			    // -> if not, than show a hint and do not join the game
    			    if (isOwnOpenHostGameAt(selectedRow))
    			    {
    			        //nichts tun, wenn man das eigene erstellte Spiel anklickt
    			    }
    			    else
    			    {
    			    	if( game.isTournamentGame() ) {
    			    		// check if actual player is allowed to join this tournament-game
    						try {
    							httpRequests httpRequests = new httpRequests();
        						httpRequests.setUrl(settlersLobby.config.getTournamentUserAllowedUrl() + settlersLobby.getNick().toLowerCase());
        						httpRequests.addParameter("turnierid", game.getTournamentId() );
        						httpRequests.addParameter("matchnumber", game.getTournamentMatchnumber() );
        						httpRequests.addParameter("round", game.getTournamentRound() );
        						httpRequests.addParameter("group", game.getTournamentGroupnumber() );
								String result = httpRequests.doRequest().trim();
								
								if( result.equals("ok") ) {
									settlersLobby.actionWantsToJoinGame(game);
								}
								else {
									// user is not allowed to join this game
	    			    			MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
	    			            	// -> add message
	    			        		messagewindow.addMessage(
	    			        			"Du stehst nicht in der Liste der für dieses Turnier-Spiel zugelassenen Spieler. Daher kannst Du diesem Spiel nicht beitreten.", 
	    			        			UIManager.getIcon("OptionPane.errorIcon")
	    			        		);
	    			        		// -> add ok-button
	    			        		messagewindow.addButton(
	    			        			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
	    			        			new ActionListener() {
	    			        			    @Override
	    			        			    public void actionPerformed(ActionEvent e) {
	    			        			    	messagewindow.dispose();
	    			        			    }
	    			        			}
	    			        		);
	    			        		// show it
	    			        		messagewindow.setVisible(true);
								}
							} catch (IOException e1) {
								e1.printStackTrace();
							}
        			    }
    			    	else if( game.isLeagueGame() && !game.getMap().toLowerCase().contains("savegame") ) {
    			    		if( !settlersLobby.checkIfUserIsInLeague() ) {
    							String hexcolor = SettlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor());
    				 			MessageDialog messagewindowleague = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
    				 			messageWithLeagueRulesLink = messagewindowleague.addMessage(
    			         			"<html>" + MessageFormat.format(I18n.getString("OpenNewS3GameDialog.LEAGUE_HINT"), hexcolor) + "</html>",
    				         		UIManager.getIcon("OptionPane.informationIcon")
    				         	);
    				 			messageWithLeagueRulesLink.addMouseListener(new MouseAdapter() {
									@Override
    				 	            public void mouseClicked(MouseEvent e) {
    				 	            	if( messageWithLeagueRulesLink.getLinkClicked() ) {
    				 	            		leagueRules.setEnabled(true);
    				 	            		leagueRulesOkButton.setEnabled(true);
    				 	            	}
    				 	            }
    				 	        });
    				 			//messageWithLeagueRulesLink.getLinkClicked()
    				 			leagueRules = messagewindowleague.addPanelWithLabelAndCheckbox(I18n.getString("OpenNewS3GameDialog.LEAGUE_ACCEPT"), "");
    				 			leagueRules.setEnabled(false);
    				 			leagueRulesOkButton = messagewindowleague.addButton(
    				         		I18n.getString("OpenNewS3GameDialog.LEAGUE_OK"),
    				         		new ActionListener() {
    				         		    @Override
    				         		    public void actionPerformed(ActionEvent e) {
    				         		    	if( false == leagueRules.isSelected() ) {
    				         		    		MessageDialog messagewindowleague_error = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
    				         		    		messagewindowleague_error.addMessage(
    				         		    			I18n.getString("OpenNewS3GameDialog.LEAGUE_HINT2"),
    								         		UIManager.getIcon("OptionPane.errorIcon")
    								         	);
    				         		    		messagewindowleague_error.addButton(
    			    				         		I18n.getString("ErrorDialog.OK"),
    			    				         		new ActionListener() {
    			    				         		    @Override
    			    				         		    public void actionPerformed(ActionEvent e) {
    			    				         		    	messagewindowleague_error.dispose();
    			    				         		    }
    			    				         		}
    			    				         	);
    				         		    		messagewindowleague_error.setVisible(true);
    				         		    	}
    				         		    	else {
    				         		    		// start the game and close this window
    				         		    		settlersLobby.actionWantsToJoinGame(game);
    				         		    		messagewindowleague.dispose();
    				         		    	}
    				         		    }
    				         		}
    				         	);
    				 			leagueRulesOkButton.setEnabled(false);
    				 			messagewindowleague.addButton(
    				         		I18n.getString("OpenNewS3GameDialog.LEAGUE_NOT_OK"),
    				         		new ActionListener() {
    				         		    @Override
    				         		    public void actionPerformed(ActionEvent e) {
    				         		    	// close this window
    				         		    	messagewindowleague.dispose();
    				         		    }
    				         		}
    				         	);
    				 			messagewindowleague.setVisible(true);
    						}
    			    		else {
    			    			settlersLobby.actionWantsToJoinGame(game);
    			    		}
    			    	}
    			    	else {
    			    		settlersLobby.actionWantsToJoinGame(game);
    			    	}
    			    }
    			}
		    }
        }
	};
	
	/**
	 * Change cursor if mouse hovers over a jTable 
	 */
	private final MouseMotionAdapter mouseAdapterOnMotion = new MouseMotionAdapter() {
		public void mouseMoved(MouseEvent e)
        {
			JTable tableObject = (JTable)e.getSource();
			tableObject.setCursor(new Cursor(Cursor.HAND_CURSOR));
        }
	};
	
	private final MouseAdapter mouseAdapterToGetMapInfo = new MouseAdapter() {

		//the methods below are needed only for the popup menu
		@Override
		public void mousePressed(MouseEvent e)
		{
		    showPopup(e);
		}

		@Override
		public void mouseReleased(MouseEvent e)
		{
		    showPopup(e);
		}

		private void showPopup(MouseEvent e)
		{
            if (e.isPopupTrigger())
            {
                //select the row in case the delete will be activated in the popupmenu
                //(so we can simply use tableGames.getSelectedRow() then).
                //This does not activate the joining of a game.
                //code taken from http://www.stupidjavatricks.com/?p=12
            	JTable clickedTable = null;
            	int rownumber_mapname = 0;
            	int rownumber_misc = 0;
                if (e.getSource() == tableOpenGames)
                {
                	clickedTable = tableOpenGames;
                	rownumber_mapname = 1;
                	rownumber_misc = 5;
                }
                if (e.getSource() == tableRunningGames)
                {
                	clickedTable = tableRunningGames;
                	rownumber_mapname = 2;
                	rownumber_misc = 5;
                }
                if( clickedTable != null ) {
                    Point p = e.getPoint();
                    // get the row index that contains that coordinate
                    int rowNumber = clickedTable.rowAtPoint( p );
                    // Get the ListSelectionModel of the JTable
                    ListSelectionModel model = clickedTable.getSelectionModel();
                    // set the selected interval of rows. Using the "rowNumber"
                    // variable for the beginning and end selects only that one row.
                    model.setSelectionInterval( rowNumber, rowNumber );
                }
                
                // if clicked map is a random map, than hide 3 mapbase-items in popup
            	// otherwise show them
                // hide also the league-menuitem if the game is not a league-game
                boolean visibility = false;
                boolean visibilityOfLeague = false;
                if( clickedTable != null && rownumber_mapname > 0 ) {
            		if( clickedTable.getSelectedRow() >= 0 ) {
            			String mapname = clickedTable.getModel().getValueAt(clickedTable.getSelectedRow(), rownumber_mapname).toString().replace(ALobbyConstants.LABEL_SAVE_GAME + File.separator, "");
            			if( mapname.toLowerCase().indexOf(File.separator + ALobbyConstants.PATH_RANDOM.toLowerCase()) == -1 && mapname.indexOf(ALobbyConstants.PATH_RANDOM + File.separator) == -1 && !mapname.equals(ALobbyConstants.LABEL_UNKNOWN_MAP) ) {
            				visibility = true;
            			}
            			if( !Configuration.getInstance().isHideLeagueInterface() ) {
            				String league = clickedTable.getModel().getValueAt(clickedTable.getSelectedRow(), clickedTable.getModel().getColumnCount()-1).toString();
            				if( league.equals(I18n.getString("GUI.LEAGUE")) ) {
            					visibilityOfLeague = true;
            				}
            			}
            		}
                }
            	// hide all options for S4-games
            	if( !settlersLobby.config.isHideS4Interface() && clickedTable != null && rownumber_misc > 0 ) {
        			if( clickedTable.getSelectedRow() >= 0 ) {
        				String misc = clickedTable.getModel().getValueAt(clickedTable.getSelectedRow(), rownumber_misc).toString();
        				if( misc == I18n.getString("GUI.VERSION_S4_CD_GOG") ) {
        					visibility = false;
        					visibilityOfLeague = false;
        				}
        			}
            	}
                for (int i = 0; i < tableGamesPopup.getComponents().length; ++i) {
					tableGamesPopup.getComponent(i).setVisible(visibility);
				}
                
                // set visibility of League-Option
                if( !Configuration.getInstance().isHideLeagueInterface() ) {
                	tableGamesPopup.getComponent(4).setVisible(visibilityOfLeague);
                }
            	
                // show the popup
                tableGamesPopup.show(e.getComponent(), e.getX(), e.getY());
                
            }
		}
	};
	
	/**
	 * Open stream of clicked entry in default browser.
	 */
	private final MouseAdapter mouseAdapterToGetStream = new MouseAdapter() {

		//the methods below are needed only for the popup menu
		@Override
		public void mousePressed(MouseEvent e)
		{
			// when the popup was activated, the selected row was also adjusted.
            int selectedRow = tableStreamedGames.getSelectedRow();
            if (selectedRow != -1)
            {
                String link = (String) tableStreamedGames.getModel().getValueAt(selectedRow, 1);
                openURL(link);
            }
		}

		@Override
		public void mouseReleased(MouseEvent e)
		{
		}

	};

	// Nach Klicken auf "Logout", rufe SettlersLobby.actionWantsToLogOut() auf
	private final ActionListener actionListenerToLogout = new ActionListener()
	{
		@Override
		public void actionPerformed(ActionEvent evt)
		{
		    // Allowing the user to log out and log in again has too many unforeseen consequences (==bugs),
		    // that cannot be easily fixed, so lets kill the program completely.
			settlersLobby.actionWantsToKillProgramm();
		}
	};

	private final ActionListener actionListenerCopyIp = new ActionListener()
	{
		@Override
		public void actionPerformed(ActionEvent evt)
		{
			StringSelection selection=new StringSelection(settlersLobby.getIp());
			Clipboard clipboard=Toolkit.getDefaultToolkit().getSystemClipboard();
			if (clipboard != null) {
			    clipboard.setContents(selection,selection);
			}
		}
	};

	/**
	 * Start and stop vpn-connection via button-click.
	 */
	private final ActionListener actionListenerVpnState = new ActionListener()
	{
		@Override
		public void actionPerformed(ActionEvent evt)
		{
			// vpn not conntected -> start it
			if (settlersLobby.getVpnIpFromSystem() == null) {
				setVpnText(I18n.getString("\uf127"), Color.yellow, I18n.getString("SettlersLobby.VPN_STARTING"));
				try {
					settlersLobby.startVpn();
				} catch (Exception e) {
					setVpnText("\uf127", Color.red, I18n.getString("SettlersLobby.VPN_OFF"));
				}
			} else {
				// vpn is connected -> stop it
				setVpnText("\uf127", Color.yellow, I18n.getString("SettlersLobby.VPN_STOPPING"));
				try {
					settlersLobby.stopVpn();
				} catch (Exception e) {
					setVpnText("\uf127", Color.red, I18n.getString("SettlersLobby.VPN_OFF"));
				}
			}
		}
	};

    private final WindowListener windowListenerToAbortGame = new WindowAdapter()
    {
        @Override
		public void windowClosing(WindowEvent evt)
        {
            if (settlersLobby.isIngameOptionsStandalone())
            {
                settlersLobby.actionCreateGameAbortedStandalone();
            }
            else
            {
                settlersLobby.actionCreateGameAbortedFromMapSelectionWindow();
            }
        }
    };

	/** 
      * use nickname in chat on left-click.
      * show popup in right-click.  
      */
	private final MouseAdapter mouseAdapterForUserList = new MouseAdapter()
	{
		@Override
		public void mouseClicked(MouseEvent evt)
		{
		    S3User user = getClickedUser(evt);
			if (user != null)
			{
				if (SwingUtilities.isRightMouseButton(evt))
				{
					JPopupMenu popupMenu = popupBuilder.cr8PopupMenu(user);
					popupMenu.show(evt.getComponent(), evt.getX(), evt.getY());
				}
				else if (SwingUtilities.isLeftMouseButton(evt))
				{
					if( false == chatPane.getInputText().endsWith(user.getNick() + " ") ) {
						chatPane.addInputText(user.getNick() + " "); //$NON-NLS-1$
					}
				}
				else if (SwingUtilities.isMiddleMouseButton(evt))
				{
				    //middle mouse button selects the user for the next private message
				    chatPane.selectMessageRecipient(user);
				}
			}
		}
	};
	public ShowMapDetail showMapDetail = null;
	private BlinkWorker btnVpnStateWorker;
	private boolean guiInitialized = false;
	private int tabbedPanePreviousTab = 0;

	/**
	 * Prüft anhand des empfangenen MouseEvents, in welcher Zeile der Userliste
	 * geklickt wurde und gibt den entsprechenden User in dieser Zeile zurück.
	 *
	 * @param event Mouseevent
	 * @return User, auf den geklickt wurde
	 */
	private S3User getClickedUser(MouseEvent event)
	{
	    S3User user = null;
        if (event != null && event.getSource() == listUsersOnline)
        {
            try
            {
                Point p = event.getPoint();
                int index = listUsersOnline.locationToIndex(p);
                Object obj = listUsersOnline.getModel().getElementAt(index);
                if (obj instanceof S3User)
                {
                    user= ((S3User)obj);
                }
            }
            catch (Exception e)
            {
                //IndexOutOfBoundsException oder sonstige Exceptions
                //werden ignoriert, in dem Fall wird null
                //zurückgegeben
                user = null;
            }
        }
        return user;
	}

	/**
	 * Macht das aktuell sichtbare Fenster unsichtbar, macht das übergebene
	 * sichtbar und speichert es als aktuell sichtbares ab.
	 *
	 * @param frame
	 *            Die Referenz auf das Fenster, welches sichtbar gemacht werden
	 *            soll.
	 */
	public void setNewCurrentActiveFrame(JFrame frame)
	{
		hideCurrentFrame();
		currentActiveFrame = frame;
		currentActiveFrame.setVisible(true);
	}

    public void hideCurrentFrame()
    {
        if (currentActiveFrame != null)
		{
            if (currentActiveFrame == windowMain)
            {
                hideChat();
            }
            else
            {
                currentActiveFrame.setVisible(false);
            }
            currentActiveFrame = null;
		}
    }

    /**
     * Get center location on screen for given window-component.
     * 
     * @param component
     * @return
     */
	static protected Point getCenterLocationForWindow(Component component)
	{
	    try
	    {
    		Dimension frameSize = new Dimension(component.getWidth(), component.getHeight());

    		// Größe des Bildschirms ermitteln
    		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

    		// Position des Fensters errechnen
    		int left = Math.max((screenSize.width - frameSize.width) / 2, 0);
    		int top = Math.max((screenSize.height - frameSize.height) / 2, 0);

    		return new Point(left, top);
	    } catch (Exception e) {
	        Test.outputException(e);
	        return new Point(0,0);
        }
	}

	/**
	 * Der Konstruktor der GUI.
	 *
	 * @param settlersLobby
	 *            Die Referenz zum Objekt der Klasse SettlersLobby.
	 */
	@SuppressWarnings("static-access")
	public GUI(SettlersLobby settlersLobby)
	{
		// get the icon
		try
		{
			icon = ImageIO.read(getClass().getResourceAsStream(
			        ALobbyConstants.PACKAGE_PATH + "/gui/img/icon.png")); //$NON-NLS-1$
		} catch (IOException e)
		{
			icon = null;
		}
        setIcon(windowMain);

        // secure settlerLobby-object 
		this.settlersLobby = settlersLobby;

		// get the TS-window-visibility-setting
		isHideTeamspeakWindowOriginal = settlersLobby.config.isHideTeamspeakWindow();
		if (settlersLobby.config.isTabbedChatPane())
		{
		    chatPane = new TabbedChatPaneSwing(settlersLobby, this);
		}
		else
		{
		    chatPane = new SingleChatPane(settlersLobby, this);
		}
		
		// create the newgame-dialog
		mapChoosingDialog = this.createOpenNewGameDialog();
		
		// create the options-dialog
		optionsFrame = new OptionsFrame(settlersLobby, String.format("%s > %s", PROGRAM_NAME, I18n.getString("GUI.SETUP")), icon); //$NON-NLS-1$ //$NON-NLS-2$
		optionsFrame.setLocation(getCenterLocationForWindow(optionsFrame));
		this.settlersLobby.showOnSpecifiedScreen(this.settlersLobby.config.getScreen(), optionsFrame);
		
		// create the firstFrame-window which is visible before user logged in
		firstFrame = new FirstFrame(settlersLobby, PROGRAM_NAME); //$NON-NLS-1$ //$NON-NLS-2$
		firstFrame.setIconImage(icon);
		firstFrame.setLocation(getCenterLocationForWindow(firstFrame));
		firstFrame.addWindowListener(windowAdapterToCloseWindow);
		firstFrame.setBackground(settlersLobby.theme.startBackgroundColor);
		settlersLobby.showOnSpecifiedScreen(settlersLobby.config.getScreen(), firstFrame);

		/***********************************************************************
		 * LISTENER ANMELDEN, Fenster: Main
		 **********************************************************************/
        // Beim Schließen des Fensters
		windowMain.addWindowListener(windowAdapterToCloseWindow);
		//um die Größenänderungen des Fensters mitzubekommmen und zu speichern,
		//wird ein ComponenListener installiert
		windowMain.addComponentListener(new ComponentAdapter()
		{
            @Override
            public void componentMoved(ComponentEvent e)
            {
                update(e);
            }

            @Override
            public void componentResized(ComponentEvent e)
            {
                update(e);
            }

            private void update(ComponentEvent e)
            {
                int state = ((JFrame)e.getComponent()).getExtendedState();
                boolean isMaximised = (state & Frame.MAXIMIZED_BOTH) == Frame.MAXIMIZED_BOTH;

                if (!isMaximised)
                {
                    //nur wenn das Fenster nicht maximiert ist, wird die Änderung
                    //übernommen
                    GUI.this.settlersLobby.config.setWindowMainPosition(
                            e.getComponent().getBounds());
                }
                //status von isMaximised wird jedesmal gespeichert, so ist der Wert
                //auch aktuell, wenn das Fenster geschlossen wird
                GUI.this.settlersLobby.config.setWindowMainMaximised(isMaximised);
            }
		});
		
		// get the fontawesome-font with font-size 14
		Font font = settlersLobby.config.getFontAweSome((float) (14 * MessageDialog.getDpiScalingFactor()), Font.BOLD);
		
		// set fontawesome-icons for some buttons
		btnVpnState.setText("\uf127");
		btnVpnState.setToolTipText(I18n.getString("SettlersLobby.VPN_OFF"));
		btnVpnState.setFont(font);
		btnLogout.setFont(font);
		btnLogout.setText("\uf08b");
		btnSettings.setFont(font);
		btnSettings.setText("\uf013");

		btnCreateGame.addMouseListener(new addCursorChangesToButton());
		btnLeague.addMouseListener(new addCursorChangesToButton());
		btnStatisticsManager.addMouseListener(new addCursorChangesToButton());
		btnVpnState.addMouseListener(new addCursorChangesToButton());
		btnSaveManager.addMouseListener(new addCursorChangesToButton());
		btnSettings.addMouseListener(new addCursorChangesToButton());
		btnLogout.addMouseListener(new addCursorChangesToButton());
		
		// add repeating backgroundImage to the buttons, if defined for actual theme
		if( settlersLobby.theme.isRepeatingButtonStyleAvailable() ) {
			btnCreateGame = settlersLobby.theme.getButtonWithStyle(I18n.getString("GUI.NEW_GAME"));
			btnLeague = settlersLobby.theme.getButtonWithStyle(I18n.getString("GUI.COMPETITIONS"));
			btnStatisticsManager = settlersLobby.theme.getButtonWithStyle(I18n.getString("GUI.BUTTON_STATISTICS_MANAGER"));
			btnVpnState = settlersLobby.theme.getButtonWithStyle(btnVpnState.getText(), btnVpnState.getFont());
			btnSaveManager = settlersLobby.theme.getButtonWithStyle(I18n.getString("GUI.SAVEMANAGER"));
			btnSettings = settlersLobby.theme.getButtonWithStyle(btnSettings.getText(), btnSettings.getFont());
			btnLogout = settlersLobby.theme.getButtonWithStyle(btnLogout.getText(), btnLogout.getFont());
		}
		
		// add specific backgroundImage to the buttons, if defined for actual theme
		// -> all together for consistent view
		if( settlersLobby.theme.isButtonStyleAvailable() ) {
			btnCreateGame = settlersLobby.theme.getButtonWithStyle(I18n.getString("GUI.NEW_GAME"), settlersLobby.theme.buttonBackgroundImage1, settlersLobby.theme.buttonBackgroundImage1Hover);
			btnStatisticsManager = settlersLobby.theme.getButtonWithStyle(I18n.getString("GUI.BUTTON_STATISTICS_MANAGER"), settlersLobby.theme.buttonBackgroundImage2, settlersLobby.theme.buttonBackgroundImage2Hover);
			btnLeague = settlersLobby.theme.getButtonWithStyle(I18n.getString("GUI.COMPETITIONS"), settlersLobby.theme.buttonBackgroundImage3, settlersLobby.theme.buttonBackgroundImage3Hover);
			btnSaveManager = settlersLobby.theme.getButtonWithStyle(I18n.getString("GUI.SAVEMANAGER"), settlersLobby.theme.buttonBackgroundImage4, settlersLobby.theme.buttonBackgroundImage4Hover);
		}
		
		btnLogout.setToolTipText(I18n.getString("GUI.LOGOUT"));
		btnSettings.setToolTipText(I18n.getString("GUI.SETUP"));
		
		// add action to each button
		btnCreateGame.addActionListener(actionListenerToCreateGame);

		btnLeague.addActionListener(actionListenerToOpenLeagueWindow);

		btnStatisticsManager.addActionListener(actionListenerToStatisticsManager);

		btnVpnState.addActionListener(actionListenerVpnState);

		// After click on "Save Manager" open the Save Manager
		btnSaveManager.addActionListener(actionListenerToSaveManager);
		//the margin returned by getMargin on the left is 14,
		//leave only 2, the rest is taken by the icon
		Insets insets = btnSaveManager.getMargin();
		insets.left = 2;
		btnSaveManager.setMargin(insets);

		// Nach Klicken auf "Einstellungen", rufe
		// SettlersLobby.actionWantsToEditSettings() auf
		btnSettings.addActionListener(actionListenerToOpenSettings);

		// call logout after click on logout-button
		btnLogout.addActionListener(actionListenerToLogout);

		// set interaction with users in userlist
		listUsersOnline.addMouseListener(mouseAdapterForUserList);

		// left-click on community-news
		tableCommunityNews.addMouseListener(mouseAdapterCommunityNews);
		
        // join game on left-click
		tableOpenGames.addMouseListener(mouseAdapterToJoinGame);
        
        // show map-info-options on right-click
        tableOpenGames.addMouseListener(mouseAdapterToGetMapInfo);
        
        // change mouse on hover
        tableCommunityNews.addMouseMotionListener(mouseAdapterOnMotion);
        
        // change mouse on hover
        tableOpenGames.addMouseMotionListener(mouseAdapterOnMotion);
        
        // show map-info-options on right-click	
        tableRunningGames.addMouseListener(mouseAdapterToGetMapInfo);
        
        // change mouse on hover
        tableRunningGames.addMouseMotionListener(mouseAdapterOnMotion);
        
        // show map-info-options on right-click	
        tableStreamedGames.addMouseListener(mouseAdapterToGetStream);
        
        // change mouse on hover
        tableStreamedGames.addMouseMotionListener(mouseAdapterOnMotion);

        /***********************************************************************
         * LISTENER ANMELDEN, Fenster: MapChoosingDialog
         **********************************************************************/
		//wenn das MapChoosingFenster geschlossen wird,
		//wird das Spiel erstellen abgebrochen
		mapChoosingDialog.addWindowListener(windowListenerToAbortGame);


        /***********************************************************************
         * LISTENER ANMELDEN, Fenster: windowIngameOptionStandalone
         **********************************************************************/
        windowIngameOptions.addWindowListener(windowAdapterToCloseWindow);
        btnDefaultIngameSettings.addActionListener(
                actionListenerToSetDefaultIngameOptions);
        btnCreateGameStandalone.addActionListener(
                actionListenerToCreateGameStandalone);


		addKeyBindings();

		//ToolTip Zeiten anpassen
		ToolTipManager.sharedInstance().setInitialDelay(0);
		ToolTipManager.sharedInstance().setDismissDelay(7000);

		popupBuilder = new PopupBuilder(settlersLobby);
		
		//we need a popupMenu to delete a game entry in the tableGames manually
		tableGamesPopup = new JPopupMenu();
	    
	    // add a menu-entry which will open the preview-image of the selected map
	    // if the selected map is not a random-map or save-game
	    JMenuItem menuItemOpenPreview = new JMenuItem(I18n.getString("GUI.MAPBASE_PREVIEW"));  //$NON-NLS-1$
	    menuItemOpenPreview.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                // when the popup was activated, the selected row was also adjusted.
                int selectedRow = -1;
                OpenGame game = null;
                if( tabbedPane.getComponentAt(tabbedPane.getSelectedIndex()) == scrollPaneGames ) {
                	selectedRow = tableOpenGames.getSelectedRow();
                	game = tableOfOpenGames.getOpenGameAt(selectedRow);
                }
                if( tabbedPane.getComponentAt(tabbedPane.getSelectedIndex()) == scrollPaneRunningGames ) {
                	selectedRow = tableRunningGames.getSelectedRow();
                	game = tableOfRunningGames.getRunningGameAt(selectedRow);
                }
                if (selectedRow != -1 && game != null )
                {
                    showMapDetail = new ShowMapDetail(settlersLobby.getGUI().getMainWindow(), settlersLobby, getPlainMapName(game.getMap()), false);
                }
            }
        });
	    menuItemOpenPreview.setVisible(true);
	    tableGamesPopup.add(menuItemOpenPreview);
	    
	    // add a menu-entry which will open the mapbase-url from the chosen map in default browser
	    // if the selected map is not a random-map or save-game
	    JMenuItem menuItemOpenMapbase = new JMenuItem(I18n.getString("GUI.MAPBASE_CALL"));  //$NON-NLS-1$
	    menuItemOpenMapbase.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                // when the popup was activated, the selected row was also adjusted.
            	int selectedRow = -1;
                OpenGame game = null;
                if( tabbedPane.getComponentAt(tabbedPane.getSelectedIndex()) == scrollPaneGames ) {
                	selectedRow = tableOpenGames.getSelectedRow();
                	game = tableOfOpenGames.getOpenGameAt(selectedRow);
                }
                if( tabbedPane.getComponentAt(tabbedPane.getSelectedIndex()) == scrollPaneRunningGames ) {
                	selectedRow = tableRunningGames.getSelectedRow();
                	game = tableOfRunningGames.getRunningGameAt(selectedRow);
                }
                if (selectedRow != -1 && game != null )
                {
                    String response = "";
					
					// define the request
		        	httpRequests httpRequests = new httpRequests();
		        	// -> set parameter to configure the JSON-response
		        	httpRequests.addParameter("linkonly", "1");
		        	// -> ignore security-token
		        	httpRequests.setIgnorePhpSessId(true);
		        	// -> show a progress-monitor
		        	httpRequests.setShowProgressMonitor(true);
		        	// -> set the url
		        	String url = "";
		        	String mapname = getPlainMapName(game.getMap());
					try {
						// encode the mapname and add it to the url
						url = settlersLobby.config.getDefaultGetMapData() + URLEncoder.encode(mapname, "UTF-8");
					} catch (UnsupportedEncodingException e2) {
						e2.printStackTrace();
					}
					httpRequests.setUrl(url);
					// -> get the response-String
					try {
						response = httpRequests.doRequest();
					} catch (IOException e1) {
					}
					
					if( !httpRequests.isError() ) {
						String link = "";
						
						if( response.length() > 0 ) {
							JSONObject obj = new JSONObject(response);
							if( !obj.isNull("link") ) {
								link = obj.getString("link");
							}
							openURL(link);
						}
						else {
			            	
			                // use MessageDialog to show the hin
			     			MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
			             	messagewindow.addMessage(
			             		MessageFormat.format("<html>" + I18n.getString("GUI.MAP_DOES_NOT_EXISTS_ON_MAPBASE") + "</html>", mapname),
			             		UIManager.getIcon("OptionPane.errorIcon")
			             	);
			             	messagewindow.addButton(
			             		UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
			             		new ActionListener() {
			             		    @Override
			             		    public void actionPerformed(ActionEvent e) {
			             		    	// close this window
			        			    	messagewindow.dispose();
			             		    }
			             		}
			             	);
			             	messagewindow.setVisible(true);
						}
					}
                }
            }
        });
	    menuItemOpenMapbase.setVisible(true);
	    tableGamesPopup.add(menuItemOpenMapbase);
	    
	    // add a menu-entry which will post the mapbase-link of this map in chat
	    // if the selected map is not a random-map
	    JMenuItem menuItemPostLinkToMap = new JMenuItem(I18n.getString("GUI.MAPBASE_LINKTO"));  //$NON-NLS-1$
	    menuItemPostLinkToMap.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                // when the popup was activated, the selected row was also adjusted.
            	int selectedRow = -1;
                OpenGame game = null;
                if( tabbedPane.getComponentAt(tabbedPane.getSelectedIndex()) == scrollPaneGames ) {
                	selectedRow = tableOpenGames.getSelectedRow();
                	game = tableOfOpenGames.getOpenGameAt(selectedRow);
                }
                if( tabbedPane.getComponentAt(tabbedPane.getSelectedIndex()) == scrollPaneRunningGames ) {
                	selectedRow = tableRunningGames.getSelectedRow();
                	game = tableOfRunningGames.getRunningGameAt(selectedRow);
                }
                if (selectedRow != -1 && game != null )
                {
                    String response = "";
					
					// define the request
		        	httpRequests httpRequests = new httpRequests();
		        	// -> set parameter to configure the JSON-response
		        	httpRequests.addParameter("linkonly", "1");
		        	// -> ignore security-token
		        	httpRequests.setIgnorePhpSessId(true);
		        	// -> show a progress-monitor
		        	httpRequests.setShowProgressMonitor(true);
		        	// -> set the url
		        	String url = "";
		        	String mapname = getPlainMapName(game.getMap());
					try {
						// encode the mapname and add it to the url
						url = settlersLobby.config.getDefaultGetMapData() + URLEncoder.encode(mapname, "UTF-8");
					} catch (UnsupportedEncodingException e2) {
						e2.printStackTrace();
					}
					httpRequests.setUrl(url);
					// -> get the response-String
					try {
						response = httpRequests.doRequest();
					} catch (IOException e1) {
					}
					
					if( !httpRequests.isError() ) {
						String link = "";
						
						if( response.length() > 0 ) {
							JSONObject obj = new JSONObject(response);
							if( !obj.isNull("link") ) {
								link = obj.getString("link");
							}
							settlersLobby.actionChatMessage(link);
						}
						else {
			            	
			                // use MessageDialog to show the hint
			     			MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
			             	messagewindow.addMessage(
			             		MessageFormat.format("<html>" + I18n.getString("GUI.MAP_DOES_NOT_EXISTS_ON_MAPBASE") + "</html>", mapname),
			             		UIManager.getIcon("OptionPane.errorIcon")
			             	);
			             	messagewindow.addButton(
			             		UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
			             		new ActionListener() {
			             		    @Override
			             		    public void actionPerformed(ActionEvent e) {
			             		    	// close this window
			        			    	messagewindow.dispose();
			             		    }
			             		}
			             	);
			             	messagewindow.setVisible(true);
						}
					}
                    
                }
            }
        });
	    menuItemPostLinkToMap.setVisible(true);
	    tableGamesPopup.add(menuItemPostLinkToMap);
	    
	    // add a menu-entry to open the map in s3 to test it
	    // (only on setmap-maps)
	    JMenuItem menuItemOpenMapInS3 = new JMenuItem(I18n.getString("GUI.START_MAPTEST"));  //$NON-NLS-1$
	    menuItemOpenMapInS3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                // when the popup was activated, the selected row was also adjusted.
            	int selectedRow = -1;
                OpenGame game = null;
                if( tabbedPane.getComponentAt(tabbedPane.getSelectedIndex()) == scrollPaneGames ) {
                	selectedRow = tableOpenGames.getSelectedRow();
                	game = tableOfOpenGames.getOpenGameAt(selectedRow);
                }
                if( tabbedPane.getComponentAt(tabbedPane.getSelectedIndex()) == scrollPaneRunningGames ) {
                	selectedRow = tableRunningGames.getSelectedRow();
                	game = tableOfRunningGames.getRunningGameAt(selectedRow);
                }
                if (selectedRow != -1 && game != null )
                {
                	CheckAndDownloadMapFile mapFile = new CheckAndDownloadMapFile(settlersLobby);
        			mapFile.setMap(game.getMap());
        			if( mapFile.checkMapFile() ) {
	                	// create the game-conditions
	    				GameStarterVanilla.StartOptions startOptions = new GameStarterVanilla.StartOptions(
	    					settlersLobby.config.getUserName(),
	    					settlersLobby.config.getUserName() + " maptest",
	    					game.getMap(),
	    					GoodsInStock.getGoodsInStockByIndex(3),
	    					false,
	    					true,
	    					false,
	    					game.isWimo(),
	    					0,
	    					"",
	    					0,
	    					0,
	    					0,
	    					"",
	    					game.getMap(),
	    					1,
	    					settlersLobby.config.getS3VersionAsMiscType()
	    				);
	    				settlersLobby.actionWantsToCreateMapGame(startOptions);
        			}
                }
            }
        });
	    menuItemOpenMapInS3.setVisible(true);
	    tableGamesPopup.add(menuItemOpenMapInS3);
	    
	    // add a menu-entry which will open the league-ranking-window
	    // if the selected game is a league-game
	    // and if league-interface is active
	    if( !Configuration.getInstance().isHideLeagueInterface() ) {
		    JMenuItem menuItemOpenLeague = new JMenuItem(I18n.getString("GUI.LEAGUE_LINKTO"));  //$NON-NLS-1$
		    menuItemOpenLeague.addActionListener(new ActionListener() {
	            @Override
	            public void actionPerformed(ActionEvent e)
	            {
	                // when the popup was activated, the selected row was also adjusted.
	            	int selectedRow = -1;
	                OpenGame game = null;
	                if( tabbedPane.getComponentAt(tabbedPane.getSelectedIndex()) == scrollPaneGames ) {
	                	selectedRow = tableOpenGames.getSelectedRow();
	                	game = tableOfOpenGames.getOpenGameAt(selectedRow);
	                }
	                if( tabbedPane.getComponentAt(tabbedPane.getSelectedIndex()) == scrollPaneRunningGames ) {
	                	selectedRow = tableRunningGames.getSelectedRow();
	                	game = tableOfRunningGames.getRunningGameAt(selectedRow);
	                }
	                if (selectedRow != -1 && game != null )
	                {
	                    if( game.isLeagueGame() ) {
	                    	displayLeagueTournamentDialog();
	                    }
	                }
	            }
	        });
		    menuItemOpenLeague.setVisible(true);
		    tableGamesPopup.add(menuItemOpenLeague);
	    }

	    if (Configuration.getInstance().isHideLeagueInterface())
	    {
	        btnLeague.setVisible(false);
	    }

	    initTeamspeakWindow();

	    settlersLobby.config.addConfigurationListener(this);

	} // Konstruktor GUI

    public void displayFirstFrame() {
		firstFrame.setVisible(true);
	}

	/**
	 * Egal in welchem Zustand sich die GUI befindet, dieser wird ausgeblendet
	 * und die Anzeige "Einstellungen" wird geöffnet. Sie enthält 3
	 * Eingebafelder: "Ort der s3.exe", "URL zum IRC Server",
	 * "Zeitüberschreitung (in Sekunden)". Zusätzlich existiert ein Button
	 * "Weiter". Die genannten Eingabefelder werden mit den übergebenen
	 * Parametern gefüllt. Die Parameter können auch leer ("") sein. Wird der
	 * Button geklickt, meldet dies die GUI an
	 * SettlersLobby.actionSettings(String gamePath, String serverURL, String
	 * timeOut) mit den Werten der Eingabefelder weiter.
	 *
	 * @param gamePath
	 *            Der Pfad zur s3.exe.
	 * @param serverURL
	 *            Die URL zum IRC-Server.
	 * @param timeOut
	 *            Die Zeitüberschreitung in Sekunden.
	 */
	public void displaySettings()
	{
		optionsFrame.setVisible(true);
	} // function displaySettings

	/**
	 * Setzt das Standardicon der Lobby für den übergebenen Frame.
	 * @param frame beliebiger Frame, der das Icon der Lobby bekommen soll.
	 */
	public void setIcon(JFrame frame)
	{
		if (icon != null)
		{
			frame.setIconImage(icon);
		}
	}


	/**
	 * Egal in welchem Zustand sich die GUI befindet, dieser wird ausgeblendet
	 * und die Anzeige "Chat" wird geöffnet. Der Chat besteht aus einem Bereich,
	 * in dem offene Spiele angezeigt werden (mit Spielname, Kartenname,
	 * maximale und aktuelle Spieleranzahl im Spiel ( 3/4 )), aus einer Liste,
	 * in der alle im Chat eingeloggten Benutzernamen aufgelistet sind, einem
	 * Textfeld, wo die Chatnachrichten erscheinen, einem Eingabefeld für die
	 * eigene neue Chatnachricht, einem Button "Senden" zum Versenden der
	 * eigenen Chatnachricht, enen Button "Eigenes Spiel" zum Erzeugen eines
	 * eigenen Spiels, ein Button "Logout" zum Ausloggen und einem Button oder
	 * Menüpunkt "Einstellungen". Wird auf den Button "Senden" geklickt, meldet
	 * dies die GUI an SettlersLobby.actionChatMessage(String chatMessage) mit
	 * dem Wert des Eingabefeldes als Parameter weiter. Wird "Eigenes Spiel"
	 * geklickt, meldet dies die GUI an SettlersLobby.actionWantsToCreateGame()
	 * weiter. Wird auf ein offenes Spiel geklickt, welches nicht voll ist (4/4
	 * oder 2/2 etc.) wird dies an SettlersLobby.actionWantsToJoinGame(int
	 * gameID) mit der entsprechenden gameID des Spiels weiter gemeldet. Die
	 * gameID wird durch die Methode GUI.addGame(...) übergeben und ist dadurch
	 * bekannt. Wird der Button "Logout" geklickt, meldet dies die GUI an
	 * SettlersLobby.actionWantsToLogOut() weiter. Wird "Einstellungen"
	 * gedrückt, meldet dies die GUI an
	 * SettlersLobby.actionWantsToEditSettings() weiter.
	 *
	 * Wird der Chat durch andere displayXXX() Methoden geschlossen, sollten die
	 * angezeigten offenen Spiele, die eingeloggten User und die Chatnachrichten
	 * geleert werden, damit nicht bei einem Aufruf von displayChat() alte Daten
	 * angezeigt werden.
	 *
	 * Eventuell werden später noch Parameter eingeführt, wie eine Liste der
	 * offenen Spiele oder eine Liste der bereits im Chat angemeldeten Spieler.
	 */
	@SuppressWarnings("static-access")
	public void displayChat()
	{
		if( false != this.guiInitialized ) {
			this.removeAllEntriesFromTableModel(tableOfCommunityNews);
			this.removeAllEntriesFromTableModel(tableOfOpenGames);
			this.removeAllEntriesFromTableModel(tableOfRunningGames);
			this.removeAllEntriesFromTableModel(tableOfStreams);
			displayChatForReal();
			return;
		}
		this.guiInitialized = true;
		//remove content from previous displayChat() calls
		windowMain.getContentPane().removeAll();
		windowMain.setTitle(PROGRAM_NAME + " > " +  I18n.getString("GUI.LOGGED_IN_AS") + " " + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		        settlersLobby.getNick() + " (" + settlersLobby.getServerName() + ")"); //$NON-NLS-1$ //$NON-NLS-2$

		// DefaultTableModel for community news
		tableOfCommunityNews = new DefaultTableModelForCommunityNews();
		
		// DefaultTableModel for open games
		tableOfOpenGames = new DefaultTableModelForOpenGames();
		
		// DefaultTableModel for running games
		tableOfRunningGames = new DefaultTableModelForRunningGames();
		
		// DefaultTableModel for streams
		tableOfStreams = new DefaultTableModelForStreams();
		
		// table-cell-centerer
		TableCellCenterRenderer tableCellCenterRenderer = new TableCellCenterRenderer();

		/***********************************************************************
		 * MAIN GUI:
		 **********************************************************************/

		// Einstellungen für das Hauptfenster
		windowMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		windowMain.setSize(800, 600);
		windowMain.setBackground(settlersLobby.theme.backgroundColor);
		
		// If the theme has a background-image for chat
		// than use it here.
		if( settlersLobby.theme.chatBackgroundImage.length() > 0 ) {
			try {
				// get the image
				Image iconAway = ImageIO.read(GUI.class.getResourceAsStream(settlersLobby.theme.chatBackgroundImage));
				// and assign it to the wrapperpanel with help of RepeatingImage
				wrapperPanel = new RepeatingImage(iconAway);
			} catch (IOException e) {
				//
			}
		}
		
		// listen for changes in table for open games to update the tab-title
		tableOfOpenGames.addTableModelListener(new TableModelListener() {
	        @Override
	        public void tableChanged(TableModelEvent evt) {
	        	SwingUtilities.invokeLater(new Runnable()
	        	{
	        	    @Override
	        	    public void run()
	        	    {
	        	    	resetOpenGameCount(true);
	        	    }
	        	});
	        }
	    });
		
		// listen for changes in table for running games to update the tab-title
		tableOfRunningGames.addTableModelListener(new TableModelListener() {
	        @Override
	        public void tableChanged(TableModelEvent evt) {
	        	SwingUtilities.invokeLater(new Runnable()
	        	{
	        	    @Override
	        	    public void run()
	        	    {
        	    		resetRunningGameCount(true);
	        	    }
	        	});
	        }
	    });

		// set the layout to the wrapperPanel in order to arrange all following components
		// within this panel
		wrapperPanel.setLayout(new BorderLayout());
		
		// add the resulting wrapper to the JRootFrame
		windowMain.getContentPane().add(wrapperPanel);
		
		// creates the component to display the userlist on right side
		usersOnline = new ListModel();
        listUsersOnline.setModel(usersOnline);
		listUsersOnline.setSelectionBackground(settlersLobby.theme.selectionColor);
		listUsersOnline.setFocusable(false);
		listUsersOnline.setEnabled(false);

        ListCellRenderer listCellRenderer = new ListCellRenderer(settlersLobby);
        listUsersOnline.setCellRenderer(listCellRenderer);

        scrollPaneUsersOnline.setViewportView(listUsersOnline);
        scrollPaneUsersOnline.setBackground(settlersLobby.theme.lobbyBackgroundColor);

        // creates the MapUserList for S3-Games, which is initial not visible
        mapUserListWindow = new S3MapUserListWindow(settlersLobby);
        mapUserListWindow.setVisible(false);
        mapUserListWindow.setBackground(settlersLobby.theme.backgroundColor);
        
        // creates the Gameoptions for S4-Games, which is initial not visible
        s4gameOptions = new S4GameOptions(settlersLobby);
        s4gameOptions.setVisible(false);
        s4gameOptions.setBackground(settlersLobby.theme.backgroundColor);

        // create the ingameoptions (on bottom right), also not visible on init
        ingameOptionsWindow = new IngameOptionsWindow(this, settlersLobby);
        ingameOptionsWindow.setVisible(false);
        
        // creates the panel which contains the ingameoptions
		onDemandWindow.setVisible(false);
		onDemandWindow.removeAll();
		onDemandWindow.setBackground(settlersLobby.theme.backgroundColor);
		onDemandWindow.addMouseWheelListener(new MouseWheelListenerForJTabbedPane());

		// creates the element for topic
		topic.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
		topic.setEditable(false);
		topic.setForeground(settlersLobby.theme.topicForegroundColor);
		topic.setBackground(settlersLobby.theme.lobbyBackgroundColor);
		topic.setOpaque(settlersLobby.theme.noTransparency);
		topic.setFont(topic.getFont().deriveFont(Font.BOLD));

		// creates the table for opengames on top of chat
		tableOpenGames.setModel(tableOfOpenGames);
		tableOpenGames.setFont(tableOpenGames.getFont().deriveFont(Font.PLAIN, settlersLobby.config.getDpiAwareFontSize()));
		tableOpenGames.setRowHeight(tableOpenGames.getFont().getSize() + (int) (6 * MessageDialog.getDpiScalingFactor()));
		tableOpenGames.getTableHeader().setReorderingAllowed(false);
		tableOpenGames.setShowGrid(false);
		// define each column
		// -> game-name
		tableOpenGames.getColumnModel().getColumn(0).setPreferredWidth((int) (125 * MessageDialog.getDpiScalingFactor()));
		// -> map
		tableOpenGames.getColumnModel().getColumn(1).setPreferredWidth((int) (125 * MessageDialog.getDpiScalingFactor()));
		// -> player (actual/max)
		tableOpenGames.getColumnModel().getColumn(2).setPreferredWidth((int) (25 * MessageDialog.getDpiScalingFactor()));
		// -> with or w/o amaz
		tableOpenGames.getColumnModel().getColumn(3).setPreferredWidth((int) (10 * MessageDialog.getDpiScalingFactor()));
		// -> goods
		tableOpenGames.getColumnModel().getColumn(4).setPreferredWidth((int) (25 * MessageDialog.getDpiScalingFactor()));
		// -> used version
		tableOpenGames.getColumnModel().getColumn(5).setPreferredWidth((int) (25 * MessageDialog.getDpiScalingFactor()));
		// -> connection
		tableOpenGames.getColumnModel().getColumn(6).setPreferredWidth((int) (25 * MessageDialog.getDpiScalingFactor()));
		if( !Configuration.getInstance().isHideLeagueInterface() )
		{
			// -> competition
			tableOpenGames.getColumnModel().getColumn(7).setPreferredWidth((int) (50 * MessageDialog.getDpiScalingFactor()));
		}
		
		tableOpenGames = styleTab(tableOpenGames, tableCellCenterRenderer);
		
		// creates the table for running games on top of chat
		tableRunningGames.setModel(tableOfRunningGames);
		tableRunningGames.setFont(tableRunningGames.getFont().deriveFont(Font.PLAIN, settlersLobby.config.getDpiAwareFontSize()));
		tableRunningGames.setRowHeight(tableRunningGames.getFont().getSize() + (int) (6 * MessageDialog.getDpiScalingFactor()));
		tableRunningGames.getTableHeader().setReorderingAllowed(false);
		tableRunningGames.setShowGrid(false);
		// define each column
		// -> starttime
		tableRunningGames.getColumnModel().getColumn(0).setPreferredWidth((int) (50 * MessageDialog.getDpiScalingFactor()));
		// -> game-name
		tableRunningGames.getColumnModel().getColumn(1).setPreferredWidth((int) (125 * MessageDialog.getDpiScalingFactor()));
		// -> map
		tableRunningGames.getColumnModel().getColumn(2).setPreferredWidth((int) (125 * MessageDialog.getDpiScalingFactor()));
		// -> player (complete)
		tableRunningGames.getColumnModel().getColumn(3).setPreferredWidth((int) (10 * MessageDialog.getDpiScalingFactor()));
		// -> options
		tableRunningGames.getColumnModel().getColumn(4).setPreferredWidth((int) (25 * MessageDialog.getDpiScalingFactor()));
		if( !Configuration.getInstance().isHideLeagueInterface() )
		{
			// -> competition
			tableRunningGames.getColumnModel().getColumn(5).setPreferredWidth((int) (50 * MessageDialog.getDpiScalingFactor()));
		}
		
		tableRunningGames = styleTab(tableRunningGames, tableCellCenterRenderer);
		
		tableOfStreams.addTableModelListener(new TableModelListener() {
	        @Override
	        public void tableChanged(TableModelEvent evt) {
	        	SwingUtilities.invokeLater(new Runnable()
	        	{
	        	    @Override
	        	    public void run()
	        	    {
	        	    	resetStreamCount(true);
	        	    }
	        	});
	        }
	    });
		
		// creates the table for streams on top of chat
		tableStreamedGames.setModel(tableOfStreams);
		tableStreamedGames.setFont(tableStreamedGames.getFont().deriveFont(Font.PLAIN, settlersLobby.config.getDpiAwareFontSize()));
		tableStreamedGames.setRowHeight(tableStreamedGames.getFont().getSize() + (int) (6 * MessageDialog.getDpiScalingFactor()));
		tableStreamedGames.getTableHeader().setReorderingAllowed(false);
		tableStreamedGames.setShowGrid(false);
		// define each column
		// -> player
		tableStreamedGames.getColumnModel().getColumn(0).setPreferredWidth((int) (50 * MessageDialog.getDpiScalingFactor()));
		// -> URL
		tableStreamedGames.getColumnModel().getColumn(1).setPreferredWidth((int) (200 * MessageDialog.getDpiScalingFactor()));
		
		tableStreamedGames = styleTab(tableStreamedGames, tableCellCenterRenderer);

		// set the background-color for the table
		scrollPaneGames.setViewportView(tableOpenGames);
		scrollPaneGames.getViewport().setBackground(settlersLobby.theme.backgroundColor);
		// override uimanager-settings for tabbedpane
		tabbedPane.setFont(tabbedPane.getFont().deriveFont(Font.BOLD, settlersLobby.config.getDpiAwareFontSize()));
		tabbedPane.setUI(new BasicTabbedPaneUI() {
			// new border around whole tabbedPane
	        private final Insets borderInsets = new Insets(0, 0, 0, 0);
	        @Override
	        protected void paintContentBorder(Graphics g, int tabPlacement, int selectedIndex) {
	        }
	        @Override
	        protected Insets getContentBorderInsets(int tabPlacement) {
	            return borderInsets;
	        }
	        // new border around tabs
	        @Override
	        protected void paintTabBorder(Graphics g, int tabPlacement, int tabIndex, int x, int y, int w, int h, boolean isSelected ) {
	        	g.setColor(new Color(122,138,138));
                g.drawLine(x, y+1, x, y+h+1); // left highlight
	        	g.drawLine(x+1, y+1, x+1, y+1); // top-left highlight
	        	g.drawLine(x+1, y+1, x+w-1, y+1); // top highlight

                g.setColor(new Color(122,138,138));
                g.drawLine(x+w-2, y+1, x+w-2, y+h+1); // right shadow
	        }
	        
	    });
		addMouseWheelListenerForJTabbedPane(tabbedPane);

		// creates the table for community news on top of chat
		tableCommunityNews.setModel(tableOfCommunityNews);
		tableCommunityNews.setFont(tableCommunityNews.getFont().deriveFont(Font.PLAIN, settlersLobby.config.getDpiAwareFontSize()));
		tableCommunityNews.setRowHeight(tableCommunityNews.getFont().getSize() + (int) (6 * MessageDialog.getDpiScalingFactor()));
		tableCommunityNews.getTableHeader().setReorderingAllowed(false);
		tableCommunityNews.setShowGrid(false);
		
		// define a TableSorter for this table
		// -> all entries will be sorted by invisible prio-value for each entry
		TableRowSorter<TableModel> sorter = new TableRowSorter<>(tableOfCommunityNews);
		sorter.setComparator(tableCommunityNews.getColumn("Prio").getModelIndex(), new Comparator<String>() {
		    @Override
		    public int compare(String name1, String name2) {
		    	int int1 = Integer.valueOf(name1);
		    	int int2 = Integer.valueOf(name2);
		        return int1 - int2;
		    }
		});
		sorter.setSortable(0, false);
		sorter.setSortable(1, false);
		sorter.setSortable(2, false);
		tableCommunityNews.setRowSorter(sorter);
		List<RowSorter.SortKey> sortKeys = new ArrayList<>();
		sortKeys.add(new RowSorter.SortKey(tableCommunityNews.getColumn("Prio").getModelIndex(), SortOrder.ASCENDING));
		sorter.setSortKeys(sortKeys);
		sorter.sort();
		
		// define each column
		// -> date
		tableCommunityNews.getColumnModel().getColumn(0).setPreferredWidth((int) (120 * MessageDialog.getDpiScalingFactor()));
		tableCommunityNews.getColumnModel().getColumn(0).setMaxWidth((int) (120 * MessageDialog.getDpiScalingFactor()));
		// -> section
		tableCommunityNews.getColumnModel().getColumn(1).setPreferredWidth((int) (200 * MessageDialog.getDpiScalingFactor()));
		tableCommunityNews.getColumnModel().getColumn(1).setMaxWidth((int) (200 * MessageDialog.getDpiScalingFactor()));
		// -> title
		tableCommunityNews.getColumnModel().getColumn(2).setPreferredWidth((int) (240 * MessageDialog.getDpiScalingFactor()));
	
		// set styling
		tableCommunityNews = styleTab(tableCommunityNews, null);
		
		// remove Prio-column from columnModel, so it will only be used for sorting
		tableCommunityNews.getColumnModel().removeColumn(tableCommunityNews.getColumn("newmarker"));
		tableCommunityNews.getColumnModel().removeColumn(tableCommunityNews.getColumn("Prio"));
		
		scrollCommunityNews.setViewportView(tableCommunityNews);
		scrollCommunityNews.getViewport().setBackground(settlersLobby.theme.backgroundColor);
		tabbedPane.add(scrollCommunityNews, I18n.getString("GUI.COMMUNITY_NEWS"));
		resetCommunityNewsCount(false);
		
		// add tab for open games
		tabbedPane.add(scrollPaneGames, I18n.getString("GUI.OPEN_GAMES"));
		resetOpenGameCount(false);

		scrollPaneRunningGames.setViewportView(tableRunningGames);
		scrollPaneRunningGames.getViewport().setBackground(settlersLobby.theme.backgroundColor);
		
		// add tab for running games
		tabbedPane.add(scrollPaneRunningGames, I18n.getString("GUI.RUNNING_GAMES"));
		resetRunningGameCount(false);

		scrollPaneStreamedGames.setViewportView(tableStreamedGames);
		scrollPaneStreamedGames.getViewport().setBackground(settlersLobby.theme.backgroundColor);
		
		// add tab for streams
		tabbedPane.add(scrollPaneStreamedGames, I18n.getString("GUI.NO_STREAMED_GAMES"));
		
		// change font-style of the selected tab back to normal
		tabbedPane.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				if (e.getSource() instanceof JTabbedPane) {
					resetCommunityNewsCount(false);
                    resetOpenGameCount(false);
                    resetRunningGameCount(false);
                    resetStreamCount(false);
                    // secure the now active tab in configuration for restart
                    settlersLobby.config.setActiveMainTab(tabbedPane.getSelectedIndex());
                }
			}
		});
		
		// set the active tab for the main tab-navigation
		int activeMainTab = 0;
		if( settlersLobby.config.getActiveMainTab() >= 0 && tabbedPane.getTabCount() > settlersLobby.config.getActiveMainTab() ) {
			activeMainTab = settlersLobby.config.getActiveMainTab();
			this.tabbedPanePreviousTab = settlersLobby.config.getActiveMainTab();
		}
		tabbedPane.setSelectedIndex(activeMainTab);
		
		// create a field to display the actual ip-address
		lblIp.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
		lblIp.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
		lblIp.setEditable(false);
		lblIp.setForeground(settlersLobby.theme.topicForegroundColor);
		lblIp.setBackground(new Color(0,0,0,0));
		lblIp.setOpaque(false);
		lblIp.setFont(topic.getFont().deriveFont(Font.BOLD));
		lblIp.setSelectionEnd(0);
		lblIp.addActionListener(actionListenerCopyIp);
		
		// create a field for Logfile-access
		JLabel lblDebug = new JLabel();
		lblDebug.addMouseListener(new MouseAdapter()  
		{  
		    @Override
            public void mouseClicked(MouseEvent e)  
		    {
		    	// show bugtracker-window on click
	    		Bugtracker bugTrackerWindow = new Bugtracker(settlersLobby, "Bugtracker");
	    		bugTrackerWindow.setVisible(true);
		    }
		    @Override
	        public void mouseEntered(MouseEvent e) {
		    	lblDebug.setForeground(settlersLobby.theme.highlightColor);
	        }

	        @Override
	        public void mouseExited(MouseEvent e) {
	        	lblDebug.setForeground(settlersLobby.theme.topicForegroundColor);
	        }
		});
		lblDebug.setForeground(settlersLobby.theme.topicForegroundColor);
		lblDebug.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		lblDebug.setText("\uf188"); //$NON-NLS-1$ //$NON-NLS-2$
		lblDebug.setFont(this.settlersLobby.config.getFontAweSome((float) (14 * MessageDialog.getDpiScalingFactor())));
		lblDebug.setCursor(new Cursor(Cursor.HAND_CURSOR));
		lblDebug.setOpaque(false);
		lblDebug.setToolTipText(I18n.getString("GUI.LOG_FILES"));
		lblDebug.setBorder(new EmptyBorder(1,1,1,1));
		
		// create wrapper for lblIp and lblDebug
		JPanel pnlIpDebug = new JPanel();
		pnlIpDebug.setLayout(new FlowLayout());
		pnlIpDebug.setOpaque(false);
		//pnlIpDebug.setBackground(new Color(0,0,0,0));
		// add both components to this panel
		pnlIpDebug.add(lblIp);
		pnlIpDebug.add(lblDebug);
		
		// create the layout based on the above defined components
		// -> first: create the east layout with userlist and gameoptions
		javax.swing.GroupLayout pnlEastLayout = new javax.swing.GroupLayout(
				pnlEast);
		pnlEast.setLayout(pnlEastLayout);
		if( settlersLobby.theme.lobbyBackgroundColor.toString().length() > 0 ) {
			pnlEast.setBackground(settlersLobby.theme.lobbyBackgroundColor);
		}
		pnlEastLayout
				.setHorizontalGroup(pnlEastLayout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								javax.swing.GroupLayout.Alignment.TRAILING,
								pnlEastLayout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												pnlEastLayout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.TRAILING)
														.addComponent(
																pnlIpDebug,
																javax.swing.GroupLayout.Alignment.LEADING,
																(int) (24 * MessageDialog.getDpiScalingFactor()),
																(int) (24 * MessageDialog.getDpiScalingFactor()),
																(int) (200 * MessageDialog.getDpiScalingFactor())
														)
                                                        .addComponent(
                                                                onDemandWindow,
                                                                javax.swing.GroupLayout.Alignment.LEADING,
                                                                (int) (200 * MessageDialog.getDpiScalingFactor()),
                                                                (int) (200 * MessageDialog.getDpiScalingFactor()),
                                                                Short.MAX_VALUE)
														.addComponent(
																scrollPaneUsersOnline,
																javax.swing.GroupLayout.Alignment.LEADING,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																(int) (200 * MessageDialog.getDpiScalingFactor()),
																Short.MAX_VALUE)
														.addGroup(
																pnlEastLayout
																		.createSequentialGroup()
																		.addComponent(
																				btnVpnState,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				Short.MAX_VALUE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				btnSettings,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				Short.MAX_VALUE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				btnLogout,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                Short.MAX_VALUE)
														)
										)
										.addContainerGap()));
		pnlEastLayout
				.setVerticalGroup(pnlEastLayout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								pnlEastLayout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												pnlEastLayout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(btnVpnState)
                                                        .addComponent(btnSettings)
														.addComponent(btnLogout))
                                        .addGap((int) (20 * MessageDialog.getDpiScalingFactor()))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(
												scrollPaneUsersOnline,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												(int) (307 * MessageDialog.getDpiScalingFactor()),
												Short.MAX_VALUE)
										.addContainerGap()
                                        .addPreferredGap(
                                                javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(
                                                onDemandWindow,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(
                                        		pnlIpDebug,
                                        		(int) (24 * MessageDialog.getDpiScalingFactor()),
                                        		(int) (24 * MessageDialog.getDpiScalingFactor()), 
                                        		(int) (24 * MessageDialog.getDpiScalingFactor())
										)
                                        .addGap((int) (10 * MessageDialog.getDpiScalingFactor()))
                                        .addContainerGap()));
		//windowMain.getContentPane().add(pnlEast, java.awt.BorderLayout.EAST);
		// set component as transparency
		pnlEast.setOpaque(settlersLobby.theme.noTransparency);
		// finally add east panel to wrapperpanel
		wrapperPanel.add(pnlEast, java.awt.BorderLayout.EAST);
		
		JPanel tabbedPaneWrapper = new JPanel();
		tabbedPaneWrapper.setLayout( new OverlayLayout(tabbedPaneWrapper) );
		tabbedPaneWrapper.setOpaque(false);
		tabbedPane.setAlignmentX(1.0f);
		tabbedPane.setAlignmentY(0.0f);
		
		// create a panel for the linklist on the bottom
        String hexcolor = SettlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor());
        JPanel pnlLinkList = new JPanel();
        pnlLinkList.setLayout(new FlowLayout(FlowLayout.RIGHT, (int) (5 * MessageDialog.getDpiScalingFactor()), 0) );
        
        // -> Game-Tips
        LinkLabel lnkTips = new LinkLabel();
        lnkTips.setBorder(new EmptyBorder(0,0,0,0));
        lnkTips.setText(MessageFormat.format("<html>" + I18n.getString("GUI.TIPS") + "</html>", hexcolor, settlersLobby.config.getFontSize()));
        lnkTips.setFont(lnkTips.getFont().deriveFont(Font.BOLD));
        pnlLinkList.add(lnkTips);

        // -> FAQ/Help
        LinkLabel lnkFAQ = new LinkLabel();
        lnkFAQ.setBorder(new EmptyBorder(0,8,0,0));
        lnkFAQ.setText(MessageFormat.format("<html>" + I18n.getString("GUI.FAQ") + "</html>", hexcolor, settlersLobby.config.getFontSize()));
        lnkFAQ.setFont(lnkFAQ.getFont().deriveFont(Font.BOLD));
        pnlLinkList.add(lnkFAQ);
        
        // -> Forum
        LinkLabel lnkForum = new LinkLabel();
        lnkForum.setBorder(new EmptyBorder(0,8,0,0));
        lnkForum.setText(MessageFormat.format("<html>" + I18n.getString("GUI.FORUM") + "</html>", hexcolor, settlersLobby.config.getFontSize()));
        lnkForum.setFont(lnkForum.getFont().deriveFont(Font.BOLD));
        pnlLinkList.add(lnkForum);
        
        // -> Adventscalendar
        /*if( settlersLobby.config.itsChristmastime() ) {
	        LinkLabel lnkAdventsCalendar = new LinkLabel();
	        lnkAdventsCalendar.setBorder(new EmptyBorder(0,0,0,0));
	        lnkAdventsCalendar.setText(MessageFormat.format("<html><strong><u>" + I18n.getString("GUI.TITLE_ADVENTSCALENDAR") + "</u></strong></html>", hexcolor));
	        lnkAdventsCalendar.setFont(lnkAdventsCalendar.getFont().deriveFont(Font.BOLD));
	        lnkAdventsCalendar.addMouseListener(new java.awt.event.MouseAdapter() {
	        	@Override
				public void mouseClicked(MouseEvent e) {
	        		settlersLobby.getAdventCalendar(true);
	        	}
	        	// if mouse enters the button change the cursor
	            public void mouseEntered(java.awt.event.MouseEvent evt) {
	            	// set the cursor to a hand-cursor
	            	lnkAdventsCalendar.setCursor(new Cursor(Cursor.HAND_CURSOR));
	            }

	            public void mouseExited(java.awt.event.MouseEvent evt) {
	            	// set the cursor back to standard-cursor
	            	lnkAdventsCalendar.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
	            }
	        });
	        pnlLinkList.add(lnkAdventsCalendar);
        }*/
        
        // -> League (only if league-interface is active)
        if( false == Configuration.getInstance().isHideLeagueInterface() ) {
	        LinkLabel lnkLeague = new LinkLabel();
	        lnkLeague.setBorder(new EmptyBorder(0,8,0,0));
	        lnkLeague.setText(MessageFormat.format("<html><a href=''" + settlersLobby.config.getLeagueUrl() + "'' style=''color:{0};font-size:{1}px;''>" + I18n.getString("GUI.LEAGUE") + "</a></html>", hexcolor, settlersLobby.config.getFontSize()));
	        lnkLeague.setFont(lnkLeague.getFont().deriveFont(Font.BOLD));
	        pnlLinkList.add(lnkLeague);
	    }
        
        // more settings for this panel
        pnlLinkList.setOpaque(false);
        pnlLinkList.setAlignmentX(1.0f);
        pnlLinkList.setAlignmentY(0.0f);
        pnlLinkList.setMaximumSize( pnlLinkList.getPreferredSize() );
		
		tabbedPaneWrapper.add(pnlLinkList);
		tabbedPaneWrapper.add(tabbedPane);
		
		// create the second layout, this one arranges the chat himself including gamelist
		javax.swing.GroupLayout pnlSouthLayout = new javax.swing.GroupLayout(
				pnlSouth);
		pnlSouth.setLayout(pnlSouthLayout);
		if( settlersLobby.theme.lobbyBackgroundColor.toString().length() > 0 ) {
			pnlSouth.setBackground(settlersLobby.theme.lobbyBackgroundColor);
		}
		pnlSouthLayout
				.setHorizontalGroup(pnlSouthLayout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(
                                javax.swing.GroupLayout.Alignment.TRAILING,
                                pnlSouthLayout
                                        .createSequentialGroup()
                                        .addGroup(
                                                pnlSouthLayout
                                                    .createParallelGroup()
                                                    .addComponent(
                                                    		tabbedPaneWrapper,
                                                            javax.swing.GroupLayout.DEFAULT_SIZE,
                                                            javax.swing.GroupLayout.PREFERRED_SIZE,
                                                            Short.MAX_VALUE)
                                                    .addComponent(
                                                            topic,
                                                            javax.swing.GroupLayout.DEFAULT_SIZE,
                                                            javax.swing.GroupLayout.PREFERRED_SIZE,
                                                            Short.MAX_VALUE)
                                                    .addGroup(
                                                            pnlSouthLayout
                                                                    .createSequentialGroup()
                                                                    .addComponent(
                                                                            btnCreateGame,
                                                                            javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                            javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                            Short.MAX_VALUE)
			                                                        .addPreferredGap(
			                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                    .addComponent(
                                                                            btnSaveManager,
                                                                            javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                            javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                            Short.MAX_VALUE)
			                                                        .addPreferredGap(
			                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                    .addComponent(
                                                                            btnLeague,
                                                                            javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                            javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                            Short.MAX_VALUE)
			                                                        .addPreferredGap(
			                                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                    .addComponent(
                                                                    		btnStatisticsManager,
                                                                            javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                            javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                            Short.MAX_VALUE)
                                                    )
                                                	.addComponent(chatPane.getPanel()))
                    	.addContainerGap()));
		pnlSouthLayout
				.setVerticalGroup(pnlSouthLayout
							.createParallelGroup(
									javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(
                                    pnlSouthLayout
                                            .createSequentialGroup()
                                            .addContainerGap()
											.addGroup(
													pnlSouthLayout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
																.addComponent(btnCreateGame)
																.addComponent(btnSaveManager)
																.addComponent(btnLeague)
																.addComponent(btnStatisticsManager)
															)
                                            .addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(
                                            		tabbedPaneWrapper,
                                                    javax.swing.GroupLayout.PREFERRED_SIZE,
                                                    (int) (160 * MessageDialog.getDpiScalingFactor()),
                                                    javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(
                                                    topic,
                                                    javax.swing.GroupLayout.DEFAULT_SIZE,
                                                    javax.swing.GroupLayout.PREFERRED_SIZE,
                                                    javax.swing.GroupLayout.PREFERRED_SIZE)
											.addComponent(chatPane.getPanel())
											.addContainerGap()));
		//windowMain.getContentPane().add(pnlSouth, java.awt.BorderLayout.CENTER);
		// set it to transparency
		pnlSouth.setOpaque(settlersLobby.theme.noTransparency);
		// finally add the panel to the wrapperpanel
		wrapperPanel.add(pnlSouth, java.awt.BorderLayout.CENTER);
		
		// create the panel on the left side (teamspeak), which is optionally
        GroupLayout pnlWestLayout = new GroupLayout(pnlWest);
        pnlWest.setLayout(pnlWestLayout);
        if( settlersLobby.theme.lobbyBackgroundColor.toString().length() > 0 ) {
        	pnlWest.setBackground(settlersLobby.theme.lobbyBackgroundColor);
        }
        //from left to right
        pnlWestLayout.setHorizontalGroup(
                        pnlWestLayout.createSequentialGroup()
                           .addContainerGap()
                           .addComponent(teamSpeakPanel.getPanel(),
                                         0,
                                         (int) (220 * MessageDialog.getDpiScalingFactor()),
                                         javax.swing.GroupLayout.PREFERRED_SIZE)
                           .addContainerGap());

        //from top to bottom
        pnlWestLayout.setVerticalGroup(
                        pnlWestLayout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(teamSpeakPanel.getPanel())
                            .addContainerGap()
                        );
        // set panel to transparency
        pnlWest.setOpaque(settlersLobby.theme.noTransparency);
        
        // create an additional panel for the left side which will be used 
        // if the teamspeak-panel on this place is disabled
        // -> to generate an additional gap between window and chat-panel
        GroupLayout pnlWestSpaceLayout = new GroupLayout(pnlWestSpace);
        pnlWestSpace.setLayout(pnlWestSpaceLayout);
        if( settlersLobby.theme.lobbyBackgroundColor.toString().length() > 0 ) {
        	pnlWestSpace.setBackground(settlersLobby.theme.lobbyBackgroundColor);
        }
        //from left to right
        pnlWestSpaceLayout.setHorizontalGroup(pnlWestSpaceLayout.createSequentialGroup().addGap(10));
        //from top to bottom
        pnlWestSpaceLayout.setVerticalGroup(pnlWestSpaceLayout.createSequentialGroup().addGap(10));
        // set panel to transparency
        pnlWestSpace.setOpaque(settlersLobby.theme.noTransparency);

        displayChatForReal();
        
	} // displayChat

	/**
	 * Remove all entries from given tableModel.
	 * 
	 * @param tableModel
	 */
	private void removeAllEntriesFromTableModel(DefaultTableModel tableModel) {
		if( tableModel != null ) {
			if (tableModel.getRowCount() > 0) {
				for (int i = tableModel.getRowCount() - 1; i > -1; i--) {
					tableModel.removeRow(i);
				}
			}
		}
	}

	/**
	 * Really show the Main-Windows.
	 */
	@SuppressWarnings("static-access")
	private void displayChatForReal() {
		// show or hide this panel depending on user-settings
	    updateTeamspeakWindowVisibility();
	    
	    // arrange the window-position self depending on screen which is used.
		Rectangle location = settlersLobby.config.getWindowMainPosition();
		if (location != null)
		{
			// get the environment
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			
			// get the screen devices
		    GraphicsDevice[] gd = ge.getScreenDevices();

		    // search for the given screen in list of devices
		    for( int i=0;i<gd.length;i++ )
		    {
		    	if( gd[i].getIDstring().equals(settlersLobby.config.getScreen()) ) {
		    		if( location.x > gd[i].getDefaultConfiguration().getBounds().width ) {
		    			location.x = location.x - gd[i].getDefaultConfiguration().getBounds().width;
		    		}
		    		if( location.x < 0 ) {
		    			location.x = gd[i].getDefaultConfiguration().getBounds().width + location.x;
		    		}
		    	}
			}
		    windowMain.setBounds(location);
		    if (settlersLobby.config.isWindowMainMaximised())
		    {
		        windowMain.setExtendedState(windowMain.getExtendedState() | Frame.MAXIMIZED_BOTH);
		    }
		}
		else
		{
			// Fenster zentrieren
		    windowMain.setLocation(getCenterLocationForWindow(windowMain));
		    windowMain.setExtendedState(windowMain.getExtendedState() | Frame.MAXIMIZED_BOTH);
		}
		
		// prepare chat
        chatPane.preDisplayChat();
        
        // show main window on specific screen
	    this.settlersLobby.showOnSpecifiedScreen(settlersLobby.config.getScreen(), windowMain);

        // activate the window
		setNewCurrentActiveFrame(windowMain);
	}
	
	/**
	 * Adds a MouseWheelListener by first removing all other from the given JTabbedPane
	 * than add the new Listener.
	 * 
	 * @param myTabbedPane
	 */
	private void addMouseWheelListenerForJTabbedPane(JTabbedPane myTabbedPane) {
		MouseWheelListenerForJTabbedPane mouseWheelListener = new MouseWheelListenerForJTabbedPane();
		for( MouseWheelListener listener : myTabbedPane.getMouseWheelListeners() ) {
			myTabbedPane.removeMouseWheelListener(listener);
		}
		myTabbedPane.addMouseWheelListener(mouseWheelListener);
	}

	/**
	 * Reset the number of active streams in tab.
	 */
	protected synchronized void resetStreamCount( boolean changed ) {
		int compontentIndex = tabbedPane.indexOfComponent(scrollPaneStreamedGames);
		if( compontentIndex > -1 ) {
			int streamcount = tableOfStreams.getRowCount();
			boolean italic = false;
			if( tabbedPane.getSelectedIndex() != compontentIndex && changed ) {
				italic = true;
			}
			String tabTitleText = "";
			if( streamcount == 0 ) {
				tabTitleText = I18n.getString("GUI.NO_STREAMED_GAMES");
			}
			else if( streamcount == 1 ) {
				tabTitleText = MessageFormat.format(I18n.getString("GUI.STREAMED_GAME"), streamcount);
				if( italic ) {
					tabTitleText = "<html><i>" + tabTitleText + "</i></html>";
				}
			}
			else {
				tabTitleText = streamcount + " " + MessageFormat.format(I18n.getString("GUI.STREAMED_GAMES"), streamcount);
				if( italic ) {
					tabTitleText = "<html><i>" + tabTitleText + "</i></html>";
				}
			}
			tabbedPane.setTitleAt(compontentIndex, tabTitleText);
			if( tabbedPane.getSelectedIndex() == compontentIndex ) {
				this.tabbedPanePreviousTab = compontentIndex;
			}
		}
	}

	/**
	 * Format a GUI-table.
	 * 
	 * @param table
	 * @param renderer
	 */
	private JTable styleTab(JTable table, TableCellCenterRenderer renderer) {
		
		if( renderer instanceof TableCellCenterRenderer ) {
			for (int i = 1; i < table.getColumnModel().getColumnCount(); ++i)
			{
				table.getColumnModel().getColumn(i).setCellRenderer(renderer);
			}
		}
		
		// add theme-specific properties
		if (settlersLobby.config.isClassicV1Theme())
		{
		    //classic V1 theme keeps the default table header renderer
		}
		else
		{
		    //for the new themes the table header should look identical to the cells
			table.getTableHeader().setDefaultRenderer(renderer);
		    // add border-bottom to table-header
			table.getTableHeader().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, settlersLobby.theme.lobbyBackgroundColor));
		    // add background-Color which will only visible behind the tableheader if active style use it
			// and align the header-cells
	    	// -> define the cellRenderer for the first row
	    	DefaultTableCellRenderer headerRendererLeft = new DefaultTableCellRenderer();
	    	if( settlersLobby.theme.tableheadercolor != null && settlersLobby.theme.tableheadercolor.toString().length() > 0 ) {
	    		headerRendererLeft.setBackground(settlersLobby.theme.tableheadercolor);
	    	}
	    	headerRendererLeft.setHorizontalAlignment( JLabel.LEFT );
	    	table.getColumnModel().getColumn(0).setHeaderRenderer(headerRendererLeft);
	    	// -> define the cellRenderer for all other rows
	    	DefaultTableCellRenderer headerRenderer = new DefaultTableCellRenderer();
	    	if( settlersLobby.theme.tableheadercolor != null && settlersLobby.theme.tableheadercolor.toString().length() > 0 ) {
	    		headerRenderer.setBackground(settlersLobby.theme.tableheadercolor);
	    	}
	    	headerRenderer.setHorizontalAlignment( JLabel.CENTER );		    	
	    	// -> set it to each column except the first
	    	for (int i = 1; i < table.getModel().getColumnCount(); i++) {
	    		table.getColumnModel().getColumn(i).setHeaderRenderer(headerRenderer);
		    }
		}
		return table;
	}

	/*
	 * Show or hides the Teamspeak-Panel on the left side
	 */
    private void updateTeamspeakWindowVisibility()
    {
    	
    	// first: remove all panels on left side
    	wrapperPanel.remove(pnlWest);
    	wrapperPanel.remove(pnlWestSpace);
    	
    	// depending on configuration show or hide the panels for left side
        if (settlersLobby.config.isHideTeamspeakWindow())
	    {
        	// stop showing the teamspeak-panel
            teamSpeakPanel.stopLoading();
            // show the space-panel with its additional gap
            wrapperPanel.add(pnlWestSpace, java.awt.BorderLayout.WEST);
	    }
        else
        {
        	// show the teamspeak-panel
        	wrapperPanel.add(pnlWest, java.awt.BorderLayout.WEST);
        	// show the url in this panel
        	teamSpeakPanel.startLoading();
	    }
        // update the window
        windowMain.validate();
    }

	/**
	 * Egal in welchem Zustand sich die GUI befindet, diese Methode startet ein
	 * Popup mit der anzeige der als parameter übergebenen Fehlernachricht und
	 * einem Button "OK". Sie hält den Programmablauf so lange an, bis der
	 * Benutzer den Button gedrückt hat.
	 *
	 * @param message
	 *            Die Fehlernachricht
	 */
	public void displayError(String message)
	{
		// generate messagedialog-window
    	MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
    	// -> add message
		messagewindow.addMessage(
			message, 
			UIManager.getIcon("OptionPane.errorIcon")
		);
		// -> add ok-button
		messagewindow.addButton(
			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
			new ActionListener() {
			    @Override
			    public void actionPerformed(ActionEvent e) {
			    	messagewindow.dispose();
			    }
			}
		);
		// show it
		messagewindow.setVisible(true);
	}

	/**
	 * wie {@link #displayError(String)}, nur wird zusätzlich noch ein
	 * Button angezeigt, um S3 zu beenden.
	 *
	 * @param message
	 * @param s3Starter aktiver S3 Prozess
	 */
	public void displayErrorTerminateS3(String message, S3Starter s3Starter)
	{
	    new ErrorDialog(message, this, false, s3Starter, settlersLobby);
	}

	/**
	 * siehe {@link #displayError(String)}
	 * @param message
	 * @param reformatMessage gibt an, ob die übergebene Nachricht entsprechend
	 * der vorgegebenen Fensterbreite formatiert werden soll.
	 *
	 */
	public void displayError(String message, boolean reformatMessage)
	{
	    new ErrorDialog(message, this, reformatMessage, settlersLobby);
	}

	/**
	 * Ein neues offenes Spiel wird mit den entsprechenden übergebenen
	 * Parametern in dem Bereich für offene Spiele auf der Oberfläche angezeigt.
	 * Die gameID ist für das schließen des Spiels und verändern der maximalen
	 * und momentanen Spieleranzahl wichtig.
	 *
	 * @param gameID
	 *            Die ID für das offene Spiel.
	 * @param gameName
	 *            Der name des offenen Spiels.
	 * @param mapName
	 *            Der Name der Karte des offenen Spiels.
	 * @param currentPlayerNumber
	 *            Die momentane Spieleranzahl.
	 * @param maxPlayerNumber
	 *            Die maximale Spieleranzahl.
	 * @param ama
	 *            true, wenn mit Amazonen gespielt wird, false, sonst
	 */
	public void addGame(OpenGame newOpenGame)
	{
		tableOfOpenGames.addOpenGame(newOpenGame);
	}
	
	/**
	 * Add new running game to list of running games.
	 */
	public void addRunningGame(OpenGame newOpenGame)
	{
		tableOfRunningGames.addRunningGame(newOpenGame);
	}

	/**
	 * schreibt die User, die man vom IRC-Server enthält, in die Liste
	 *
	 * @param users
	 */
	@SuppressWarnings("static-access")
	public void setUserList(IrcUser[] users)
	{
	    if (usersOnline == null)
	    {
	        return;
	    }
	    
	    int oldUsersOnlineSize = usersOnline.getSize();

	    //das ListModel wird komplett neu angelegt
	    //andernfalls gab es immer Probleme mit sporadischen
	    //Exceptions im AWT-Thread
	    //es wird erst komplett gefüllt, bevor es dann zum erstenmal
	    //angezeigt wird
	    //das model braucht eine read-only List, daher wird
	    //zuerst eine lokal Liste erzeugt, die dann ans Model
	    //übergeben wird und nur noch vom Model benutzt wird
	    List<S3User> userList = new ArrayList<S3User>(users.length);
		for (int i = 0; i < users.length; ++i)
		{
		    IrcUser user = users[i];
			S3User userToAdd = new S3User(
			        user.getNick(), 
			        user.isOp(),
			        user.isAway(), 
			        user.getAwayMsg(),
			        settlersLobby.config.isIgnoredUser(user.getNick()),
			        user.isBuddy(),
			        user.hasVpnActive());
			if( settlersLobby.isBetaUsed() ) {
			    // mark the active user as beta-user if he has a beta-version of the aLobby
		        if( user.getNick().equalsIgnoreCase(settlersLobby.getNick()) ) {
			        userToAdd.setBetaUser(true);
    			}
    			else {
    				// check if this user from irc-list exists in original-list
    				// and if yes check if he has been marked as beta-user
    				// and if this is also true mark the user again as beta-user
    				S3User originalS3User = usersOnline.getElementByS3UserName(user.getNick());
    				if( originalS3User != null ) {
    					userToAdd.setBetaUser(originalS3User.isBetaUser());
    				}
    			}
			}
			userList.add(userToAdd);
		}
		usersOnline = new ListModel(userList);
        listUsersOnline.setModel(usersOnline);
        //durch das neusetzen des Models ist kein manuelles aktualisieren
        //der GUI nötig, das passiert automatisch
        //dies scheint auch dabei zu helfen die sporadischen Exceptions zu vermeiden
        if( usersOnline.getSize() > oldUsersOnlineSize && usersOnline.getSize() > 1 ) {
        	settlersLobby.queryForActiveStreams();
        }
	}

    public void setMapUserList(UserInGameList userList)
    {
        this.mapUserListWindow.displayUsers(userList);
        if( settlersLobby.getGameState().isHost() ) {
        	this.mapUserListWindow.addButton();
        }
        makeComponentVisible(mapUserListWindow, I18n.getString("GUI.PLAYERS"), mapUserListWindow.isVisible());
    }
    
    /**
     * Set the S4-Game-Options visible on right bottom side
     * if users are in list.
     * 
     * @param UserInGameList
     * @param isHost
     * @throws Throwable 
     */
    public void setS4GameOptions( UserInGameList userList, boolean isHost, boolean visible ) {
    	Test.output("setS4GameOptions used!");
    	this.s4gameOptions.displayUsers(userList);
        if( false != isHost ) {
        	this.s4gameOptions.addHostButtons();
        }
        else {
        	this.s4gameOptions.addPlayerButtons();
        }
    	makeComponentVisible(s4gameOptions, I18n.getString("GUI.PLAYERS"), s4gameOptions.isVisible());
    }
    
    public void removeButtonFromMapUserListWindow() {
    	if( this.mapUserListWindow instanceof S3MapUserListWindow ) {
    		this.mapUserListWindow.removeButton();
    	}
    }

    public void displayIngameOptions(boolean display)
    {
    	if( settlersLobby.config.getS3Versionnumber() >= 300 ) {
    		display = false;
    	}
        this.ingameOptionsWindow.display(display);
        makeComponentVisible(ingameOptionsWindow, I18n.getString("GUI.GAME_OPTIONS"), display);
    }

    /**
     * Zeigt die Spiele Optionen für Siedler 3 als Standalone Variante an.
     * Von diesem Fenster kommt man nicht wieder zurück, die einzige Möglichkeit
     * ist das Schliessen des Fensters und damit der Anwendung.
     */
    public void displayIngameOptionsStandalone()
    {
        //In der standalone Variante kann nicht erkannt werden,
        //ob S3 schon läuft bzw. wann ein neues Spiel gestartet wird.
        //Die Tasks werden daher sofort aktiviert, sobald man auf
        //die jeweilige Checkbox klickt
        //Ferner gibt es noch zusätzlich einen "Start" Button,
        //der die Tasks entsprechend der Einstellung nochmals neu
        //scheduled. Dies ist notwendig, um den stopStoringTools
        //Task für ein weiteres Spiel zu reaktivieren und um
        //das Autosave zu einem definierten Zeitpunkt erneut
        //zu starten.
    	
    	// set icon for window
        setIcon(windowIngameOptions);

        // set minimum dimension for window
        // so the window will not be smaller than this values
        windowIngameOptions.setMinimumSize(new Dimension((int) (400 * MessageDialog.getDpiScalingFactor()), (int) (280 * MessageDialog.getDpiScalingFactor())));
        windowIngameOptions.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        windowIngameOptions.getContentPane().setBackground(settlersLobby.theme.startBackgroundColor);
        windowIngameOptions.setResizable(false);
        windowIngameOptions.setBackground(settlersLobby.theme.startBackgroundColor);
        windowIngameOptions.setForeground(settlersLobby.theme.startForegroundColor);
        windowIngameOptions.isOpaque();

        // generate window-object with the ingameoptions
        // and set all necessary color-configurations
        pnlIngameOptions = new IngameOptionsWindow(this, settlersLobby);
        // -> transparency to true with following background-color
        //    hint: without this combination either the image would not be painted complete 
        // 		    or the checkboxes in this panel will be overlapping themself
        pnlIngameOptions.setOpaque(true);
        pnlIngameOptions.setBackground(new Color(0,0,0,0));
        // -> font-color
        pnlIngameOptions.setForeground(settlersLobby.theme.startForegroundColor);
        // -> border with title
        pnlIngameOptions.setBorder(
        	javax.swing.BorderFactory.createTitledBorder(
                BorderFactory.createEmptyBorder((int) (10 * MessageDialog.getDpiScalingFactor()),0,0,0),
                " " + I18n.getString("GUI.GAME_OPTIONS") + " ", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
                javax.swing.border.TitledBorder.DEFAULT_POSITION,
                new java.awt.Font("Trebuchet MS italic", 1, (int) (17 * MessageDialog.getDpiScalingFactor())),
                settlersLobby.theme.startForegroundColor
        	)
        ); //$NON-NLS-1$
        pnlIngameOptions.display(true);

        // create component for background-image including all necessary color-configurations
        JPanel panelWindowIngameOptions = new ImagePanel(backgroundImage);
        panelWindowIngameOptions.setBackground(settlersLobby.theme.backgroundColor);
        panelWindowIngameOptions.setForeground(settlersLobby.theme.getTextColor());

        // set button-colors to appropriated values
        btnCreateGameStandalone.setBackground(settlersLobby.theme.backgroundColor);
        btnCreateGameStandalone.setForeground(settlersLobby.theme.getTextColor());
        btnDefaultIngameSettings.setBackground(settlersLobby.theme.backgroundColor);
        btnDefaultIngameSettings.setForeground(settlersLobby.theme.getTextColor());

        // create layout to display in this window
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(panelWindowIngameOptions);
        panelWindowIngameOptions.setLayout(layout);
        
        // define horizontal order of window-elements
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(pnlIngameOptions, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(btnCreateGameStandalone)
                                .addGap(0, (int) (50 * MessageDialog.getDpiScalingFactor()), Short.MAX_VALUE)
                                .addComponent(btnDefaultIngameSettings)
                                .addContainerGap()
                                )
                )
                .addContainerGap()
            )
        );
        
        // define vertical order of window-elements        
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(pnlIngameOptions, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnCreateGameStandalone)
                    .addComponent(btnDefaultIngameSettings)
                )
                .addContainerGap()
            )
        );
        
        // add the created layout to window-object
        windowIngameOptions.add(panelWindowIngameOptions);
        
        // minimize window so that all components are visible
        windowIngameOptions.pack();

        // display window in relation to its parent
        windowIngameOptions.setLocationRelativeTo(windowMain);
        
        // set window active
        setNewCurrentActiveFrame(windowIngameOptions);
    }

	public void displayMapChoosingDialog()
	{
		if (mapChoosingDialog == null)
		{
			mapChoosingDialog = this.createOpenNewGameDialog();
		}
		mapChoosingDialog.display();
	}
	
	public void displayLeagueTournamentDialog()
	{
		if (leagueTournamentDialog == null)
		{
			leagueTournamentDialog = this.createLeagueTournamentDialog();
		}
		leagueTournamentDialog.display();
	}

	public String getChoosedMap()
	{
		if (mapChoosingDialog == null)
		{
			mapChoosingDialog = this.createOpenNewGameDialog();
		}
		return mapChoosingDialog.getMapPath();
	}

	/**
	 * Show simple wait-window with individual label.
	 * 
	 * @param label
	 */
	@SuppressWarnings("static-access")
	public void displayWait(String label)
	{
		// close existing wait-window
        closeWait();

	    // create new one and style it
		waitDialog = new WaitDialog(this.settlersLobby, currentActiveFrame, I18n.getString("GUI.PLEASE_WAIT"), false, label); //$NON-NLS-1$
		this.settlersLobby.showOnSpecifiedScreen(settlersLobby.config.getScreen(), waitDialog);
		waitDialog.setVisible(true);
	}

	/**
	 * Close a wait-window if one is open
	 */
	public void closeWait()
	{
		if (waitDialog != null)
		{
			waitDialog.setVisible(false);
			waitDialog.dispose();
			waitDialog = null;
		}
	}

	/**
	 * Set the topic shown on top of the chat.
	 * 
	 * @param channel
	 * @param topic
	 */
    public void setTopic(String channel, String topic)
    {
        if (settlersLobby.isConnectedWithServer())
        {
            if (topic == null)
            {
                chatPane.setChannel(channel);
            }
            else if (!topic.isEmpty())
            {
                chatPane.showTopicMessage(channel, topic);
            }
        }
        this.topic.setText("  " + channel + "     " +  (topic == null ? "" : topic)); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

    }

    /**
     * Return the main window-object
     * 
     * @return JFrame
     */
    public JFrame getMainWindow()
    {
        if (settlersLobby.isIngameOptionsStandalone())
        {
            //wenn die standalone ingame options benutzt werden, so ist dieses
            //Fenster das "Hauptfenster"
            return windowIngameOptions;
        }
        return windowMain;
    }
    
    /**
     * Return the newgame-dialog
     * 
     * @return OpenNewGameDialog
     */
    public OpenNewGameDialog getMapChoosingDialog() {
    	return mapChoosingDialog;
    }
    
    /**
     * Set the newgame-dialog.
     * 
     * @param openNewGameDialog
     */
    public void setMapChoosingDialog( OpenNewGameDialog openNewGameDialog) {
    	this.mapChoosingDialog = openNewGameDialog;
    }

    /**
     * Opens the URL in the System Default Browser 
     * by given url-string.
     * 
     * @param url	url-string (e.g. "https://..:") to open
     */
    public static void openURL(String url)
    {
        if( Desktop.isDesktopSupported() && url.length() > 0 )
        {
            try
            {
            	// convert string into url-object.
                URL myurl = new URL(url);
                openURL(myurl);
            }
            catch (Exception e)
            {
                Test.outputException(e);
            }
        }
    }
    
    /**
     * Opens the URL in the Systems Default Browser 
     * by given url-object.
     * 
     * @param url	URL-object of the url
     */
    public static void openURL(URL myurl)
    {
        if( Desktop.isDesktopSupported() )
        {
            try
            {
            	// convert url-object into uri-object
                URI uri = new URI(myurl.getProtocol(), 
                        null /*userInfo*/,
                        myurl.getHost(), 
                        myurl.getPort(), 
                        myurl.getPath(), 
                        myurl.getQuery(), 
                        myurl.getRef());
                openURL(uri);
            }
            catch (Exception e)
            {
                Test.outputException(e);
            }
        }
    }

    /**
     * Opens the URL in the Systems Default Browser 
     * by given uri-object.
     * 
     * @param url	URI-object of the url
     */
    public static void openURL(URI url)
    {
        if (Desktop.isDesktopSupported())
        {
            try
            {
                Desktop desktop = Desktop.getDesktop();
                desktop.browse(url);
            }
            catch (Exception e)
            {
                Test.outputException(e);
            }
        }
    }


    /**
     * Macht eine der Komponenten {@code mapUserListWindow} oder
     * {@code ingameOptionsWindow} im OnDemandWindow sichtbar oder unsichtbar.
     *
     * @param component {@code mapUserListWindow} oder {@code ingameOptionsWindow}
     * @param tabTitle
     * @param makeVisible true zum sichtbar machen, false zum unsichtbar machen
     */
    private void makeComponentVisible(Component component, String tabTitle, boolean makeVisible)
    {
        //Komponenten werden sichtbar gemacht, indem sie
        //hinzugefügt werden und unsichtbar, indem
        //sie gelöscht werden.
        //Ein Versuch mit setVisible() auf die Komponenten hat nicht so recht
        //funktioniert, daher das harte hinzufügen und löschen
        int index = onDemandWindow.indexOfComponent(component);
        if (makeVisible)
        {
            if (index == -1)
            {
                onDemandWindow.add(tabTitle, component); //$NON-NLS-1$
            }
            onDemandWindow.setSelectedComponent(component);
        }
        else
        {
            onDemandWindow.remove(component);
        }

        updateOnDemandWindowVisibility();
    }

    /**
     * Zeigt das OnDemandWindow an oder versteckt es, je nach internem Zustand.
     */
    private void updateOnDemandWindowVisibility()
    {
        //wenn die JTabbedpane keine Elemente mehr hat
        //wird sie ganz unsichtbar gemacht
        if (onDemandWindow.getComponentCount() > 0)
        {
            onDemandWindow.setVisible(true);
        }
        else
        {
            onDemandWindow.setVisible(false);
        }
    }

    /**
     * fügt Tastaturshortcuts hinzu.
     */
    private void addKeyBindings()
    {
        btnCreateGame.setMnemonic(KeyEvent.VK_N);
        //CRTL+N ebenfalls für Eigenes Spiel
        KeyStroke key = KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK);
        btnCreateGame.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
        .put(key, "actionListenerToCreateGame"); //$NON-NLS-1$
        btnCreateGame.getActionMap().put("actionListenerToCreateGame",  //$NON-NLS-1$
                actionListenerToCreateGame);

        //dieselbe Belegung jetzt nochmal für die standalone Variante
        btnCreateGameStandalone.setMnemonic(KeyEvent.VK_N);
        btnCreateGameStandalone.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
        .put(key, "actionListenerToCreateGameStandalone"); //$NON-NLS-1$
        btnCreateGameStandalone.getActionMap().put("actionListenerToCreateGameStandalone",  //$NON-NLS-1$
                actionListenerToCreateGameStandalone);

    }

    /**
     * Die Darstellung der IngameOptions wird an den internen Zustand
     * der Werte in der {@code Settlerslobby} angepasst.
     */
    public void updateSelectedIngameOptions()
    {
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
			public void run()
            {
                if (pnlIngameOptions != null)
                {
                    pnlIngameOptions.updateView();
                }
                if (ingameOptionsWindow != null)
                {
                    ingameOptionsWindow.updateView();
                }
            }
        });
    }

    /**
     * Zeigt ein Hinweisfenster an, was dazu auffordert in das Kartenauswahlfenster
     * von S3 zu wechseln.
     * @param label Text, der im Fenster dargestellt werden soll (html möglich)
     */
    public void displayMapWait(String label)
    {
        waitMapDialog = new JDialog(currentActiveFrame,
                I18n.getString("GUI.WAIT_FOR_S3_MAP_WINDOW"), false); //$NON-NLS-1$

        JLabel jLabel = new JLabel(label);
        jLabel.setFont(jLabel.getFont().deriveFont(Font.PLAIN));
        jLabel.setBorder(BorderFactory.createEmptyBorder((int) (10 * MessageDialog.getDpiScalingFactor()),(int) (10 * MessageDialog.getDpiScalingFactor()),(int) (10 * MessageDialog.getDpiScalingFactor()),(int) (10 * MessageDialog.getDpiScalingFactor())));
        waitMapDialog.add(jLabel);
        waitMapDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        waitMapDialog.setBackground(settlersLobby.theme.lobbyBackgroundColor);
        waitMapDialog.setForeground(settlersLobby.theme.getTextColor());

        // wenn das Dialog Fenster geschlossen wird,
        // muss der gamestarter gestoppt werden
        waitMapDialog.addWindowListener(new WindowAdapter() {
            @Override
			public void windowClosing(WindowEvent we) {
                settlersLobby.actionCreateGameAbortedStandalone();
            }
        });
        //waitMapDialog.getContentPane().setBackground(new java.awt.Color(255, 255, 255));
        waitMapDialog.pack();
        waitMapDialog.setLocationRelativeTo(getMainWindow());
        waitMapDialog.setVisible(true);
    }

    /**
     * schliesst das Hinweisfenster für die S3 Kartenauswahl.
     */
    public void closeMapWait()
    {
        if (waitMapDialog != null)
        {
            waitMapDialog.setVisible(false);
            waitMapDialog.dispose();
            waitMapDialog = null;
        }
    }

    public void displayInformation(String title, String message)
    {
        JOptionPane.showMessageDialog(getActiveWindow(), message,
                title, JOptionPane.INFORMATION_MESSAGE);
    }

    public boolean displayConfirmDialog(String title, String message)
    {
        int choice = JOptionPane.showConfirmDialog(getActiveWindow(), message,
                title, JOptionPane.OK_CANCEL_OPTION);
        return choice == JOptionPane.OK_OPTION;
    }

    public Object displaySelectionDialog(String title, String message,
            List<? extends Object> list)
    {
        Object[] possibilities = list.toArray();
        Object obj = JOptionPane.showInputDialog(
                getActiveWindow(),
                message,
                title,
                JOptionPane.PLAIN_MESSAGE,
                null,
                possibilities,
                null);
        return obj;

    }

    private Window getActiveWindow()
    {
        Window windowFocusOwner =
            KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusedWindow();
        if (windowFocusOwner != null)
        {
            return windowFocusOwner;
        }
        return currentActiveFrame;
    }


/*    private static final Color inputBoxBackgroundColor = new java.awt.Color(27, 36, 39);
    static
    {
        //Hier können einige Default-Einstellungen vorgenommen werden
        UIDefaults uidefs = UIManager.getDefaults();
        //Object obj = uidefs.get("Dialog.background");
        //uidefs.put("ComboBox.selectionBackground", Color.white);
        uidefs.put("ComboBox.background", inputBoxBackgroundColor); //$NON-NLS-1$
        //uidefs.put("OptionPane.background", Color.WHITE);
        uidefs.put("ProgressBar.background", inputBoxBackgroundColor); //$NON-NLS-1$
        //uidefs.put("ProgressBar.foreground", Color.WHITE);
        //uidefs.put("Panel.background", Color.WHITE);
    }*/

    public void updateListUserOnline() {
    	listUsersOnline.setCellRenderer(new ListCellRenderer(settlersLobby));
    	settlersLobby.getIRCCommunicator().updateUserListInGui();
    } 

    /**
     * Called if some of the configuration has been changed.
     */
    @SuppressWarnings("static-access")
	@Override
	public void configurationWasChanged() {
    	if (settlersLobby.isConnectedWithServer()) {
    		updateListUserOnline();
    		chatPane.setFontSize(settlersLobby.config.getDpiAwareFontSize());
    		if (settlersLobby.config.isHideTeamspeakWindow() != isHideTeamspeakWindowOriginal)
    		{
    		    updateTeamspeakWindowVisibility();
    		}
    		isHideTeamspeakWindowOriginal = settlersLobby.config.isHideTeamspeakWindow();
    		windowMain.repaint();
    		
    		// remove the listener from the input-message-field
    		// and add it, if this function is activated
    		chatPane.removeAutoCompleteForMessageField();
    		if( settlersLobby.config.isAutoCompleteNicknames() ) {
    			chatPane.addAutoCompleteForMessageField();
    		}
    	}
    }

    /**
     * prüft ob im tableOfOpenGames in Zeile row das eigene erstellte Spiel steht.
     * @param row
     * @return
     */
    private boolean isOwnOpenHostGameAt(int row)
    {
        try
        {
            //game und currentOpenGame sind niemals identisch,
            //da auch das eigene erstellte Spiel erst durch eine
            //onPrgMsgNewGame Nachricht in der GUI neu erzeugt wird
            //TODO eventuell sollte man dieses Verhalten ändern
            OpenGame game = tableOfOpenGames.getOpenGameAt(row);
            OpenGame openHostGame = settlersLobby.getOpenHostGame();
            if (game != null &&
                openHostGame != null &&
                game.getHostUserName().equals(openHostGame.getHostUserName()))
            {
                return true;
            }
        }
        catch (Exception e)
        {
            //unerwartete exception, gebe false zurück
            Test.outputException(e);
        }
        return false;
    }

    public void flashWindowIfNotFocused()
    {
        if (!isChatWindowFocused())
        {
            flashWindow();
        }
    }

    private void flashWindow()
    {
        NativeFunctions.getInstance().flashWindow(getMainWindow().getTitle(), 20, 500);
    }


    public boolean isChatWindowFocused() {
    	return windowMain.isFocused();
    }

    public IChatPane getChatPane()
    {
        return chatPane;
    }

    /**
     * Cleart das Chatfenster.
     */
    public void clearChat()
    {
        chatPane.clearChat();
    }

    /**
     * Falls ein Spiel in der Liste der offenen Spiele selektiert ist,
     * wird diese Selektion aufgehoben.
     */
    public void unselectGame()
    {
    	tableOpenGames.getSelectionModel().clearSelection();
    }

    /**
     * Get the imageIcon for window-decorations.
     * 
     * @return Image
     */
    public Image getIcon()
    {
        return icon;
    }

    private void displayLeagueGamesDialog()
    {
    	if (leagueTournamentDialog == null)
		{
			leagueTournamentDialog = this.createLeagueTournamentDialog();
		}
    	leagueTournamentDialog.display();
    }

    private void initTeamspeakWindow()
    {
        teamSpeakPanel = new TeamSpeakPanel(settlersLobby, settlersLobby.config.isHideTeamspeakWindow());
    }

    public void hideChat()
    {
        windowMain.setVisible(false);
        teamSpeakPanel.stopLoading();
        chatPane.postHideChat();
    }

    public void showIp(String ip) {
        lblIp.setText("IP: " + ip);
        lblIp.setToolTipText(ip);
        lblIp.getParent().repaint();
        //Dimension d = lblIp.getPreferredSize();
        //lblIp.setPreferredSize(new Dimension(d.width+14,d.height));
    }

    public void setIpVpn(String ipVpn) {
        setVpnText("\uf0c1", Color.green, I18n.getString("SettlersLobby.VPN_ON"));
        lblIp.setText("VPN IP: " + ipVpn);
        lblIp.setToolTipText(ipVpn);
        lblIp.getParent().repaint();
        //Dimension d = lblIp.getPreferredSize();
        //lblIp.setPreferredSize(new Dimension(d.width+14,d.height));
    }

    /**
     * Set the value and color for the vpn-button.
     * 
     * @param ipVpn
     * @param fontcolor
     */
    public void setVpnText(String value, Color fontcolor, String tooltip) {
    	if( fontcolor != Color.YELLOW ) {
    		if( btnVpnStateWorker != null ) {
    			btnVpnStateWorker = null;
    		}
    	}
    	btnVpnState.setText(value);
    	btnVpnState.setForeground(fontcolor);
    	btnVpnState.setToolTipText(tooltip);
    	if( fontcolor == Color.YELLOW ) {
    		btnVpnStateWorker = new BlinkWorker(btnVpnState, Color.ORANGE);
    		btnVpnStateWorker.execute();
    	}
    }

    /**
     * Get the vpnstatebutton, necessary e.g. for settlersLobby-class.
     * 
     * @return JButton
     */
    public JButton getVpnStateButton() {
    	return btnVpnState;
    }

    public void onNickChange(String oldNick, String newNick)
    {
        if (chatPane != null)
        {
            chatPane.onNickChange(oldNick, newNick);
        }
    }

    public void setS3SmActiveState(boolean value)
    {
        if (value)
        {
            btnSaveManager.setIcon(s3SmIconGreen);
        }
        else
        {
            btnSaveManager.setIcon(s3SmIconRed);
        }
    }

    /**
     * Create the OpenNewGameDialog depending on actual conditions.
     * Adds the required tabs including its components and properties.
     * 
     * This is the place to add OpenGame-Dialogs for other aLobby-integrated games.
     *
     * @return OpenNewGameDialog
     */
    @SuppressWarnings("static-access")
	public OpenNewGameDialog createOpenNewGameDialog() {
	
    	// creates the dialog
    	OpenNewGameDialog openNewGameDialog = new OpenNewGameDialog(this);
    	
    	// get the S3-New-Game-Dialog
    	JPanel s3PanelDialog = new OpenNewS3GameDialog(settlersLobby, openNewGameDialog);
    	s3PanelDialog.setVisible(true);
    	s3PanelDialogObject = (OpenNewS3GameDialog) s3PanelDialog;
    	s3PanelDialogObject.setParentObject(openNewGameDialog);
    	s3PanelDialogObject.loadMaps();
    	
    	// get the S4-New-Game-Dialog
    	if( !settlersLobby.config.isHideS4Interface() ) {
	    	JPanel s4PanelDialog = new OpenNewS4GameDialog(settlersLobby, openNewGameDialog);
	    	s4PanelDialog.setVisible(true);
	    	s4PanelDialogObject = (OpenNewS4GameDialog) s4PanelDialog;
	    	s4PanelDialogObject.setParentObject(openNewGameDialog);
	    	s4PanelDialogObject.loadMaps();
    	}
    	
    	// create the tetris-button
    	JPanel tetrisPanelButton = new OpenTetrisGameDialog(settlersLobby, this);
    	
    	// adds the S3- and S4-tab to tabnavigation in dialog-window
    	if( settlersLobby.config.isHideS4Interface() ) {
    		openNewGameDialog.addTab(I18n.getString("GUI.S3TITLE"), s3PanelDialogObject).addTab("Tetris", tetrisPanelButton);
    	}
    	else {
    		openNewGameDialog.addTab(I18n.getString("GUI.S3TITLE"), s3PanelDialogObject).addTab("<html>" + I18n.getString("GUI.S4TITLE") + " <sup><i>beta</i></sup></html>", s4PanelDialogObject).addTab("Tetris", tetrisPanelButton);
    	}
    	
    	// set window-title
    	openNewGameDialog.setTitle(I18n.getString("GUI.STARTNEWS3GAME"));
    	
    	// set active tab depending on actual setting
		int tabFromSetting = settlersLobby.config.getnewGameTab();
		if( tabFromSetting < 0 ) {
			tabFromSetting = 0;
		}
		openNewGameDialog.getTabListe().setSelectedIndex(tabFromSetting);
		openNewGameDialog.getTabListe().addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JTabbedPane sourceTabbedPane = (JTabbedPane) e.getSource();
				Component activeTab = sourceTabbedPane.getSelectedComponent();
				settlersLobby.config.setnewGameTab(sourceTabbedPane.indexOfComponent(activeTab));
			}
		});
		
		// window to screen-functions
		this.settlersLobby.showOnSpecifiedScreen(settlersLobby.config.getScreen(), openNewGameDialog);
		
    	// returns the dialog
    	return openNewGameDialog;
    }
    
    /**
     * Create the OpenNewGameDialog depending on actual conditions.
     * Adds the required tabs including its components and properties.
     * 
     * This is the place to add OpenGame-Dialogs for other aLobby-integrated games.
     *
     * @return OpenNewGameDialog
     */
    @SuppressWarnings("static-access")
	private LeagueTournamentDialog createLeagueTournamentDialog() {
	
    	// creates the dialog 
    	LeagueTournamentDialog myLeagueTournamentDialog = new LeagueTournamentDialog(this);
    	
    	// load the following only if the league-interface is not hidden
    	if( !Configuration.getInstance().isHideLeagueInterface() )
	    {
    	
	    	// get the Hall Of Fame-Tab
	    	/*JPanel hallOfFameTab = new HallOfFameTab(settlersLobby, myLeagueTournamentDialog);
	    	hallOfFameTab.setVisible(true);
	    	HallOfFameTab hallOfFameTabObject = (HallOfFameTab) hallOfFameTab;
	    	hallOfFameTabObject.setName("halloffame");
	    	hallOfFameTabObject.setParentObject(myLeagueTournamentDialog);*/
	    	
	    	// get the Best Randomers-Tab
	    	JPanel bestRandomersTab = new BestRandomersTab(settlersLobby, myLeagueTournamentDialog);
	    	bestRandomersTab.setVisible(true);
	    	BestRandomersTab bestRandomersTabObject = (BestRandomersTab) bestRandomersTab;
	    	bestRandomersTabObject.setName("bestrandomers");
	    	bestRandomersTabObject.setParentObject(myLeagueTournamentDialog);
	    	
	    	// get the Best Setmapers-Tab
	    	JPanel bestSetmapersTab = new BestSetmapersTab(settlersLobby, myLeagueTournamentDialog);
	    	bestSetmapersTab.setVisible(true);
	    	BestSetmapersTab bestSetmapersTabObject = (BestSetmapersTab) bestSetmapersTab;
	    	bestSetmapersTabObject.setName("bestsetmapers");
	    	bestSetmapersTabObject.setParentObject(myLeagueTournamentDialog);
	    	
	    	// get the Tournament-Dialog
	    	JPanel tournamentDialog = new TournamentDialog(settlersLobby, myLeagueTournamentDialog);
	    	tournamentDialog.setVisible(true);
	    	TournamentDialog tournamentDialogObject = (TournamentDialog) tournamentDialog;
	    	tournamentDialogObject.setName("tournaments");
	    	tournamentDialogObject.setParentObject(myLeagueTournamentDialog);

	    	// adds the tabs to tabnavigation in dialog-window
	    	myLeagueTournamentDialog/*.addTab(I18n.getString("GUI.COMPETITION_TAB_HALL_OF_FAME"), hallOfFameTabObject)*/
	    							.addTab(I18n.getString("GUI.COMPETITION_TAB_BEST_RANDOMERS"), bestRandomersTabObject)
	    							.addTab(I18n.getString("GUI.COMPETITION_TAB_BEST_SETMAPERS"), bestSetmapersTabObject)
	    							.addTab(I18n.getString("GUI.COMPETITION_TAB_TOURNAMENTS"), tournamentDialogObject);
	    	// set window-title
	    	myLeagueTournamentDialog.setTitle(I18n.getString("GUI.COMPETITION_TITLE"));
	    	
	    	// show the window on specific screen
	    	this.settlersLobby.showOnSpecifiedScreen(settlersLobby.config.getScreen(), myLeagueTournamentDialog);
	    	
	    }
    	// returns the dialog
    	return myLeagueTournamentDialog;
    }
    
    /** 
     * Update the list of strings for the autocompletion in textfield
     * -> type in the start of the nickname will complete it to full nickname
     * -> only if this option is activated
     * @param map 
     */
	public void updateAutoComplete(Map<String, String> unsortMap) {
		if( settlersLobby.config.isAutoCompleteNicknames() ) {
	        // sort the list in alphabetical order
	        Map<String, String> treeMap = new TreeMap<String, String>(unsortMap);
	        // and push the result to chatpane, where the autocompletion-task is done
			chatPane.setKeywordList(treeMap);
		}
	}

	/**
	 * Reset the count of community news in tab.
	 * Mark the tab as changed (with italic font-style and red color) if its not the active tab.
	 * @param changed 
	 */
	public synchronized void resetCommunityNewsCount(boolean changed) {
		int compontentIndex = tabbedPane.indexOfComponent(scrollCommunityNews);
		if( compontentIndex > -1 ) {
			int newCount = tableOfCommunityNews.getRowCount();
			boolean italic = false;
			if( tabbedPane.getSelectedIndex() != compontentIndex && changed ) {
				italic = true;
			}
			// if the tab for community news was previously active
			// than reset all unread entries to read
			// -> therefor we have to remove the TableModelListener as it would call this
			//    function here again and again and again
			if( this.tabbedPanePreviousTab == compontentIndex ) {
				TableModelListener[] listeners = tableOfCommunityNews.getTableModelListeners();
				for (int i=0; i < listeners.length; i++) {
					tableOfCommunityNews.removeTableModelListener(listeners[i]);
				}
				for(int row = 0;row < tableOfCommunityNews.getRowCount();row++) {
					tableOfCommunityNews.setValueAt("0", row, 3);
				}
				for (int i=0; i < listeners.length; i++) {
					tableOfCommunityNews.addTableModelListener(listeners[i]);
				}
			}
			String tabTitleText = "";
			if( newCount == 0 ) {
				tabTitleText = I18n.getString("GUI.NO_NEWS");
			}
			else if( newCount == 1 ) {
				tabTitleText = MessageFormat.format(I18n.getString("GUI.NEWS_SINGULAR"), newCount);
				if( italic ) {
					tabTitleText = "<html><i style='color: red;'>" + tabTitleText + "</i></html>";
				}
			}
			else {
				tabTitleText = MessageFormat.format(I18n.getString("GUI.NEWS_PLURAL"), newCount);
				if( italic ) {
					tabTitleText = "<html><i style='color: red;'>" + tabTitleText + "</i></html>";
				}
			}
			tabbedPane.setTitleAt(compontentIndex, tabTitleText);
			if( tabbedPane.getSelectedIndex() == compontentIndex ) {
				this.tabbedPanePreviousTab = compontentIndex;
			}
		}
	}
	
	/**
	 * Reset the count of open games in tab.
	 * Mark the tab as changed (with italic font-style) if its not the active tab.
	 * @param changed 
	 */
	public synchronized void resetOpenGameCount(boolean changed) {
		int compontentIndex = tabbedPane.indexOfComponent(scrollPaneGames);
		if( compontentIndex > -1 ) {
			int openGamesCount = tableOfOpenGames.getRowCount();
			boolean italic = false;
			if( tabbedPane.getSelectedIndex() != compontentIndex && changed ) {
				italic = true;
			}
			String tabTitleText = "";
			if( openGamesCount == 0 ) {
				tabTitleText = I18n.getString("GUI.NO_OPEN_GAMES");
			}
			else if( openGamesCount == 1 ) {
				tabTitleText = MessageFormat.format(I18n.getString("GUI.OPEN_GAME"), openGamesCount);
				if( italic ) {
					tabTitleText = "<html><i>" + tabTitleText + "</i></html>";
				}
			}
			else {
				tabTitleText = MessageFormat.format(I18n.getString("GUI.OPEN_GAMES"), openGamesCount);
				if( italic ) {
					tabTitleText = "<html><i>" + tabTitleText + "</i></html>";
				}
			}
			tabbedPane.setTitleAt(compontentIndex, tabTitleText);
			if( tabbedPane.getSelectedIndex() == compontentIndex ) {
				this.tabbedPanePreviousTab = compontentIndex;
			}
		}
	}
	
	/**
	 * Reset the count of running games in tab.
	 */
	public synchronized void resetRunningGameCount(boolean changed) {
		int compontentIndex = tabbedPane.indexOfComponent(scrollPaneRunningGames);
		if( compontentIndex > -1 ) {
			int runningGamesCount = tableOfRunningGames.getRowCount();
			boolean italic = false;
			if( tabbedPane.getSelectedIndex() != compontentIndex && changed ) {
				italic = true;
			}
			String tabTitleText = "";
			if( runningGamesCount == 0 ) {
				tabTitleText = I18n.getString("GUI.NO_RUNNING_GAMES");
			}
			else if( runningGamesCount == 1 ) {
				tabTitleText = MessageFormat.format(I18n.getString("GUI.RUNNING_GAME"), runningGamesCount);
				if( italic ) {
					tabTitleText = "<html><i>" + tabTitleText + "</i></html>";
				}
			}
			else {
				tabTitleText = MessageFormat.format(I18n.getString("GUI.RUNNING_GAMES"), runningGamesCount);
				if( italic ) {
					tabTitleText = "<html><i>" + tabTitleText + "</i></html>";
				}
			}
			tabbedPane.setTitleAt(compontentIndex, tabTitleText);
			if( tabbedPane.getSelectedIndex() == compontentIndex ) {
				this.tabbedPanePreviousTab = compontentIndex;
			}
		}
	}

	/**
	 * Removes the mapChoosingDialog-Object. It will be regenerate at next request.
	 */
	public void resetMapChoosingDialog() {
		mapChoosingDialog = null;		
	}

	/**
	 * Add a stream to table only if the given username is actual online
	 * and only if he is not already in the listing.
	 * 
	 * @param user
	 * @param link
	 * @param onlinesince
	 */
	public void addStream(String user, String link, String onlinesince) {
		boolean check1 = false;
		boolean check2 = true;
		for (int i = 0; i < this.usersOnline.getSize(); i++) {
            if( this.usersOnline.getElementAt(i).getNick().equalsIgnoreCase(user) ) {
            	check1 = true;
            	i = this.usersOnline.getSize();
            }
		}
		for(int row = 0;row < tableOfStreams.getRowCount();row++) {
			if( tableOfStreams.getValueAt(row, 0).toString().equalsIgnoreCase(user) ) {
				check2 = false;
			}
		}
		if( check1 && check2 ) {
			tableOfStreams.addStream(user, link, onlinesince);
			// inform users that a stream of one of his buddys jumped online
			if (this.settlersLobby.config.isBuddyList(user.toLowerCase()) && this.settlersLobby.config.isNotifyIfBuddyComesOnline() ) {
				this.settlersLobby.messageHandler.addServerMessage(
	                    MessageFormat.format(I18n.getString("SettlersLobby.MSG_STREAM_OF_BUDDY_ONLINE"), user, link)); //$NON-NLS-1$
	        }
		}
	}

	/**
	 * Remove a stream from table.
	 * @param user
	 */
	public void removeStream(String user) {
		tableOfStreams.removeStream(user);
	}
   
	/**
	 * Return the streamingTable.
	 * 
	 * @return
	 */
	public DefaultTableModelForStreams getTableOfStreams() {
		return tableOfStreams;
	}

	/**
	 * Return the optionsFrame.
	 * 
	 * @return
	 */
	public OptionsFrame getOptionsFrame() {
		return optionsFrame;
	}
	
	/**
	 * Get the plain Map-name without any path 
	 * 
	 * @param map
	 * @return
	 */
	public String getPlainMapName( String map ) {
		map = map.toLowerCase();
		map = map.replace(ALobbyConstants.PATH_USER.toLowerCase() + File.separator, "");
        map = map.replace(ALobbyConstants.PATH_MAP.toLowerCase() + File.separator, "");
        map = map.replace(ALobbyConstants.PATH_S3MULTI.toLowerCase() + File.separator, "");
        map = map.replace(ALobbyConstants.PATH_RANDOM.toLowerCase() + File.separator, "");
        map = map.replace(ALobbyConstants.LABEL_SAVE_GAME.toLowerCase() + File.separator, "");
		return map;
	}
	
	/**
	 * Return the teamspeak-object. 
	 * @return 
	 */
	public ITeamspeakPanel getTeamSpeakPanel() {
		return teamSpeakPanel;
	}
	
	public ListModel getUsersOnline() {
		return this.usersOnline;
	}

	/**
	 * Add a community news in tab.
	 * 
	 * @param user
	 * @param date
	 * @param title
	 * @param link 
	 * @param type
	 * @param doNotSendNewMark 
	 * 
	 */
	public void addCommunityNews(String prio, String md5, String date, String title, String text, String link, String type, boolean doNotSendNewMark) {
		tableOfCommunityNews.addNews(prio, md5, date, title, text, link, type, doNotSendNewMark);
	}
	
	/**
	 * Removes all news from community news tab with specific type.
	 * 
	 * @param type
	 */
	public void removeCommunityNewsByType(String type) {
		tableOfCommunityNews.removeNewsByType(type);
	}
	
	/**
	 * Return the CommunityNewsTableModel.
	 * 
	 * @return DefaultTableModelForCommunityNews
	 */
	public DefaultTableModelForCommunityNews getCommunityNews() {
		
		return tableOfCommunityNews;
	}
	
	/**
	 * Return the CommunityNewsTable.
	 * 
	 * @return
	 */
	public JTable getCommunityNewsTable() {
		return this.tableCommunityNews;
	}
	
	/**
	 * Return the tabbedPane
	 * @return 
	 */
	public JTabbedPane getTabbedPane() {
		return this.tabbedPane;
	}

	/**
	 * Remove and entry by its MD5-Hash.
	 * 
	 * @param type
	 */
	public void removeCommunityNewsByMd5(String type) {
		tableOfCommunityNews.removeNewsByMd5(type);
	}

	/**
	 * Return the mapUserListWindow
	 * 
	 * @return
	 */
	public S3MapUserListWindow getUserListWindow() {
		return mapUserListWindow;
	}

	
}
