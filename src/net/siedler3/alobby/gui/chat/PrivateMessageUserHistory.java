/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui.chat;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.LinkedList;
import java.util.ListIterator;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

import net.siedler3.alobby.util.TimerTaskCompatibility;

/**
 * Stores the last 10 users that received or sent a private message.
 * The list is traversed by pressing TAB.
 *
 * @author jim
 *
 */
// Synchronisation is required, because the Class is accessed by the IRC thread,
// the timer thread and the GUI-thread
public class PrivateMessageUserHistory
{
    private static final String PRIVATE_MESSAGE_USER_HISTORY_TAB_ACTION = "PrivateMessageUserHistoryTabAction";
    private JTextField input;
    private IChatPane chatPane;
    private LinkedList<String> history = new LinkedList<String>();
    private ListIterator<String> iter = history.listIterator();
    private TimerTaskCompatibility resetIteratorTask;


    public PrivateMessageUserHistory(JTextField input, IChatPane chatPane)
    {
        this.input = input;
        this.chatPane = chatPane;
        installKeyBindings();
        input.setFocusTraversalKeysEnabled(false);
    }

    public synchronized void add(String user)
    {
        if (user != null && user.length() > 0)
        {
            history.remove(user);
            history.addFirst(user);
            if (history.size() > 10)
            {
                history.removeLast();
            }
            resetIterator();
        }
    }

    public synchronized void resetIterator()
    {
        iter = history.listIterator();
    }

    private synchronized void performTabAction()
    {
        if (iter.hasNext())
        {
            String result = iter.next();
            if (result != null)
            {
                chatPane.selectMessageRecipient(result);
            }
            scheduleResetTimer();
        }
        else if (history.size() > 0)
        {
            resetIterator();
            chatPane.selectMessageRecipient((String)null);
        }
    }

    private void scheduleResetTimer()
    {
        if (resetIteratorTask != null)
        {
            resetIteratorTask.cancel();
        }
        resetIteratorTask = new TimerTaskCompatibility() {
            @Override
            public void run()
            {
                resetIterator();
            }
        };
        //reset the iterator after 3 seconds without pressing TAB
        resetIteratorTask.schedule(3000);
    }

    private void installKeyBindings()
    {
        //keybinding for Tab key
        Action tabAction = new AbstractAction()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                performTabAction();
            }
        };
        input.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, KeyEvent.SHIFT_DOWN_MASK),
                                PRIVATE_MESSAGE_USER_HISTORY_TAB_ACTION);
        input.getActionMap().put(PRIVATE_MESSAGE_USER_HISTORY_TAB_ACTION,
                                 tabAction);

    }

    public String getFirst()
    {
        return history.peekFirst();
    }
}
