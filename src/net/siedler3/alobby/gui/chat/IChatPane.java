package net.siedler3.alobby.gui.chat;

import java.util.Map;

import javax.swing.JPanel;

import net.siedler3.alobby.gui.additional.S3User;

public interface IChatPane
{
    public JPanel getPanel();

    public void addInputText(String text);
    public void setInputText(String text);

    public void clearChat();

    /**
     * called before the chat window is being displayed.
     */
    public void preDisplayChat();
    /**
     * called after the chat window is hidden.
     */
    public void postHideChat();

    public void selectMessageRecipient(S3User user);
    public void selectMessageRecipient(String user);

    public void setFontSize(int fontSize);

    public void setChannel(String channel);
    public void showTopicMessage(String channel, String topic);

    public void onNickChange(String oldNick, String newNick);

	public void setKeywordList(Map<String, String> keywords);

	public void removeAutoCompleteForMessageField();

	public void addAutoCompleteForMessageField();

	public String getInputText();

}
