/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.controlcenter;

import java.io.File;
import java.text.Collator;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.regex.Pattern;

import net.siedler3.alobby.util.CryptoSupport;
/**
 * Repräsentiert ein offenes Spiel.
 * Ist observierbar.
 *
 * @author Stephan Bauer (aka maximilius)
 */
public class OpenGame extends Observable {
	
	public enum MiscType
	{
		//currently only encodes the S3 Version
	    UNKNOWN(-1),
		CD(0),
		GOG(1),
		ALOBBY(2),
		S4(3),
		S3CE(4);
		
		private int value;
		private MiscType(int value)
		{
			this.value = value;
		}
		
		public int value()
		{
			return value;
		}
		
		public static MiscType valueOf(int value)
		{
			switch (value)
			{
            case 0:
                return CD;
            case 1:
                return GOG;
			case 2:
				return ALOBBY;
			case 3:
				return S4;
			case 4:
				return S3CE;
			default:
				return UNKNOWN;
			}
		}
	}

	/**
	 * Provide possible game options for S3CE.
	 * 
	 * @author JHNP727
	 */
	public enum S3CEGameOption {
		NEW_RANDOM_BALANCING(1, "GUI.NEW_BALANCING"),
		MORE_RESOURCES(1 << 1, "GUI.MORE_RESOURCES"),
		NEW_THIEVES(1 << 2, "GUI.NEW_THIEVES"),
		STEALING_ALLOWED(1 << 3, "GUI.STEALING_ALLOWED"),
		INCREASED_TRANSPORT_LIMIT(1 << 4, "GUI.INCREASED_TRANSPORT_LIMIT")
		;

		private final long value;
		private final String translationKey;

		private S3CEGameOption(long value, String translationKey) {
			this.value = value;
			this.translationKey = translationKey;
		}

		public static List<S3CEGameOption> parse(long mask) {
			List<OpenGame.S3CEGameOption> gameModes = new ArrayList<>();
			for (OpenGame.S3CEGameOption gameOption : OpenGame.S3CEGameOption.values()) {
				if (gameOption.isActive(mask)) {
					gameModes.add(gameOption);
				}
			}

			return gameModes;
		}

		public String getTranslationKey() {
			return this.translationKey;
		}

		public long getValue() {
			return this.value;
		}

		public boolean isActive(long mask) {
			return (mask & this.value) == this.value;
		}
	}

	public final static String NEW_NAME = "name";
	public final static String NEW_MAX_PLAYER = "maxPlayer";
	public final static String NEW_CURRENT_PLAYER = "currentPlayer";
	public final static String NEW_GAME_MODE = "gameMode";
	public final static String DESTROYED = "destroyed";
	public final static String PLAYER_ADDED = "player_added:";
	public final static String PLAYER_REMOVED = "player_removed:";

	public static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
	public static final SimpleDateFormat TIME_FORMAT2 = new SimpleDateFormat("dd.MM.yyyy HH:mm");


	private String ip;
	private String hostUserName;
	private String name;
	private String map = null;
	private boolean amazonen;

	private int goods;
	private int maxPlayer;
	private int currentPlayer;
	private List<String> playerList;
	private boolean leagueGame = false;
	private long startTime;
	private MiscType misc = MiscType.UNKNOWN;
	private String saveGameFileName = "";
	private long saveGameTimestamp = 0; //only used for savegames
	private boolean wimo;
	private List<S3CEGameOption> ceGameOptions;
	private boolean requiresFutureVersion;
	private long runningGameStartTimestamp = 0; // only used for runing games
	private long creationTime;
	private int tournamentid = 0;
	private String tournamentname;
	private int round;
	private int matchnumber;
	private int groupnumber;
	private boolean withTrojans;
	private int teamcount;
	private boolean isCoop;

	/**
	 * Erzeugt eine neue Instanz.
	 *
	 * @param ip				IP des Hosts
	 * @param hostUserName		Der Spielername des Hosts.
	 * @param name				Der Name des Spiels.
	 * @param map				Der Kartenname.
	 * @param amazonen			Ob Amazonen mitspielen können.
	 * @param goodsInStockString				Welcher Warenbestand.
	 * @param maxPlayer			Die maximale Spieleranzahl.
	 * @param currentPlayer		Die derzeitige Spieleranzahl.
	 * @param isLeagueGame		Liga-Game.
	 * @param wimo				WiMo-Mode.
	 * @param tournamentid		Turnier-ID.
	 * @param tournamentname	Turnier-Name.
	 */
	public OpenGame(String ip, String hostUserName, String name, String map,
			boolean amazonen, int goodsInStockString, int maxPlayer, int currentPlayer, boolean isLeagueGame, boolean wimo,
			int tournamentid, String tournamentname, int round, int groupnumber, int matchnumber, List<S3CEGameOption> ceGameOptions)
	{
		this.ip = ip;
		this.hostUserName = hostUserName;
		this.name = name;
		this.map = map;
		this.amazonen = amazonen;
		this.goods = goodsInStockString;
		this.maxPlayer = maxPlayer;
		this.currentPlayer = currentPlayer;
		this.playerList = new ArrayList<String>();
		this.playerList.add(hostUserName); //der Host muss initial gesetzt werden
		this.leagueGame = isLeagueGame;
		this.wimo = wimo;
		this.requiresFutureVersion = false;
		this.tournamentid = tournamentid;
		this.tournamentname = tournamentname;
		this.round = round;
		this.matchnumber = matchnumber;
		this.groupnumber = groupnumber;
		this.ceGameOptions = ceGameOptions;
	}

	/**
	 * Getter
	 * @return Die IP.
	 */
	public String getIP()
	{
		return ip;
	}

	/**
	 * Getter
	 * @return Den Spielnamen.
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Getter
	 * @return Die maximale Spieleranzahl.
	 */
	public int getMaxPlayer()
	{
		return maxPlayer;
	}

	/**
	 * Getter
	 * @return Die derzeitige Spieleranzahl.
	 */
	public int getCurrentPlayer()
	{
        if (getPlayerList().size() < getMaxPlayer()) {
            return getPlayerList().size();
        } else {
            return getMaxPlayer();
        }
        //return currentPlayer;
	}

	/**
	 * Getter
	 * @return Den Kartennamen.
	 */
	public String getMap()
	{
		return map;
	}
	
	/**
	 * Get the plain mapname without "user" or something before it
	 * @return
	 */
	public String getPlainMap() {
		return map.replaceAll("(?i)"+Pattern.quote(ALobbyConstants.PATH_USER + File.separator), "").replaceAll("(?i)"+Pattern.quote(ALobbyConstants.PATH_S3MULTI + File.separator), "");
	}

    public boolean doesRequireFutureVersion()
    {
        return requiresFutureVersion;
    }

    public void setRequireFutureVersion(boolean requiresFutureVersion)
    {
        this.requiresFutureVersion = requiresFutureVersion;
    }

	/**
	 * Getter
	 * @return Ob Amazonen mitspielen können.
	 */
	public boolean isAmazonen()
	{
		return amazonen;
	}
	
	/**
	 * Getter
	 * @return whether it is a ecenomy-game
	 */
	public boolean isWimo()
	{
		return wimo;
	}

	/**
	 * Getter
	 * @return Den Spielernamen des Hosts.
	 */
	public String getHostUserName()
	{
		return hostUserName;
	}

	/**
	 * Getter
	 * @return Die Warenanzahl.
	 */
	public int getGoods()
	{
		return goods;
	}

	/**
	 * Checker
	 * 
	 * @return if this is a league-game
	 */
    public boolean isLeagueGame()
    {
        return leagueGame;
    }

    /**
	 * Checker
	 * 
	 * @return if this is a tournament-game.
	 */
    
    public boolean isTournamentGame()
    {
        return tournamentid > 0 ? true : false;
    }
    
    /**
	 * Getter
	 * @return tournamentid.
	 */
	public int getTournamentId()
	{
		return tournamentid;
	}
	
	/**
	 * Getter
	 * @return tournamentname.
	 */
	public String getTournamentName()
	{
		return tournamentname;
	}

	/**
	 * Getter
	 * @return CE game options.
	 */
	public List<S3CEGameOption> getCeGameOptions() { return ceGameOptions; }

	/**
	 * Computes the game mode mask for the given game options.
	 * @return bitmask representing game options
	 */
	public long getGameModeMask() {
		long mask = 0;
		for (S3CEGameOption gameOption : ceGameOptions) {
			mask |= gameOption.value;
		}

		return mask;
	}

	/**
	 * Setzt die maximale Spieleranzahl.
	 * Die Observer werden informiert.
	 * @param maxPlayer Die neue maximale Spieleranzahl.
	 */
	public void setMaxPlayer(int maxPlayer)
	{
	    if (this.maxPlayer != maxPlayer)
	    {
    		this.maxPlayer = maxPlayer;
    		setChanged();
    		this.notifyObservers(NEW_MAX_PLAYER);
	    }
	}

	/**
	 * Setzt den Spielnamen.
	 * Die Observer werden informiert.
	 * @param name Der neue Spielname.
	 */
	public void setName(String name)
	{
		this.name = name;
		setChanged();
		this.notifyObservers(NEW_NAME);
	}

	/**
	 * Setzt die S3:CE Game Optionen.
	 *
	 * @param gameOptions die neuen Game Optionen
	 */
	public void setS3C3Options(List<S3CEGameOption> gameOptions)
	{
		this.ceGameOptions = gameOptions;
		setChanged();
		this.notifyObservers(NEW_GAME_MODE);
	}

    /**
     * Setzt die derzeitige Spieleranzahl.
     * Die Observer werden informiert.
     * @param currentPlayer Die neue derzeitige Spieleranzahl.
     */
    public void setCurrentPlayer(int currentPlayer)
    {
        if (currentPlayer > maxPlayer) {
            Test.output("We wanted to set the number of players to a higher value than the maxPlayers...");
            currentPlayer = maxPlayer;
        }
        if (this.currentPlayer != currentPlayer)
        {
            this.currentPlayer = currentPlayer;
            setChanged();
            this.notifyObservers(NEW_CURRENT_PLAYER);
        }
    }

    /**
     * Setzt die derzeitige Spieleranzahl.
     * Die Observer werden informiert.
     * @param currentPlayer Die neue derzeitige Spieleranzahl.
     */
    public void setCurrentPlayer()
    {
        setCurrentPlayer(playerList.size());
    }

	/**
	 * Die Observer werden informiert, dass dieses offene Spiel nicht mehr
	 * existiert.
	 */
	public void destroy()
	{
		setChanged();
		this.notifyObservers(DESTROYED);
	}

	/**
	 * Adds a list of player to the playerlist.
	 *
	 * @param mapUserList 				comma-separated userlist (e.g. nickname1,nickname2,nickname3)
	 * @param doAllowDoublicateUsers	specify if the nicknames should be unambiguously or not
	 */
	public void addPlayer(String mapUserList, boolean doAllowDoublicateUsers)
	{
		Test.output("commaseparated playerlist: " + mapUserList + ", actual size: " + playerList.size());
	    boolean changed = false;
        String[] tokens = mapUserList.split(",");
        for (String user : tokens)
        {
            if (!user.isEmpty())
            {
                if (!playerList.contains(user))
                {
                    playerList.add(user);
                    changed = true;
                }
                else if ( false != doAllowDoublicateUsers ) {
                	playerList.add(user);
                    changed = true;
                }
            }
        }
        if (changed)
        {
            setChanged();
            this.notifyObservers(PLAYER_ADDED + mapUserList);
        }
	}

	/**
	 * Remove a player from the playerlist.
	 * 
	 * @param playerName
	 */
	public void removePlayer(String playerName)
	{
        if (!playerName.isEmpty())
        {
            if (playerList.remove(playerName))
            {
                Test.output("Remove  player " + playerName + " from the game: " + this.name);
                setChanged();
                this.notifyObservers(PLAYER_REMOVED + playerName);
            }
        } else {
            Test.output("Trying to remove a player with empty-namefrom the game: " + this.name);
        }
        // Synchronize the number of players with the players list
        setCurrentPlayer();
	}

    public List<String> getPlayerList()
    {
        return playerList;
    }
    
    public long getCreationTime()
    {
        return this.creationTime;
    }

    public void setCreationTime(long creationTime)
    {
        this.creationTime = creationTime;
    }

    public long getStartTime()
    {
        return startTime;
    }

    public void setStartTime(long startTime)
    {
        this.startTime = startTime;
    }
    
    public String getSortedPlayerListAsString() {
    	Collections.sort(playerList, Collator.getInstance());
    	return getPlayerListAsString();
    }

    public String getPlayerListAsString()
    {
        StringBuilder sb = new StringBuilder();
        if (getPlayerList().size() > 0)
        {
            for (String name : getPlayerList())
            {
                sb.append(name).append(",");
            }
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString();
    }

    /**
     * For debug-tasks.
     */
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append(name);
        sb.append(", ");
        sb.append(map);
        sb.append(", ");
        sb.append(goods);
        sb.append(", ");
        sb.append(hostUserName);
        sb.append(", ");
        sb.append(currentPlayer).append('/').append(maxPlayer);
        sb.append(", ");
        sb.append(TIME_FORMAT.format(new Date(startTime)));
        sb.append(", ");
        sb.append(playerList);
        sb.append(", ");
        sb.append(getPlayerListAsString());
        sb.append(", ");
        sb.append(getSortedPlayerListAsString());
        sb.append(", ");
        sb.append(misc.toString());
        sb.append(", ");
        sb.append(saveGameFileName);
        sb.append(", ");
        sb.append(saveGameTimestamp + " -> " + TIME_FORMAT2.format(new Date(saveGameTimestamp)));
        sb.append(", ");
        sb.append(runningGameStartTimestamp + " -> " + TIME_FORMAT2.format(new Date(runningGameStartTimestamp)));
        if( this.isLeagueGame() ) {
        	sb.append(", ");
        	sb.append("ist ein Liga-Spiel");
        }
        if( this.isTournamentGame() ) {
        	sb.append(", ");
        	sb.append("ist ein Turnier-Spiel");
        }
        return sb.toString();
    }

    public long getSaveGameTimestamp() 
    {
		return saveGameTimestamp;
	}

	public void setSaveGameTimestamp(long timestamp) 
	{
		this.saveGameTimestamp = timestamp;
	}
	
	public long getRunningGameStartTimestamp() 
    {
		return runningGameStartTimestamp;
	}

	public void setRunningGameStartTimestamp(long timestamp) 
	{
		this.runningGameStartTimestamp = timestamp;
	}

	public MiscType getMisc() 
	{
		return misc;
	}

	public void setMisc(MiscType misc) 
	{
		this.misc = misc;
	}

	public String getSaveGameFileName() 
	{
		return saveGameFileName;
	}

	public void setSaveGameFileName(String saveGameFileName) 
	{
		this.saveGameFileName = saveGameFileName;
	}

	public void setSaveGame(String saveGameFileName, long timestamp) 
	{
		setSaveGameFileName(saveGameFileName);
		setSaveGameTimestamp(timestamp);
	}

	/**
	 * Generate md5-hash representing this game.
	 * 
	 * Consists of:
	 * - Mapname
	 * - List of Player-Nicknames sorted alphabetical
	 * 
	 * Will be used for ..
	 * .. saving addition information about a game which are not in the s3-savegamefile.
	 * .. publishing the information about a new running game to mapbase to identify a game.
	 * .. league- and tournament-games to identify a single game.
	 */
	public String getMd5() {
		String mapName = getPlainMap();
		if( mapName.contains("RANDOM") ) {
			mapName = "\\Random";
		}
		mapName = mapName.replace("Savegame\\", "");
		String md5values = ReqQuery.getId() + mapName + getSortedPlayerListAsString();
    	String md5 = CryptoSupport.md5(md5values).toLowerCase();
    	Test.output("generiere md5: " + md5values + " -> " + md5);
    	return md5;
	}

	public int getTournamentRound() {
		return this.round;
	}

	public int getTournamentGroupnumber() {
		return this.groupnumber;
	}

	public int getTournamentMatchnumber() {
		return this.matchnumber;
	}

	public void setTrojans(boolean withTrojans) {
		this.withTrojans = withTrojans;
	}

	public boolean isTrojans() {
		return this.withTrojans;
	}

	public int getTeamCount() {
		return this.teamcount;
	}
	
	public void setTeamCount( int teamcount ) {
		this.teamcount = teamcount;
	}

	public void setCoop(boolean coop) {
		this.isCoop = coop;
	}
	
	public boolean isCoop() {
		return this.isCoop;
	}

	public void setGoods(int i) {
		this.goods = i;
	}

	/**
	 * Reset the list of players in this game.
	 */
	public void resetPlayerList() {
		this.playerList = new ArrayList<String>();
	}
}
