/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.controlcenter;

import java.io.File;

import com.sun.jna.Native;
import com.sun.jna.platform.win32.Advapi32;
import com.sun.jna.platform.win32.W32Errors;
import com.sun.jna.platform.win32.Win32Exception;
import com.sun.jna.platform.win32.WinNT;
import com.sun.jna.platform.win32.WinReg;
import com.sun.jna.platform.win32.WinReg.HKEY;
import com.sun.jna.platform.win32.WinReg.HKEYByReference;
import com.sun.jna.ptr.IntByReference;

/**
 * Provides methods to read values from the Windows registry.
 * Based on JNA-functionality.
 *
 * @author jim
 *
 */
public class ReqQuery
{
    private static class RegKey
    {
        WinReg.HKEY root;
        String key;
        String name;
        public RegKey(WinReg.HKEY branch, String key, String name)
        {
            this.root = branch;
            this.key = key;
            this.name = name;
        }

        @Override
        public String toString()
        {
            return String.format("%s, %s, %s", root.toString(), key, name);
        }

        /* taken from Advapi32Util class of JNA.
         * Patched with "WinNT.KEY_WOW64_32KEY"
         *
         */
        private static String registryGetStringValue(HKEY root, String key, String value)
        {
            HKEYByReference phkKey = new HKEYByReference();
            int rc = Advapi32.INSTANCE.RegOpenKeyEx(root, key, 0, WinNT.KEY_READ | WinNT.KEY_WOW64_64KEY, phkKey);
            if (rc != W32Errors.ERROR_SUCCESS) {
                throw new Win32Exception(rc);
            }
            try {
                IntByReference lpcbData = new IntByReference();
                IntByReference lpType = new IntByReference();
                rc = Advapi32.INSTANCE.RegQueryValueEx(
                    phkKey.getValue(), value, 0, lpType, (char[]) null, lpcbData);
                if (rc != W32Errors.ERROR_SUCCESS && rc != W32Errors.ERROR_INSUFFICIENT_BUFFER) {
                    throw new Win32Exception(rc);
                }
                if (lpType.getValue() != WinNT.REG_SZ && lpType.getValue() != WinNT.REG_EXPAND_SZ) {
                    throw new RuntimeException("Unexpected registry type " + lpType.getValue() + ", expected REG_SZ or REG_EXPAND_SZ");
                }
                char[] data = new char[lpcbData.getValue()];
                rc = Advapi32.INSTANCE.RegQueryValueEx(
                    phkKey.getValue(), value, 0, lpType, data, lpcbData);
                if (rc != W32Errors.ERROR_SUCCESS && rc != W32Errors.ERROR_INSUFFICIENT_BUFFER) {
                    throw new Win32Exception(rc);
                }
                return Native.toString(data);
            } finally {
                rc = Advapi32.INSTANCE.RegCloseKey(phkKey.getValue());
                if (rc != W32Errors.ERROR_SUCCESS) {
                    throw new Win32Exception(rc);
                }
            }
        }

        /* taken from Advapi32Util class of JNA.
         * Patched with "WinNT.KEY_WOW64_32KEY"
         *
         */
        private static String registry32BitGetStringValue(HKEY root, String key, String value)
        {
            HKEYByReference phkKey = new HKEYByReference();
            int rc = Advapi32.INSTANCE.RegOpenKeyEx(root, key, 0, WinNT.KEY_READ | WinNT.KEY_WOW64_32KEY, phkKey);
            if (rc != W32Errors.ERROR_SUCCESS) {
                throw new Win32Exception(rc);
            }
            try {
                IntByReference lpcbData = new IntByReference();
                IntByReference lpType = new IntByReference();
                rc = Advapi32.INSTANCE.RegQueryValueEx(
                    phkKey.getValue(), value, 0, lpType, (char[]) null, lpcbData);
                if (rc != W32Errors.ERROR_SUCCESS && rc != W32Errors.ERROR_INSUFFICIENT_BUFFER) {
                    throw new Win32Exception(rc);
                }
                if (lpType.getValue() != WinNT.REG_SZ && lpType.getValue() != WinNT.REG_EXPAND_SZ) {
                    throw new RuntimeException("Unexpected registry type " + lpType.getValue() + ", expected REG_SZ or REG_EXPAND_SZ");
                }
                char[] data = new char[lpcbData.getValue()];
                rc = Advapi32.INSTANCE.RegQueryValueEx(
                    phkKey.getValue(), value, 0, lpType, data, lpcbData);
                if (rc != W32Errors.ERROR_SUCCESS && rc != W32Errors.ERROR_INSUFFICIENT_BUFFER) {
                    throw new Win32Exception(rc);
                }
                return Native.toString(data);
            } finally {
                rc = Advapi32.INSTANCE.RegCloseKey(phkKey.getValue());
                if (rc != W32Errors.ERROR_SUCCESS) {
                    throw new Win32Exception(rc);
                }
            }
        }

        /* taken from Advapi32Util class of JNA.
         * Patched with "WinNT.KEY_WOW64_32KEY"
         *
         */
        private static int registry32BitGetIntValue(HKEY root, String key, String value)
        {
            HKEYByReference phkKey = new HKEYByReference();
            int rc = Advapi32.INSTANCE.RegOpenKeyEx(root, key, 0, WinNT.KEY_READ | WinNT.KEY_WOW64_32KEY, phkKey);
            if (rc != W32Errors.ERROR_SUCCESS) {
                throw new Win32Exception(rc);
            }
            try {
                IntByReference lpcbData = new IntByReference();
                IntByReference lpType = new IntByReference();
                rc = Advapi32.INSTANCE.RegQueryValueEx(
                    phkKey.getValue(), value, 0, lpType, (char[]) null, lpcbData);
                if (rc != W32Errors.ERROR_SUCCESS && rc != W32Errors.ERROR_INSUFFICIENT_BUFFER) {
                    throw new Win32Exception(rc);
                }
                if (lpType.getValue() != WinNT.REG_DWORD) {
                    throw new RuntimeException("Unexpected registry type " + lpType.getValue() + ", expected REG_DWORD");
                }
                IntByReference data = new IntByReference();
                rc = Advapi32.INSTANCE.RegQueryValueEx(
                    phkKey.getValue(), value, 0, lpType, data, lpcbData);
                if (rc != W32Errors.ERROR_SUCCESS && rc != W32Errors.ERROR_INSUFFICIENT_BUFFER) {
                    throw new Win32Exception(rc);
                }
                return data.getValue();
            } finally {
                rc = Advapi32.INSTANCE.RegCloseKey(phkKey.getValue());
                if (rc != W32Errors.ERROR_SUCCESS) {
                    throw new Win32Exception(rc);
                }
            }
        }

    	/**
    	* taken from Advapi32Util class of JNA.
        * Patched with "WinNT.KEY_WOW64_32KEY"
        * 
        * Set a string value in registry.
        *
        * @param root
        * Root key.
        * @param keyPath
        * Path to an existing registry key.
        * @param name
        * Value name.
        * @param value
        * Value to write to registry.
        */
        /*
        public static void registry32BitSetStringValue(HKEY root, String keyPath, String name, String value)
        {
	        HKEYByReference phkKey = new HKEYByReference();
	        int rc = Advapi32.INSTANCE.RegOpenKeyEx(root, keyPath, 0, WinNT.KEY_READ | WinNT.KEY_WOW64_32KEY | WinNT.KEY_WRITE, phkKey);
	        if (rc != W32Errors.ERROR_SUCCESS)
	        {
	        	throw new Win32Exception(rc);
	        }
	        try {
	        	char[] data = Native.toCharArray(value);
	        	int rc2 = Advapi32.INSTANCE.RegSetValueEx(phkKey.getValue(), name, 0, WinNT.REG_SZ, data, data.length * Native.WCHAR_SIZE);
	        	if (rc2 != W32Errors.ERROR_SUCCESS)
	        	{
	        		throw new Win32Exception(rc2);
	        	}
	        } finally {
		        rc = Advapi32.INSTANCE.RegCloseKey(phkKey.getValue());
		        if (rc != W32Errors.ERROR_SUCCESS)
		        {
		        	throw new Win32Exception(rc);
		        }
	        }
        }*/

        /**
        * taken from Advapi32Util class of JNA.
        * Patched with "WinNT.KEY_WOW64_32KEY"
        * 
        * Set an integer value in registry.
        *
        * @param root
        * Root key.
        * @param keyPath
        * Path to an existing registry key.
        * @param name
        * Value name.
        * @param value
        * Value to write to registry.
        */
        public static void registry32BitSetIntValue(HKEY root, String keyPath, String name, int value)
        {
	        HKEYByReference phkKey = new HKEYByReference();
	        int rc = Advapi32.INSTANCE.RegOpenKeyEx(root, keyPath, 0, WinNT.KEY_READ | WinNT.KEY_WOW64_32KEY | WinNT.KEY_WRITE, phkKey);
	        if (rc != W32Errors.ERROR_SUCCESS)
	        {
	        	throw new Win32Exception(rc);
	        }
	        try {
	        	byte[] data = new byte[4];
	        	data[0] = (byte) (value & 0xff);
	        	data[1] = (byte) ((value >> 8) & 0xff);
	        	data[2] = (byte) ((value >> 16) & 0xff);
	        	data[3] = (byte) ((value >> 24) & 0xff);
	        	int rc2 = Advapi32.INSTANCE.RegSetValueEx(phkKey.getValue(), name, 0, WinNT.REG_DWORD, data, 4);
	        	if (rc2 != W32Errors.ERROR_SUCCESS)
	        	{
	        		throw new Win32Exception(rc2);
	        	}
	        } finally {
		        rc = Advapi32.INSTANCE.RegCloseKey(phkKey.getValue());
		        if (rc != W32Errors.ERROR_SUCCESS)
		        {
		        	throw new Win32Exception(rc);
		        }
	        }
        }

        public String readRegistryStringValue()
        {
            String result;
            try
            {
                result = registry32BitGetStringValue(root, key, name);
            }
            catch (Win32Exception e)
            {
                //Test.output("Error reading " + this);
                result = null;
            }
            return result;
        }
 
        public String readRegistryStringValueNoWow()
        {
            String result;
            try
            {
                result = registryGetStringValue(root, key, name);
            }
            catch (Win32Exception e)
            {
               // Test.output("Error reading " + this);
                result = null;
            }
            return result;
        }

        public int readRegistryIntValue()
        {
            int result;
            try
            {
                result = registry32BitGetIntValue(root, key, name);
            }
            catch (Win32Exception e)
            {
                //Test.output("Error reading " + this);
                Test.outputException(e);
                throw e;
            }
            return result;
        }

        /*
        public boolean writeRegistryStringValue(String value)
        {
            try
            {
                registry32BitSetStringValue(root, key, name, value);
            }
            catch (Win32Exception e)
            {
                Test.output("Error writing " + this);
                Test.outputException(e);
                return false;
            }
            return true;
        }*/

        public boolean writeRegistryIntValue(int value)
        {
            try
            {
                registry32BitSetIntValue(root, key, name, value);
            }
            catch (Win32Exception e)
            {
                Test.output("Error writing " + this);
                Test.outputException(e);
                return false;
            }
            return true;
        }
    }

    private static final RegKey S3_EXE_CD_CMD = new RegKey(WinReg.HKEY_LOCAL_MACHINE,
            "Software\\Microsoft\\Windows\\CurrentVersion\\App Paths\\S3.EXE", "");

    private static final RegKey S3_EDITOR_PATH_CD_CMD = new RegKey(WinReg.HKEY_LOCAL_MACHINE,
            "Software\\BlueByte\\S3PLUSCD", "EditorPathPlus");

    private static final RegKey S3_EXE_GOG_OLD_CMD = new RegKey(WinReg.HKEY_LOCAL_MACHINE,
            "Software\\Microsoft\\Windows\\CurrentVersion\\App Paths\\S3.EXE", "@");

    private static final RegKey S3_EXE_GOG_NEW_CMD = new RegKey(WinReg.HKEY_LOCAL_MACHINE,
            "Software\\GOG.com\\Games\\1207659185", "exe");
    
    private static final RegKey S3_HE_x64 = new RegKey(WinReg.HKEY_LOCAL_MACHINE,
            "Software\\Wow6432Node\\Ubisoft\\Launcher\\Installs\\11784", "InstallDir");
    
    private static final RegKey S3_HE_x32 = new RegKey(WinReg.HKEY_LOCAL_MACHINE,
            "Software\\Ubisoft\\Launcher\\Installs\\11784", "InstallDir");
    
    private static final RegKey S4_HE_x64 = new RegKey(WinReg.HKEY_LOCAL_MACHINE,
            "Software\\Wow6432Node\\Ubisoft\\Launcher\\Installs\\11785", "InstallDir");
    
    private static final RegKey S4_HE_x32 = new RegKey(WinReg.HKEY_LOCAL_MACHINE,
            "Software\\Ubisoft\\Launcher\\Installs\\11785", "InstallDir");

    private static final RegKey S4_DIR = new RegKey(WinReg.HKEY_LOCAL_MACHINE,
            "Software\\BlueByte\\Settlers4", "Path");

    private static final String S3_REG_PATH = "Software\\BlueByte\\Siedler3\\1.0\\General";

    private static final RegKey S3_FONT = new RegKey(WinReg.HKEY_LOCAL_MACHINE, S3_REG_PATH, "Font");

    private static final RegKey S3_FONTSIZE = new RegKey(WinReg.HKEY_LOCAL_MACHINE, S3_REG_PATH, "FontSize");

    private static final RegKey S3_INTRO = new RegKey(WinReg.HKEY_LOCAL_MACHINE, S3_REG_PATH, "Intro");

    private static final RegKey S3_RESOLUTION = new RegKey(WinReg.HKEY_LOCAL_MACHINE, S3_REG_PATH, "Resolution");

    private static final RegKey S3_SERIALNUMBER = new RegKey(WinReg.HKEY_LOCAL_MACHINE, S3_REG_PATH, "SerialNumber");

    private static final RegKey NTVER_REG_HKCU = new RegKey(WinReg.HKEY_CURRENT_USER,
            "Software\\Siedler 3 Community\\alobby", "CurrentWindowsNTVersion");

    private static final RegKey NTVER_REG_HKLM = new RegKey(WinReg.HKEY_LOCAL_MACHINE,
            "Software\\Siedler 3 Community\\alobby", "CurrentWindowsNTVersion");

    private static final RegKey ID_REG_HKCU = new RegKey(WinReg.HKEY_CURRENT_USER,
            "Software\\Siedler 3 Community\\alobby", "ID");

    private static final RegKey ID_REG_HKLM = new RegKey(WinReg.HKEY_LOCAL_MACHINE,
            "Software\\Siedler 3 Community\\alobby", "ID");

    public static String s3Font = getS3Font();
    public static int s3FontSize = getS3FontSize();
    public static String s3ExeFilePath = getS3ExeFilePath();

    private static final String APPCOMPAT_PATH = "Software\\Microsoft\\Windows NT\\CurrentVersion\\AppCompatFlags\\Layers";

    private static final RegKey S3_EXE_COMPAT_FLAG = new RegKey(WinReg.HKEY_LOCAL_MACHINE, APPCOMPAT_PATH,  getS3ExeFilePath());
    private static final RegKey S3_EXE_USER_COMPAT_FLAG = new RegKey(WinReg.HKEY_CURRENT_USER, APPCOMPAT_PATH, getS3ExeFilePath());

    private static final RegKey S3_MULTI_EXE_COMPAT_FLAG = new RegKey(WinReg.HKEY_LOCAL_MACHINE, APPCOMPAT_PATH, new File (getS3ExeDirectory(), "S3_multi.EXE").toString());
    private static final RegKey S3_MULTI_EXE_USER_COMPAT_FLAG = new RegKey(WinReg.HKEY_CURRENT_USER, APPCOMPAT_PATH, new File (getS3ExeDirectory(), "S3_multi.EXE").toString());

    private static final RegKey S3_ALOBBY_EXE_COMPAT_FLAG = new RegKey(WinReg.HKEY_LOCAL_MACHINE, APPCOMPAT_PATH, new File (getS3ExeDirectory(), "s3_alobby.exe").toString());
    private static final RegKey S3_ALOBBY_EXE_USER_COMPAT_FLAG = new RegKey(WinReg.HKEY_CURRENT_USER, APPCOMPAT_PATH, new File (getS3ExeDirectory(), "s3_alobby.exe").toString());

    public static boolean isAppCompatSetForS3Exe()
    {
        String result = null;
        try
        {
            result = S3_EXE_COMPAT_FLAG.readRegistryStringValueNoWow();
        }
        catch (Exception e)
        {       
            Test.outputException(e);
        }
        if (result != null && (result.contains("WIN") || result.contains("VISTA"))) {
            Test.output(result);
            return true;
        }

        try
        {
            result = S3_EXE_USER_COMPAT_FLAG.readRegistryStringValue();
        }
        catch (Exception e)
        {       
            Test.outputException(e);
        }
        if (result != null && (result.contains("WIN") || result.contains("VISTA"))) {
            Test.output(result);
            return true;
        } else {
            return false;
        }
    }

    public static boolean isAppCompatSetForS3AlobbyExe()
    {
        String result = null;
        try
        {
            result = S3_ALOBBY_EXE_COMPAT_FLAG.readRegistryStringValueNoWow();
        }
        catch (Exception e)
        {       
            Test.outputException(e);
        }
        if (result != null && (result.contains("WIN") || result.contains("VISTA"))) {
            Test.output(result);
            return true;
        }

        try
        {
            result = S3_ALOBBY_EXE_USER_COMPAT_FLAG.readRegistryStringValue();
        }
        catch (Exception e)
        {       
            Test.outputException(e);
        }
        if (result != null && (result.contains("WIN") || result.contains("VISTA"))) {
            Test.output(result);
            return true;
        } else {
            return false;
        }
    }

    public static boolean isAppCompatSetForS3MultiExe()
    {
        String result = null;
        try
        {
            result = S3_MULTI_EXE_COMPAT_FLAG.readRegistryStringValueNoWow();
        }
        catch (Exception e)
        {       
            Test.outputException(e);
        }
        if (result != null && (result.contains("WIN") || result.contains("VISTA"))) {
            Test.output(result);
            return true;
        }

        try
        {
            result = S3_MULTI_EXE_USER_COMPAT_FLAG.readRegistryStringValue();
        }
        catch (Exception e)
        {       
            Test.outputException(e);
        }
        if (result != null && (result.contains("WIN") || result.contains("VISTA"))) {
            Test.output(result);
            return true;
        } else {
            return false;
        }
    }

    public static boolean isAppCompatSetForAlobbyExe(String path)
    {
        String result = null;
        try
        {
            result = new RegKey(WinReg.HKEY_LOCAL_MACHINE, APPCOMPAT_PATH,  path).readRegistryStringValueNoWow();
        }
        catch (Exception e)
        {       
            Test.outputException(e);
        }
        if (result != null && (result.contains("WIN") || result.contains("VISTA"))) {
            Test.output(result);
            return true;
        }

        try
        {
            result = new RegKey(WinReg.HKEY_CURRENT_USER, APPCOMPAT_PATH,  path).readRegistryStringValue();
        }
        catch (Exception e)
        {       
            Test.outputException(e);
        }
        if (result != null && (result.contains("WIN") || result.contains("VISTA"))) {
            Test.output(result);
            return true;
        } else {
            return false;
        }
    }

    public static double getNtVer()
    {
    	String result = null;
    	try
    	{
    		result = NTVER_REG_HKLM.readRegistryStringValue();
    		Test.output("Real NT version: " + result);
    	}
    	catch (Exception e)
        {    	
    		try
	    	{
	    		result = NTVER_REG_HKCU.readRegistryStringValue();
	    		Test.output("Real NT version: " + result);
	    	}
	    	catch (Exception e2)
	        {
	            Test.outputException(e);
	        }
        }
    	if (result != null) {
    		double resultAsDouble = Double.parseDouble(result);
    		return resultAsDouble;
    	}
        return 0.0;
    }

    public static String getId()
    {
    	String result = null;
    	try
    	{
    		result = ID_REG_HKLM.readRegistryStringValue();
    		Test.output(result);
    	}
    	catch (Exception e)
        {    	
    		try
	    	{
	    		result = ID_REG_HKCU.readRegistryStringValue();
	    		Test.output(result);
	    	}
	    	catch (Exception e2)
	        {
	            Test.outputException(e);
	        }
        }
        return result;
    }


    public static String getS3Font()
    {
    	String result = null;
    	try
    	{
    		result = S3_FONT.readRegistryStringValue();
    		Test.output("s3 Font: " + result);
    	}
    	catch (Win32Exception e)
    	{
    		// Do nothing - game is likely not installed
    	}
    	catch (Exception e)
        {
            Test.outputException(e);
        }
        return result;
    }

    public static int getS3FontSize()
    {
        int value = -1;
        try
        {
            value = S3_FONTSIZE.readRegistryIntValue();
            Test.output("s3 fontsize: " + value);
        }
    	catch (Win32Exception e)
    	{
    		// Do nothing - game is likely not installed
    	}
        catch (Exception e)
        {
            Test.outputException(e);
        }
        return value;
    }

    /**
     * reads the path of the s3.exe
     * @return
     */
    public static String getS3ExeFilePath()
    {
    	String result = null;
    	try
    	{
    		result = S3_EXE_GOG_NEW_CMD.readRegistryStringValue();
    	}
    	catch (Win32Exception e)
    	{
    		// Do nothing - game is likely not installed
    	}
        catch (Exception e)
        {
            Test.outputException(e);
        }
    	if (result == null || result.isEmpty())
    	{
        	try
        	{
        		result = S3_EXE_GOG_OLD_CMD.readRegistryStringValue();
        		if (result.contains("Siedler3R.exe")) {
        		    // It is the S3 History Edition :/
        		    result = null;
        		}
        	}
        	catch (Win32Exception e)
        	{
        		// Do nothing - game is likely not installed
        	}
            catch (Exception e)
            {
                Test.outputException(e);
            }
    	}
        if (result == null || result.isEmpty())
        {
            try
            {
                result = S3_EXE_CD_CMD.readRegistryStringValue();
                if (result.contains("Siedler3R.exe")) {
                    // It is the S3 History Edition :/
                    result = null;
                }
            }
            catch (Win32Exception e)
            {
                // Do nothing - game is likely not installed
            }
            catch (Exception e)
            {
                Test.outputException(e);
            }
        }
        if (result == null || result.isEmpty())
        {
            try
            {
                result = S3_EDITOR_PATH_CD_CMD.readRegistryStringValue();
                if (result.startsWith("\\")) {
                    // This is a relative string, e.g.: "\S3Editor", so can't help us
                    result = null;
                } else {
                    // String is the expected one, e.g. "c:\Siedler3\S3Editor"
                    // :attention: this might fail with very old Mission CD or Amazon CD installations,
                    // where the user could choose where to install the Editor instead of it being
                    // automatically installed inside the S3 folder
                    result = result.substring(0, result.lastIndexOf("\\"));
                    // The end string should look like "c:\Siedler3\s3.exe"
                    result = result + "s3.exe";
                }
            }
            catch (Win32Exception e)
            {
                // Do nothing - game is likely not installed
            }
            catch (Exception e)
            {
                Test.outputException(e);
            }
        }
        // Test.output("s3.exe: " + result);
        return result;
    }

    /**
     * reads the value of the Intro setting.
     *
     * @return -1 in case of an error, else value of Intro setting.
     */
    public static int getS3Intro()
    {
        int value = -1;
        try
        {
            value = S3_INTRO.readRegistryIntValue();
            Test.output("s3 intro: " + value);
        }
        catch (Win32Exception e)
        {
            // Do nothing - game is likely not installed
        }
        catch (Exception e)
        {
            Test.outputException(e);
        }
        return value;
    }

    public static boolean setS3Intro(int value)
    {
        boolean result = false;
        try
        {
            result = S3_INTRO.writeRegistryIntValue(value);
            Test.output("s3 intro: " + value);
        }
        catch (Exception e)
        {
            Test.outputException(e);
        }
        return result;
    }

    /**
     * reads the value of the Resolution setting.
     *
     * @return -1 in case of an error, else value of Resolution setting.
     */
    public static int getS3Resolution()
    {
        int value = -1;
        try
        {
            value = S3_RESOLUTION.readRegistryIntValue();
            Test.output("s3 resolution: " + value);
        }
        catch (Win32Exception e)
        {
            // Do nothing - game is likely not installed
        }
        catch (Exception e)
        {
            Test.outputException(e);
        }
        return value;
    }

    public static boolean setS3Resolution(int value)
    {
        boolean result = false;
        try
        {
            result = S3_RESOLUTION.writeRegistryIntValue(value);
            Test.output("s3 resolution: " + value);
        }
        catch (Exception e)
        {
            Test.outputException(e);
        }
        return result;
    }

    /**
     * reads the value of the SerialNumber setting.
     *
     * @return null in case of an error or if Serial Number was not set or is empty, else value of SerialNumber setting.
     */
    public static String getS3SerialNumber()
    {
        String value = null;
        try
        {
            value = S3_SERIALNUMBER.readRegistryStringValue();
            if (value != null && value.isBlank()) {
                value = null;
            }
        }
        catch (Win32Exception e)
        {
            // Do nothing - game is likely not installed
        }
        catch (Exception e)
        {
            Test.outputException(e);
        }
        return value;
    }

    public static boolean isS3SerialNumberSet() {
        boolean result = ReqQuery.getS3SerialNumber() != null;
        Test.output("isS3SerialNumberSet(): " + result);
        return result;
    }

    public static void updateS3Font()
    {
        ReqQuery.s3Font = ReqQuery.getS3Font();
        ReqQuery.s3FontSize = ReqQuery.getS3FontSize();
    }

    public static void updateS3Font(double scale)
    {
        ReqQuery.s3Font = ReqQuery.getS3Font();
        ReqQuery.s3FontSize = (int) (ReqQuery.getS3FontSize() * scale);
    }
        
    public static File getS3ExeDirectory()
    {
    	if (s3ExeFilePath != null && !s3ExeFilePath.isEmpty())
    	{
    		File result = new File(s3ExeFilePath).getParentFile();
    		if (result.isDirectory())
    		{
    		    // Test.output("s3 dir: " + result);
    			return result;
    		} else {
    		    // Test.output("s3 dir is supposed to be: '" + result + "', but it does not exist!");
    		}
    	}
    	return null;
    }
    
    /**
     * Check if the S3-History-Edition is installed 
     * 
     * @return
     */
    public static boolean isS3HistoryEditionInstalled() {
    	String result = null;
    	try
    	{
    		result = S3_HE_x64.readRegistryStringValueNoWow();
    		Test.output("S3_HE_x64 dir: " + result);
    	}
    	catch (Win32Exception e)
    	{
    		// Do nothing - game is likely not installed
    	}
        catch (Exception e)
        {
            Test.outputException(e);
        }
    	if( result != null && !result.isEmpty() ) {
    		return true;
    	}
    	try
    	{
    		result = S3_HE_x32.readRegistryStringValueNoWow();
    		Test.output("S3_HE_x32 dir: " + result);
    	}
    	catch (Win32Exception e)
    	{
    		// Do nothing - game is likely not installed
    	}
        catch (Exception e)
        {
            Test.outputException(e);
        }
		return false;
    }
    
    /**
     * Check if the S4-History-Edition is installed 
     * 
     * @return
     */
    public static boolean isS4HistoryEditionInstalled() {
    	String result = null;
    	try
    	{
    		result = S4_HE_x64.readRegistryStringValueNoWow();
    		Test.output("S4_HE_x64 dir: " + result);
    	}
    	catch (Win32Exception e)
    	{
    		// Do nothing - game is likely not installed
    	}
        catch (Exception e)
        {
            Test.outputException(e);
        }
    	if( result != null && !result.isEmpty() ) {
    		return true;
    	}
    	try
    	{
    		result = S4_HE_x32.readRegistryStringValueNoWow();
    		Test.output("S4_HE_x32 dir: " + result);
    	}
    	catch (Win32Exception e)
    	{
    		// Do nothing - game is likely not installed
    	}
        catch (Exception e)
        {
            Test.outputException(e);
        }
		return false;
    }

	public static File getS4Directory()
	{
		String result = null;
    	try
    	{
    		result = S4_DIR.readRegistryStringValue();
    		// Test.output("s4 dir: " + result);
    	}
    	catch (Win32Exception e)
    	{
    		// Do nothing - game is likely not installed
    	}
        catch (Exception e)
        {
            Test.outputException(e);
        }
		if (result != null && !result.isEmpty())
		{
			File resultFile = new File(result);
			return resultFile;
		}
		return null;
	}

	public static File getS4ExeDirectory()
	{
		String result = null;
    	try
    	{
    		result = S4_DIR.readRegistryStringValue();
    		// Test.output("s4 dir: " + result);
    	}
    	catch (Win32Exception e)
    	{
    		// Do nothing - game is likely not installed
    	}
        catch (Exception e)
        {
            Test.outputException(e);
        }
		if (result != null && !result.isEmpty())
		{
			File resultFile = new File(result, "Exe");
			return resultFile;
		}
		return null;
	}
}
