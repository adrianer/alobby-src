/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.controlcenter;

import java.util.Vector;

/**
 * Verteilt alle Nachrichten an die Ausgabemedien
 * 
 * @author Phil
 **/
public class MessageHandler
{
    private Vector<IMessageListener> listener = new Vector<IMessageListener>();
    
    public MessageHandler()
    {
    }

    /**
     * Verteilt Chat Nachrichten an die registrierten Listener.
     * @param user
     * @param message
     * @param bold
     */
    public void addChatMessage(String user, String message, boolean bold)
    { 
        for (IMessageListener messageListener : listener)
        {
            messageListener.addChatMessage(user, message, bold);
        }       
    }
    
    /**
     * Verteilt Servernachrichten an die registrierten Listener
     * @param serverMessage
     */
    public void addServerMessage(String serverMessage)
    {
    	for(IMessageListener messageListener : listener){
    		messageListener.addServerMessage(serverMessage);
    	}
    }


    
    /**
     * Verteilt private Nachrichten an die registrierten Listener.
     * @param sender
     * @param receiver
     * @param message
     */
    public void addWhisperedMessage(String sender, String receiver, String message)
    {
		for (IMessageListener messageListener : listener)
		{
			messageListener.addWhisperedMessage(sender, receiver, message);
		}		
    }
    
    /**
     * Verteilt Notices an die registrierten Listener.
     * 
     * @param sender
     * @param receiver
     * @param message
     */
    public void addNoticeMessage(String sender, String receiver, String message)
    {
        for (IMessageListener messageListener : listener)
        {
            messageListener.addNoticeMessage(sender, receiver, message);
        }       
    }
    
    /**
     * Verteilt Action Nachrichten an die registrierten Listener.
     * @param user
     * @param message
     */
    public void addActionMessage(String user, String message)
    { 
        for (IMessageListener messageListener : listener)
        {
            messageListener.addActionMessage(user, message);
        }       
    }

    
    /**
     * Registriert einen neuen Listener bei MessageHandler
     * 
     * @param newListener Der Listener, der registiert werden soll
     */
    public void registerNewListener(IMessageListener newListener){        
    	listener.add(newListener);
    }
    
    /**
     * Entfernt einen Listener, wenn er vorhanden ist.
     * @param listener Der Listener, der entfernt werden soll.
     */
    public void removeListener(IMessageListener listener){
    	this.listener.remove(listener);
    }
}
