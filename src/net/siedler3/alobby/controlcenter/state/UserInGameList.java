/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.controlcenter.state;

import java.util.ArrayList;
import java.util.List;

import net.siedler3.alobby.controlcenter.Test;

public class UserInGameList
{
    String host;
    String mapName;
    List<UserInGame> userList;
    
    public UserInGameList()
    {
        this("", "", new ArrayList<UserInGame>());
    }
    
    public UserInGameList(String host, String mapName)
    {
        this(host, mapName, new ArrayList<UserInGame>());
    }

    public UserInGameList(String host, String mapName,
            List<UserInGame> userList)
    {
        super();
        this.host = host;
        this.mapName = mapName;
        this.userList = userList;
    }

    public String getHost()
    {
        return host;
    }

    public String getMapName()
    {
        return mapName;
    }

    public List<UserInGame> getUserList()
    {
        return userList;
    }
    
    public void updateUser(UserInGame user, boolean allowDoublicateUserNames)
    {
    	Test.output("add user " + user.getNick() + " to userlist");
        int index = userList.indexOf(user);
        if (index >= 0 && false != allowDoublicateUserNames)
        {
            userList.set(index, user);
        }
        else
        {
            userList.add(user);
        }
    }
    
    public boolean removeUser(UserInGame user)
    {
        return userList.remove(user);
    }
    
    public UserInGame getUser(String nick)
    {
        UserInGame user = new UserInGame(nick);
        int index = userList.indexOf(user);
        if (index >= 0)
        {
            user = userList.get(index);
            return user;
        }
        return null;
    }

    @Override
    public UserInGameList clone() throws CloneNotSupportedException
    {

        if (userList instanceof ArrayList)
        {
            @SuppressWarnings("unchecked")
            List<UserInGame> clone = (List<UserInGame>)((ArrayList<UserInGame>)userList).clone();
            return new UserInGameList(host, mapName, clone);
        }
        throw new CloneNotSupportedException();
    }

    /**
     * Set nickname for host.
     * 
     * @param nick
     */
	public void setHostNick(String nick) {
		this.host = nick;
	}

	/**
	 * Set Map for the game.
	 * 
	 * @param map
	 */
	public void setMap(String map) {
		this.mapName = map;
	}
    
}
