/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.controlcenter.state;

import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.SettlersLobby;

public class GameStateJoining extends GameState
{
    
    public GameStateJoining(SettlersLobby settlersLobby, UserInGameList userList,
            UserInGame user, UserInGame host)
    {
        super(settlersLobby, userList, user, host);
    }


    @Override
    public synchronized void userHasJoinedGame()
    {
        if (!isHost)
        {
            //Nachricht an Host senden, daß man da ist
            settlersLobby.sendGameProgramMessage(
                    ALobbyConstants.TOKEN_JOINED, host, userList.getMapName());
        }
        user.setJoining(false);
        userList.updateUser(user, true);
        //GameState auf Minichat setzen
        setGameState(new GameStateInMinichat(settlersLobby, userList, user, host), false);
    }

}
