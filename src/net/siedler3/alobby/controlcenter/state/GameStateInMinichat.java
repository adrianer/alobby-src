/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.controlcenter.state;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.OpenGame;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.util.httpRequests;


public class GameStateInMinichat extends GameState
{

    public GameStateInMinichat(SettlersLobby settlersLobby, UserInGameList userList,
            UserInGame user, UserInGame host)
    {
        super(settlersLobby, userList, user, host);
    }

    @Override
    public synchronized void onWantsToJoin(UserInGame user, String mapName)
    {
        //nur der host darf OnWantsToJoin Nachrichten verarbeiten
        if (!isHost)
        {
            return;
        }
        if(!isValid(mapName))
        {
            //der user hat eine andere map gestartet als wir?!?
            //entsprechend markieren
            user.setValid(false); 
        }
        //spieler einfügen und farblich markieren als "wantsToJoin"
        user.setJoining(true);
        userList.updateUser(user, true);
        updateListInGui();
    }
    
    
    @Override
    public synchronized void onJoin(UserInGame user, String mapName, String ip)
    {
        //nur der host darf OnJoin Nachrichten verarbeiten
        if (isHost)
        {
            rawOnJoin(user, mapName);
            //wenn ein Spieler im Spiel ankommt, sendet der Host
            //eine Liste aller aktuellen Spieler (und zwar an alle)
            settlersLobby.scheduleSendMapUserList();
            updateListInGui();
            // add the user to the openHostGame-userlist
            OpenGame currentOpenGame = settlersLobby.getOpenHostGame();
            currentOpenGame.addPlayer(user.getNick(), false);
            currentOpenGame.setCurrentPlayer();
            if( currentOpenGame != null ) {
    		    // prepare message to update info on webserver
    	    	// -> responsive is not relevant for us here
    	    	httpRequests httpRequests = new httpRequests();
    	    	httpRequests.setUrl(settlersLobby.config.getLobbyRequestUrl());
    	    	httpRequests.addParameter("updategame", 1);
    	    	httpRequests.addParameter("state", "open");
    	    	httpRequests.addParameter("hostname", settlersLobby.getNick());
    	    	httpRequests.addParameter("ip", settlersLobby.ipToUse());
    	    	// no token-security for this request
    	    	httpRequests.setIgnorePhpSessId(true);
    	    	// send the list of players in this game as JSON
    			List<String> users = currentOpenGame.getPlayerList();
    			JSONArray usersAsJSON = new JSONArray();
    			for( int i = 0;i<users.size();i++ ) {
    				JSONObject item = new JSONObject();
    				item.put("name", users.get(i));
    				usersAsJSON.put(item);
    			}
    			Test.output("playerlist: " + usersAsJSON.toString());
    			httpRequests.addParameter("playerlist", usersAsJSON.toString());
    			// send the player-count
    	    	httpRequests.addParameter("playercount", users.size());
    	    	try {
    	    		httpRequests.doRequest();
    	    	} catch (IOException e) {
    	    	}
            }
        }
    }
    
    private void rawOnJoin(UserInGame user, String mapName)
    {
        if(!isValid(mapName))
        {
            //der user hat eine andere map gestartet als wir?!?
            //entsprechend markieren
            user.setValid(false); 
        }
        //Spieler einfügen/aktualisieren und farblich als "anwesend" markieren 
        user.setJoining(false);
        userList.updateUser(user, true);
    }
    
    @Override
    public synchronized void onJoin(String mapUserList, String mapName, String hostName)
    {
        //der host hat eine neue MapUserList übermittelt.
        //alle user entsprechend updaten
        if (!isHost)
        {
            if (host.getNick().equals(hostName))
            {
                String[] tokens = mapUserList.split(",");
                for (String user : tokens)
                {
                    if (!user.isEmpty())
                    {
                        rawOnJoin(new UserInGame(user), mapName);
                    }
                }
                updateListInGui();
            }
        }
    }

    public synchronized void onPart(UserInGame user, String mapName, String hostName)
    {
        //der host hat eine Nachricht gesendet, daß ein user gegangen ist
        if (!isHost)
        {
            if (host.getNick().equals(hostName))
            {
                onPart(user, mapName);
                updateListInGui();
            }
        }
    }


    @Override
    public synchronized void onPart(UserInGame user, String mapName)
    {
        boolean isChanged = userList.removeUser(user);
        if (isChanged)
        {
            updateListInGui();
            if (isHost)
            {
                //wenn ein Spieler das Spiel verlässt, wird das vom
                //Host an alle verteilt
                sendBroadcastGameProgramMessage(
                        ALobbyConstants.TOKEN_USER_XY_HAS_LEFT, user, mapName);
            }
        }
    }
    
    
    @Override
    public synchronized void userHasStartedGame()
    {
        Test.output("Spiel gestartet.");
        setGameState(new GameStateInGame(settlersLobby, userList, user, host), false);
    }

    @Override
    public synchronized void userWantsToJoinGame(String hostName, String mapName, boolean s4game)
    {
        //statewechsel zuvor nicht korrekt, trotzdem weitermachen
        //sende Nachricht an den bisherigen Host, daß man weg ist
        if (!isHost)
        {
            sendGameProgramMessage(ALobbyConstants.TOKEN_LEAVING, host);
        }
        super.userWantsToJoinGame(hostName, mapName, s4game);
    }
    
    @Override
    public synchronized void userWantsToCreateGame(String mapName, boolean s4game)
    {
        //statewechsel zuvor nicht korrekt, trotzdem weitermachen
        
        //sende Nachricht an den bisherigen Host, daß man weg ist
        if (!isHost)
        {
            sendGameProgramMessage(ALobbyConstants.TOKEN_LEAVING, host);
        }
        super.userWantsToCreateGame(mapName, s4game);
    }

    @Override
    public synchronized List<String> getMapUserListGameProgramMessage()
    {
        List<String> result = new ArrayList<String>();
        if (isHost)
        {
            int i = 0;
            int size = userList.getUserList().size();
            List<UserInGame> users = userList.getUserList();
            final String messageBegin = ALobbyConstants.TOKEN_MAP_USER_LIST + ";" + 
                                        user.getNick() + ";" + 
                                        userList.getMapName() + ";";
            //Verschlüsselung und messageBegin bei der maximalen Länge berücksichtigen
            final int maxLen = (450 * 6 / 8) - messageBegin.length();
            while (i < size)
            {
                StringBuilder sb = new StringBuilder();
                for (;i < size; i++)
                {
                	UserInGame user = users.get(i);
                    String nick = user.getNick();
                    //es werden nur die user übermittelt, 
                    //die gültig im Spiel angemeldet sind
                    if (user.isJoining() || (!user.isValid()))
                    {
                    	Test.output("WTF, ignoring user: " + nick);
                        continue;
                    }
                    if (sb.length() > maxLen)
                    {
                    	Test.output("Message too long, ignoring user: " + nick);
                        continue;
                    }
                    Test.output("Adding user: " + nick);
                    sb.append(nick);
                    sb.append(',');
                }
                if (sb.length() > 0)
                {
                    //das letzte Komma löschen
                    sb.deleteCharAt(sb.length() - 1);
                }
                String msg = messageBegin + sb.toString();
                result.add(msg);
            }
        }
        Test.output("sende nachricht: " + result);
        return result;
    }

    @Override
    public synchronized void setIsInMiniChat(String nick, boolean isInMiniChat)
    {
        UserInGame user = userList.getUser(nick);
        if (user != null)
        {
            user.setInChatVerified(isInMiniChat);
            userList.updateUser(user, true);
            updateListInGui();
        }
    }

    @Override
    public boolean isInMiniChat()
    {
        return true;
    }

    


}
