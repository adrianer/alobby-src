/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.controlcenter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.siedler3.alobby.i18n.I18n;

/**
 * Kapselt die mehrsprachige Darstellung von RandomMaps.
 * Da der Kartenname in der Spielinformation übertragen wird,
 * muss sichergestellt werden, daß dort nur ein neutraler
 * Name übertragen wird, der dann in die jeweilige Sprache
 * umgewandelt werden kann.
 *  
 * @author jim
 *
 */
public class S3RandomMap extends S3Map
{

    private String randomPath = ""; //$NON-NLS-1$
    private String size;
    private boolean mirrorXAxis;
    private boolean mirrorYAxis;

    public final static String RANDOM_MAP_SIZE_0 = "384"; //$NON-NLS-1$
    public final static String RANDOM_MAP_SIZE_1 = "448"; //$NON-NLS-1$
    public final static String RANDOM_MAP_SIZE_2 = "512"; //$NON-NLS-1$
    public final static String RANDOM_MAP_SIZE_3 = "576"; //$NON-NLS-1$
    public final static String RANDOM_MAP_SIZE_4 = "640"; //$NON-NLS-1$
    public final static String RANDOM_MAP_SIZE_5 = "704"; //$NON-NLS-1$
    public final static String RANDOM_MAP_SIZE_6 = "768"; //$NON-NLS-1$
    public final static String RANDOM_MAP_MIRROR_0 = I18n.getString("S3RandomMap.REFLECTION_NONE"); //$NON-NLS-1$
    public final static String RANDOM_MAP_MIRROR_1 = I18n.getString("S3RandomMap.REFLECTION_X_AXIS"); //$NON-NLS-1$
    public final static String RANDOM_MAP_MIRROR_2 = I18n.getString("S3RandomMap.REFLECTION_Y_AXIS"); //$NON-NLS-1$
    public final static String RANDOM_MAP_MIRROR_3 = I18n.getString("S3RandomMap.REFLECTION_BOTH"); //$NON-NLS-1$
    
    public static final S3RandomMap[] randomMaps = new S3RandomMap[28];
    static
    {
        addRandomMap(RANDOM_MAP_SIZE_6, 0);
        addRandomMap(RANDOM_MAP_SIZE_5, 4);
        addRandomMap(RANDOM_MAP_SIZE_4, 8);
        addRandomMap(RANDOM_MAP_SIZE_3, 12);
        addRandomMap(RANDOM_MAP_SIZE_2, 16);
        addRandomMap(RANDOM_MAP_SIZE_1, 20);
        addRandomMap(RANDOM_MAP_SIZE_0, 24);
    }

    
    protected S3RandomMap(String size, boolean mirrorXAxis, boolean mirrorYAxis)
    {
        super();
        this.mirrorXAxis = mirrorXAxis;
        this.mirrorYAxis = mirrorYAxis;
        this.size = size;
        isValid = parseSettingsFromString(toString());
    }
    
    /**
     * ermittelt die Random Map Eigenschaften aus dem übergebenen Namen
     * @param input Format: z.B. 768_0_1 oder RANDOM\384_0_0
     */
    public S3RandomMap(String input)
    {
        super();
        isValid = parseSettingsFromString(input);
    }
    

    /**
     * Gibt die RandomMap als intern codierten String zurück.
     * @return die RandomMap als intern codierter String
     */
    @Override
    public String toString()
    {
        return randomPath + size + "_" + encodeBoolean(mirrorXAxis) + "_" + encodeBoolean(mirrorYAxis);  //$NON-NLS-1$ //$NON-NLS-2$
    }
    
    /**
     * Gibt den String zurück, wie er in der GUI angezeigt werden soll.
     * Dabei werden Spracheinstellungen berücksichtigt.
     * @return
     */
    public String toDisplayString()
    {
        String result = randomPath + size + "\\"; //$NON-NLS-1$
        if (isMirrorBoth())
        {
            result += RANDOM_MAP_MIRROR_3; 
        }
        else if (isMirrorXAxis())
        {
            result += RANDOM_MAP_MIRROR_1;
        }
        else if (isMirrorYAxis())
        {
            result += RANDOM_MAP_MIRROR_2;
        }
        else 
        {
            result += RANDOM_MAP_MIRROR_0;   
        }
        return result;
    }
    
    protected boolean parseSettingsFromString(String input)
    {
        Pattern pattern = Pattern.compile(
                // "RANDOM\\" kann drinstehen oder auch nicht.
                // Wenn es drinsteht, muss es erhalten bleiben.
                // Grund ist die unterschiedliche Handhabung von Favoriten
                // und Zufallskarten bei der Mapauswahl.
                // In den Favoriten ist "RANDOM\\" bereits im Namen enthalten
                // und muss auch enthalten bleiben, im Array randomMaps
                // kommt kein "RANDOM\\" vor und darf dort auch nicht erscheinen
                "(" + Pattern.quote(ALobbyConstants.PATH_RANDOM + "\\") + ")?" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                // prüfe auf die erlaubten Kartengroessen
                + "("  //$NON-NLS-1$
                +  RANDOM_MAP_SIZE_0 + "|" +  RANDOM_MAP_SIZE_1 + "|" +  RANDOM_MAP_SIZE_2 + "|" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                +  RANDOM_MAP_SIZE_3 + "|" +  RANDOM_MAP_SIZE_4 + "|" +  RANDOM_MAP_SIZE_5 + "|" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                +  RANDOM_MAP_SIZE_6 + ")" +  //$NON-NLS-1$
                //Spiegelung
                "_([01])_([01])"); //$NON-NLS-1$
        Matcher m = pattern.matcher(input);
        if (m.matches())
        {
            randomPath = m.group(1) != null ? m.group(1) : ""; //$NON-NLS-1$
            size = m.group(2);
            mirrorXAxis = decodeBoolean(m.group(3));
            mirrorYAxis = decodeBoolean(m.group(4));
            return true;
        }
        //keine gültige Kodierung, benutze 768 als default Grösse
        size = RANDOM_MAP_SIZE_6;
        mirrorXAxis = mirrorYAxis = false;
        randomPath = ""; //$NON-NLS-1$
        return false;
    }
    
    private String encodeBoolean(boolean b)
    {
        return b ? "1" : "0"; //$NON-NLS-1$ //$NON-NLS-2$
    }
    
    private boolean decodeBoolean(String input)
    {
        return Integer.parseInt(input) == 1;
    }
    
    
    public String getSize()
    {
        return size;
    }


    public void setSize(String size)
    {
        this.size = size;
    }


    /**
     * 
     * @return true falls nur an der X-Achse gespiegelt wird
     */
    public boolean isMirrorXAxis()
    {
        return mirrorXAxis && (!mirrorYAxis);
    }

    /**
     * 
     * @return true falls nur an der Y-Achse gespiegelt wird
     */
    public boolean isMirrorYAxis()
    {
        return mirrorYAxis && (!mirrorXAxis);
    }


    public boolean isMirrorBoth()
    {
        return mirrorXAxis && mirrorYAxis;
    }
    
    public boolean isMirrorNone()
    {
        return (!mirrorXAxis) && (!mirrorYAxis);
    }
    
    /**
     * Initialisiert 4 Zufallskarten mit der Größe size ab dem index startIndex.
     * 
     * @param size
     *            Die Kartengröße.
     * @param startIndex
     *            Der Index.
     */
    private static void addRandomMap(String size, int startIndex)
    {
        randomMaps[startIndex++] = new S3RandomMap(size, false, false);
        randomMaps[startIndex++] = new S3RandomMap(size, true, false);
        randomMaps[startIndex++] = new S3RandomMap(size, false, true);
        randomMaps[startIndex++] = new S3RandomMap(size, true, true);
    }

    
}
