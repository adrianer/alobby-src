package net.siedler3.alobby.controlcenter;

import java.awt.Color;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Toolkit;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;

import net.siedler3.alobby.gui.GUI;
import net.siedler3.alobby.gui.MessageDialog;
import net.siedler3.alobby.gui.additional.ButtonImage;
import net.siedler3.alobby.gui.additional.RepeatingButtonImage;
import net.siedler3.alobby.i18n.I18n;

/*
 * ALobbyTheme manages the available themes
 * and there different properties (e.g. colors for buttons).
 */
public class ALobbyTheme
{
	
	/**
	 * Enumerated list of all available styles for aLobby. 
	 */
	public enum ALobbyThemes
    {
    	
    	// define names for each available themes
    	// including language-specific title as second parameter
    	// hint: no translate via i18n at this point because ALobbyThemes is loaded before configured language is available
    	//       -> translate in function toString() when the field is displayed
    	// -> classicv1
        classicV1("classicV1", "ALobbyTheme.ALOBBY_THEME_CLASSICV1"),
        // -> classicv2
        classicV2("classicV2", "ALobbyTheme.ALOBBY_THEME_CLASSICV2"),
        // -> BB
        BB("BB", "ALobbyTheme.ALOBBY_THEME_BB"),
    	// -> S3
    	S3("S3", "ALobbyTheme.ALOBBY_THEME_S3"),
    	// -> S4
    	S4("S4", "ALobbyTheme.ALOBBY_THEME_S4"),
    	// -> Christmas-theme (only used 31 days before 31.12. of actual year)
    	CHRISTMAS("CHRISTMAS", "ALobbyTheme.ALOBBY_THEME_CHRISTMAS"),
    	// -> Halloween-theme (only used during Halloween)
    	HALLOWEEN("HALLOWEEN", "ALobbyTheme.ALOBBY_THEME_HALLOWEEN"),
    	// -> Easter-theme (only used around easter)
    	EASTER("EASTER", "ALobbyTheme.ALOBBY_THEME_EASTER"),
    	// -> Pinky-theme
    	PINKY("PINKY", "ALobbyTheme.ALOBBY_THEME_PINKY");
        
    	private final String value;
    	
    	private final String displayName;
    
        private ALobbyThemes(String value, String displayName)
        {
            this.value = value;
            this.displayName = displayName;
        }
        
        /*
         * @return String with human-friendly displayName
         */
        public String getDisplayName() {
        	return displayName;
        }

        /*
         * override java-own toString-function for enumeration
         * and translate displayName-value via i18n as return-value
         * 
         * @see java.lang.Enum#toString()
         * @return String with human-friendly displayName
         */
        @Override
        public String toString()
        {
            return I18n.getString(displayName);
        }
        
        /*
         * @return all available themes as enumeration with its human-unfriendly value
         */
        public static ALobbyThemes getEnum(String value)
        {
            ALobbyThemes result;
            try
            {
                result = valueOf(value);
                if (result != null)
                {
                    return result;
                }
            }
            catch (IllegalArgumentException e)
            {
            }
            
            // build enumeration
            for (ALobbyThemes v : values())
            {
                if (v.value.equalsIgnoreCase(value))
                {
                    return v;
                }
            }
            //no match, use default
            return BB;
        }
    }

    // define variable for theme-colors and images
    public Color backgroundColor;
    public static Color textColor;
    public Color lightTextColor;
    public Color highlightColor;
    public Color importantColor;
    public Color startBackgroundColor;
    public Color startForegroundColor;
    public Color lobbyBackgroundColor;
    public Color selectionColor;
    public Color inputBoxBackgroundColor;
    public Color topicForegroundColor;
    public Color tableheadercolor;
    public String iconAway;
    public String iconOperator;
    public String iconBuddy;
    public String iconIgnored;
    public String iconGame;
    public String iconNoAway;
    public String iconNoOperator;
    public String iconNoBuddy;
    public String iconNoIgnored;
    public String singleChatPaneBackgroundImage = "";
    public String chatBackgroundImage = "";
    public String chatBackgroundImageDirection = "";
    public String buttonRepeatingBackgroundImage = "";
    public String buttonRepeatingBackgroundImageHover = "";
    public String buttonRepeatingBackgroundImageLeft = "";
    public String buttonRepeatingBackgroundImageLeftHover = "";
    public String buttonRepeatingBackgroundImageRight = "";
    public String buttonRepeatingBackgroundImageRightHover = "";
    public String buttonBackgroundImage1 = "";
    public String buttonBackgroundImage1Hover = "";
    public String buttonBackgroundImage2 = "";
    public String buttonBackgroundImage2Hover = "";
    public String buttonBackgroundImage3 = "";
    public String buttonBackgroundImage3Hover = "";
    public String buttonBackgroundImage4 = "";
    public String buttonBackgroundImage4Hover = "";
    public boolean noTransparency = true;
	private Color buttonColor;
    
    /**
     * set colors for each available theme
     * depending on active configured theme
     */
    void configureColorsForSelectedTheme()
    {
    	// get aLobby-configuration
        Configuration config = Configuration.getInstance();
        
        // get active theme as ALobbyThemes-object
        ALobbyThemes theme = config.getTheme();
        
        try {
	        
	        // set color-values for the active theme
	        switch(theme) {
	        	case BB:
	        		backgroundColor = new Color(27, 36, 39);
	        		textColor = new Color(246, 213, 84);
	                setTextColor(textColor);
	                lightTextColor = new Color(246, 213, 84);
	                highlightColor = new Color(246, 213, 84);
	                importantColor = new Color(217, 94, 57);
	                topicForegroundColor = new Color(217, 94, 57);
	                startBackgroundColor = new Color(27, 36, 39);
	                startForegroundColor = new Color(246, 213, 84);
	                lobbyBackgroundColor = new Color(45, 55, 64);
	                selectionColor = new Color(246, 213, 84);
	                inputBoxBackgroundColor = backgroundColor;
	                UIManager.put("TabbedPane.selected", lobbyBackgroundColor);
	                UIManager.put("ProgressBar.background", backgroundColor);
	                UIManager.put("ProgressBar.foreground", getTextColor());
					iconAway = ALobbyConstants.PACKAGE_PATH + "/gui/img/away.png";
					iconOperator = ALobbyConstants.PACKAGE_PATH + "/gui/img/operator.png";
					iconBuddy = ALobbyConstants.PACKAGE_PATH + "/gui/img/buddy.png";
					iconIgnored = ALobbyConstants.PACKAGE_PATH + "/gui/img/ignored.png";
					iconGame = ALobbyConstants.PACKAGE_PATH + "/gui/img/game.png";
					iconNoAway = ALobbyConstants.PACKAGE_PATH + "/gui/img/noaway.png";
					iconNoOperator = ALobbyConstants.PACKAGE_PATH + "/gui/img/nooperator.png";
					iconNoBuddy = ALobbyConstants.PACKAGE_PATH + "/gui/img/nobuddy.png";
					iconNoIgnored = ALobbyConstants.PACKAGE_PATH + "/gui/img/noignored.png";
					buttonColor = new Color(246, 213, 84);
			        UIManager.put("ToolTip.background", textColor); // background-foreground inverted
			        UIManager.put("ToolTip.foreground", backgroundColor); // background-foreground inverted
	        		break;
	        	case classicV1:
	        		backgroundColor = Color.WHITE;
	        		textColor = Color.DARK_GRAY;
                    setTextColor(textColor);
	                lightTextColor = Color.LIGHT_GRAY;
	                highlightColor = new Color(37, 109, 255);
	                importantColor = new Color(217, 94, 57);
	                topicForegroundColor = new Color(217, 94, 57);
	                startBackgroundColor = new Color(59, 78, 111);
	                startForegroundColor = Color.WHITE;
	                lobbyBackgroundColor = UIManager.getColor("Panel.background");
	                selectionColor = new Color(255, 225, 203);
	                inputBoxBackgroundColor = new java.awt.Color(241, 246, 255);
	                iconAway = ALobbyConstants.PACKAGE_PATH + "/gui/img/away.png";
					iconOperator = ALobbyConstants.PACKAGE_PATH + "/gui/img/operator.png";
					iconBuddy = ALobbyConstants.PACKAGE_PATH + "/gui/img/buddy.png";
					iconIgnored = ALobbyConstants.PACKAGE_PATH + "/gui/img/ignored.png";
					iconGame = ALobbyConstants.PACKAGE_PATH + "/gui/img/game.png";
					iconNoAway = ALobbyConstants.PACKAGE_PATH + "/gui/img/noaway.png";
					iconNoOperator = ALobbyConstants.PACKAGE_PATH + "/gui/img/nooperator.png";
					iconNoBuddy = ALobbyConstants.PACKAGE_PATH + "/gui/img/nobuddy.png";
					iconNoIgnored = ALobbyConstants.PACKAGE_PATH + "/gui/img/noignored.png";
					buttonColor = Color.DARK_GRAY;
                    UIManager.put("ToolTip.background", textColor); // background-foreground inverted
                    UIManager.put("ToolTip.foreground", backgroundColor); // background-foreground inverted
	        		break;
	        	case classicV2:
	        		backgroundColor = Color.WHITE;
	                setTextColor(Color.DARK_GRAY);
                    setTextColor(textColor);
	                lightTextColor = Color.LIGHT_GRAY;
	                highlightColor = new Color(37, 109, 255);
	                importantColor = new Color(217, 94, 57);
	                topicForegroundColor = new Color(217, 94, 57); 
	                startBackgroundColor = new Color(59, 78, 111);
	                startForegroundColor = Color.WHITE;
	                lobbyBackgroundColor = UIManager.getColor("Panel.background");
	                selectionColor = new Color(255, 225, 203);
	                inputBoxBackgroundColor = backgroundColor;
	                iconAway = ALobbyConstants.PACKAGE_PATH + "/gui/img/away.png";
					iconOperator = ALobbyConstants.PACKAGE_PATH + "/gui/img/operator.png";
					iconBuddy = ALobbyConstants.PACKAGE_PATH + "/gui/img/buddy.png";
					iconIgnored = ALobbyConstants.PACKAGE_PATH + "/gui/img/ignored.png";
					iconGame = ALobbyConstants.PACKAGE_PATH + "/gui/img/game.png";
					iconNoAway = ALobbyConstants.PACKAGE_PATH + "/gui/img/noaway.png";
					iconNoOperator = ALobbyConstants.PACKAGE_PATH + "/gui/img/nooperator.png";
					iconNoBuddy = ALobbyConstants.PACKAGE_PATH + "/gui/img/nobuddy.png";
					iconNoIgnored = ALobbyConstants.PACKAGE_PATH + "/gui/img/noignored.png";
					buttonColor = Color.DARK_GRAY;
                    UIManager.put("ToolTip.background", textColor); // background-foreground inverted
                    UIManager.put("ToolTip.foreground", backgroundColor); // background-foreground inverted
	        		break;
	        	case S3:
	        		backgroundColor = new Color(27, 36, 39);
                    textColor = new Color(246, 213, 84);
                    setTextColor(textColor);
	                lightTextColor = new Color(246, 213, 84);
	                highlightColor = new Color(246, 213, 84);
	                importantColor = new Color(217, 94, 57);
	                topicForegroundColor = new Color(255,255,255);
	                startBackgroundColor = new Color(27, 36, 39);
	                startForegroundColor = new Color(246, 213, 84);
	                lobbyBackgroundColor = new Color(45, 55, 64);
	                selectionColor = new Color(246, 213, 84);
	                inputBoxBackgroundColor = backgroundColor;
	                UIManager.put("TabbedPane.selected", lobbyBackgroundColor);
	                UIManager.put("ProgressBar.background", backgroundColor);
	                UIManager.put("ProgressBar.foreground", getTextColor());
	                UIManager.put("TextPane.margin", new Insets(2,2,2,2));
	                tableheadercolor = new Color(57,65,73);
					iconAway = ALobbyConstants.PACKAGE_PATH + "/gui/img/away.png";
					iconOperator = ALobbyConstants.PACKAGE_PATH + "/gui/img/operator.png";
					iconBuddy = ALobbyConstants.PACKAGE_PATH + "/gui/img/buddy.png";
					iconIgnored = ALobbyConstants.PACKAGE_PATH + "/gui/img/ignored.png";
					iconGame = ALobbyConstants.PACKAGE_PATH + "/gui/img/game.png";
					iconNoAway = ALobbyConstants.PACKAGE_PATH + "/gui/img/noaway.png";
					iconNoOperator = ALobbyConstants.PACKAGE_PATH + "/gui/img/nooperator.png";
					iconNoBuddy = ALobbyConstants.PACKAGE_PATH + "/gui/img/nobuddy.png";
					iconNoIgnored = ALobbyConstants.PACKAGE_PATH + "/gui/img/noignored.png";
					chatBackgroundImage = ALobbyConstants.PACKAGE_PATH + "/gui/img/gras2.png";
					buttonRepeatingBackgroundImage = ALobbyConstants.PACKAGE_PATH + "/gui/img/bg_button_middle.png";
					buttonRepeatingBackgroundImageHover = ALobbyConstants.PACKAGE_PATH + "/gui/img/bg_button_middle_hover.png";
					buttonRepeatingBackgroundImageLeft = ALobbyConstants.PACKAGE_PATH + "/gui/img/bg_button_left.png";
					buttonRepeatingBackgroundImageLeftHover = ALobbyConstants.PACKAGE_PATH + "/gui/img/bg_button_left_hover.png";
					buttonRepeatingBackgroundImageRight = ALobbyConstants.PACKAGE_PATH + "/gui/img/bg_button_right.png";
					buttonRepeatingBackgroundImageRightHover = ALobbyConstants.PACKAGE_PATH + "/gui/img/bg_button_right_hover.png";
					buttonColor = new Color(246, 213, 84);
                    UIManager.put("ToolTip.background", textColor); // background-foreground inverted
                    UIManager.put("ToolTip.foreground", backgroundColor); // background-foreground inverted
					noTransparency = false;
					break;
	        	case S4:
	        		backgroundColor = new Color(27, 36, 39);
                    textColor = new Color(246, 213, 84);
                    setTextColor(textColor);
	                lightTextColor = new Color(246, 213, 84);
	                highlightColor = new Color(246, 213, 84);
	                importantColor = new Color(217, 94, 57);
	                topicForegroundColor = new Color(255,255,255);
	                startBackgroundColor = new Color(27, 36, 39);
	                startForegroundColor = new Color(246, 213, 84);
	                lobbyBackgroundColor = new Color(45, 55, 64);
	                selectionColor = new Color(246, 213, 84);
	                inputBoxBackgroundColor = backgroundColor;
	                UIManager.put("TabbedPane.selected", lobbyBackgroundColor);
	                UIManager.put("ProgressBar.background", backgroundColor);
	                UIManager.put("ProgressBar.foreground", getTextColor());
	                UIManager.put("TextPane.margin", new Insets(2,2,2,2));
	                tableheadercolor = new Color(57,65,73);
					iconAway = ALobbyConstants.PACKAGE_PATH + "/gui/img/away.png";
					iconOperator = ALobbyConstants.PACKAGE_PATH + "/gui/img/operator.png";
					iconBuddy = ALobbyConstants.PACKAGE_PATH + "/gui/img/buddy.png";
					iconIgnored = ALobbyConstants.PACKAGE_PATH + "/gui/img/ignored.png";
					iconGame = ALobbyConstants.PACKAGE_PATH + "/gui/img/game.png";
					iconNoAway = ALobbyConstants.PACKAGE_PATH + "/gui/img/noaway.png";
					iconNoOperator = ALobbyConstants.PACKAGE_PATH + "/gui/img/nooperator.png";
					iconNoBuddy = ALobbyConstants.PACKAGE_PATH + "/gui/img/nobuddy.png";
					iconNoIgnored = ALobbyConstants.PACKAGE_PATH + "/gui/img/noignored.png";
					chatBackgroundImage = ALobbyConstants.PACKAGE_PATH + "/gui/img/grasS4.jpg";
					buttonRepeatingBackgroundImage = ALobbyConstants.PACKAGE_PATH + "/gui/img/s4_bg_button_middle.png";
					buttonRepeatingBackgroundImageHover = ALobbyConstants.PACKAGE_PATH + "/gui/img/s4_bg_button_middle_hover.png";
					buttonRepeatingBackgroundImageLeft = ALobbyConstants.PACKAGE_PATH + "/gui/img/s4_bg_button_left.png";
					buttonRepeatingBackgroundImageLeftHover = ALobbyConstants.PACKAGE_PATH + "/gui/img/s4_bg_button_left_hover.png";
					buttonRepeatingBackgroundImageRight = ALobbyConstants.PACKAGE_PATH + "/gui/img/s4_bg_button_right.png";
					buttonRepeatingBackgroundImageRightHover = ALobbyConstants.PACKAGE_PATH + "/gui/img/s4_bg_button_right_hover.png";
					buttonColor = new Color(255,255,255);
                    UIManager.put("ToolTip.background", textColor); // background-foreground inverted
                    UIManager.put("ToolTip.foreground", backgroundColor); // background-foreground inverted
					noTransparency = false;
					break;
	        	case CHRISTMAS:
	        		backgroundColor = new Color(16, 21, 23);
                    textColor = new Color(246, 213, 84);
                    setTextColor(textColor);
	                lightTextColor = new Color(246, 213, 84);
	                highlightColor = new Color(246, 213, 84);
	                importantColor = new Color(217, 94, 57);
	                topicForegroundColor = new Color(217, 94, 57);
	                startBackgroundColor = new Color(27, 36, 39);
	                startForegroundColor = new Color(246, 213, 84);
	                lobbyBackgroundColor = new Color(45, 55, 64);
	                selectionColor = new Color(246, 213, 84);
	                inputBoxBackgroundColor = backgroundColor;
	                UIManager.put("TabbedPane.selected", lobbyBackgroundColor);
	                UIManager.put("ProgressBar.background", backgroundColor);
	                UIManager.put("ProgressBar.foreground", getTextColor());
	                UIManager.put("TextPane.margin", new Insets(2,2,2,150));
					iconAway = ALobbyConstants.PACKAGE_PATH + "/gui/img/lutscher.png";
					iconOperator = ALobbyConstants.PACKAGE_PATH + "/gui/img/weihnachtsmuetze.png";
					iconBuddy = ALobbyConstants.PACKAGE_PATH + "/gui/img/buddy.png";
					iconIgnored = ALobbyConstants.PACKAGE_PATH + "/gui/img/ignored.png";
					iconGame = ALobbyConstants.PACKAGE_PATH + "/gui/img/zuckerstab.png";
					iconNoAway = ALobbyConstants.PACKAGE_PATH + "/gui/img/noaway.png";
					iconNoOperator = ALobbyConstants.PACKAGE_PATH + "/gui/img/nooperator.png";
					iconNoBuddy = ALobbyConstants.PACKAGE_PATH + "/gui/img/nobuddy.png";
					iconNoIgnored = ALobbyConstants.PACKAGE_PATH + "/gui/img/noignored.png";
					chatBackgroundImage = ALobbyConstants.PACKAGE_PATH + "/gui/img/sternenhimmel.png";
					// Source for weihnachtsbaum.png: http://www.clipartlord.com/2015/11/10/free-beautiful-cartoon-christmas-tree-clip-art/
					singleChatPaneBackgroundImage = ALobbyConstants.PACKAGE_PATH + "/gui/img/weihnachtsbaum.png";
					// Source for glocke02.png: http://www.clipartlord.com/2016/06/13/free-simple-christmas-bell-clip-art/
					buttonBackgroundImage1 = ALobbyConstants.PACKAGE_PATH + "/gui/img/glocke02.png";
					buttonBackgroundImage1Hover = ALobbyConstants.PACKAGE_PATH + "/gui/img/glocke02hover.png";
					buttonBackgroundImage2 = ALobbyConstants.PACKAGE_PATH + "/gui/img/ball01.png";
					buttonBackgroundImage2Hover = ALobbyConstants.PACKAGE_PATH + "/gui/img/ball01.png";
					buttonBackgroundImage3 = ALobbyConstants.PACKAGE_PATH + "/gui/img/reindeer01.png";
					buttonBackgroundImage3Hover = ALobbyConstants.PACKAGE_PATH + "/gui/img/reindeer01.png";
					buttonBackgroundImage4 = ALobbyConstants.PACKAGE_PATH + "/gui/img/tree01.png";
					buttonBackgroundImage4Hover = ALobbyConstants.PACKAGE_PATH + "/gui/img/tree01.png";
					buttonColor = new Color(246, 213, 84);
                    UIManager.put("ToolTip.background", textColor); // background-foreground inverted
                    UIManager.put("ToolTip.foreground", backgroundColor); // background-foreground inverted
					noTransparency = false;
					break;
	        	case HALLOWEEN:
	        		backgroundColor = new Color(16, 21, 23);
                    textColor = new Color(246, 213, 84);
                    setTextColor(textColor);
	                lightTextColor = new Color(246, 213, 84);
	                highlightColor = new Color(246, 213, 84);
	                importantColor = new Color(217, 94, 57);
	                topicForegroundColor = new Color(217, 94, 57);
	                startBackgroundColor = new Color(27, 36, 39);
	                startForegroundColor = new Color(246, 213, 84);
	                lobbyBackgroundColor = new Color(45, 55, 64);
	                selectionColor = new Color(246, 213, 84);
	                inputBoxBackgroundColor = backgroundColor;
	                UIManager.put("TabbedPane.selected", lobbyBackgroundColor);
	                UIManager.put("ProgressBar.background", backgroundColor);
	                UIManager.put("ProgressBar.foreground", getTextColor());
	                UIManager.put("TextPane.margin", new Insets(2,2,2,150));
	                // Source for schaedel.png: http://cliparts.co/clipart/3745101 - public domain
					iconAway = ALobbyConstants.PACKAGE_PATH + "/gui/img/schaedel.png";
					iconOperator = ALobbyConstants.PACKAGE_PATH + "/gui/img/operator.png";
					iconBuddy = ALobbyConstants.PACKAGE_PATH + "/gui/img/buddy.png";
					iconIgnored = ALobbyConstants.PACKAGE_PATH + "/gui/img/ignored.png";
					// Source for fledermaus.png: http://publicdomainvectors.org/de/kostenlose-vektorgrafiken/Gelbes-Fledermaus-Silhouette-vektor-illustration/14991.html - public domain
					iconGame = ALobbyConstants.PACKAGE_PATH + "/gui/img/fledermaus.png";
					iconNoAway = ALobbyConstants.PACKAGE_PATH + "/gui/img/noaway.png";
					iconNoOperator = ALobbyConstants.PACKAGE_PATH + "/gui/img/nooperator.png";
					iconNoBuddy = ALobbyConstants.PACKAGE_PATH + "/gui/img/nobuddy.png";
					iconNoIgnored = ALobbyConstants.PACKAGE_PATH + "/gui/img/noignored.png";
					chatBackgroundImage = ALobbyConstants.PACKAGE_PATH + "/gui/img/sternenhimmel.png";
					// Source for kuerbis.png: http://www.clipartlord.com/2016/01/11/free-jack-o-lantern-clip-art-4/ - public domain
					buttonBackgroundImage1 = ALobbyConstants.PACKAGE_PATH + "/gui/img/kuerbis.png";
					buttonBackgroundImage1Hover = ALobbyConstants.PACKAGE_PATH + "/gui/img/kuerbis.png";
					buttonBackgroundImage2 = ALobbyConstants.PACKAGE_PATH + "/gui/img/kuerbis.png";
					buttonBackgroundImage2Hover = ALobbyConstants.PACKAGE_PATH + "/gui/img/kuerbis.png";
					buttonBackgroundImage3 = ALobbyConstants.PACKAGE_PATH + "/gui/img/kuerbis.png";
					buttonBackgroundImage3Hover = ALobbyConstants.PACKAGE_PATH + "/gui/img/kuerbis.png";
					buttonBackgroundImage4 = ALobbyConstants.PACKAGE_PATH + "/gui/img/kuerbis.png";
					buttonBackgroundImage4Hover = ALobbyConstants.PACKAGE_PATH + "/gui/img/kuerbis.png";
					buttonColor = new Color(246, 213, 84);
                    UIManager.put("ToolTip.background", textColor); // background-foreground inverted
                    UIManager.put("ToolTip.foreground", backgroundColor); // background-foreground inverted
					noTransparency = false;
					break;
	        	case EASTER:
	        		backgroundColor = new Color(27, 36, 39);
                    textColor = new Color(246, 213, 84);
                    setTextColor(textColor);
	                lightTextColor = new Color(246, 213, 84);
	                highlightColor = new Color(246, 213, 84);
	                importantColor = new Color(217, 94, 57);
	                topicForegroundColor = new Color(255,255,255);
	                startBackgroundColor = new Color(27, 36, 39);
	                startForegroundColor = new Color(246, 213, 84);
	                lobbyBackgroundColor = new Color(45, 55, 64);
	                selectionColor = new Color(246, 213, 84);
	                inputBoxBackgroundColor = backgroundColor;
	                UIManager.put("TabbedPane.selected", lobbyBackgroundColor);
	                UIManager.put("ProgressBar.background", backgroundColor);
	                UIManager.put("ProgressBar.foreground", getTextColor());
	                UIManager.put("TextPane.margin", new Insets(2,2,2,150));
	                // Source for egg_away.png: http://www.clipartlord.com/2016/06/23/free-realistic-egg-clip-art/ - public domain
					iconAway = ALobbyConstants.PACKAGE_PATH + "/gui/img/egg_away.png";
					iconOperator = ALobbyConstants.PACKAGE_PATH + "/gui/img/operator.png";
					iconBuddy = ALobbyConstants.PACKAGE_PATH + "/gui/img/buddy.png";
					iconIgnored = ALobbyConstants.PACKAGE_PATH + "/gui/img/ignored.png";
					// Source for lamb.png: http://www.clipartlord.com/2013/04/18/free-lamb-clip-art/ - public domain
					iconGame = ALobbyConstants.PACKAGE_PATH + "/gui/img/lamb.png";
					iconNoAway = ALobbyConstants.PACKAGE_PATH + "/gui/img/noaway.png";
					iconNoOperator = ALobbyConstants.PACKAGE_PATH + "/gui/img/nooperator.png";
					iconNoBuddy = ALobbyConstants.PACKAGE_PATH + "/gui/img/nobuddy.png";
					iconNoIgnored = ALobbyConstants.PACKAGE_PATH + "/gui/img/noignored.png";
					chatBackgroundImage = ALobbyConstants.PACKAGE_PATH + "/gui/img/gras2.png";
					// Source for bunny-pic: http://www.clipartlord.com/2014/01/09/free-bunny-holding-easter-egg-clip-art/
					singleChatPaneBackgroundImage = ALobbyConstants.PACKAGE_PATH + "/gui/img/bunny.png";
					// Sources for egg-pigs (free to use)
					// -> http://www.clipartlord.com/2013/02/18/free-green-easter-egg-clip-art/
					// -> http://www.clipartlord.com/2013/01/04/free-blue-easter-egg-stars-clip-art/
					// -> http://www.clipartlord.com/2012/12/28/free-easter-egg-clip-art/
					// -> http://www.clipartlord.com/2013/03/18/free-purple-easter-egg-clip-art/
					buttonBackgroundImage1 = ALobbyConstants.PACKAGE_PATH + "/gui/img/egg.png";
					buttonBackgroundImage1Hover = ALobbyConstants.PACKAGE_PATH + "/gui/img/egg.png";
					buttonBackgroundImage2 = ALobbyConstants.PACKAGE_PATH + "/gui/img/egg2.png";
					buttonBackgroundImage2Hover = ALobbyConstants.PACKAGE_PATH + "/gui/img/egg2.png";
					buttonBackgroundImage3 = ALobbyConstants.PACKAGE_PATH + "/gui/img/egg3.png";
					buttonBackgroundImage3Hover = ALobbyConstants.PACKAGE_PATH + "/gui/img/egg3.png";
					buttonBackgroundImage4 = ALobbyConstants.PACKAGE_PATH + "/gui/img/egg4.png";
					buttonBackgroundImage4Hover = ALobbyConstants.PACKAGE_PATH + "/gui/img/egg4.png";
					buttonColor = new Color(246, 213, 84);
                    UIManager.put("ToolTip.background", textColor); // background-foreground inverted
                    UIManager.put("ToolTip.foreground", backgroundColor); // background-foreground inverted
					noTransparency = false;
					break;
	        	case PINKY:
	        		backgroundColor = new Color(143, 40, 132);
                    textColor = new Color(246, 213, 84);
                    setTextColor(textColor);
	                lightTextColor = new Color(246, 213, 84);
	                highlightColor = new Color(246, 213, 84);
	                importantColor = new Color(217, 94, 57);
	                topicForegroundColor = new Color(255,255,255);
	                startBackgroundColor = new Color(27, 36, 39);
	                startForegroundColor = new Color(246, 213, 84);
	                lobbyBackgroundColor = new Color(45, 55, 64);
	                selectionColor = new Color(246, 213, 84);
	                inputBoxBackgroundColor = new Color(27, 36, 39);
	                UIManager.put("TabbedPane.selected", lobbyBackgroundColor);
	                UIManager.put("ProgressBar.background", backgroundColor);
	                UIManager.put("ProgressBar.foreground", getTextColor());
	                UIManager.put("TextPane.margin", new Insets(2,2,2,2));
	                tableheadercolor = new Color(57,65,73);
					iconAway = ALobbyConstants.PACKAGE_PATH + "/gui/img/away.png";
					iconOperator = ALobbyConstants.PACKAGE_PATH + "/gui/img/operator.png";
					iconBuddy = ALobbyConstants.PACKAGE_PATH + "/gui/img/buddy.png";
					iconIgnored = ALobbyConstants.PACKAGE_PATH + "/gui/img/ignored.png";
					iconGame = ALobbyConstants.PACKAGE_PATH + "/gui/img/game.png";
					iconNoAway = ALobbyConstants.PACKAGE_PATH + "/gui/img/noaway.png";
					iconNoOperator = ALobbyConstants.PACKAGE_PATH + "/gui/img/nooperator.png";
					iconNoBuddy = ALobbyConstants.PACKAGE_PATH + "/gui/img/nobuddy.png";
					iconNoIgnored = ALobbyConstants.PACKAGE_PATH + "/gui/img/noignored.png";
					buttonRepeatingBackgroundImage = ALobbyConstants.PACKAGE_PATH + "/gui/img/bg_button_middle.png";
					buttonRepeatingBackgroundImageHover = ALobbyConstants.PACKAGE_PATH + "/gui/img/bg_button_middle_hover.png";
					buttonRepeatingBackgroundImageLeft = ALobbyConstants.PACKAGE_PATH + "/gui/img/bg_button_left.png";
					buttonRepeatingBackgroundImageLeftHover = ALobbyConstants.PACKAGE_PATH + "/gui/img/bg_button_left_hover.png";
					buttonRepeatingBackgroundImageRight = ALobbyConstants.PACKAGE_PATH + "/gui/img/bg_button_right.png";
					buttonRepeatingBackgroundImageRightHover = ALobbyConstants.PACKAGE_PATH + "/gui/img/bg_button_right_hover.png";
					buttonColor = new Color(246, 213, 84);
                    UIManager.put("ToolTip.background", textColor); // background-foreground inverted
                    UIManager.put("ToolTip.foreground", backgroundColor); // background-foreground inverted
					noTransparency = false;
					break;
	        }
	        
	        // set UIManager-values depending on active theme
	        if (!config.isClassicV1Theme())
	        {
	        	// for all theme except classicV1
	            UIManager.put("TabbedPane.selectedForeground", getTextColor());
	            UIManager.put("TabbedPane.background", backgroundColor);
	            UIManager.put("TabbedPane.foreground", getTextColor());
	            UIManager.put("Table.background", backgroundColor);
	            UIManager.put("Table.foreground", getTextColor());
	            UIManager.put("TableHeader.background", backgroundColor);
	            UIManager.put("TableHeader.foreground", getTextColor());
	            UIManager.put("TextArea.background", backgroundColor);
	            UIManager.put("TextArea.foreground", getTextColor());
	            UIManager.put("TextArea.caretForeground", getTextColor());
	            UIManager.put("TextField.background", backgroundColor);
	            UIManager.put("TextField.foreground", getTextColor());
	            UIManager.put("TextField.caretForeground", getTextColor());
	            UIManager.put("TextPane.background", backgroundColor);
	            UIManager.put("TextPane.foreground", getTextColor());
	            UIManager.put("CheckBox.background", backgroundColor);
	            UIManager.put("CheckBox.foreground", getTextColor());
	            UIManager.put("ComboBox.background", backgroundColor);
	            UIManager.put("ComboBox.foreground", getTextColor());
	            UIManager.put("OptionPane.background", backgroundColor);
	            UIManager.put("OptionPane.foreground", getTextColor());
	            UIManager.put("OptionPane.messageForeground", getTextColor());
	            UIManager.put("PasswordField.background", backgroundColor);
	            UIManager.put("PasswordField.foreground", getTextColor());
	            UIManager.put("PasswordField.caretForeground", getTextColor());
	            UIManager.put("Button.background", backgroundColor);
	            UIManager.put("Button.foreground", getTextColor());
	            UIManager.put("ScrollBar.background", backgroundColor);
	            UIManager.put("ScrollPane.background", backgroundColor);
	            UIManager.put("ScrollPane.foreground", getTextColor());
	            UIManager.put("Label.background", backgroundColor);
	            UIManager.put("Label.foreground", getTextColor());
	            UIManager.put("List.background", backgroundColor);
	            UIManager.put("List.foreground", getTextColor());
	            UIManager.put("Panel.background", backgroundColor);
	            UIManager.put("Panel.foreground", getTextColor());
	            UIManager.put("RadioButton.background", backgroundColor);
	            UIManager.put("RadioButton.foreground", getTextColor());
	        }
	        else 
	        {
	        	// for classicV1
	            UIManager.put("ComboBox.background", inputBoxBackgroundColor); //$NON-NLS-1$
	            UIManager.put("ProgressBar.background", inputBoxBackgroundColor); //$NON-NLS-1$
	            UIManager.put("TextField.background", inputBoxBackgroundColor); //$NON-NLS-1$
	            UIManager.put("TextArea.background", inputBoxBackgroundColor); //$NON-NLS-1$
	            
	            UIManager.put("OptionPane.background", backgroundColor);
	            UIManager.put("Panel.background", backgroundColor);
	    
	            UIManager.put("CheckBox.background", backgroundColor);
	            UIManager.put("CheckBox.foreground", getTextColor());
	            UIManager.put("RadioButton.background", backgroundColor);
	            UIManager.put("RadioButton.foreground", getTextColor());
	    
	        }

            UIManager.put("ToolTip.font", new Font("Dialog", Font.PLAIN, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("Label.font", new Font("Dialog", Font.BOLD, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("Button.font", new Font("Dialog", Font.BOLD, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("CheckBox.font", new Font("Dialog", Font.BOLD, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("CheckBoxMenuItem.font", new Font("Dialog", Font.BOLD, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("ComboBox.font", new Font("Dialog", Font.BOLD, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("DesktopIcon.font", new Font("Dialog", Font.PLAIN, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("EditorPane.font", new Font("Dialog", Font.PLAIN, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("FormattedTextField.font", new Font("Dialog", Font.PLAIN, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("InternalFrame.titleFont", new Font("Dialog", Font.PLAIN, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("List.font", new Font("Dialog", Font.PLAIN, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("Menu.font", new Font("Dialog", Font.PLAIN, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("MenuBar.font", new Font("Dialog", Font.PLAIN, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("MenuItem.font", new Font("Dialog", Font.PLAIN, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("OptionPane.font", new Font("Dialog", Font.BOLD, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("Panel.font", new Font("Dialog", Font.PLAIN, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("PasswordField.font", new Font("Dialog", Font.PLAIN, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("PopupMenu.font", new Font("Dialog", Font.PLAIN, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("ProgressBar.font", new Font("Dialog", Font.PLAIN, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("RadioButton.font", new Font("Dialog", Font.BOLD, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("RadioButtonMenuItem.font", new Font("Dialog", Font.BOLD, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("ScrollPane.font", new Font("Dialog", Font.PLAIN, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("Slider.font", new Font("Dialog", Font.PLAIN, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("Spinner.font", new Font("Dialog", Font.PLAIN, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("TabbedPane.font", new Font("Dialog", Font.PLAIN, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("Table.font", new Font("Dialog", Font.PLAIN, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("TableHeader.font", new Font("Dialog", Font.PLAIN, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("TextArea.font", new Font("Dialog", Font.PLAIN, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("TextField.font", new Font("Dialog", Font.PLAIN, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("TextPane.font", new Font("Dialog", Font.PLAIN, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("TitledBorder.font", new Font("Dialog", Font.BOLD, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("ToggleButton.font", new Font("Dialog", Font.BOLD, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("ToolBar.font", new Font("Dialog", Font.PLAIN, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("Tree.font", new Font("Dialog", Font.PLAIN, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("Viewport.font", new Font("Dialog", Font.PLAIN, (int) (12 * MessageDialog.getDpiScalingFactor())));
            UIManager.put("Scrollbar.shadow", new ColorUIResource(246, 213, 84));
            UIManager.put("Scrollbar.thumb", new ColorUIResource(246, 213, 84));
            UIManager.put("Scrollbar.thumbShadow", new ColorUIResource(246, 213, 84));
        } catch (Exception e) {
            // error loading theme-configurations
        }
    }
    
    /**
     *  creates a button including repeating backgroundImages
     *  depending on actual theme.
     *  
     *  @param text			button-text
     *  @return JButton		the generated button			
     */
    public JButton getButtonWithStyle( String text ) {
    	    	
    	try {
    		// only generate button 
    		// if the actual theme supports repeating backgroundImages for buttons
    		if( this.isRepeatingButtonStyleAvailable() ) {
    			JButton button = new RepeatingButtonImage(
					text, 
					ImageIO.read(GUI.class.getResourceAsStream(this.buttonRepeatingBackgroundImage)),
					ImageIO.read(GUI.class.getResourceAsStream(this.buttonRepeatingBackgroundImageHover))
				);
    			if( this.buttonRepeatingBackgroundImageLeft.length() > 0 && 
    				this.buttonRepeatingBackgroundImageLeftHover.length() > 0 && 
    				this.buttonRepeatingBackgroundImageRight.length() > 0 && 
    				this.buttonRepeatingBackgroundImageRightHover.length() > 0 
    			) {
					button = new RepeatingButtonImage(
						text, 
						ImageIO.read(GUI.class.getResourceAsStream(this.buttonRepeatingBackgroundImage)),
						ImageIO.read(GUI.class.getResourceAsStream(this.buttonRepeatingBackgroundImageHover)),
						ImageIO.read(GUI.class.getResourceAsStream(this.buttonRepeatingBackgroundImageLeft)),
						ImageIO.read(GUI.class.getResourceAsStream(this.buttonRepeatingBackgroundImageLeftHover)),
						ImageIO.read(GUI.class.getResourceAsStream(this.buttonRepeatingBackgroundImageRight)),
						ImageIO.read(GUI.class.getResourceAsStream(this.buttonRepeatingBackgroundImageRightHover))
					);
    			}
    			button.setForeground(this.buttonColor);
    			button.setBorderPainted(false);
    			
    			// return the created button
    			return button;
    		} else {
	    		// only generate button 
	    		// if the actual theme supports backgroundImages for buttons
	    		if( this.isButtonStyleAvailable() ) {
	    			JButton button = new ButtonImage(
						text, 
						ImageIO.read(GUI.class.getResourceAsStream(this.buttonBackgroundImage1)),
						ImageIO.read(GUI.class.getResourceAsStream(this.buttonBackgroundImage1Hover))
					);
	    			button.setForeground(this.buttonColor);
	    			
	    			// return the created button
	    			return button;
	    		}
    		}
    		
    	}
    	catch(IOException e) {
    		
    	}
		return null;
    }
    
    /**
     *  creates a button including repeating backgroundImages
     *  depending on actual theme.
     *  
     *  @param text			button-text
     *  @return JButton		the generated button			
     */
    public JButton getButtonWithStyle( String text, Font font ) {
    	    	
    	try {
    		// only generate button 
    		// if the actual theme supports repeating backgroundImages for buttons
    		if( this.isRepeatingButtonStyleAvailable() ) {
    			JButton button = new RepeatingButtonImage(
					text, 
					ImageIO.read(GUI.class.getResourceAsStream(this.buttonRepeatingBackgroundImage)),
					ImageIO.read(GUI.class.getResourceAsStream(this.buttonRepeatingBackgroundImageHover))
				);
    			if( this.buttonRepeatingBackgroundImageLeft.length() > 0 && 
    				this.buttonRepeatingBackgroundImageLeftHover.length() > 0 && 
    				this.buttonRepeatingBackgroundImageRight.length() > 0 && 
    				this.buttonRepeatingBackgroundImageRightHover.length() > 0 
    			) {
					button = new RepeatingButtonImage(
						text, 
						ImageIO.read(GUI.class.getResourceAsStream(this.buttonRepeatingBackgroundImage)),
						ImageIO.read(GUI.class.getResourceAsStream(this.buttonRepeatingBackgroundImageHover)),
						ImageIO.read(GUI.class.getResourceAsStream(this.buttonRepeatingBackgroundImageLeft)),
						ImageIO.read(GUI.class.getResourceAsStream(this.buttonRepeatingBackgroundImageLeftHover)),
						ImageIO.read(GUI.class.getResourceAsStream(this.buttonRepeatingBackgroundImageRight)),
						ImageIO.read(GUI.class.getResourceAsStream(this.buttonRepeatingBackgroundImageRightHover))
					);
    			}
    			button.setBorderPainted(false);
    			button.setFont(font);
    			// return the created button
    			return button;
    		} else {
	    		// only generate button 
	    		// if the actual theme supports backgroundImages for buttons
	    		if( this.isButtonStyleAvailable() ) {
	    			JButton button = new ButtonImage(
						text, 
						ImageIO.read(GUI.class.getResourceAsStream(this.buttonBackgroundImage1)),
						ImageIO.read(GUI.class.getResourceAsStream(this.buttonBackgroundImage1Hover))
					);
	    			button.setFont(font);
	    			// return the created button
	    			return button;
	    		}
    		}
    		
    	}
    	catch(IOException e) {
    		
    	}
		return null;
    }
    
    /**
     *  creates a button including repeating backgroundImages
     *  depending on actual theme.
     *  
     *  @param text			button-text
     *  @param image		image to display
     *  @param imageHover	image to display at mouseenter
     *  @return JButton		the generated button			
     */
    public JButton getButtonWithStyle( String text, String image, String imageHover ) {
    	    	
    	try {
			// only generate button 
			// if the actual theme supports backgroundImages for buttons
			if( this.isButtonStyleAvailable() && image.length() > 0 && imageHover.length() > 0 ) {
				JButton button = new ButtonImage(
					text, 
					ImageIO.read(GUI.class.getResourceAsStream(image)),
					ImageIO.read(GUI.class.getResourceAsStream(imageHover))
				);
				
				// return the created button
				return button;
			}
    	}
    	catch(IOException e) {
    		
    	}
		return null;
    }
    
    /**
     * check if a button with repeating background-Images is available
     * 
     *  @return	boolean		true if the theme allows to generate buttons with background-images
     */
    public boolean isRepeatingButtonStyleAvailable() {
    	if( 
    		this.buttonRepeatingBackgroundImage.length() > 0 && 
    		this.buttonRepeatingBackgroundImageHover.length() > 0
    	)
    	{
    		return true;
    	}
    	return false;
    }
    
    /**
     * check if a button with background-Images is available
     * 
     *  @return	boolean		true if the theme allows to generate buttons with background-images
     */
    public boolean isButtonStyleAvailable() {
    	if( 
    		(
    			this.buttonBackgroundImage1.length() > 0 && 
    			this.buttonBackgroundImage1Hover.length() > 0
    		) || 
    		(
    			this.buttonBackgroundImage2.length() > 0 && 
    			this.buttonBackgroundImage2Hover.length() > 0
    		) ||
    		(
    			this.buttonBackgroundImage3.length() > 0 && 
    			this.buttonBackgroundImage3Hover.length() > 0
    		) ||
    		(
    			this.buttonBackgroundImage4.length() > 0 && 
    			this.buttonBackgroundImage4Hover.length() > 0
    		) 
    	)
    	{
    		return true;
    	}
    	return false;
    }

    /**
     * Get textColor-property of actual style
     * 
     * @return Color
     */
	public Color getTextColor() {
		if( textColor == null ) {
			this.configureColorsForSelectedTheme();
		}
		return textColor;
	}
	
	/**
     * Get textColor-property of actual style
     * 
     * @return Color
     */
	public static Color getStaticTextColor() {
		return textColor;
	}

	/**
	 * Set textColor-property of actual style
	 */
	public static void setTextColor(Color textColor) {
		ALobbyTheme.textColor = textColor;
	}
    
}
