/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.controlcenter;

import java.util.Locale;

import net.siedler3.alobby.communication.IRCCommunicator;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.i18n.I18nAwayMsg;

public abstract class AwayStatus
{
	public final static String MESSAGE_AWAY_DEFAULT = I18n.getString("AwayStatus.AWAY_DEFAULT");  //$NON-NLS-1$
	public static String MESSAGE_AWAY_TIMER = I18n.getString("AwayStatus.AWAY_TIMER");  //$NON-NLS-1$
	public static String MESSAGE_AWAY_IN_GAME = I18nAwayMsg.getString("AwayStatus.AWAY_IN_GAME"); //$NON-NLS-1$

	//für alle unterstützten Sprachen muss hier die
	//away in game message ausgelesen werden
	public static String[] ALL_MESSAGES_AWAY_IN_GAME = {
		I18nAwayMsg.getString("AwayStatus.AWAY_IN_GAME", Locale.GERMAN),
		I18nAwayMsg.getString("AwayStatus.AWAY_IN_GAME", Locale.ENGLISH),
		I18nAwayMsg.getString("AwayStatus.AWAY_IN_GAME", new Locale("pl", "PL")),
		I18nAwayMsg.getString("AwayStatus.AWAY_IN_GAME_CHRISTMAS", Locale.GERMAN),
	    I18nAwayMsg.getString("AwayStatus.AWAY_IN_GAME_CHRISTMAS", Locale.ENGLISH),
	    I18nAwayMsg.getString("AwayStatus.AWAY_IN_GAME_CHRISTMAS", new Locale("pl", "PL"))
	};


	protected SettlersLobby settlersLobby;
	protected IRCCommunicator ircCommunicator;
	protected boolean isUserAway;

	protected AwayStatus(SettlersLobby settlersLobby, IRCCommunicator ircCommunicator, boolean isUserAway)
	{
		this.settlersLobby = settlersLobby;
		this.ircCommunicator = ircCommunicator;
		this.isUserAway = isUserAway;
		if( this.settlersLobby.config.isChristmasTheme() ) {
			MESSAGE_AWAY_TIMER = I18n.getString("AwayStatus.AWAY_TIMER_CHRISTMAS");
			MESSAGE_AWAY_IN_GAME = I18nAwayMsg.getString("AwayStatus.AWAY_IN_GAME_CHRISTMAS");
		}
	}

	protected void userWasActive()
	{
		// tue nichts
	}

	protected void doAway(String msg)
	{
		//wenn msg leer, nehme default string
        if (msg == null || msg.isEmpty())
        {
            msg = settlersLobby.getDefaultAwayMsg();
        }
        if (msg == null || msg.isEmpty())
        {
            msg = MESSAGE_AWAY_DEFAULT;
            //wäre msg leer, würde der away status wieder zurückgesetzt.
            //daher als letzte Möglichkeit einen fixen Wert nehmen.
        }
        ircCommunicator.sendAway(msg);
        settlersLobby.onCommandAnswer(I18n.getString("AwayStatus.MSG_AWAY_ON") + msg); //$NON-NLS-1$
        //die away programm nachricht schicken
        settlersLobby.sendProgramMessageAwayMessage(msg);
	}

	protected boolean isUserAway()
	{
		return isUserAway;
	}

	protected void doBack()
	{
		if (isUserAway)
    	{
			isUserAway = false;
	    	settlersLobby.onCommandAnswer(I18n.getString("AwayStatus.MSG_AWAY_OFF")); //$NON-NLS-1$
    	}
    	//immer senden, egal ob away oder nicht
    	//ein leeres AWAY an den Irc Server setzt den AWAY status zurück
    	ircCommunicator.sendAway(""); //$NON-NLS-1$

        //die away_back programm nachricht schicken
    	settlersLobby.sendProgramMessageAwayBack();
	}

	public void onGameStarted()
	{
		// tue nichts
	}

	public void onGameClosed()
	{
		// tue nichts
	}

	protected void destroy()
	{
		// tue nichts
	}

	protected void doIdleAway()
	{
		// tue nichts
	}

	/**
	 * Prueft, ob die übergebene Zeichenkette eine solche ist, die darauf
	 * hinweist, dass der User im Spiel ist.
	 * @param awayMsg Die zu prüfende Zeichenkette.
	 * @return true, wenn der User im Spiel ist, andernfalls false.
	 */
	public static boolean isInGameMessage(String awayMsg) {
	    boolean result = false;
	    //iteriere über die AWAY_IN_GAME Nachricht in allen Sprachen
	    for (String msg : ALL_MESSAGES_AWAY_IN_GAME)
	    {
	        if (awayMsg.equals(msg))
	        {
	            result = true;
	            break;
	        }
	    }
	    return result;
	}
}
