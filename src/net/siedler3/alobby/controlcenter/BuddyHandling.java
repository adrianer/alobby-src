package net.siedler3.alobby.controlcenter;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.util.CryptoSupport;

public class BuddyHandling
{
    /**
     * Importiert die Buddys und ignorierten Users des aktuell in der
     * Config gespeicherten Users.
     * @throws Exception
     * @ Wenn die Verbindung nicht zustande kommt oder
     * das Resultat nicht dem erwartetem entspricht.
     */
    public static void importBuddyIgnore() throws Exception  {
        Configuration config = Configuration.getInstance();
         // Daten bauen
        String data = URLEncoder.encode("action", "UTF-8") + "=" + URLEncoder.encode("import", "UTF-8"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
        data += "&" + URLEncoder.encode("user", "UTF-8") + "=" + URLEncoder.encode(config.getUserName(), "UTF-8"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
        data += "&" + URLEncoder.encode("pw", "UTF-8") + "=" + URLEncoder.encode(CryptoSupport.md5("aLobby" + config.getPassword()), "UTF-8"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$

        // Daten senden
        URL url = new URL(config.getBuddyIgnoreUrl());
        URLConnection conn = url.openConnection();
        conn.setDoOutput(true);
        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
        wr.write(data);
        wr.flush();
        wr.close();

        // Antwort auswerten
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));

        String line = rd.readLine();
        rd.close();
        if (line == null || !Pattern.compile("(([a-zA-Z0-9]|_)*;)*/(([a-zA-Z0-9]|_)*;)*").matcher(line).matches()) { //$NON-NLS-1$
            throw new Exception(I18n.getString("SettlersLobby.ERROR_WRONG_SERVER_RESPONSE")); //$NON-NLS-1$
        }

        StringTokenizer listTrenner = new StringTokenizer(line, "/"); //$NON-NLS-1$
        if (!line.startsWith("/")) { //$NON-NLS-1$
            ArrayList<String> buddys = new ArrayList<String>();
            StringTokenizer buddyTrenner = new StringTokenizer(listTrenner.nextToken(), ";"); //$NON-NLS-1$
            while (buddyTrenner.hasMoreTokens()) {
                buddys.add(buddyTrenner.nextToken());
            }
            config.setBuddys(buddys);
        }
        if (listTrenner.hasMoreTokens()) {
            ArrayList<String> ignored = new ArrayList<String>();
            StringTokenizer ignoredTrenner = new StringTokenizer(listTrenner.nextToken(), ";"); //$NON-NLS-1$
            while (ignoredTrenner.hasMoreTokens()) {
                ignored.add(ignoredTrenner.nextToken());
            }
            config.setIgnoredUsers(ignored);
        }
    }

    /**
     * Exportiert die Buddys und ignorierten User des aktuell in der
     * Config gespeicherten Users.
     * @throws Exception
     * @ Wenn die Verbindung nicht zustande kommt oder
     * das Resultat nicht dem erwartetem entspricht.
     */
    public static void exportBuddyIgnore() throws Exception  {
        Configuration config = Configuration.getInstance();
        // Daten zusammbauen
        String data = URLEncoder.encode("action", "UTF-8") + "=" + URLEncoder.encode("export", "UTF-8"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
        data += "&" + URLEncoder.encode("user", "UTF-8") + "=" + URLEncoder.encode(config.getUserName(), "UTF-8"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
        data += "&" + URLEncoder.encode("pw", "UTF-8") + "=" + URLEncoder.encode(CryptoSupport.md5("aLobby" + config.getPassword()), "UTF-8"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
        List<String> buddys = config.getBuddys();
        if (buddys != null)
        {
            for (int i = 0; i < buddys.size(); i++) {
                data += "&" + URLEncoder.encode("buddys[" + Integer.toString(i) + "]", "UTF-8") + "=" + URLEncoder.encode(buddys.get(i), "UTF-8"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
            }
        }
        List<String> ignore = config.getIgnoredUsers();
        if (ignore != null)
        {
            for (int i = 0; i < ignore.size(); i++) {
                data += "&" + URLEncoder.encode("ignore[" + Integer.toString(i) + "]", "UTF-8") + "=" + URLEncoder.encode(ignore.get(i), "UTF-8"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
            }
        }
        if (buddys == null && ignore == null)
        {
            //if there are no user in any list, abort
            return;
        }

        // Daten Senden
        URL url = new URL(config.getBuddyIgnoreUrl());
        URLConnection conn = url.openConnection();
        conn.setDoOutput(true);
        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
        wr.write(data);
        wr.flush();
        wr.close();

        // Antwort auswerten
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line = rd.readLine();
        rd.close();
        if (line == null || !line.equals("ok")) { //$NON-NLS-1$
            throw new Exception(I18n.getString("SettlersLobby.ERROR_WRONG_SERVER_RESPONSE")); //$NON-NLS-1$
        }
    }

}
