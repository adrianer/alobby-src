/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.controlcenter;

import net.siedler3.alobby.communication.IRCCommunicator;

public class AwayStatusAutomatic extends AwayStatus
{
	private AwayByAbstinence awayByAbstinence;
	
	protected AwayStatusAutomatic(SettlersLobby settlersLobby, IRCCommunicator ircCommunicator, boolean isUserAway)
	{
		super(settlersLobby, ircCommunicator, isUserAway);
		if (!isUserAway)
		{
			awayByAbstinence = new AwayByAbstinence(settlersLobby.config, this);
		}
	}
	
	protected void userWasActive()
	{
		if (awayByAbstinence != null)
		{
			awayByAbstinence.userWasActive();
		} else {
			super.doBack();
			awayByAbstinence = new AwayByAbstinence(settlersLobby.config, this);
		}	
	}
		
	protected void doIdleAway()
	{
		super.doAway(MESSAGE_AWAY_TIMER);	
		awayByAbstinence = null;
		isUserAway = true;
	}
	
	protected void doAway(String msg)
	{
		super.doAway(msg);
		awayByAbstinence.interrupt();
		settlersLobby.setAwayStatus(new AwayStatusManual(settlersLobby, ircCommunicator, true));
	}
	
	protected void doBack()
	{
		// tue nichts, wird durch userWasActive() erledigt
	}

	public void onGameStarted()
	{
		if (settlersLobby.isActivateAwayOnGameStart())
		{
			settlersLobby.setAwayStatus(new AwayStatusInGame(settlersLobby, ircCommunicator, isUserAway));
		}
	}
	
	protected void destroy()
	{
		if (awayByAbstinence != null)
		{
			awayByAbstinence.interrupt();
		}
	}	
}
