/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.controlcenter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import net.siedler3.alobby.util.S3MapData;

public class S3Map implements ISMap
{
    private String mapPath;
    private SettlersLobby settlersLobby;
    private int player = 0;
    private int widthHeight = 0;
    
    protected boolean isValid;
    
    public S3Map() {
    	mapPath = null;
        isValid = false;
	}
    
    protected S3Map( SettlersLobby settlersLobby )
    {
        mapPath = null;
        isValid = false;
    }
    
    private S3Map(String mapPath)
    {
        this.mapPath = mapPath;
        isValid = mapPath.startsWith(ALobbyConstants.PATH_S3MULTI) ||
            mapPath.startsWith(ALobbyConstants.PATH_USER);
    }
    
    private S3Map(String mapPath, SettlersLobby settlersLobby)
    {
        this.mapPath = mapPath;
        this.settlersLobby = settlersLobby;
        getMapData();
        isValid = mapPath.startsWith(ALobbyConstants.PATH_S3MULTI) ||
            mapPath.startsWith(ALobbyConstants.PATH_USER);
    }

	public String toDisplayString()
    {
        return toString();
    }
    
    public String toString()
    {
        return mapPath;
    }
    
    /**
     * Liefert true, falls die Karte in einem korrekten Format angegeben ist.
     * Es wird nicht geprüft, ob die Mapdatei tatsächlich existiert.
     * 
     * @return true, falls die Karte in einem korrekten Format angegeben ist.
     */
    public boolean isValid()
    {
        return isValid;
    }
    
    public static ISMap newInstance(String mapPath)
    {
        if (mapPath.startsWith(ALobbyConstants.PATH_RANDOM))
        {
            return new S3RandomMap(mapPath);
        }
        return new S3Map(mapPath);
    }
    
    public static ISMap newInstance(String mapPath, SettlersLobby settlersLobby)
    {
        if (mapPath.startsWith(ALobbyConstants.PATH_RANDOM))
        {
            return new S3RandomMap(mapPath);
        }
        return new S3Map(mapPath, settlersLobby);
    }
    
    public static String getDisplayName(String mapPath, SettlersLobby settlersLobby)
    {
        ISMap map = newInstance(mapPath, settlersLobby);
        if (map.isValid())
        {
            return map.toDisplayString();
        }
        //eventuell ein savgame, einfach den Wert zurückgeben
        return mapPath;
    }
    
    public static String getDisplayName(String mapPath)
    {
    	ISMap map = newInstance(mapPath);
        if (map.isValid())
        {
            return map.toDisplayString();
        }
        //eventuell ein savgame, einfach den Wert zurückgeben
        return mapPath;
    }
    
    /**
     * Load map-data like player-count and map-size.
     */
    private void getMapData() {
		if( !this.mapPath.contains(ALobbyConstants.PATH_RANDOM) && this.settlersLobby instanceof SettlersLobby ) {
			// get the mapData for this map from local file
			S3MapData mapData = new S3MapData();
			try {
				mapData.S3MapDataContentReader(new FileInputStream(settlersLobby.getS3MapDirectory().toString()  + File.separator + this.mapPath.toString()));
				mapData.loadMapResources();
				mapData.readBasicMapInformation();
				this.widthHeight = mapData.getWidthHeight();
				this.player = mapData.getS3MapDataContent().getPlayerCount();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			mapData = null;
			System.gc();		
		}
    }

	@Override
	public int getPlayer() {
		return this.player;
	}

	@Override
	public int getWidthHeight() {
		return this.widthHeight;
	}
}
