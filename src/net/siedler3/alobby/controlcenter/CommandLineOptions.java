package net.siedler3.alobby.controlcenter;

/**
 * Define command line-options which could be used
 * to manipulate alobby-settings.
 */

public class CommandLineOptions
{
    private String optionInstaller = null;
    private String optionConfigDir = null;
    private String optionNoDxDebug = null;
    private String optionNoBetaHint = null;
    private String[] args;
    private boolean valid = false;
	private String runInEclipse = null;

    public CommandLineOptions(String[] args)
    {
        this.args = args;
        valid = parse();
    }

    /**
     * Parse the command line-arguments.
     * @return
     */
    private boolean parse()
    {
        boolean result = true;
        try
        {
            for (int i = 0; i < args.length; ++i)
            {
                if ("--installer".equals(args[i]) || "/installer".equals(args[i]))
                {
                    optionInstaller = args[i+1];
                    ++i;
                }
                else if("--configDir".equals(args[i]) || "/configDir".equals(args[i]))
                {
                    optionConfigDir = args[i+1];
                    ++i;
                }
                else if("--nodxdebug".equals(args[i]) || "/nodxdebug".equals(args[i]))
                {
                    optionNoDxDebug = "disable";
                }
                else if("--nobetahint".equals(args[i]) || "/nobetahint".equals(args[i]))
                {
                    optionNoBetaHint = "disable";
                }
                else if("--runInEclipse".equals(args[i]) || "/runInEclipse".equals(args[i]))
                {
                	runInEclipse  = "disable";
                }
                else
                {
                    result = false;
                }
            }
        }
        catch (Exception e)
        {
            result = false;
            Test.outputException(e);
        }
        return result;
    }

    /**
     * Return if the commandline-options where valid.
     * 
     * @return boolean true/false
     */
    public boolean isValid()
    {
        return valid;
    }

    /**
     * Return the via commandline configured installdir
     * where e.g. vpn-client will be located.
     * 
     * @return String the directory
     */
    public String getInstallerDir()
    {
        return optionInstaller;
    }

    /**
     * Return the via commandline configured configdir
     * where e.g. the logfiles will be located.
     * 
     * @return String the directory
     */
    public String getConfigDir()
    {
        return optionConfigDir;
    }
    
    /**
     * Return if the dxdebug on startup should be disabled.
     * 
     * @return true if it should be disabled
     */
    public boolean disableDxDebug()
    {
        return optionNoDxDebug == null ? false : true;
    }
    
    /**
     * Return if the betahint on startup should be disabled.
     * 
     * @return true if it should be disabled
     */
    public boolean disableBetaHint()
    {
        return optionNoBetaHint == null ? false : true;
    }
    
    /**
     * Return if this App is running from eclipse or not
     * The value has to be set in eclipse > debug configuration > settlerslobby > arguments. 
     */
    public boolean isRunInEcplise() {
    	return this.runInEclipse == null ? false : true;
    }
}
