/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.controlcenter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Chatlog implements IMessageListener
{
    private File logFile;
    private File chatlogDir;
	private String fileName;
	private boolean logging = false;
	private PrintWriter logWriter;
	private SettlersLobby settlersLobby;
	
    public Chatlog(SettlersLobby settlersLobby)
    {
    	this.settlersLobby = settlersLobby;
    }
    
	public void startLogging()
	{
		chatlogDir = new File(SettlersLobby.configDir, "Chatlog");
		setFileName();
        logFile = new File(fileName);  
        
        if (!chatlogDir.exists())
        {
            chatlogDir.mkdir();	
        }            
	    try
	    {    	
	        logFile.createNewFile();
	        logWriter = new PrintWriter(new FileWriter(logFile));
	        logWriter.println("***************************");
			logWriter.println("Chatbeginn :" + getCurrentDate());
			logWriter.println("***************************");
			logWriter.flush();			
	    }
	    catch(IOException io)
	    {
	        System.out.println("Konnte Datei nicht erstellen");	
	        return;
	    }
	    logging = true;
	    settlersLobby.setLogChat(logging);
	    addAsMessageListener(settlersLobby.getMessageHandler()); 
	}
	
	public void stopLogging()
	{
		removeAsMessageListener(settlersLobby.getMessageHandler());
		logWriter.println("***************************");
		logWriter.println("Chatende :" + getCurrentDate());
		logWriter.print  ("***************************");
		logWriter.flush();
		logging = false;
		logWriter.close();	    
	}
		
	public boolean isLogging()
	{
		return logging;
	}
	
	public void setLogging(boolean checked)
	{
		if (checked)
		{
			if (!settlersLobby.getChatlog().isLogging())
			{
				settlersLobby.getChatlog().startLogging();
			}
	    }
	    else 
	    {
	    	if (settlersLobby.getChatlog().isLogging())
	    	{
	    		settlersLobby.getChatlog().stopLogging();
	    	}
	    }    	
	}
	
	public void addChatMessage(String user, String message, boolean bold)
	{
		// ignore message if it contains a screenshot-url
		// to prevent analyzing of posted screenshot-urls via log.txt
		if( !message.contains(settlersLobby.config.getScreenUploaderDomain()) ) {
			// "bold" wird fürs Logfile ignoriert
			logWriter.println(getCurrentTime() + " " + user + ": " + message);
			logWriter.flush();
		}
	}
	
	public void addServerMessage(String serverMessage)
	{
		logWriter.println("Server: " + serverMessage);
		logWriter.flush();
	}
	
	public void addNoticeMessage(String sender, String receiver, String message)
	{
		logWriter.print(getCurrentTime() + " " + sender + " [meldet");
		if (!receiver.equals(""))
		{
			logWriter.print(" " + receiver);
		}
		logWriter.println("]: " + message);
		logWriter.flush();
	}
	
	public void addWhisperedMessage(String sender, String receiver, String message)
	{
		logWriter.print(getCurrentTime() + " " + sender + " [flüstert");
		if(!receiver.equals(""))
		{
			logWriter.print(" zu " + receiver);
		}
		logWriter.println("]: " + message);
		logWriter.flush();
	}

	private void addAsMessageListener(MessageHandler messageHandler)
	{
		messageHandler.registerNewListener(this);
	}
	
	private void removeAsMessageListener(MessageHandler messageHandler)
	{
	   messageHandler.removeListener(this);	
	}
	
	/**
	 * Liefert den Dateinamen für die Chatlogdatei. Falls mehrere in einer Minute erstellt werden, wird eine 1(/2/3) hintendran gehängt.
	 */
	private void setFileName()
	{
	    fileName = SettlersLobby.configDir + "/Chatlog/" + getCurrentDate();	
	    if(new File(fileName + ".txt").exists()){
	    	for(int i = 1 ; i <= 3 ; i++){
	    		if(!new File(fileName + "__"+ i +".txt").exists()){
	    			fileName += ("__" + i);
	    			break;
	    		}
	    	}
	    }
	    fileName += ".txt";
	}
	
	private String getCurrentTime()
	{
	    return new SimpleDateFormat("HH:mm").format(new Date());
	}
	
	private String getCurrentDate()
	{
	    return new SimpleDateFormat("yyyy_MM_dd__HH_mm").format(new Date());	
	}

    @Override
    public void addActionMessage(String user, String message)
    {
        logWriter.println(getCurrentTime() + " * " + user + " " + message);
        logWriter.flush();
    }
}
