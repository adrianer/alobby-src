/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.controlcenter;

import java.io.File;
import java.io.FilenameFilter;
import java.io.RandomAccessFile;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import java.util.regex.Pattern;

import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.util.GameDataFile;

/**
 * Kapselt Informationen zu einem Savegame.
 * @author Stephan Bauer (aka maximilius)
 * @author Jim
 */
public class SaveGame implements ISMap
{

	private Vector<String> playerNamesVector = new Vector<String>();
	private String mapName;
	private Date date;
	private File file;
	private long timestamp;

	private boolean isValuesAreRead = false;
	private SimpleDateFormat gameDateTimeFormat;
	private boolean league = false;;
	private int tournamentid = 0;
	private String tournamentname = "";
	private long creationTime;
	private int round;
	private int groupnumber;
	private int matchnumber;
	private String playerlist;
	private String usersWithRaces;

	/**
	 * Liest eine Savegamedatei ein, extrahiert die Spielernamen und den
	 * Kartennamen
	 *
	 * @param file
	 *            Das Savegame File
	 */
	public SaveGame(File file, String dateTimeFormatString)
	{
		date = new Date(file.lastModified());
		this.file = file;
		this.gameDateTimeFormat = null;
		if (dateTimeFormatString != null)
		{
			this.gameDateTimeFormat = getGameDateTimeFormat(dateTimeFormatString);
		}
	}

	/**
	 * Liefert den Zeitstempel als String des Savegames zurück.
	 *
	 * @return Der Zeitstempel als String.
	 */
	@Override
    public String toDisplayString()
	{
		return getTimeStampString() + I18n.getString("SaveGame.TIME_SUFFIX"); //$NON-NLS-1$
	}

   public String getTimeStampString()
   {
      return gameDateTimeFormat.format(date); //$NON-NLS-1$
   }
	public static SimpleDateFormat getGameDateTimeFormat(String dateTimeFormat)
	{
		SimpleDateFormat gameDateTimeFormat = new SimpleDateFormat(dateTimeFormat); //$NON-NLS-1$
		return gameDateTimeFormat;
	}

    /**
     * Liefert den Dateinamen des Savegames zurück.
     *
     * @return Der Dateiname des Savegames.
     */
	@Override public String toString()
	{
	    return getFileName();
	}


	/**
	 * Liefert die Spielernamen als String in einem Vector zurück.
	 *
	 * @return Der Vector mit den Spielernamen.
	 */
	public Vector<String> getPlayerNames()
	{
		readValuesFromSavegameIfUnread();
		return playerNamesVector;
	}
	
	/**
	 * Get the playernames as String
	 * without computerplayer.
	 * 
	 * @return
	 */
	public String getPlayerNamesAsString() {
		readValuesFromSavegameIfUnread();
		String playerNames = "";
		Test.output("vectorliste: " + playerNamesVector);
		for (String playerName : playerNamesVector) {
			if( !playerName.contains("Computerspieler") && !playerName.contains("computerplayer") ) {
				if( playerNames.length() > 0 ) {
					playerNames = playerNames + ",";
				}
				playerNames = playerNames + playerName;
			}
		}
		return playerNames;
	}

	/**
	 * Liefert den Kartennamen zurück.
	 *
	 * @return Der Kartennamen.
	 */
	public String getMapName()
	{
		readValuesFromSavegameIfUnread();
		return mapName;
	}

	public String getNameDisplayedInGameSelection()
	{
	    return gameDateTimeFormat.format(date);
	}
	
	/**
	 * Liest die relevanten Daten aus der Savegamedatei aus, wenn das
	 * noch nicht geschehen ist.
	 */
	public synchronized void readValuesFromSavegameIfUnread() {
		if (!isValuesAreRead) {
			readValuesFromSavegame();
			isValuesAreRead = true;
		}
	}

	private void readValuesFromSavegame()
	{
	    try
	    {
	        //mapName und SpielerVector zurücksetzen.
	        this.mapName = ""; //$NON-NLS-1$
	        playerNamesVector.clear();
            RandomAccessFile randomAccessFile =
                new RandomAccessFile(file,"r"); //$NON-NLS-1$

            //Wenn man bei offset 0x10 beginnt, funktioniert
            //die Entschlüsselung des folgenden Abschnitts.
            //Innerhalb des Abschnitts ab offset 24 beginnt der mapname
            int offsetMapRead = 0x10;
            randomAccessFile.seek(offsetMapRead);
            String map = readFixedString(randomAccessFile, 0x100);
            map = decodeString(map);
            map = map.substring(24);
            map = getZeroString(map);
            this.mapName = map;

            int offset = 0x10;
            randomAccessFile.seek(offset);
            String mydate = readFixedString(randomAccessFile, 24);
            mydate = decodeString(mydate);
            mydate = mydate.substring(20, 24);
            //little Endian Format
            timestamp = readDoubleWordLittleEndian(mydate) * 1000; //C benutzt timestamp in Sekunden, Java in Millisekunden            
            
            playerNamesVector.clear();
            for (int i = 0; i < 20; ++i)
            {
                int off = 0x392 + i*0x134;
                randomAccessFile.seek(off);
                String str = readFixedString(randomAccessFile, 100);
                String decodedString = decodeString(str);
                String name = getZeroString(decodedString);
                if (name != null)
                {
                    playerNamesVector.add(name);
                }
            }
            randomAccessFile.close();
            removeDuplicateEntriesFromEnd();
	    }
	    catch (Exception e)
	    {
        }
	}

    /**
	 * löscht die mehrfachen Spielereinträge am Ende. Zusätzlich
	 * werden noch eventuell vorhandene leere Einträge gelöscht.
	 * Diese entstehen, wenn Plätze gesperrt wurden.
	 */
    private void removeDuplicateEntriesFromEnd()
    {
        String previousName = null;
        String currentName;
        for (int index = playerNamesVector.size() - 1; index >= 0; --index)
        {
            currentName = playerNamesVector.elementAt(index);
            if (previousName != null)
            {
                if (previousName.equals(currentName))
                {
                    //gleicher Name wie zuvor, löschen
                    playerNamesVector.remove(index + 1);
                }
                else
                {
                    //ungleiche Namen gefunden, hier muss die Schleife
                    //abgebrochen werden
                    //Falls der letzte Name Computerspieler war, wird ein (*)
                    //angehängt, um zu kennzeichnen, dass eventuell zuviele
                    //Spieler gelöscht wurden
                    if (previousName.equals("Computerspieler") || //$NON-NLS-1$
                        previousName.equals("computerplayer")) //$NON-NLS-1$
                    {
                        playerNamesVector.set(index + 1, previousName + "(*)"); //$NON-NLS-1$
                    }
                    break;
                }
            }
            previousName = currentName;
        }
        //noch ein zweites Mal durch die Liste gehen, um alle
        //leeren Einträge zu löschen
        //Dies darf erst nach dem Entfernen der mehrfachen Einträge passieren
        for (int index = playerNamesVector.size() - 1; index >= 0; --index)
        {
            currentName = playerNamesVector.elementAt(index);
            if (currentName.isEmpty())
            {
                playerNamesVector.remove(index);
            }
        }
    }

    /**
     * versucht die Verschlüsselung der S3 savegames rückgängig zu
     * machen. Der String input muß mit einem Zeichen beginnen,
     * bei dem der verschlüsselte Wert gleich dem entschlüsselten
     * Wert ist. Wo solche Starts von Verschlüsselungssequenzen sind,
     * lässt sich nicht definitiv sagen, häufig steht zumindest links
     * davon mindestens eine '0'.
     *
     * Aus dem aktuellen Zeichen und dessen Verschlüsselung wird
     * eine XOR-mask für das folgende Zeichen berechnet.
     * Für das erste Zeichen kennt man diese XOR-mask nicht, daher
     * wird sie auf 0 gesetzt, was zur Folge hat, daß Ver- und Entschlüsselung
     * vom ersten Zeichen identisch sind (bzw. sein müssen).
     * Für die Spielernamen in den S3-savgames ist dieses tatsächlich der Fall,
     * der erste Buchstabe steht immer im Klartext drin.
     *
     * So wird die XorMask für das folgende Zeichen aus dem aktuellen Zeichen berechnet:
     * plainValueXorMask = (2 * unverschlüsselter Wert) XOR unverschlüsselter Wert
     *
     * xorMask = (2 * verschlüsselter Wert) XOR plainValueXorMask
     *
     * Mit dieser xorMask kann jetzt das folgende Zeichen entschlüsselt werden.
     *
     * @param input
     * @return
     */
    private static String decodeString(String input)
    {
        byte[] byteArray = new byte[input.length()];
        int xorMask = 0;
        int value = 0;
        int decValue = 0;
        for (int i = 0; i < input.length(); ++i)
        {
            value = input.charAt(i) & 0xff;
            //die XOR-Maske, die aus dem vorherigen Zeichen berechnet wurde
            //entschlüsselt das aktuelle Zeichen
            decValue = (value ^ xorMask) & 0xff;
            byteArray[i] = (byte)(decValue & 0xff);
            //Berechnung der xorMask für das folgende Zeichen
            int plainValueXorMask = (2*decValue ^ decValue) & 0xff;
            xorMask = ((2*value) ^ plainValueXorMask) & 0xff;
        }
        String result = ""; //$NON-NLS-1$
        try
        {
            result = new String(byteArray, "ISO-8859-1"); //$NON-NLS-1$
        }
        catch (Exception e) {
        }
        return result;
    }

    /**
     * Wenn input einen Null-terminierten String enthält (C/C++ Standard),
     * wird dieser zurückgeliefert.
     *
     * @param input
     * @return
     */
    private static String getZeroString(String input)
    {
        String result = ""; //$NON-NLS-1$
        int index = input.indexOf(0);
        if (index >=0)
        {
            result = input.substring(0, index);
        }
        return result;
    }

    /**
     * liest {@code length} viele Zeichen aus {@code file}.
     * @param file
     * @param length
     * @return der ausgelesene String
     */
    private static String readFixedString(RandomAccessFile file, int length)
    {
        byte[] byteArray = new byte[length];
        int readLength = 0;
        String result = ""; //$NON-NLS-1$
        try
        {
            readLength = file.read(byteArray);
            result = new String(byteArray, 0, readLength, "ISO-8859-1"); //$NON-NLS-1$
        }
        catch (Exception e)
        {
        }
        return result;
    }

    /**
     * Erzeugt das zum savegame gehörende Datum, so wie es in Siedler3
     * angezeigt wird. Das Datum muss aus dem savegame selber ausgelesen
     * werden.
     *
     * @param savegameName Name des savegames mit Endung (*.mps)
     * @param gamePath Pfad der S3.exe
     * @return
     */
    public static String getNameDisplayedInGameSelection(
            String savegameName, String gamePath, String dateTimeFormatString)
    {
    	File file;
    	if (isBackupSave(savegameName))
    	{
            file = new File(getSavegameBackupDirFromGamePath(gamePath) + File.separator +
                    savegameName);
    	} else {
            file = new File(getSavegameDirFromGamePath(gamePath) + File.separator +
                    savegameName);
    	}
    	
        String result = getGameDateTimeFormat(dateTimeFormatString).format(new Date(file.lastModified()));
        
        if (!file.exists())
        {
        	Test.output("Save " + file.toString() + " does not exist!");
        	return result;
        }
        
        String realTime = getTimeDisplayedInGameSelection(savegameName, gamePath, dateTimeFormatString);
        if (realTime != null)
        {
        	return realTime;
        } else {
        	return result;
        }
    }

   public static String getTimeDisplayedInGameSelection(String savegameName, String gamePath, String dateTimeFormatString)
   {
      File file;
      if (isBackupSave(savegameName))
         file = new File(getSavegameBackupDirFromGamePath(gamePath) + File.separator + savegameName); 
      else
         file = new File(getSavegameDirFromGamePath(gamePath) + File.separator + savegameName);

      String result;
        
      if (!file.exists())
      {
         Test.output("Save " + file.toString() + " does not exist!");
         result = null;
      }
      else
      {
         long lDate = (new SaveGame(file, dateTimeFormatString)).getRealTimeStamp();
         result = getGameDateTimeFormat(dateTimeFormatString).format(new Date(lDate));
      }
      return result;
   }

    public long getRealTimeStamp()
    {
      readValuesFromSavegameIfUnread();
      return timestamp;
    }
    
    
    private static long readDoubleWordLittleEndian(String str)
    {
        //little Endian Format
        long doubleWord = (str.charAt(3) << 24) + (str.charAt(2) << 16) +
            (str.charAt(1) << 8) + (str.charAt(0));
        return doubleWord;
    }

    /**
     * Liefert den Dateinamen des Savegames zurück.
     * @return Der Dateiname des Savegames.
     */
	public String getFileName() {
		return file.getName();
	}
	
	/**
	 * Is this a league-game?
	 * 
	 * @return boolean
	 */
	public boolean isLeagueGame() {
		return league;
	}
	
	/**
	 * Is this a tournamentgame? 
	 * 
	 * @return boolean
	 */
	public boolean isTournamentGame() {
		return tournamentid > 0 ? true : false;
	}
	
	/**
	 * Return the tournamentid
	 * 
	 * @return int
	 */
	public int getTournamentId() {
		return tournamentid;
	}
	
	/**
	 * Return the tournamentname
	 * 
	 * @return String
	 */
	public String getTournamentName() {
		return tournamentname;
	}
	
	public static boolean isBackupSave(String fileName)
	{
		// Saves from the SaveManager end with \\.[0-9]+
	    final String FILENAME_REGULAR_EXPRESSION =
	        "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}\\.mps\\.[0-9]+$"; //$NON-NLS-1$

	    final Pattern filenamePattern =
	        Pattern.compile(FILENAME_REGULAR_EXPRESSION);
	    
	    return filenamePattern.matcher(fileName).matches();
	}

	public static boolean isRegularSave(String fileName)
	{
	    final String FILENAME_REGULAR_EXPRESSION =
	        "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}\\.mps$"; //$NON-NLS-1$

	    final Pattern filenamePattern =
	        Pattern.compile(FILENAME_REGULAR_EXPRESSION);
	    
	    return filenamePattern.matcher(fileName).matches();
	}

	public static class SavegameFilenameFilter implements FilenameFilter
	{
		// Saves from the SaveManager end with \\.[0-9]+
	    private static final String FILENAME_REGULAR_EXPRESSION =
	        "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}\\.mps(\\.[0-9]+)?$"; //$NON-NLS-1$

	    private static final Pattern filenamePattern =
	        Pattern.compile(FILENAME_REGULAR_EXPRESSION);

        @Override
        public boolean accept(File dir, String fileName)
        {
            //alle gültigen savegames haben einen Dateinamen der Länge 40
            //(inklusive Dateiendung).
            //Problem: der Name scheint eine Checksumme zu sein,
            //die von S3 überprüft wird. Wenn die Checksumme ungültig
            //ist, wird das Savegame gar nicht erst in S3 angezeigt.
            //Dies kann leider nicht erkannt werden,
            //so daß die Indexberechnung zur Kartenauswahl grundsätzlich
            //falsch ist, wenn so ein ungültiges Savegame im Pfad liegt.
            //Dieses muss der User aber selber kaputt gemacht haben,
            //von daher ist er selber schuld, wenn es nicht funktioniert.
            //Durch die Fixierung der Länge auf 40 wird die Datei "temp.mps"
            //ausgefiltert, die überbleibt wenn man direkt beim saven
            //das Spiel beendet.
            return (filenamePattern.matcher(fileName).matches());
        }
	}

	/**
	 * Leitet aus dem Pfad zur s3.exe den Pfad zu den Multiplayer
	 * Savegames ab.
	 * @param gamePath
	 * @return Pfad zu den Multiplayer Savegames
	 */
	public static String getSavegameDirFromGamePath(String gamePath)
	{
	    return gamePath.substring(0, gamePath.lastIndexOf("\\") + 1) //$NON-NLS-1$
            + "Save" + File.separator + "Multi"; //$NON-NLS-1$ //$NON-NLS-2$
	}

	public static String getSavegameBackupDirFromGamePath(String gamePath)
	{
	    return getSavegameDirFromGamePath(gamePath) + "Backups"; //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * Liefert alle savegames als File Objekt.
	 * @param gamePath Pfad zur s3.exe
	 * @return alle savegames als File Objekt.
	 */
	public static File[] getSavegameFiles(String gamePath)
	{
        File[] saveGameFiles = new File(SaveGame.getSavegameDirFromGamePath(gamePath))
            .listFiles(new SavegameFilenameFilter());
        return saveGameFiles;
	}

	/**
	 * Liefert den Namen der im Savegame gespielten Karte.
	 *
	 * @param savegameAsMapPath format: Savegame\xyz.mps
	 * @param gamePath
	 * @return Name der im Savegame gespielten Karte
	 */
	public static String getS3MapnameOfSaveGame(String savegameAsMapPath, String gamePath, String dateTimeFormatString)
	{
		File file = getFileFromSaveGameName(savegameAsMapPath, gamePath);
		return getS3MapnameOfSaveGame(file, dateTimeFormatString);
	}
	
	public static String getS3MapnameOfSaveGame(File saveGame, String dateTimeFormatString)
	{
	    SaveGame s = new SaveGame(saveGame, dateTimeFormatString); 
        return s.getMapName();
	}

	public static File getFileFromSaveGameName(String savegameAsMapPath, String gamePath) 
	{
		if (!savegameAsMapPath.startsWith(ALobbyConstants.LABEL_SAVE_GAME) ||
	            savegameAsMapPath.lastIndexOf(File.separator) == -1 )
	    {
	        throw new IllegalArgumentException(MessageFormat.format(
	                I18n.getString("SaveGame.INVALID_FORMAT"), savegameAsMapPath)); //$NON-NLS-1$
	    }
	    // xyz.mps
	    String savegameName = savegameAsMapPath.substring(savegameAsMapPath.lastIndexOf(File.separator) + 1);
	    // Siedler3/Save/Multi
	    String path;
    	if (isBackupSave(savegameName))
    	{
    		path = getSavegameBackupDirFromGamePath(gamePath);
    	} else {
    		path = getSavegameDirFromGamePath(gamePath);
    	}
    	
	    File file = new File(path + File.separator + savegameName);
		return file;
	}

    @Override
    public boolean isValid()
    {
        //true falls ein Dateiname übergeben wurde und er einem Savegamenamen
        //entspricht. Es wird nicht überprüft, ob die Datei tatsächlich vorhanden ist
        return file != null &&
            SavegameFilenameFilter.filenamePattern.matcher(file.getName()).matches();
    }

	public static long getTimestamp(File savegame) 
	{
		return savegame.lastModified();
	}

	@Override
	public int getPlayer() {
		return 0;
	}

	@Override
	public int getWidthHeight() {
		return 0;
	}
	
	/**
	 * Get the original creationtime of this game.
	 * 
	 * @return Long
	 */
	public Long getCreationTime() {
		return this.creationTime;
	}

	/**
	 * Reads additional game-data with help of GameDataFile from separate
	 * file which holds additional game-data.
	 * 
	 * E.g.:
	 * - if this is a league-game
	 * - or if it is a tournament-game
	 * 
	 * @param md5
	 */
	public void getAdditionalDataFromFile( SettlersLobby settlersLobby, OpenGame openGame ) {
		// call gameDataFile to get the game-data
    	GameDataFile gameDataFile = new GameDataFile(settlersLobby, openGame);
    	if( gameDataFile.get("league") != null ) {
    		this.league = Boolean.parseBoolean(gameDataFile.get("league"));
    	}
    	if( gameDataFile.get("tournamentid") != null ) {
    		this.tournamentid = Integer.parseInt(gameDataFile.get("tournamentid"));
    	}
		this.tournamentname = gameDataFile.get("tournamentname");
		if( gameDataFile.get("creationTime") != null ) {
			this.creationTime = Long.parseLong(gameDataFile.get("creationTime"));
		}
		if( gameDataFile.get("round") != null ) {
			this.round = Integer.parseInt(gameDataFile.get("round"));
		}
		if( gameDataFile.get("groupnumber") != null ) {
			this.groupnumber = Integer.parseInt(gameDataFile.get("groupnumber"));
		}
		if( gameDataFile.get("matchnumber") != null ) {
			this.matchnumber = Integer.parseInt(gameDataFile.get("matchnumber"));
		}
		this.playerlist = gameDataFile.get("playerlist");
		this.usersWithRaces = gameDataFile.get("usersWithRaces");
	}

	public int getTournamentMatchnumber() {
		return this.matchnumber;
	}

	public int getTournamentGroupnumber() {
		return this.groupnumber;
	}

	public int getTournamentRound() {
		return this.round;
	}

	public String getTournamentPlayerlist() {
		return this.playerlist;
	}
	
	public String getUsersWithRaces() {
		return this.usersWithRaces;
	}

}
