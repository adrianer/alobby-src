/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.controlcenter;

import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

/**
 * Bildet einen Container für Instanzen von OpenGame.
 * @author Stephan Bauer (aka maximilius)
 */
public class OpenGameContainer implements Observer {

	protected Vector<OpenGame> openGames;

	/**
	 * Liefert eine neue Instanz.
	 */
	public OpenGameContainer()
	{
		openGames = new Vector<OpenGame>();
	}

	/**
	 * Fügt dem Container das übergebene OpenGame hinzu und registriert sich
	 * als Observer beim OpenGame, um es selbstständig zu löschen, wenn es
	 * sich zerstört.
	 * @param openGame Das hinzuzufügende OpenGame.
	 */
	public void addOpenGame(OpenGame openGame) {
		openGames.add(openGame);
		openGame.addObserver(this);
	}

	/**
	 * Sucht das OpenGame mit der übergebenen IP und dem Spielernamen des Hosts.
	 * @param ip			Die IP des zu findenden OpenGames.
	 * @param hostUserName	Der Spielername des Hosts des zu findenen OpenGames.
	 * @return Das gefundene OpenGame, oder wenn es nicht gefunden wurde, null.
	 */
	public OpenGame getOpenGame(String ip, String hostUserName) {
		OpenGame result = null;
		for (OpenGame openGame : openGames) {
			if (openGame.getHostUserName().equals(hostUserName)
					&& openGame.getIP().equals(ip)) {
				result = openGame;
				break;
			}
		}
		return result;
	}

	/**
	 * Nur zu verwenden, wenn die IP nicht bekannt ist!
	 * Sucht das OpenGame mit dem übergebenen Spielernamen des Hosts.
	 * @param hostUserName	Der Spielername des Hosts des zu findenen OpenGames.
	 * @return Das gefundene OpenGame, oder wenn es nicht gefunden wurde, null.
	 */
	public OpenGame getOpenGame(String hostUserName) {
		OpenGame result = null;
		for (OpenGame openGame : openGames) {
			if (openGame.getHostUserName().equals(hostUserName)) {
				result = openGame;
				break;
			}
		}
		return result;
	}

	/**
	 * Ist das übergebene Observable eine Instanz der Klasse OpenGame und
	 * die Änderung die Zerstörung dessen, dann wird es aus dem Container
	 * gelöscht.
	 * @param openGame	Das Observable.
	 * @param change	Die Änderung.
	 */
	@Override
    public void update(Observable openGame, Object change) {
		if (openGame instanceof OpenGame) {
			if (change.equals(OpenGame.DESTROYED)) {
				openGames.remove(((OpenGame) openGame));
			}
		}
	}

	/**
	 * Leert den Container.
	 */
	public void clear() {
		openGames.clear();
	}
}
