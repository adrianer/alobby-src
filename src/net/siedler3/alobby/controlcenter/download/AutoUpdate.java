/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.controlcenter.download;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.gui.StartFrame;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.util.gpg.Gpg;

public class AutoUpdate
{
	private UpdateDownloadV2 updateDownload;
	private SettlersLobby settlersLobby;

	private String downloadURL = "";

	public AutoUpdate(SettlersLobby settlersLobby) {
		this.settlersLobby = settlersLobby;
	}


	/**
	 * Testet, ob das Programm inaktuell ist, läd die neuen Dateien vom Server
	 * und ruft dann das Update-Programm auf.
	 * @param settlersLobby
	 * @param sF
	 * @param showNoUpdateAvailableMessage
	 * 			True, wenn bei keinem neuen Update eine Nachricht angezeigt
	 * 			werden soll, andernfalls false.
	 */
	public void updateProgramIfNecessary(StartFrame sF, boolean showNoUpdateAvailableMessage)
	{
		if (!checkIfVersionIsUpToDate(sF, ALobbyConstants.VERSION))
		{
			//die downloadURL wurde in checkIfVersionIsUpToDate() gesetzt
			if (downloadURL == null)
			{
			    return;
			}
			if (!downloadURL.startsWith(settlersLobby.config.getLobbyUrl()) && !downloadURL.startsWith("http://" + settlersLobby.config.getLobbyDomain())) ////$NON-NLS-1$
			{
		         int pressedKey = JOptionPane.showConfirmDialog(sF,
		                 MessageFormat.format(I18n.getString("AutoUpdate.MSG_STRANGE_URL"), downloadURL, settlersLobby.config.getLobbyDomain()), //$NON-NLS-1$, $NON-NLS-2$
		                    I18n.getString("AutoUpdate.DIALOG_TITLE_NEW_UPDATE"), JOptionPane.YES_NO_OPTION); //$NON-NLS-1$
		         if (pressedKey == JOptionPane.NO_OPTION)
		                return;
			}
			updateDownload = new UpdateDownloadV2(settlersLobby, downloadURL);

			File[] signedUpdateFile = updateDownload.downloadSignedFile();
			if (signedUpdateFile != null)
			{
			    //check signature
				if (!Gpg.verifySignature(signedUpdateFile[0], signedUpdateFile[1]))
				{
                    //abort in case of signature failure
                    displayInformation(I18n.getString("AutoUpdate.DIALOG_TITLE"),  //$NON-NLS-1$
                                       updateDownload.getMessage(Download.ERROR_INVALID_SIGNATURE));
                    return;
				}
				
                displayInformation(I18n.getString("AutoUpdate.DIALOG_TITLE"),  //$NON-NLS-1$
                        updateDownload.getMessage(Download.MELDUNG_ERFOLGREICH));

				//close the lobby
				settlersLobby.prepareForUpdateOfProgram();

				if (!callUpdater(signedUpdateFile[0]))
				{
					displayInformation(I18n.getString("AutoUpdate.DIALOG_TITLE_ERROR_UPDATER"), //$NON-NLS-1$
					                   I18n.getString("AutoUpdate.ERROR_UPDATER"));  //$NON-NLS-1$
					settlersLobby.displayFirstFrame();
					return;
				}
				else
				{
				    //close program to release locks on files
				    System.exit(0);
				}
			}
		}
		else if (showNoUpdateAvailableMessage)
		{
			displayInformation(I18n.getString("AutoUpdate.DIALOG_TITLE_NO_UPDATE_AVAILABLE"), //$NON-NLS-1$
					           I18n.getString("AutoUpdate.NO_UPDATE_AVAILABLE"));  //$NON-NLS-1$
		}

	}

	private void displayInformation(String title, String message)
	{
	    settlersLobby.getGUI().displayInformation(title, message);
	}

	private boolean callUpdater(File installer)
	{
		String[] command = {
            "cmd", //$NON-NLS-1$
            "/c", //$NON-NLS-1$
            installer.getAbsolutePath(),
            "/S", //silent mode
            "/INSTDIR", //provide the current dir as install dir
            System.getProperty("user.dir"),
            "/CONFIGDIR",
            SettlersLobby.configDir
		};
		//the installer will restart the alobby after updating

		try
		{
		    new ProcessBuilder(command).start();
		}
		catch (IOException io)
		{
			System.out.println(io.getMessage());
			return false;
		}
	    return true;
	}

	/**
	 * Überprüft, ob die Programmversion mit der aktuellen Version übereinstimmt.
	 * Die Programmversion wird über eine Website verglichen.
	 *
	 * @param version
	 *
	 * @return true, wenn keine neue Version verfügbar ist oder ein Fehler entstanden ist
	 *         , sonst false
	 */
    private boolean checkIfVersionIsUpToDate(StartFrame sF, String version)
    {
    	URL url = null;
    	BufferedReader bufferedReader;
    	String website;
        String content = ""; //$NON-NLS-1$
        String[] splitted = new String[2];
        int[] versionSplitted = new int[3];
        int[] currentVersionSplitted = new int[3];
        String currentVersion = null;
        int[] currentTestVersionSplitted = new int[3];
        String currentTestVersion = null;
        String downloadTestURL = "";
        boolean isTestVersion = false;
        int[] currentMinorTestVersionSplitted = new int[4];
        String currentMinorTestVersion = null;
        String downloadMinorTestURL = "";
        boolean isMinorTestVersion = false;

    	try
    	{
    	    url = new URL(settlersLobby.config.getUrlIsLobbyNewVersion());
    	}catch (MalformedURLException e)
    	{
    	    JOptionPane.showMessageDialog(sF, I18n.getString("AutoUpdate.ERROR_INVALID_URL"), I18n.getString("AutoUpdate.DIALOG_TITLE_ERROR"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
    	    return true;
    	}

    	try
    	{
    	    Test.output("reading " + url.toString());
    		bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()));
    		while ((website = bufferedReader.readLine()) != null)
    		{
    		    content += website;
    		}
    		bufferedReader.close();
    	}catch (IOException e)
    	{
    	    Test.outputException(e);
    		JOptionPane.showMessageDialog(sF, I18n.getString("AutoUpdate.ERROR_URL_UNREACHABLE"),  //$NON-NLS-1$
    				I18n.getString("AutoUpdate.DIALOG_TITLE_ERROR"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$
    	    return true;
    	}

    	if (content != null && content.length() > 0)
    	{
    	    splitted = content.split(";");
    	    if (splitted.length < 2)
    	    {
    	    	//wrong response
    	    	return true;
    	    }

    	    currentVersion = splitted[0];
    	    Test.output("update version: " + currentVersion);
    	    downloadURL   = splitted[1];

    	    currentVersionSplitted = splitVersion(currentVersion);
    	    versionSplitted = splitVersion(version);

            if (currentVersionSplitted == null || versionSplitted == null ||
	                currentVersionSplitted.length < 2 || versionSplitted.length < 2)
            {
            	return true;
            }

            isMinorTestVersion = versionSplitted.length == 4;

            if (splitted.length >= 6)
            {
            	currentMinorTestVersion = splitted[4];
            	downloadMinorTestURL   = splitted[5];
            	currentMinorTestVersionSplitted = splitVersion(currentMinorTestVersion);
	            if (isMinorTestVersion && currentMinorTestVersionSplitted != null && currentMinorTestVersionSplitted.length >= 3)
	            {
	            	//We are only interested in the test version when it has
	            	//a higher version number than the stable one.
	            	if (!compareVersion(currentVersionSplitted, currentMinorTestVersionSplitted))
	            	{
	            		currentVersion = currentMinorTestVersion;
	            		currentVersionSplitted = currentMinorTestVersionSplitted;
	            		downloadURL = downloadMinorTestURL;
	            	}
	            }
            }

            isTestVersion = versionSplitted[2] >= 90;

            if (splitted.length >= 4)
            {
            	currentTestVersion = splitted[2];
	            downloadTestURL   = splitted[3];
	            currentTestVersionSplitted = splitVersion(currentTestVersion);
	            if (isTestVersion && currentTestVersionSplitted != null && currentTestVersionSplitted.length >= 2)
	            {
	            	//We are only interested in the test version when it has
	            	//a higher version number than the stable one.
	            	if (!compareVersion(currentVersionSplitted, currentTestVersionSplitted))
	            	{
	            		currentVersion = currentTestVersion;
	            		currentVersionSplitted = currentTestVersionSplitted;
	            		downloadURL = downloadTestURL;
	            	}
	            }
            }
            return compareVersion(versionSplitted, currentVersionSplitted);

        }
        else
        {
            return true;
        }
    }

    /**
     * Returns false, if the newVersion is higher than the baseVersion.
     * Otherwise returns true.
     * @param baseVersion
     * @param newVersion
     * @return
     */
    private boolean compareVersion(int[] baseVersion, int[] newVersion)
    {
        /*True wenn:
        1: 1. Stelle: Server-Version ist größer als Rechner-Version
           2. Stelle: egal
           3. Stelle: egal
        2: 1. Stelle: Server-Version ist gleich der Rechner-Version
           2. Stelle: Server-Version ist größer als Rechner-Version
           3. Stelle: egal
        3: 1. Stelle: Server-Version ist gleich der Rechner-Version
           2. Stelle: Server-Version ist gleich der Rechner-Version
           3. Stelle: Server-Version ist größer als Rechner-Version
        4: 1. Stelle: Server-Version ist gleich der Rechner-Version
           2. Stelle: Server-Version ist gleich der Rechner-Version
           3. Stelle: Server-Version ist gleich der Rechner-Version
           4. Stelle: Server-Version ist größer als Rechner-Version*/
      if (newVersion[0] > baseVersion[0]
                  ||(newVersion[0] == baseVersion[0] &&
                		  newVersion[1] >  baseVersion[1])
                  ||(newVersion[0] == baseVersion[0] &&
                		  newVersion[1] == baseVersion[1] &&
                		  newVersion[2] >  baseVersion[2])
                  ||(newVersion.length >= 4 &&
                  		  newVersion[0] == baseVersion[0] &&
        		          newVersion[1] == baseVersion[1] &&
		        		  newVersion[2] == baseVersion[2] &&
        				  (baseVersion.length <= 3
        				  || newVersion[3] >  baseVersion[3])))
      {
          return false;
      }
      else
      {
          return true;
      }
    }

    /**
     * Zum Vergleichen der Versionen werden Integer benötigt. Es wird diese
     * Methode genommen, um die If-Bedingungen möglichst übersichtlich zu halten.
     * Der String wird gesplittet und als Integer-Array zurückgegeben.
     * @param string
     * @return
     */
    private int[] splitVersion(String string)
    {
    	if (!Pattern.matches("[0-9]{1,2}\\.[0-9]+\\.[0-9]+\\.*[0-9]*", string))
    	{
    	    Test.output("invalid version string: " + string);
        	return null;
    	}

      	String[] splitted = string.split("\\.");
    	int[] splittedInt = new int[splitted.length];

    	for(int i = 0 ; i <= splitted.length-1 ; i++)
    	{
   	        splittedInt[i] = Integer.parseInt(splitted[i]);
    	}

    	return splittedInt;
    }
}
