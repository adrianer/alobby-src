/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.controlcenter.download;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.swing.ProgressMonitor;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.util.UrlHelper;

public class MapDownload extends Download
{

    private static class MapListEntry
    {
        public String mapname;
        public String downloadUrl;

        public MapListEntry(String mapname, String downloadUrl)
        {
            this.mapname = mapname;
            this.downloadUrl = downloadUrl;
        }

        @Override
        public String toString()
        {
            return mapname;
        }
    }

	
    public MapDownload(SettlersLobby settlersLobby) {
		super(settlersLobby);
	}

	/**
     * Liefert angepasste Nachrichten von den jeweiligen Unterklassen
     * 
     * @param message kann folgende Nachrichten enthalten:
     * Error:Server;
     * Error:Verzeichnis;
     * Error:temporäres Verzeichnis;
     * Error:herunterladen;
     * Error:entpacken;
     * Error:kopieren;
     * Meldung:erfolgreich;
     * Meldung:downloading;
     * @return
     */
    @Override
    protected String getMessage(String message)
    {
        if (message.equals(ERROR_SERVER))
        	return I18n.getString("MapDownload.ERROR_SERVER"); //$NON-NLS-1$
        
        else if (message.equals(ERROR_VERZEICHNIS))
        	return I18n.getString("MapDownload.ERROR_DIRECTORY"); //$NON-NLS-1$
        
        else if (message.equals(ERROR_TEMPORAERES_VERZEICHNIS))
        	return I18n.getString("MapDownload.ERROR_CREATE_TMP"); //$NON-NLS-1$
        
        else if (message.equals(ERROR_HERUNTERLADEN))
        	return I18n.getString("MapDownload.ERROR_DOWNLOAD"); //$NON-NLS-1$
        
        else if (message.equals(ERROR_ENTPACKEN))
            return I18n.getString("MapDownload.ERROR_EXTRACT"); //$NON-NLS-1$
            
        else if (message.equals(MELDUNG_ERFOLGREICH))
        	return I18n.getString("MapDownload.MSG_SUCCESS"); //$NON-NLS-1$
        
        else if (message.equals(ERROR_KOPIEREN))
        	return I18n.getString("MapDownload.ERROR_COPY"); //$NON-NLS-1$
       
        else
        	return I18n.getString("MapDownload.ERROR"); //$NON-NLS-1$
    }
    
    @Override
    protected String getDownloadPath()
    {
        return settlersLobby.config.getAutomaticMapDownloadPath();	
    }
    
    @Override
    protected URL getDownloadURL(String serverURL, String fileName)
	{
		URL url = null;
        URL mapbaseUrl;
        BufferedReader in = null;
        ProgressMonitor monitor = null;
        try
        {
            Test.output("Looking for map with fileName: " + fileName);
            //es wird ein Progressmonitor erzeugt, damit der User
            //bemerkt, dass irgendeine Aktion läuft
            //Der Balken zeigt eigentlich nicht den tatsächlichen Fortschritt an,
            //aber falls der Server nicht erreichbar ist, sieht man zumindest
            //das Fenster, so daß man weiß, daß aktuell noch etwas passiert.
            //Das Maximum steht auf 10, aber sobald man überhaupt etwas vom
            //Server empfangen hat, hat man vermutlich schon die komplette Antwort.
            //Wirklich wichtig ist also nur der Schritt von 0 auf 1.
            monitor = new ProgressMonitor(
                    null, I18n.getString("MapDownload.MSG_CONNECT_TO_SERVER"), null, 0, 10); //$NON-NLS-1$
            monitor.setMillisToDecideToPopup(0);
            monitor.setMillisToPopup(0);
            int counter = 0;
            monitor.setProgress(counter++);

            String encodedMapName = UrlHelper.urlEncode(fileName.trim());
            mapbaseUrl = new URL(serverURL + encodedMapName);

            //explicitly check for the charset, as the mapname may contain non-ascii characters
            //in = UrlHelper.createBufferedReaderFromUrl(mapbaseUrl);
            URLConnection connection = mapbaseUrl.openConnection();
            in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
    
            String line;
            //(?:X) ist eine non-capturing group für X
            Pattern pattern = Pattern.compile("(.+);((?:http://|https://|ftp://|file:/).+)"); //$NON-NLS-1$
            Matcher matcher;
            List<MapListEntry> mapList = new ArrayList<MapListEntry>();
            
            while ((line = in.readLine()) != null)
            {
                Test.output(line);
                monitor.setProgress(counter++);
                if (monitor.isCanceled())
                {
                    monitor.close();
                    throw new UserAbortException();
                }
                matcher = pattern.matcher(line);
                if (matcher.matches())
                {
                    mapList.add(new MapListEntry(matcher.group(1), matcher.group(2)));
                }
            }

            if (!mapList.isEmpty())
            {
                if (gui != null)
                {
                    if (mapList.size() > 1)
                    {
                        String message = I18n.getString("MapDownload.MSG_SELECT_MAP"); //$NON-NLS-1$
                        Object selectedMap = gui.displaySelectionDialog(I18n.getString("MapDownload.DIALOG_TITLE"), message, mapList); //$NON-NLS-1$
                        if (selectedMap != null)
                        {
                            url = new URL(((MapListEntry)selectedMap).downloadUrl);
                        }
                        else
                        {
                            throw new UserAbortException();
                        }
                    }
                    else
                    {
                        url = new URL(mapList.get(0).downloadUrl);
                    }
                }
                else
                {
                    url = new URL(mapList.get(0).downloadUrl);
                }
                try
                {
                    // url.toURI() wirft eine Exception, wenn die url nicht 
                    // korrekt encodiert ist.
                    // Vereinzelt kommt es auf der mapbase vor, daß der downloadlink 
                    // noch spaces enthält.
                    url.toURI();
                }
                catch (java.net.URISyntaxException se) 
                {
                    //eventuell war die URL gar nicht encodiert
                    //es wird versucht dies nachträglich zu machen
                    url = new URI(null, url.toExternalForm(), null).toURL();
                }
                Test.output("Found map URL: " + url);

            }
        }
        catch (UserAbortException uae)
        {
            throw uae;
        }
        catch (Exception e) 
        {
            appendError(e.toString());
            Test.outputException(e);
        }
        finally 
        {
            try 
            {
                if (in != null) 
                {
                    in.close();
                }
                if (monitor != null)
                {
                    monitor.close();
                }
            } 
            catch (IOException ioe) 
            {
            }
        }
        return url;
	}
    
    /**
     * erzeugt ein passende Pattern anhand des Kartennamens.
     * Das Pattern ist für die Funktion {@link #isExpectedFile(Pattern, String)}
     * gedacht. 
     * 
     * @param name Kartenname, ohne Dateiendung
     * @return
     */ 
    @Override
    protected Pattern getRegExPattern(String name)
    {
        //vor und hinter dem mapnamen dürfen Zeichen stehen, aber kein "\"
        // im Prinzip ein "*mapname*.map"
        String separatorAsRegEx = File.separator;
        if (File.separator.equals("\\")) //$NON-NLS-1$
        {
            // backslash muss in java für reguläre Ausdrücke zusätzlich escaped werden
            separatorAsRegEx = "\\" + File.separator; //$NON-NLS-1$
        }
        String anyCharButSeparator = "[^" + separatorAsRegEx + "]*"; //$NON-NLS-1$ //$NON-NLS-2$
        
        String regEx = "^" + anyCharButSeparator + Pattern.quote(name) + anyCharButSeparator + "\\.map"; //$NON-NLS-1$ //$NON-NLS-2$
        return Pattern.compile(regEx, Pattern.CASE_INSENSITIVE);
    }
    
    /**
     * Ermittelt aus dem S3.exe Pfad den zugehörigen Pfad des Usermap
     * Verzeichnisses und gibt ihn als File-Objekt zurück
     * 
     * @param gamePath
     * @return 
     */
    @Override
    protected File getDestinationDirectory(String gamePath)
    {
        File userMapDir = null;
        if (gamePath != null && !gamePath.isEmpty())
        {
            String s3Dir = new File(gamePath).getParent();
            if (!s3Dir.isEmpty())
            {
                userMapDir = new File(new File(s3Dir, "Map"), "User"); //$NON-NLS-1$ //$NON-NLS-2$
            }
        }
        return userMapDir;
    }

    /**
     * Extrahiert die Karte aus dem Archiv.
     * Es werden .zip und .rar Archive unterstützt.
     * Zusätzlich werden auch ungepackte .map Dateien
     * akzeptiert, diese werden einfach zurückgegeben.
     * 
     * @param archive
     * @param name Name der Karte
     * @return File Objekt der extrahierten Karte
     */
    @Override
    protected File extractFile(File archive, String name)
    {
        String fileExtension = Download.getExtension(archive);
        if (fileExtension.equals("map")) //$NON-NLS-1$
        {
            //Das Archiv ist gar kein Archiv, sondern schon direkt eine .map-Datei
            //Kein Entpacken nötig, gebe direkt die input-Datei zurück
            return archive;
        }
        //ansonsten wird die standard-Methode benutzt
        return super.extractFile(archive, name);
    }

    @Override
    protected File extractZipEntry(ZipFile zipfile, ZipEntry entry,
            String destDir) throws IOException
    {
        if (entry.isDirectory())
        {
            //die ausgewählte map ist ein Directory, das darf nicht sein, Abbruch
            return null;
        }
        return super.extractZipEntry(zipfile, entry, destDir);
    }

    @Override
    protected void onExtractError(File archiveFile)
    {
        if (displayErrorAndConfirm(getMessage(ERROR_ENTPACKEN)))
        {
            openFileLocation(archiveFile);
        }
    }
    
}

