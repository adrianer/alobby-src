/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.controlcenter.download;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.regex.Pattern;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.i18n.I18n;

public class UpdateDownloadV2 extends Download
{
    private String downloadUrl;

    /**
     *
     * @param settlersLobby
     * @param downloadUrl URL, unter der das Update heruntergeladen werden kann
     */
	public UpdateDownloadV2(SettlersLobby settlersLobby, String downloadUrl)
	{
		super(settlersLobby);
		this.downloadUrl = downloadUrl;
		this.doNotExtract = true;
	}

    @Override
    protected File getDestinationDirectory(String path)
    {
    	//Die Methode ist für das Update nicht erforderlich.
    	//Daher kann Ordner != null zurückgegeben werden.
        return new File("");
    }

    /**
     * Liefert angepasste Nachrichten von den jeweiligen Unterklassen
     *
     * @param message kann folgende Nachrichten enthalten:
     * Error:Server;
     * Error:Verzeichnis;
     * Error:temporäres Verzeichnis;
     * Error:herunterladen;
     * Error:entpacken;
     * Error:kopieren;
     * Error:verschieben;
     * Meldung:erfolgreich;
     * Meldung:downloading;
     * @return
     */
    @Override
    protected String getMessage(String message)
    {
    	if (message.equals(ERROR_SERVER))
    		return I18n.getString("UpdateDownload.ERROR_SERVER"); //$NON-NLS-1$
    	else if (message.equals(ERROR_VERZEICHNIS))
    		return I18n.getString("UpdateDownload.ERROR_DIRECTORY"); //$NON-NLS-1$
    	else if (message.equals(ERROR_TEMPORAERES_VERZEICHNIS))
    		return I18n.getString("UpdateDownload.ERROR_CREATE_TMP"); //$NON-NLS-1$
    	else if (message.equals(ERROR_HERUNTERLADEN))
    		return I18n.getString("UpdateDownload.ERROR_DOWNLOAD"); //$NON-NLS-1$
    	else if (message.equals(ERROR_ENTPACKEN))
    		return I18n.getString("UpdateDownload.ERROR_EXTRACT"); //$NON-NLS-1$
    	else if (message.equals(ERROR_KOPIEREN))
    		return I18n.getString("UpdateDownload.ERROR_COPY"); //$NON-NLS-1$
    	else if (message.equals(ERROR_VERSCHIEBEN))
    		return I18n.getString("UpdateDownload.ERROR_MOVE"); //$NON-NLS-1$
    	else if (message.equals(ERROR_INVALID_SIGNATURE))
    	    return I18n.getString("UpdateDownload.ERROR_INVALID_SIGNATURE"); //$NON-NLS-1$
    	else if (message.equals(MELDUNG_ERFOLGREICH))
    		return I18n.getString("UpdateDownload.MSG_SUCCESS"); //$NON-NLS-1$
    	else
    	    return message;
    }

	@Override
    protected String getDownloadPath()
    {
        return downloadUrl;
    }

	@Override
    protected URL getDownloadURL(String serverURL, String fileName)
	{
		try
		{
		    //serverURL besteht aus downloadUrl und
		    //ist schon vollständig.
		    //Es ist keine weiter Auswertung nötig.
            return new URL(serverURL);
		}
		catch (MalformedURLException mue)
		{
			return null;
		}
	}

    @Override
    protected Pattern getRegExPattern(String name)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean downloadFile(String fileName)
    {
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @return the file and the corresponding .asc file containing the signature.
     */
    public File[] downloadSignedFile()
    {
        File[] result = null;

        String serverURL = getDownloadPath();

        URL url = null;
        try
        {
            url = getDownloadURL(serverURL, null);
        }
        catch (UserAbortException e)
        {
            return result;
        }
        if (url == null)
        {
            displayError(getMessage(ERROR_SERVER));
            return result;
        }

        try
        {
            Path tmpDir = Files.createTempDirectory("alobby_");
            if (tmpDir == null)
            {
                displayError(getMessage(ERROR_TEMPORAERES_VERZEICHNIS));
                return result;
            }
            File tmpAlobbyDir = tmpDir.toFile();

            try
            {
                result = downloadSignedFileToDir(url, tmpAlobbyDir);
            }
            catch (UserAbortException e)
            {
                tmpAlobbyDir.deleteOnExit();
                return result;
            }
            if (result == null)
            {
                displayError(getMessage(ERROR_HERUNTERLADEN));
                tmpAlobbyDir.deleteOnExit();
                return result;
            }
        }
        catch (IOException e)
        {
            Test.outputException(e);
        }
        return result;
    }

    private File[] downloadSignedFileToDir(URL url, File directory)
    {
        File[] result = new File[2];
        URL urlForSignature;

        URLConnection conn = null;
        try
        {
            conn = url.openConnection();
            conn.getContentLength(); //resolve any redirects
            urlForSignature = new URL(conn.getURL().toExternalForm() + ".asc");
        }
        catch (Exception e)
        {
            Test.outputException(e);
            return null;
        }

        result[0] = downloadFileToDir(conn.getURL(), directory);
        result[1] = downloadFileToDir(urlForSignature, directory);

        if (result[0] == null || result[1] == null)
        {
            return null;
        }
        return result;
    }
}
