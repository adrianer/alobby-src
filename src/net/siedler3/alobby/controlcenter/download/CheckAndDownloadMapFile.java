/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.controlcenter.download;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.util.S3Support;
import net.siedler3.alobby.util.httpRequests;

/**
 * Download a S3-Map from MapBase.
 * Including check if S3-Map are local available.
 * If not download the map from MapBase.
 * Otherwise check whether the MD5-Hash of file are identical to the MapBase-Map-Hash.
 * 
 * @author Zwirni
 *
 */
public class CheckAndDownloadMapFile {
	
	/**
	 * The file with path, e.g. User\river.map
	 */
	private String mapFile = "";
	
	/**
	 * The filename, e.g. river.map
	 */
	private String mapFilename = "";
	
	/**
	 * Marker if map-File exists local.
	 */
	private boolean mapFileExists = false;

	/**
	 * SettlersLobby-Object
	 */
	private SettlersLobby settlersLobby;
	
	/**
	 * Return-value for this class
	 */
	private boolean successMessage = false;

	/**
	 * Show optional no message
	 */
	private boolean noMessage = false;
	
	/**
	 * Constructor
	 * 
	 * @param settlersLobby
	 */
	public CheckAndDownloadMapFile( SettlersLobby settlersLobby ) {
		this.settlersLobby = settlersLobby;
	}
	
	/**
	 * Set the Map-Name as String incl. S3-specific directory (User / Multi).
	 * E.g.: USER\river.map
	 */
	public void setMap( String mapname ) {
		
		if( mapname.length() > 0 ) {
			// set filename only if its not "Random" or "Multi"
			if( !mapname.contains(ALobbyConstants.PATH_RANDOM + File.separator) && !mapname.contains(ALobbyConstants.PATH_S3MULTI + File.separator) ) {
				// filename including absolute path
				this.mapFile = this.settlersLobby.getS3MapDirectory().toString() + File.separator + mapname;
				
				// filename without any path
				this.mapFilename = mapname.replace(ALobbyConstants.PATH_USER + File.separator, "").replace(ALobbyConstants.PATH_S3MULTI + File.separator, "");
				
				// check if map really not exists local as file
				File file = new File(this.mapFile);
				this.mapFileExists = file.exists();
			}
			else {
				// set success to true for random- and multi-maps
				this.successMessage = true;
			}
		}
	}
	
	/**
	 * Check the file.
	 * 
	 * Step 1: 	Get data for this mapfile from MapBase.
	 * Step 2: 	Check the response:
	 * 		 	-> if file exists local 
	 */
	public boolean checkMapFile() {
		if( this.mapFilename.length() > 0 ) {
			
			// do not check the map if this is a savegame or a random
			if( this.mapFilename.contains(ALobbyConstants.PATH_RANDOM + File.separator) || this.mapFilename.startsWith(ALobbyConstants.LABEL_SAVE_GAME) ) {
				return this.successMessage;
			}
			
			boolean download = true;
			String downloadUrl = "";
			String mapBaseMd5 = "";
			
			// get file-info from mapBase independently whether the map-file is local available or not
			String response = "";
			
			// check whether the md5 of the local file is the same as on mapbase
        	httpRequests httpRequests = new httpRequests();
        	// -> set an parameter
        	httpRequests.addParameter("json", 1);
        	// -> ignore security-token
        	httpRequests.setIgnorePhpSessId(true);
        	// -> set the url
        	String url = "";
			try {
				// encode the searchstring and add it to the url
				url = settlersLobby.config.getDefaultGetMapMd5() + URLEncoder.encode(this.mapFilename, "UTF-8");
				httpRequests.setUrl(url);
				// -> get the response-String
				response = httpRequests.doRequest();
				
	    		if( !httpRequests.isError() ) {
	    			if( response.length() > 0 ) {
		    			// compare the md5-values
		    			response = response.replace("\r\n", "").trim();
		    			if( response.length() > 0 ) {
		    				String[] responseArray = response.split(";");
		    				if( responseArray.length >= 2 ) {
		    					mapBaseMd5 = responseArray[0];
		    					for ( int f = 1; f < responseArray.length; f++ ) {
		    						if( downloadUrl.length() > 0 ) {
		    							downloadUrl = downloadUrl + ";";
		    						}
		    						downloadUrl = downloadUrl + responseArray[f];
		    					}
		    				}
		    			}
	    			}
	    			else {
	    				// if the response is empty, than the map does not exists
	    				// on MapBase, but a game with this map should be started
	    				this.successMessage = false;
	    			}
	    		}	   
			} catch (UnsupportedEncodingException e2) {
				e2.getStackTrace();
			} catch (IOException e) {
				e.getStackTrace();
			}
						
			if( this.mapFileExists ) {
				if( mapBaseMd5.length() > 0 && mapBaseMd5.equals(S3Support.getMD5Checksum(this.mapFile).toLowerCase()) ) {
					this.successMessage = true;
					download = false;
				}
			}
			 		
			if( download && downloadUrl.length() > 0 ) {
				// download the file
				MapDownload mapDownload = new MapDownload(this.settlersLobby);
				mapDownload.overwriteFileWithoutHint(this.mapFileExists);
				return mapDownload.downloadFile(this.mapFilename.replace(".map", ""), this.noMessage);
			}
			
		}
		return this.successMessage;
	}

	/**
	 * Set that only a simple message should be shown if the map has been downloaded
	 * 
	 * @param b
	 */
	public void setNoMessage(boolean noMessage) {
		this.noMessage = noMessage;
	}
	
}