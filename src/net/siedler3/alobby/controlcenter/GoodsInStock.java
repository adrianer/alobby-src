package net.siedler3.alobby.controlcenter;

import net.siedler3.alobby.s3gameinterface.S3Language;

public enum GoodsInStock
{
    Vorgabe(0),
    Wenig(1),
    Mittel(2),
    Viel(3);

    private int index;
    private GoodsInStock(int index)
    {
        this.index = index;
    }

    public int getIndex()
    {
        return index;
    }

    public String getLanguageString()
    {
        S3Language lang = S3Language.getInstance();
        String key = lang.goodsInStock[index];
        return key;
    }
    public String getLanguageMiniChatString()
    {
        S3Language lang = S3Language.getInstance();
        String key = lang.goodsInStockMiniChat[index];
        return key;
    }

    public static GoodsInStock getGoodsInStockByIndex(int index)
    {
        for (GoodsInStock goods : values())
        {
            if (goods.index == index)
            {
                return goods;
            }
        }
        return null;
    }
}