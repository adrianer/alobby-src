/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.s3gameinterface;

import java.awt.event.KeyEvent;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.nativesupport.NativeFunctions;

public class AutoSaveAbortKey implements Runnable
{
	private boolean isRunning = true;

    private AutoSaveTask autoSaveTask;

    public AutoSaveAbortKey(AutoSaveTask autoSaveTask)
    {
    	this.autoSaveTask = autoSaveTask;
    }

    public void startMonitoring()
    {
        SettlersLobby.executor.execute(this);
    }

    public void stopMonitoring()
    {
    	setRunning(false);
    }

    @Override
    public void run()
    {
        //call this once to delete any result between the last call to isPressed and upcoming ones.
        //if someone presses space while chatting between to savepoints, this might be visible
        //in the first call to isPressed (LSB set to one)
        NativeFunctions.getInstance().isPressed(KeyEvent.VK_SPACE);
    	while (isRunning())
    	{
    		if (NativeFunctions.getInstance().isPressed(KeyEvent.VK_SPACE))
    		{
    		    setRunning(false);
    		    autoSaveTask.abort();
    		}
    	}
    }

    private synchronized boolean isRunning()
    {
        return isRunning;
    }

    private synchronized void setRunning(boolean isRunning)
    {
        this.isRunning = isRunning;
    }
}
