/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.s3gameinterface;

import net.siedler3.alobby.controlcenter.GoodsInStock;
import net.siedler3.alobby.controlcenter.OpenGame.MiscType;
import net.siedler3.alobby.util.KeyBlockStarter;

import java.util.Calendar;
import java.util.TimeZone;

public interface GameStarter {
	
	// Marker if s3 is running
	public boolean isRunning = false;
	
	// Object of s3Starter
	public S3Starter s3Starter = null;

    /**
     * Startoptions for the gamestarter.
     * This object is unique for either way to start s3.
     *
     * @author jim
     *
     */
    class StartOptions
    {
    	// Playername
        private String playerName;
        
        // Game-name
        private String gameName;
        
        // Map-name
        private String mapName;
        
        // Map-name in its original 
        private String mapNameOriginal;

        // Marker if this is a league-game
        private boolean leagueGame;

        // Object for the choosen goods
        private GoodsInStock selectGoodsInStock;

        // Marker if random-position has been choosed.
        private boolean selectRandomPosis;

        // Marker if s3Starter should navigate to minichat.
        private boolean selectMiniChat;

        // Marker if this should be a WiMo 
        private boolean selectWiMo;

        // IP of the game.
        private String ip;

        // Time where the game has been created
        private long createTime;
        
        // The choosen goods as integer-value
        private int goods;
        
        // Id if it is a tournament-game
        private int tournamentid;
        
        // Name of the tournament
        private String tournamentname;
        
        // Round of the tournament
        private int round;
        
        // Group in  the tournament
        private int groupnumber;
        
        // Matchnumber in the tournament
        private int matchnumber;
        
        // List of players in this game (used for tournament)
        private String playerlist;

        // max player count for this game
		private int maxPlayerNumber;

        private MiscType miscType;

        /**
         * Constructor to set the startoptions in the object for host.
         *
         * @param playerName Name des Spielers
         * @param gameName Name des Spiels
         * @param mapName Kartenname wie er vom MapChoosingDialog geliefert wird.
         * Z.B. "User\alien.map" oder falls es ein Savegame ist "Savegame\*.mps"
         * @param selectGoodsInStock Warenbestand wählen
         * @param selectRandomPosis Zufallspositionen wählen
         * @param selectMiniChat bin in Minichat navigieren
         * @param selectWiMo to start a WiMo-Game
         * @param matchnumber
         * @param groupnumber
         * @param round
         * @param groupnumber
         * @param matchnumber
         * @param playerlist
         * @param mapNameOriginal
         * @param maxPlayerNumber
         * @param miscType
         */
        public StartOptions(String playerName, String gameName, String mapName,
                            GoodsInStock selectGoodsInStock, boolean selectRandomPosis,
                            boolean selectMiniChat, boolean isLeagueGame, boolean selectWiMo, int tournamentid, String tournamentname,
                            int round, int groupnumber, int matchnumber, String playerlist, String mapNameOriginal, int maxPlayerNumber, MiscType miscType)
        {
            this.playerName = playerName;
            this.gameName = gameName;
            this.mapName = mapName;
            this.selectGoodsInStock = selectGoodsInStock;
            this.selectRandomPosis = selectRandomPosis;
            this.selectMiniChat = selectMiniChat;
            this.leagueGame = isLeagueGame;
            this.selectWiMo = selectWiMo;
            this.tournamentid = tournamentid;
            this.tournamentname = tournamentname;
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            this.createTime = calendar.getTimeInMillis();
            this.round = round;
            this.groupnumber = groupnumber;
            this.matchnumber = matchnumber;
            this.playerlist = playerlist;
            this.mapNameOriginal = mapNameOriginal;
            this.maxPlayerNumber = maxPlayerNumber;
            this.miscType = miscType;
        }

        /**
         *  Constructor to set the startoptions in the object for players.
         * 
         * @param playerName
         * @param ip
         * @param mapName
         * @param goods
         * @param league
         * @param tournamentid
         * @param tournamentname
         * @param miscType 
         */
        public StartOptions(String playerName, String ip, String mapName, int goods, boolean league, int tournamentid, String tournamentname, MiscType miscType )
        {
            this.playerName = playerName;
            this.ip = ip;
            this.mapName = mapName;
            this.goods = goods;
            this.leagueGame = league;
            this.tournamentid = tournamentid;
            this.tournamentname = tournamentname;
            this.miscType = miscType;
        }

        /**
         * Return the GoodsInStock for this game.
         * 
         * @return GoodsInStock
         */
        public GoodsInStock getSelectGoodsInStock()
        {
            return selectGoodsInStock;
        }

        /**
         * Get the Goods as integer-value.
         * 
         * @return integer
         */
        public int getGoods() {
            return this.goods;
        }

        /**
         * Set the GoodsInStock for this game.
         * 
         * @param selectGoodsInStock
         */
        public void setSelectGoodsInStock(GoodsInStock selectGoodsInStock)
        {
            this.selectGoodsInStock = selectGoodsInStock;
        }

        /**
         * Return if random-position has been choosed for this game.
         * 
         * @return
         */
        public boolean isSelectRandomPosis()
        {
            return selectRandomPosis;
        }

        /**
         * Set if the selected random position.
         * 
         * @param selectRandomPosis
         */
        public void setSelectRandomPosis(boolean selectRandomPosis)
        {
            this.selectRandomPosis = selectRandomPosis;
        }

        /**
         * Return if navigation to minichat has been activated.
         * 
         * @return
         */
        public boolean isSelectMiniChat()
        {
            return selectMiniChat;
        }

        /**
         * Set the marker is navigation to minchat should be used.
         * 
         * @param selectMiniChat
         */
        public void setSelectMiniChat(boolean selectMiniChat)
        {
            this.selectMiniChat = selectMiniChat;
        }

        /**
         * Return the name of the host.
         * 
         * @return String
         */
        public String getPlayerName()
        {
            return playerName;
        }

        /**
         * Set the name of the host.
         * 
         * @param playerName
         */
        public void setPlayerName(String playerName)
        {
            this.playerName = playerName;
        }

        /**
         * Return the mapname.
         * 
         * @return String
         */
        public String getMapName()
        {
            return mapName;
        }

        /**
         * Return the mapname in its original.
         * 
         * @return String
         */
        public String getMapNameOriginal()
        {
            return mapNameOriginal;
        }

        /**
         * Set the mapname.
         * 
         * @param mapName
         */
        public void setMapName(String mapName)
        {
            this.mapName = mapName;
        }

        /**
         * Return the IP of the host.
         * 
         * @return String
         */
        public String getIp()
        {
            return ip;
        }

        /**
         * Set the IP of the host.
         * 
         * @param ip
         */
        public void setIp(String ip)
        {
            this.ip = ip;
        }

        /**
         * Get the game-name.
         * 
         * @return String
         */
        public String getGameName()
        {
            return gameName;
        }

        /**
         * Set the game-name.
         * 
         * @param gameName
         */
        public void setGameName(String gameName)
        {
            this.gameName = gameName;
        }

        /**
         * Return if this is a league-game.
         * 
         * @return boolean	true if it is a league-game.
         */
        public boolean isLeagueGame()
        {
            return leagueGame;
        }

        /**
         * Return the tournament-ID.
         * 
         * @return
         */
        public int getTournamentId() {
            return this.tournamentid;
        }

        /**
         * Return the tournament-name.
         * 
         * @return
         */
        public String getTournamentName() {
            return this.tournamentname;
        }

        /**
         * Check whether WiMo is activated or disabled.
         */
        public boolean isWiMo() {
            return selectWiMo;
        }

        /**
         * Get the time of the moment this game is created.
         */
        public long getCreationTime() {
            return this.createTime;
        }

        /**
         * Return the tournament groupnumber.
         * 
         * @return int
         */
        public int getTournamentGroupnumber() {
            return this.groupnumber;
        }

        /**
         * Return the tournament-matchnumber.
         * 
         * @return int
         */
        public int getTournamentMatchnumber() {
            return this.matchnumber;
        }

        /**
         * Return the tournament-round.
         * 
         * @return int
         */
        public int getTournamentRound() {
            return this.round;
        }

        /**
         * Return the player-list as String
         * 
         * @return String
         */
        public String getTournamentPlayerlist() {
            return this.playerlist;
        }

        /**
         * Return the max player number for this game.
         * 
         * @return
         */
        public int getMaxPlayerNumber() {
        	return this.maxPlayerNumber;
        }
        
        /**
         * Return the miscType for this game.
         * 
         * @return
         */
        public MiscType getMisc() {
            return this.miscType;
        }
        
    }

    /**
     * Join a game with given options.
     * 
     * @param startOptions
     * @param hostUserName
     * @param mapName
     * @param goods
     * @param league
     * @param tournamentid
     * @param tournamentname
     */
    void joinGame(final StartOptions startOptions, final String hostUserName, final String mapName, final int goods, final boolean league, final int tournamentid, final String tournamentname );
    
    /**
     * Create a game with given options.
     * 
     * @param startOptions
     */
    void createGame(final StartOptions startOptions);
    
    /**
     * Stop a game.
     * 
     * @throws Exception
     */
    void stopGame() throws Exception;
    
    /**
     * Return the maximum playernumber for this game.
     * 	
     * @return int
     */
    int getMaximumPlayerNumber();
    
    /**
     * Return if s3 is running.
     * 
     * @return boolean
     */
    boolean isRunning();
    
    /**
     * Return if s3 is ready.
     * 
     * @return boolean
     */
    boolean isReady();
    
    /**
     * Return if the host is away.
     * 
     * @return boolean
     */
    boolean isHostAway();
    
    /**
     * Return the s3Starter-object
     * 
     * @return S3Starter
     */
	S3Starter getS3Starter();
	
	/**
	 * Return the keyblocker-object
	 * 
	 * @return KeyBlockStarter
	 */
	KeyBlockStarter getKeyBlockStarter();
	
	/**
	 * Return if s3 is running.
	 * 
	 * @return boolean
	 */
	boolean isS3Running();
	
	/**
	 * Set s3 as running.
	 * 
	 * @param isRunning
	 */
	void setRunning(boolean isRunning);
}
