/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.s3gameinterface;

import java.awt.AWTException;
import java.awt.Robot;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;


/**
 * Überwacht den Host und meldet Spieleranzahländerungen und Spielstart.
 * 
 * @author Stephan Bauer
 * @version 0.1
 */
public abstract class ObserverFramework extends Thread
{
	protected Robot robot;
	protected SettlersLobby settlersLobby;
	protected int currentEdition;
	protected PixelColorChecker miniChat;
	protected PixelColorChecker[] startColor;
	protected PixelColorChecker row;
	protected PixelColorChecker gesperrt;
	protected PixelColorChecker kick;
	protected PixelColorChecker computer;
	protected PixelColorChecker amazonen;
	protected int rowY[] = new int[20];
	protected int maxPlayerNumber;
	protected int currentPlayerNumber;
	protected GameStarterVanilla gameStarter;
	protected ImageChecker miniChatVisible;
	protected S3InGameCheck s3InGameCheck;

	protected ObserverFramework(GameStarterVanilla gameStarter)
	{
		this.gameStarter = gameStarter;
		ThreadCommunicator threadCommunicator;
		try
		{
			threadCommunicator = ThreadCommunicator.getInstance();

			robot = threadCommunicator.getRobot();
			settlersLobby = threadCommunicator.getActionListener();
			currentEdition = threadCommunicator.getCurrentEdition();

			// Erkennt deutsche, englische, französische und polnische Version:
			miniChat = new PixelColorChecker(robot, 255, 223, 0, 503, 28);
			miniChat.add(239, 223, 24, 333, 40);
			miniChat.add(82, 117, 148, 536, 593);
			miniChat.setScreenSizeS3Menu();

			startColor = new PixelColorChecker[4];
			startColor[GameStarterVanilla.ORANGE_AND_GREEN] = new PixelColorChecker(
					robot, 255, 93, 24, 1, 1);
			startColor[GameStarterVanilla.LILA] = new PixelColorChecker(robot, 140,
					105, 255, 0, 0);
			startColor[GameStarterVanilla.GOLD] = new PixelColorChecker(robot, 165,
					150, 82, 0, 0);
			startColor[GameStarterVanilla.RED] = new PixelColorChecker(robot, 181,
					0, 16, 1020, 2);
			for (PixelColorChecker tmp : startColor)
			{
			    tmp.setScreenSizeS3Menu();
			}
			
			s3InGameCheck = new S3InGameCheck();
			
			//die linke obere Ecke und die rechte untere Ecke von einem Button
			//der die Verbindungsqualität anzeigt, markiert eine Zeile
			//die Farbe links oben kommt ausschliesslich in der Ecke links oben vor
			//ansonsten würde es nicht funktionieren
			row = new PixelColorChecker(robot, 123, 125, 140, 0, 0);
			row.add(0, 0, 0, 23, 15);
			row.setScreenSizeS3Menu();
			
			//Ecke oben links und ein Pixel auf dem geschlossenen Button, auch diese
			//Kombination ist eindeutig
			gesperrt = new PixelColorChecker(robot, 123, 125, 140, 0, 0);
			gesperrt.add(198, 190, 181, 13, 5);
			gesperrt.setScreenSizeS3Menu();
			
			kick = new PixelColorChecker(robot, 255, 0, 0, 11, 7);
			kick.setScreenSizeS3Menu();
			computer = new PixelColorChecker(robot, 255, 223, 0, 72, 6);
			computer.setScreenSizeS3Menu();
			amazonen = new PixelColorChecker(robot, 148, 60, 0, 277, 46);
			amazonen.setScreenSizeS3Menu();
			
			createImageCheckers();
		} catch (AWTException e)
		{
			// Tue nichts, getInstance() muss eine Referenz zurückliefern, wäre
			// dies nicht der Fall, hätte der GameStarter schon abgebrochen und
			// dieser Thread wäre erst gar nicht gestartet worden.
		}
	}

	protected int getMaxPlayerNumber() throws Exception
	{
		int erg = 0;
		if (!isMiniChatVisible())
		{
			throw new Exception();
		}
		try
		{
			gesperrt.storeScreenCapture();
			for (int i = 0; (i < 20) && (rowY[i] != 0); i++)
			{
				//Die Erkennung eines Computerspielers funktioniert nicht zuverlässig,
				//im Gegenteil, sie kann sogar normale Spieler für Computerspieler halten
				//daher wird für die MaxPlayerNumber nur auf die gesperrten Zeilen geschaut
				//if (!gesperrt.match(760, rowY[i]) && !computer.match(27, rowY[i]))
				if (!gesperrt.match(760, rowY[i]))
				{
					erg++;
				}
			}
		}
		finally
		{
			gesperrt.discardScreenCapture();
		}
		return erg;
	}

	protected int getCurrentPlayerNumber() throws Exception
	{
		int erg = 0;
		if (!isMiniChatVisible())
		{
			throw new Exception();
		}
		
		try
		{
			kick.storeScreenCapture();
			for (int i = 0; (i < 20) && (rowY[i] != 0); i++)
			{
				if (kick.match(736, rowY[i]))
				{
					erg++;
					//Test.output("++");
				}
			}
			//Test.output(Integer.toString(erg));
		}
		finally
		{
			kick.discardScreenCapture();
		}
		return erg;
	}

	protected void getRows()
	{
		int ii = 0;
		try
		{
			//when getRows is called, minichat must be visibile
			row.storeScreenCapture();
			for (int i = 105; (i < 525) && (ii < 20); i++)
			{
				if (row.match(308, i))
				{
					rowY[ii] = i;
					ii++;
				}
			}
			//Test.output(ii + " rows/players detected");
		}
		finally
		{
			row.discardScreenCapture();
		}
	}

	protected void observeTillMiniChat() throws InterruptedException
	{
		do
		{
			sleep(1000);
		} while (!isInterrupted() && !isMiniChatVisible());
		
		if (!isInterrupted())
		{
			Test.output("minichat reached");
			getRows();
			settlersLobby.actionMiniChatReached();
		}
	}

	protected void initPlayerNumbers()
	{
		Test.output("start");
		do
		{
			try
			{
				maxPlayerNumber = getMaxPlayerNumber();
				currentPlayerNumber = getCurrentPlayerNumber();
			} catch (Exception e)
			{
				// ignorieren, wird wiederholt
			}
		} while (!isInterrupted()
				&& (maxPlayerNumber == 0 || currentPlayerNumber == 0));
		//Test.output(String.format("%d/%d", currentPlayerNumber, maxPlayerNumber));
	}
	
    private void createImageCheckers()
    {
        //Sobald im oberen rechten Fenster irgendwo die Schriftfarbe erscheint, 
        //hat man den Minichat erreicht
        //prüfe den Bereich, an dem Karte stehen sollte
        miniChatVisible = new ImageChecker(robot, 500, 20, 50, 25, 
                ImageChecker.S3_FONT_COLOR);
        miniChatVisible.setScreenSizeS3Menu();
    }
    
    protected boolean isMiniChatVisible()
    {
        // es wird sowohl die alte als auch die neue Variante überprüft
        // der PixelColorChecker "miniChat" funktioniert bei Standardschrift 
        // und Größe,
        // wird eins von beiden variiert, greift er eventuell ins Leere.
        // Hier sollte der ImageChecker "miniChatVisible" trotzdem noch 
        // erfolgreich sein 
        return miniChat.match() || miniChatVisible.match();
    }

	abstract public void run();
	
}
