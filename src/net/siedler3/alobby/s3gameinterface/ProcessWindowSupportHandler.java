/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.s3gameinterface;

import java.awt.Dimension;
import java.awt.Toolkit;

import net.siedler3.alobby.controlcenter.Configuration;



/**
 * If Process Window Support is available, it may accessed by
 * the singleton object of this class.
 * Process Window Support currently allows to check if the
 * the window under observation is currently the foreground window.
 */
public class ProcessWindowSupportHandler
{
    private static ProcessWindowSupportHandler singleton;

    private IProcessWindowSupport processWindowSupport;

    public IProcessWindowSupport getProcessWindowSupport()
    {
        return processWindowSupport;
    }

    public synchronized void setProcessWindowSupport(IProcessWindowSupport processWindowSupport)
    {
        this.processWindowSupport = processWindowSupport;
    }

    /**
     * checks if the ProcessWindowSupport is currently active and if the window
     * we are currently observing is not the foreground window.
     * If this function returns true, we can skip all the pixel and image
     * checks.
     * @return true if Process is active and the window of interest is not
     * the foreground window.
     */
    public boolean isProcessWindowSupportActiveAndNotForegroundWindow()
    {
        if (!Configuration.getInstance().isActivateForegroundWindowCheck())
        {
            return false;
        }
        return processWindowSupport != null && (!processWindowSupport.isForegroundWindow());
    }


    public static ProcessWindowSupportHandler getInstance()
    {
        if (singleton == null)
        {
            singleton = new ProcessWindowSupportHandler();
        }
        return singleton;
    }

    public void removeProcessWindowSupport(IProcessWindowSupport processWindowSupport)
    {
        if (this.processWindowSupport == processWindowSupport);
        {
            setProcessWindowSupport(null);
        }
    }

    public boolean isScreenSizeCheckActiveAndScreenSizeEquals(Dimension requiredScreenSize)
    {
        if (!Configuration.getInstance().isActivateScreenSizeCheck())
        {
            return false;
        }
        Dimension currentScreenSize = Toolkit.getDefaultToolkit().getScreenSize();
        if (currentScreenSize.equals(requiredScreenSize))
        {
            return true;
        }
        return false;
    }

    public boolean isScreenSizeCheckActiveAndScreenSizeNotEquals(Dimension requiredScreenSize)
    {
        if (!Configuration.getInstance().isActivateScreenSizeCheck())
        {
            return false;
        }
        Dimension currentScreenSize = Toolkit.getDefaultToolkit().getScreenSize();
        if (!currentScreenSize.equals(requiredScreenSize))
        {
            return true;
        }
        return false;
    }

}
