/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.s3gameinterface;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.util.Enumeration;
import java.util.Vector;

import net.siedler3.alobby.controlcenter.Test;

/**
 * Kapselt Farbdaten, Bildschirmpositionen und Prüfungen.
 * @author Stephan Bauer (maximilius)
 * @version    0.1
 */
public class PixelColorChecker
{
    //auf gewissen Rechnern kann es nötig
    //sein, eine gewisse Abweichung der
    //Farbwerte zu erlauben
    public static int colorVariation = 0;

    private Vector<XYColor> colorVector;
    private Robot robot;
    private Dimension screensize = null;
    private BufferedImage screenCapture = null;
    
    /**
     * Konstruktor.
     *
     * @param robot
     *            Robot zum Fernsteuern.
     * @param r
     *            Rot erster Farbe.
     * @param g
     *            Grün erster Farbe.
     * @param b
     *            Blau erster Farbe.
     * @param x
     *            X-Position erster Farbe.
     * @param y
     *            Y-Position erster Farbe.
     */
    public PixelColorChecker(Robot robot, int r, int g, int b, int x, int y)
    {
        this.robot = robot;
        colorVector = new Vector<XYColor>();
        colorVector.addElement(new XYColor(r, g, b, x, y));
    }

    /**
     * Fügt eine Farbe hinzu.
     *
     * @param r
     *            Rot.
     * @param g
     *            Grün.
     * @param b
     *            Blau.
     * @param x
     *            X-Position.
     * @param y
     *            Y-Position.
     */
    public void add(int r, int g, int b, int x, int y)
    {
        colorVector.addElement(new XYColor(r, g, b, x, y));
    }

    /**
     * Überprüft, ob die geaddeten Farben an ihren Koordinaten, um x und y
     * verschoben auf dem Bildschirm vorhanden sind.
     *
     * @param x
     *            X-Verschiebung.
     * @param y
     *            Y-Verschiebung.
     * @return Boolean True im Übereinstimmungsfall, sonst false.
     */
    public boolean match(int x, int y)
    {
        boolean result = true;
        if (screenCapture != null)
        {
            return matchAgainstImage(x, y);
        }
        if (screensize != null)
        {
            if (ProcessWindowSupportHandler.getInstance().isScreenSizeCheckActiveAndScreenSizeNotEquals(screensize))
            {
                 //the current resolution is not the requested one, abort
                return false;
            }/* else {
                Test.output("Screen size is: " + screensize.width + "x" + screensize.height);
            }*/
        }

        if (ProcessWindowSupportHandler.getInstance().isProcessWindowSupportActiveAndNotForegroundWindow())
        {
            // The window we want to check is not visible
            // abort immediately
            Test.output("Window not visibile or not in the forground!");
            return false;
        }

        Enumeration<XYColor> e = colorVector.elements();
        XYColor xYColor;
        while (result && e.hasMoreElements())
        {
            xYColor = (XYColor) e.nextElement();

            result = match(xYColor.getColor(),
                           robot.getPixelColor(
                                   xYColor.getX() + x,
                                   xYColor.getY() + y));

            // Test.output("Pixel-color: " + robot.getPixelColor(xYColor.getX() + x, xYColor.getY() + y) + ", x:" + xYColor.getX() + ", y:" + xYColor.getY());
        }
        return result;
    }

    private boolean matchAgainstImage(int x, int y) 
    {
        boolean result = true;
        Enumeration<XYColor> e = colorVector.elements();
        XYColor xYColor;
        while (result && e.hasMoreElements())
        {
            xYColor = (XYColor) e.nextElement();

            result = match(xYColor.getColor(),
                           new Color(screenCapture.getRGB(
                                           xYColor.getX() + x,
                                           xYColor.getY()+ y)));
        }
        return result;
    }

    /**
     * Überprüft, ob die beiden Farben übereinstimmen.
     * Wenn {@code colorVariation} größer 0 ist, wird auch eine gewisse
     * Abweichung in den Farbwerten akzeptiert.
     *
     * @param c1
     * @param c2
     * @return true falls c1 und c2 die (evtl. im Rahmen der Abweichung)
     *         gleiche Farbe darstellen.
     */
    public static boolean match(Color c1, Color c2)
    {
        if (c1 == null || c2 == null)
        {
            //allow null references, which will never match
            return false;
        }
        if (colorVariation > 0)
        {
            //wenn Farbabweichung erlaubt ist, so sind auch
            //Farbwerte in Ordnung, die bis zu "colorVariation"-viele
            //Einheiten vom gefordeten Wert abweichen
            //colorVariation sollte daher nur kleine Werte haben (0-10)
            return Math.abs(c1.getRed() - c2.getRed()) <= colorVariation &&
                   Math.abs(c1.getGreen() - c2.getGreen()) <= colorVariation &&
                   Math.abs(c1.getBlue() - c2.getBlue()) <= colorVariation;
        }
        else
        {
            return c1.equals(c2);
        }
    }

    /**
     * Überprüft, ob die geaddeten Farben an ihren Koordinaten auf dem
     * Bildschirm vorhanden sind.
     *
     * @return Boolean True im Übereinstimmungsfall, sonst false.
     */
    public boolean match()
    {
        return match(0, 0);
    }


    /**
     * ruft {@code counter} mal pixelColorChecker.match() auf, bis true geliefert
     * wird. Zwischen den Versuchen wird wird {@code delay} ms lang gewartet.
     *
     * @param pixelColorChecker
     * @param counter Anzahl Versuche
     * @param delay Pause in ms zwischen den Versuchen
     * @return true falls die Pixelfarbe innerhalb der Anzahl Versuche erkannt wurde,
     *  sonst false
     * @throws InterruptedException
     */
    public static boolean waitUntilPixelColorMatch(PixelColorChecker pixelColorChecker,
            int counter, int delay) throws InterruptedException
    {
        boolean isMatch = false;
        for (int i = 0; i < counter; ++i)
        {
            isMatch = pixelColorChecker.match();
            if (isMatch)
            {
                break;
            }
            Thread.sleep(delay);
        }
        return isMatch;
    }

    public void setScreenSize(int width, int height)
    {
        this.screensize = new Dimension(width, height);
    }

    public void setScreenSizeS3Menu()
    {
        setScreenSize(800, 600);
    }
    
    public boolean storeScreenCapture() 
    {
        if (screensize == null)
        {
            return false;
        }
        if (ProcessWindowSupportHandler.getInstance().isScreenSizeCheckActiveAndScreenSizeNotEquals(screensize))
        {
            //the current resolution is not the requested one, abort
            return false;
        }

        if (ProcessWindowSupportHandler.getInstance().isProcessWindowSupportActiveAndNotForegroundWindow())
        {
            // The window we want to check is not visible
            // abort immediately
            return false;
        }
            
        this.screenCapture = robot.createScreenCapture(new Rectangle(screensize.width, screensize.height));
        return true;
    }
    
    public void discardScreenCapture()
    {
        this.screenCapture = null;
    }


}
