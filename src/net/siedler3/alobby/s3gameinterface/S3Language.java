/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.s3gameinterface;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import net.siedler3.alobby.controlcenter.GoodsInStock;
import net.siedler3.alobby.util.LocaleChanger;

/**
 * Diese Klasse enthält unterschiedliche Definitionen für
 * verschiedene Sprachen. Es sollen Deutsch, Englisch,
 * Polnisch, Französisch, Italienisch und Spanisch unterstützt werden,
 * zumindest was die Siedler3-Sprachversion betrifft.
 */
public class S3Language
{
    public enum Lang
    {
        DE,
        EN,
        PL,
        FR,
        IT,
        ES,
//        KO,
//        JA
        ;

        public static Lang getLangByIndex(int index)
        {
            return values()[index];
        }

    }

    public String languageIndicator;
    public String map;
    public String[] goodsInStock;
    public String[] goodsInStockMiniChat;

    public S3Language()
    {

    }

    private static final String goodsInStock_DE = "Warenbestand";
    private static final String goodsInStock_EN = "Goods";
    private static final String goodsInStock_PL = "Ilo\u015B\u0107 materia\u0142\u00f3w";
    private static final String goodsInStock_FR = "Fournitures "; //Leerzeichen am Ende
    private static final String goodsInStock_IT = "Bonus";
    private static final String goodsInStock_ES = "Mercanc\u00EDas";

    private static final S3Language LANG_DE = new S3Language();
    private static final S3Language LANG_EN = new S3Language();
    private static final S3Language LANG_PL = new S3Language();
    private static final S3Language LANG_FR = new S3Language();
    private static final S3Language LANG_IT = new S3Language();
    private static final S3Language LANG_ES = new S3Language();

    static
    {
        //------ German ------

        LANG_DE.languageIndicator = "Netzwerkart";

        LANG_DE.map = "Karte";

        LANG_DE.goodsInStock = new String[]{
                "Vorgabe",
                "Wenig",
                "Mittel",
                "Viel"
        };

        LANG_DE.goodsInStockMiniChat = new String[]{
                goodsInStock_DE + ": " + LANG_DE.goodsInStock[0],
                goodsInStock_DE + ": " + LANG_DE.goodsInStock[1],
                goodsInStock_DE + ": " + LANG_DE.goodsInStock[2],
                goodsInStock_DE + ": " + LANG_DE.goodsInStock[3]
        };

        //------ English ------

        LANG_EN.languageIndicator = "Type of Network";

        LANG_EN.map = "Map";

        LANG_EN.goodsInStock = new String[]{
                "Default",
                "Low",
                "Medium",
                "High"
        };

        LANG_EN.goodsInStockMiniChat = new String[]{
                goodsInStock_EN + ": " + LANG_EN.goodsInStock[0],
                goodsInStock_EN + ": " + LANG_EN.goodsInStock[1],
                goodsInStock_EN + ": " + LANG_EN.goodsInStock[2],
                goodsInStock_EN + ": " + LANG_EN.goodsInStock[3]
        };

        //------ Polish ------

        LANG_PL.languageIndicator = "Typ sieci";

        LANG_PL.map = "Mapa";

        LANG_PL.goodsInStock = new String[]{
                "Domy\u015Blna", //Domyslna
                "Ma\u0142o",     //Malo
                "\u015Arednia",  //Srednia
                "Du\u017Co"      //Duzo
        };

        LANG_PL.goodsInStockMiniChat = new String[]{
                goodsInStock_PL + ": " + LANG_PL.goodsInStock[0],
                goodsInStock_PL + ": " + LANG_PL.goodsInStock[1],
                goodsInStock_PL + ": " + LANG_PL.goodsInStock[2],
                goodsInStock_PL + ": " + LANG_PL.goodsInStock[3]
        };

        //------ French ------

        LANG_FR.languageIndicator = "Protocole";

        LANG_FR.map = "Carte "; //Leerzeichen am Ende

        LANG_FR.goodsInStock = new String[]{
                "Par D\u00e9faut", //Par Defaut
                "Peu",
                "Normal",
                "Beaucoup"
        };

        LANG_FR.goodsInStockMiniChat = new String[]{
                goodsInStock_FR + ": " + LANG_FR.goodsInStock[0],
                goodsInStock_FR + ": " + LANG_FR.goodsInStock[1],
                goodsInStock_FR + ": " + LANG_FR.goodsInStock[2],
                goodsInStock_FR + ": " + LANG_FR.goodsInStock[3]
        };

        //------ Italian ------

        LANG_IT.languageIndicator = "Tipo di connessione";

        LANG_IT.map = "Mappa";

        LANG_IT.goodsInStock = new String[]{
                "Standard",
                "Basso",
                "Medio",
                "Alto"
        };

        LANG_IT.goodsInStockMiniChat = new String[]{
                goodsInStock_IT + ": " + LANG_IT.goodsInStock[0],
                goodsInStock_IT + ": " + LANG_IT.goodsInStock[1],
                goodsInStock_IT + ": " + LANG_IT.goodsInStock[2],
                goodsInStock_IT + ": " + LANG_IT.goodsInStock[3]
        };

        //------ Spanish ------

        LANG_ES.languageIndicator = "Tipo de red";

        LANG_ES.map = "Mapa";

        LANG_ES.goodsInStock = new String[]{
                "X defecto",
                "Bajo",
                "Medio",
                "Alto"
        };

        LANG_ES.goodsInStockMiniChat = new String[]{
                goodsInStock_ES + ": " + LANG_ES.goodsInStock[0],
                goodsInStock_ES + ": " + LANG_ES.goodsInStock[1],
                goodsInStock_ES + ": " + LANG_ES.goodsInStock[2],
                goodsInStock_ES + ": " + LANG_ES.goodsInStock[3]
        };

      //-----------------------



        //muss dieselbe Reihenfolge haben wie die Sprachreihenfolge in Lang
        languageCheck = new String[] {
                LANG_DE.languageIndicator,
                LANG_EN.languageIndicator,
                LANG_PL.languageIndicator,
                LANG_FR.languageIndicator,
                LANG_IT.languageIndicator,
                LANG_ES.languageIndicator
        };

        // Array with possible language-string for tcp/ip-title in s3-window
        // Sources: 
        // - http://contents.driverguide.com/content.php?id=617384&path=ATI789_WHQL%2FDX81%2FDirectX.cab%2Fdplay.inf
        // - http://ftp.uma.es/Drivers/TVIDEO/ATI/128RAGE/WIN9X/DIRECTX6/DIRECTX/DPLAY.INF
        tcpIpDirectPlay = new String[]{
        		"Internet TCP/IP Connection For DirectPlay", //English
                "Internet-TCP/IP-Verbindung für DirectPlay", //German on WinXP
                "Internet TCP/IP-Verbindung für DirectPlay", //German on Win98
                "TCP/IP-Internetverbindung für DirectPlay", //German on ???
                "Connexion Internet TCP/IP pour DirectPlay", //French
                "Internet TCP/IP-verbinding voor DirectPlay", //Dutch
                "Internett-TCP/IP-tilkobling for DirectPlay", //Norwegian
                "Internet TCP/IP kapcsolat DirectPlayhez", // Hungarian
                "Conexão TCP/IP Internet para o DirectPlay", // Brazilian/Portugese
                "Internet TCP/IP spojení pro DirectPlay", // Czech
                "Connessione TCP/IP Internet per DirectPlay", // Italian
                "Internet TCP/IP-anslutning för DirectPlay", // Swedish
                "Conexión TCP/IP de Internet para DirectPlay" // Spanish
        };
        
        // label for player-row in statistic screen
        // TODO insert correct spellings for PL, FR, IT, ES
        playerLabel = new String[] {
        		"Spieler", // DE
                "Player", // EN
                "Player", // PL
                "Player", // FR
                "Player", // IT
                "Player" // ES
        };
    }

    public static final String[] languageCheck;

    public static final String[] tcpIpDirectPlay;
    
    public static final String[] playerLabel;

    public static Lang getCurrentS3Language(File s3Path)
    {
    	int locale = LocaleChanger.readLocale(s3Path);
        switch(locale)
        {
            case 1:
                return Lang.EN;
            case 2:
                return Lang.IT;
            case 3:
                return Lang.FR;
            case 4:
                return Lang.PL;
            case 5:
                return Lang.ES;
//            case 6:
//                return Lang.KO;
//            case 7:
//                return Lang.JA;
            case 0:
            case -1:
            default:
                return Lang.DE;
        }
    }
    

    private static Lang currentLang = getCurrentS3Language(null);

    /**
     * gebe das zu {@code lang} gehörende Language Objekt zurück.
     *
     * @param lang
     * @return
     */
    public static S3Language getInstance(Lang lang)
    {
        switch (lang)
        {
            case EN:
                return LANG_EN;
            case PL:
                return LANG_PL;
            case FR:
                return LANG_FR;
            case IT:
                return LANG_IT;
            case ES:
                return LANG_ES;
            case DE:
            default:
                return LANG_DE;
        }
    }

    /**
     * Benutzt den Wert in currentLang als Sprachversion
     * für Siedler3. Default ist DE.
     * Nach dem Starten von Siedler3 wird eine Versionerkennung
     * durchgeführt, die den Wert von currentLang ändern kann.
     *
     * @return Language Objekt
     */
    public static S3Language getInstance()
    {
        return getInstance(currentLang);
    }

    /**
     * Bestimmt die Sprachversion von S3 und gibt das zugehörige Language
     * Objekt zurück.
     * Default ist DE.
     * @return Language Objekt
     */
    public static S3Language getInstance(boolean useRegistryValue)
    {
        return getInstance(getCurrentS3Language(null));
    }

    public static int getGoodsInStockMiniChatIndex(Lang lang, String reference)
    {
        S3Language tmpLang = getInstance(lang);
        for (int i = 0; i < tmpLang.goodsInStockMiniChat.length; ++i)
        {
            if (tmpLang.goodsInStockMiniChat[i].equals(reference))
            {
                return i;
            }
        }
        return -1;
    }
    
    /**
     * Get the goodsInStock-Index by language and a reference-value (e.g. "Low").
     * 
     * @param lang
     * @param reference
     * @return
     */
    private int getGoodsInStock(Lang lang, String reference)
    {
        S3Language tmpLang = getInstance(lang);
        for (int i = 0; i < tmpLang.goodsInStock.length; ++i)
        {
            if (tmpLang.goodsInStock[i].equals(reference))
            {
                return i;
            }
        }
        return -1;
    }
    
    /**
     * Get the goodsInStock-object for a string in unknown language (e.g. "Low").
     * 
     * @return GoodsInStock
     */
    public GoodsInStock getGoodsInStockByUnknownLanguage( String value ) {
    	// get all available languages as list
	    ArrayList<Lang> langs = new ArrayList<S3Language.Lang>(Arrays.asList(S3Language.Lang.values()));
	    // loop through the list of languages
	    for( Lang lang : langs) {
	    	// get the goodsinstock-index for this language
	    	int langIndex = getGoodsInStock(lang, value);
	    	// if the index is >= 0 than we have found the correct language 
	    	if( langIndex >= 0 ) {
	    		// get the goodsinstock for language of the actual user
	    		return GoodsInStock.getGoodsInStockByIndex(langIndex);
	    	}
	    }
	    return null;
    }

    public static Lang getCurrentLanguage()
    {
        return currentLang;
    }

    public static void setCurrentLanguage(Lang lang)
    {
        currentLang = lang;
    }

    // Polish characters:
    // Small a with acute: \u0105
    // Small c with acute: \u0107
    // Small e with acute: \u0119
    // Small l with dash:  \u0142
    // Small o with acute: \u00F3
    // Small s with acute: \u015B
    // Small z with dot:   \u017C
    // Small z with acute: \u017a

    // French characters:
    // Small e with acute: \u00e9

    // Spanish characters:
    // Small i with acute: \u00ED
}
