/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.s3gameinterface;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;

import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.util.TimerTaskCompatibility;


public class StopToolStoringTask extends TimerTaskCompatibility
{
    private static final String path = ALobbyConstants.PACKAGE_PATH + "/s3gameinterface/img/2/";
    private static final String[] filenames = {
      "Angel.bmp",
      "Axt.bmp",
      "Hammer.bmp",
      "Saege.bmp",
      "Schaufel.bmp",
      "Sense.bmp",
      "Spitzhacke.bmp"
    };
    private static Robot robot;
    private static BufferedImage[] imgTools = null;
    private static BufferedImage imgGoods = null;
    private static BufferedImage imgGoodsSelected = null;
    private static BufferedImage imgStop = null;
    private static final Point buttonStopStoring = new Point(58, 670);

    private static final Dimension[] supportedScreenSizes = {
            new Dimension(1024, 768),
            new Dimension(1366, 768)
        };

    private static boolean isInitialised = false;


    static
    {
        try
        {
            robot = ThreadCommunicator.getInstance().getRobot();
            imgTools = new BufferedImage[filenames.length];
            for (int i = 0; i < filenames.length; ++i)
            {
                //dies sind die icons der einzelen Werkzeuge
                //(Hammer, Schaufel, Axt etc.)
                imgTools[i] = ImageIO.read(
                        StopToolStoringTask.class.getResourceAsStream(
                            path + filenames[i]));
            };
            //das Waren icon
            imgGoods = ImageIO.read(
                        StopToolStoringTask.class.getResourceAsStream(
                            path + "Waren.bmp"));
            //das Waren icon, wenn es selektiert ist ist
            imgGoodsSelected = ImageIO.read(
                    StopToolStoringTask.class.getResourceAsStream(
                        path + "Waren_.bmp"));

            //die Hand für den Einlagerungsstop
            imgStop = ImageIO.read(
                        StopToolStoringTask.class.getResourceAsStream(
                            path + "Stop.bmp"));
            isInitialised  = true;
        }
        catch (Exception e)
        {
            isInitialised  = false;
            Test.outputException(e);
        }
    }

    private ImageChecker[] imageCheckerTools = null;
    private MultiImageChecker imageCheckerGoods = null;
    private ImageChecker imageCheckerStop = null;
    private S3InGameCheck s3InGame = null;
    private boolean initialised = false;

    private SettlersLobby settlersLobby;

    /**
     * Task, der die Einlagerung der Werkzeuge stoppt, sobald S3 sichtbar ist.
     */
    public StopToolStoringTask(SettlersLobby settlersLobby)
    {
        this.settlersLobby = settlersLobby;
        if (isInitialised)
        {
            try
            {
                //für jedes icon wird jetzt ein ImageChecker erzeugt,
                //mit den zugehörigen Koordinaten
                //Für die Werkzeuge wird das komplette Fenster durchsucht,
                //da sie an beliebiger Stelle stehen könnten,
                //die anderen werden nur an den Koordinaten gesucht, an denen
                //sie zu sehen sind, wenn S3 aktiv ist
                imageCheckerTools = new ImageChecker[imgTools.length];
                for (int i = 0; i < imgTools.length; ++i)
                {
                    imageCheckerTools[i] = new ImageChecker(
                            robot, 20, 370, 176, 267, imgTools[i]);
                }
                ImageChecker tmpImageChecker;

                imageCheckerGoods = new MultiImageChecker();
                tmpImageChecker = new ImageChecker(robot, 87, 241,
                        imgGoods.getWidth(), imgGoods.getHeight(), imgGoods);
                imageCheckerGoods.add(tmpImageChecker);

                tmpImageChecker = new ImageChecker(robot, 87, 241,
                        imgGoods.getWidth(), imgGoods.getHeight(), imgGoodsSelected);
                imageCheckerGoods.add(tmpImageChecker);


                imageCheckerStop = new ImageChecker(robot, 46, 661,
                        imgStop.getWidth(), imgStop.getHeight(), imgStop);

                s3InGame = new S3InGameCheck();
                initialised = true; //alle Vorbereitungen ok, der Task ist einsatzbereit
            }
            catch (Exception e)
            {
                Test.outputException(e);
            }
        }
    }

    @Override
    public void run()
    {
        if (!initialised)
        {
            Test.output("Task nicht initialisiert");
            cancel();
            disableOption();
            return;
        }
        try
        {
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            if (!supportedScreenSizes[0].equals(screenSize) && !supportedScreenSizes[1].equals(screenSize))
            {
                //die aktuelle Auflösung entspricht nicht der unterstützten,
                //der Task wird beendet, aber nicht gecancelt
                return;
            }
        }
        catch (Exception e)
        {
            Test.outputException(e);
            //unerwartete Exception, beenden
            return;
        }

        //Anfangserkennung nur mit einzelnen Pixeln, da die
        //Suche nach einem kompletten Image eventuell eher dazu neigt
        //Structured Exceptions von S3 beim Starten zu erzeugen
        //Die Erkennung eines einzelnen Pixels ist scheinbar weniger störend
        if (s3InGame.isInGame())
        {
            //sobald S3 im Spiel angekommen ist,
            //wird der Task beendet.
            //Anschliessend wird in einem extra Thread das Klicken erledigt,
            //damit der TimerThread nicht belastet wird.
            Test.output("match S3");
            cancel();
            SettlersLobby.executor.execute(new StopToolStoring());
            disableOption();
        }
    }

    /**
     * runnable, das die entsprechenden Mausklicks zum Stoppen
     * der Einlagerung ausführt.
     *
     * @author jim
     *
     */
    private class StopToolStoring implements Runnable
    {
        @Override
        public void run()
        {
            try
            {
                //initiale Pause
                Thread.sleep(100);
                //für den Fall, daß aus Versehen die S3 Erkennung
                //fehlerhaft war, da der user die richtige Auflösung
                //benutzt und an der überprüften Stelle die S3-entsprechende
                //Farbe stand, wird hier solange gewartet, bis der
                //Waren-Button sichtbar ist. Normalerweise ist das
                //schon in der ersten Iteration der Fall.
                //Für zuvor beschriebenen Ausnahmefall wird aber recht lange
                //versucht, den Button zu finden, bevor aufgegeben wird.
                ImageChecker goodsMatch =
                    imageCheckerGoods.waitUntilMatchFirst(1000, 100);
                if (goodsMatch == null)
                {
                    Test.output("Fehler");
                    return;
                }
                //aktuelle Mausposition merken
                Point currentPosition = GameStarterVanilla.getCurrentMousePosition();

                GameStarterVanilla.mouseClick(robot, goodsMatch.getMatchCenter());
                GameStarterVanilla.mouseClick(robot, 180, 310); //klicke Warentransport

                //Warten bis die Waren angezeigt werden, sobald der StopButton
                //sichtbar ist, sind auch die Waren zu sehen
                ImageChecker.waitUntilImageMatch(imageCheckerStop, 20, 100);

                //lokalisiere alle Werkzeug-icons auf dem Bildschirm
                for (ImageChecker imageChecker : imageCheckerTools)
                {
                    if (imageChecker.match())
                    {
                        //Werkzeug selektieren und Einlagerung stoppen
                        GameStarterVanilla.mouseClick(robot, imageChecker.getMatchCenter());
                        GameStarterVanilla.mouseClick(robot, buttonStopStoring);
                    }
                }

                GameStarterVanilla.mouseClick(robot, 180, 620); //Selektion aufheben
                GameStarterVanilla.mouseClick(robot, 20, 20); //Anzeige der Spielerfarben

                if (currentPosition != null)
                {
                    //zurück zur Ursprungsposition
                    robot.mouseMove(currentPosition.x, currentPosition.y);
                }
            }
            catch (InterruptedException e)
            {
                Thread.currentThread().interrupt();
                Test.outputException(e);
            }
        }
    }

    private void disableOption()
    {
        if(settlersLobby != null)
        {
            //Der Task ist beendet, die entsprechende Anzeige in der GUI
            //muss aktualisiert werden
            settlersLobby.setStopStoringTools(false);
            settlersLobby.updateSelectedIngameOptions();
        }
    }

}
