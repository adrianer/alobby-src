package net.siedler3.alobby.s3gameinterface;

import java.io.File;

import net.siedler3.alobby.controlcenter.Configuration;
import net.siedler3.alobby.controlcenter.OpenGame;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.controlcenter.state.UserInGameList;
import net.siedler3.alobby.util.GameDataFile;

/**
 * Observer-object for s3-games which are launched as host with s3launcher.
 * 
 * @author Zwirni
 */
public class S3LauncherObserverHost extends S3LauncherObserver {
	
	// Marker if host-info has been send
	private boolean hostInfoSend = false;
	
	// Values to check if playernumber has been changed
	private int oldPlayerNumber = 0;
	private int oldMaxPlayerNumber = 0;
	private long oldGameModeMask = 0;

	/**
	 * Constructor.
	 * 
	 * @param settlersLobby
	 * @param gameStarter
	 */
	protected S3LauncherObserverHost(SettlersLobby settlersLobby, GameStarterLauncher gameStarter) {
		super(settlersLobby, gameStarter);
	}
	
	/**
	 * Check actual observing-state depending on available values
	 * to start necessary commands to interact with aLobby as Host.
	 */
	@Override
	void checkState() {
		// send info about new game if minichat is reached and player number is available
		if( false != isInMiniChat && maxPlayerNumber > 0 && false == hostInfoSend ) {
			Test.output("send host info");

			settlersLobby.newGame(gameStarter.getStartOptions().getGameName(),
				gameStarter.getStartOptions().getMapName(), maxPlayerNumber, 1, isAmazAllowed,
				gameStarter.getStartOptions().getSelectGoodsInStock(), gameStarter.getStartOptions().isLeagueGame(), 
				isEconomyMode, gameStarter.getStartOptions().getTournamentId(), 
				gameStarter.getStartOptions().getTournamentName(), gameStarter.getStartOptions().getTournamentRound(), 
				gameStarter.getStartOptions().getTournamentGroupnumber(), gameStarter.getStartOptions().getTournamentMatchnumber(), 
				gameStarter.getStartOptions().getTournamentPlayerlist(),
				OpenGame.S3CEGameOption.parse(gameModeMask)
			);
			hostInfoSend  = true;
		}
		// send info about moment when host left the game
		// -> open game
		if( false != hasLeftGame && settlersLobby.getOpenHostGame() != null ) {
			settlersLobby.sendProgramMessageCloseGame(settlersLobby.getOpenHostGame());
			settlersLobby.actionCreateGameAborted();
			settlersLobby.getAwayStatus().onGameClosed();
			settlersLobby.getGameState().userHasLeftGame(false);
			settlersLobby.getGUI().getUserListWindow().displayUsers(new UserInGameList());
			settlersLobby.unselectGameInGui();
			settlersLobby.getGUI().getTeamSpeakPanel().startLoading();
		}
		// -> running game
		else if( false != hasLeftGame && settlersLobby.getRunningHostGame() != null ) {
			settlersLobby.sendProgramMessageCloseGame(settlersLobby.getRunningHostGame());
			settlersLobby.actionCreateGameAborted();
			settlersLobby.getAwayStatus().onGameClosed();
			settlersLobby.getGameState().userHasLeftGame(false);
			settlersLobby.getGUI().getUserListWindow().displayUsers(new UserInGameList());
			settlersLobby.removeGameFromRunningGames();
			settlersLobby.unselectGameInGui();
			settlersLobby.getGUI().getTeamSpeakPanel().startLoading();
		}
		// send info about moment when game has been started
		if( false != gameHasStarted && settlersLobby.getOpenHostGame() != null ) {
			settlersLobby.getOpenHostGame().setStartTime(System.currentTimeMillis());
			settlersLobby.actionGameStarted(gameStarter.getStartOptions());
		}
		// upload taken statistic screenshot
		if( !"".equals(statisticScreenshotPath) && settlersLobby.getRunningHostGame() != null ) {
			uploadStatisticScreenshot(statisticScreenshotPath);
			statisticScreenshotPath = "";
		}
		// send info about changed player
		if( maxPlayerNumber > 0 && playerNumber > 0 && hasPlayerNumberChanged() && (settlersLobby.getOpenHostGame() != null || settlersLobby.getRunningHostGame() != null)) {
			// send actual playernumber in channel
			settlersLobby.newCurrentPlayerNumber(playerNumber);
			// send max playernumber in channel
			settlersLobby.newMaxPlayerNumber(maxPlayerNumber);

			// send update of the userlist of this game
			settlersLobby.getOpenHostGame().resetPlayerList();
			settlersLobby.getOpenHostGame().addPlayer(playerList, false);
			for( int i = 0;i<removedPlayer.size();i++ ) {
				settlersLobby.getOpenHostGame().removePlayer(removedPlayer.get(i));
			}
			settlersLobby.scheduleSendMapUserList();

			// update the userlist in GUI
			userInGameList.setHostNick(settlersLobby.getNick());
			userInGameList.setMap(settlersLobby.getOpenHostGame().getMap());
			settlersLobby.getGUI().getUserListWindow().displayUsers(userInGameList);

			// save the actual values for next run
			oldPlayerNumber = playerNumber;
			oldMaxPlayerNumber = maxPlayerNumber;
		}

		if (settlersLobby.getOpenHostGame() != null && gameModeMask != oldGameModeMask) {
			settlersLobby.sendNewGameMode(gameModeMask);
			oldGameModeMask = gameModeMask;
		}
	}

	/**
	 * Upload statistic Screenshot incl. game-data and set link in chat after upload.
	 * 
	 * @param statisticScreenshotPath	the absolute path to the screenshot
	 */
	private void uploadStatisticScreenshot(String statisticScreenshotPath) {
		if( settlersLobby.getRunningHostGame() != null ) {
			this.playercount = settlersLobby.getRunningHostGame().getCurrentPlayer();
			if( settlersLobby.getRunningHostGame().isLeagueGame() ) {
				this.league = 1;
			}
			this.tournamentid = settlersLobby.getRunningHostGame().getTournamentId();
			this.round = settlersLobby.getRunningHostGame().getTournamentRound();
			this.matchnumber = settlersLobby.getRunningHostGame().getTournamentMatchnumber();
			this.groupnumber = settlersLobby.getRunningHostGame().getTournamentGroupnumber();
			this.wimo = settlersLobby.getRunningHostGame().isWimo();
			this.mapname = settlersLobby.getRunningHostGame().getPlainMap();
			this.goods = settlersLobby.getRunningHostGame().getGoods();
			
			// load the game-data file
			this.gameDataFile = new GameDataFile(settlersLobby, settlersLobby.getRunningHostGame());
			this.racelist = gameDataFile.get("usersWithRaces");
			this.startTime = gameDataFile.get("creationTime");
			
			// upload screenshot if it is activated in settings
			if( Configuration.getInstance().isActivateS3StatsScreenCapture() ) {
				performStatisticScreenshotUpload(new File(statisticScreenshotPath));
			}
		}
	}

	/**
	 * Check if player count for this game has been changed since last check.
	 * 
	 * @return
	 */
	private boolean hasPlayerNumberChanged() {
		if( oldPlayerNumber != playerNumber ) {
			return true;
		}
		if( oldMaxPlayerNumber != maxPlayerNumber ) {
			return true;
		}
		return false;
	}

}
