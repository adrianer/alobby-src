/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.s3gameinterface;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.nativesupport.NativeFunctions;

public class WindowFont
{
	/**
	 * erzeugt ein Bild, das den übergebenen Text in der Siedler-Menü-Schrift
	 * auf schwarzem Hintergrund enthält. Damit kann man es benutzen, um
	 * damit auf einem screenshot nach dem Text zu suchen (durch einen einfachen
	 * Bildervergleich, Pixel für Pixel). 
	 * @param text
	 * @return
	 */
	public static BufferedImage createReferenceImage(String text)
	{
	    BufferedImage result = null;
	    final Color c = ImageChecker.S3_FONT_COLOR;
	    try
	    {
	        File tmpfile = File.createTempFile("alobby_pic_", ".bmp");
	        NativeFunctions.getInstance().createReferenceBitmap(text, c.getRed(), c.getGreen(), c.getBlue(),
	        //createReferenceBitmap(text, c.getRed(), c.getGreen(), c.getBlue(),
	    	               tmpfile.getAbsolutePath());
	        result = ImageIO.read(tmpfile);
	        //deleteOnExit() zum debuggen benutzen, damit man sich
	        //zwischendurch die erzeugten Bilder anschauen kann
	        //tmpfile.deleteOnExit();
	        tmpfile.delete();
	    }
	    catch (Exception e) 
	    {
	        Test.outputException(e);
	        return null;
        }
	    return result;
	}
	
	public static List<BufferedImage> createReferenceImage(List<String> label)
	{
	    List<BufferedImage> images = new ArrayList<BufferedImage>();
	    
        BufferedImage result = null;
        for (int i = 0; i < label.size(); ++i)
        {
            String text = label.get(i);
            result = createReferenceImage(text);
            images.add(result);
        }
        return images;
	}
	
}
