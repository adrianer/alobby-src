/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.s3gameinterface;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Robot;
import java.awt.Toolkit;

import net.siedler3.alobby.controlcenter.Configuration;
import net.siedler3.alobby.controlcenter.Test;

/**
 * Checks if a game is already running (in S3).
 *
 */
public class S3InGameCheck
{
    private PixelColorChecker s3InGame;
    private PixelColorChecker s3InGame2;
    private PixelColorChecker s3InGame3;
    private PixelColorChecker s3InGame4;
    private PixelColorChecker s3InGame5;
    private boolean initialised = false;
    private static final Dimension[] POSSIBLE_S3_SCREENSIZES = {
        new Dimension(1366, 768),
        new Dimension(1024, 768),
        new Dimension(800, 600),
        new Dimension(640, 480)
    };

    public S3InGameCheck()
    {
        try
        {
            Robot robot = ThreadCommunicator.getInstance().getRobot();

            //1366x768
            s3InGame5 = new PixelColorChecker(robot, 33, 69, 16, 0, 0);
            s3InGame5.add(82, 109, 33, 0, 1);
            s3InGame5.add(189, 215, 57, 0, 3);
            s3InGame5.setScreenSize(1366, 768);

            //1024x768
            s3InGame = new PixelColorChecker(robot, 33, 69, 16, 0, 0);
            s3InGame.add(82, 109, 33, 0, 1);
            s3InGame.add(189, 215, 57, 0, 3);
            s3InGame.setScreenSize(1024, 768);

            //second check for 1024x768, due to windows8 bug with black bar at top
            s3InGame2 = new PixelColorChecker(robot, 33, 65, 90, 5, 30);
            s3InGame2.add(24, 56, 82, 5, 31);
            s3InGame2.add(33, 56, 74, 5, 32);
            s3InGame2.setScreenSize(1024, 768);

            //add a check for 800x600, in case the color matcher for start game
            //did not match
            s3InGame3 = new PixelColorChecker(robot, 33, 56, 66, 5, 30);
            s3InGame3.add(33, 48, 66, 5, 31);
            s3InGame3.add(24, 48, 66, 5, 32);
            s3InGame3.setScreenSize(800, 600);

            //640x480
            s3InGame4 = new PixelColorChecker(robot, 107, 125, 140, 5, 30);
            s3InGame4.add(74, 97, 107, 5, 31);
            s3InGame4.add(49, 69, 82, 5, 32);
            s3InGame4.setScreenSize(640, 480);

            initialised = true;
        }
        catch (AWTException e)
        {
            Test.outputException(e);
        }
    }

    /**
     * checks if a game is running.
     * @return true if a game is running.
     */
    public boolean isInGame()
    {
        if (!initialised)
        {
            return false;
        }
        if (Configuration.getInstance().isActivateScreenSizeCheck())
        {
            //check if the screen size is one of the possible S3 screen sizes.
            //if not we do not have to check any pixel colors.
            boolean screenSizeMatch = false;
            Dimension currentScreenSize = Toolkit.getDefaultToolkit().getScreenSize();
            for (Dimension tmp : POSSIBLE_S3_SCREENSIZES)
            {
                if (currentScreenSize.equals(tmp))
                {
                    screenSizeMatch = true;
                    break;
                }
            }
            if (!screenSizeMatch)
            {
                return false;
            }
        }

        //only true when all pixels match in any of the matchers
        boolean result = s3InGame.match() || s3InGame2.match() || s3InGame3.match() || s3InGame4.match() || s3InGame5.match();
        if (result)
        {
            Test.output("ingameCheck successful");
        } else {
            Test.output("ingameCheck unsuccessful");
        }
        return result;
    }
}
