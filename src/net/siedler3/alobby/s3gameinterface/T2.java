/**
 * 
 */
package net.siedler3.alobby.s3gameinterface;

/**
 * ermöglicht die Rückgabe von zwei Parametern.
 *
 * @param <E1>
 * @param <E2>
 */
public class T2< E1, E2 > 
{
    public final E1 e1;
    public final E2 e2;
    public T2( final E1 e1, final E2 e2 ) 
    {
         this.e1 = e1;
         this.e2 = e2;
    }
    
    public static < E1, E2 > T2< E1, E2 > t2( final E1 e1, final E2 e2 ) {
        return new T2< E1, E2 >( e1, e2 );
    } 
}