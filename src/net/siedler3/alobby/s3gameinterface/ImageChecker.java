/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.s3gameinterface;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.imageio.ImageIO;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.i18n.I18n;


public class ImageChecker
{
    public static final Color S3_FONT_COLOR = new Color(255,223,0);
    private Robot robot;
    private int x;
    private int y;
    private int width;
    private int height;
    private BufferedImage imageToMatch;
    private BufferedImage imageCapture;
    private BufferedImage userSuppliedImageCapture;
    //falls colorToMatchInCapture gesetzt ist, wird imageToMatch ignoriert
    private Color colorToMatchInCapture = null;

    private Point matchStart;
    private Point matchCenter;
    private boolean filterScreenCapture = false;
    private boolean adjustWidthOf2ndImageToFitInto1st = false;
    private String textInImage = null;
    private Dimension screensize = null;
    private boolean autoCleanup = true;
	private int i;

    private ImageChecker(Robot robot, int x, int y, int width, int height)
    {
        super();
        this.robot = robot;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    /**
     * Konstruktor um später imageToMatch innerhalb
     * der angegebenen Koordinaten zu finden.
     *
     * @param robot
     * @param x
     * @param y
     * @param width
     * @param height
     * @param imageToMatch
     */
    public ImageChecker(Robot robot, int x, int y, int width, int height,
            BufferedImage imageToMatch)
    {
        this(robot, x, y, width, height);
        this.imageToMatch = imageToMatch;
    }

    /**
     * Konstruktor um später imageToMatch innerhalb
     * der angegebenen Koordinaten zu finden.
     *
     * @param robot
     * @param x
     * @param y
     * @param width
     * @param height
     * @param imageToMatch
     * @param filterScreenCapture
     * @param adjustWidthOf2ndImageToFitInto1st
     * @param textInImage
     */
    public ImageChecker(Robot robot, int x, int y, int width, int height,
            BufferedImage imageToMatch, boolean filterScreenCapture,
            boolean adjustWidthOf2ndImageToFitInto1st, String textInImage)
    {
        this(robot, x, y, width, height, imageToMatch);
        setFilterScreenCapture(filterScreenCapture);
        setAdjustWidthOf2ndImageToFitInto1st(adjustWidthOf2ndImageToFitInto1st);
        setTextInImage(textInImage);
    }

    /**
     * erzeugt einen ImageChecker, der überprüfen kann, ob die übergebene
     * Farbe im Bereich mindestens einmal vorkommt.
     *
     * @param robot
     * @param x
     * @param y
     * @param width
     * @param height
     * @param colorToMatchInCapture
     */
    public ImageChecker(Robot robot, int x, int y, int width, int height,
            Color colorToMatchInCapture)
    {
        this(robot, x, y, width, height);
        this.colorToMatchInCapture = colorToMatchInCapture;
    }

    /**
     * Konstruktor um später {@code textToFind} auf dem Bildschirm im Siedlerfont
     * mit der Siedlerschriftfarbe bei den angegebenen Koordinaten zu finden.
     * Dieser Konstruktor benutzt die Klasse ReferenceImageHandling zum Erstellen
     * des Images.
     *
     * @param robot
     * @param x
     * @param y
     * @param width
     * @param height
     * @param textToFind
     * @param isTextLanguageSpecific
     */
    public ImageChecker(Robot robot, int x, int y, int width, int height,
            String textToFind, boolean isTextLanguageSpecific)
    {
        this(robot, x, y, width, height,
                ReferenceImageHandling.createReferenceImage(
                        textToFind, isTextLanguageSpecific));
        this.filterScreenCapture = true;
        setAdjustWidthOf2ndImageToFitInto1st(true);
        setTextInImage(textToFind);

    }



    /**
     * überprüft, ob das Bild aktuell auf dem Bildschirm im entsprechenden
     * Bereich zu finden ist.
     * Wurde das Objekt mit zwei Bildern initialisiert,
     * so liefert die Funktion true, sobald eines der beiden gefunden wird.
     * Wurde das Objekt mit einer Farbe initialisiert, so liefert die Funktion
     * true, sobald die Farbe auf dem Bildschirm im entsprechenden Bereich
     * zumindest einmalig gefunden wird.
     *
     * Wurde zuvor ein {@code #userSuppliedImageCapture} gesetzt, wird kein
     * aktueller screenshot erzeugt, sondern es wird {@code #userSuppliedImageCapture}
     * anstelle des Screenshots benutzt. Nach dem Vergleich wird
     * {@code #userSuppliedImageCapture} gelöscht.
     *
     * @return true falls imageToMatch (oder imageToMatchAlternative) bei den
     * spezifizierten Koordinaten gefunden wurde.
     */
    public boolean match()
    {
        if (screensize != null)
        {
            if (ProcessWindowSupportHandler.getInstance().isScreenSizeCheckActiveAndScreenSizeNotEquals(screensize))
            {
                //die aktuelle Auflösung entspricht nicht der geforderten, Abbruch
            	Test.output("wrong resolution");
                return false;
            }
        }

        if (userSuppliedImageCapture != null)
        {
            this.imageCapture = userSuppliedImageCapture;
        }
        else
        {
            if (ProcessWindowSupportHandler.getInstance().isProcessWindowSupportActiveAndNotForegroundWindow())
            {
                // The window we want to check is not visible
                // abort immediately
            	Test.output("window not in foreground");
                return false;
            }

            this.imageCapture = robot.createScreenCapture(
                    new Rectangle(x, y, width, height));
            if (filterScreenCapture)
            {
                filterS3Font(this.imageCapture);
            }
        }
        //Sonderbehandlung für colorInArea
        if (colorToMatchInCapture != null)
        {
        	Test.output("run matchColorInArea");
            return matchColorInArea();
        }

        matchStart = new Point();
        boolean result = (imageToMatch != null) && match(imageCapture, imageToMatch,
                adjustWidthOf2ndImageToFitInto1st, matchStart, filterScreenCapture);
        if (!result)
        {
            matchStart = null;
            matchCenter = null;
        }
        else
        {
            matchStart.x += this.x;
            matchStart.y += this.y;
            matchCenter = new Point();
            int width = imageToMatch.getWidth();
            if (adjustWidthOf2ndImageToFitInto1st)
            {
                width = getAdjustedWidth(imageCapture.getWidth(), width);
            }
            matchCenter.x = matchStart.x + (width / 2);
            matchCenter.y = matchStart.y + (imageToMatch.getHeight() / 2);
        }
        
        // FIXME remove after test
        if( SettlersLobby.isBetaUsed() ) {
        	// get Date
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss");
			Calendar now = Calendar.getInstance();
			
			// save picture and create screenshot-Directory if it doesn't exists
			File screenShotDir = new File(SettlersLobby.configDir, "debug");
			if (!screenShotDir.exists()) {
				screenShotDir.mkdirs();
			}
			File screenShotFile = new File(screenShotDir.getAbsolutePath(), formatter.format(now.getTime()) + "_full_" + this.i + ".png");
			try {
				ImageIO.write(imageToMatch, "png", screenShotFile);
			} catch (IOException e) {
				Test.outputException(e);
			}
        }
        
        // das vom user übergebene Image wird nach jedem match Aufruf gelöscht
        userSuppliedImageCapture = null;
        autoCleanup();
        return result;
    }
    
    public void setIndex( int i ) {
    	this.i = i;
    }

    private void autoCleanup()
    {
        if (isAutoCleanup())
        {
            cleanup();
        }
    }

    public Point getMatchStart()
    {
        return matchStart;
    }

    /**
     * überprüft ob colorToMatchInCapture im spezifizierten Screenshot-Bereich
     * vorkommt.
     * @return
     */
    private boolean matchColorInArea()
    {
        matchStart = null;

        int w = imageCapture.getWidth();
        int h = imageCapture.getHeight();

        //zuerst wird nur die mittlere Zeile durchsucht, da man hier
        //sehr wahrscheinlich fündig wird, sofern man Text sucht
        //(wofür die Funktion in erster Linie benutzt wird).
        //Somit muss nicht das komplette Array kopiert werden
        int colorToMatch = colorToMatchInCapture.getRGB();
        int[] rgbs = new int[w*1];
        imageCapture.getRGB(0, h/2, w, 1, rgbs, 0, w);
        boolean result = false;
        for (int i = 0; i < rgbs.length; ++i)
        {
            if (match(rgbs[i], colorToMatch))
            {
                matchStart = new Point(i % w, (i / w));
                matchStart.y += h/2;
                //in absolute Koordinaten umrechnen
                matchStart.x += this.x;
                matchStart.y += this.y;

                matchCenter = matchStart;
                result = true;
                break;
            }
        }

        if (!result)
        {
            //long time1 = System.currentTimeMillis();
            rgbs = new int[w*h];
            imageCapture.getRGB(0, 0, w, h, rgbs, 0, w);

            for (int i = 0; i < rgbs.length; ++i)
            {
                if (match(rgbs[i], colorToMatch))
                {
                    matchStart = new Point(i % w, i / w);
                    //in absolute Koordinaten umrechnen
                    matchStart.x += this.x;
                    matchStart.y += this.y;

                    matchCenter = matchStart;
                    result = true;
                    break;
                }
            }
            //long time2 = System.currentTimeMillis();
            //Test.output("Dauer: " + (time2 - time1));
        }
        // das vom user übergebene Image wird nach jedem match Aufruf gelöscht
        userSuppliedImageCapture = null;
        autoCleanup();
        rgbs = null;
        return result;
    }


    /**
     *
     * @param image1 larger image
     * @param image2 smaller image that is supposed to be found in image1
     * @param matchStart the match starting point relative to image1 (x|y) will be
     * stored here if a match is found.
     * @return true if image2 is found in image1.
     */
    public static boolean match(BufferedImage image1, BufferedImage image2, Point matchStart)
    {
        return match(image1, image2, false, matchStart, false);
    }
    private static boolean match(BufferedImage image1, BufferedImage image2,
            boolean adjustImage2Width, Point matchStart, boolean isTextMatch )
    {
        //long time1 = System.currentTimeMillis();
        if (image1 == null)
        {
            return false;
        }
        int w = image1.getWidth();
        int h = image1.getHeight();
        int w2 = image2.getWidth();
        int h2 = image2.getHeight();
        if (adjustImage2Width)
        {
            w2 = getAdjustedWidth(w, w2);
        }
        if (w < w2 || h < h2)
        {
            return false;
        }
        int[] rgbs = new int[w2*h2];
        int[] rgbs2 = new int[w2*h2];

        int xMaxOffset = w - w2;
        int yMaxOffset = h - h2;

        int colorImg1;
        int colorImg2;
        //image2 verändert sich nicht, daher können die RGB Werte einmalig
        //ausserhalb der folgenden Schleife ausgelesen werden
        image2.getRGB(0, 0, w2, h2, rgbs2, 0, w2);

        List<Point> sampleCoordinates = new ArrayList<Point>();
        int xTestOffset = w2/2;
        int yTestOffset = h2/2;

        //benutze ein paar Testkoordinaten, die stichprobenartig überprüft werden,
        //bevor das komplette image ausgelesen und dann komplett
        //verglichen wird.
        //Der Aufruf von image1.getRGB(x, y, w2, h2, rgbs, 0, w2);
        //benötigt viel Zeit, sollte daher möglichst vermieden werden,
        //wenn man nicht eine gute Chance auf einen Treffer hat.
        sampleCoordinates.add(new Point(0,0));
        sampleCoordinates.add(new Point(xTestOffset, yTestOffset));
        sampleCoordinates.add(new Point(0, yTestOffset));
        sampleCoordinates.add(new Point(xTestOffset, 0));

        if (isTextMatch)
        {
            //wenn es sich um einen Textvergleich handelt, dann
            //suche zusätzliche Koordinaten in der Schriftfarbe.
            //Die sind deutlich seltener als der Hintergrund,
            //wenn also diese Punkte alle stimmen, scheint ein kompletter
            //Vergleich lohnend
            int counter = 0;
            for (int i = 0; i < rgbs2.length; ++i)
            {
                colorImg2 = S3_FONT_COLOR.getRGB();
                if (match(rgbs2[i], colorImg2))
                {
                    sampleCoordinates.add(new Point(i % w2, i / w2));
                    counter++;
                    if (counter >= 14)
                    {
                        break;
                    }
                }
            }
        }

        //Berechne die RGB Werte der Testkoordinaten vom Vergleichsbild (image2)
        int[] sampleRGB = new int[sampleCoordinates.size()];
        for (int i = 0; i < sampleCoordinates.size(); i++)
        {
            Point p = sampleCoordinates.get(i);
            sampleRGB[i] = rgbs2[p.y * w2 + p.x];
        }
        int sampleCoordinatesSize = sampleCoordinates.size();

        boolean isMatch = false;
        for (int y = 0; y <= yMaxOffset; y++)
        {
            for (int x = 0; x <= xMaxOffset; x++)
            {
                boolean tmpMatch = true;
                //bevor das komplette Image verglichen wird,
                //wird erstmal überprüft, ob überhaupt die Testkoordinaten
                //übereinstimmen
                //Ansonsten kann man sich das ganze Kopieren und Vergleichen
                //komplett sparen.
                for (int i = 0; i < sampleCoordinatesSize; ++i)
                {
                    Point p = sampleCoordinates.get(i);
                    int tmpX = x + p.x;
                    int tmpY = y + p.y;
                    colorImg1 = image1.getRGB(tmpX, tmpY);
                    colorImg2 = sampleRGB[i];
                    if (!match(colorImg1, colorImg2))
                    {
                        //Farben stimmen nicht überein, Testkoordinatenüberprüfung
                        //abbrechen
                        tmpMatch = false;
                        break;
                    }
                }
                if (!tmpMatch)
                {
                    //Stichprobe ergab schon einen Mismatch, kompletter Vergleich
                    //nicht notwendig, weiter in die nächste Iteration
                    continue;
                }

                //kostspieliege Funktion, aber für den kompletten Vergleich notwendig
                image1.getRGB(x, y, w2, h2, rgbs, 0, w2);

                //überprüfe alle Pixel!
                for (int i = 0; i < rgbs.length; ++i)
                {
                    if (!match(rgbs[i], rgbs2[i]))
                    {
                        tmpMatch = false;
                        break;
                    }
                }
                if (tmpMatch)
                {
                    isMatch = true;
                    //Test.output("match at " + x + "/" + y); //$NON-NLS-1$ //$NON-NLS-2$
                    if (matchStart != null)
                    {
                        matchStart.x = x;
                        matchStart.y = y;
                    }
                    //Match gefunden, Abbruch aus innerer Schleife
                    break;
                }
            }
            //falls Match gefunden, dann auch Abbruch äußere Schleife
            if (isMatch)
                break;
        }
        //long time2 = System.currentTimeMillis();
        //Test.output("Dauer: " + (time2 - time1)); //$NON-NLS-1$
        //explicitly set the large array references to null
        rgbs = null;
        rgbs2 = null;
        sampleCoordinates.clear();
        return isMatch;
    }

    private static int getAdjustedWidth(int w1, int w2)
    {
        //Wenn w2 größer ist als w1, so kann w2 nicht in w1 liegen
        //w2 wird so angepaßt, daß es kleiner ist als w1 und zusätzlich
        //noch minimal verschoben werden kann, um überhaupt ein match zu
        //ermöglichen
        w2 = Math.min(w2, w1 - 5);
        w2 = Math.max(w2, 1); //positiv
        return w2;
    }

    /**
     * Filtert das {@code image} nach der S3 Fontfarbe. Alle Farben,
     * die nicht der Fontfarbe entsprechen, werden auf Schwarz gesetzt.
     * Damit erhält man ein image, das nur den Text in der S3 Fontfarbe
     * auf schwarzem Hintergrund enthält.
     *
     * @param image
     */
    public static void filterS3Font(BufferedImage image)
    {
        int fontColor = S3_FONT_COLOR.getRGB();
        int colorBlack = Color.BLACK.getRGB();
        if (image == null)
        {
            return;
        }
        int w = image.getWidth();
        int h = image.getHeight();
        int[] rgbs = new int[w*h];
        image.getRGB(0, 0, w, h, rgbs, 0, w);
        for (int i = 0; i < rgbs.length; ++i)
        {
            if (!match(rgbs[i],fontColor))
            {
                rgbs[i] = colorBlack;
            }
        }
        image.setRGB(0, 0, w, h, rgbs, 0, w);
    }

    public static boolean match(int rgb1, int rgb2)
    {
        if (PixelColorChecker.colorVariation > 0)
        {
            //wenn Farbabweichung erlaubt ist, so sind auch
            //Farbwerte in Ordnung, die bis zu "colorVariation"-viele
            //Einheiten vom gefordeten Wert abweichen
            //colorVariation sollte daher nur kleine Werte haben (0-10)
            return Math.abs(((rgb1 >> 16) & 0xff) - ((rgb2 >> 16) & 0xff))
                            <= PixelColorChecker.colorVariation &&
                   Math.abs(((rgb1 >> 8) & 0xff) - ((rgb2 >> 8) & 0xff))
                            <= PixelColorChecker.colorVariation &&
                   Math.abs((rgb1 & 0xff) - (rgb2 & 0xff))
                                    <= PixelColorChecker.colorVariation;
        }
        else
        {
            return rgb1 == rgb2;
        }
    }

    public Point getMatchCenter()
    {
        return matchCenter;
    }

    public void setAdjustWidthOf2ndImageToFitInto1st(
            boolean adjustWidthOf2ndImageToFitInto1st)
    {
        this.adjustWidthOf2ndImageToFitInto1st = adjustWidthOf2ndImageToFitInto1st;
    }

    /**
     * ruft {@code counter} mal imageChecker.match() auf, bis true geliefert wird.
     * Zwischen den Versuchen wird wird {@code delay} ms lang gewartet.
     *
     * @param imageChecker
     * @param counter Anzahl Versuche
     * @param delay Pause in ms zwischen den Versuchen
     * @return true falls das Image innerhalb der Anzahl Versuche erkannt wurde,
     *  sonst false
     * @throws InterruptedException
     */
    public static boolean waitUntilImageMatch(ImageChecker imageChecker, int counter, int delay) throws InterruptedException
    {
        boolean isMatch = false;
        for (int i = 0; i < counter; ++i)
        {
            isMatch = imageChecker.match();
            Test.output("match on " + i + ": " + isMatch + ", check again with delay " + delay);
            if (isMatch)
            {
                break;
            }
            Thread.sleep(delay);
        }
        Test.output("result: " + isMatch);
        return isMatch;
    }

    public void setFilterScreenCapture(boolean filterScreenCapture)
    {
        this.filterScreenCapture = filterScreenCapture;
    }

    public BufferedImage getImageCapture()
    {
        return imageCapture;
    }
    
    /**
     * Return the userSuppliedImageCapture.
     * 
     * @return BufferedImage
     */
    public BufferedImage getUserSuppliedImageCapture() {
    	return this.userSuppliedImageCapture;
    }
    
    /**
     * Set the userSuppliedImageCaputure.
     * 
     * @param userSuppliedImageCapture
     */
    public void setUserSuppliedImageCapture(BufferedImage userSuppliedImageCapture)
    {
        if (userSuppliedImageCapture == null)
        {
            return;
        }
        if (userSuppliedImageCapture.getWidth() != width ||
                userSuppliedImageCapture.getHeight() != height)
        {
            throw new IllegalArgumentException(I18n.getString("ImageChecker.ERROR_SIZE")); //$NON-NLS-1$
        }
        this.userSuppliedImageCapture = userSuppliedImageCapture;
    }

    public String getTextInImage()
    {
        return textInImage;
    }

    public void setTextInImage(String textInImage)
    {
        this.textInImage = textInImage;
    }

    public void setScreenSize(int width, int height)
    {
        this.screensize = new Dimension(width, height);
    }

    public void setScreenSizeS3Menu()
    {
        setScreenSize(800, 600);
    }
    
    public void cleanup()
    {
        this.imageCapture = null;
        this.userSuppliedImageCapture = null;
    }
    
    public boolean isAutoCleanup()
    {
        return autoCleanup;
    }

    public void setAutoCleanup(boolean autoCleanup)
    {
        this.autoCleanup = autoCleanup;
    }


}
