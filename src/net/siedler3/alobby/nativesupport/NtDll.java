package net.siedler3.alobby.nativesupport;

import com.sun.jna.Native;
import com.sun.jna.win32.StdCallLibrary;

public interface NtDll extends StdCallLibrary
{
    //use instance without type mapping
    //wine_get_version() returns a const char *, not a wchar*
    NtDll INSTANCE = (NtDll) Native.loadLibrary("ntdll", NtDll.class);

    String wine_get_version();
}
