package net.siedler3.alobby.nativesupport;

import java.awt.Dimension;
import java.io.File;

import net.siedler3.alobby.util.IProvidesProcessIdAndWindowHandle;

import com.sun.jna.Platform;
import com.sun.jna.platform.win32.WinDef.HWND;

public abstract class NativeFunctions {
	private static NativeFunctions functions = null;
	public static NativeFunctions getInstance() {
		if (functions == null) {
			if (Platform.isWindows()) {
				functions = new WinFunctions();
			}
		}
		return functions;
	}

	public void init()
	{
	}

	/*Hook methods**/
	public abstract boolean isPressed(int key);

	/*Highlighting methods**/
	/**
     * Lässt das Fenster blinken.
     * @param frameTitle Titel, mit dem das Fenster identifiziert wird
     * @param time Frequenz zwischen dem Blinken
     * @param maximalFlashingTime Beschränkt die Blink-Zeit
     */
	public abstract void flashWindow(String window, int count, int rate);

	/*IngameMessage methods**/
	/**
	 * Zeigt message an Position (X|Y) für time in Siedler 3 an.
	 * Die Funktion zeigt den String NUR, wenn Siedler 3 wirklich gestartet ist.
	 * Der Funktion gibt zurück, ob S3 zum Zeitpunkt des Beendes geöffnet ist, um
	 * zu unterdrücken, dass eine Nachricht öfters angezeigt wird, da sie nur die
	 * Hälfte der Zeit angezeigt wurde. Sie würde dadurch in eine unendliche
	 * Schleife geraten.
	 * @param message
	 * @param x
	 * @param y
	 * @param fontSize
	 * @param duration duration in ms
	 * @return
	 */
	public abstract boolean showMessage(String message, int x, int y, int fontSize, int duration);
	/**
	 * Stoppt das Anzeigen der aktuellen Nachricht und lässt showMessage()
	 * zurückkehren
	 */
	public abstract void stopShowing();

	/*WindowFont methods**/
	/**
	 * erzeugt ein natives Windows Fenster und zeigt dort
	 * {@code label} auf schwarzem Hintergrund an. Die Schriftfarbe
	 * entspricht dem RGB-Wert.
	 * Das Bild wird als Bitmap unter {@code fileName} abgespeichert.
	 * Anschliessend wird das Fenster sofort wieder geschlossen.
	 *
	 * @param label
	 * @param labelColorR
	 * @param labelcolorG
	 * @param labelcolorB
	 * @return immer 0, sobald die interne Window Message handling loop
	 * verlassen wird.
	 */
	public abstract void createReferenceBitmap(String text, int red, int green,
			int blue, String absolutePath);

	/*SendInputNative methods**/
	/**
     * simuliert die Eingabe von "text" mittels Tastatur.
     * Dazu wird "text" in die virtuellen Keycodes zerlegt und anschliessend
     * als Folge von nativen KeyEvents an die native Funktion SendInput() übergeben.
     * Bietet eine ähnliche Funktionalität wie eine Kombination aus
     * {@link java.awt.Robot#keyPress(int)} und {@link java.awt.Robot#keyRelease(int)},
     * allerdings sollte es jeweils an das standardmässig eingestellte
     * Tastaturlayout angepasst sein.
     *
     * @param text Text der eingegeben werden soll
     * @return true falls die Tastatureingaben erfolgreich abgesetzt wurden
     */
	public abstract boolean enterText(String text);

	/*NativeProcess methods**/
	public abstract boolean createProcessImpl(IProvidesProcessIdAndWindowHandle result, String cmdLine);
	public abstract boolean createProcessImpl(IProvidesProcessIdAndWindowHandle process, String cmdLine, File directory);

	public abstract void destroyImpl(int pId);
	public abstract int waitForImpl(int pId);
	public abstract boolean isForegroundWindow(HWND hwnd);
	public abstract Dimension getResolution(HWND hwnd);
	public abstract int getPPID(int pid);
	/**
	 * When running in Wine on Linux, returns the Wine version.
	 * Otherwise raises java.lang.UnsatisfiedLinkError.
	 * @return
	 */
	public abstract String getWineVersion();

	/**
	 * Derives the ProcessId and WindowHandle from the ProcessHandle
	 */
	public abstract boolean deriveProcessIdAndWindowHandleFromProcessHandle(IProvidesProcessIdAndWindowHandle process, long processHandle);
	public abstract boolean findS3WindowHandleUsingFindWindow(IProvidesProcessIdAndWindowHandle process, long processHandle);
	
	public abstract boolean isUserWindowsAdmin();
	
}
