package net.siedler3.alobby.nativesupport;

import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Arrays;
import java.util.List;

import net.siedler3.alobby.controlcenter.ReqQuery;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.nativesupport.GDI32Lib.BitmapFileHeader;
import net.siedler3.alobby.nativesupport.GDI32Lib.BitmapInfoHeader;
import net.siedler3.alobby.nativesupport.User32Lib.MONITORINFO;
import net.siedler3.alobby.s3gameinterface.IProcessWindowSupport;
import net.siedler3.alobby.s3gameinterface.ProcessWindowSupportHandler;
import net.siedler3.alobby.util.IProvidesProcessIdAndWindowHandle;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import com.sun.jna.platform.win32.BaseTSD.ULONG_PTR;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.Tlhelp32.PROCESSENTRY32;
import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinBase;
import com.sun.jna.platform.win32.WinBase.PROCESS_INFORMATION;
import com.sun.jna.platform.win32.WinBase.STARTUPINFO;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinDef.HBITMAP;
import com.sun.jna.platform.win32.WinDef.HDC;
import com.sun.jna.platform.win32.WinDef.HFONT;
import com.sun.jna.platform.win32.WinDef.HWND;
import com.sun.jna.platform.win32.WinDef.RECT;
import com.sun.jna.platform.win32.WinDef.WORD;
import com.sun.jna.platform.win32.WinGDI.BITMAPINFO;
import com.sun.jna.platform.win32.WinNT;
import com.sun.jna.platform.win32.WinNT.HANDLE;
import com.sun.jna.platform.win32.WinUser;
import com.sun.jna.platform.win32.WinUser.FLASHWINFO;
import com.sun.jna.platform.win32.WinUser.INPUT;
import com.sun.jna.platform.win32.WinUser.KEYBDINPUT;
import com.sun.jna.platform.win32.WinUser.SIZE;
import com.sun.jna.platform.win32.WinUser.WNDENUMPROC;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;

public class WinFunctions extends NativeFunctions {
	public static DWORD TH32CS_SNAPPROCESS = new DWORD(2);

    private static GDI32Lib gdi32 = GDI32Lib.INSTANCE;
    private static User32Lib user32 = User32Lib.INSTANCE;

    //should be part of WinUser, but is missing in the currently used version
    private static final int WS_CAPTION = 0x00C00000;
    private static final int SWP_NOSIZE = 0x0001;
    private static final int SWP_NOMOVE = 0x0002;
    private static final int SWP_NOZORDER = 0x0004;
    private static final int SWP_FRAMECHANGED = 0x0020;

    private boolean stopShowing = false;

    @Override
    public void init()
    {
        Test.output("init start");
        initInputStructure();
        Test.output("init end");
    }

    @Override
    public boolean isPressed(int key) {
        int result = user32.GetAsyncKeyState(key);
        if (result != 0)
        {
            Test.output("" + result + ", " + key);
        }
        return result != 0;
    }

    @Override
    public void flashWindow(String window, int count, int rate) {
        FLASHWINFO flashing = new FLASHWINFO();
        flashing.hWnd = user32.FindWindow(null, window);
        flashing.dwFlags = User32.FLASHW_TIMERNOFG | User32.FLASHW_ALL;
        flashing.uCount = count;
        flashing.dwTimeout = rate;
        flashing.cbSize = flashing.size();
        user32.FlashWindowEx(flashing);
    }

    @Override
    public boolean showMessage(String message, int x, int y, int fontSize, int duration) {

        IProcessWindowSupport processWindowSupport = ProcessWindowSupportHandler.getInstance().getProcessWindowSupport();
        if (processWindowSupport == null)
        {
            //TODO get windowHandle in a different way from Process to continue here
            //for the moment we simply abort
            return false;
        }
        HWND window = processWindowSupport.getProcessWindowHandle();

        HDC hdc = user32.GetDC(window);

        HFONT font = gdi32.CreateFont(fontSize, 0, 0, 0, 500, 0, 0, 0, 1, 0, 0, 0, 0, null);

        RECT messageArea = new RECT();
        messageArea.left   = x;
        messageArea.top    = y;
        messageArea.right  = x+800;
        messageArea.bottom = y+50;

        setStopShowing(false);
        long endTime = System.currentTimeMillis() + duration;
        if (user32.IsWindow(window)) 
        {
            gdi32.SelectObject(hdc, font);
            gdi32.SetBkMode(hdc, 1);
            gdi32.SetTextColor(hdc,0xFFFFFF);
            while (!isStopShowing() && user32.IsWindow(window))
            {
                if (isForegroundWindow(window))
                {
                    user32.DrawText(hdc, message, message.length(), messageArea, 0);
                }
                else
                {
                    try
                    {
                        Thread.sleep(50);
                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                        Thread.currentThread().interrupt(); //keep interrupted state
                        break;
                    }
                }

                if (System.currentTimeMillis() > endTime)
                {
                    break;
                }
            }
        }
        user32.ReleaseDC(window, hdc);
        gdi32.DeleteObject(font);
        

        return user32.IsWindow(window);
    }

    @Override
    public void stopShowing()
    {
        setStopShowing(true);
    }

    @Override
    public void createReferenceBitmap(String text, int red, int green,
                    int blue, String absolutePath) {
        //Creating font based on S3-Registry-Entries.
        HFONT font = gdi32.CreateFont(ReqQuery.s3FontSize, 0, 0, 0, 0, 0, 0, 0,
                        1, 0, 0, 0, 0, ReqQuery.s3Font);

        createAndWriteReferenceBitmap(absolutePath, text, font, convertColor(red,green,blue));

        gdi32.DeleteObject(font);
    }

    /**
     * Converts the color values to a _BGR Hex-Value to handle with native function SetTextColor.
     * @param red
     * @param green
     * @param blue
     * @return rgb hex value
     */
    private int convertColor(int red, int green, int blue) {
        return ((blue & 0xFF) << 16) | ((green & 0xFF) << 8) | (red & 0xFF);
    }

    private void createAndWriteReferenceBitmap(String filePath, String text, HFONT font, int rgb16) {
        HDC hDisplay = gdi32.CreateDC("DISPLAY", null, null, null);
        HDC hCompatible = gdi32.CreateCompatibleDC(hDisplay);
        SIZE sSize = new SIZE();

        //Berechnung des benötigten Platzes für den Text
        gdi32.SelectObject(hCompatible, font);
        gdi32.GetTextExtentPoint32(hCompatible, text, text.length(), sSize);

        int screenWidth = gdi32.GetDeviceCaps(hDisplay, GDI32Lib.HORZRES);
        int screenHeight = gdi32.GetDeviceCaps(hDisplay, GDI32Lib.VERTRES);
        //Ist der Text zu lang für die Bildschirmbreite, muss er abgeschnitten werden.
        int width = screenWidth > sSize.cx ? sSize.cx : screenWidth;
        int height = screenHeight > sSize.cy ? sSize.cy : screenHeight;

        BitmapInfoHeader bhInfo = new BitmapInfoHeader();
        bhInfo.biSize = bhInfo.size();
        bhInfo.biHeight = height;
        bhInfo.biWidth = width;
        bhInfo.biPlanes = 1;
        bhInfo.biBitCount = 24;
        bhInfo.biCompression = GDI32Lib.BI_RGB;
        bhInfo.biSizeImage = ((((bhInfo.biWidth * bhInfo.biBitCount) + 31) & ~31) >> 3) * bhInfo.biHeight;
        bhInfo.biXPelsPerMeter = 0;
        bhInfo.biYPelsPerMeter = 0;
        bhInfo.biClrImportant = 0;

        BITMAPINFO bInfo = new BITMAPINFO();
        bInfo.bmiHeader = bhInfo;

        PointerByReference dibSection = new PointerByReference();
        HBITMAP hBitmap = gdi32.CreateDIBSection(hDisplay, bInfo, GDI32Lib.DIB_RGB_COLORS, dibSection, null, 0);

        if (hBitmap == null) {
            //TODO Exception!
            return;
        }

        //Text zeichnen
        HANDLE replacedObj = gdi32.SelectObject(hCompatible, hBitmap);
        gdi32.SetTextColor(hCompatible, rgb16);
        gdi32.SetBkColor(hCompatible, 0x000000);
        gdi32.SelectObject(hCompatible, font);
        gdi32.TextOut(hCompatible, 0, 0, text, text.length());

        //Möglicher Fehler: bhFile.size() statt sizeof(BITMAPFILEHEADER)
        BitmapFileHeader bhFile = new BitmapFileHeader();
        bhFile.bfOffBits=bhFile.size()+bhInfo.size();
        bhFile.bfSize=(3*bhInfo.biHeight*bhInfo.biWidth)+bhFile.size()+bhInfo.size();
        bhFile.bfType=0x4d42;
        bhFile.bfReserved1 = 0;
        bhFile.bfReserved2 = 0;

        HANDLE file = Kernel32.INSTANCE.CreateFile(filePath, Kernel32.GENERIC_READ | Kernel32.GENERIC_WRITE, 0, null, Kernel32.CREATE_ALWAYS, Kernel32.FILE_ATTRIBUTE_NORMAL, null);
        if (file == Kernel32.INVALID_HANDLE_VALUE) {
            //TODO Exception!
            Test.output("create file failed");
            return;
        }

        int bytesToWrite = bhFile.size();
        IntByReference bytesWritten = new IntByReference();
        if (!Kernel32.INSTANCE.WriteFile(file, bhFile.getBytes(), bytesToWrite, bytesWritten, null)) {
            //TODO Exception!
            Test.output("bhFile write failed");
            return;
        }

        bytesToWrite = bhInfo.size();
        if (!Kernel32.INSTANCE.WriteFile(file, bhInfo.getBytes(), bytesToWrite, bytesWritten, null)) {
            //TODO Exception!
            Test.output("h write failed");
            return;
        }

        bytesToWrite = bhInfo.biSizeImage;
        if (!Kernel32.INSTANCE.WriteFile(file, dibSection.getValue().getByteArray(0, bytesToWrite), bytesToWrite,
                        bytesWritten, null)) {
            //TODO Exception!
            Test.output("dibSection write failed");
            return;
        }

        Kernel32.INSTANCE.CloseHandle(file);
        gdi32.DeleteObject(gdi32.SelectObject(hCompatible, replacedObj));
        gdi32.DeleteDC(hCompatible);
        gdi32.DeleteDC(hDisplay);
    }


    @Override
    public boolean enterText(String text) {
        if (kiStruct == null) {
            initInputStructure();
        }
        else
        {
            kiStruct.count.setValue(0);
        }
        boolean result = true;
        short scanCode = 0;

        result &= prepareSendInput();
        for (int i = 0 ; i < text.length() ; i++) {
            scanCode = user32.VkKeyScan(text.charAt(i));
            result &= sendChar(text.charAt(i), scanCode);
        }
        sendKeyInputEvents();
        return result;
    }

    public static class KeyInputStructure extends Structure {
        public DWORD count;
        public INPUT[] atInput;

        @Override
        protected List<String> getFieldOrder()
        {
            return Arrays.asList(new String[] { "count", "atInput" });
        }
    }

    private KeyInputStructure kiStruct;
    private int sizeOfInputStructureElement;

    private void initInputStructure() {
        INPUT input = new INPUT();
        input.input.setType(KEYBDINPUT.class);
        sizeOfInputStructureElement = input.size();

        kiStruct = new KeyInputStructure();
        kiStruct.count = new DWORD(0);
        //this call is very time consuming
        kiStruct.atInput = (INPUT[]) input.toArray(1024);
    }

    private boolean sendKeyInputEvents() {
        DWORD error = user32.SendInput(kiStruct.count, kiStruct.atInput, sizeOfInputStructureElement);
        if (error != kiStruct.count)
        {
            kiStruct.count.setValue(0);
            return false;
        }
        kiStruct.count.setValue(0);
        return true;
    }

    private boolean sendChar(char charAt, short scanCode) {
        boolean result = true;
        short controlByte = (short) (scanCode >> 8);
        if (controlByte == -1) {
            return false;
        }
        if ((controlByte & 0x4) != 0) {
            result &= pressKey(KeyEvent.VK_ALT);
        }
        if ((controlByte & 0x2) != 0) {
            result &= pressKey(KeyEvent.VK_CONTROL);
        }
        if ((controlByte & 0x1) != 0) {
            result &= pressKey(KeyEvent.VK_SHIFT);
        }
        result &= pressKey(scanCode & 0xff);
        result &= releaseKey(scanCode & 0xff);
        if ((controlByte & 0x1) != 0) {
            result &= releaseKey(KeyEvent.VK_SHIFT);
        }
        if ((controlByte & 0x2) != 0) {
            result &= releaseKey(KeyEvent.VK_CONTROL);
        }
        if ((controlByte & 0x4) != 0) {
            result &= releaseKey(KeyEvent.VK_ALT);
        }
        return result;
    }

    private boolean prepareSendInput() {
        boolean result = true;

        if ((user32.GetKeyState(KeyEvent.VK_CAPS_LOCK) & 0x01) != 0)
        {
            result &= pressKey(KeyEvent.VK_CAPS_LOCK);
            result &= releaseKey(KeyEvent.VK_CAPS_LOCK);
        }
        result &= releaseKey(KeyEvent.VK_SHIFT);
        result &= releaseKey(KeyEvent.VK_CONTROL);
        result &= releaseKey(KeyEvent.VK_ALT);
        return result;
    }

    private boolean releaseKey(int key)
    {
        return addKeyInputEvent(key, true);
    }

    private boolean pressKey(int key)
    {
        return addKeyInputEvent(key, false);
    }

    private boolean addKeyInputEvent(int key, boolean releaseKey)
    {
        int i = kiStruct.count.intValue();
        if (i >= (kiStruct.atInput.length))
        {
            return false;
        }
        INPUT iKey = kiStruct.atInput[i];
        iKey.type = new DWORD(INPUT.INPUT_KEYBOARD);
        iKey.input.ki.wVk = new WORD(key);
        if (releaseKey)
        {
            iKey.input.ki.dwFlags = new DWORD(User32Lib.KEYEVENTF_KEYUP);
        }
        else //pressKey
        {
            iKey.input.ki.dwFlags = new DWORD(0);
        }
        iKey.input.ki.time = new DWORD(0);
        iKey.input.ki.wScan = new WORD(0);
        iKey.input.ki.dwExtraInfo = new ULONG_PTR(0);
        iKey.input.setType(KEYBDINPUT.class);
        kiStruct.count.setValue(kiStruct.count.intValue() + 1);
        return true;
    }

    public static class ENUM_PROC_PARAM extends Structure {
    	public HWND hWnd;
    	public DWORD threadId;

    	public ENUM_PROC_PARAM() {
    		super();
    		setAlignType(Structure.ALIGN_NONE);
    	}

    	@Override
        protected List<String> getFieldOrder()
        {
            return Arrays.asList(new String[] { "hWnd", "threadId" });
        }
    }

    @Override
    public boolean deriveProcessIdAndWindowHandleFromProcessHandle(IProvidesProcessIdAndWindowHandle process, long processHandle)
    {
        HANDLE hProcess = new HANDLE();
        hProcess.setPointer(Pointer.createConstant(processHandle));
        final int pId = Kernel32.INSTANCE.GetProcessId(hProcess);
        Test.output("pId: " + pId);

        user32.WaitForInputIdle(hProcess, new DWORD(WinBase.INFINITE));
        boolean result = false;

        //retrieve the corresponding Window handle of the first window
        //that belongs to the process. Our application has only one window.
        HWND hWindow = deriveWindowHandleFromProcessId(pId);
        if (hWindow != null)
        {
            process.setProcessId(pId);
            process.setProcessWindowHandle(hWindow);
            result = true;
            adjustWindowStyle(hWindow);
        }
        return result;
    }

    private void adjustWindowStyle(HWND hWindow)
    {
        //this code tries to remove the WS_CAPTION style
        //(The window has a title bar (includes the WS_BORDER style))
        //from the window
        //The idea is taken from http://www.tombraiderforums.com/showthread.php?t=199915
        //where this was done with an autohotkey script instead:
        //
        //WinSet, Style, ^0xC00000 ; toggle title bar
        //WinMove, , , 0, 0, 1920, 1080
        //
        //the corresponding winApi functions are
        //SetWindowLong and SetWindowPos
        //(SetWindowLongPtr is not used, as we do not change a pointer value)
        //
        //All this should help to avoid the problem on windows 8, that a window border
        //is created around the fullscreen s3-window

        try
        {
            int initialValue = user32.GetWindowLong(hWindow, WinUser.GWL_STYLE);
            if (initialValue != 0)
            {
                Test.output(String.format("GetWindowLong: %08X" , initialValue));
                int value = initialValue;
                if ((value & WS_CAPTION) == 0)
                {
                    Test.output("no style adjustment");
                }
                value &= ~WS_CAPTION;
                Test.output(String.format("        style: %08X" , value));
                int result = user32.SetWindowLong(hWindow, WinUser.GWL_STYLE, value);
                Test.output(String.format("SetWindowLong: %08X" , result));
                if (result != 0)
                {
                    boolean result2 = user32.SetWindowPos(hWindow, null, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED);
                    Test.output("SetWindowPos: " + result2);
                }
            }
        }
        catch (Exception e)
        {
            Test.outputException(e);
        }

    }


    private HWND deriveWindowHandleFromProcessId(final int pId)
    {
        final ENUM_PROC_PARAM procParams = new ENUM_PROC_PARAM();

        for (int i = 0 ; i < 50 ; i++)
        {
            //Test.output("enum Windows " + i);
            //use a loop until we find the window..usually this should already
            //be the case in the first iteration
            user32.EnumWindows(new WNDENUMPROC() {
                @Override
                public boolean callback(HWND hWindow, Pointer lparam) {
                    IntByReference processId = new IntByReference();
                    DWORD tpId = new DWORD(user32.GetWindowThreadProcessId(hWindow, processId));
                    if (processId.getValue() == pId)
                    {
                        procParams.hWnd = hWindow;
                        procParams.threadId = tpId;
                        return false;
                    }
                    else
                    {
                        /*
                        //only needed when started using cmd /c
                        int ppid = getPPID(processId.getValue());
                        Test.output("ppid:" + ppid);
                        if (ppid != -1 && ppid == pId)
                        {
                            //if the command was started in a new shell, then
                            //the PPID is the one that belongs to the shell and
                            //is the one we are searching for
                            // shell -> S3.exe -> S3 window
                            procParams.hWnd = hWindow;
                            procParams.threadId = tpId;
                            return false;
                        }*/

                        procParams.hWnd = null;
                        procParams.threadId = null;
                        return true;
                    }
                }
            }, procParams.getPointer());

            if (procParams.hWnd != null)
            {
                return procParams.hWnd;
            }

            try
            {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                Test.outputException(e);
                break;
            }
        }

        //in case the process started, but the window does not show up, we leave it to
        //the user to kill the hanging process.
        return null;
    }

    @Override
    public boolean findS3WindowHandleUsingFindWindow(IProvidesProcessIdAndWindowHandle process, long processHandle)
    {
        HANDLE hProcess = new HANDLE();
        hProcess.setPointer(Pointer.createConstant(processHandle));
        final int pId = Kernel32.INSTANCE.GetProcessId(hProcess);
        Test.output("pId: " + pId);

        user32.WaitForInputIdle(hProcess, new DWORD(WinBase.INFINITE));
        boolean result = false;
        HWND hWindow = retrieveS3WindowHandleWithFindWindow(pId);
        if (hWindow != null)
        {
            process.setProcessId(pId);
            process.setProcessWindowHandle(hWindow);
            result = true;
            adjustWindowStyle(hWindow);
        }
        return result;
    }

    private HWND retrieveS3WindowHandleWithFindWindow(final int pId)
    {
        HWND hWindow = user32.FindWindow(null, "S3");
        if (hWindow != null)
        {
            IntByReference processId = new IntByReference();
            user32.GetWindowThreadProcessId(hWindow, processId);
            if (processId.getValue() == pId)
            {
                return hWindow;
            }
        }
        return null;
    }
    
    @Override
    public boolean createProcessImpl(IProvidesProcessIdAndWindowHandle process, String cmdLine) 
    {
    	return createProcessImpl(process, cmdLine, null);
    }
    
    @Override
    public boolean createProcessImpl(IProvidesProcessIdAndWindowHandle process, String cmdLine, File directory) 
    {
		boolean result = false;
		String lpCurrentDirectory = directory != null ? directory.getAbsolutePath() : null;
		STARTUPINFO si = new STARTUPINFO();
		PROCESS_INFORMATION.ByReference pi = new PROCESS_INFORMATION.ByReference();
		si.clear();
		//procParams.clear();
		pi.clear();
		si.cb = new DWORD(si.size());

		// Start the child process.
		if (!Kernel32.INSTANCE.CreateProcess(null, cmdLine, null, null, false, new DWORD(0), null, lpCurrentDirectory, si, pi)) {
			return false;
		}

		//retrieve the process ID
		final int pId = Kernel32.INSTANCE.GetProcessId(pi.hProcess);
		user32.WaitForInputIdle(pi.hProcess, new DWORD(WinBase.INFINITE));
		//retrieve the corresponding Window handle of the first window
	    //that belongs to the process. Our application has only one window.

		HWND hWindow = deriveWindowHandleFromProcessId(pId);
		if (hWindow != null)
        {
            process.setProcessId(pId);
            process.setProcessWindowHandle(hWindow);
            result = true;
        }
        //close the process handle and thread handle of the pi structure
		Kernel32.INSTANCE.CloseHandle(pi.hThread);
		Kernel32.INSTANCE.CloseHandle(pi.hProcess);
		return result;
	}

	@Override
	public void destroyImpl(final int pId) {
		HANDLE hProc = Kernel32.INSTANCE.OpenProcess(WinNT.SYNCHRONIZE|WinNT.PROCESS_TERMINATE, false, pId);
		if (hProc == null) {
			return;
		}

		user32.EnumWindows(new WNDENUMPROC() {
				@Override
				public boolean callback(HWND hWindow, Pointer arg1) {
					IntByReference processId = new IntByReference();
					user32.GetWindowThreadProcessId(hWindow, processId);
					if (processId.getValue() == pId) {
						user32.PostMessage(hWindow, 0x10, null, null);
					}
					return true;
				}
			}, new IntByReference(pId).getPointer());

		if (Kernel32.INSTANCE.WaitForSingleObject(hProc, 3000) != WinBase.WAIT_OBJECT_0) {
			Kernel32.INSTANCE.TerminateProcess(hProc, 0);
		}

		Kernel32.INSTANCE.CloseHandle(hProc);
	}

	@Override
	public int waitForImpl(int pId) {
		HANDLE hProc = Kernel32.INSTANCE.OpenProcess(WinNT.SYNCHRONIZE, false, pId);
		if (hProc == null) {
			return 1;
		}

		IntByReference exitCode = new IntByReference();
		Kernel32.INSTANCE.WaitForSingleObject(hProc, WinBase.INFINITE);
		Kernel32.INSTANCE.GetExitCodeProcess(hProc, exitCode);
		Kernel32.INSTANCE.CloseHandle(hProc);
		return exitCode.getValue();
	}

	@Override
	public boolean isForegroundWindow(HWND hwnd) {
		return hwnd.equals(user32.GetForegroundWindow());
	}

	@Override
    public Dimension getResolution(HWND hwnd)
	{
	    com.sun.jna.platform.win32.WinUser.HMONITOR monitor = user32.MonitorFromWindow(hwnd, User32Lib.MONITOR_DEFAULTTONEAREST);
	    MONITORINFO.ByReference pInfo = new MONITORINFO.ByReference();

	    pInfo.cbSize = pInfo.size();
	    user32.GetMonitorInfo(monitor, pInfo);
	    int monitor_width = pInfo.rcMonitor.right - pInfo.rcMonitor.left;
	    int monitor_height = pInfo.rcMonitor.bottom - pInfo.rcMonitor.top;
	    return new Dimension(monitor_width, monitor_height);
	}

    @Override
    public String getWineVersion()
    {
        String result = null;
        try
        {
            //on windows the function does not exist in the dll,
            //therefore an UnsatisfiedLinkError is thrown,
            //which is an error, not an exception,
            //therefore need to catch Throwable
            result = NtDll.INSTANCE.wine_get_version().toString();
        }
        catch (Throwable e)
        {
            result = null;
            //Test.output(e.toString());
        }
        return result;
    }

    private synchronized boolean isStopShowing()
    {
        return stopShowing;
    }

    private synchronized void setStopShowing(boolean stopShowing)
    {
        this.stopShowing = stopShowing;
    }


	/**
	 * @param child process id
	 * @return if no parent process is found -1, else the parent process id
	 */
	@Override
    public int getPPID(int pid) {
		HANDLE h = Kernel32.INSTANCE.CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, new DWORD(0));
		PROCESSENTRY32.ByReference pe = new PROCESSENTRY32.ByReference();
		pe.dwSize = new DWORD(pe.size());

		if (Kernel32.INSTANCE.Process32First(h, pe)) {
			do {
				if (pe.th32ProcessID.intValue() == pid) {
					return pe.th32ParentProcessID.intValue();
				}
			} while (Kernel32.INSTANCE.Process32Next(h, pe));
		}

		return -1;
	}

	@Override
	public boolean isUserWindowsAdmin() 
	{
		return Shell32Lib.INSTANCE.IsUserAnAdmin();
	}
}
