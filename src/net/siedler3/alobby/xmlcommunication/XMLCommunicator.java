/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.xmlcommunication;

import java.io.File;
import java.io.IOException;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import net.siedler3.alobby.controlcenter.Test;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * Mit dem XMLCommunication kann man durch ein XML-File navigieren.
 *
 * @author Stephan Bauer (aka maximilius)
 * @author jim
 * @version 1.0
 */
public class XMLCommunicator
{
	/**
	 * Der Knotenpunkt dieses XMLCommunicators.
	 *
	 * @since 1.0
	 */
	private Node node;
	private Document document;
	public boolean isDefaultValueWasUsed = true;

	XPathFactory factory = XPathFactory.newInstance();
	private NodeList emptyTextNodes = null;
	XPath xpath = factory.newXPath();

	/**
	 * Der Konstruktor ist protected, damit nur XMLFile auf ihn zugreifen kann
	 *
	 * @param node
	 *            Der Knotenpunkt.
	 * @since 1.0
	 */
	protected XMLCommunicator(Node node)
	{
	    if (node == null)
	    {
	        throw new NullPointerException();
	    }
		this.node = node;
		this.document = node.getOwnerDocument();
		
		// Use XPath to find empty text nodes in XML and remove them
		XPathExpression xpathExp;
		try {
			xpathExp = factory.newXPath().compile("//text()[normalize-space(.) = '']");
			this.emptyTextNodes = (NodeList)xpathExp.evaluate(this.document, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Remove each empty text node from document.
		for (int i = 0; i < this.emptyTextNodes.getLength(); i++) {
		    Node emptyTextNode = this.emptyTextNodes.item(i);
		    emptyTextNode.getParentNode().removeChild(emptyTextNode);
		}
	}

	/**
	 * Gibt den Wert des Knotenpunks mit dem angegebenen Pfad zurück.
	 *
	 * @param path
	 *            Der Pfad zum Knotenpunkt. z.B.
	 *            "nodeA/nodeB/nodeC[2]/nodeD[1]". Wenn in einer Ebene mehrere
	 *            Knotenpunkte mit dem selben Namen vorhanden sind, kann man
	 *            einen Index (beginnend ab 1) angeben .
	 *
	 * @return Den Wert des Knotenpunktes in dem angegebenen Pfad.
	 * @throws IlligealArgumentException
	 *             Wenn der Pfad ungültig ist.
	 * @since 1.0
	 */
	public String getByPath(String path) throws IllegalArgumentException
	{
	    String result = null;
	    try
	    {
	        Node node = getNodeByPath(path);
	        result = node.getNodeValue();
	    }
	    catch (Exception e)
	    {
	    }
		return result;
	}

    /**
     * Löscht den Knoten mit dem angegebenen Pfad, falls er vorhanden ist.
     *
     * @param path
     *            Der Pfad zum Knotenpunkt. z.B.
     *            "nodeA/nodeB/nodeC[2]/nodeD[1]". Wenn in einer Ebene mehrere
     *            Knotenpunkte mit dem selben Namen vorhanden sind, kann man
     *            einen Index (beginnend ab 1) angeben .
     *
     * @return true falls ein Knoten gefunden und gelöscht wurde, sonst false
     *
     */
	public boolean deleteByPath(String path)
	{
	    try
        {
            Node node = getNodeByPath(path);
            if (node != null)
            {
                Node parent = node.getParentNode();

                parent.removeChild(node);
                //falls der gelöschte Knoten ein Textknoten ist,
                //wird auch noch der umfassende Elementknoten mitgelöscht
                if (node.getNodeType() == Node.TEXT_NODE &&
                    parent.getNodeType() == Node.ELEMENT_NODE)
                {
                    Node grandParent = parent.getParentNode();
                    if (grandParent != null)
                    {
                        //Vorgänger sichern
                        //Node previous = parent.getPreviousSibling();
                        grandParent.removeChild(parent);
                        
                        // if grandparent does not have any children -> delete it
                        if( grandParent.getChildNodes().getLength() == 0 ) {
                        	grandParent.getParentNode().removeChild(grandParent);
                        }

                        //falls der Vorgängerknoten ein Textknoten war, der nur der Formatierung
                        //dient, so kann er gelöscht werden. Ansonsten entstehen Leerzeilen
                        //im Dokument
                        /*
                        if (previous != null && previous.getNodeType() == Node.TEXT_NODE &&
                                previous.getTextContent().matches("\\s+"))
                        {
                            grandParent.removeChild(previous);
                        }*/
                    }
                }
                return true;
            }
        }
        catch (Exception e)
        {
        }
        return false;
	}

	/**
	 * Gibt alle vorkommenden Knotenwerte von {@code path} zurück.
	 * @param path
	 * @return
	 * @throws IllegalArgumentException
	 */
    public String[] getAllByPath(String path) throws IllegalArgumentException
    {
        String[] result = null;
        try
        {
            XPathExpression expr = getXPathExpression(path);
            Object obj = expr.evaluate(document, XPathConstants.NODESET);
            NodeList nodeList = (NodeList) obj;
            result = new String[nodeList.getLength()];
            for (int i = 0; i < nodeList.getLength(); ++i)
            {
                result[i] = nodeList.item(i).getNodeValue();
            }
        }
        catch (Exception e)
        {
        }
        if (result == null)
        {
            throw new NullPointerException();
        }
        return result;
    }

    /**
	 * Gibt alle vorkommenden Knotennamen von {@code path} zurück.
	 * @param path
	 * @return
	 * @throws IllegalArgumentException
	 */
    public String[] getAllNodeNamesByPath(String path) throws IllegalArgumentException
    {
        String[] result = null;
        try
        {
            XPathExpression expr = xpath.compile(prepareXPathStringNodes(path));
            Object obj = expr.evaluate(document, XPathConstants.NODESET);
            NodeList nodeList = (NodeList) obj;
            result = new String[nodeList.getLength()];
            for (int i = 0; i < nodeList.getLength(); ++i)
            {
                result[i] = nodeList.item(i).getNodeName();
            }
        }
        catch (Exception e)
        {
        }
        if (result == null)
        {
            throw new NullPointerException();
        }
        return result;
    }

    /**
     * erzeugt aus {@code path} einen XPath konformen Ausdruck, siehe
     * {@link #getXPathExpression(String)}.
     *
     * @param path
     * @return
     * @throws XPathExpressionException
     */
    private XPathExpression getXPathExpression(String path) throws XPathExpressionException
    {
        XPathExpression expr = xpath.compile(prepareXPathString(path));
        return expr;
    }

    /**
     * versucht aus {@code path} einen XPath String zu erzeugen, der
     * vom Wurzelknoten aus beginnt. Die meisten (alten) Path-angaben sind nicht
     * XPath konform und beinhalten auch nicht das Wurzelelement, sondern
     * beginnen erst eine Ebene tiefer. Sollte {@code path} daher nicht mit
     * einem "/" beginnen, wird automatisch "/config/" vor {@code path} eingefügt,
     * zusätzlich wird am Ende "/text()" angehängt, um direkt das Textelement
     * zu referenzieren.
     *
     * @param path traditionelle Pfadangabe oder XPath-konforme Pfadangabe
     * @return XPath-konforme Pfadangabe
     */
    private String prepareXPathString(String path)
    {
        if (path.startsWith("/"))
        {
            return path;
        }
        String value = "/" + this.node.getNodeName() + "/";
        String value2 = path.isEmpty() ? "text()" : "/text()";
        return value + path + value2;
    }

    /**
     * versucht aus {@code path} einen XPath String zu erzeugen, der
     * vom Wurzelknoten aus beginnt. Die meisten (alten) Path-angaben sind nicht
     * XPath konform und beinhalten auch nicht das Wurzelelement, sondern
     * beginnen erst eine Ebene tiefer. Sollte {@code path} daher nicht mit
     * einem "/" beginnen, wird automatisch "/config/" vor {@code path} eingefügt
     *
     * @param path traditionelle Pfadangabe oder XPath-konforme Pfadangabe
     * @return XPath-konforme Pfadangabe
     */
    private String prepareXPathStringNodes(String path)
    {
        if (path.startsWith("/"))
        {
            return path;
        }
        String value = "/" + this.node.getNodeName() + "/";
        return value + path;
    }

	/**
	 * Gibt den Wert des Knotenpunks mit dem angegebenen Pfad zurück, so der
	 * Knotenpunkt existiert. Andernfalls wird der Standardwert zurückgegeben.
	 *
	 * @param path
	 * 			  Der Pfad zum Knotenpunkt. z.B.
	 *            "nodeA/nodeB/nodeC[2]/nodeD[1]". Wenn in einer Ebene mehrere
	 *            Knotenpunkte mit dem selben Namen vorhanden sind, kann man
	 *            einen Index (beginnend ab 1) angeben.
	 * @param communicatorForDefaultValue
	 * 			  Der zurückzugebende Standardwert im Misserfolgsfall, der mit
	 *            Hilfe dieses Kommunikators erfragt wird.
	 * @return
	 */
	public String getByPath(String path, XMLCommunicator communicatorForDefaultValue)
	{
		try
		{
		    String result = getByPath(path);
		    if (result != null)
		    {
		        return result;
		    }
		} catch (NullPointerException npe)
		{
		} catch (IllegalArgumentException iae)
		{
		}
		//bei Rückgabe null oder einer Exception wird der default Wert
		//benutzt. Er wird inzwischen nicht mehr in das Dokument eingetragen,
		//da dort nur die vom default-Wert abweichenden Werte stehen sollen.
		//Der default-Wert wird aus dem xml-Dokument im jar-file ausgelesen
        isDefaultValueWasUsed = true;
        String defaultValue = "";
        try
        {
            defaultValue = communicatorForDefaultValue.getByPath(path);
        }
        catch (Exception e)
        {
            //sollte nie passieren
            Test.outputException(e);
        }
        return defaultValue;
	}

	/**
	 * Setzt den Wert des Knotenpunkts mit dem angegebenen Pfad auf den
	 * Wert {@code value}.
	 * Existiert der Knoten nicht, wird er erzeugt.
	 *
	 * @param path
	 *            Der Pfad zum Knotenpunkt. z.B.
	 *            "nodeA/nodeB/nodeC[2]/nodeD[1]". Wenn in einer Ebene mehrere
	 *            Knotenpunkte mit dem selben Namen vorhanden sind, kann man
	 *            einen Index angeben.
	 * @param value
	 *            Der neue Wert für den Knotenpunkt.
	 * @throws IllegalArgumentException
	 *             Wenn der Pfad ungültig ist.
	 */
	public void setByPath(String path, String value)
			throws IllegalArgumentException
	{
		createNodeByPath(path).setNodeValue(value);
	}

	/**
	 * Gibt den Knotenpunkt mit dem angegebenen Pfad zurück.
	 *
	 * @param path
	 *            Der Pfad zum Knotenpunkt. z.B.
	 *            "nodeA/nodeB/nodeC[2]/nodeD[1]". Wenn in einer Ebene mehrere
	 *            Knotenpunkte mit dem selben Namen vorhanden sind, kann man
	 *            einen Index angeben.
	 * @return Der Knotenpunkt.
	 * @throws IlligealArgumentException
	 *             Wenn der Pfad ungültig ist.
	 * @since 1.0
	 */
	private Node getNodeByPath(String path) throws IllegalArgumentException
	{
	    Node node = null;
        try
        {
            XPathExpression expr = getXPathExpression(path);
            Object obj = expr.evaluate(document, XPathConstants.NODE);
            node = (Node) obj;
        }
        catch (Exception e)
        {
            throw new IllegalArgumentException(path);
        }
        return node;
	}


	/**
	 * Erzeugt den Knoten mit dem angegebenen Pfad, falls er noch nicht
	 * vorhanden ist. Der Pfad muß beim
	 * Wurzelknoten beginnen und darf nur einen simplen Aufbau wie
	 * "/nodeA/nodeB/nodeC[2]/nodeD[1]" haben. Andere speziellere XPath Konstrukte
	 * führen zu undefiniertem Verhalten.
	 * Falls Zwischenknoten nicht existieren, werden sie ebenfalls erzeugt.
	 *
	 * @param path
	 * @return Der neue erzeugte Knoten
	 * @throws IllegalArgumentException
	 */
    public Node createNodeByPath(String path) throws IllegalArgumentException
    {
        //Die Methode funktioniert nur, wenn der Wurzelknoten existiert,
        //was aber bereits durch den Konstruktor gewährleistet sein sollte.
        Node result = null;
        try
        {
            path = prepareXPathString(path);
            if (!path.startsWith("/") || path.contains("//") || path.contains("@"))
            {
                //nur Pfade von der Wurzel und einfache Elemente, keine Attribute
                throw new IllegalArgumentException(path);
            }

            String tmpPath;
            while ((result = getNodeByPath(path)) == null)
            {
                //Solange der gewünschte Knoten nicht existiert, wird
                //der erste nicht vorhandene Knoten auf dem
                //Pfad gesucht und erzeugt.
                //Bei jedem Durchlauf wird ein Knoten angehängt,
                //dadurch wird im letzten
                //Durchlauf automatisch der gewünschte Knoten erzeugt.
                //Enthält der Pfad einen [Index], so werden zur Not
                //so lange leere Knoten erzeugt, bis der Index erreicht wird.
                //Falls der Knoten initial schon existiert, so ist
                //result beim ersten Überprüfen schon != null, wodurch
                //die Schleife nie ausgeführt wird, das Ergebnis aber
                //korrekt gesetzt ist.
                tmpPath = path;
                Node tmpNode;
                String path1;
                String path2;
                do
                {
                    //Der XPath wird solange verkürzt, bis er
                    //zum ersten Mal einen Knoten zurückliefert
                    //Dies ist die Stelle, an der ein
                    //Knoten als Child eingefügt wird.
                    int index = tmpPath.lastIndexOf('/');
                    if (index == -1)
                    {
                        //etwas ist schiefgelaufen,
                        //nicht einmal der Wurzelknoten
                        //wurde gefunden
                        return null;
                    }
                    path1 = tmpPath.substring(0, index);
                    path2 = tmpPath.substring(index + 1);
                    if (path1.isEmpty())
                    {
                        //Nicht einmal der Wurzelknoten wurde gefunden, Abbruch.
                        return null;
                    }
                    tmpNode = getNodeByPath(path1);
                    tmpPath = path1; //Pfad verkürzen für nächste Iteration
                } while(tmpNode == null);

                Node newChild;
                if (path2.equals("text()"))
                {
                    newChild = tmpNode.appendChild(document.createTextNode(""));
                }
                else
                {
                    //der XmlElementname steht in path2, vorher noch
                    //eventuelle [] ausfiltern
                    int index = path2.indexOf('[');
                    if (index >= 0)
                    {
                        path2 = path2.substring(0, index);
                    }
                    newChild = tmpNode.appendChild(document.createElement(path2));
                }
                // falls der vorige Knoten ein Textfeld zum Formatieren ist,
                // wird dies gelöscht. Es wird später beim Schreiben wieder in
                // der korrekten Einrückung hinzugefügt
                Node previous = newChild.getPreviousSibling();
                if (previous != null && previous.getNodeType() == Node.TEXT_NODE &&
                        previous.getTextContent().matches("\\s+"))
                {
                    tmpNode.removeChild(previous);
                }
            } //Ende vom while loop, in der letzten Iteration wird der gewünschte
              // Knoten erzeugt
        }
        catch (Exception e)
        {
            Test.outputException(e);
        }
        return result;
    }


    /**
     * Schreibt das komplette Dokument mitsamt seinen Unterknoten in die Datei.
     *
     * @param file
     * @throws Exception
     *
     */
	private void writeFile(File file) throws Exception
	{
	    try
	    {
	        DOMSource source = new DOMSource(document);

    	    Transformer transformer = TransformerFactory.newInstance().newTransformer();
    	    //diese Property funktioniert nur mit dem momentanen Transformer,
    	    //anders kann man aber scheinbar keine Einrückung spezifizieren
    	    transformer.setOutputProperty
    	       ("{http://xml.apache.org/xslt}indent-amount", "4");
    	    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
    	    transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
    	    transformer.setOutputProperty(OutputKeys.METHOD, "xml");
    	    transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

    	    StreamResult result = new StreamResult(file);

    	    transformer.transform(source, result);
	    }
	    catch (TransformerException e)
	    {
	        Test.outputException(e);
	        if (e.getException() != null && e.getException() instanceof Exception)
	        {
	            throw (Exception) e.getException();
	        }
	        else
	        {
	            throw e;
	        }
        }
	}

	/**
	 * Speichert den Inhalt des XMLCommunicators in einer XML Datei mit dem
	 * übergebenen Dateinamen.
	 *
	 * @param fileName
	 *            Der Dateiname der XML Datei.
	 * @throws IOException
	 */
	public void saveInXMLFile(String fileName) throws Exception
	{
		writeFile(new File(fileName));
	}
}
