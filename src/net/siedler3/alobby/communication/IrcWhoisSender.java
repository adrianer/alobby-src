/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.communication;

import java.util.List;

import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.util.TimerTaskCompatibility;


public class IrcWhoisSender extends TimerTaskCompatibility
{

    private static int MAX_PARAMETER_LENGTH = 300;
    private List<IrcUser> list;
    private int index;
    private IRCCommunicator ircCommunicator;


    public IrcWhoisSender(List<IrcUser> list, IRCCommunicator ircCommunicator)
    {
        super();
        this.list = list;
        this.index = 0;
        this.ircCommunicator = ircCommunicator;
    }


    @Override
    public void run()
    {
        StringBuilder sb = new StringBuilder(MAX_PARAMETER_LENGTH + 30);
        //count the number of users handled in one 'whois' command
        //it is limited by getMaxTargetsForCommand()
        int targetCounter = 0;
        while (index < list.size() &&
               targetCounter < ircCommunicator.getIrcServerInfo().getMaxTargetsForCommand() &&
               sb.length() < MAX_PARAMETER_LENGTH)
        {
            sb.append(list.get(index).getNick());
            sb.append(",");
            index++;
            targetCounter++;
        }
        if (sb.length() > 0)
        {
            //remove last ','
            sb.deleteCharAt(sb.length() - 1);
        }
        ircCommunicator.sendWhoisWithoutLog(sb.toString());
        if (index >= list.size())
        {
            //die Liste ist komplett abgearbeitet,
            //der Task beendet sich selber
            Test.output("Whois Task finished: " + System.currentTimeMillis());
            cancel();
        }
    }
}
