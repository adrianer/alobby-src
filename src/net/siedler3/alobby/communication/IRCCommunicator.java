/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.communication;

import static net.siedler3.alobby.communication.NickServCommand.DROP;
import static net.siedler3.alobby.communication.NickServCommand.REGISTER;
import static net.siedler3.alobby.communication.NickServCommand.STATUS;

import java.io.File;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.Configuration;
import net.siedler3.alobby.controlcenter.ReqQuery;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.modules.connect.IrcServerCapabilities;
import net.siedler3.alobby.util.ArraySupport;
import net.siedler3.alobby.util.Base64Coder;
import net.siedler3.alobby.util.CryptoSupport;
import net.siedler3.alobby.util.S3Support;
import net.siedler3.alobby.util.TimerTaskCompatibility;
import net.siedler3.alobby.util.s4support.S4Support;

import org.jibble.pircbot.Colors;
import org.jibble.pircbot.ReplyConstants;
import org.jibble.pircbot.User;

/**
 * TODO
 * Diese Klasse verstoesst klar gegen das SRP und ist damit nicht wartbar.
 * Diese Klasse soll langfristig durch IrcEventListener ersetzt werden,
 * die sich direkt bei IrcEventDispatcher registrieren.
 * Solange dies noch nicht vollstaendig passiert ist, existiert diese Subklasse
 * von IrcEventDispatcher als Zwischenloesung, damit die alte Funktionalität
 * weiter sicher gestellt ist.
 * @author maximilius
 */
public class IRCCommunicator extends IrcEventDispatcher
{

	private final static String PROGRAM_MESSAGE_TOKEN = "s3Lobby;"; //$NON-NLS-1$
	private final static String CRYPTO_TOKEN = "%"; //$NON-NLS-1$
	public final static String CRYPTO_MULTIPART_MESSAGE_TOKEN = "§"; //$NON-NLS-1$
	private final static String CRYPTO_XMLRPC_TOKEN = "$"; //$NON-NLS-1$

	private final static String PROGRAM_NAME_STANDARD =
	    "<<" + ALobbyConstants.PROGRAM_NAME + " " + ALobbyConstants.VERSION + ">>"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	private final static String PROGRAM_NAME_AWAY =
	    ">>" + ALobbyConstants.PROGRAM_NAME + " " + ALobbyConstants.VERSION + "<<"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

	private final static String AWAY_INDICATOR = ">>" + ALobbyConstants.PROGRAM_NAME + " "; //$NON-NLS-1$ //$NON-NLS-2$
	private final static String HERE_INDICATOR = "<<" + ALobbyConstants.PROGRAM_NAME + " "; //$NON-NLS-1$ //$NON-NLS-2$

	private final SettlersLobby settlersLobby;
	private String channel;

    // speichert, ob der Server die Erfolgs/Misserfolgs-Meldung beim Login schon
	// gebracht hat
	private final boolean statusOfLoginIsKnown = false;

	// speichert, ob der Login erfolgreich war
	private final boolean userIsLoggedIn = false;

	// speichert den Grund für den fehlgeschlagene Login
	private final String reasonForFailedLogin = ""; //$NON-NLS-1$

	// speichert, ob der Server die Erfolgs/Misserfolgs-Meldung bei der
	// Registration schon gebracht hat
	private boolean statusOfRegistrationIsKnown = false;

	// speichert, ob die Registration erfolgreich war
	private boolean userIsRegistrated = false;

	// speichert den Grund für die fehlgeschlagene Registration
	private String reasonForFailedRegistration = ""; //$NON-NLS-1$

	private final IrcUserList ircUserList;

    private String defaultIrcName()
    {       
	    String versionString = getSettlerVersionString();
		Test.output(PROGRAM_NAME_STANDARD + " " + versionString + this.getVersion());
		return PROGRAM_NAME_STANDARD + " " + versionString + this.getVersion(); //$NON-NLS-1$
    }

    private String awayIrcName()
    {
    	String versionString = getSettlerVersionString();
    	return PROGRAM_NAME_AWAY + " " + versionString + this.getVersion(); //$NON-NLS-1$
    }

    //wird gesetzt, damit die Whois Ausgaben vom Automatismus
    //nicht das logfile vollschreiben
    private boolean hideWhois = false;
    //speichert das letzte automatische whois Kommando,
    //wird zur korrekten Wiedererkennung benötigt
    private String hideCommandMsg;
    private final Configuration config = Configuration.getInstance();
    private TimerTaskCompatibility ircScanAwayTask;
    private TimerTaskCompatibility ircSendWhoisTask;

    private boolean prepareShutdown = false;
    private boolean isDisposed = false;

    private IrcServerCapabilities ircServerCapabilities;

    /**
	 * Selektor für statusOfLoginIsKnown
	 */
	public boolean statusOfLoginIsKnown()
	{
		return statusOfLoginIsKnown;
	}

	/**
	 * Selektor für userIsLoggedIn
	 */
	public boolean userIsLoggedIn()
	{
		return userIsLoggedIn;
	}

	/**
	 * Selektor für statusOfRegistrationIsKnown
	 */
	public boolean statusOfRegistrationIsKnown()
	{
		return statusOfRegistrationIsKnown;
	}

	/**
	 * Selektor für userIsRegistrated
	 */
	public boolean userIsRegistrated()
	{
		return userIsRegistrated;
	}

	/**
	 * Selektor für reasonForFailedRegistration
	 */
	public String getReasonForFailedRegistration()
	{
		return reasonForFailedRegistration;
	}

	/**
	 * Selektor für reasonForFailedLogin
	 */
	public String getReasonForFailedLogin()
	{
		return reasonForFailedLogin;
	}

	public IRCCommunicator(SettlersLobby settlersLobby)
	{
		super();
		this.settlersLobby = settlersLobby;
		ircUserList = new IrcUserList(this.config);
		_setVersion(defaultIrcName());
		prepareShutdown = false;
		this.ircServerCapabilities = new IrcServerCapabilities(this);
		ircServerCapabilities.startObserveServerCapabilites();
	}

	/**
	 * Registriert Nick mit Passwort und eMail auf dem Server, sodass dieser von
	 * anderen nicht mehr benutzt werden kann.
	 *
	 * @param userPassword
	 * @
	 */
	@Override
    public void registerNickToServer(String userPassword, String eMail)
	{
	    sendNickServCommand(REGISTER ,  userPassword + " " + eMail); //$NON-NLS-1$
	}

	public void deleteNickFromServer(String userPassword)
	{
	    sendNickServCommand(DROP, userPassword);
	}

	@Override
    public void queryNickStatus()
	{
	    sendNickServCommand(STATUS, getNick());
	}

    @Override
    public void sendNickServCommand(NickServCommand command, String options)
    {
        if (options != null)
        {
            rawSendNickServCommand(command.getCommand() + " " + options);
        }
        else
        {
            rawSendNickServCommand(command.getCommand());
        }
    }

    private void rawSendNickServCommand(String commandLine)
    {
        if (commandLine.toUpperCase().startsWith("SET PASSWORD"))
        {
            //need to send it directly, otherwise setVerbose(false) does not
            //skip this command if it would be inside the queue
            sendRawLine("NICKSERV " + commandLine);
        }
        else
        {
            sendRawLineViaQueue("NICKSERV " + commandLine);
        }
    }

    private void rawSendChanServCommand(String commandLine)
    {
        sendRawLineViaQueue("CHANSERV " + commandLine);
    }



	/**
	 * Sendet eine Nachricht bzw. einen Befehl an den Server.
	 * @param message die Nachricht, die gesendet werden soll, kann auch ein Befehl sein.
	 * @param isPrivateMessage true falls die Nachricht eine private Nachricht ist
	 * @param receiverOfPrivateMessage der Empfänger der privaten Nachricht
	 * falls isPrivateMessage==true, sonst leer
	 * @return true falls die Nachricht eine öffentliche Nachricht an den channel ist
	 * @
	 */
	public boolean sendMessageToServer(String message, boolean isPrivateMessage,
			String receiverOfPrivateMessage)
	{
		if (isPrivateMessage)
		{
		    //wenn man eine msg schickt, bekommt man vom Server
		    //eine Nachricht, falls der user away ist
		    //daher wird vorher der bishere Status gelöscht
		    ircUserList.setAwayUser(receiverOfPrivateMessage, ""); //$NON-NLS-1$
		    sendMessage(receiverOfPrivateMessage, message); // sende private
			// Message
			return false;
		}
		else if (!isCommand(message))
		{
			sendMessage(channel, message); // sende öffentliche Message
			return true;
		}
		else
		{
		    return false;
		}
	}

	/**
	 * schickt die Nachricht fett formatiert an den Channel und zeigt sie auch
	 * im Chat an.
	 * @param message
	 * @
	 */
    private void sendAndShowBoldMessage(String message)
    {
        String ircMessage = Colors.BOLD + message;
        sendMessage(channel, ircMessage);
        settlersLobby.ircChatMessage(getNick(), message, true);
    }


	/**
	 * Prüft die übergebene Nachricht auf Chat Commandos und setzt sie um.
	 *
	 * @param 	message		Die Nachricht.
	 * @return 	True, wenn es sich um ein Chat Command gehandelt hat, andernfalls
	 * 			false
	 * @
	 */
	private boolean isCommand(String message)
	{
		// Suchmuster abarbeiten, trifft keines zu, false, sonst true returnen
		if (message.matches("/[?]( (.)*)?") || message.matches("/help( (.)*)?")) //$NON-NLS-1$ //$NON-NLS-2$
		{
			outputChannelCommandHelp();
			return true;
		} else
		{
			Matcher mKickReason = Pattern.compile("/kick (([a-zA-Z0-9]|_)+):((.)+)").matcher(message); //$NON-NLS-1$
			Matcher mKick = Pattern.compile("/kick (([a-zA-Z0-9]|_)+)").matcher(message); //$NON-NLS-1$
			Matcher mBan = Pattern.compile("/ban (([a-zA-Z0-9]|_)+)").matcher(message);		 //$NON-NLS-1$
			Matcher mUnBan = Pattern.compile("/unban (([a-zA-Z0-9]|_)+)").matcher(message);			 //$NON-NLS-1$
			Matcher mIgnore = Pattern.compile("/ignore (([a-zA-Z0-9]|_)+)").matcher(message); //$NON-NLS-1$
			Matcher mShowJoin = Pattern.compile("/showjoin").matcher(message); //$NON-NLS-1$
			Matcher mIp = Pattern.compile("/ip").matcher(message); //$NON-NLS-1$
			Matcher mClear = Pattern.compile("/clear").matcher(message); //$NON-NLS-1$
			Matcher mAway = Pattern.compile("/away(| ((.)+))").matcher(message); //$NON-NLS-1$
			Matcher mBack = Pattern.compile("/back").matcher(message); //$NON-NLS-1$
			Matcher mAwayCheck = Pattern.compile("/awaycheck").matcher(message); //$NON-NLS-1$
			Matcher mWhois = Pattern.compile("/whois (([a-zA-Z0-9]|_)+)").matcher(message); //$NON-NLS-1$
			Matcher mTopic = Pattern.compile("/topic (.+)").matcher(message); //$NON-NLS-1$
			Matcher mTime = Pattern.compile("/time").matcher(message); //$NON-NLS-1$
			Matcher mChatlog = Pattern.compile("/chatlog").matcher(message); //$NON-NLS-1$
			Matcher mMode = Pattern.compile("/mode (\\S+) ?(.*)").matcher(message); //$NON-NLS-1$
			Matcher mBold = Pattern.compile("/bold (.+)").matcher(message); //$NON-NLS-1$
			Matcher mMotd = Pattern.compile("/motd", Pattern.CASE_INSENSITIVE).matcher(message); //$NON-NLS-1$
			Matcher mDrop = Pattern.compile("/drop ?(\\S*)").matcher(message); //$NON-NLS-1$
			Matcher mNickServ = Pattern.compile("/ns (.+)").matcher(message); //$NON-NLS-1$
			Matcher mChanServ = Pattern.compile("/cs (.+)").matcher(message); //$NON-NLS-1$

			if (mKickReason.matches())
			{
				doKick(mKickReason.group(1), mKickReason.group(3));
				return true;
			} else if (mKick.matches())
			{
				doKick(mKick.group(1), "chat command"); //$NON-NLS-1$
				return true;
			} else if (mBan.matches())
			{
				doBan(mBan.group(1));
				return true;
			} else if (mUnBan.matches())
			{
				doUnBan(mUnBan.group(1));
				return true;
			} else if (mIgnore.matches())
			{
				doIgnore(mIgnore.group(1));
				return true;
			}  else if (mShowJoin.matches())
			{
				doShowJoin();
				return true;
			}  else if (mIp.matches())
            {
                doShowIp();
                return true;
            }  else if (mClear.matches())
            {
                settlersLobby.clearChat();
                return true;
            }  else if (mAway.matches())
            {
            	settlersLobby.doAway(mAway.group(2));
                return true;
            }  else if (mBack.matches())
            {
            	settlersLobby.doBack();
                return true;
            }  else if (mAwayCheck.matches())
            {
                settlersLobby.doListAwayMessages();
                return true;
            } else if (mWhois.matches())
            {
                sendWhois(mWhois.group(1));
                return true;
            } else if (mTopic.matches())
            {
                sendTopic(mTopic.group(1));
                return true;
			} else if (mTime.matches())
			{
				setTimeDisplay();
				return true;
			} else if (mChatlog.matches())
			{
				setChatLogging();
				return true;
            } else if (mMode.matches())
            {
            	setMode(mMode.group(1), mMode.group(2));
                return true;
            } else if (mBold.matches())
            {
                if (iAmOp())
                {
                    sendAndShowBoldMessage(mBold.group(1));
                }
                return true;
			} else if (mMotd.matches())
            {
                sendMotd();
                return true;

            }
			else if (mDrop.matches())
            {
			    deleteNickFromServer(mDrop.group(1));
                return true;
            }
			else if (mNickServ.matches())
			{
			    rawSendNickServCommand(mNickServ.group(1));
			    return true;
			}
            else if (mChanServ.matches())
            {
                rawSendChanServCommand(mChanServ.group(1));
                return true;
            }
			else if (message.matches("/.*"))	 //$NON-NLS-1$
			{
				settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.ERROR_CMD")); //$NON-NLS-1$
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	/**
	 * Schaltet an bzw. aus, dass im Chat angezeigt wird, wenn jemand die Lobby
	 * verlässt, oder betritt.
	 */
	private void doShowJoin()
	{
		settlersLobby.switchShowUserJoinsAndLefts();
		if (settlersLobby.isShowUserJoinsAndLefts())
		{
			settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.MSG_SHOW_JOIN")); //$NON-NLS-1$
		} else
		{
			settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.MSG_DO_NOT_SHOW_JOIN")); //$NON-NLS-1$
		}
	}

	/**
	 * zeigt die IP in der Lobby an
	 */
    private void doShowIp()
    {
        settlersLobby.onCommandAnswer(MessageFormat.format(
                I18n.getString("IRCCommunicator.MSG_CURRENT_IP"), settlersLobby.getIp())); //$NON-NLS-1$
    }


	/**
	 * Gibt im Chat eine Hilfe aus, welche Chat Commands existieren.
	 * @
	 */
	private void outputChannelCommandHelp()
	{
		settlersLobby.onCommandAnswer(MessageFormat.format(
		        I18n.getString("IRCCommunicator.HELP1") , ALobbyConstants.VERSION)); //$NON-NLS-1$
		settlersLobby.onCommandAnswer(MessageFormat.format(
                I18n.getString("IRCCommunicator.HELP2"), settlersLobby.getHelpPath())); //$NON-NLS-1$
		settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.HELP3")); //$NON-NLS-1$
		settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.HELP4")); //$NON-NLS-1$
		settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.HELP5")); //$NON-NLS-1$
		settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.HELP_W")); //$NON-NLS-1$
		settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.HELP_WHISPER")); //$NON-NLS-1$
		settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.HELP_IGNORE")); //$NON-NLS-1$
		settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.HELP_SHOWJOIN")); //$NON-NLS-1$
		settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.HELP_IP")); //$NON-NLS-1$
		settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.HELP_AWAY_TEXT")); //$NON-NLS-1$
		settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.HELP_AWAY")); //$NON-NLS-1$
		settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.HELP_BACK")); //$NON-NLS-1$
		settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.HELP_AWAYCHECK")); //$NON-NLS-1$
        settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.HELP_TIME")); //$NON-NLS-1$
        settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.HELP_CHATLOG")); //$NON-NLS-1$
        settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.HELP_ME")); //$NON-NLS-1$
        settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.HELP_CLEAR")); //$NON-NLS-1$

        //chanop Kommandos nur anzeigen, wenn der User auch die Rechte hat
        User user = getUserByNick(getNick());
		if (user != null && user.isOp())
		{
    		settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.HELP_BAN")); //$NON-NLS-1$
    		settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.HELP_UNBAN")); //$NON-NLS-1$
    		settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.HELP_KICK")); //$NON-NLS-1$
    		settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.HELP_KICK_REASON")); //$NON-NLS-1$
    		settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.HELP_TOPIC")); //$NON-NLS-1$
    	    settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.HELP_BOLD")); //$NON-NLS-1$
		}
	}

	/**
	 * Negiert den Ignorierenstatus des Users mit dem übergebenen nick.
	 * Es wird als Bestätigung eine Nachricht im Chat ausgegeben.
	 *
	 * @param nick
	 */
	public void doIgnore(String nick)
	{
		if (config.isIgnoredUser(nick))
		{
			config.removeIgnoredUser(nick);
			settlersLobby.onCommandAnswer(MessageFormat.format(
                    I18n.getString("IRCCommunicator.MSG_UNIGNORE"), nick)); //$NON-NLS-1$
		} else
		{
			config.addIgnoredUser(nick);
			settlersLobby.onCommandAnswer(MessageFormat.format(
			        I18n.getString("IRCCommunicator.MSG_IGNORE"), nick)); //$NON-NLS-1$
		}
		updateUserListInGui();
	}

	/**
	 * Setzt den Modus des Users mit dem angegebenen Nick auf b+.
	 * Gibt im Chat eine Nachricht als Bestätigung aus.
	 *
	 * @param nick
	 * @
	 */
	public void doBan(String nick)
	{
		if (iAmOp())
		{
			settlersLobby.onCommandAnswer(MessageFormat.format(
                    I18n.getString("IRCCommunicator.MSG_BAN"), nick )); //$NON-NLS-1$
			ban(channel, nick + "!*@*"); //$NON-NLS-1$
		}
	}
	
	public void doBanlist()
	{
		if (iAmOp())
		{
			sendMessageToServer("/cs banlist #S3Lobby", false, ""); //$NON-NLS-1$
		}
	}

	/**
	 * Setzt den Modus des Users mit dem angegebenen Nick auf b-.
	 * Gibt im Chat eine Nachricht als Bestätigung aus.
	 *
	 * @param nick
	 * @
	 */
	private void doUnBan(String nick)
	{
		if (iAmOp())
		{
			settlersLobby.onCommandAnswer(MessageFormat.format(
			        I18n.getString("IRCCommunicator.MSG_UNBAN"), nick )); //$NON-NLS-1$
			unBan(channel, nick + "!*@*"); //$NON-NLS-1$
		}
	}

	/**
	 * Prüft, ob der vom PircBot benutzte User Operator ist.
	 * Ist er dies nicht, wird eine Nachricht im Chatfenster ausgegeben.
	 *
	 * @return	True, wenn der vom PircBot benutzte User Operator ist, andernfalls
	 * 			false
	 * @
	 */
	private boolean iAmOp()
	{
		User myUser = getUserByNick(getNick());
		if (myUser.isOp())
		{
			return true;
		} else
		{
			settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.ERROR_OP_ONLY")); //$NON-NLS-1$
			return false;
		}
	}

	/**
	 * Prüft, ob der User mit dem übergebenen nick im Channel ist und kickt ihn
	 * unter Angabe des übergebenen Grundes.
	 *
	 * @param	nick	Der Nick des zu kickenden Users.
	 * @param	reason	Der Grund, warum der User gekickt wurde.
	 * @
	 */
	public void doKick(String nick, String reason)
	{
		if (iAmOp())
		{
			User kickUser = getUserByNick(nick);
			if (kickUser != null)
			{
				kick(channel, nick, reason);
				settlersLobby.onCommandAnswer(MessageFormat.format(
				        I18n.getString("IRCCommunicator.MSG_KICK"), nick, reason)); //$NON-NLS-1$
			} else
			{
				settlersLobby.onCommandAnswer(MessageFormat.format(
				        I18n.getString("IRCCommunicator.ERROR_NOT_IN_CHANNEL"), nick )); //$NON-NLS-1$
			}
		}
	}

	/**
	 * Sucht den User mit dem übergebenen Nick im Channel und gibt ihn zurück.
	 *
	 * @param	nick	Der Nick des gesuchten Users.
	 * @return	Der gesuchte User. Null, wenn der User nicht gefunden wurde.
	 * @
	 */
	public User getUserByNick(String nick)
	{
		User[] allUsers = getUsers(channel);
		User resultUser = null;
		for (int i = 0; i < allUsers.length; i++)
		{
			if (allUsers[i].getNick().equals(nick))
			{
				resultUser = allUsers[i];
				break;
			}
		}
		return resultUser;
	}

	public void sendProgramMessage(String programMessage)
	{
	    // eine Notice geht an den Channel, die sehen alle außer
	    // der user selber
	    // daher wird noch eine notice an den user selbst geschickt,
	    // damit er seine eigenen Nachrichten auch mitbekommt
	    sendProgramMessageTo(programMessage, channel);
	    sendProgramMessageTo(programMessage, getNick());
	}

	/**
	 * Send IRC-message to single user or the hole channel.
	 * Including check of the message is to long for IRC (max. 512 bytes)
	 * and split message into 2 or more parts.
	 *
	 * @param programMessage	message to send
	 * @param user 				single user or channel (= the receiver)
	 */
	public void sendProgramMessageTo(String programMessage, String user)
	{
	    String msg = PROGRAM_MESSAGE_TOKEN + programMessage;
	    // if encryption for messages is active
	    if (config.isDoEncryption())
	    {
	    	String nick = getNick();
	    	
	    	// encrypt the message with nickname of the sender
	        msg = CryptoSupport.encode(msg, nick, CRYPTO_TOKEN);
	        
	        // if the length of the encrypted base64-string exceeds 460 characters 
	        // (max length for IRC-protocol: 512 = 460 + overhead)
	        // split the original-string in multiple string, sending them with 
	        // md5-hash of original-string as marker
	        if( msg.length() > 400 ) {
	        	// create the md5-hash which includes the original message and the timestamp in ms
	        	String msgHash = CryptoSupport.md5(msg + System.currentTimeMillis());
	        	
	        	// Split the encrypted message in its parts
	        	// -> use 460 - 32 (hash) - 6 (crypto-marker and multipart-settings) = 422 as length for each part
	        	String[] msgParts = msg.split("(?<=\\G.{362})");
	        	
	        	// create and send an initial value with info about
	        	// -> md5-hash
	        	// -> and how much parts must be received
	        	String msgInit = CryptoSupport.encode(msgHash + msgParts.length, nick, CRYPTO_MULTIPART_MESSAGE_TOKEN);
	        	sendProgrammMessage(msgInit, user);
	        	
	        	// loop through the message-parts and send each
	        	int sort = 0;
	        	for (String msgPart: msgParts) {
	        		msgPart = CRYPTO_MULTIPART_MESSAGE_TOKEN + msgHash + CRYPTO_MULTIPART_MESSAGE_TOKEN + sort + CRYPTO_MULTIPART_MESSAGE_TOKEN + msgPart;
	        		
	        		// and send it
	        		sendProgrammMessage(msgPart, user);
	        		
	        		sort++;
	        	}
	        	// stop execution of this function
	        	return;
	        }
	    }
	    sendProgrammMessage(msg, user);
	}
	
	public void sendProgrammMessage(String msg, String user) {
	    if (prepareShutdown)
	    {
	        //die letzten ProgrammMessages noch möglichst schnell absenden
	        sendNoticeDirect(user, msg);
	    }
	    else
	    {
	        //senden via Queue
	    	sendNotice(user, msg);
	    }
	}

	private void sendNoticeDirect(String target, String notice)
	{
		sendRawLine("NOTICE " + target + " :" + notice); //$NON-NLS-1$ //$NON-NLS-2$
	}


	@Override
	public void onJoin(String channel, String sender, String login,
			String hostname)
	{
	    super.onJoin(channel, sender, login, hostname);
		User[] users = getUsers(channel);
		ircUserList.updatePircUsers(channel, users);
		ircUserList.addUser(sender);

		settlersLobby.onJoin(sender);
		updateUserListInGui();
		if (sender.equals(getNick()))
		{
		    //set channel in gui
		    settlersLobby.onTopic(channel, null);
		}
	}


	@Override
	public void onMessage(String channel, String sender, String login,
			String hostname, String message)
	{
	    super.onMessage(channel, sender, login, hostname, message);
    	//Eine Nachricht, die mit /bold beginnt, darf nur von einem Op geschickt werden.
    	if (message.startsWith(Colors.BOLD))
    	{
    		IrcUser user = ircUserList.getUser(sender);
            if (user != null && user.isOp())
            {
            	//Als Op darf er die Nachricht fett ausgeben.
                message = message.substring(Colors.BOLD.length());
    		    settlersLobby.ircChatMessage(sender, message, true);
            }
            else
            {
                //Diese Nachricht wurde nicht durch das /bold Kommando
                //von einem alobby-client ausgelöst, das ist nämlich nur als
                //op erlaubt, es ist entweder von einem normalen irc client
                //oder durch einen sonstigen Trick.
            	//Daher bleibt das bold-Zeichen erhalten und die Nachricht
            	//wird normal-gedruckt ausgegeben, das bold-Zeichen wird zu einem
                //eckigen Kästchen, da es ein nicht-druckbares Zeichen ist.
        	    settlersLobby.ircChatMessage(sender, message, false);
            }
    	}
    	else
    	{
			settlersLobby.ircChatMessage(sender, message, false);
    	}
	}

	@Override
	public void onPrivateMessage(String sender, String login,
			String hostname, String message)
	{
	    super.onPrivateMessage(sender, login, hostname, message);
		settlersLobby.ircWhisperedMessage(sender, message);
	}
	
	private Map<String, Map<Integer,String>> noticesArray = new HashMap<String, Map<Integer,String>>();
	private Map<String, Integer> noticesInitArray = new HashMap<String, Integer>();

	/**
	 * Vom IRC wurde eine Notice erhalten. Da sämtliche Programmsteuernachrichten
	 * als Notice gesendet werden, enthält diese Methode einen Parser, der alle
	 * Programmsteuernachrichten erkennt. Zusätzlich werden auch noch bestimmte
	 * Fehlermeldungen erkannt, die vom IRC-Server ebenfalls als Notice gesendet
	 * werden.
	 */

	@Override
	public void onNotice(String sourceNick, String sourceLogin,
			String sourceHostname, String target, String notice)
	{
		super.onNotice(sourceNick, sourceLogin, sourceHostname, target, notice);
	    boolean isEncrypted = false;
	    if (notice.length() > 0 && notice.startsWith(CRYPTO_TOKEN))
	    {
	        isEncrypted = true;
	        notice = CryptoSupport.decode(notice, sourceNick, CRYPTO_TOKEN);
	        Test.outputWithIpFilter(notice);
	    }
	    // if notice was send via xmlrpc it starts with another token and was build with another encryption
	    else if( notice.length() > 0 && notice.startsWith(CRYPTO_XMLRPC_TOKEN) )
	    {
	        isEncrypted = true;
	        notice = Base64Coder.decodeString(notice.substring(CRYPTO_XMLRPC_TOKEN.length()));
	        Test.outputWithIpFilter(notice);
	    }
	    // if this is a multipart-message add it to the message-array 
	    // and check if all parts of this message where received
	    else if( notice.length() > 0 && notice.startsWith(CRYPTO_MULTIPART_MESSAGE_TOKEN) ) {
	    	// check if this is an initial message or a content-message
	    	// (initial message does only have one CRYPTO_MULTIPART_MESSAGE_TOKEN)
	    	char[] md5HashArray = new char[32];
	    	String md5Hash = "";
	    	if( ( notice.length() - notice.replace(CRYPTO_MULTIPART_MESSAGE_TOKEN, "").length() ) == 3 ) {
	    		// its a content-message
	    		// -> get the hash-value between the marker
	    		notice.getChars(1, 33, md5HashArray, 0);
	    		if( md5HashArray != null ) {
	    			md5Hash = String.valueOf(md5HashArray);
	    			// -> get the sort-marker
		    		String noticeSort = notice.replace(CRYPTO_MULTIPART_MESSAGE_TOKEN + md5Hash + CRYPTO_MULTIPART_MESSAGE_TOKEN, "");
		    		Integer sort = Integer.parseInt(String.valueOf(noticeSort.substring(0, noticeSort.indexOf(CRYPTO_MULTIPART_MESSAGE_TOKEN))));
	    			
	    			// -> add the string to the hashmap
	    			if( noticesArray.get(md5Hash) == null ) {
	    				Map<Integer,String> value = new HashMap<Integer,String>();
	    				value.put(sort, notice);
	    				noticesArray.put(md5Hash, value);
	    			}
	    			else {
	    				Map<Integer,String> map = noticesArray.get(md5Hash);
	    				map.put(sort, notice);
	    			}
	    		}
	    	}
	    	else {
	    		// its a initial message
	    		// -> decode message
	    		notice = CryptoSupport.decode(notice, sourceNick, CRYPTO_MULTIPART_MESSAGE_TOKEN);
	    		
	    		// -> get the md5-hash (first 32 chars)
	    		notice.getChars(0, 32, md5HashArray, 0);
	    		if( md5HashArray != null ) {
	    			md5Hash = String.valueOf(md5HashArray);
	    		
	    			// -> get the message-part-count
	    			char msgPartCount = notice.charAt(32);
	    			
	    			// save them in hashmap
	    			if( noticesInitArray.get(md5Hash) == null ) {
	    				noticesInitArray.put(md5Hash, Integer.parseInt(String.valueOf(msgPartCount)));
	    			}
	    		}
	    	}
	    	// check if the last message-part completed a full message
	    	boolean ready = false;
	    	if( false == md5Hash.equals("") ) {
	    		if( noticesInitArray.get(md5Hash) != null ) {
	    			if( noticesArray.get(md5Hash) != null ) {
	    				if( noticesArray.get(md5Hash).size() == noticesInitArray.get(md5Hash).intValue() ) {
	    					// get the message-part-string into one string
	    					Map<Integer,String> map = noticesArray.get(md5Hash);
	    					ArrayList<Integer> sortedKeys = new ArrayList<Integer>(map.keySet()); 
	    					Collections.sort(sortedKeys);
	    					notice = "";
	    					for (Integer x : sortedKeys) {
	    						notice = notice.concat(map.get(x).replace(CRYPTO_MULTIPART_MESSAGE_TOKEN + md5Hash + CRYPTO_MULTIPART_MESSAGE_TOKEN + x + CRYPTO_MULTIPART_MESSAGE_TOKEN, ""));
	    					}
	    					// -> decode the string with multipart-token
	    	    			notice = CryptoSupport.decode(notice, sourceNick, CRYPTO_MULTIPART_MESSAGE_TOKEN);
	    	    			// -> decode the string again with normal crypto-token
	    	    			notice = CryptoSupport.decode(notice, sourceNick, CRYPTO_TOKEN);
	    					Test.outputWithIpFilter("resulting notice: " + notice);
	    					// remove the message-parts from array
	    					noticesArray.remove(md5Hash);
	    					noticesInitArray.remove(md5Hash);
	    					// set marker
	    					ready = true;
	    				}
	    			}
	    		}
	    	}
	    	if( false == ready ) {
	    		return;
	    	}
	    	else {
	    		isEncrypted = true;
	    	}
	    }
	    	    
		if (notice.length() >= PROGRAM_MESSAGE_TOKEN.length() &&
		        notice.substring(0, PROGRAM_MESSAGE_TOKEN.length()).equals(PROGRAM_MESSAGE_TOKEN))
		{
		    if (prepareShutdown)
		    {
		        //sobald der shutdown vorbereitet wird, werden alle eintreffenden
		        //ProgramMessages ignoriert
		        //andernfalls könnten dadurch eventuell noch neue Ausgangsnachrichten
		        //produziert werden, was nicht gewollt ist.
		        return;
		    }
		    if (config.isTrustOnlyEncryptedMessages() && isEncrypted == false)
		    {
		        //es wurde eine Programmnachricht empfangen, aber sie war nicht verschlüsselt
		        //da nur verschlüsselte akzeptiert werden, wird die Nachricht
		        //heimlich verworfen
		        Test.output("ignored: " + sourceNick + ": " + notice); //$NON-NLS-1$ //$NON-NLS-2$
		        return;
		    }
		    //es wurde eine Programmsteuernachricht empfangen, weiteres handling
		    //in settlersLobby
			settlersLobby.ircProgramMessage(notice
					.substring(PROGRAM_MESSAGE_TOKEN.length()), sourceNick, target);
			return;
		}
		
		String[] splittedNotice = notice.split(" "); //$NON-NLS-1$
		// Liest aus, ob das Passwort zu kurz ist:
		// NOTICE test :Versuchen Sie es erneut mit einem ungewöhnlicheren
		// Passwort. Passwörter sollten
		// mindestens fünf Zeichen lang sein und nicht zu leicht zu erraten
		// sein.
		if (splittedNotice.length >= 8 && splittedNotice[0].equals("Versuchen") //$NON-NLS-1$
				&& splittedNotice[6].equals("ungewöhnlicheren") //$NON-NLS-1$
				&& splittedNotice[7].equals("Passwort.")) //$NON-NLS-1$
		{
			statusOfRegistrationIsKnown = true;
			userIsRegistrated = false;
			reasonForFailedRegistration = I18n.getString("IRCCommunicator.ERROR_PASSWORD_TOO_SHORT"); //$NON-NLS-1$
			return;
		}

		// Liest aus, ob das Passwort zu kurz ist:
		// NOTICE test :Passworte sollten mindestens 5 Zeichen lang sein und
		if (splittedNotice.length >= 8 && splittedNotice[0].equals("Passworte") //$NON-NLS-1$
				&& splittedNotice[2].equals("mindestens") //$NON-NLS-1$
				&& splittedNotice[4].equals("Zeichen")) //$NON-NLS-1$
		{
			statusOfRegistrationIsKnown = true;
			userIsRegistrated = false;
			reasonForFailedRegistration = I18n.getString("IRCCommunicator.ERROR_PASSWORD_TOO_SHORT"); //$NON-NLS-1$
			return;
		}
		// Liest aus, ob der User erfolgreich registriert wurde:
		// "NOTICE newnick :Der Chatname "newnick" wurde für Sie registriert."
		if (splittedNotice.length >= 7 && splittedNotice[1].equals("Chatname") //$NON-NLS-1$
				&& splittedNotice[3].equals("wurde") //$NON-NLS-1$
				&& splittedNotice[6].equals("registriert.")) //$NON-NLS-1$
		{
			statusOfRegistrationIsKnown = true;
			userIsRegistrated = true;
			return;
		}

		// Liest aus, ob der User erfolgreich registriert wurde:
		// "NOTICE newnick : Dein Nickname newnick ist unter deinem Host registriert worden: ..."
		if (splittedNotice.length >= 7 && splittedNotice[1].equals("Nickname") //$NON-NLS-1$
				&& splittedNotice[7].equals("registriert") //$NON-NLS-1$
				&& splittedNotice[8].equals("worden:")) //$NON-NLS-1$
		{
			statusOfRegistrationIsKnown = true;
			userIsRegistrated = true;
			return;
		}

		// Liest aus, ob die Registration gescheitert ist:
		// NOTICE newnick :Der Chatname newnick ist bereits registriert.
		if (splittedNotice.length >= 6 && splittedNotice[1].equals("Chatname") //$NON-NLS-1$
				&& splittedNotice[4].equals("bereits") //$NON-NLS-1$
				&& splittedNotice[5].equals("registriert.")) //$NON-NLS-1$
		{
			statusOfRegistrationIsKnown = true;
			userIsRegistrated = false;
			reasonForFailedRegistration = I18n.getString("IRCCommunicator.ERROR_NICK_ALREADY_REGISTERED"); //$NON-NLS-1$
			return;
		}

		// Liest aus, ob die Registration gescheitert ist:
		// NOTICE newnick :Dieser Nickname newnick ist bereits registriert!
		if (splittedNotice.length >= 6 && splittedNotice[1].equals("Nickname") //$NON-NLS-1$
				&& splittedNotice[4].equals("bereits") //$NON-NLS-1$
				&& splittedNotice[5].equals("registriert!")) //$NON-NLS-1$
		{
			statusOfRegistrationIsKnown = true;
			userIsRegistrated = false;
			reasonForFailedRegistration = I18n.getString("IRCCommunicator.ERROR_NICK_ALREADY_REGISTERED"); //$NON-NLS-1$
			return;
		}

		// ungültige email
	    if (splittedNotice.length >= 5 && splittedNotice[1].equals("ist") //$NON-NLS-1$
	            && splittedNotice[2].equals("keine") //$NON-NLS-1$
	            && splittedNotice[3].equals("gültige") //$NON-NLS-1$
	            && splittedNotice[4].equals("eMail-Adresse.")) //$NON-NLS-1$
	    {
	        statusOfRegistrationIsKnown = true;
            userIsRegistrated = false;
            reasonForFailedRegistration = I18n.getString("IRCCommunicator.ERROR_INVALID_EMAIL"); //$NON-NLS-1$
            return;
	    }

		if (isEncrypted == false)
		{
		    //Die Nachricht war nicht verschlüsselt und es ist keine
		    //Programmnachricht. Daher soll sie ausgegeben werden, eventuell
		    //ist sie vom chanserv/nickserv oder einem normalen irc-client

		    if (isNickServe(sourceNick, sourceLogin, sourceHostname))
		    {
		    	// if message containts "E-mail-Adresse" than cut the email from string 
		    	// and show it in settings
		    	if( notice.contains("E-mail-Adresse") ) {
		    		String email = notice.replace("E-mail-Adresse:", "").trim();
		    		if( email.matches(ALobbyConstants.EMAIL_VALIDATION_STRING) ) {
		    			// set the email to show in settings-window
		    			settlersLobby.getGUI().getOptionsFrame().setEmail(email);
		    		}
		    	}
		    	else {
		    		if( notice.contains("Email address") ) {
			    		String email = notice.replace("Email address:", "").trim();
			    		if( email.matches(ALobbyConstants.EMAIL_VALIDATION_STRING) ) {
			    			// set the email to show in settings-window
			    			settlersLobby.getGUI().getOptionsFrame().setEmail(email);
			    		}
			    	}
		    	}
		        if (notice.startsWith("STATUS"))
		        {
		            //do not show STATUS messages in the GUI
		            return;
		        }
		    }

		    if (isFromIrcServer(sourceNick, sourceLogin, sourceHostname))
		    {
		        //Notice vom IRC-Server
                if (notice.startsWith("Your \"real name\"")) //$NON-NLS-1$
                {
                    //diese Antwort kommt vom SETNAME Befehl, der für
                    //den away Mechanismus missbraucht wird, daher wird
                    //diese Antwort nicht angezeigt
                }
                else
                {
    		        //Bei Notice vom ircserver ist target falsch gesetzt
    		        //bug im pircbot?
                    settlersLobby.onNotice(sourceNick, "", notice); //$NON-NLS-1$
                }
		    }
		    else
		    {
		        settlersLobby.onNotice(sourceNick, target, notice);
		    }
		}
	} // onNotice

    private boolean isFromIrcServer(String sourceNick, String sourceLogin, String sourceHostname)
    {
        //it seems to be a bug from the pircbot, that sourceLogin and sourceHostname are empty
        //when the notice is from the irc server, nevertheless, we exploit it
        return sourceNick.toLowerCase().startsWith("irc.") ||       //$NON-NLS-1$
            (sourceLogin.isEmpty() && sourceHostname.isEmpty());
    }


	@Override
	public void onUserList(String channel, User[] users)
	{
	    super.onUserList(channel, users);
	    ircUserList.updatePircUsers(channel, users);
	    ircUserList.sychronizeUsers();

	    startScanAwayStatus();

	    updateUserListInGui();
	}

    /**
	 * wird aufgerufen, wenn jemand den Channel verlässt
     * @
	 */

	@Override
	public void onPart(String channel, String sender, String login,
			String hostname)
	{
	    super.onPart(channel, sender, login, hostname);
	    User[] users = getUsers(channel);
	    ircUserList.updatePircUsers(channel, users);
	    ircUserList.removeUser(sender);

		settlersLobby.userHasLeft(sender);
		updateUserListInGui();
		settlersLobby.onPart(sender, hostname);
	}

	/**
	 * wird aufgerufen, wenn jemand den Server verlässt
	 */

	@Override
	public void onQuit(String sourceNick, String sourceLogin,
			String sourceHostname, String reason)
	{
	    super.onQuit(sourceNick, sourceLogin, sourceHostname, reason);
	    User[] users = getUsers(channel);
	    ircUserList.updatePircUsers(users);
	    ircUserList.removeUser(sourceNick);

		settlersLobby.userHasLeft(sourceNick);
		updateUserListInGui();
		settlersLobby.onQuit(sourceNick, sourceHostname, reason);
	}


	@Override
	public void onOp(String channel, String sourceNick, String sourceLogin,
			String sourceHostname, String recipient)
	{
	    super.onOp(channel, sourceNick, sourceLogin, sourceHostname, recipient);
	    User[] users = getUsers(channel);
	    ircUserList.updatePircUsers(channel, users);
	    updateUserListInGui();
	}


	@Override
	public void onDeop(String channel, String sourceNick,
			String sourceLogin, String sourceHostname, String recipient)
	{
	    super.onDeop(channel, sourceNick, sourceLogin, sourceHostname, recipient);
	    User[] users = getUsers(channel);
	    ircUserList.updatePircUsers(channel, users);
	    updateUserListInGui();
	}


	@Override
	public void onNickChange(String oldNick, String login, String hostname,
			String newNick)
	{
	    super.onNickChange(oldNick, login, hostname, newNick);

	    User[] users = getUsers(channel);
	    ircUserList.updatePircUsers(users);
	    //der user wurde umbenannt, in ircUserList updaten
	    ircUserList.renameUser(oldNick, newNick);

		if (oldNick.equals(settlersLobby.getNick()))
		{
			settlersLobby.setUserName(newNick); // Nick, den die SettlersLobby
			// hält
			// und nur bei sich selbst angezeigt
			// wird
			// Grund für obige Diskrepanz ist, dass onMessage nicht ausgeführt
			// wird,
			// wenn man selbst eine Nachricht versendet

		}
		settlersLobby.onNickChange(oldNick, newNick);
		updateUserListInGui();
	}


	@Override
	public void onKick(String channel, String kickerNick,
			String kickerLogin, String kickerHostname, String recipientNick,
			String reason)
	{
	    super.onKick(channel, kickerNick, kickerLogin, kickerHostname, recipientNick, reason);
		if (getNick().equals(recipientNick))
		{
			settlersLobby.onKick(reason);
		} else
		{
	        User[] users = getUsers(channel);
	        ircUserList.updatePircUsers(channel, users);
	        ircUserList.removeUser(recipientNick);

	        updateUserListInGui();
			settlersLobby.userHasLeft(recipientNick);
			settlersLobby.onKick(recipientNick, kickerNick, kickerHostname);
		}
	}



	@Override
    public void onMode(String channel, String sourceNick, String sourceLogin, String sourceHostname, String mode)
    {
        super.onMode(channel, sourceNick, sourceLogin, sourceHostname, mode);
        settlersLobby.onCommandAnswer(String.format("%s %s mode %s", channel, sourceNick, mode));
    }

    /**
	 * Sollte bei unserer Implementierung nur aufgerufen werden,
	 * wenn der Server die Verbindung geschlossen hat.
	 * Wenn wir selber rausgehen, wird dispose() benutzt, was kein
	 * onDisconnect() aufruft.
	 */

	@Override
	public void onDisconnect()
	{
	    super.onDisconnect();
	    stopBackgroundThreads();
        ircUserList.clearAll();
	}



    @Override
	public void onTopic(String channel, String topic, String setBy,
            long date, boolean changed)
    {
        super.onTopic(channel, topic, setBy, date, changed);
        if (channel.equalsIgnoreCase(this.channel))
        {
            settlersLobby.onTopic(channel, topic);
        }
    }

	/*
	 * public void onIncomingFileTransfer(DccFileTransfer transfer) {
	 * Test.output("aufgerufen");
	 * settlersLobby.onIncomingFileTransfer(transfer); }
	 */
	/*
	 * public void onFileTransferFinished(DccFileTransfer transfer, Exception
	 * e) { Test.output("aufgerufen");
	 * settlersLobby.onFileTransferFinished(transfer, e); }
	 */

    /**
	 * Joint den Channel und startet das automatische Awaygehen.
	 */
	public void joinChannel()
	{
		if (channel != null)
	    {
		    //no channel, no topic
		    settlersLobby.onTopic("#", ""); //$NON-NLS-1$
		    joinChannel(channel);
	    }
	}

    public void sendWhois(String nick)
    {
    	sendRawLineViaQueue("WHOIS " + nick); //$NON-NLS-1$
    }

    public void sendTopic(String topic)
    {
    	setTopic(channel, topic);
    }

    public void sendAway(String msg)
    {
        msg = (msg == null) ? "" : msg; //$NON-NLS-1$
        sendRawLineViaQueue("AWAY :" + msg); //$NON-NLS-1$
        //away msg in der Userliste setzen
        ircUserList.setAwayUser(getNick(), msg);
    }

    public void sendMotd()
    {
    	sendRawLineViaQueue("MOTD");
	}


    public void setTimeDisplay(){
    	if(settlersLobby.isShowTime()){
			settlersLobby.setShowTime(false);
		    settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.MSG_TIME_OFF"));	 //$NON-NLS-1$
		}

		else{
			settlersLobby.setShowTime(true);
			settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.MSG_TIME_ON"));					 //$NON-NLS-1$
		}
    }

    public void setChatLogging(){
    	if (!settlersLobby.getChatlog().isLogging())
    	{
			settlersLobby.getChatlog().startLogging();
			if (settlersLobby.getChatlog().isLogging())
		    	settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.MSG_CHATLOG_ON")); //$NON-NLS-1$
			else
				settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.ERROR_CHATLOG")); //$NON-NLS-1$
		}
		else if (settlersLobby.getChatlog().isLogging())
		{
			settlersLobby.getChatlog().stopLogging();
			settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.MSG_CHATLOG_OFF"));			 //$NON-NLS-1$
		}
    }
    /* (non-Javadoc)
     * @see org.jibble.pircbot.PircBot#onServerResponse(int, java.lang.String)
     */

    @Override
	public void onServerResponse(int code, String response)
    {
        super.onServerResponse(code, response);
        switch (code)
        {
            case ReplyConstants.RPL_WHOISIDLE:
            {
                //317     RPL_WHOISIDLE
                //      "<nick> <integer> :seconds idle"
                if (!hideWhois)
                {
                    Test.output("Idle: " + response); //$NON-NLS-1$
                }
                break;
            }
            case ReplyConstants.RPL_AWAY:
            {
                //301     RPL_AWAY
                //      "<nick> :<away message>"
                int firstSpace = response.indexOf(' ');
                int secondSpace = response.indexOf(' ', firstSpace + 1);
                int msgStart = response.lastIndexOf(':', secondSpace + 1);

                //String sourceNick = response.substring(0, firstSpace);
                String targetNick = response.substring(firstSpace + 1, secondSpace);
                String awayMessage = response.substring(msgStart + 1);
                ircUserList.setAwayUser(targetNick, awayMessage);
                if (!hideWhois)
                {
                    settlersLobby.onCommandAnswer(MessageFormat.format(
                            I18n.getString("IRCCommunicator.MSG_USER_AWAY"), //$NON-NLS-1$
                            targetNick, awayMessage));
                    updateUserListInGui();
                }
                break;
            }
            case ReplyConstants.RPL_NOWAWAY:
            {
                //306     RPL_NOWAWAY
                //      ":You have been marked as being away"

                //kommt nur für den eigenen User
                ircUserList.setAwayUser(getNick(), true);
                sendSetIrcname(awayIrcName());
                updateUserListInGui();
                break;
            }
            case ReplyConstants.RPL_UNAWAY:
            {
                //305     RPL_UNAWAY
                //      ":You are no longer marked as being away"

                //kommt nur für den eigenen User
                ircUserList.setAwayUser(getNick(), false);
                sendSetIrcname(defaultIrcName());
                updateUserListInGui();
                break;
            }
            case ReplyConstants.RPL_WHOREPLY:
            {

                //352     RPL_WHOREPLY
                //      "<channel> <user> <host> <server> <nick> \
                //      <H|G>[*][@|+] :<hopcount> <real name>"
                try
                {
                    StringTokenizer st = new StringTokenizer(response);
                    for (int i=0; i < 5; i++)
                    {
                        st.nextToken();
                    }
                    String nick = st.nextToken();
                    int index = response.indexOf(':');
                    if (index != -1)
                    {
                        index = response.indexOf(' ', index + 1);
                        String ircName = response.substring(index + 1);
                        if (ircName.startsWith(AWAY_INDICATOR))
                        {
                            ircUserList.setAwayUser(nick, true);
                        }
                        else if(ircName.startsWith(HERE_INDICATOR))
                        {
                            ircUserList.setAwayUser(nick, false);
                        }
                    }
                }
                catch (Exception e)
                {}
                break;
            }
            case ReplyConstants.RPL_ENDOFWHO:
            {
                updateUserListInGui();
                startRetrieveAwayMessages();
                break;
            }
            case ReplyConstants.RPL_ENDOFWHOIS:
            {
                if (this.hideWhois && this.hideCommandMsg != null)
                {
                    if (response.contains(this.hideCommandMsg))
                    {
                        //die response gehört zu der letzten
                        //automatischen whois Abfrage
                        //hideWhois kann zurückgesetzt werden
                        setVerbose(ALobbyConstants.IRC_DEBUG_ON);
                        this.hideWhois = false;
                        this.hideCommandMsg = null;
                    }
                }
                updateUserListInGui();
                break;
            }
            case ReplyConstants.RPL_WHOISUSER:
            {

                //311    RPL_WHOISUSER
                //"<nick> <user> <host> * :<real name>"
                int firstSpace = response.indexOf(' ');
                int secondSpace = response.indexOf(' ', firstSpace + 1);
                if (secondSpace == -1)
                {
                    //ungültiges Format
                    break;
                }
                String whoisNick = response.substring(firstSpace + 1, secondSpace);

                // Ein whois liefert alle Daten,
                // die in der IrcUserList gespeichert werden.
                // Diese hier ist die erste Info,
                // die zurückgesendet wird.
                // Daher werden jetzt alle bisher gespeicherten infos gelöscht,
                // ansonsten würde die away message nie zurückgesetzt werden.
                ircUserList.clearInfoUser(whoisNick);
                break;
            }
            case ReplyConstants.ERR_CHANOPRIVSNEEDED:
            {
                settlersLobby.onCommandAnswer(I18n.getString("IRCCommunicator.ERROR_OP_ONLY")); //$NON-NLS-1$
                break;
            }
            //the following lines are for motd
            case ReplyConstants.RPL_MOTDSTART:
            case ReplyConstants.RPL_MOTD:
            case ReplyConstants.RPL_ENDOFMOTD:
            {
            	settlersLobby.onCommandAnswer(response.substring(response.indexOf(' ')+ 1));
            	break;
            }

            case ReplyConstants.RPL_BANLIST:
                settlersLobby.onCommandAnswer(response.substring(response.indexOf(' ')+ 1));
                break;
            case ReplyConstants.RPL_CHANNELMODEIS:
            {
                Matcher m = Pattern.compile("(\\S+) (\\S+) (\\S+)\\s*").matcher(response); //$NON-NLS-1$
                if (m.matches())
                {
                    settlersLobby.onCommandAnswer("Mode " + m.group(2) + ": " + m.group(3));
                }
                break;
            }
            default:
                //log all error codes, even if the user cannot handle them
                if (isErrorCode(code))
                {
                    settlersLobby.onCommandAnswer(response.substring(response.indexOf(' ')+ 1));
                }
        }
    }

    private boolean isErrorCode(int code)
    {
        return code >= 400 && code < 600;
    }

    /* (non-Javadoc)
     * @see org.jibble.pircbot.PircBot#onConnect()
     */

    /**
     * sobald sich irgendetwas in der ircUserList verändert hat,
     * muss auch die Anzeige in der GUI aktualisiert werden.
     */
    public void updateUserListInGui()
    {
        settlersLobby.ircSendUserList(ircUserList.getSortedUserList());
    }

    private void sendSetIrcname(String ircname)
    {
        if (ircname.length() > 50)
        {
            //setname ist limiert
            ircname = ircname.substring(0, 50);
        }
        sendRawLineViaQueue("SETNAME :" + ircname); //$NON-NLS-1$
    }

    public void sendWho(String channel)
    {
    	sendRawLineViaQueue("WHO " + channel); //$NON-NLS-1$
    }

    /**
     * startet einen TimerTask, der
     * den Away Status der User überprüft.
     * Es wird der RealName ausgelesen, der eventuell
     * den AWAY_INDICATOR enthält. In dem Fall
     * wird der User als away angezeigt, auch wenn man die
     * away message nicht kennt.
     * Der TimerTask wird inzwischen nur noch einmal ausgeführt, da alle
     * weiteren away Meldungen über Programmsteuernachrichten
     * empfangen werden.
     */
    private void startScanAwayStatus()
    {
        stopScanAwayStatus();
        ircScanAwayTask = new TimerTaskCompatibility() {

            @Override
			public void run()
            {
                sendWho(channel);
            }
        };
        //in 1 s und dann alle 60s einmal /WHO #channel senden
        //timerScanAwayStatus.schedule(
        //        task, 1000, 60000);
        //Jetzt wird der task nur noch einmalig nach dem joinen ausgeführt.
        //Alle weiteren away Meldungen werden über Programmnachrichten ausgetauscht.
        //Dadurch erscheinen die user sofort als away und die clients müssen
        //den Server nicht ständig mit /WHO Anfragen belasten.
        ircScanAwayTask.schedule(1000);
    }

    private void stopScanAwayStatus()
    {
        if (ircScanAwayTask != null)
        {
            ircScanAwayTask.cancel();
        }
    }

    public String getChannel()
    {
        return channel;
    }

    /**
     * sendet einen /WHOIS Befehl an den IRC-Server, ohne
     * die ouput-queue zu benutzen. Für die Dauer, bis alle
     * daraus resultieren Antworten erhalten worden sind, werden
     * die IRC-Aktivitäten nicht geloggt.
     *
     * @param msg /WHOIS Befehl, kann einen oder eine Liste von Nicks enthalten
     */
    public void sendWhoisWithoutLog(String msg)
    {
        if (msg != null && (!msg.isEmpty()))
        {
            this.hideWhois = true;
            this.hideCommandMsg = msg;
            //ohne queue senden
            sendWhoisDirect(msg);
            //keine Ausgabe ins log
            setVerbose(false);
        }
    }

    private void sendWhoisDirect(String msg)
    {
        if (msg != null)
        {
        	sendRawLine("WHOIS " + msg); //$NON-NLS-1$
        }
    }

    /**
     * startet einen task, der die away messages ermittelt.
     */
    private void startRetrieveAwayMessages()
    {
        stopRetrieveAwayMessages();
        this.ircSendWhoisTask = new IrcWhoisSender(this.ircUserList.getAwayUserList(), this);
        //die Wiederholungen liegen höher als MessageDelay, damit die Queue
        //eine Chance hat Nachrichten zu senden
        ircSendWhoisTask.schedule(100, (long) (getMessageDelay() * 2.1));
    }

    private void stopRetrieveAwayMessages()
    {
        if (this.ircSendWhoisTask != null)
        {
            this.ircSendWhoisTask.cancel();
        }
    }

    private void stopBackgroundThreads()
    {
        stopRetrieveAwayMessages();
        stopScanAwayStatus();
    }

    /**
     * Wenn eine Programmnachricht empfangen wurde, dass ein user
     * away ist, wird diese Methode aufgerufen, um die
     * Information in die IrcUserList zu übertragen.
     *
     * @param username
     * @param awayMessage
     */
    public void onUserAway(String username, String awayMessage)
    {
        ircUserList.setAwayUser(username, awayMessage);
        updateUserListInGui();
    }

    /**
     * Wenn eine Programmnachricht empfangen wurde, dass ein user
     * nicht mehr away ist, wird diese Methode aufgerufen, um die
     * Information in die IrcUserList zu übertragen.
     *
     * @param username
     */
    public void onUserBack(String username)
    {
        ircUserList.setAwayUser(username, false);
        updateUserListInGui();
    }

    public IrcUser[] getSortedUserList()
    {
        return ircUserList.getSortedUserList();
    }


    @Override
	public void onAction(String sender, String login, String hostname,
            String target, String action)
    {
        super.onAction(sender, login, hostname, target, action);
        settlersLobby.onAction(sender, action);
    }


    @Override
	public synchronized void dispose()
    {
        // Ein normaler Aufruf von dispose setzt leider nicht
        // isConnected() zurück und es ist auch von aussen sonst nicht
        // erkennbar, ob schon dispose aufgerufen wurde.
        // Daher wird intern ein flag gesetzt, welches bei
        // isActive() ausgewertet werden kann.
        super.dispose();
        isDisposed = true;
    }

    public boolean isActive()
    {
        //wenn dispose aufgerufen wurde, ist isDisposed true.
        //falls der IrcServer die Verbindung abgebrochen hat,
        //ist isConnected() false.
        //nur wenn keines der beiden eingetroffen ist, liefert
        //isActive() true
        return !isDisposed && isConnected();
    }

    @Override
    public boolean isNickServe(String sourceNick, String sourceLogin, String sourceHostname)
    {
        //sourceHostname may be irc.siedler.net
        String serverInfo = getServerInfo();
        if (sourceNick.equalsIgnoreCase("NickServ") && //$NON-NLS-1$
            sourceLogin.equalsIgnoreCase("services") &&
            serverInfo != null)
        {
            //the following check has to handle all of the listed
            //possibilities of nickserv hostname and server-name:
            serverInfo = serverInfo.toLowerCase();
            String sourceHostnameLc = sourceHostname.toLowerCase();
            if (sourceHostnameLc.equals(serverInfo) ||
                sourceHostnameLc.endsWith(serverInfo) ||
                serverInfo.endsWith(sourceHostnameLc))
            {
                return true;
            }
            
            //new combination:
            if ((sourceHostnameLc.endsWith(".siedler3.net") && serverInfo.endsWith(".siedler3.net")) ||
                (sourceHostnameLc.endsWith("." + settlersLobby.config.getMainDomain()) && serverInfo.endsWith("." + settlersLobby.config.getMainDomain())))
            {
                return true;
            }
        } else if (sourceNick.equalsIgnoreCase("NickServ") &&
                sourceLogin.equalsIgnoreCase("NickServ") &&
                serverInfo != null &&
                serverInfo.toLowerCase().endsWith(".freenode.net"))
        {
            //for testing purposes
            String sourceHostnameLc = sourceHostname.toLowerCase();
            if (sourceHostnameLc.equals("services.") ||
                sourceHostnameLc.endsWith(".freenode.net"))
            {
                return true;
            }
        }
        return false;
    }

    @Override
    public IrcServerInfo getIrcServerInfo()
    {
        return ircServerCapabilities;
    }

    public void setChannel(String channel)
    {
        this.channel = channel;
        ircUserList.setChannel(channel);
    }

    /**
     * Get a list of all users in chat in alphabetical order.
     * 
     * @return
     */
	public Map<String, String> getUserListSortedByName() {
		
		// get the list
		IrcUser[] ircUserSortedBySettings = ircUserList.getSortedUserList();
		
		// create a map with the nicknames of all users
		Map<String, String> aMap = new HashMap<String, String>();
		for( int i = ircUserSortedBySettings.length - 1; i >= 0; i-- ) {
			String value = ircUserSortedBySettings[i].getNick();
			aMap.put(value.toLowerCase(), value);
		}
		
		// sort this list
		aMap = ArraySupport.sortByValue(aMap);
		return aMap;		
	}

	/**
	 * Set the vpn-state of one user.
	 * 
	 * @param userName
	 * @param vpnState
	 */
	public void onVPNStateChange(String userName, boolean vpnState) {
		ircUserList.setUserVPNState(userName, vpnState);
        updateUserListInGui();
	}

	/**
	 * Return a String with installed S3- and S4-Version.
	 * @return
	 */
	private String getSettlerVersionString() {
		String S3 = "";
    	if (settlersLobby.isS3GamePathValid())
    	{
        	if (S3Support.isGog(settlersLobby.getS3Directory().getAbsolutePath()))
        	{
        		S3 = "S3-GOG;";
        	} else {
        		S3 = "S3-CD;";
        	}
    	}
    	String S4 = "";
    	if (settlersLobby.isS4GamePathValid())
    	{
	    	File s4Exedir = ReqQuery.getS4ExeDirectory();
		    if (s4Exedir != null && s4Exedir.exists() && s4Exedir.isDirectory())
		    {
		    	if (S4Support.isGog(s4Exedir.getAbsolutePath()))
		    	{
		    		S4 = "S4-GOG;";
		    	} else {
		    		S4 = "S4-CD;";
		    	}
		    }
    	}
	    return S3 + S4;
	}

	/**
	 * Return the IRC-capabilities-object.
	 * 
	 * @return IrcServerCapabilities
	 */
	public IrcServerCapabilities getIRCCapabilities() {
		return this.ircServerCapabilities;
	}
	
}
