package net.siedler3.alobby.communication;

import java.util.regex.Pattern;


public class NickServPatterns
{
    //TODO should be moved to a separate configuration
    public static final String PATTERN_PASSWORD_1 = "mindestens fünf Zeichen lang sein und nicht zu leicht zu erraten"; //$NON-NLS-1$
    public static final String PATTERN_PASSWORD_2 = "Passworte sollten mindestens 5 Zeichen lang sein"; //$NON-NLS-1$
    public static final String PATTERN_PASSWORD_2_anope2= "Please try again with a more obscure password."; //$NON-NLS-1$
    public static final String PATTERN_INVALID_EMAIL_1 = "E-Mail-Adressen müssen der Form"; //$NON-NLS-1$
    public static final String PATTERN_INVALID_EMAIL_2 = "ist keine gültige eMail-Adresse"; //$NON-NLS-1$
    public static final String PATTERN_INVALID_EMAIL_2_anope2 = "Incorrect email address."; //$NON-NLS-1$
    public static final String PATTERN_TIME_DELAY_1 = "Sekunden vor der Benutzung des REGISTER-Befehls warten"; //$NON-NLS-1$
    public static final String PATTERN_EMAIL_SENT_2 = "bitte tippe /msg NickServ confirm"; //$NON-NLS-1$
    public static final String PATTERN_EMAIL_SENT_3_freenode = "An email containing nickname activation instructions has been sent to"; //$NON-NLS-1$
    public static final Pattern PATTERN_REGEX_REQUIRES_AUTH_CODE_2 = Pattern.compile("Dieser Nickname wurde bereits angefordert, bitte pr.*fe deinen Email Account f.*r das Passwort."); //$NON-NLS-1$
    public static final String PATTERN_WRONG_PASSWORD = "Falsches Passwort."; //$NON-NLS-1$
    public static final String PATTERN_WRONG_PASSWORD_2_freenode = "Invalid password"; //$NON-NLS-1$
    public static final String PATTERN_WRONG_PASSWORD_3_anope2 = "Password incorrect."; //$NON-NLS-1$
    public static final Pattern PATTERN_REGEX_PASSWORD_CHANGED_2 = Pattern.compile("Passwort ge.*ndert."); //$NON-NLS-1$
    public static final Pattern PATTERN_REGEX_PASSWORD_CHANGED_2_anope2 = Pattern.compile("Password for (.+) changed.");
    public static final Pattern PATTERN_REGEX_PASSWORD_CHANGED_3_freenode = Pattern.compile("The password for (.+) has been changed to (.+)");
    public static final Pattern PATTERN_REGEX_RESEND_2 = Pattern.compile("Dein Passcode wurde noch einmal an (.+@.+) geschickt."); //$NON-NLS-1$
    public static final Pattern PATTERN_REGEX_EMAIL_NOT_CONFIRMED_2_anope18 = Pattern.compile("^Dieser Nickname ben.*tigt das Email Best.*tigungs Passwort vor Abschlu.* der Registrierung.$"); //$NON-NLS-1$
    public static final Pattern PATTERN_REGEX_EMAIL_NOT_CONFIRMED_2 = Pattern.compile("^(.+) is an unconfirmed nickname.$"); //$NON-NLS-1$
    public static final Pattern PATTERN_REGEX_RESET_FLAG_EMAIL_NOT_CONFIRMED_2 = Pattern.compile("^Dein Nickname .+ ist unter deinem Host registriert worden: .+$"); //$NON-NLS-1$
    public static final Pattern PATTERN_REGEX_RESEND_NOT_DONE_2 = Pattern.compile("Dein Account ist bereits best.*tigt."); //$NON-NLS-1$
    public static final Pattern PATTERN_REGEX_RESEND_EMAIL_BLOCKED_2 = Pattern.compile("Bitte warte noch (.\\d+.) Sekunden und versuche es dann erneut."); //$NON-NLS-1$
    public static final Pattern PATTERN_REGEX_RESEND_EMAIL_SERVICE_DOWN_2 = Pattern.compile("Die Services k.*nnen gerade keine Mail verschicken, bitte versuche es sp.*ter noch einmal."); //$NON-NLS-1$
    public static final Pattern PATTERN_RESET_NOT_REGISTERED_NICK_2_anope2 = Pattern.compile("Nick (.+) isn't registered."); //$NON-NLS-1$
    public static final Pattern PATTERN_RESET_NOT_REGISTERED_NICK_3_freenode = Pattern.compile("(.+) is not a registered nickname."); //$NON-NLS-1$
    public static final Pattern PATTERN_RESET_EMAIL_SENT_2_anope2 = Pattern.compile("Password reset email for (.+) has been sent."); //$NON-NLS-1$
    public static final String PATTERN_AUTHENTICATED_NICK_2_anope2 = "Du bist jetzt für deinen Nick angemeldet."; //$NON-NLS-1$
    public static final String PATTERN_AUTHENTICATED_NICK_3_anope2 = "Du bist jetzt für Deinen Nick angemeldet."; //$NON-NLS-1$
    public static final String PATTERN_INVALID_PASSCODE_2_anope2 = "Invalid passcode."; //$NON-NLS-1$
    public static final String PATTERN_RESET_AUTHENTIFICATION_EXPIRED_2_anope2 = "Deine Anfrage für die Zurücksetzung des Passworts ist abgelaufen.";  //$NON-NLS-1$
    public static final String PATTERN_NOT_LOGGED_IN_1_freenode = "You are not logged in."; //$NON-NLS-1$
    public static final String PATTERN_NOT_LOGGED_IN_2_freenode = "This nickname is registered. Please choose a different nickname, or identify"; //$NON-NLS-1$
    public static final String PATTERN_NOT_LOGGED_IN_3_anope2 = "This nickname is registered and protected.  If it is your"; //$NON-NLS-1$
    public static final String PATTERN_LOGGED_IN_NICK_1_freenode = "You are now identified for"; //$NON-NLS-1$
    public static final String PATTERN_LOGGED_IN_NICK_2_freenode = "You are logged in as"; //$NON-NLS-1$
    public static final String PATTERN_LOGGED_IN_NICK_3_anope2 = "Passwort akzeptiert - Du bist jetzt angemeldet."; //$NON-NLS-1$
    public static final String PATTERN_LOGGED_IN_NICK_4_anope2 = "You are now logged in as"; //$NON-NLS-1$

    public static boolean isNickServStatusReply(String nick, String[] splittedNotice)
    {
        //old anope format: STATUS nick status-code
        //anope 2.x format: STATUS nick status-code account 
        //only support nick==account
        return (splittedNotice.length == 3 &&
                splittedNotice[0].equals("STATUS") && //$NON-NLS-1$
                splittedNotice[1].equals(nick)) ||
               (splittedNotice.length == 4 &&
                splittedNotice[0].equals("STATUS") && //$NON-NLS-1$
                splittedNotice[1].equals(nick) &&
                splittedNotice[3].equals(nick)
                );
    }
}
