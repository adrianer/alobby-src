package net.siedler3.alobby.communication;
import net.siedler3.alobby.controlcenter.Test;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.jibble.pircbot.DccChat;
import org.jibble.pircbot.DccFileTransfer;
import org.jibble.pircbot.PircBot;
import org.jibble.pircbot.User;

public abstract class IrcEventDispatcher extends PircBot implements IrcInterface
{

	private static IrcEventDispatcher myInstance;
	private final List<IrcEventListener> eventListeners;

    public static IrcEventDispatcher getInstance() {
    	// TODO Wenn IRCCommunicator Klasse abgeschafft wurde,
    	// den diese Klasse in Singleton umwandeln.
        if (myInstance == null)
        {
            throw new NullPointerException("IrcInterface not initialised");
        }
    	return myInstance;
    }

	public IrcEventDispatcher()
	{
	    //need a CopyOnWriteArrayList here, because Listeners
	    //may decide to remove themselves exactly when they
	    //were just called by one of the onXXX Methods
	    //Without this there are a lot of ConcurrentModification Exceptions
		eventListeners = new CopyOnWriteArrayList<IrcEventListener>();
		try {
			super.setEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			Test.outputException(e);
		}
		updateSingletonInstance(this);
	}

	private static void updateSingletonInstance(IrcEventDispatcher newInstance)
	{
	    //there can be only one active IrcCommunicator. As the IrcCommunicator
	    //is disposed and created new for every new connection,
	    //the (Pseudo) singleton instance
	    //has to be updated as soon as a new IrcCommunicator is created
	    myInstance = newInstance;
	}

    @Override
	public void addEventListener(IrcEventListener anEventHandler) {
	    if (!eventListeners.contains(anEventHandler))
	    {
	        eventListeners.add(anEventHandler);
	    }
	}

	@Override
	public boolean removeEventListener(IrcEventListener anEventHandler) {
		return eventListeners.remove(anEventHandler);
	}

	@Override
	public void setNick(String newNick)
	{
		super.setName(newNick);
		super.setUserName(newNick);
	}

	@Override
	public void _setVersion(String version)
	{
		super.setVersion(version);
	}

	@Override
	public void onConnect() {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onConnect();
		}
	}

	@Override
	public void onDisconnect() {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onDisconnect();
		}
	}

	@Override
	public void onServerResponse(int code, String response) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onServerResponse(code, response);
		}
	}

	@Override
	public void onUserList(String channel, User[] users) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onUserList(channel, users);
		}
	}

	@Override
	public void onMessage(String channel, String sender, String login, String hostname, String message) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onMessage(channel, sender, login, hostname, message);
		}
	}

	@Override
	public void onPrivateMessage(String sender, String login, String hostname, String message) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onPrivateMessage(sender, login, hostname, message);
		}
	}

	@Override
	public void onAction(String sender, String login, String hostname, String target, String action) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onAction(sender, login, hostname, target, action);
		}
	}

	@Override
	public void onNotice(String sourceNick, String sourceLogin, String sourceHostname, String target, String notice) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onNotice(sourceNick, sourceLogin, sourceHostname, target, notice);
		}
	}

	@Override
	public void onJoin(String channel, String sender, String login, String hostname) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onJoin(channel, sender, login, hostname);
		}
	}

	@Override
	public void onPart(String channel, String sender, String login, String hostname) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onPart(channel, sender, login, hostname);
		}
	}

	@Override
	public void onNickChange(String oldNick, String login, String hostname, String newNick) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onNickChange(oldNick, login, hostname, newNick);
		}
	}

	@Override
	public void onKick(String channel, String kickerNick, String kickerLogin, String kickerHostname, String recipientNick, String reason) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onKick(channel, kickerNick, kickerLogin, kickerHostname, recipientNick, reason);
		}
	}

	@Override
	public void onQuit(String sourceNick, String sourceLogin, String sourceHostname, String reason) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onQuit(sourceNick, sourceLogin, sourceHostname, reason);
		}
	}

	@Override
	public void onTopic(String channel, String topic, String setBy, long date, boolean changed) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onTopic(channel, topic, setBy, date, changed);
		}
	}

	@Override
	public void onChannelInfo(String channel, int userCount, String topic) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onChannelInfo(channel, userCount, topic);
		}
	}

	@Override
	public void onMode(String channel, String sourceNick, String sourceLogin, String sourceHostname, String mode) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onMode(channel, sourceNick, sourceLogin, sourceHostname, mode);
		}
	}

	@Override
	public void onUserMode(String targetNick, String sourceNick, String sourceLogin, String sourceHostname, String mode) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onUserMode(targetNick, sourceNick, sourceLogin, sourceHostname, mode);
		}
	}

	@Override
	public void onOp(String channel, String sourceNick, String sourceLogin, String sourceHostname, String recipient) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onOp(channel, sourceNick, sourceLogin, sourceHostname, recipient);
		}
	}

	@Override
	public void onDeop(String channel, String sourceNick, String sourceLogin, String sourceHostname, String recipient) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onDeop(channel, sourceNick, sourceLogin, sourceHostname, recipient);
		}
	}

	@Override
	public void onVoice(String channel, String sourceNick, String sourceLogin, String sourceHostname, String recipient) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onVoice(channel, sourceNick, sourceLogin, sourceHostname, recipient);
		}
	}

	@Override
	public void onDeVoice(String channel, String sourceNick, String sourceLogin, String sourceHostname, String recipient) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onDeVoice(channel, sourceNick, sourceLogin, sourceHostname, recipient);
		}
	}

	@Override
	public void onSetChannelKey(String channel, String sourceNick, String sourceLogin, String sourceHostname, String key) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onSetChannelKey(channel, sourceNick, sourceLogin, sourceHostname, key);
		}
	}

	@Override
	public void onRemoveChannelKey(String channel, String sourceNick, String sourceLogin, String sourceHostname, String key) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onRemoveChannelKey(channel, sourceNick, sourceLogin, sourceHostname, key);
		}
	}

	@Override
	public void onSetChannelLimit(String channel, String sourceNick, String sourceLogin, String sourceHostname, int limit) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onSetChannelLimit(channel, sourceNick, sourceLogin, sourceHostname, limit);
		}
	}

	@Override
	public void onRemoveChannelLimit(String channel, String sourceNick, String sourceLogin, String sourceHostname) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onRemoveChannelLimit(channel, sourceNick, sourceLogin, sourceHostname);
		}
	}

	@Override
	public void onSetChannelBan(String channel, String sourceNick, String sourceLogin, String sourceHostname, String hostmask) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onSetChannelBan(channel, sourceNick, sourceLogin, sourceHostname, hostmask);
		}
	}

	@Override
	public void onRemoveChannelBan(String channel, String sourceNick, String sourceLogin, String sourceHostname, String hostmask) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onRemoveChannelBan(channel, sourceNick, sourceLogin, sourceHostname, hostmask);
		}
	}

	@Override
	public void onSetTopicProtection(String channel, String sourceNick, String sourceLogin, String sourceHostname) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onSetTopicProtection(channel, sourceNick, sourceLogin, sourceHostname);
		}
	}

	@Override
	public void onRemoveTopicProtection(String channel, String sourceNick, String sourceLogin, String sourceHostname) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onRemoveTopicProtection(channel, sourceNick, sourceLogin, sourceHostname);
		}
	}

	@Override
	public void onSetNoExternalMessages(String channel, String sourceNick, String sourceLogin, String sourceHostname) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onSetNoExternalMessages(channel, sourceNick, sourceLogin, sourceHostname);
		}
	}

	@Override
	public void onRemoveNoExternalMessages(String channel, String sourceNick, String sourceLogin, String sourceHostname) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onRemoveNoExternalMessages(channel, sourceNick, sourceLogin, sourceHostname);
		}
	}

	@Override
	public void onSetInviteOnly(String channel, String sourceNick, String sourceLogin, String sourceHostname) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onSetInviteOnly(channel, sourceNick, sourceLogin, sourceHostname);
		}
	}

	@Override
	public void onRemoveInviteOnly(String channel, String sourceNick, String sourceLogin, String sourceHostname) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onRemoveInviteOnly(channel, sourceNick, sourceLogin, sourceHostname);
		}
	}

	@Override
	public void onSetModerated(String channel, String sourceNick, String sourceLogin, String sourceHostname) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onSetModerated(channel, sourceNick, sourceLogin, sourceHostname);
		}
	}

	@Override
	public void onRemoveModerated(String channel, String sourceNick, String sourceLogin, String sourceHostname) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onRemoveModerated(channel, sourceNick, sourceLogin, sourceHostname);
		}
	}

	@Override
	public void onSetPrivate(String channel, String sourceNick, String sourceLogin, String sourceHostname) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onSetPrivate(channel, sourceNick, sourceLogin, sourceHostname);
		}
	}

	@Override
	public void onRemovePrivate(String channel, String sourceNick, String sourceLogin, String sourceHostname) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onRemovePrivate(channel, sourceNick, sourceLogin, sourceHostname);
		}
	}

	@Override
	public void onSetSecret(String channel, String sourceNick, String sourceLogin, String sourceHostname) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onSetSecret(channel, sourceNick, sourceLogin, sourceHostname);
		}
	}

	@Override
	public void onRemoveSecret(String channel, String sourceNick, String sourceLogin, String sourceHostname) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onRemoveSecret(channel, sourceNick, sourceLogin, sourceHostname);
		}
	}

	@Override
	public void onInvite(String targetNick, String sourceNick, String sourceLogin, String sourceHostname, String channel)  {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onInvite(targetNick, sourceNick, sourceLogin, sourceHostname, channel);
		}
	}

	@Override
	public void onIncomingFileTransfer(DccFileTransfer transfer) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onIncomingFileTransfer(transfer);
		}
	}

	@Override
	public void onFileTransferFinished(DccFileTransfer transfer, Exception e) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onFileTransferFinished(transfer, e);
		}
	}

	@Override
	public void onIncomingChatRequest(DccChat chat) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onIncomingChatRequest(chat);
		}
	}

	@Override
	public void onVersion(String sourceNick, String sourceLogin, String sourceHostname, String target) {
		super.onVersion(sourceNick, sourceLogin, sourceHostname, target);
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onVersion(sourceNick, sourceLogin, sourceHostname, target);
		}
	}

	@Override
	public void onPing(String sourceNick, String sourceLogin, String sourceHostname, String target, String pingValue) {
		super.onPing(sourceNick, sourceLogin, sourceHostname, target, pingValue);
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onPing(sourceNick, sourceLogin, sourceHostname, target, pingValue);
		}
	}

	@Override
	public void onServerPing(String response) {
		super.onServerPing(response);
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onServerPing(response);
		}
	}

	@Override
	public void onTime(String sourceNick, String sourceLogin, String sourceHostname, String target) {
		super.onTime(sourceNick, sourceLogin, sourceHostname, target);
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onTime(sourceNick, sourceLogin, sourceHostname, target);
		}
	}

	@Override
	public void onFinger(String sourceNick, String sourceLogin, String sourceHostname, String target) {
		super.onFinger(sourceNick, sourceLogin, sourceHostname, target);
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onFinger(sourceNick, sourceLogin, sourceHostname, target);
		}
	}

	@Override
	public void onUnknown(String line) {
		for (IrcEventListener eventListener : eventListeners) {
			eventListener.onUnknown(line);
		}
	}
}
