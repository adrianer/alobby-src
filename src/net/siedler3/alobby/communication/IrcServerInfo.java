package net.siedler3.alobby.communication;

public interface IrcServerInfo
{
    public int getMaxTargetsForCommand();

    public boolean isRegistrationRequiresEmailAuthentication();
    public boolean isResetPasswordSupported();
    public boolean hideRegistrationInFrontend();

}