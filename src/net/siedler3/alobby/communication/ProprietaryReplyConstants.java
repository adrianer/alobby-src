package net.siedler3.alobby.communication;

public class ProprietaryReplyConstants
{
    public static final int RPL_ISUPPORT = 5;
    public static final int RPL_PROTOCOL = RPL_ISUPPORT;
    public static final int RPL_WHOISHOST = 378;
    public static final int ERR_SERVICESDOWN = 440;
}
