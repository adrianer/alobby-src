package net.siedler3.alobby.communication.protocol;

import net.siedler3.alobby.controlcenter.OpenGame;

public abstract class ProgramMessageAdapter implements IProgramMessageListener
{

    @Override
    public void onPrgMsgNewGame(OpenGame game)
    {
    }

    @Override
    public void onPrgMsgNewCurrentPlayerNumber(String ip, String hostUserName, int currentPlayerNumber)
    {
    }

    @Override
    public void onPrgMsgNewMaxPlayerNumber(String ip, String hostUserName, int maxPlayerNumber)
    {
    }

    @Override
    public void onPrgMsgCloseGame(String ip, String hostUserName)
    {
    }

    @Override
    public void onPrgMsgQueryOpenGame(String userName)
    {
    }

    @Override
    public void onPrgMsgIncrementCurrentPlayerNumber(String userName)
    {
    }

    @Override
    public void onPrgMsgDecrementCurrentPlayerNumber(String userName)
    {
    }

    @Override
    public void onPrgMsgNewGameName(String ip, String hostUserName, String name)
    {
    }

    @Override
    public void onPrgMsgAwayMessage(String userName, String awayMessage)
    {
    }

    @Override
    public void onPrgMsgAwayBack(String userName)
    {
    }

    @Override
    public void onPrgMsgUserWantsToJoin(String userName, String map)
    {
    }

    @Override
    public void onPrgMsgUserJoined(String userName, String map, String ip)
    {
    }

    @Override
    public void onPrgMsgUserLeaving(String userName, String map)
    {
    }

    @Override
    public void onPrgMsgBroadcastMapUserList(String hostUserName, String map, String userList)
    {
    }

    @Override
    public void onPrgMsgBroadcastUserLeft(String hostUserName, String map, String userName)
    {
    }

}
