package net.siedler3.alobby.communication.protocol;

import net.siedler3.alobby.controlcenter.OpenGame;

public interface IProgramMessageListener
{
    public void onPrgMsgNewGame(OpenGame game);

    public void onPrgMsgNewCurrentPlayerNumber(String ip, String hostUserName, int currentPlayerNumber);

    public void onPrgMsgNewMaxPlayerNumber(String ip, String hostUserName, int maxPlayerNumber);

    public void onPrgMsgCloseGame(String ip, String hostUserName);

    public void onPrgMsgQueryOpenGame(String userName);

    public void onPrgMsgIncrementCurrentPlayerNumber(String userName);

    public void onPrgMsgDecrementCurrentPlayerNumber(String userName);

    public void onPrgMsgNewGameName(String ip, String hostUserName, String name);

    public default void onPrgMsgNewGameMode(String ip, String hostUserName, long gameMode) { }

    public void onPrgMsgAwayMessage(String userName, String awayMessage);

    public void onPrgMsgAwayBack(String userName);

    public void onPrgMsgUserWantsToJoin(String userName, String map);

    public void onPrgMsgUserJoined(String userName, String map, String ip);

    public void onPrgMsgUserLeaving(String userName, String map);
    /**
     *
     * @param hostUserName
     * @param map
     * @param userList comma separated list of usernames
     */
    public void onPrgMsgBroadcastMapUserList(String hostUserName, String map, String userList);

    public void onPrgMsgBroadcastUserLeft(String hostUserName, String map, String userName);

	public void onPrgMsgGameRunning(OpenGame game);

	public void onPrgMsgCloseRunningGame(String ip, String hostUserName);

	public void onPrgMsgQueryRunningGame(String userName);

	public void onPrgMsgSetRaceInGame(String hostUserName, String senderName, String race);

	public void onPrgMsgCloseStream(String user);

	public void onPrgMsgNewStream(String user, String link, String onlinesince);

	public void onPrgMsgVPNStateMessage(String senderName, boolean userVpnState);

	public void onPrgMsgQueryVPNState(String userName);

	public void onPrgMsgS4StartGame(String senderName, String ip, String playername, String ipUserlist);

	public void onPrgMsgDirectMessage(String receiver, String message);

	public void onPrgMsgBetaUserJoined(String user);

	public void onPrgMsgQueryBetaUsers(String userName);

	public void onPrgMsgCommunityNews(String prio, String md5, String date, String title, String text, String link, String type);

	public void onPrgMsgCommunityNewsRemove(String type);

}
