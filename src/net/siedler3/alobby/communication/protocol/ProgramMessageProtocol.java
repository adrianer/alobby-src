package net.siedler3.alobby.communication.protocol;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.GoodsInStock;
import net.siedler3.alobby.controlcenter.OpenGame;
import net.siedler3.alobby.controlcenter.OpenGame.MiscType;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.s3gameinterface.S3Language;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.util.Base64Coder;

public class ProgramMessageProtocol
{
    public enum Token
    {
        NEW_GAME(0),
        NEW_CURRENT_PLAYER_NUMBER(1),
        NEW_MAX_PLAYER_NUMBER(2),
        CLOSE_GAME(3),
        QUERY_OPEN_GAME(4), //previously named SEND_OPEN_GAME
        INCREMENT_CURRENT_PLAYER_NUMBER(5),
        DECREMENT_CURRENT_PLAYER_NUMBER(6),
        //NEW_GOODS_IN_STOCK(7),
        NEW_GAME_NAME_ENC(8),
        NEW_GAME_ENC(9),
        USER_WANTS_TO_JOIN(10),
        USER_JOINED(11),
        USER_LEAVING(12),
        MAP_USER_LIST(13),
        USER_XY_HAS_LEFT(14),
        USER_AWAY_MESSAGE(20),
        USER_AWAY_BACK(21),
    	GAME_RUNNING(22),
    	CLOSE_RUNNING_GAME(23),
    	QUERY_RUNNING_GAME(24),
    	RACE_IN_GAME(25),
    	NEW_STREAM(26),
    	CLOSE_STREAM(27),
    	USER_ACTIVATED_VPN(28),
    	USER_DEACTIVATED_VPN(29),
    	QUERY_USER_VPN_STATE(30),
    	S4_GAME_START(31), 
    	DIRECT_MESSAGE(32),  // a message send by external application through xmlrpc into chat for a single user 
    	BETA_USER(33),
    	QUERY_BETA_USER(34),
    	COMMUNITYNEWS(35),
    	COMMUNITYNEWS_REMOVE(36),
        NEW_GAME_MODE(37)
        ;

        private int value;

        private Token(int value)
        {
            this.value = value;
        }

        public String getValue()
        {
            return Integer.toString(this.value);
        }

        public static Token valueOf(int value)
        {
            for (Token token : values())
            {
                if (token.value == value)
                {
                    return token;
                }
            }
            return null;
        }

    }

    private static final char SEPARATOR = ';';
    private static final String PROTOCOL_ERROR = "Protocol Error"; //$NON-NLS-1$
    private static final String MESSAGE_TYPE_SAVEGAME = "savegame"; //$NON-NLS-1$
    private static final String MESSAGE_TYPE_NORMAL = "normal"; //$NON-NLS-1$
    private static final String MESSAGE_TYPE_LEAGUEGAME = "leaguegame"; //$NON-NLS-1$
    private static final String MESSAGE_TYPE_RUNNINGGAME = "runninggame"; //$NON-NLS-1$

    private List<IProgramMessageListener> listenerList = new ArrayList<IProgramMessageListener>();


    public void addListener(IProgramMessageListener listener)
    {
        if (listener == null)
        {
            throw new NullPointerException();
        }

        if (!listenerList.contains(listener))
        {
            listenerList.add(listener);
        }
    }

    public void removeListener(IProgramMessageListener listener)
    {
        if (listener == null)
        {
            throw new NullPointerException();
        }
        listenerList.remove(listener);
    }

    /**
     * Create message about new current player number of an open game.
     * 
     * @param game	the game-object
     * @return 		the message-string
     */
    public String createMsgNewCurrentPlayerNumber(OpenGame game)
    {
        return createMsgPlayerNumber(Token.NEW_CURRENT_PLAYER_NUMBER, game.getCurrentPlayer(), game);
    }
    
    /**
     * Create message about new maxplayer number of an open game.
     * 
     * @param game	the game-object
     * @return 		the message-string
     */
    public String createMsgNewMaxPlayerNumber(OpenGame game)
    {
        return createMsgPlayerNumber(Token.NEW_MAX_PLAYER_NUMBER, game.getMaxPlayer(), game);
    }

    private String createMsgPlayerNumber(Token token, int numberOfPlayers, OpenGame game)
    {
        String result = format(token, game.getIP(), game.getHostUserName(), numberOfPlayers);
        return result;
    }

    public String createMsgNewGameMode(OpenGame game, long newGameModeMask)
    {
        return format(Token.NEW_GAME_MODE, game.getIP(), game.getHostUserName(), newGameModeMask);
    }

    public String createMsgNewGameName(OpenGame game)
    {
        //the game name may contain semicolons (which is the separator character),
        //so always Base64-encode the game name
        String encodedName = Base64Coder.encodeString(game.getName());
        String result = format(Token.NEW_GAME_NAME_ENC, game.getIP(), game.getHostUserName(), encodedName);
        return result;
    }

    public String createMsgGameInfo(OpenGame game) {
    	return createMsgGameInfo(game, Token.NEW_GAME_ENC);
    }
    
    public String createMsgRunningGameInfo(OpenGame game) {
    	return createMsgGameInfo(game, Token.GAME_RUNNING);
    }
    
    public String createMsgGameInfo(OpenGame game, Token token)
    {
        String amazonenString = Boolean.toString(game.isAmazonen());
        String trojanString = Boolean.toString(game.isTrojans());
        String coopString = Boolean.toString(game.isCoop());
        int goods = game.getGoods();
        String programMessageData;
        String name;
        String map;
        String isLeagueGameString = encodeBoolean(game.isLeagueGame());
        String misc = Integer.toString(game.getMisc().value());
        String saveGameFileName = game.getSaveGameFileName();
        boolean wimo = game.isWimo();
        if (saveGameFileName.toLowerCase().endsWith(".mps"))
        {
            //cut off suffix ".mps"
            saveGameFileName = saveGameFileName.substring(0, saveGameFileName.length() - 4);
        } else if (saveGameFileName.toLowerCase().contains(".mps."))
        {
            //cut off suffix ".mps.{integer}"
            saveGameFileName = saveGameFileName.substring(0, saveGameFileName.toLowerCase().indexOf(".mps."));
        }
        String timeStamp = Long.toString(game.getCreationTime());
        
        // if this game is a running game, get its timestamp
        if( token.equals(Token.GAME_RUNNING) ) {
        	timeStamp = Long.toString(game.getRunningGameStartTimestamp());
        }
        else if ( !saveGameFileName.isEmpty() )
        {
        	// otherwise get the time of creation
        	timeStamp = Long.toString(game.getSaveGameTimestamp()); 
        }
        
        // get the teamcount (for S4)
        int teamcount = game.getTeamCount();
        
        // get the tournament-settings
        int tournamentid = game.getTournamentId();
        String tournamentname = game.getTournamentName();
        int round = game.getTournamentRound();
        int groupnumber = game.getTournamentGroupnumber();
        int matchnumber = game.getTournamentMatchnumber();
        // TODO remove tournamentPlayerlist in future protocol as we must use another way to get the tournamentplayer-list
        //String tournamentPlayerlist = game.getTournamentPlayerlist();
        String tournamentPlayerlist = "";
        
        // get the playerlist
        String playerlist = game.getPlayerListAsString();

        // Add CE game options to the game's name
        long gameModes = 0;
        for (OpenGame.S3CEGameOption gameOption : game.getCeGameOptions()) {
            gameModes |= gameOption.getValue();
        }

        // encode mapname and host-username in each case
        name = Base64Coder.encodeString(game.getName());
        map = Base64Coder.encodeString(game.getMap());
        Test.output("Map name is: " + map);
        
        /**
         * Defining the protocol for transferring gaming-informations
         * via irc-protocol.
         * 
         * Since 08.2016 the first item represents the s3-protocol-version as integer-value.
         * 
         * Order for message (pre-2016):
         * Hard-coded:
         * - 0 => token
         * - 1 => own ip
         * - 2 => own nickname
         * - 3 => name of the created game
         * - 4 => map-filename
         * - 5 => max players
         * - 6 => current players
         * - 7 => amazon allowed?
         * - 8 => goods in stock
         * 
         * If it is a save-game:
         * - 9 => is this a league-game as save-game?
         * - 10 => cd-version
         * - 11 => filename of a save-game which will be re-opened
         * - 12 => timestamp
         * 
         * If it is not a save-game and not a league-game:
         * - 9 => not a league-game
         * - 10 => cd-version
         * 
         * If it is a league-game
         * - 9 => mark as league-game
         * 
         * Order for message (post-2016)
         * - 0 => token
         * - 1 => own ip
         * - 2 => own nickname
         * - 3 => s3-protocol-version (from version.properties)
         * - 4 => name of the created game
         * - 5 => map-filename
         * - 6 => max players
         * - 7 => current players
         * - 8 => amazon allowed?
         * - 9 => goods as index of GoodsInStock
         * - 10 => wimo-mark
         * - 11 => message-type: save-game, league-game, normal
         * 
         * If it is a save-game:
         * - 12 => is this a league-game as save-game?
         * - 13 => cd-version
         * - 14 => filename of a save-game which will be re-opened
         * - 15 => timestamp from save-file
         * - 16 => marker for S4-Trojans
         * - 17 => S4-teamcount
         * 
         * If it is not a save-game:
         * - 12 => not a league-game
         * - 13 => cd-version
         * - 14 => timestamp for running game
         * - 15 => marker for S4-Trojans
         * - 16 => S4-teamcount
         * - 17 => marker for S4-coop
         */

        // create game-info for protocol in actual s3-protocol-version-format
        Object[] mandatoryParams = {
                token,
                ALobbyConstants.versionProps.getProperty("s3-protocol-version"),
                game.getIP(),
                game.getHostUserName(),
                name,
                map,
                game.getMaxPlayer(),
                game.getCurrentPlayer(),
                amazonenString,
                goods,
                wimo,
        };
        List<Object> params = new ArrayList<Object>(Arrays.asList(mandatoryParams));

        //skip optional elements if possible to have shorter messages
        // If it is a save-game
        if (!saveGameFileName.isEmpty())
        {
            params.add(MESSAGE_TYPE_SAVEGAME);
            params.add(isLeagueGameString);
            params.add(misc);
            params.add(playerlist);
            params.add(tournamentid);
            params.add(tournamentname);
            params.add(round);
            params.add(groupnumber);
            params.add(matchnumber);
            params.add(Long.toString(gameModes, 10));
            params.add(tournamentPlayerlist);
            params.add(saveGameFileName);
            params.add(timeStamp);
            params.add(trojanString);
            params.add(teamcount);
            params.add(coopString);
        } else {
        	if( token.equals(Token.GAME_RUNNING) ) {
        		params.add(MESSAGE_TYPE_RUNNINGGAME);
        	}
        	else {
        		if (game.isLeagueGame()) {
        			params.add(MESSAGE_TYPE_LEAGUEGAME);
        		} else {
        			params.add(MESSAGE_TYPE_NORMAL);
        		}
            }
            params.add(isLeagueGameString);
            params.add(misc);
            params.add(playerlist);
            params.add(tournamentid);
            params.add(tournamentname);
            params.add(round);
            params.add(groupnumber);
            params.add(matchnumber);
            params.add(Long.toString(gameModes, 10));
            params.add(tournamentPlayerlist);
            params.add(timeStamp);
            params.add(trojanString);
            params.add(teamcount);
            params.add(coopString);
        }
        
        programMessageData = format(params.toArray());

        return programMessageData;
    }

    public String createMsgQueryOpenGames(String user)
    {
        return format(Token.QUERY_OPEN_GAME, user);
    }
    
    public String createMsgQueryRunningGames(String user)
    {
        return format(Token.QUERY_RUNNING_GAME, user);
    }

    public String createMsgIncrementCurrentPlayerNumber(String hostUserName)
    {
        //TODO hostUserName zur Nachricht hinzufügen?
        return format(Token.INCREMENT_CURRENT_PLAYER_NUMBER);
    }
    
    /**
     * Send a message to all user in the channel with the information which race this
     * user has in his game.
     * The message will only be recognized by other players in the same game. 
     * They will save this information in their own gamedata-file.
     * 
     * @param hostUserName
     * @param userName
     * @param race
     * @return
     */
    public String createMsgSendRaceToPlayersInGame(String hostUserName, String userName, String race) {
    	return format(Token.RACE_IN_GAME, hostUserName, userName, race);
	}

    public String createMsgDecrementCurrentPlayerNumber(String hostUserName)
    {
        //TODO hostUserName zur Nachricht hinzufügen?
        return format(Token.DECREMENT_CURRENT_PLAYER_NUMBER);
    }

    public String createMsgCloseGame(OpenGame game)
    {
        String result = format(Token.CLOSE_GAME, game.getIP(), game.getHostUserName());
        return result;
    }
    
    public String createMsgGameStarted(OpenGame game)
    {
        return format(createMsgGameInfo(game, Token.GAME_RUNNING));
    }
    
    public String createMsgCloseRunningGame(OpenGame game)
    {
        return format(Token.CLOSE_RUNNING_GAME, game.getIP(), game.getHostUserName());
    }

    public String createMsgAwayMessage(String userName, String awayMessage)
    {
        String result = format(Token.USER_AWAY_MESSAGE, userName, awayMessage);
        return result;
    }
    
    /**
     * Create a message-string to inform all users, that this user has
     * an active or inactive vpn-connection.
     * 
     * @param userName
     * @param vpnState
     * @return
     */
    public String createMsgVPNState(String userName, boolean vpnState)
    {
    	String result = format(Token.USER_ACTIVATED_VPN, userName);
    	if( false == vpnState ) {
    		result = format(Token.USER_DEACTIVATED_VPN, userName);
    	}
        return result;
    }

    public String createMsgAwayBack(String userName)
    {
        String result = format(Token.USER_AWAY_BACK, userName);
        return result;
    }

    public String createMsgUpdateGameState(String token, String userName, String mapName)
    {
        return format(token, userName, mapName);
    }
    
    public String createMsgUpdateGameState(String token, String userName, String mapName, String userIP)
    {
        return format(token, userName, mapName, userIP);
    }

    public String createMsgBrodcastUpdateGameState(String token, String userName, String mapName, String userToUpdate)
    {
        String result = format(token, userName, mapName, userToUpdate);
        return result;
    }


    public void parseProgramMessage(String programMessage, String senderName, String target, String ownUserName)
    {
        String[] tokens = programMessage.split(";"); //$NON-NLS-1$
        if (tokens.length > 0)
        {
            Token token;
            try
            {
                int value = Integer.valueOf(tokens[0]);
                token = Token.valueOf(value);
            }
            catch (Exception e)
            {
                //unknown token, ignore message
                Test.output(PROTOCOL_ERROR);
                return;
            }
            String[] tmpTokens = Arrays.copyOfRange(tokens, 1, tokens.length);
            switch (token)
            {
                case NEW_GAME:
                case NEW_GAME_ENC:
                    onPrgMsgNewGame(token, tmpTokens, senderName);
                    break;
                case NEW_CURRENT_PLAYER_NUMBER:
                case NEW_MAX_PLAYER_NUMBER:
                    onPrgMsgNewPlayerNumber(token, tmpTokens, senderName);
                    break;
                case CLOSE_GAME:
                    onPrgMsgCloseGame(tmpTokens, senderName);
                    break;
                case QUERY_OPEN_GAME:
                    onPrgMsgQueryOpenGame(tmpTokens, senderName);
                    break;
                case INCREMENT_CURRENT_PLAYER_NUMBER:
                case DECREMENT_CURRENT_PLAYER_NUMBER:
                    onPrgMsgDeOrIncrementCurrentPlayerNumber(token, tmpTokens, senderName);
                    break;
                case NEW_GAME_NAME_ENC:
                    onPrgMsgNewGameName(tmpTokens, senderName);
                    break;
                case USER_AWAY_MESSAGE:
                    onPrgMsgAwayMessage(tmpTokens, senderName);
                    break;
                case USER_AWAY_BACK:
                    onPrgMsgAwayBack(tmpTokens, senderName);
                    break;
                case USER_WANTS_TO_JOIN:
                case USER_LEAVING:
                case USER_JOINED:
                case MAP_USER_LIST:
                case USER_XY_HAS_LEFT:
                    onPrgMsgGameState(token, programMessage, senderName, target, ownUserName);
                    break;
                case GAME_RUNNING:
                	onPrgMsgGameRunning(token, tmpTokens, senderName);
                	break;
                case CLOSE_RUNNING_GAME:
                	onPrgMsgCloseRunningGame(token, tmpTokens, senderName);
                	break;
                case QUERY_RUNNING_GAME:
                	onPrgMsgQueryRunningGame(tmpTokens, senderName);
                	break;
                case RACE_IN_GAME:
                	onPrgMsgSetRaceInGame(tmpTokens, senderName);
                	break;
                case NEW_STREAM:
                	onPrgMsgNewStream(tmpTokens, senderName);
                	break;
                case CLOSE_STREAM:
                	onPrgMsgCloseStream(tmpTokens, senderName);
                	break;
                case USER_ACTIVATED_VPN:
                	onPrgMsgUserVPNState(senderName, true);
                	break;
                case USER_DEACTIVATED_VPN:
                	onPrgMsgUserVPNState(senderName, false);
                	break;
                case QUERY_USER_VPN_STATE:
                	onPrgMsgQueryVPNState(tmpTokens, senderName);
                	break;
                case S4_GAME_START:
                	onPrgMsgQueryS4StartGame(tmpTokens, senderName);
                	break;
                case DIRECT_MESSAGE:
                	onPrgMsgDirectMessage(tmpTokens);
                	break;
                case BETA_USER:
                	onPrgMsgBetaUserJoined(tmpTokens);
                	break;
                case QUERY_BETA_USER:
                	onPrgMsgQueryBetaUsers(tmpTokens);
                	break;
                case COMMUNITYNEWS:
                	onPrgMsgCommunityNews(tmpTokens, senderName);
                	break;
                case COMMUNITYNEWS_REMOVE:
                	onPrgMsgCommunityNewsRemove(tmpTokens, senderName);
                	break;
                case NEW_GAME_MODE:
                    onPrgMsgNewGameMode(token, tmpTokens, senderName);
                    break;
                default:
                    Test.output(PROTOCOL_ERROR + " -> unknown token: " + token);
            }
        }

    }

    /**
     * Add a Community News from incoming message.
     * 
     * @param tmpTokens
     * @param senderName
     */
    private void onPrgMsgCommunityNews(String[] tmpTokens, String senderName) {
    	if( tmpTokens.length != 7 ) {
    		Test.output(PROTOCOL_ERROR);
            return;
    	}
    	String prio = tmpTokens[0];
		String md5 = tmpTokens[1];
		String date = tmpTokens[2];
		String title = tmpTokens[3];
		String text = tmpTokens[4];
		String link = tmpTokens[5];
		String type = tmpTokens[6];
		for (IProgramMessageListener listener : listenerList)
        {
            listener.onPrgMsgCommunityNews(prio, md5, date, title, text, link, type);
        }
	}

    /**
     * Update the game mode for an open game.
     * @param elements
     */
	private void onPrgMsgNewGameMode(Token token, String[] elements, String senderName) {
        if (elements.length != 3)
        {
            Test.output(PROTOCOL_ERROR);
            return;
        }

        if (!bVerifySender(elements[1], senderName))
        {
            return;
        }
        String ip;
        String hostUserName;
        long gameMode = 0;
        try
        {
            ip = elements[0];
            hostUserName = elements[1];
            gameMode = Long.parseLong(elements[2], 10);
        }
        catch (IllegalArgumentException e)
        {
            // Der neue Name konnte nicht decoded werden, unschön,
            // aber unkritisch, es bleibt einfach der alte Name stehen.
            Test.outputException(e);
            return;
        }

        for (IProgramMessageListener listener : listenerList)
        {
            listener.onPrgMsgNewGameMode(ip, hostUserName, gameMode);
        }
    }

    /**
     * Remove a Community News from specific type via incoming message.
     * 
     * @param tmpTokens
     * @param senderName
     */
    private void onPrgMsgCommunityNewsRemove(String[] tmpTokens, String senderName) {
    	if( tmpTokens.length != 1 ) {
    		Test.output(PROTOCOL_ERROR);
            return;
    	}
    	String type = tmpTokens[0];
		for (IProgramMessageListener listener : listenerList)
        {
            listener.onPrgMsgCommunityNewsRemove(type);
        }
	}

	/**
     * Answer a query for beta-users.
     * 
     * @param tmpTokens
     */
    private void onPrgMsgQueryBetaUsers(String[] tokens) {
    	if (tokens.length != 1)
        {
            Test.output(PROTOCOL_ERROR);
            return;
        }
        String userName = tokens[0];
        for (IProgramMessageListener listener : listenerList)
        {
            listener.onPrgMsgQueryBetaUsers(userName);
        }
	}

	/**
     * Got info about channel joining beta-user.
     * Mark the user in userlist as beta-user of the actual user is one of this:
     * - Admin
     * - Mod
     * - Beta-User
     * 
     * @param tmpTokens
     */
    private void onPrgMsgBetaUserJoined(String[] tmpTokens) {
    	if (tmpTokens.length != 1)
        {
            Test.output(PROTOCOL_ERROR);
            return;
        }
    	String user = tmpTokens[0];
        for (IProgramMessageListener listener : listenerList)
        {
            listener.onPrgMsgBetaUserJoined(user);
        }
	}

	/**
     * Process an incoming direct-message for the user
     * from an external application.
     * E.g. used for info about vpn-key which is now ready to use.
     * 
     * @param tmpTokens
     */
    private void onPrgMsgDirectMessage(String[] tmpTokens) {
    	if (tmpTokens.length != 2)
        {
            Test.output(PROTOCOL_ERROR);
            return;
        }
    	String receiver = tmpTokens[0];
		String message = tmpTokens[1];
        for (IProgramMessageListener listener : listenerList)
        {
            listener.onPrgMsgDirectMessage(receiver, message);
        }
	}

	/**
     * Process an incoming message about starting S4-game.
     * 
     * @param tmpTokens
     * @param senderName
     */
    private void onPrgMsgQueryS4StartGame(String[] tmpTokens, String senderName) {
		if (tmpTokens.length != 4)
        {
            Test.output(PROTOCOL_ERROR);
            return;
        }
        if (!bVerifySender(tmpTokens[0], senderName))
        {
            return;
        }
        
		String ip = tmpTokens[1];
        String playername = tmpTokens[2];
        String ipUserlist = tmpTokens[3];
        Test.output("onPrgMsgQueryS4StartGame STARTING S4 game from " + senderName);
        for (IProgramMessageListener listener : listenerList)
        {
            Test.output("onPrgMsgQueryS4StartGame listener " + listener.toString() + " STARTING S4 game from " + senderName);
            listener.onPrgMsgS4StartGame(senderName, ip, playername, ipUserlist);
        }
	}

	private void onPrgMsgQueryVPNState(String[] tmpTokens, String senderName) {
    	if (tmpTokens.length != 1)
        {
            Test.output(PROTOCOL_ERROR);
            return;
        }
        if (!bVerifySender(tmpTokens[0], senderName))
        {
            return;
        }
        String userName = tmpTokens[0];
        for (IProgramMessageListener listener : listenerList)
        {
            listener.onPrgMsgQueryVPNState(userName);
        }
	}

	private void onPrgMsgUserVPNState(String senderName, boolean userVpnState) {
    	for (IProgramMessageListener listener : listenerList)
        {
            listener.onPrgMsgVPNStateMessage(senderName, userVpnState);
        }
	}

	/**
     *  Remove an active stream from list.
     *  Hint: the source for this request is the lobby-server, not the irc.
     */
    private void onPrgMsgCloseStream(String[] tmpTokens, String senderName) {
    	if( !senderName.equals("NickServ") ) {
    		return;
    	}
    	String user = tmpTokens[0];
    	for (IProgramMessageListener listener : listenerList)
        {
            listener.onPrgMsgCloseStream(user);
        }
	}

    /**
     *  Add a stream to list.
     *  Hint: 
     *  - the source for this request is normally the lobby-server, not the irc.
     *  - an irc message is send if the streamer himself joins the chat.
     */
	private void onPrgMsgNewStream(String[] tmpTokens, String senderName) {
		String user = tmpTokens[0];
		if( !senderName.equals("NickServ") && !user.toLowerCase().equals(senderName.toLowerCase()) ) {
    		return;
    	}
		String link = tmpTokens[1];
		String onlinesince = tmpTokens[2];
		for (IProgramMessageListener listener : listenerList)
        {
            listener.onPrgMsgNewStream(user, link, onlinesince);
        }
	}

	private void onPrgMsgCloseRunningGame(Token token, String[] elements, String senderName) {
    	if (elements.length != 2)
        {
            Test.output(PROTOCOL_ERROR);
            return;
        }
        if (!bVerifySender(elements[1], senderName))
        {
            return;
        }
        String ip;
        String hostUserName;
        ip = elements[0];
        hostUserName = elements[1];

        for (IProgramMessageListener listener : listenerList)
        {
            listener.onPrgMsgCloseRunningGame(ip, hostUserName);
        }
	}

	private void onPrgMsgGameState(Token token, String programMessage, String senderName, String receiver, String ownUserName)
    {
        //mapname kann weitere Semikolons enthalten, der greedy
        //quantifier sammelt so lange Zeichen, solange er die ganze
        //Zeile noch matchen kann. Die anderen Felder bekommen nur einen
        //reluctant quantifier (.+?), so dass sie nur bis zum nächsten Semikolon bzw. Zeilenende
        //Zeichen sammeln, aber selber auf keinen Fall ein Semikolon enthalten
        //Beispiel: pattern (.+?);(.+);(.+?)
        //input : 1;2;3;4;5
        //Ergebnis: group(1) = 1
        //          group(2) = 2;3;4
        //          group(3) = 5
        //alle zusätzlichen Semikolons sind also beim greedy quantifier gelandet
        //solange es nur einen Parameter gibt, der die zusätzlichen Semikolons
        //enthalten kann, kann man also mit solch einem regulären Ausdruck matchen
        //ein einfaches String.split(';') geht leider nicht mehr.

        //Token;sender;mapname(mit evtl semikolon)
        String patternHostMessage = "(\\d+);(.+?);(.+)"; //$NON-NLS-1$
        //Token;sender;mapname(mit evtl semikolon);Parameter(ohne Semikolon)
        //Der Parameter ist entweder die mapuserliste oder ein userName
        String patternBroadcastMessage = "(\\d+);(.+?);(.+);(.+?)"; //$NON-NLS-1$

        Matcher mHostMessage = Pattern.compile(patternHostMessage).matcher(programMessage);
        Matcher mBroadcastMessage = Pattern.compile(patternBroadcastMessage).matcher(programMessage);

        if (mHostMessage.matches() &&
            bVerifySender(mHostMessage.group(2), senderName) &&
            receiver.equals(ownUserName))
        {
            //die Nachrichten duerfen nur akzeptiert, wenn sie direkt an den
            //user adressiert wurden
            {
                String userName = mHostMessage.group(2);
                String map = mHostMessage.group(3);
                String[] programMessageList = programMessage.split(";");
                String ip = "";
                if( programMessageList.length == 4 ) {
                	ip = programMessageList[3];
                }
                for (IProgramMessageListener listener : listenerList)
                {
                    if (token == Token.USER_WANTS_TO_JOIN)
                    {
                        listener.onPrgMsgUserWantsToJoin(userName, map);
                    }
                    if (token == Token.USER_JOINED)
                    {
                        listener.onPrgMsgUserJoined(userName, map, ip);
                    }
                    if (token ==  Token.USER_LEAVING)
                    {
                        listener.onPrgMsgUserLeaving(userName, map);
                    }
                }
            }
        }
        //kein else if, sondern ein eigenes if. Falls nämlich mindestens ein semikolon
        //im mapnamen ist, matchen erstmal sowohl mHostMessage als auch mBroadcastMessage.
        //Erst wenn man das token untersucht, wird klar welche Art von Nachricht es ist,
        //das token wurde aber bewußt nicht mit in den regulären Ausdruck genommmen
        if (mBroadcastMessage.matches() &&
            bVerifySender(mBroadcastMessage.group(2), senderName))
        {
            String hostUserName = mBroadcastMessage.group(2);
            String map = mBroadcastMessage.group(3);

            //dies hier sind broadcast messages vom Host
            for (IProgramMessageListener listener : listenerList)
            {
                if (token == Token.MAP_USER_LIST)
                {
                    String userList = mBroadcastMessage.group(4);
                    listener.onPrgMsgBroadcastMapUserList(hostUserName, map, userList);
                }
                if (token == Token.USER_XY_HAS_LEFT)
                {
                    String userName = mBroadcastMessage.group(4);
                    listener.onPrgMsgBroadcastUserLeft(hostUserName, map, userName);
                }
            }
        }

    }

    private void onPrgMsgAwayBack(String[] elements, String senderName)
    {
        if (elements.length != 1)
        {
            Test.output(PROTOCOL_ERROR);
            return;
        }

        if (!bVerifySender(elements[0], senderName))
        {
            return;
        }

        String userName = elements[0];

        for (IProgramMessageListener listener : listenerList)
        {
            listener.onPrgMsgAwayBack(userName);
        }

    }

    private void onPrgMsgAwayMessage(String[] elements, String senderName)
    {
        if (elements.length < 2)
        {
            Test.output(PROTOCOL_ERROR);
            return;
        }

        if (!bVerifySender(elements[0], senderName))
        {
            return;
        }

        String userName = elements[0];
        //die Away Message könnte ; enthalten, daher alle restlichen tokens lesen
        String awayMessage = elements[1];
        for (int i = 2; i < elements.length; ++i)
        {
            awayMessage += SEPARATOR + elements[i];
        }

        for (IProgramMessageListener listener : listenerList)
        {
            listener.onPrgMsgAwayMessage(userName, awayMessage);
        }

    }

    private void onPrgMsgNewGameName(String[] elements, String senderName)
    {
        if (elements.length != 3)
        {
            Test.output(PROTOCOL_ERROR);
            return;
        }


        if (!bVerifySender(elements[1], senderName))
        {
            return;
        }
        String ip;
        String hostUserName;
        String name;
        try
        {
            ip = elements[0];
            hostUserName = elements[1];
            name = Base64Coder.decodeString(elements[2]);
        }
        catch (IllegalArgumentException e)
        {
            // Der neue Name konnte nicht decoded werden, unschön,
            // aber unkritisch, es bleibt einfach der alte Name stehen.
            Test.outputException(e);
            return;
        }

        for (IProgramMessageListener listener : listenerList)
        {
            listener.onPrgMsgNewGameName(ip, hostUserName, name);
        }

    }

    private void onPrgMsgDeOrIncrementCurrentPlayerNumber(Token token, String[] elements, String senderName)
    {
        if (elements.length != 0)
        {
            Test.output(PROTOCOL_ERROR);
            return;
        }
        //TODO senderName is no present in elements
        for (IProgramMessageListener listener : listenerList)
        {
            if (token == Token.INCREMENT_CURRENT_PLAYER_NUMBER)
            {
                listener.onPrgMsgIncrementCurrentPlayerNumber(senderName);
            }
            if (token == Token.DECREMENT_CURRENT_PLAYER_NUMBER)
            {
                listener.onPrgMsgDecrementCurrentPlayerNumber(senderName);
            }
        }


    }

    private void onPrgMsgQueryOpenGame(String[] tokens, String senderName)
    {
        if (tokens.length != 1)
        {
            Test.output(PROTOCOL_ERROR);
            return;
        }
        if (!bVerifySender(tokens[0], senderName))
        {
            return;
        }
        String userName = tokens[0];

        for (IProgramMessageListener listener : listenerList)
        {
            listener.onPrgMsgQueryOpenGame(userName);
        }

    }
    
    /**
     * Get the information about a players race in a game.
     * Use this information only if the actual user is in the same game.
     * 
     * @param tokens
     * @param senderName
     */
    private void onPrgMsgSetRaceInGame( String[] tokens, String senderName ) {
    	for (int i=0; i<tokens.length; i++){
    		Test.output("Element " + Integer.toString(i) + ": "+ tokens[i]);
    	}
    	if (!bVerifySender(tokens[1], senderName))
        {
            return;
        }
    	if( tokens.length == 3 ) {
    		Test.output("daten angekommen");
    		String hostUserName = tokens[0];
    		String race = tokens[2];
    		for (IProgramMessageListener listener : listenerList)
            {
                listener.onPrgMsgSetRaceInGame(hostUserName, senderName, race);
            }
    	}
    	return;
    }
    
    private void onPrgMsgQueryRunningGame(String[] tokens, String senderName)
    {
        if (tokens.length != 1)
        {
            Test.output(PROTOCOL_ERROR);
            return;
        }
        if (!bVerifySender(tokens[0], senderName))
        {
            return;
        }
        String userName = tokens[0];

        for (IProgramMessageListener listener : listenerList)
        {
            listener.onPrgMsgQueryRunningGame(userName);
        }

    }

    private void onPrgMsgCloseGame(String[] elements, String senderName)
    {
        if (elements.length != 2)
        {
            Test.output(PROTOCOL_ERROR);
            return;
        }
        if (!bVerifySender(elements[1], senderName))
        {
            return;
        }
        String ip;
        String hostUserName;
        ip = elements[0];
        hostUserName = elements[1];
        
        for (IProgramMessageListener listener : listenerList)
        {
            listener.onPrgMsgCloseGame(ip, hostUserName);
        }
    }
    
    private void onPrgMsgGameRunning(Token token, String[] elements, String senderName)
    {
    	//only check the minimum length,
    	//new elements may be added in future versions
    	if (elements.length < 7)
    	{
    		Test.output(PROTOCOL_ERROR);
    		return;
    	}

    	if (!bVerifySender(elements[1], elements[2], senderName))
    	{
    		return;
    	}
    	

    	OpenGame game = createGameFromMessage(token, elements);
    	if (game != null)
    	{
    		for (IProgramMessageListener listener : listenerList)
    		{
    			listener.onPrgMsgGameRunning(game);
    		}
    	}
    }

    private void onPrgMsgNewPlayerNumber(Token token, String[] elements, String senderName)
    {
        if (elements.length != 3)
        {
            Test.output(PROTOCOL_ERROR);
            return;
        }

        if (!bVerifySender(elements[1], senderName))
        {
            return;
        }

        String ip;
        String hostUserName;
        int playerNumber = 0;
        try
        {
            ip = elements[0];
            hostUserName = elements[1];
            playerNumber = Integer.parseInt(elements[2]);
            for (IProgramMessageListener listener : listenerList)
            {
                if (token == Token.NEW_CURRENT_PLAYER_NUMBER)
                {
                    listener.onPrgMsgNewCurrentPlayerNumber(ip, hostUserName, playerNumber);
                }
                if (token == Token.NEW_MAX_PLAYER_NUMBER)
                {
                    listener.onPrgMsgNewMaxPlayerNumber(ip, hostUserName, playerNumber);
                }
            }
        }
        catch (NumberFormatException e)
        {
        }
    }

    private void onPrgMsgNewGame(Token token, String[] elements, String senderName)
    {
        //only check the minimum length,
        //new elements may be added in future versions
        if (elements.length < 7)
        {
            Test.output(PROTOCOL_ERROR);
            return;
        }

        if (!bVerifySender(elements[1], elements[2], senderName))
        {
            return;
        }

        OpenGame game = createGameFromMessage(token, elements);
        if (game != null)
        {
            for (IProgramMessageListener listener : listenerList)
            {
                listener.onPrgMsgNewGame(game);
            }
        }
    }

    private int getProtocolVersion(String[] elements) {
        String protocolVersion = elements[0].toString();
        if (protocolVersion.length() >= 7) {
            protocolVersion = "0";
        }
        int intProtocolVersion = Integer.parseInt(protocolVersion);
        return intProtocolVersion;
    }

    private OpenGame createGameFromMessage(Token token, String[] elements)
    {
        OpenGame game = null;
        try
        {
            
            /**
             * Check which s3-protocol-version is used for this message
             * 
             * If index 2 is an integer, than it is a message with s3-protocol-version.
             * If index 2 is not an integer, than it is a old-style-message from pre-2016-versions.
             */
            
            // define each possible value
            int maxPlayerNumber = 0;
            int currentPlayerNumber = 0;
            boolean amazons = false;
            boolean trojans = false;
            boolean coop = false;
            int misc = -1;
            String saveGameFileName = "";
            long timestamp = 0;
            long creationTimestamp = 0;
            String goodsInStockString = "";
            int goods = -1;
            String messagetype = "";
            boolean isLeagueGame = false;
            boolean wimo = false;
            boolean futureProtocol = false;
            int tournamentid = 0;
            String tournamentname = "";
            int round = 0;
            int groupnumber = 0;
            int matchnumber = 0;
            int teamcount = 0;
            long gameModes = 0;
            List<OpenGame.S3CEGameOption> c3GameOptions = new ArrayList<>();

            // read values which must be available in every message
            int protocolVersion = this.getProtocolVersion(elements);
            String ip;
            String hostUserName;
            String name = "";
            String map = "";
            String playerlist = "";
            
            // read values
            /*for (int i=0; i<elements.length; i++){
                 Test.output("Element " + Integer.toString(i) + ": "+ elements[i]);
            }*/
            switch (protocolVersion) {
                case 10:
                case 9:
                    // aLobby 3.6.4.6 beta and newer
                    ip = elements[1];
                    hostUserName = elements[2];
                    name = elements[3];
                    map = elements[4];
                    // get values for version 1-protocol
                    maxPlayerNumber = Integer.parseInt(elements[5]);
                    currentPlayerNumber = Integer.parseInt(elements[6]);
                    amazons = Boolean.valueOf(elements[7]);
                    goods = Integer.parseInt(elements[8]);
                    wimo = Boolean.valueOf(elements[9]);
                    messagetype = elements[10];
                    isLeagueGame = decodeBoolean(elements[11]);
                    misc = Integer.valueOf(elements[12]);
                    playerlist = elements[13];
                    tournamentid = Integer.valueOf(elements[14]);
                    tournamentname = elements[15];
                    round = Integer.parseInt(elements[16]);
                    groupnumber = Integer.parseInt(elements[17]);
                    matchnumber = Integer.parseInt(elements[18]);
                    gameModes = Long.parseLong(elements[19]);

                    // if it is a normal-game
                    if( messagetype.equals(MESSAGE_TYPE_NORMAL) || messagetype.equals(MESSAGE_TYPE_LEAGUEGAME) ) {
                        // get the creation time
                        creationTimestamp = Long.valueOf(elements[21]);
                        trojans = Boolean.valueOf(elements[22]);
                        teamcount = Integer.valueOf(elements[23]);
                        coop = Boolean.valueOf(elements[24]);
                    }
                    // if it is a save-game
                    else if( messagetype.equals(MESSAGE_TYPE_SAVEGAME) ) {
                        saveGameFileName = elements[21];
                        //add default file extension ".mps"
                        saveGameFileName = addSaveGameFileExtension(saveGameFileName);
                        timestamp = Long.valueOf(elements[22]);
                        trojans = Boolean.valueOf(elements[23]);
                        teamcount = Integer.valueOf(elements[24]);
                        coop = Boolean.valueOf(elements[25]);
                    }
                    else if(messagetype.endsWith(MESSAGE_TYPE_RUNNINGGAME)) {
                        timestamp = Long.valueOf(elements[21]);
                        trojans = Boolean.valueOf(elements[22]);
                        teamcount = Integer.valueOf(elements[23]);
                        coop = Boolean.valueOf(elements[24]);
                    }
                    break;

	            case 8:
		    		// aLobby 3.3.5 and newer
		            ip = elements[1];
		            hostUserName = elements[2];
		            name = elements[3];
		            map = elements[4];
		            // get values for version 1-protocol
		            maxPlayerNumber = Integer.parseInt(elements[5]);
		            currentPlayerNumber = Integer.parseInt(elements[6]);
		            amazons = Boolean.valueOf(elements[7]);
		            goods = Integer.parseInt(elements[8]);
		            wimo = Boolean.valueOf(elements[9]);
		            messagetype = elements[10];
		            isLeagueGame = decodeBoolean(elements[11]);
		            misc = Integer.valueOf(elements[12]);
		            playerlist = elements[13];
		            tournamentid = Integer.valueOf(elements[14]);
		            tournamentname = elements[15];
		            round = Integer.parseInt(elements[16]);
		            groupnumber = Integer.parseInt(elements[17]);
		            matchnumber = Integer.parseInt(elements[18]);
		                
		            // if it is a normal-game
		            if( messagetype.equals(MESSAGE_TYPE_NORMAL) || messagetype.equals(MESSAGE_TYPE_LEAGUEGAME) ) {
		            	// get the creation time
		            	creationTimestamp = Long.valueOf(elements[20]);
		            	trojans = Boolean.valueOf(elements[21]);
		            	teamcount = Integer.valueOf(elements[22]);
		            	coop = Boolean.valueOf(elements[23]);
		            }
		            // if it is a save-game
		            else if( messagetype.equals(MESSAGE_TYPE_SAVEGAME) ) {
		                saveGameFileName = elements[20];
		                //add default file extension ".mps"
		                saveGameFileName = addSaveGameFileExtension(saveGameFileName);
		                timestamp = Long.valueOf(elements[21]);
		                trojans = Boolean.valueOf(elements[22]);
		                teamcount = Integer.valueOf(elements[23]);
		                coop = Boolean.valueOf(elements[24]);
		            }
		            else if(messagetype.endsWith(MESSAGE_TYPE_RUNNINGGAME)) {
		                timestamp = Long.valueOf(elements[20]);
		                trojans = Boolean.valueOf(elements[21]);
		                teamcount = Integer.valueOf(elements[22]);
		                coop = Boolean.valueOf(elements[23]);
		            }
		    		break;
	            case 7:
		    		// aLobby 3.3.4 and newer
		            ip = elements[1];
		            hostUserName = elements[2];
		            name = elements[3];
		            map = elements[4];
		            // get values for version 1-protocol
		            maxPlayerNumber = Integer.parseInt(elements[5]);
		            currentPlayerNumber = Integer.parseInt(elements[6]);
		            amazons = Boolean.valueOf(elements[7]);
		            goodsInStockString = getGoodsInStock(elements[8]);
		            wimo = Boolean.valueOf(elements[9]);
		            messagetype = elements[10];
		            isLeagueGame = decodeBoolean(elements[11]);
		            misc = Integer.valueOf(elements[12]);
		            playerlist = elements[13];
		            tournamentid = Integer.valueOf(elements[14]);
		            tournamentname = elements[15];
		            round = Integer.parseInt(elements[16]);
		            groupnumber = Integer.parseInt(elements[17]);
		            matchnumber = Integer.parseInt(elements[18]);
		                
		            // if it is a normal-game
		            if( messagetype.equals(MESSAGE_TYPE_NORMAL) || messagetype.equals(MESSAGE_TYPE_LEAGUEGAME) ) {
		            	// get the creation time
		            	creationTimestamp = Long.valueOf(elements[20]);
		            	trojans = Boolean.valueOf(elements[21]);
		            	teamcount = Integer.valueOf(elements[22]);
		            	coop = Boolean.valueOf(elements[23]);
		            }
		            // if it is a save-game
		            else if( messagetype.equals(MESSAGE_TYPE_SAVEGAME) ) {
		                saveGameFileName = elements[20];
		                //add default file extension ".mps"
		                saveGameFileName = addSaveGameFileExtension(saveGameFileName);
		                timestamp = Long.valueOf(elements[21]);
		                trojans = Boolean.valueOf(elements[22]);
		                teamcount = Integer.valueOf(elements[23]);
		                coop = Boolean.valueOf(elements[24]);
		            }
		            else if(messagetype.endsWith(MESSAGE_TYPE_RUNNINGGAME)) {
		                timestamp = Long.valueOf(elements[20]);
		                trojans = Boolean.valueOf(elements[21]);
		                teamcount = Integer.valueOf(elements[22]);
		                coop = Boolean.valueOf(elements[23]);
		            }
		    		break;
	            case 6:
		    		// aLobby 3.2.1 and newer
		            ip = elements[1];
		            hostUserName = elements[2];
		            name = elements[3];
		            map = elements[4];
		            // get values for version 1-protocol
		            maxPlayerNumber = Integer.parseInt(elements[5]);
		            currentPlayerNumber = Integer.parseInt(elements[6]);
		            amazons = Boolean.valueOf(elements[7]);
		            goodsInStockString = getGoodsInStock(elements[8]);
		            wimo = Boolean.valueOf(elements[9]);
		            messagetype = elements[10];
		            isLeagueGame = decodeBoolean(elements[11]);
		            misc = Integer.valueOf(elements[12]);
		            playerlist = elements[13];
		            tournamentid = Integer.valueOf(elements[14]);
		            tournamentname = elements[15];
		            round = Integer.parseInt(elements[16]);
		            groupnumber = Integer.parseInt(elements[17]);
		            matchnumber = Integer.parseInt(elements[18]);
		                
		            // if it is a normal-game
		            if( messagetype.equals(MESSAGE_TYPE_NORMAL) || messagetype.equals(MESSAGE_TYPE_LEAGUEGAME) ) {
		            	// get the creation time
		            	creationTimestamp = Long.valueOf(elements[20]);
		            	trojans = Boolean.valueOf(elements[21]);
		            	teamcount = Integer.valueOf(elements[22]);
		            }
		            // if it is a save-game
		            else if( messagetype.equals(MESSAGE_TYPE_SAVEGAME) ) {
		                saveGameFileName = elements[20];
		                //add default file extension ".mps"
		                saveGameFileName = addSaveGameFileExtension(saveGameFileName);
		                timestamp = Long.valueOf(elements[21]);
		                trojans = Boolean.valueOf(elements[22]);
		                teamcount = Integer.valueOf(elements[23]);
		            }
		            else if(messagetype.endsWith(MESSAGE_TYPE_RUNNINGGAME)) {
		                timestamp = Long.valueOf(elements[20]);
		                trojans = Boolean.valueOf(elements[21]);
		                teamcount = Integer.valueOf(elements[22]);
		            }
		    		break;
		        case 5:
		    		// aLobby 2.9.11 and newer
		            ip = elements[1];
		            hostUserName = elements[2];
		            name = elements[3];
		            map = elements[4];
		            // get values for version 1-protocol
		            maxPlayerNumber = Integer.parseInt(elements[5]);
		            currentPlayerNumber = Integer.parseInt(elements[6]);
		            amazons = Boolean.valueOf(elements[7]);
		            goodsInStockString = getGoodsInStock(elements[8]);
		            wimo = Boolean.valueOf(elements[9]);
		            messagetype = elements[10];
		            isLeagueGame = decodeBoolean(elements[11]);
		            misc = Integer.valueOf(elements[12]);
		            playerlist = elements[13];
		            tournamentid = Integer.valueOf(elements[14]);
		            tournamentname = elements[15];
		            round = Integer.parseInt(elements[16]);
		            groupnumber = Integer.parseInt(elements[17]);
		            matchnumber = Integer.parseInt(elements[18]);
		                
		            // if it is a normal-game
		            if( messagetype.equals(MESSAGE_TYPE_NORMAL) || messagetype.equals(MESSAGE_TYPE_LEAGUEGAME) ) {
		            	// get the creation time
		            	creationTimestamp = Long.valueOf(elements[20]);
		            }
		            // if it is a save-game
		            else if( messagetype.equals(MESSAGE_TYPE_SAVEGAME) ) {
		                saveGameFileName = elements[20];
		                //add default file extension ".mps"
		                saveGameFileName = addSaveGameFileExtension(saveGameFileName);
		                timestamp = Long.valueOf(elements[21]);
		            }
		            else if(messagetype.endsWith(MESSAGE_TYPE_RUNNINGGAME)) {
		                timestamp = Long.valueOf(elements[20]);                 	
		            }
		    		break;
	            case 4:
	        		// aLobby 2.9.5 and newer
	                ip = elements[1];
	                hostUserName = elements[2];
	                name = elements[3];
	                map = elements[4];
	                // get values for version 1-protocol
	                maxPlayerNumber = Integer.parseInt(elements[5]);
	                currentPlayerNumber = Integer.parseInt(elements[6]);
	                amazons = Boolean.valueOf(elements[7]);
	                goodsInStockString = getGoodsInStock(elements[8]);
	                wimo = Boolean.valueOf(elements[9]);
	                messagetype = elements[10];
	                isLeagueGame = decodeBoolean(elements[11]);
	                misc = Integer.valueOf(elements[12]);
		                
	                // if it is a normal-game
	                if( messagetype.equals(MESSAGE_TYPE_NORMAL) || messagetype.equals(MESSAGE_TYPE_LEAGUEGAME) ) {
	                	// get the creation time
	                	creationTimestamp = Long.valueOf(elements[13]);
	                }
	                // if it is a save-game
	                else if( messagetype.equals(MESSAGE_TYPE_SAVEGAME) ) {
	                    saveGameFileName = elements[13];
	                    //add default file extension ".mps"
	                    saveGameFileName = addSaveGameFileExtension(saveGameFileName);
	                    timestamp = Long.valueOf(elements[14]);
	                }
	                else if(messagetype.endsWith(MESSAGE_TYPE_RUNNINGGAME)) {
	                    timestamp = Long.valueOf(elements[13]);                 	
	                }
	        		break;
            	case 3:
            		// aLobby 2.9.5 and newer
                    ip = elements[1];
                    hostUserName = elements[2];
                    name = elements[3];
                    map = elements[4];
                    // get values for version 1-protocol
                    maxPlayerNumber = Integer.parseInt(elements[5]);
                    currentPlayerNumber = Integer.parseInt(elements[6]);
                    amazons = Boolean.valueOf(elements[7]);
                    goodsInStockString = getGoodsInStock(elements[8]);
                    wimo = Boolean.valueOf(elements[9]);
                    messagetype = elements[10];
                    isLeagueGame = decodeBoolean(elements[11]);
                    misc = Integer.valueOf(elements[12]);
                        
                    // if it is a normal-game
                    if( messagetype.equals(MESSAGE_TYPE_NORMAL) ) {
                    }
                    // if it is a save-game
                    else if( messagetype.equals(MESSAGE_TYPE_SAVEGAME) ) {
                        saveGameFileName = elements[13];
                        //add default file extension ".mps"
                        saveGameFileName = addSaveGameFileExtension(saveGameFileName);
                        timestamp = Long.valueOf(elements[14]);
                    }
                    else if(messagetype.endsWith(MESSAGE_TYPE_RUNNINGGAME)) {
                        timestamp = Long.valueOf(elements[13]);                 	
                    }
            		break;
                case 2:
                    // aLobby 2.9.2 and newer (notice the missing break statement: it's the same as protocol version 1 ;-) )
                case 1: {
                    // aLobby 2.9.0 and newer
                    ip = elements[1];
                    hostUserName = elements[2];
                    name = elements[3];
                    map = elements[4];
                    // get values for version 1-protocol
                    maxPlayerNumber = Integer.parseInt(elements[5]);
                    currentPlayerNumber = Integer.parseInt(elements[6]);
                    amazons = Boolean.valueOf(elements[7]);
                    goodsInStockString = getGoodsInStock(elements[8]);
                    wimo = Boolean.valueOf(elements[9]);
                    messagetype = elements[10];
                    isLeagueGame = decodeBoolean(elements[11]);
                    misc = Integer.valueOf(elements[12]);
    
                    // if it is a normal-game
                    if( messagetype.equals(MESSAGE_TYPE_NORMAL) ) {
                    }
                    // if it is a save-game
                    else if( messagetype.equals(MESSAGE_TYPE_SAVEGAME) ) {
                        saveGameFileName = elements[13];
                        //add default file extension ".mps"
                        saveGameFileName = addSaveGameFileExtension(saveGameFileName);
                        timestamp = Long.valueOf(elements[14]);
                    }
                    break;
                }
                case 0: {
                    // aLobby 2.8.2 and older
                    // The "protocolVersion" is here the host's IP address, e.g. 0.0.0.0
                    ip = elements[0];
                    hostUserName = elements[1];
                    name = elements[2];
                    map = elements[3];
                    // get values from old-style-message
                    maxPlayerNumber = Integer.parseInt(elements[4]);
                    currentPlayerNumber = Integer.parseInt(elements[5]);
                    amazons = Boolean.valueOf(elements[6]);
        
                    //present since V1.2.0
                    //for (int i=0; i<elements.length; i++){
                    //    Test.output("Element " + Integer.toString(i) + ": "+ elements[i]);
                    //}
                    if (elements.length >= 10)
                    {
                        try
                        {
                            misc = Integer.valueOf(elements[9]);
                        }
                        catch (NumberFormatException e)
                        {
                            //ignore and keep default settings
                            Test.outputException(e);
                        }
                    }
                    if (elements.length >= 12)
                    {
                        try
                        {
                            saveGameFileName = elements[10];
                            //add default file extension ".mps"
                            saveGameFileName = addSaveGameFileExtension(saveGameFileName);
                            timestamp = Long.valueOf(elements[11]);
                        }
                        catch (NumberFormatException e)
                        {
                            //ignore and keep default settings
                            Test.outputException(e);
                        }
                    }
        
                    //isLeagueGame present since V1.1.0
                    //encoding changed in V1.2.0, no problem as it was not used so far
                    if (elements.length >= 9)
                    {
                        isLeagueGame = decodeBoolean(elements[8]);
                    }
        
                    //goods in stock was optional in very early version
                    if (elements.length >= 8)
                    {
                        goodsInStockString = getGoodsInStock(elements[7]);
                    }
                    break;
                }
                default :{
                    // A future aLobby protocol
                    ip = elements[1];
                    hostUserName = elements[2];
                    name = I18n.getString("ProgramMessageProtocol.VERSION_CONFLICT");
                    map = ALobbyConstants.LABEL_UNKNOWN_MAP;
                    futureProtocol = true;
                    break;
                }
            }
            if( ( token.equals(Token.GAME_RUNNING) || token.equals(Token.NEW_GAME_ENC) ) && !futureProtocol)
            {
                name = Base64Coder.decodeString(name);
                map = Base64Coder.decodeString(map);
            }
            
            // get goods-value from older irc-protocol-version than version 8
            if( goods < 0 && !"".equals(goodsInStockString) ) {
            	goods = getGoodsInStockAsInt(goodsInStockString);
            }
            
            // if this is a s4-game and goods is set, than add 1 to match correct GoodsInStock-index
            if( goods >= 0 && MiscType.valueOf(misc) == MiscType.S4 ) {
            	goods++;
            }

            // parse game modes from bitmask
            c3GameOptions = OpenGame.S3CEGameOption.parse(gameModes);

            /**
             * If the message doesn't consists of correct value for
             * misc, than show a hint, that the user should update his
             * aLobby.
             */
            /*
            if( misc == 0 ) {
                MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
                // -> add message
                messagewindow.addMessage(
                    "Ein offenes Spiel kann dir nicht angezeigt werden, da Du nicht die aktuellste aLobby-Version verwendest.", 
                    UIManager.getIcon("OptionPane.errorIcon")
                );
                // -> add ok-button
                messagewindow.addButton(
                    UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
                        new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                // simply close the window with warn-message
                                messagewindow.dispose();
                            }
                        }
                );
                // show it
                messagewindow.setVisible(true);
                return null;
            }*/
            Test.output("get misc: " + misc);

            game = new OpenGame(ip,
                                hostUserName,
                                name,
                                map,
                                amazons,
                                goods,
                                maxPlayerNumber,
                                currentPlayerNumber,
                                isLeagueGame,
                                wimo,
                                tournamentid,
                                tournamentname,
                                round, 
                                groupnumber, 
                                matchnumber,
                                c3GameOptions);
            game.addPlayer(playerlist, false);
            game.setMisc(MiscType.valueOf(misc));
            game.setTrojans(trojans);
            game.setCoop(coop);
            game.setTeamCount(teamcount);
            if( saveGameFileName.length() > 0 && timestamp > 0 ) {
            	game.setSaveGame(saveGameFileName, timestamp);
            	game.setRunningGameStartTimestamp(timestamp);
            }
            if( messagetype.endsWith(MESSAGE_TYPE_RUNNINGGAME) && timestamp > 0 ) {
            	game.setRunningGameStartTimestamp(timestamp);
            }
            if( ( messagetype.equals(MESSAGE_TYPE_NORMAL) || messagetype.equals(MESSAGE_TYPE_LEAGUEGAME) ) && creationTimestamp > 0 ) {
            	game.setCreationTime(creationTimestamp);
            }
            game.setRequireFutureVersion(futureProtocol);
        }
        catch (Exception e)
        {
            // die Nachricht konnte nicht korrekt verarbeitet werden
            Test.outputException(e);
        }
        return game;
    }

    private static String addSaveGameFileExtension(String saveGameFileName) 
    {
        //for savegames the default file extension is not transmitted
        if (!saveGameFileName.isEmpty())
        {
            if (!saveGameFileName.contains("."))
            {
                saveGameFileName = saveGameFileName + ".mps"; 
            }
        }
        return saveGameFileName;
    }

    private String getGoodsInStock(String inputValue)
    {
        String result;
        try
        {
            GoodsInStock goods = GoodsInStock.valueOf(inputValue);
            result = goods.getLanguageString();
        }
        catch (Exception e)
        {
            result = ""; //$NON-NLS-1$
        }
        return result;
    }
    
    private int getGoodsInStockAsInt(String elements)
    {
    	S3Language s3Language = new S3Language(); 
    	GoodsInStock goods = s3Language.getGoodsInStockByUnknownLanguage(elements);
        return goods.getIndex();
    }

    /**
     * prüft ob pretendedSenderName == actualSenderName.
     * @param pretendedSenderName
     * @param actualSenderName
     * @return true wenn sie gleich sind oder mindestens einer null
     */
    private static boolean bVerifySender(String pretendedSenderName, String actualSenderName)
    {
        //wenn mindestens einer der Parameter null ist, kann man zwar nicht
        //verifzieren, ob der Sender der echte ist, man hat aber auch keine Möglichkeit
        //das Gegenteil zu beweisen, da der Wert ja nicht verfügbar ist.
        //Daher wird auch in diesem Fall true zurückgegeben.
        //Nötig wegen Abwärtskomtibilität
        boolean result = true;
        if (pretendedSenderName != null && actualSenderName != null)
        {
            result = pretendedSenderName.equals(actualSenderName);
            if (!result)
            {
                Test.output("ERROR, bVerifySender failed."); //$NON-NLS-1$
            }
        }
        return result;
    }

    /**
     * prüft ob pretendedSenderName == actualSenderName.
     * @param pretendedSenderName
     * @param actualSenderName
     * @return true wenn sie gleich sind oder mindestens einer null
     */
    private static boolean bVerifySender(String pretendedSenderName1, String pretendedSenderName2, String actualSenderName)
    {
        //wenn mindestens einer der Parameter null ist, kann man zwar nicht
        //verifzieren, ob der Sender der echte ist, man hat aber auch keine Möglichkeit
        //das Gegenteil zu beweisen, da der Wert ja nicht verfügbar ist.
        //Daher wird auch in diesem Fall true zurückgegeben.
        //Nötig wegen Abwärtskomtibilität
        boolean result = true;
        if (pretendedSenderName1 != null && pretendedSenderName2 != null && actualSenderName != null) {
            if (pretendedSenderName1.equals(actualSenderName) || pretendedSenderName2.equals(actualSenderName)) {
                result = true;
            } else {
                result = false;
                Test.output("ERROR, bVerifySender failed."); //$NON-NLS-1$
            }
        }
        return result;
    }

    private static String format(Object... entries)
    {
        StringBuilder sb = new StringBuilder();
        Object obj;
        String value;
        for (int i = 0; i < entries.length; i++)
        {
            obj = entries[i];
            if (obj == null)
            {
                value = "";
            }
            else if (obj instanceof Token)
            {
                value = ((Token)obj).getValue();
            }
            else
            {
                value = entries[i].toString();
            }
            sb.append(value);

            if (i < entries.length - 1)
            {
                sb.append(SEPARATOR);
            }
        }
        return sb.toString();
    }
    
    private static String encodeBoolean(boolean leagueGame) 
    {
        return leagueGame ? "1" : "";
    }
    
    private static boolean decodeBoolean(String value)
    {
        //encoding was changed from true/false to "1"/"" in version 1.2.0
        return "1".equals(value) || Boolean.valueOf(value);
    }

    /**
     * Query the channel to request the vpn-state of each user.
     * 
     * @param user
     * @return String with request for vpn-state, e.g.: 30;RequestedUser
     */
	public String createMsgQueryVPNState(String user) {
		return format(Token.QUERY_USER_VPN_STATE, user);
	}

	/**
	 * Create message to inform explicit user to inform him
	 * about the starting s4-game.
	 * 
	 * @param openGame
	 * @param user 
	 * @param ipUserList 
	 * @return String with resulting message, e.g.: 31;Spielername;1.2.3.4;Hostname
	 */
	public String createMsgStartS4Game(OpenGame game, String user, String ipUserList) {
		return format(Token.S4_GAME_START, game.getHostUserName(), game.getIP(), user, ipUserList);
	}

	/**
	 * Create message to inform channel about a joining beta-user.
	 * 
	 * @param user	sender of this message
	 * @return
	 */
	public String createMsgBetaUsersJoined(String user) {
		return format(Token.BETA_USER, user);
	}

	/**
	 * Query the channel for beta-users.
	 * 
	 * @param user
	 * @return
	 */
	public String createMsgQueryBetaUsers(String user) {
		return format(Token.QUERY_BETA_USER, user);
	}

}
