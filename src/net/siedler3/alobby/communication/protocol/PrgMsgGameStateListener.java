package net.siedler3.alobby.communication.protocol;

import net.siedler3.alobby.controlcenter.OpenGame;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.controlcenter.state.UserInGame;

public class PrgMsgGameStateListener extends ProgramMessageAdapter
{
    private SettlersLobby settlersLobby;

    public PrgMsgGameStateListener(SettlersLobby settlerLobby)
    {
        super();
        this.settlersLobby = settlerLobby;
    }

    @Override
    public void onPrgMsgUserWantsToJoin(String userName, String map)
    {
        settlersLobby.getGameState().onWantsToJoin(new UserInGame(userName), map);
    }

    @Override
    public void onPrgMsgUserJoined(String userName, String map, String ip)
    {
        settlersLobby.getGameState().onJoin(new UserInGame(userName), map, ip);
    }

    @Override
    public void onPrgMsgUserLeaving(String userName, String map)
    {
        settlersLobby.getGameState().onPart(new UserInGame(userName), map);
    }

    @Override
    public void onPrgMsgBroadcastMapUserList(String hostUserName, String map, String userList)
    {
        settlersLobby.getGameState().onJoin(userList, map, hostUserName);
    }

    @Override
    public void onPrgMsgBroadcastUserLeft(String hostUserName, String map, String userName)
    {
        settlersLobby.getGameState().onPart(new UserInGame(userName), map, hostUserName);
    }

	@Override
	public void onPrgMsgGameRunning(OpenGame game) {
	}

	@Override
	public void onPrgMsgCloseRunningGame(String ip, String hostUserName) {
	}

	@Override
	public void onPrgMsgQueryRunningGame(String userName) {
	}

	@Override
	public void onPrgMsgSetRaceInGame(String hostUserName, String senderName, String race) {
	}

	@Override
	public void onPrgMsgCloseStream(String user) {
	}

	@Override
	public void onPrgMsgNewStream(String user, String link, String onlinesince) {
	}

	@Override
	public void onPrgMsgVPNStateMessage(String senderName, boolean userVpnState) {
	}

	@Override
	public void onPrgMsgQueryVPNState(String userName) {
	}

	@Override
	public void onPrgMsgS4StartGame(String senderName, String ip, String playername, String ipUserlist) {
	    Test.output("onPrgMsgS4StartGame STARTING S4 game from " + senderName);
		settlersLobby.startS4Game(senderName, ip, playername, ipUserlist); 
	}

	@Override
	public void onPrgMsgDirectMessage(String receiver, String message) {
	}

	@Override
	public void onPrgMsgBetaUserJoined(String user) {
	}

	@Override
	public void onPrgMsgQueryBetaUsers(String userName) {
	}
	@Override
	public void onPrgMsgCommunityNews(String prio, String md5, String date, String title, String text, String link,	String type) {
	}

	@Override
	public void onPrgMsgCommunityNewsRemove(String type) {
	}
}
