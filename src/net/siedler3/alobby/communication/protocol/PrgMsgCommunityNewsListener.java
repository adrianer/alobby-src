package net.siedler3.alobby.communication.protocol;

import net.siedler3.alobby.controlcenter.OpenGame;
import net.siedler3.alobby.controlcenter.SettlersLobby;

public class PrgMsgCommunityNewsListener extends ProgramMessageAdapter
{
    private SettlersLobby settlersLobby;

    public PrgMsgCommunityNewsListener(SettlersLobby settlerLobby)
    {
        super();
        this.settlersLobby = settlerLobby;
    }

    @Override
    public void onPrgMsgNewGame(OpenGame game)
    {
    }

    @Override
    public void onPrgMsgNewCurrentPlayerNumber(String ip, String hostUserName, int currentPlayerNumber)
    {
    }

    @Override
    public void onPrgMsgNewMaxPlayerNumber(String ip, String hostUserName, int maxPlayerNumber)
    {

    }

    @Override
    public void onPrgMsgCloseGame(String ip, String hostUserName)
    {
    }

    @Override
    public void onPrgMsgQueryOpenGame(String userName)
    {
    }

    @Override
    public void onPrgMsgUserJoined(String userName, String map, String ip)
    {
    }

    @Override
    public void onPrgMsgUserLeaving(String userName, String map)
    {
    }

    @Override
    public void onPrgMsgIncrementCurrentPlayerNumber(String userName)
    {
     }

    @Override
    public void onPrgMsgDecrementCurrentPlayerNumber(String userName)
    {

    }

    @Override
    public void onPrgMsgNewGameName(String ip, String hostUserName, String name)
    {

    }

    @Override
    public void onPrgMsgBroadcastMapUserList(String hostUserName, String map, String userList)
    {
    }

    @Override
    public void onPrgMsgBroadcastUserLeft(String hostUserName, String map, String userName)
    {
    }

	@Override
	public void onPrgMsgGameRunning(OpenGame game) {
	}

	@Override
	public void onPrgMsgCloseRunningGame(String ip, String hostUserName) {
	}

	@Override
	public void onPrgMsgQueryRunningGame(String userName) {
	}
	
	@Override
	public void onPrgMsgSetRaceInGame(String hostUserName, String senderName, String race) {
	}

	@Override
	public void onPrgMsgCloseStream(String user) {
	}

	@Override
	public void onPrgMsgNewStream(String user, String link, String onlinesince) {
	}

	@Override
	public void onPrgMsgVPNStateMessage(String senderName, boolean userVpnState) {
	}

	@Override
	public void onPrgMsgQueryVPNState(String userName) {
	}

	@Override
	public void onPrgMsgS4StartGame(String senderName, String ip, String hostUserName, String ipUserList) {
	}

	@Override
	public void onPrgMsgDirectMessage(String receiver, String message) {
	
	}

	@Override
	public void onPrgMsgBetaUserJoined(String user) {
	}

	@Override
	public void onPrgMsgQueryBetaUsers(String userName) {
	}

	@Override
	public void onPrgMsgCommunityNews(String prio, String md5, String date, String title, String text, String link, String type) {
		settlersLobby.getGUI().addCommunityNews(prio, md5, date, title, text, link, type, false);
	}

	@Override
	public void onPrgMsgCommunityNewsRemove(String type) {
		settlersLobby.getGUI().removeCommunityNewsByType(type);
		settlersLobby.getGUI().removeCommunityNewsByMd5(type);
	}

}
