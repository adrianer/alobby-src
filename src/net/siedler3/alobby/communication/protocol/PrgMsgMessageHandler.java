package net.siedler3.alobby.communication.protocol;

import net.siedler3.alobby.controlcenter.OpenGame;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.gui.additional.S3User;

public class PrgMsgMessageHandler extends ProgramMessageAdapter
{
    private SettlersLobby settlersLobby;

    public PrgMsgMessageHandler(SettlersLobby settlerLobby)
    {
        super();
        this.settlersLobby = settlerLobby;
    }
    
    @Override
    public void onPrgMsgVPNStateMessage(String userName, boolean vpnState)
    {
    }
    
    @Override
    public void onPrgMsgAwayMessage(String userName, String awayMessage)
    {
    }

    @Override
    public void onPrgMsgAwayBack(String userName)
    {
    }

	@Override
	public void onPrgMsgGameRunning(OpenGame game) {
	}

	@Override
	public void onPrgMsgCloseRunningGame(String ip, String hostUserName) {
	}

	@Override
	public void onPrgMsgQueryRunningGame(String userName) {
	}

	@Override
	public void onPrgMsgSetRaceInGame(String hostUserName, String senderName, String race) {
	}

	@Override
	public void onPrgMsgCloseStream(String user) {
	}

	@Override
	public void onPrgMsgNewStream(String user, String link, String onlinesince) {
	}

	@Override
	public void onPrgMsgQueryVPNState(String userName) {
	}

	@Override
	public void onPrgMsgS4StartGame(String senderName, String ip, String hostUserName, String ipUserList) {
	}

	/**
	 * Got a direct message from XMLRPC-request from external applications.
	 * 
	 * @param receiver
	 * @param message 
	 */
	@Override
	public void onPrgMsgDirectMessage(String receiver, String message) {
		// show the received message only if the receiver is valid
		if( settlersLobby.getNick().equalsIgnoreCase(receiver.toLowerCase()) ) {
			if( settlersLobby.messageHandler != null ) {
				settlersLobby.messageHandler.addServerMessage(message);
			}
		}
	}
	
	/**
     * Got info about channel joining beta-user.
     * Mark the user in userlist as beta-user of the actual user is one of this:
     * - Admin
     * - Mod
     * - Beta-User
     * 
     * @param user	the user who joined the channel and has a beta
     */
	@Override
	public void onPrgMsgBetaUserJoined(String user) {
		// get the S3User-object of the actual user
		S3User activeS3User = settlersLobby.getGUI().getUsersOnline().getElementByS3UserName(settlersLobby.getNick());
		// check if the actual user is admin, mod or other beta-user
		if( activeS3User.isAdmin() || activeS3User.isBetaUser() ) {
			// set beta-info to the user in the userlist who send this info
			S3User s3User = settlersLobby.getGUI().getUsersOnline().getElementByS3UserName(user);
			s3User.setBetaUser(true);
			settlersLobby.getGUI().updateListUserOnline();
		}
	}

	/**
	 * Answer a query for beta-users which is send by other beta-users.
	 */
	@Override
	public void onPrgMsgQueryBetaUsers(String userName) {
		settlersLobby.onPrgMsgQueryBetaUsers(userName);
	}

	@Override
	public void onPrgMsgCommunityNews(String prio, String md5, String date, String title, String text, String link,	String type) {
	}

	@Override
	public void onPrgMsgCommunityNewsRemove(String type) {
	}

}
