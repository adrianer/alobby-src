package net.siedler3.alobby.communication.protocol;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import net.siedler3.alobby.communication.protocol.ProgramMessageProtocol.Token;
import net.siedler3.alobby.controlcenter.OpenGame;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.controlcenter.state.UserInGameList;
import net.siedler3.alobby.controlcenter.OpenGame.MiscType;
import net.siedler3.alobby.s3gameinterface.GameStarter;
import net.siedler3.alobby.util.httpRequests;

public class PrgMsgOpenGameListener extends ProgramMessageAdapter
{
    private SettlersLobby settlersLobby;
    private Map<String, Token> lastMessageFromUser = new HashMap<String, Token>();

    public PrgMsgOpenGameListener(SettlersLobby settlerLobby)
    {
        super();
        this.settlersLobby = settlerLobby;
    }

    @Override
    public void onPrgMsgNewGame(OpenGame game)
    {
        //first check if there is still an open game of
        //the host (usually this entry should have already been deleted before)
        settlersLobby.closeGame(game.getIP(), game.getHostUserName());

        settlersLobby.getOpenGames().addOpenGame(game);
        //also add it to the GUI-List of open games
        settlersLobby.getGUI().addGame(game);
    }

    @Override
    public void onPrgMsgNewCurrentPlayerNumber(String ip, String hostUserName, int currentPlayerNumber)
    {
        OpenGame openGame = getOpenGame(ip, hostUserName);
        if (openGame != null)
        {
            openGame.setCurrentPlayer(currentPlayerNumber);
        }
    }

    @Override
    public void onPrgMsgNewMaxPlayerNumber(String ip, String hostUserName, int maxPlayerNumber)
    {
        OpenGame openGame = getOpenGame(ip, hostUserName);
        if (openGame != null)
        {
            openGame.setMaxPlayer(maxPlayerNumber);
        }
    }

    @Override
    public void onPrgMsgCloseGame(String ip, String hostUserName)
    {
    	// remove own s4-game if it is closed by host
    	OpenGame openGame = getOpenGame(ip, hostUserName);
    	if( openGame != null && openGame.getMisc() == MiscType.S4 && settlersLobby.getOpenJoinGame() != null && settlersLobby.getOpenJoinGame().getIP().equals(ip) ) {
    		Test.output("setS4GameOptions aaaa");
    		settlersLobby.getGUI().setS4GameOptions(new UserInGameList(), false, false);
    		settlersLobby.getAwayStatus().onGameClosed();
    		settlersLobby.setAsOpenJoinGame(null);
    	}
        settlersLobby.closeGame(ip, hostUserName);
    }

    @Override
    public void onPrgMsgQueryOpenGame(String userName)
    {
        settlersLobby.onPrgMsgQueryOpenGame(userName);
    }

    @Override
    public void onPrgMsgUserJoined(String userName, String map, String ip)
    {
        lastMessageFromUser.put(userName, Token.USER_JOINED);
    }

    @Override
    public void onPrgMsgUserLeaving(String userName, String map)
    {
        lastMessageFromUser.put(userName, Token.USER_LEAVING);
    }

    @Override
    public void onPrgMsgIncrementCurrentPlayerNumber(String userName)
    {
        OpenGame currentOpenGame = settlersLobby.getOpenHostGame();
        GameStarter gameStarter = settlersLobby.getGameStarter();
        Test.output("debug2: " + currentOpenGame + " -> " + gameStarter.isHostAway());
        if (currentOpenGame != null && ( ( currentOpenGame.getMisc() != MiscType.S4 && gameStarter.isHostAway() ) || ( currentOpenGame.getMisc() == MiscType.S4 ) ) )
        {
            //The following message sequences may occur:
            //  valid versions:
            //    1. USER_JOINED
            //    2. INCREMENT_CURRENT_PLAYER_NUMBER
            //    3. DECREMENT_CURRENT_PLAYER_NUMBER
            //    4. USER_LEAVING
            //
            //  buggy versions 1.0.5-1.0.7
            //    1. USER_JOINED
            //    2. INCREMENT_CURRENT_PLAYER_NUMBER
            //    3. INCREMENT_CURRENT_PLAYER_NUMBER
            //    4. USER_LEAVING

            //  very old versions:
            //    1. INCREMENT_CURRENT_PLAYER_NUMBER
            //    2. DECREMENT_CURRENT_PLAYER_NUMBER
            //
            // so only in the buggy version two INCREMENT_CURRENT_PLAYER_NUMBER messages can occur, this has
            // to be mapped to a DECREMENT_CURRENT_PLAYER_NUMBER message
            if (lastMessageFromUser.get(userName) == Token.INCREMENT_CURRENT_PLAYER_NUMBER)
            {
                //an increment followed by a second increment can only occur from a client
                //with version 1.05-1.07, were an additional increment was sent instead of decrement
                Test.output("map wrong increment to decrement");
                settlersLobby.newCurrentPlayerNumber(currentOpenGame.getCurrentPlayer() - 1);
                lastMessageFromUser.put(userName, Token.DECREMENT_CURRENT_PLAYER_NUMBER);
            }
            else
            {
                Test.output("away increment");
                settlersLobby.newCurrentPlayerNumber(currentOpenGame.getCurrentPlayer() + 1);
                lastMessageFromUser.put(userName, Token.INCREMENT_CURRENT_PLAYER_NUMBER);
            }
        }
        else
        {
            lastMessageFromUser.put(userName, Token.INCREMENT_CURRENT_PLAYER_NUMBER);
        }
    }

    @Override
    public void onPrgMsgDecrementCurrentPlayerNumber(String userName)
    {
        OpenGame currentOpenGame = settlersLobby.getOpenHostGame();
        GameStarter gameStarter = settlersLobby.getGameStarter();
        if (currentOpenGame != null && gameStarter.isHostAway())
        {
            Test.output("Decrement player number as host is away");
            settlersLobby.newCurrentPlayerNumber(currentOpenGame.getCurrentPlayer() - 1);
        }
        else
        {
            Test.output("EXPERIMENTAL: Decrement player even though host is NOT away");
            settlersLobby.newCurrentPlayerNumber(currentOpenGame.getCurrentPlayer() - 1);
        }
        lastMessageFromUser.put(userName, Token.DECREMENT_CURRENT_PLAYER_NUMBER);
    }

    @Override
    public void onPrgMsgNewGameName(String ip, String hostUserName, String name)
    {
        OpenGame openGame = getOpenGame(ip, hostUserName);
        if (openGame != null) {
            openGame.setName(name);
        }
    }

    @Override
    public void onPrgMsgNewGameMode(String ip, String hostUserName, long gameMode) {
        OpenGame openGame = getOpenGame(ip, hostUserName);
        if (openGame != null) {
            openGame.setS3C3Options(OpenGame.S3CEGameOption.parse(gameMode));
        }
    }

    @Override
    public void onPrgMsgBroadcastMapUserList(String hostUserName, String map, String userList)
    {
        //this is in fact a gameState programMessage for the users trying to start a game,
        //but it is also used by every user to update the userList of the open games
        OpenGame game = getOpenGameByUser(hostUserName, map);
        if (game != null )
        {
        	// cleanup playerlist for S3CE-games before adding complete list
        	Test.output("check s3ce");
        	if( game.getMisc() == MiscType.S3CE ) {
        		game.resetPlayerList();
        	}
        	// add the player to list
            game.addPlayer(userList, false);
            // update the current player number
           	game.setCurrentPlayer();
        }

    }

    @Override
    public void onPrgMsgBroadcastUserLeft(String hostUserName, String map, String userName)
    {
        //this is in fact a gameState programMessage for the users trying to start a game,
        //but it is also used by every user to update the userList of the open games
        OpenGame game = getOpenGameByUser(hostUserName, map);
        if (game != null)
        {
            game.removePlayer(userName);
        }
        // remove the leaving user from openhostgame-object if this user is the host of a game
        if( settlersLobby.isHost() ) {
        	OpenGame openGame = settlersLobby.getOpenHostGame();
        	if( openGame instanceof OpenGame ) {
        		openGame.removePlayer(userName);
        		
                if( openGame != null ) {
        		    // prepare message to update info on webserver
        	    	// -> responsive is not relevant for us here
        	    	httpRequests httpRequests = new httpRequests();
        	    	httpRequests.setUrl(settlersLobby.config.getLobbyRequestUrl());
        	    	httpRequests.addParameter("updategame", 1);
        	    	httpRequests.addParameter("state", "open");
        	    	httpRequests.addParameter("hostname", settlersLobby.getNick());
        	    	httpRequests.addParameter("ip", settlersLobby.ipToUse());
        	    	// no token-security for this request
        	    	httpRequests.setIgnorePhpSessId(true);
        	    	// send the list of players in this game as JSON
        			List<String> users = openGame.getPlayerList();
        			JSONArray usersAsJSON = new JSONArray();
        			for( int i = 0;i<users.size();i++ ) {
        				JSONObject item = new JSONObject();
        				item.put("name", users.get(i));
        				usersAsJSON.put(item);
        			}
        			Test.output("playerlist: " + usersAsJSON.toString());
        			httpRequests.addParameter("playerlist", usersAsJSON.toString());
        			// send the player-count
        	    	httpRequests.addParameter("playercount", users.size());
        	    	try {
        	    		httpRequests.doRequest();
        	    	} catch (IOException e) {
        	    	}
                }
        	}
        }
        // remove the leaving user from game if the given user is the actual user
        // and if it is a s4 game
        if( settlersLobby.getNick().equals(userName) && game != null && game.getMisc() == MiscType.S4 ) {
        	// leave the game
    		// -> inform host about leaving
    		settlersLobby.getGameState().userHasLeftGame(true);
    		// -> remove options-window
    		settlersLobby.getGUI().setS4GameOptions(new UserInGameList(), false, false);
    		// -> send info about decrement the player-number to host
    		settlersLobby.onSendDecrementPlayerNumber();
    		// -> reset marker for open joined game
    		settlersLobby.setAsOpenJoinGame(null);
    		// -> set user as not playing anymore
    		settlersLobby.getAwayStatus().onGameClosed();
        }
    }

    private OpenGame getOpenGameByUser(String hostUserName, String map)
    {
        OpenGame game = settlersLobby.getOpenGames().getOpenGame(hostUserName);
        if (game == null || (!map.equals(game.getMap())))
        {
            //no game found from user, or map does not match, abort
            Test.output("Trying to remove  player " + hostUserName + " from the map " + map + " failed, as map-username username combination was not found!");
            return null;
        }
        return game;
    }

    private OpenGame getOpenGame(String ip, String hostUserName)
    {
        return settlersLobby.getOpenGames().getOpenGame(ip, hostUserName);
    }

	@Override
	public void onPrgMsgGameRunning(OpenGame game) {
	}

	@Override
	public void onPrgMsgCloseRunningGame(String ip, String hostUserName) {
	
	}

	@Override
	public void onPrgMsgQueryRunningGame(String userName) {
	}

	@Override
	public void onPrgMsgSetRaceInGame(String hostUserName, String senderName, String race) {
	}

	@Override
	public void onPrgMsgCloseStream(String user) {
	}

	@Override
	public void onPrgMsgNewStream(String user, String link, String onlinesince) {
	}

	@Override
	public void onPrgMsgVPNStateMessage(String senderName, boolean userVpnState) {
	}

	@Override
	public void onPrgMsgQueryVPNState(String userName) {
	}

	@Override
	public void onPrgMsgS4StartGame(String senderName, String ip, String hostUserName, String ipUserList) {
	}

	@Override
	public void onPrgMsgDirectMessage(String receiver, String message) {
	}

	@Override
	public void onPrgMsgBetaUserJoined(String user) {
	}

	@Override
	public void onPrgMsgQueryBetaUsers(String userName) {
	}

	@Override
	public void onPrgMsgCommunityNews(String prio, String md5, String date, String title, String text, String link, String type) {
	}

	@Override
	public void onPrgMsgCommunityNewsRemove(String type) {
	}
	
}
