package net.siedler3.alobby.communication;

public enum NickServCommand
{
    CONFIRM,
    DROP,
    HELP,
    INFO,
    REGISTER,
    RESEND,
    SET_PASSWORD {
        @Override
        public String getCommand()
        {
            return "SET PASSWORD";
        }
    },
    STATUS,
    RESETPASS,
    ALOBBY
    ;

    public String getCommand()
    {
        return toString();
    }

    public void transmitUsing(IrcInterface ircInterface, String options)
    {
        ircInterface.sendNickServCommand(this, options);
    }
}
