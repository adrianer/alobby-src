/*
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "stdafx.h"
#include <stdio.h> 
#include <stdexcept>

using namespace std;


// to local process
LPVOID WriteMem( unsigned long nAddress, const unsigned char * pNew, const unsigned char * pOrig, unsigned int nLen )
{
	if( pOrig != NULL ) {
		if( memcmp( (void *)nAddress, pOrig, nLen ) != 0 ) {
			throw std::exception( "Incompatible" );
		}
	}
	DWORD dummy;
	VirtualProtect( (LPVOID)nAddress, nLen, PAGE_EXECUTE_READWRITE, &dummy );
	memcpy( (LPVOID)nAddress, pNew, nLen );

	return (LPVOID)(nAddress + nLen);
}

void writeAbs( unsigned long nAddress, const void * pFunc )
{	
	DWORD dummy;
	VirtualProtect( (LPVOID)nAddress, 4, PAGE_EXECUTE_READWRITE, &dummy );
	*((unsigned long *)nAddress) = (const unsigned long)pFunc;
}

void WINAPI exp_s4_renderFrame( PVOID * pDunno )
{
	__asm
	{
		push pDunno
		mov eax, dword ptr [0x5c12a0]			
		call dword ptr [eax]		// renderFrame@guiengine
	}
	Sleep(10);
}


__declspec(dllexport) BOOL APIENTRY DllMain(HMODULE hModule, DWORD dwReason, LPVOID lpReserved)
{
	if (dwReason == DLL_PROCESS_ATTACH)
	{
		Sleep(1500);
		//MessageBox(
		//	GetDesktopWindow(),
		//	(LPCWSTR)"ok",
		//	(LPCWSTR)"OK",
		//	MB_OK | MB_ICONERROR | MB_SYSTEMMODAL
		//	);

		// CPU usage fix, sleep 10ms after each rendered frame in menu
		// Seems to only work in the multiplayer-lobby and the "Game Settings" menu
		static void * pRenderFrame = exp_s4_renderFrame;
		writeAbs( 0x4ce35c, &pRenderFrame );
		// CPU usage fix, sleep 10ms (0x0A) after each rendered frame ingame
		static unsigned char pSleepCode[] = { 0x90, 0x6A, 0x0A, 0xFF, 0x15, 0x7C, 0x13, 0x5C, 0x00, 0xC3 };
		WriteMem( 0x4c6146, pSleepCode, NULL, sizeof( pSleepCode ) );
	}
	return TRUE;
}
