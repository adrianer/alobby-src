/*
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "stdafx.h"
#include "MakeString.h"
#include <stdio.h> 
#include <stdexcept>

#pragma comment(lib, "detours.lib")
#pragma comment(lib, "Ws2_32.lib")

using namespace std;

#define OUTPUT_DEBUG(msg) (::OutputDebugString((MAKE_STRING(msg)).c_str()))

// for later use by the my_gethostbyname function
string wantedIP;

// We'll use this function pointer to call the original gethostbyname
// after we're done performing any rewrites.
hostent* (WINAPI *real_gethostbyname)(const char*) = gethostbyname;

// 
hostent* WINAPI my_gethostbyname(const char* name)
{
	string strname(name);


	// Call the original gethostbyname function and return the data to the caller
	struct hostent *remoteHost;
	struct in_addr addr;
	int i = 0;
	char *addr_list[1] = {0};
	remoteHost = real_gethostbyname(strname.c_str());

	const char *wanted = wantedIP.c_str();

    while (remoteHost->h_addr_list[i] != 0) {
		addr_list[0] = remoteHost->h_addr_list[i++];
        addr.s_addr = *(u_long *) addr_list[0];
		if (strstr(inet_ntoa(addr), wanted)) {
			break;
		}
    }

	remoteHost->h_addr_list = addr_list;
	return remoteHost;
}

// 
__declspec(dllexport) BOOL APIENTRY DllMain(HMODULE hModule, DWORD dwReason, LPVOID lpReserved)
{
	if (dwReason == DLL_PROCESS_ATTACH)
	{
		ifstream infile(".ip");
		wantedIP = "10.3.";
		string line;
		if (infile)
		{
			getline(infile, line);
			wantedIP = line;
		}
		OUTPUT_DEBUG("S4_alobby: using \"" << wantedIP << "\" as wanted IP(-part)" << endl);

		// Use the Detours library to hook gethostbyname
		DetourTransactionBegin();
		DetourUpdateThread(GetCurrentThread());
		LONG result = DetourAttach(&(PVOID&)real_gethostbyname, my_gethostbyname);
		DetourTransactionCommit();

		if (result != NO_ERROR)
		{
			MessageBox(
				GetDesktopWindow(),
				"Error patching S4_alobby.exe gethostbyname()",
				"Based on the work of Dan Farino",
				MB_OK | MB_ICONERROR | MB_SYSTEMMODAL
				);

			return FALSE;
		}
	}
	return TRUE;
}
