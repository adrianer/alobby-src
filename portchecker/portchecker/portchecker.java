/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2019 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package portchecker;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import org.json.JSONObject;

public class portchecker
{

	/**
	 * Main constructor for this class.
	 * Checks if the necessary ip-address which should be checked is given as argument.
	 * 
	 * obligatory arguments:
	 * => --ip IP => represents the client-ip, e.g. 10.0.3.10
	 * => --host-ip => represents the host-ip, eg. 10.0.3.1
	 * => --configDir => directory where the JSON-files with the ports to check exists and the file with the results will be written
	 * 
	 * optional arguments:
	 * => --debugDir => directory where the logfile from portchecker.exe will be written
	 * 
	 * @param args	the command-line arguments
	 * @throws InterruptedException
	 * @throws IOException
	 */
	public static void main(String[] args) throws InterruptedException, IOException
	{
		String host = "";
		String configDir = "";
		String debugDir = "";
		String ownIp = "";
		// check the arguments
		for (int i = 0; i < args.length; ++i)
        {
			if( "--host-ip".equals(args[i]) ) {
				host = args[i+1];
				++i;
			}
			else if( "--ip".equals(args[i]) ) {
				ownIp = args[i+1];
				++i;
			}
			else if("--configDir".equals(args[i]) )
            {
                configDir = args[i+1];
                ++i;
            }
			else if("--debugDir".equals(args[i]) )
            {
                debugDir = args[i+1];
                ++i;
            }
        }

		// do not start portcheck if no host is configured
		if( "".equals(host) ) {
			System.out.println("No host-ip configured. Call with: --host-ip IP");
			System.exit(1);
		}
		
		// do not start portcheck if no own ip is configured
		if( "".equals(ownIp) ) {
			System.out.println("No own-ip configured. Call with: --ip IP");
			System.exit(2);
		}
		
		// do not start portcheck if no configDir is configured
		if( "".equals(configDir) ) {
			System.out.println("No configDir configured. Call with: --configDir DIR");
			System.exit(3);
		}
		else {
			File configDirObject = new File(configDir);
			if( false == configDirObject.exists() ) {
				System.out.println("Given configDir does not exists. Call with: --configDir DIR");
				System.exit(3);
			}
			if( false == configDirObject.canWrite() ) {
				System.out.println("Given configDir can not be written. Call with: --configDir DIR");
				System.exit(3);
			}
			if( false == new File(configDir + "/portlist.json").exists() ) {
				System.out.println("No portlist.json in configDir found!");
				System.exit(3);
			}
			if( false == new File(configDir + "/portlist.json").canRead() ) {
				System.out.println("File portlist.json in configDir could not be read!");
				System.exit(3);
			}
		}
		
		if( false == "".equals(debugDir) ) {
			File debugDirObject = new File(debugDir);
			if( false == debugDirObject.exists() ) {
				System.out.println("Given debugDir does not exists. Call with: --debugDir DIR");
				System.exit(3);
			}
			if( false == debugDirObject.canWrite() ) {
				System.out.println("Given debugDir can not be written. Call with: --debugDir DIR");
				System.exit(3);
			}
		}
		
		new checkThePorts( ownIp, host, configDir, debugDir );
	}
	
	/**
	 * Check the accessibility of configured tcp- and udp-ports.
	 * 
	 * @author Zwirni
	 *
	 */
	public static class checkThePorts {
		
		Socket cs;
		private ArrayList<Integer> tcpPortsOutgoing =  new ArrayList<Integer>();
		private ArrayList<Integer> tcpPortsIncoming =  new ArrayList<Integer>();
		private ArrayList<portResult> tcpPortCheckResults =  new ArrayList<portResult>();
		private ArrayList<Integer> udpPortsOutgoing =  new ArrayList<Integer>();
		private ArrayList<Integer> udpPortsIncoming =  new ArrayList<Integer>();
		private ArrayList<portResult> udpPortCheckResults =  new ArrayList<portResult>();
		private Format dateFormat = new SimpleDateFormat("HH:mm:ss.SSS");
		
		// IP of the host where socketListener.java is running.
		// Hint: this has to be the VPN-Host-IP.
		private String host = "";
		
		// configdir where the resulting json-file will be saves
		private String configDir = "";
		
		// debug-directory where (if given path is valid) the portchecker-logfile will be written
		private String debugDir = "";
		
		// the ip we actualy use on this machine
		private InetAddress ownIp = null;
		
		// default timeout
		private int timeout = 30;

		// set marker for outgoing-data-test
		final int directionOutgoing = 1;
		
		// set marker for incoming-data-test
		final int directionIncoming = 2;
		
		
		/**
		 * Run the portchecker.
		 * 
		 * @param ownIp			ip of the client
		 * @param host			ip of the server
		 * @param configDir		directory where the resulting JSON-file will be places
		 * @param debugDir		directory where the logfile from portchecker.exe will be written
		 * @throws InterruptedException
		 * @throws IOException
		 */
		public checkThePorts(String ownIp, String host, String configDir, String debugDir) throws InterruptedException, IOException {
			
			debug("ownIp: " + ownIp);
			debug("host: " + host);
			debug("configDir: " + configDir);
			debug("debugDir: " + debugDir);
			
			this.ownIp = InetAddress.getByName(ownIp);;
			this.host = host;
			this.configDir = configDir;
			this.debugDir = debugDir;
			
			// check if tcp-port 80 is available
			// -> if not show return exit-value 3
			portResult portResult80 = checkSingleTCPPort(80, false);
			if( portResult80 != null ) {
				int result = portResult80.getResult();
				if( result != 3 ) {
					System.exit(4);
				}
			}
			
			// get the ports to check from portlist.json in configDir
			File f = new File(this.configDir + "/portlist.json");
	        if( f.exists() && f.canRead() ){
	            try {
	            	// get the content of the file
	                String text = new String(Files.readAllBytes(Paths.get(f.getAbsolutePath())), StandardCharsets.UTF_8);
	                // get the JSON-object of this content
					JSONObject obj = new JSONObject(text);
					// get the tcp-ports and iterate through the results
					if( !obj.isNull("tcp") ) {
						JSONObject tcpList = obj.getJSONObject("tcp");
						// get the keys from the list
						Iterator<String> keys = tcpList.keys();
						
						// loop through the keys
						while(keys.hasNext()) {
						    String key = keys.next();
					        JSONObject port = tcpList.getJSONObject(key);
					        if( port.getInt("direction") == this.directionOutgoing ) {
					        	tcpPortsOutgoing.add(port.getInt("port"));
					        }
					        else {
					        	tcpPortsIncoming.add(port.getInt("port"));
					        }
						}
					}
					// get the udp-results and iterate through the results
					if( !obj.isNull("udp") ) {
						JSONObject udpList = obj.getJSONObject("udp");
						// get the keys from the list
						Iterator<String> keys = udpList.keys();
						
						// loop through the keys
						while(keys.hasNext()) {
						    String key = keys.next();
					        JSONObject port = udpList.getJSONObject(key);
					        if( port.getInt("direction") == this.directionOutgoing ) {
					        	udpPortsOutgoing.add(port.getInt("port"));
					        }
					        else {
					        	udpPortsIncoming.add(port.getInt("port"));
					        }
						}
					}
					
				} catch (IOException e) {
					System.out.println("Error reading portlist.json in configDir (format-error)!");
		        	System.exit(3);
				}
	        }
	        else {
	        	System.out.println("File portlist.json in configDir could not be read!");
	        	System.exit(3);
	        }
			
			// start the tcp-port-listener with check for outgoing data
			tcpPortChecker( false );
			
			// start the tcp-port-listener with check for incoming data
			tcpPortChecker( true );
			
			// start the udp-port-listener with check for outgoing data
			udpPortChecker( false );
			
			// start the udp-port-listener with check for incoming data
			udpPortChecker( true );
			
			// now write the result as JSON in the configDir
			// where the calling application can get and process it
			
			// -> create the JSON-object for tcp-results
			JSONObject tcpResults = new JSONObject();
			for( portResult port : tcpPortCheckResults ) {
				JSONObject values = new JSONObject();
				values.put("port", port.getPort());
				values.put("result", port.getResult());
				values.put("direction", port.getDirection());
				tcpResults.put(String.valueOf(port.getPort()) + port.getDirection(), values);
			}
			
			// -> create the JSON-object for udp-results
			JSONObject udpResults = new JSONObject();
			for( portResult port : udpPortCheckResults ) {
				JSONObject values = new JSONObject();
				values.put("port", port.getPort());
				values.put("result", port.getResult());
				values.put("direction", port.getDirection());
				udpResults.put(String.valueOf(port.getPort()) + port.getDirection(), values);
			}
			
			// -> create result-json for all results
			JSONObject results = new JSONObject();
			// -> and add all result to this
			results.put("tcp", tcpResults);
			results.put("udp", udpResults);
			
			// -> write it into the file in the configDir
			try (FileWriter file = new FileWriter(this.configDir + "/portcheckresults.json")) {
				file.write(results.toString());
			}
			
			// clean exit
			System.exit(0);
			
		}

		/**
		 * Check for open ports on udp.
		 * 
		 * @param inDirection			direction to check (true if incoming should be checked)
		 * @throws InterruptedException 
		 * @throws UnknownHostException 
		 * @throws IOException 
		 */
		private void udpPortChecker( boolean inDirection ) throws InterruptedException, UnknownHostException {
			
			// get host-ip
			InetAddress host = InetAddress.getByName(this.host);
			
			// get port-list-to-check depending on in- or outgoing
			ArrayList<Integer> udpPorts = this.udpPortsOutgoing;
			if( inDirection ) {
				udpPorts = this.udpPortsIncoming;				
			}
			
			// loop through the ports and check each if it is available
			for( int port: udpPorts ) {
				// wait 10ms before adding the next port-listener as it may be to fast
				Thread.sleep(10);
	            
	            // generate the result-object for this port
	            portResult portResult = new portResult();
	            portResult.setPort(port);
	            // -> set direction in result-object
	            if( inDirection ) {
	            	portResult.setDirection(this.directionIncoming);
	            }
	            else {
	            	portResult.setDirection(this.directionOutgoing);
	            }
	            
	            try{
	                byte [] bytes = new byte[128];
	                DatagramSocket ds = null;
	                if( inDirection ) {
	                	ds = new DatagramSocket(port, this.ownIp);
	                }
	                else {
	                	ds = new DatagramSocket();
	                }
	                if( ds != null ) {
		                DatagramPacket dp = new DatagramPacket(bytes, bytes.length, host, port);
		                ds.setSoTimeout(this.timeout * 1000);
		                ds.send(dp);
		                dp = new DatagramPacket(bytes, bytes.length);
		                ds.receive(dp);
		                ds.close();
	                }
	            }
	            catch(InterruptedIOException e){
	            	if( inDirection ) {
	            		debug("local udp port " + port + " on " + this.ownIp + " is closed.");
	            	}
	            	else {
	            		debug("udp port " + port + " on " + host + " is closed.");
	            	}
	            	portResult.setResult(0);
	            	udpPortCheckResults.add(portResult);
	                continue;
	            }
	            catch(IOException e){
	            	if( inDirection ) {
	            		debug("local udp port " + port + " on " + this.ownIp + " is closed.");
	            	}
	            	else {
	            		debug("udp port " + port + " on " + host + " is closed.");
	            	}
	            	portResult.setResult(0);
	            	udpPortCheckResults.add(portResult);
	            	continue;
	            }
	            if( inDirection ) {
            		debug("local udp port " + port + " on " + this.ownIp + " is open.");
            	}
            	else {
            		debug("udp port " + port + " on " + host + " is open.");
            	}
	            portResult.setResult(3);
	            udpPortCheckResults.add(portResult);
			}
		}

		/**
		 * Check for open ports on tcp. 
		 * 
		 * @param inDirection 
		 * @throws InterruptedException 
		 */
		private void tcpPortChecker(boolean inDirection) throws InterruptedException {
			// get port-list-to-check depending on in- or outgoing
			ArrayList<Integer> tcpPorts = this.tcpPortsOutgoing;
			if( inDirection ) {
				tcpPorts = this.tcpPortsIncoming;				
			}
			
			// loop through the ports and check each if it is available
			for( int port: tcpPorts ) {
				// wait 100ms before adding the next port-listener as it may be to fast
				Thread.sleep(100);
				
				// check this port
				portResult portResult = checkSingleTCPPort(port, inDirection);
	            
				// add the result
	            tcpPortCheckResults.add(portResult);
			}
		}
		
		/**
		 * Check a single tcp-port
		 * 
		 * @param port			the single port to check via tcp
		 * @param inDirection 	the direction which should be checked (true if port is open for 
		 * 						incomming data should be checked)
		 */
		private portResult checkSingleTCPPort( int port, boolean inDirection ) {
			// generate the result-object for this port
            portResult portResult = new portResult();
            portResult.setPort(port);
            // -> set direction in result-object
            if( inDirection ) {
            	portResult.setDirection(this.directionIncoming);
            }
            else {
            	portResult.setDirection(this.directionOutgoing);
            }
            
            System.out.println("direction: " + inDirection);
            
            Socket s = null;
            String reason = null ;
            try {
            	if( inDirection ) {
            		s = new Socket(host, port, this.ownIp, port);
            	}
            	else {
            		s = new Socket();
            	}
                s.setReuseAddress(true);
                SocketAddress sa = new InetSocketAddress(host, port);
                s.connect(sa, this.timeout * 1000);
            } catch (IOException e) {
                if ( e.getMessage().equals("Connection refused")) {
                	portResult.setResult(0);
                	if( inDirection ) {
                		debug("local tcp port " + port + " for incoming  data on " + this.ownIp + " is closed.");
                	}
                	else {
                		debug("tcp port " + port + " for outgoing data to " + host + " is closed.");
                	}
                };
                if ( e instanceof UnknownHostException ) {
                	portResult.setResult(1);
                    reason = "node " + host + " is unresolved.";
                }
                if ( e instanceof SocketTimeoutException ) {
                	portResult.setResult(2);
                    reason = "timeout while attempting to reach node " + host + " on port " + port;
                }
            } finally {
                if (s != null) {
                    if ( s.isConnected()) {
                    	portResult.setResult(3);
                    	if( inDirection ) {
                    		debug("local tcp port " + port + " for incoming data on " + this.ownIp + " is reachable!");
                    	}
                    	else {
                    		debug("tcp port " + port + " for outgoing data to " + host + " is reachable!");
                    	}
                    } else {
                    	portResult.setResult(4);
                    	debug("tcp port " + port + " for outgoing data to " + host + " is not reachable; reason: " + reason);
                    }
                    try {
                        s.close();
                    } catch (IOException e) {
                    	e.printStackTrace();
                    }
                }
                else {
                	debug("socket for tcp-port " + port + " not opened");
                }
            }
            return portResult;
		}
		
		/**
		 * Output the given debug-string into debug-file in debugDir and/or console.
		 * 
		 * @param text	text to output in debug-logfile
		 * @throws IOException 
		 */
		private void debug( String text ) {
			// output into logfile, if valid
			if( text.length() > 0 && debugDirIsValid() ) {
				// -> write it into the file
				try (FileWriter file = new FileWriter(this.debugDir + "/portchecker.log", true)) {
					file.write(dateFormat.format(new Date()) + " " + text + "\n");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			// output to console
			System.out.println(text);
		}
		
		/**
		 * Check if debugDir is valid.
		 */
		private boolean debugDirIsValid() {
			if( !"".equals(this.configDir) ) {
				File path = new File(this.configDir);
				if( path.exists() && path.canWrite() ) {
					return true;
				}
			}
			return false;
		}
		
		/**
		 * Result-Object for portchecker which manages the results for a single port.
		 * 
		 * @author Zwirni
		 */
		public class portResult {
			
			// the used port
			private int port = 0;
			
			// Possible values for result:
			// - 0 => closed
			// - 1 => unresolved
			// - 2 => timeout
			// - 3 => is reachable
			// - 4 => is not reachable
			private int result = 0;
			
			// marker for direction:
			// -> 0 => not set
			// -> 1 => outgoing
			// -> 2 => incoming
			private int direction = 0;
			
			// set port
			public void setPort( int port ) {
				this.port = port;
			}
			
			// set check-result
			public void setResult( int result ) {
				this.result = result;
			}
			
			// set direction which has been checked
			public void setDirection( int direction ) {
				if( direction == directionIncoming || direction == directionOutgoing ) {
					this.direction = direction;
				}
			}
			
			// get the port
			public int getPort() {
				return this.port;
			}
			
			// get the result
			public int getResult() {
				return this.result;
			}
			
			// get the direction
			public int getDirection() {
				return this.direction;
			}
			
		}

	}
}
