/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2015 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
//INPUT structure needs a defined _WIN32_WINNT, use win2000 as minimum
#define _WIN32_WINNT 0x0500

#include "unicodeHandling.h"
#include "sendInputNative.h"
#include <jni.h>
#include <windows.h>
#include <tchar.h>
#include <stdio.h>
#include <stdlib.h>


typedef struct {
    WORD count;
    INPUT atInput[1024];
} T_KEYB_INPUT;

T_KEYB_INPUT gtInput;

static boolean bAddKeyInputEvent(INPUT* ptInput)
{
	if (gtInput.count >= (sizeof(gtInput.atInput)/sizeof(INPUT) - 1))
    {
		return FALSE;
    }
	memcpy(&gtInput.atInput[gtInput.count++], ptInput, sizeof(INPUT));
	return TRUE;
}

static boolean bSendKeyInputEvents()
{
	int error;
	error = SendInput(gtInput.count,gtInput.atInput,sizeof(INPUT));
	if (error != gtInput.count)
	{
		gtInput.count = 0;
		return FALSE;
	}
	gtInput.count = 0;
	return TRUE;
}

static boolean bPressKey(WORD vKeyCode)
{
	INPUT key;
	key.type = INPUT_KEYBOARD;
	key.ki.wVk = vKeyCode;
	key.ki.dwFlags = 0;
	key.ki.time = 0;
	key.ki.wScan = 0;
	key.ki.dwExtraInfo = 0;

	return bAddKeyInputEvent(&key);
}

static boolean bReleaseKey(WORD vKeyCode)
{
	INPUT key;
	key.type = INPUT_KEYBOARD;
	key.ki.wVk = vKeyCode;
	key.ki.dwFlags = KEYEVENTF_KEYUP;
	key.ki.time = 0;
	key.ki.wScan = 0;
	key.ki.dwExtraInfo = 0;

	return bAddKeyInputEvent(&key);
}

static boolean bSendChar(TCHAR c)
{
	SHORT keyScanCode = VkKeyScan(c);
	SHORT controlByte = keyScanCode >> 8;
	boolean bResult = TRUE;

	if (controlByte == -1)
	{
		//unsupported character
		return FALSE;
	}

	if ((controlByte & 0x4) != 0)
	{
		bResult &= bPressKey(VK_MENU);
	}
	if ((controlByte & 0x2) != 0)
	{
		bResult &= bPressKey(VK_CONTROL);
	}
	if ((controlByte & 0x1) != 0)
	{
		bResult &= bPressKey(VK_SHIFT);
	}

	bResult &= bPressKey(keyScanCode & 0xff);
	bResult &= bReleaseKey(keyScanCode & 0xff);

	if ((controlByte & 0x1) != 0)
	{
		bResult &= bReleaseKey(VK_SHIFT);
	}
	if ((controlByte & 0x2) != 0)
	{
		bResult &= bReleaseKey(VK_CONTROL);
	}
	if ((controlByte & 0x4) != 0)
	{
		bResult &= bReleaseKey(VK_MENU);
	}
	return bResult;
}

static boolean bPrepareSendInput()
{
	boolean bResult = TRUE;
	//turn off CAPS LOCK if activated
	if ((GetKeyState(VK_CAPITAL) & 0x01) != 0)
	{
		bResult &= bPressKey(VK_CAPITAL);
		bResult &= bReleaseKey(VK_CAPITAL);
	}
	//always release SHIFT, CONTROL and ALT
	bResult &= bReleaseKey(VK_SHIFT);
	bResult &= bReleaseKey(VK_CONTROL);
	bResult &= bReleaseKey(VK_MENU);
	return bResult;
}

JNIEXPORT jboolean JNICALL Java_net_siedler3_alobby_s3gameinterface_SendInputNative_enterText
  (JNIEnv *env, jclass SendInputNative, jstring jText)
{
	boolean bResult = TRUE;
	WORD i;
	SHORT keyScanCode;
	LPCTSTR text = (*env)->GET_STRING_CHARS(env, jText, NULL);

	bResult &= bPrepareSendInput();
	for (i = 0; i < _tcslen(text); ++i)
	{
		keyScanCode = VkKeyScan(text[i]);
		bResult &= bSendChar(text[i]);
	}
	bSendKeyInputEvents();
	(*env)->RELEASE_STRING_CHARS(env, jText, text);
	return bResult;
}
