/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2015 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */

#include "KeyPressed.h"
#include <jni.h>
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>

static int log = 0;

JNIEXPORT void JNICALL Java_net_siedler3_alobby_s3gameinterface_AutoSaveKey_startKeyLogging
  (JNIEnv *env, jclass autoSaveKey, jobject autoSaveKeyObject)
{
    jmethodID jmid = (*env)->GetMethodID(env, autoSaveKey, "setKeyPressed", "()V");
    log = 1;
    GetAsyncKeyState(VK_SPACE); //ersten Aufruf ignorieren
    while (log)
    {
        if (GetAsyncKeyState(VK_SPACE))
        {
            (*env)->CallVoidMethod(env, autoSaveKeyObject, jmid);
            break;
        }
        Sleep(1);
    }
}

JNIEXPORT void JNICALL Java_net_siedler3_alobby_s3gameinterface_AutoSaveKey_stopKeyLogging
  (JNIEnv *env, jclass autoSaveKey)
{
	log = 0;
}
