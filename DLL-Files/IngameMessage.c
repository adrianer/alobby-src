/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2015 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */

#include "unicodeHandling.h"
#include "IngameMessage.h"
#include <jni.h>
#include <windows.h>
#include <tchar.h>

BOOL stopShowing = FALSE;

JNIEXPORT jboolean JNICALL Java_net_siedler3_alobby_gui_additional_JNIIngameMessage_showMessage
  (JNIEnv *env, jclass class, jstring message, jint xCoordinate, jint yCoordinate, jint fontSize)
{
    HWND hwnd = FindWindow(NULL, _T("S3"));
    HDC hdc;
    RECT messageArea;
    LPCTSTR wMessage;
    int x, y;
    HFONT hFont = CreateFont((int)fontSize, 0, 0, 0, 500, FALSE, FALSE, FALSE, 1, OUT_DEFAULT_PRECIS,
                           CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH, 0);

    wMessage = (*env)->GET_STRING_CHARS(env, message, 0);
    x = (int)xCoordinate;
    y = (int)yCoordinate;

    messageArea.left   = x;
    messageArea.top    = y;
    messageArea.right  = x+800;
    messageArea.bottom = y+50;

    stopShowing = FALSE;

    if (IsWindow(hwnd))
    {
        hdc = GetDC(hwnd);
        SetBkMode(hdc, TRANSPARENT);

        SelectObject(hdc, hFont);
        SetTextColor(hdc, RGB(255,255,255));

        while (!stopShowing && IsWindow(hwnd))
        {//Wenn das Fenster nicht im Vordergrund steht, wird 50 ms gewartet
         //und erneut abgefragt, ob es inzwischen im Vordergrund steht.
            if (GetForegroundWindow() == hwnd)
                DrawText(hdc, wMessage, -1, &messageArea, 0);
            else
                Sleep(50);
        }

        if (IsWindow(hwnd))
            ReleaseDC(hwnd, hdc);
    }

    (*env)->RELEASE_STRING_CHARS(env, message, wMessage);

    return (jboolean)IsWindow(hwnd);
}

JNIEXPORT void JNICALL Java_net_siedler3_alobby_gui_additional_JNIIngameMessage_stopShowing
  (JNIEnv *env, jclass class)
{
    stopShowing = TRUE;
}
