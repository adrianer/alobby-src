/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2015 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */

#include "windows.h"
#include "NativeProcess.h"
#include "unicodeHandling.h"
#include <jni.h>
#include <stdio.h>
#include <tchar.h>

#define TA_FAILED 0
#define TA_SUCCESS_CLEAN 1
#define TA_SUCCESS_KILL 2

#define FID_PROCESS_HANDLE "processHandle"
#define FID_PROCESS_ID "processId"
#define FID_PROCESS_WINDOW_HANDLE "processWindowHandle"

typedef struct {
	HWND hWnd;
	DWORD dwThreadId;
} T_ENUM_PROC_PARAM;


HANDLE getProcessHandleFromJavaObject(JNIEnv * env, jobject nativeProcessObj);
DWORD getProcessIdFromJavaObject(JNIEnv * env, jobject nativeProcessObj);
HWND getProcessWindowHandleFromJavaObject(JNIEnv * env, jobject nativeProcessObj);

BOOL CALLBACK EnumWindowsProc(HWND hwnd, LPARAM param);
DWORD WINAPI TerminateApp( DWORD dwPID, DWORD dwTimeout );
BOOL CALLBACK TerminateAppEnum( HWND hwnd, LPARAM lParam );


JNIEXPORT jboolean JNICALL Java_net_siedler3_alobby_util_NativeProcess_createProcessImpl
  (JNIEnv * env, jobject nativeProcessObj, jstring jCmdLine)
{
	jclass jclass;
	jfieldID jfid;

    STARTUPINFO si;
    PROCESS_INFORMATION pi;
    DWORD pId;
    T_ENUM_PROC_PARAM tEnumProcParam;
    int i;
    jboolean result;
    LPCTSTR cmdLineTmp;
    LPTSTR cmdLine;
    result = FALSE;

    ZeroMemory( &tEnumProcParam, sizeof(tEnumProcParam) );
    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);
    ZeroMemory( &pi, sizeof(pi) );

    cmdLineTmp = (*env)->GET_STRING_CHARS(env, jCmdLine, NULL);
    cmdLine = _tcsdup(cmdLineTmp); //createProcess needs a LPTSTR
    (*env)->RELEASE_STRING_CHARS(env, jCmdLine, cmdLineTmp);


    // Start the child process.
    if ( !CreateProcess( NULL,   // No module name (use command line)
    		cmdLine,        // Command line
    		NULL,           // Process handle not inheritable
    		NULL,           // Thread handle not inheritable
    		FALSE,          // Set handle inheritance to FALSE
    		0,              // No creation flags
    		NULL,           // Use parent's environment block
    		NULL,           // Use parent's starting directory
    		&si,            // Pointer to STARTUPINFO structure
    		&pi )           // Pointer to PROCESS_INFORMATION structure
    	)
    {
    	free (cmdLine);
        return FALSE;
    }
    free (cmdLine);

    //retrieve the process ID
    pId = GetProcessId(pi.hProcess);

    //retrieve the corresponding Window handle of the first window
    //that belongs to the process. Our application has only one window.
    tEnumProcParam.dwThreadId = pi.dwThreadId;
    WaitForInputIdle(pi.hProcess, INFINITE);

    for (i = 0; i < 100; i++)
    {
        //use a loop until we find the window..usually this should already
        //be the case in the first iteration

    	EnumWindows(EnumWindowsProc, (LPARAM) &tEnumProcParam);
    	if (tEnumProcParam.hWnd != 0)
    	{
    		// if EnumWindowsProc() finds the window, it stores it
    		// in tEnumProcParam.hWnd
    		//window found, abort loop
    		break;
    	}
    	Sleep(100);
    }

    if (tEnumProcParam.hWnd != 0)
    {
    	//store the process id and windowHandle in the java class object
    	jclass = (*env)->GetObjectClass( env, nativeProcessObj );
    	jfid = (*env)->GetFieldID( env, jclass, FID_PROCESS_ID, "I");
    	(*env)->SetIntField(env, nativeProcessObj, jfid, pId);
    	jfid = (*env)->GetFieldID( env, jclass, FID_PROCESS_WINDOW_HANDLE, "J");
    	(*env)->SetLongField(env, nativeProcessObj, jfid, (jlong) tEnumProcParam.hWnd);
    	result = TRUE;
    }
    //in case the process started, but the window does not show up, we leave it to
    //the user to kill the hanging process. The Java side does not know of the process.

    //close the process handle and thread handle
    CloseHandle( pi.hThread );
    CloseHandle( pi.hProcess );

    return result;
}

JNIEXPORT void JNICALL Java_net_siedler3_alobby_util_NativeProcess_destroyImpl
	(JNIEnv * env, jobject nativeProcessObj)
{
	DWORD dwPID;
	dwPID = getProcessIdFromJavaObject(env, nativeProcessObj);
	TerminateApp(dwPID, 3000); // do not care if the process is really terminated
}

JNIEXPORT jint JNICALL Java_net_siedler3_alobby_util_NativeProcess_waitForImpl
    (JNIEnv * env, jobject nativeProcessObj)
{
	DWORD exitCode;
	HANDLE hProcess;
	DWORD dwPID;

	dwPID = getProcessIdFromJavaObject(env, nativeProcessObj);
	hProcess = OpenProcess(SYNCHRONIZE, FALSE, dwPID);
	if (hProcess == NULL)
	{
		//in case we cannot access the process,
		//as this seems to be the behaviour of the default sun-implementation
		return 1;
	}

    // Wait until child process exits.
    WaitForSingleObject( hProcess, INFINITE );
    GetExitCodeProcess(hProcess, &exitCode);
    CloseHandle( hProcess );
    return exitCode;
}

JNIEXPORT jboolean JNICALL Java_net_siedler3_alobby_util_NativeProcess_isForegroundWindow
	(JNIEnv * env, jobject nativeProcessObj)
{
	HWND hWndForeground;
	HWND hWnd;
	hWnd = getProcessWindowHandleFromJavaObject(env, nativeProcessObj);
	hWndForeground = GetForegroundWindow();
	return hWnd == hWndForeground;
}


DWORD getProcessIdFromJavaObject(JNIEnv * env, jobject nativeProcessObj)
{
	jclass jclass;
	jfieldID jfid;
	jint pId;
	jclass = (*env)->GetObjectClass( env, nativeProcessObj );
	jfid = (*env)->GetFieldID( env, jclass, FID_PROCESS_ID, "I");
	pId = (*env)->GetIntField(env, nativeProcessObj, jfid);
	return pId;
}

HWND getProcessWindowHandleFromJavaObject(JNIEnv * env, jobject nativeProcessObj)
{
	jclass jclass;
	jfieldID jfid;
	jlong pHandle;
	jclass = (*env)->GetObjectClass( env, nativeProcessObj );
	jfid = (*env)->GetFieldID( env, jclass, FID_PROCESS_WINDOW_HANDLE, "J");
	pHandle = (*env)->GetLongField(env, nativeProcessObj, jfid);
	return (HANDLE) pHandle;
}


BOOL CALLBACK EnumWindowsProc(HWND hwnd, LPARAM param)
{
	//When calling EnumWindows, EnumWindowsProc is called for every window that is
	//currently existing with its corresponding WindowHandle .
	//We are interested in the one that belongs to our process,
	//so abort the enumeration as soon as it is found

	DWORD pID;
	DWORD tpId = GetWindowThreadProcessId(hwnd, &pID);
	T_ENUM_PROC_PARAM * ptEnumProcParam = (T_ENUM_PROC_PARAM *) param;

	if (tpId == ptEnumProcParam->dwThreadId)
	{
		//we have found a window with the same threadProcessId,
		//so this is the window we are interested in, store
		//it in our structure
		ptEnumProcParam->hWnd = hwnd;
		//abort further enumeration by returning false
		return FALSE;
	}
	return TRUE;
}


DWORD WINAPI TerminateApp( DWORD dwPID, DWORD dwTimeout )
{
     HANDLE hProc;
     DWORD dwRet;

     // If we can't open the process with PROCESS_TERMINATE rights,
     // then we give up immediately.
     hProc = OpenProcess(SYNCHRONIZE|PROCESS_TERMINATE, FALSE, dwPID);

     if (hProc == NULL)
     {
    	 return TA_FAILED ;
     }

     // TerminateAppEnum() posts WM_CLOSE to all windows whose PID
     // matches your process's.
     EnumWindows((WNDENUMPROC)TerminateAppEnum, (LPARAM) dwPID);

     // Wait on the handle. If it signals, great. If it times out,
     // then you kill it.
     if (WaitForSingleObject(hProc, dwTimeout) != WAIT_OBJECT_0)
     {
    	 dwRet = (TerminateProcess(hProc,0) ? TA_SUCCESS_KILL : TA_FAILED);
     }
     else
     {
    	 dwRet = TA_SUCCESS_CLEAN ;
     }

     CloseHandle(hProc);

     return dwRet;
}

BOOL CALLBACK TerminateAppEnum( HWND hwnd, LPARAM lParam )
{
   DWORD dwID;

   GetWindowThreadProcessId(hwnd, &dwID);

   if (dwID == (DWORD)lParam)
   {
      PostMessage(hwnd, WM_CLOSE, 0, 0);
   }

   return TRUE;
}



