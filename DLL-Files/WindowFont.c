/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2015 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#include "unicodeHandling.h"
#include "WindowFont.h"
#include <jni.h>
#include <windows.h>
#include <tchar.h>

void translateUmlaut(JNIEnv*, jstring);
int createAndWriteReferenceBitmap(LPCTSTR fileName);

static HFONT hFont;
static LPCTSTR label;
static int  fontSize;
static int  R, G, B;

JNIEXPORT jint JNICALL Java_net_siedler3_alobby_s3gameinterface_WindowFont_createReferenceBitmap
  (JNIEnv *env, jclass cl, jstring string, jint colorR, jint colorG, jint colorB, jstring fileName)
{
    HKEY hKey;
    DWORD dwDataInt = sizeof(DWORD);
    DWORD dwDataString = 256;
    LPCTSTR pcFileName;
    BYTE fontName[256] = "";

    //jstring in char* bzw wchar* umwandeln
    label = (*env)->GET_STRING_CHARS(env, string, 0);

    pcFileName = (*env)->GET_STRING_CHARS(env, fileName, 0);

    //RGB-Werte abspeichern
    R = (int)colorR;
    G = (int)colorG;
    B = (int)colorB;

    //Font und Fontsize aus Registry auslesen
    RegOpenKey(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\BlueByte\\Siedler3\\1.0\\General"), &hKey);
    RegQueryValueEx(hKey, _T("FontSize"), 0, 0, (BYTE*)&fontSize, &dwDataInt);
    RegQueryValueEx(hKey, _T("Font"), 0, 0, fontName, &dwDataString);
    RegCloseKey(hKey);

    //der Fontname wird von Windows entweder in einen Ansi-String oder einen Unicode
    //String verwandelt. Daher ist der Cast nach (LPCTSTR) korrekt
    hFont = CreateFont(fontSize, 0, 0, 0, 0, FALSE, FALSE, FALSE, 1, OUT_DEFAULT_PRECIS,
                       CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH, (LPCTSTR) fontName);

    //erzeugt den String und speichert ihn in die Datei ab.
    createAndWriteReferenceBitmap(pcFileName);

    //allokierte Objekte wieder freigeben
    DeleteObject(hFont);
    (*env)->RELEASE_STRING_CHARS(env, string, label);
    (*env)->RELEASE_STRING_CHARS(env, fileName, pcFileName);
    return 0;
}


int createAndWriteReferenceBitmap(LPCTSTR fileName)
{
    HDC hdc;
    HDC hdc2;
    HBITMAP aBmp;
    BITMAPINFO bi;
    HGDIOBJ OldObj;
    void *dibvalues;
    HANDLE fileHandle;
    size_t len;
    SIZE sz;
    int width, height;
    int maxWidth, maxHeight;

    BITMAPFILEHEADER bmfh;
    BITMAPINFOHEADER bmih;
    DWORD bytes_write;
    DWORD bytes_written;

    hdc = CreateDC(_T("DISPLAY"), NULL, NULL, NULL);
    hdc2 = CreateCompatibleDC(hdc);

    len = _tcslen(label);
    //Den Font setzen um die korrekte Gr��e des dargestellten
    //Textes berechnen zu k�nnen und sp�ter auch zeichnen zu k�nnen
    SelectObject(hdc2, hFont);
	GetTextExtentPoint32(hdc2, label, len, &sz);
	// so breit und hoch ist der dargestellte Text, mehr muss nicht gespeichert werden
    width  = (int) sz.cx;
    height = (int) sz.cy;

    //so gross ist der Bildschirm, l�nger kann der Text nicht sein
    maxWidth = GetDeviceCaps(hdc, HORZRES);
    maxHeight = GetDeviceCaps(hdc, VERTRES);

    //zu lange Strings m�ssen abgeschnitten werden
    width  = min(width, maxWidth);
    height = min(height, maxHeight);

    ZeroMemory(&bmih,sizeof(BITMAPINFOHEADER));
    bmih.biSize=sizeof(BITMAPINFOHEADER);
      bmih.biHeight=height;
        bmih.biWidth=width;
        bmih.biPlanes=1;
        bmih.biBitCount=24;
    bmih.biCompression=BI_RGB;
    bmih.biSizeImage = ((((bmih.biWidth * bmih.biBitCount) + 31) & ~31) >> 3) * bmih.biHeight;
        bmih.biXPelsPerMeter = 0;
        bmih.biYPelsPerMeter = 0;
        bmih.biClrImportant = 0;
    //bmih.biSizeImage=(3*bmih.biHeight*bmih.biWidth);
        //bmih.biSizeImage = 0;

    bi.bmiHeader=bmih;

    aBmp = CreateDIBSection(hdc, &bi, DIB_RGB_COLORS, &dibvalues, NULL, 0);

    if (aBmp==NULL)
    {
        OutputDebugString(_T("CreateDIBSection failed!\n"));
        return 0;
    }

    //Setze das Bitmap f�r den MemoryDeviceContext
    //hierein wird sp�ter der Text geschrieben
    OldObj = SelectObject(hdc2, aBmp);

    SetTextColor(hdc2, RGB(R, G, B));
    SetBkColor  (hdc2, RGB(0, 0, 0));
    SelectObject(hdc2, hFont);

    TextOut(hdc2, 0, 0, label, len);


    ZeroMemory(&bmfh,sizeof(BITMAPFILEHEADER));
    bmfh.bfOffBits=sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER);
    bmfh.bfSize=(3*bmih.biHeight*bmih.biWidth)+sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER);
	bmfh.bfType=0x4d42;
	bmfh.bfReserved1 = 0;
	bmfh.bfReserved2 = 0;

    fileHandle=CreateFile(fileName,GENERIC_READ | GENERIC_WRITE,(DWORD)0,NULL,
                                                    CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,(HANDLE) NULL);
    if (fileHandle==INVALID_HANDLE_VALUE)
    {
        OutputDebugString(_T("CreateFile failed!\n"));
        return 0;
    }

	// Write the BITMAPFILEHEADER
	bytes_write=sizeof(BITMAPFILEHEADER);

    if (!WriteFile(fileHandle,(void*)&bmfh,bytes_write,&bytes_written,NULL))
    {
        OutputDebugString(_T("WriteFile failed!\n"));
        return 0;
    }


    //Write the BITMAPINFOHEADER
    bytes_write=sizeof(BITMAPINFOHEADER);
    if (!WriteFile(fileHandle,(void*)&bmih,bytes_write,&bytes_written,NULL))
    {
        OutputDebugString(_T("WriteFile failed!\n"));
        return 0;
    }

	//Write the Color Index Array???
	bytes_write=bmih.biSizeImage;
    if (!WriteFile(fileHandle, dibvalues,bytes_write,&bytes_written,NULL))
    {
        OutputDebugString(_T("WriteFile failed!\n"));
        return 0;
    }

    CloseHandle(fileHandle);

    DeleteObject(SelectObject(hdc2,OldObj));
    DeleteDC(hdc2);
    DeleteDC(hdc);

    return 1;
}
