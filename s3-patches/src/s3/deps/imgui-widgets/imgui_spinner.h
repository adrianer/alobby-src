#ifndef SIEDLER3_FIXES_IMGUI_SPINNER_H
#define SIEDLER3_FIXES_IMGUI_SPINNER_H

#include <imgui.h>
#include <imgui_internal.h>

namespace ImGui {

bool BufferingBar(const char* label, float value,  const ImVec2& size_arg, const ImU32& bg_col, const ImU32& fg_col);
bool Spinner(const char* label, float radius, int thickness, const ImU32& color);

}

#endif // SIEDLER3_FIXES_IMGUI_SPINNER_H
