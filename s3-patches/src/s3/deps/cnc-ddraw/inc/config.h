#ifndef CONFIG_H
#define CONFIG_H

#define WIN32_LEAN_AND_MEAN
#include "windows.h"


typedef struct ddraw_config
{
    RECT window_rect;
    int window_state;
    char rendering_mode;
    BOOL should_apply_texture_filter;
    BOOL reduce_max_fps;
    BOOL force_display_resolution;
    BOOL force_window_mode;
    DWORD display_width;
    DWORD display_height;
} ddraw_config;

extern ddraw_config g_config;

void init_config();

#endif
