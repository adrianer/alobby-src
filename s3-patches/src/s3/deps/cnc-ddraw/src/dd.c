#include "windows.h"
#include <detours.h>
#include <math.h>
#include <directinput.h>
#include <stdio.h>
#include <IDirectDrawSurface.h>
#include "ddclipper.h"
#include "IDirectDraw.h"
#include "ddraw.h"
#include "dd.h"
#include "hook.h"
#include "config.h"
#include "mouse.h"
#include "wndproc.h"
#include "render_d3d9.h"
#include "render_gdi.h"
#include "render_ogl.h"
#include "debug.h"
#include "ddraw_utils.h"

HMODULE g_ddraw_module;
HMODULE g_patched_ddraw_module;
dd_proxy* g_ddProxy;
char g_warning_text[512] = {0};
DWORD g_warning_time_end;
SRWLOCK g_warning_lock;

DIRECTDRAWCREATEPROC g_realDirectDrawCreate;
DIRECTDRAWCREATECLIPPERPROC g_realDirectDrawCreateClipper;
DIRECTDRAWCREATEEXPROC g_realDirectDrawCreateEx;
DIRECTDRAWENUMERATEAPROC g_realDirectDrawEnumerateA;
DIRECTDRAWENUMERATEEXAPROC g_realDirectDrawEnumerateExA;
DIRECTDRAWENUMERATEEXWPROC g_realDirectDrawEnumerateExW;
DIRECTDRAWENUMERATEWPROC g_realDirectDrawEnumerateW;
COCREATEINSTANCEPROC g_realCoCreateInstance;

HRESULT WINAPI fake_DirectDrawCreate(GUID FAR* lpGUID, LPDIRECTDRAW FAR* lplpDD, IUnknown FAR* pUnkOuter)
{
    dd_dprintf("-> %s(lpGUID=%p, lplpDD=%p, pUnkOuter=%p)\n", __FUNCTION__, lpGUID, lplpDD, pUnkOuter);
    HRESULT ret = dd_CreateEx(lpGUID, (LPVOID*)lplpDD, (REFIID)&IID_IDirectDraw, pUnkOuter);
    dd_dprintf("<- %s\n", __FUNCTION__);
    return ret;
}

HRESULT WINAPI fake_DirectDrawCreateClipper(DWORD dwFlags, LPDIRECTDRAWCLIPPER FAR* lplpDDClipper, IUnknown FAR* pUnkOuter)
{
    dd_dprintf("-> %s(dwFlags=%08X, DDClipper=%p, unkOuter=%p)\n", __FUNCTION__, (int)dwFlags, lplpDDClipper, pUnkOuter);
    HRESULT ret = dd_CreateClipper(dwFlags, lplpDDClipper, pUnkOuter);
    dd_dprintf("<- %s\n", __FUNCTION__);
    return ret;
}

HRESULT WINAPI fake_DirectDrawCreateEx(GUID* lpGuid, LPVOID* lplpDD, REFIID iid, IUnknown* pUnkOuter)
{
    dd_dprintf("-> %s(lpGUID=%p, lplpDD=%p, riid=%08X, pUnkOuter=%p)\n", __FUNCTION__, lpGuid, lplpDD, iid, pUnkOuter);
    HRESULT ret = dd_CreateEx(lpGuid, lplpDD, iid, pUnkOuter);
    dd_dprintf("<- %s\n", __FUNCTION__);
    return ret;
}

HRESULT WINAPI fake_DirectDrawEnumerateA(LPDDENUMCALLBACK lpCallback, LPVOID lpContext)
{
    dd_dprintf("-> %s(lpCallback=%p, lpContext=%p)\n", __FUNCTION__, lpCallback, lpContext);

    if (lpCallback)
        lpCallback(NULL, "display", "(null)", lpContext);

    dd_dprintf("<- %s\n", __FUNCTION__);
    return DD_OK;
}

HRESULT WINAPI fake_DirectDrawEnumerateExA(LPDDENUMCALLBACKEXA lpCallback, LPVOID lpContext, DWORD dwFlags)
{
    dd_dprintf("-> %s(lpCallback=%p, lpContext=%p, dwFlags=%d)\n", __FUNCTION__, lpCallback, lpContext, dwFlags);

    if (lpCallback)
        lpCallback(NULL, "display", "(null)", lpContext, NULL);

    dd_dprintf("<- %s\n", __FUNCTION__);
    return DD_OK;
}

HRESULT WINAPI fake_DirectDrawEnumerateExW(LPDDENUMCALLBACKEXW lpCallback, LPVOID lpContext, DWORD dwFlags)
{
    dd_dprintf("-> %s(lpCallback=%p, lpContext=%p, dwFlags=%d)\n", __FUNCTION__, lpCallback, lpContext, dwFlags);

    if (lpCallback)
        lpCallback(NULL, L"display", L"(null)", lpContext, NULL);

    dd_dprintf("<- %s\n", __FUNCTION__);
    return DD_OK;
}

HRESULT WINAPI fake_DirectDrawEnumerateW(LPDDENUMCALLBACKW lpCallback, LPVOID lpContext)
{
    dd_dprintf("-> %s(lpCallback=%p, lpContext=%p)\n", __FUNCTION__, lpCallback, lpContext);

    if (lpCallback)
        lpCallback(NULL, L"display", L"(null)", lpContext);

    dd_dprintf("<- %s\n", __FUNCTION__);
    return DD_OK;
}

HRESULT WINAPI handle_CoCreateInstance(REFCLSID  rclsid, LPUNKNOWN pUnkOuter, DWORD     dwClsContext, REFIID    riid, LPVOID    *ppv) {
    HRESULT result;
    dd_dprintf("-> %s(rclsid=%d, riid=%d, ppv=%d, riid_is_dd=%d)\n", __FUNCTION__, rclsid, riid, ppv);
    if (IsEqualCLSID(rclsid, &CLSID_DirectDraw)) {
        result = dd_CreateEx(NULL, ppv, riid, pUnkOuter);
        dd_dprintf("<- %s: intercepted dd call (result=%d)\n", __FUNCTION__, result);
    } else {
        result = g_realCoCreateInstance(rclsid, pUnkOuter, dwClsContext, riid, ppv);
        dd_dprintf("<- %s: returning regular processing\n", __FUNCTION__);
    }

    return result;
}

void init_globals() {
    dd_dprintf("Loading ddraw library...\n");

    // Some users may have custom compatibility ddraw.dll's in their Settlers directory already (f.e. DxWrapper on VirtualBox). Some of
    // those may be incompatible with the launcher. At any rate, we do not need their compat layers for the launcher, so let's avoid any
    // issues by loading the system's ddraw.dll specifically.
    g_patched_ddraw_module = LoadLibraryEx("ddraw.dll", NULL, LOAD_LIBRARY_SEARCH_SYSTEM32);
    if (!g_patched_ddraw_module) {
        dbg_printf("ddrawHandle could not be obtained.\n");
        return;
    }
    dd_dprintf("Continue init process (dd=%0X2).\n", g_patched_ddraw_module);

    g_realDirectDrawCreate = GetProcAddress(g_patched_ddraw_module, "DirectDrawCreateA");
    g_realDirectDrawCreateClipper = GetProcAddress(g_patched_ddraw_module, "DirectDrawCreateClipper");
    g_realDirectDrawCreateEx = GetProcAddress(g_patched_ddraw_module, "DirectDrawCreateEx");
    g_realDirectDrawEnumerateA = GetProcAddress(g_patched_ddraw_module, "DirectDrawEnumerateA");
    g_realDirectDrawEnumerateExA = GetProcAddress(g_patched_ddraw_module, "DirectDrawEnumerateExA");
    g_realDirectDrawEnumerateExW = GetProcAddress(g_patched_ddraw_module, "DirectDrawEnumerateExW");
    g_realDirectDrawEnumerateW = GetProcAddress(g_patched_ddraw_module, "DirectDrawEnumerateW");
}

void show_d3d9_driver_warning() {
    char warningMessage[] = "Hardware rendering unavailable using fallback rendering which may not be as good/fast. Update graphics drivers/DirectX.";
    show_warning(warningMessage, 15);
}

void show_warning(char* message, unsigned int durationSeconds) {
    _snprintf(g_warning_text, sizeof(g_warning_text), message);
    g_warning_time_end = timeGetTime() + (durationSeconds * 1000);
}

void install_ddraw_compat(void** coCreateAddr, char renderingMode, char shouldApplyTextureFilter, char reduceMaxFps, char forceDisplayResolution, unsigned int displayWidth, unsigned int displayHeight, char forceWindowMode) {
    g_ddraw_module = GetCurrentThread();
#ifndef NDEBUG
    dbg_init();
#endif

    g_config.rendering_mode = renderingMode;
    g_config.should_apply_texture_filter = shouldApplyTextureFilter;
    g_config.reduce_max_fps = reduceMaxFps;
    g_config.force_display_resolution = forceDisplayResolution;
    g_config.force_window_mode = forceWindowMode;
    g_config.display_width = displayWidth;
    g_config.display_height = displayHeight;

    init_globals();
    hook_init();

    InitializeSRWLock(&g_warning_lock);
    *coCreateAddr = (void*)&handle_CoCreateInstance;

    DetourTransactionBegin();
    DetourAttach((PVOID*)&g_realDirectDrawCreate, (PVOID)fake_DirectDrawCreate);
    DetourTransactionCommit();

    DetourTransactionBegin();
    DetourAttach((PVOID*)&g_realDirectDrawCreateClipper, (PVOID)fake_DirectDrawCreateClipper);
    DetourTransactionCommit();

    DetourTransactionBegin();
    DetourAttach((PVOID*)&g_realDirectDrawCreateEx, (PVOID)fake_DirectDrawCreateEx);
    DetourTransactionCommit();

    DetourTransactionBegin();
    DetourAttach((PVOID*)&g_realDirectDrawEnumerateA, (PVOID)fake_DirectDrawEnumerateA);
    DetourTransactionCommit();

    DetourTransactionBegin();
    DetourAttach((PVOID*)&g_realDirectDrawEnumerateExA, (PVOID)fake_DirectDrawEnumerateExA);
    DetourTransactionCommit();

    DetourTransactionBegin();
    DetourAttach((PVOID*)&g_realDirectDrawEnumerateExW, (PVOID)fake_DirectDrawEnumerateExW);
    DetourTransactionCommit();

    DetourTransactionBegin();
    DetourAttach((PVOID*)&g_realDirectDrawEnumerateW, (PVOID)fake_DirectDrawEnumerateW);
    DetourTransactionCommit();

    dinput_hook();
}

HRESULT dd_EnumDisplayModes(DWORD dwFlags, LPDDSURFACEDESC lpDDSurfaceDesc, LPVOID lpContext, LPDDENUMMODESCALLBACK lpEnumModesCallback)
{
    DWORD i = 0;
    DDSURFACEDESC s;

    // Some games crash when you feed them with too many resolutions...

    if (g_ddProxy->bpp)
    {
        dd_dprintf("     g_ddraw->bpp=%u\n", g_ddProxy->bpp);

        //set up some filters to keep the list short
        DWORD refresh_rate = 0;
        DWORD bpp = 0;
        DWORD flags = 99998;
        DWORD fixed_output = 99998;
        DEVMODE m;

        memset(&m, 0, sizeof(DEVMODE));
        m.dmSize = sizeof(DEVMODE);

        while (EnumDisplaySettings(NULL, i, &m))
        {
            if (refresh_rate != 60 && m.dmDisplayFrequency >= 50)
                refresh_rate = m.dmDisplayFrequency;

            if (bpp != 32 && m.dmBitsPerPel >= 16)
                bpp = m.dmBitsPerPel;

            if (flags != 0)
                flags = m.dmDisplayFlags;

            if (fixed_output != DMDFO_DEFAULT)
                fixed_output = m.dmDisplayFixedOutput;

            memset(&m, 0, sizeof(DEVMODE));
            m.dmSize = sizeof(DEVMODE);
            i++;
        }

        memset(&m, 0, sizeof(DEVMODE));
        m.dmSize = sizeof(DEVMODE);
        i = 0;

        while (EnumDisplaySettings(NULL, i, &m))
        {
            if (refresh_rate == m.dmDisplayFrequency &&
                bpp == m.dmBitsPerPel &&
                flags == m.dmDisplayFlags &&
                fixed_output == m.dmDisplayFixedOutput)
            {
                dprintfex("     %d: %dx%d@%d %d bpp\n", (int)i, (int)m.dmPelsWidth, (int)m.dmPelsHeight, (int)m.dmDisplayFrequency, (int)m.dmBitsPerPel);

                memset(&s, 0, sizeof(DDSURFACEDESC));

                s.dwSize = sizeof(DDSURFACEDESC);
                s.dwFlags = DDSD_HEIGHT | DDSD_REFRESHRATE | DDSD_WIDTH | DDSD_PIXELFORMAT;
                s.dwHeight = m.dmPelsHeight;
                s.dwWidth = m.dmPelsWidth;
                s.dwRefreshRate = 60;
                s.ddpfPixelFormat.dwSize = sizeof(DDPIXELFORMAT);

                s.ddpfPixelFormat.dwFlags = DDPF_PALETTEINDEXED8 | DDPF_RGB;
                s.ddpfPixelFormat.dwRGBBitCount = 8;
                
                if (g_ddProxy->bpp == 16)
                {
                    s.ddpfPixelFormat.dwFlags = DDPF_RGB;
                    s.ddpfPixelFormat.dwRGBBitCount = 16;
                    s.ddpfPixelFormat.dwRBitMask = 0xF800;
                    s.ddpfPixelFormat.dwGBitMask = 0x07E0;
                    s.ddpfPixelFormat.dwBBitMask = 0x001F;
                }

                if (lpEnumModesCallback(&s, lpContext) == DDENUMRET_CANCEL)
                {
                    dd_dprintf("     DDENUMRET_CANCEL returned, stopping\n");
                    break;
                }
            }

            memset(&m, 0, sizeof(DEVMODE));
            m.dmSize = sizeof(DEVMODE);
            i++;
        }
    }
    else
    {
        SIZE resolutions[] =
        {
            { 320, 200 },
            { 320, 240 },
            { 640, 400 },
            { 640, 480 },
            { 800, 600 },
            { 1024, 768 },
            { 1280, 1024 },
            { 1280, 720 },
            { 1920, 1080 },
        };

        for (i = 0; i < sizeof(resolutions) / sizeof(resolutions[0]); i++)
        {
            memset(&s, 0, sizeof(DDSURFACEDESC));

            s.dwSize = sizeof(DDSURFACEDESC);
            s.dwFlags = DDSD_HEIGHT | DDSD_REFRESHRATE | DDSD_WIDTH | DDSD_PIXELFORMAT;
            s.dwHeight = resolutions[i].cy;
            s.dwWidth = resolutions[i].cx;
            s.dwRefreshRate = 60;
            s.ddpfPixelFormat.dwSize = sizeof(DDPIXELFORMAT);
            s.ddpfPixelFormat.dwFlags = DDPF_PALETTEINDEXED8 | DDPF_RGB;
            s.ddpfPixelFormat.dwRGBBitCount = 8;

            if (lpEnumModesCallback(&s, lpContext) == DDENUMRET_CANCEL)
            {
                dd_dprintf("     DDENUMRET_CANCEL returned, stopping\n");
                break;
            }

            s.ddpfPixelFormat.dwFlags = DDPF_RGB;
            s.ddpfPixelFormat.dwRGBBitCount = 16;
            s.ddpfPixelFormat.dwRBitMask = 0xF800;
            s.ddpfPixelFormat.dwGBitMask = 0x07E0;
            s.ddpfPixelFormat.dwBBitMask = 0x001F;

            if (lpEnumModesCallback(&s, lpContext) == DDENUMRET_CANCEL)
            {
                dd_dprintf("     DDENUMRET_CANCEL returned, stopping\n");
                break;
            }
        }
    }

    return DD_OK;
}

HRESULT dd_GetCaps(LPDDCAPS lpDDDriverCaps, LPDDCAPS lpDDEmulCaps)
{
    if (lpDDDriverCaps)
    {
        lpDDDriverCaps->dwSize = sizeof(DDCAPS);
        lpDDDriverCaps->dwCaps = DDCAPS_BLT | DDCAPS_PALETTE | DDCAPS_BLTCOLORFILL | DDCAPS_BLTSTRETCH | DDCAPS_CANCLIP;
        lpDDDriverCaps->dwCKeyCaps = 0;
        lpDDDriverCaps->dwPalCaps = DDPCAPS_8BIT | DDPCAPS_PRIMARYSURFACE;
        lpDDDriverCaps->dwVidMemTotal = 16777216;
        lpDDDriverCaps->dwVidMemFree = 16777216;
        lpDDDriverCaps->dwMaxVisibleOverlays = 0;
        lpDDDriverCaps->dwCurrVisibleOverlays = 0;
        lpDDDriverCaps->dwNumFourCCCodes = 0;
        lpDDDriverCaps->dwAlignBoundarySrc = 0;
        lpDDDriverCaps->dwAlignSizeSrc = 0;
        lpDDDriverCaps->dwAlignBoundaryDest = 0;
        lpDDDriverCaps->dwAlignSizeDest = 0;
        lpDDDriverCaps->ddsCaps.dwCaps = DDSCAPS_FLIP;
    }

    if (lpDDEmulCaps)
    {
        lpDDEmulCaps->dwSize = 0;
    }

    return DD_OK;
}

HRESULT dd_GetDisplayMode(LPDDSURFACEDESC lpDDSurfaceDesc)
{
    if (lpDDSurfaceDesc)
    {
        memset(lpDDSurfaceDesc, 0, sizeof(DDSURFACEDESC));

        lpDDSurfaceDesc->dwSize = sizeof(DDSURFACEDESC);
        lpDDSurfaceDesc->dwFlags = DDSD_HEIGHT | DDSD_REFRESHRATE | DDSD_WIDTH | DDSD_PITCH | DDSD_PIXELFORMAT;
        lpDDSurfaceDesc->dwHeight = g_ddProxy->height ? g_ddProxy->height : 768;
        lpDDSurfaceDesc->dwWidth = g_ddProxy->width ? g_ddProxy->width : 1024;
        lpDDSurfaceDesc->lPitch = lpDDSurfaceDesc->dwWidth;
        lpDDSurfaceDesc->dwRefreshRate = 60;
        lpDDSurfaceDesc->ddpfPixelFormat.dwSize = sizeof(DDPIXELFORMAT);

        lpDDSurfaceDesc->ddpfPixelFormat.dwFlags = DDPF_PALETTEINDEXED8 | DDPF_RGB;
        lpDDSurfaceDesc->ddpfPixelFormat.dwRGBBitCount = 8;

        if (g_ddProxy->bpp != 8)
        {
            lpDDSurfaceDesc->lPitch = lpDDSurfaceDesc->dwWidth * 2;
            lpDDSurfaceDesc->ddpfPixelFormat.dwFlags = DDPF_RGB;
            lpDDSurfaceDesc->ddpfPixelFormat.dwRGBBitCount = 16;
            lpDDSurfaceDesc->ddpfPixelFormat.dwRBitMask = 0xF800;
            lpDDSurfaceDesc->ddpfPixelFormat.dwGBitMask = 0x07E0;
            lpDDSurfaceDesc->ddpfPixelFormat.dwBBitMask = 0x001F;
        }
    }

    return DD_OK;
}

HRESULT dd_GetMonitorFrequency(LPDWORD lpdwFreq)
{
    *lpdwFreq = 60;
    return DD_OK;
}

HRESULT dd_RestoreDisplayMode()
{
    if (!g_ddProxy->render.run)
    {
        return DD_OK;
    }

    /* only stop drawing in GL mode when minimized */
    if (g_ddProxy->renderer != gdi_render_main)
    {
        EnterCriticalSection(&g_ddProxy->cs);
        g_ddProxy->render.run = FALSE;
        ReleaseSemaphore(g_ddProxy->render.sem, 1, NULL);
        LeaveCriticalSection(&g_ddProxy->cs);

        if (g_ddProxy->render.thread)
        {
            WaitForSingleObject(g_ddProxy->render.thread, INFINITE);
            g_ddProxy->render.thread = NULL;
        }

        if (g_ddProxy->renderer == d3d9_render_main)
        {
            d3d9_release();
        }
    }
    
    if (!g_ddProxy->windowed)
    {
        if (g_ddProxy->renderer != d3d9_render_main)
        {
            ChangeDisplaySettings(NULL, 0);
        }
    }

    return DD_OK;
}

HRESULT dd_SetDisplayMode(DWORD width, DWORD height, DWORD bpp)
{
    if (bpp != 8 && bpp != 16)
        return DDERR_INVALIDMODE;

    if (g_ddProxy->render.thread)
    {
        EnterCriticalSection(&g_ddProxy->cs);
        g_ddProxy->render.run = FALSE;
        ReleaseSemaphore(g_ddProxy->render.sem, 1, NULL);
        LeaveCriticalSection(&g_ddProxy->cs);

        WaitForSingleObject(g_ddProxy->render.thread, INFINITE);
        g_ddProxy->render.thread = NULL;
    }

    if (!g_ddProxy->mode.dmPelsWidth)
    {
        g_ddProxy->mode.dmSize = sizeof(DEVMODE);
        g_ddProxy->mode.dmDriverExtra = 0;

        if (EnumDisplaySettings(NULL, ENUM_CURRENT_SETTINGS, &g_ddProxy->mode) == FALSE)
        {
            g_ddProxy->mode.dmSize = sizeof(DEVMODE);
            g_ddProxy->mode.dmFields = DM_PELSWIDTH | DM_PELSHEIGHT | DM_BITSPERPEL | DM_DISPLAYFREQUENCY;
            g_ddProxy->mode.dmPelsWidth = real_GetSystemMetrics(SM_CXSCREEN);
            g_ddProxy->mode.dmPelsHeight = real_GetSystemMetrics(SM_CYSCREEN);
            g_ddProxy->mode.dmDisplayFrequency = 60;
            g_ddProxy->mode.dmBitsPerPel = 32;

            if (!g_ddProxy->mode.dmPelsWidth || !g_ddProxy->mode.dmPelsHeight)
            {
                g_ddProxy->fullscreen = FALSE;
            }
        }

        const int IDR_MYMENU = 93;

        const HANDLE hbicon = LoadImage(GetModuleHandle(0), MAKEINTRESOURCE(IDR_MYMENU), IMAGE_ICON, real_GetSystemMetrics(SM_CXICON), real_GetSystemMetrics(SM_CYICON), 0);
        if (hbicon)
            real_SendMessageA(g_ddProxy->hwnd, WM_SETICON, ICON_BIG, (LPARAM)hbicon);

        const HANDLE hsicon = LoadImage(GetModuleHandle(0), MAKEINTRESOURCE(IDR_MYMENU), IMAGE_ICON, real_GetSystemMetrics(SM_CXSMICON), real_GetSystemMetrics(SM_CYSMICON), 0);
        if (hsicon)
            real_SendMessageA(g_ddProxy->hwnd, WM_SETICON, ICON_SMALL, (LPARAM)hsicon);
    }

    if (g_ddProxy->altenter)
    {
        g_ddProxy->altenter = FALSE;

        memset(&g_ddProxy->render.mode, 0, sizeof(DEVMODE));

        g_ddProxy->render.mode.dmSize = sizeof(DEVMODE);
        g_ddProxy->render.mode.dmFields = DM_PELSWIDTH | DM_PELSHEIGHT;
        g_ddProxy->render.mode.dmPelsWidth = g_ddProxy->render.width;
        g_ddProxy->render.mode.dmPelsHeight = g_ddProxy->render.height;

        if (g_ddProxy->render.bpp)
        {
            g_ddProxy->render.mode.dmFields |= DM_BITSPERPEL;
            g_ddProxy->render.mode.dmBitsPerPel = g_ddProxy->render.bpp;
        }

        if (ChangeDisplaySettings(&g_ddProxy->render.mode, CDS_TEST) != DISP_CHANGE_SUCCESSFUL)
        {
            g_ddProxy->render.width = g_ddProxy->width;
            g_ddProxy->render.height = g_ddProxy->height;
        }
    }
    else
    {
        g_ddProxy->render.width = g_config.window_rect.right;
        g_ddProxy->render.height = g_config.window_rect.bottom;
    }

    //temporary fix: center window for games that keep changing their resolution
    if (g_ddProxy->width &&
        (g_ddProxy->width != width || g_ddProxy->height != height) &&
        (width > g_config.window_rect.right || height > g_config.window_rect.bottom))
    {
        g_config.window_rect.left = -32000;
        g_config.window_rect.top = -32000;
    }

    g_ddProxy->width = width;
    g_ddProxy->height = height;
    g_ddProxy->bpp = bpp;

    g_ddProxy->cursor.x = width / 2;
    g_ddProxy->cursor.y = height / 2;

    BOOL border = g_ddProxy->border;

    if (g_ddProxy->fullscreen)
    {
        g_ddProxy->render.width = g_ddProxy->mode.dmPelsWidth;
        g_ddProxy->render.height = g_ddProxy->mode.dmPelsHeight;

        if (g_ddProxy->windowed) //windowed-fullscreen aka borderless
        {
            border = FALSE;

            g_config.window_rect.left = -32000;
            g_config.window_rect.top = -32000;

            // prevent OpenGL from going automatically into fullscreen exclusive mode
            if (g_ddProxy->renderer == ogl_render_main)
                g_ddProxy->render.height++;

        }

        if (g_ddProxy->force_display_resolution) {
            g_ddProxy->render.width = g_ddProxy->display_width;
            g_ddProxy->render.height = g_ddProxy->display_height;
        }
    }

    if (g_ddProxy->render.width < g_ddProxy->width)
    {
        g_ddProxy->render.width = g_ddProxy->width;
    }

    if (g_ddProxy->render.height < g_ddProxy->height)
    {
        g_ddProxy->render.height = g_ddProxy->height;
    }

    g_ddProxy->render.run = TRUE;
    
    BOOL lock_mouse = g_ddProxy->locked || g_ddProxy->fullscreen;
    mouse_unlock();
	
    memset(&g_ddProxy->render.mode, 0, sizeof(DEVMODE));

    g_ddProxy->render.mode.dmSize = sizeof(DEVMODE);
    g_ddProxy->render.mode.dmFields = DM_PELSWIDTH|DM_PELSHEIGHT;
    g_ddProxy->render.mode.dmPelsWidth = g_ddProxy->render.width;
    g_ddProxy->render.mode.dmPelsHeight = g_ddProxy->render.height;

    if (g_ddProxy->render.bpp)
    {
        g_ddProxy->render.mode.dmFields |= DM_BITSPERPEL;
        g_ddProxy->render.mode.dmBitsPerPel = g_ddProxy->render.bpp;
    }
    
    BOOL maintas = g_ddProxy->maintas;

    if (!g_ddProxy->windowed || g_ddProxy->force_display_resolution)
    {
        // Making sure the chosen resolution is valid
        int old_width = g_ddProxy->render.width;
        int old_height = g_ddProxy->render.height;

        if (ChangeDisplaySettings(&g_ddProxy->render.mode, CDS_TEST) != DISP_CHANGE_SUCCESSFUL)
        {
            // fail... compare resolutions
            if (g_ddProxy->render.width > g_ddProxy->mode.dmPelsWidth ||
                g_ddProxy->render.height > g_ddProxy->mode.dmPelsHeight)
            {
                dprintf("Chosen resolution is bigger than current screen resolution.");
                // chosen game resolution higher than current resolution, use windowed mode for this case
                g_ddProxy->windowed = TRUE;
            }
            else
            {
                // Try 2x scaling
                g_ddProxy->render.width *= 2;
                g_ddProxy->render.height *= 2;

                g_ddProxy->render.mode.dmPelsWidth = g_ddProxy->render.width;
                g_ddProxy->render.mode.dmPelsHeight = g_ddProxy->render.height;

                if ((g_ddProxy->render.width > g_ddProxy->mode.dmPelsWidth ||
                     g_ddProxy->render.height > g_ddProxy->mode.dmPelsHeight) ||
                    ChangeDisplaySettings(&g_ddProxy->render.mode, CDS_TEST) != DISP_CHANGE_SUCCESSFUL)
                {
                    SIZE res = {0};

                    //try to get a resolution with the same aspect ratio as the requested resolution
                    BOOL found_res = util_get_lowest_resolution(
                        (float)old_width / old_height,
                        &res,
                        old_width + 1, //don't return the original resolution since we tested that one already
                        old_height + 1, g_ddProxy->mode.dmPelsWidth, g_ddProxy->mode.dmPelsHeight);

                    if (!found_res)
                    {
                        //try to get a resolution with the same aspect ratio as the current display mode
                        found_res = util_get_lowest_resolution(
                            (float)g_ddProxy->mode.dmPelsWidth / g_ddProxy->mode.dmPelsHeight,
                            &res,
                            old_width,
                            old_height, g_ddProxy->mode.dmPelsWidth, g_ddProxy->mode.dmPelsHeight);

                        if (found_res)
                            maintas = TRUE;
                    }

                    g_ddProxy->render.width = res.cx;
                    g_ddProxy->render.height = res.cy;

                    g_ddProxy->render.mode.dmPelsWidth = g_ddProxy->render.width;
                    g_ddProxy->render.mode.dmPelsHeight = g_ddProxy->render.height;
                    
                    if (!found_res || ChangeDisplaySettings(&g_ddProxy->render.mode, CDS_TEST) != DISP_CHANGE_SUCCESSFUL)
                    {
                        // try current display settings
                        g_ddProxy->render.width = g_ddProxy->mode.dmPelsWidth;
                        g_ddProxy->render.height = g_ddProxy->mode.dmPelsHeight;

                        g_ddProxy->render.mode.dmPelsWidth = g_ddProxy->render.width;
                        g_ddProxy->render.mode.dmPelsHeight = g_ddProxy->render.height;

                        if (ChangeDisplaySettings(&g_ddProxy->render.mode, CDS_TEST) != DISP_CHANGE_SUCCESSFUL)
                        {
                            // everything failed, use windowed mode instead
                            g_ddProxy->render.width = old_width;
                            g_ddProxy->render.height = old_height;

                            g_ddProxy->render.mode.dmPelsWidth = g_ddProxy->render.width;
                            g_ddProxy->render.mode.dmPelsHeight = g_ddProxy->render.height;

                            dprintf("Display change failed, falling back to window mode\n");
                            g_ddProxy->windowed = TRUE;
                            g_ddProxy->force_display_resolution = FALSE;
                        }
                        else
                            maintas = TRUE;
                    }
                }
            }
        }
    }

    if (g_ddProxy->nonexclusive && !g_ddProxy->windowed && g_ddProxy->renderer == ogl_render_main)
    {
        g_ddProxy->render.height++;
    }

    if (!g_ddProxy->handlemouse)
    {
        g_ddProxy->boxing = maintas = FALSE;
    }

    g_ddProxy->render.viewport.width = g_ddProxy->render.width;
    g_ddProxy->render.viewport.height = g_ddProxy->render.height;
    g_ddProxy->render.viewport.x = 0;
    g_ddProxy->render.viewport.y = 0;
    
    if (g_ddProxy->boxing)
    {
        g_ddProxy->render.viewport.width = g_ddProxy->width;
        g_ddProxy->render.viewport.height = g_ddProxy->height;

        int i;
        for (i = 20; i-- > 1;)
        {
            if (g_ddProxy->width * i <= g_ddProxy->render.width && g_ddProxy->height * i <= g_ddProxy->render.height)
            {
                g_ddProxy->render.viewport.width *= i;
                g_ddProxy->render.viewport.height *= i;
                break;
            }
        }

        g_ddProxy->render.viewport.y = g_ddProxy->render.height / 2 - g_ddProxy->render.viewport.height / 2;
        g_ddProxy->render.viewport.x = g_ddProxy->render.width / 2 - g_ddProxy->render.viewport.width / 2;
    }
    else if (maintas)
    {
        g_ddProxy->render.viewport.width = g_ddProxy->render.width;
        g_ddProxy->render.viewport.height = ((float)g_ddProxy->height / g_ddProxy->width) * g_ddProxy->render.viewport.width;
        
        if (g_ddProxy->render.viewport.height > g_ddProxy->render.height)
        {
            g_ddProxy->render.viewport.width =
                ((float)g_ddProxy->render.viewport.width / g_ddProxy->render.viewport.height) *
                g_ddProxy->render.height;

            g_ddProxy->render.viewport.height = g_ddProxy->render.height;
        }

        g_ddProxy->render.viewport.y = g_ddProxy->render.height / 2 - g_ddProxy->render.viewport.height / 2;
        g_ddProxy->render.viewport.x = g_ddProxy->render.width / 2 - g_ddProxy->render.viewport.width / 2;
    }

    g_ddProxy->render.scale_w = ((float)g_ddProxy->render.viewport.width / g_ddProxy->width);
    g_ddProxy->render.scale_h = ((float)g_ddProxy->render.viewport.height / g_ddProxy->height);
    g_ddProxy->render.unscale_w = ((float)g_ddProxy->width / g_ddProxy->render.viewport.width);
    g_ddProxy->render.unscale_h = ((float)g_ddProxy->height / g_ddProxy->render.viewport.height);

    if (g_ddProxy->windowed && !g_ddProxy->force_display_resolution)
    {
        MSG msg; // workaround for "Not Responding" window problem in cnc games
        PeekMessage(&msg, g_ddProxy->hwnd, 0, 0, PM_NOREMOVE);

        if (!border)
        {
            real_SetWindowLongA(g_ddProxy->hwnd, GWL_STYLE, GetWindowLong(g_ddProxy->hwnd, GWL_STYLE) & ~(WS_CAPTION | WS_THICKFRAME | WS_MINIMIZE | WS_MAXIMIZE | WS_SYSMENU));
        }
        else
        {
            real_SetWindowLongA(g_ddProxy->hwnd, GWL_STYLE, (GetWindowLong(g_ddProxy->hwnd, GWL_STYLE) | WS_OVERLAPPEDWINDOW) & ~WS_MAXIMIZEBOX);
        }

        if (g_ddProxy->wine)
            real_SetWindowLongA(g_ddProxy->hwnd, GWL_STYLE, (GetWindowLong(g_ddProxy->hwnd, GWL_STYLE) | WS_MINIMIZEBOX) & ~(WS_MAXIMIZEBOX | WS_THICKFRAME));

        /* center the window with correct dimensions */
        int cy = g_ddProxy->mode.dmPelsWidth ? g_ddProxy->mode.dmPelsWidth : g_ddProxy->render.width;
        int cx = g_ddProxy->mode.dmPelsHeight ? g_ddProxy->mode.dmPelsHeight : g_ddProxy->render.height;
        int x = (g_config.window_rect.left != -32000) ? g_config.window_rect.left : (cy / 2) - (g_ddProxy->render.width / 2);
        int y = (g_config.window_rect.top != -32000) ? g_config.window_rect.top : (cx / 2) - (g_ddProxy->render.height / 2);
        
        RECT dst = { x, y, g_ddProxy->render.width + x, g_ddProxy->render.height + y };
        
        AdjustWindowRect(&dst, GetWindowLong(g_ddProxy->hwnd, GWL_STYLE), FALSE);
        real_SetWindowPos(g_ddProxy->hwnd, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
        real_MoveWindow(g_ddProxy->hwnd, dst.left, dst.top, (dst.right - dst.left), (dst.bottom - dst.top), TRUE);

        BOOL d3d9_active = FALSE;

        if (g_ddProxy->renderer == d3d9_render_main)
        {
            d3d9_active = d3d9_create();

            if (!d3d9_active)
            {
                d3d9_release();

                show_d3d9_driver_warning();
                g_ddProxy->renderer = gdi_render_main;
            }
        }

        if (lock_mouse)
            mouse_lock();
    }
    else
    {
        LONG style = GetWindowLong(g_ddProxy->hwnd, GWL_STYLE);

        if ((style & WS_CAPTION))
        {
            real_SetWindowLongA(g_ddProxy->hwnd, GWL_STYLE, style & ~(WS_CAPTION | WS_THICKFRAME | WS_MINIMIZE | WS_MAXIMIZE | WS_SYSMENU));
        }

        BOOL d3d9_active = FALSE;

        if (g_ddProxy->renderer == d3d9_render_main)
        {
            d3d9_active = d3d9_create();

            if (!d3d9_active)
            {
                d3d9_release();

                show_d3d9_driver_warning();
                g_ddProxy->renderer = gdi_render_main;
            }
        }

        if (!d3d9_active && ChangeDisplaySettings(&g_ddProxy->render.mode, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL)
        {
            g_ddProxy->render.run = FALSE;
            g_ddProxy->windowed = TRUE;
            return dd_SetDisplayMode(width, height, bpp);
        }

        if (g_ddProxy->wine)
        {
            real_SetWindowLongA(g_ddProxy->hwnd, GWL_STYLE, GetWindowLong(g_ddProxy->hwnd, GWL_STYLE) | WS_MINIMIZEBOX);
        }

        real_SetWindowPos(g_ddProxy->hwnd, HWND_TOPMOST, 0, 0, g_ddProxy->render.width, g_ddProxy->render.height, SWP_SHOWWINDOW);
        g_ddProxy->last_set_window_pos_tick = timeGetTime();

        mouse_lock();
    }
    
    if (g_ddProxy->render.viewport.x != 0 || g_ddProxy->render.viewport.y != 0)
    {
        RedrawWindow(g_ddProxy->hwnd, NULL, NULL, RDW_ERASE | RDW_INVALIDATE);
    }

    if (g_ddProxy->render.thread == NULL)
    {
        InterlockedExchange(&g_ddProxy->render.palette_updated, TRUE);
        InterlockedExchange(&g_ddProxy->render.surface_updated, TRUE);
        ReleaseSemaphore(g_ddProxy->render.sem, 1, NULL);

        g_ddProxy->render.thread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)g_ddProxy->renderer, NULL, 0, NULL);
    }

    if (g_ddProxy->sierrahack)
    {
        PostMessageA(g_ddProxy->hwnd, WM_MOVE, 0, MAKELPARAM(-32000, -32000));
        PostMessageA(g_ddProxy->hwnd, WM_DISPLAYCHANGE, g_ddProxy->bpp, MAKELPARAM(g_ddProxy->width, g_ddProxy->height));
    }

    return DD_OK;
}

HRESULT dd_SetCooperativeLevel(HWND hwnd, DWORD dwFlags)
{
    PIXELFORMATDESCRIPTOR pfd;

    /* Red Alert for some weird reason does this on Windows XP */
    if (hwnd == NULL)
    {
        return DD_OK;
    }

    if (g_ddProxy->hwnd == NULL)
    {
        g_ddProxy->hwnd = hwnd;
    }

    if (!g_ddProxy->hwndInitialized)
    {
        g_ddProxy->hwndInitialized = TRUE;

        g_ddProxy->wndproc = (LRESULT(CALLBACK *)(HWND, UINT, WPARAM, LPARAM))GetWindowLong(hwnd, GWL_WNDPROC);
        real_SetWindowLongA(g_ddProxy->hwnd, GWL_WNDPROC, (LONG)fake_WndProc);

        if (!g_ddProxy->render.hdc)
        {
            g_ddProxy->render.hdc = GetDC(g_ddProxy->hwnd);

            memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));

            pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
            pfd.nVersion = 1;
            pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_DOUBLEBUFFER | (g_ddProxy->renderer == ogl_render_main ? PFD_SUPPORT_OPENGL : 0);
            pfd.iPixelType = PFD_TYPE_RGBA;
            pfd.cColorBits = g_ddProxy->render.bpp ? g_ddProxy->render.bpp : g_ddProxy->mode.dmBitsPerPel;
            pfd.iLayerType = PFD_MAIN_PLANE;

            SetPixelFormat(g_ddProxy->render.hdc, ChoosePixelFormat(g_ddProxy->render.hdc, &pfd), &pfd);
        }

        if (g_ddProxy->handlemouse && g_ddProxy->windowed)
        {
            while (real_ShowCursor(FALSE) > 0); //workaround for direct input games
            while (real_ShowCursor(TRUE) < 0);
        }

        real_SetCursor(LoadCursor(NULL, IDC_ARROW));

        GetWindowText(g_ddProxy->hwnd, (LPTSTR)&g_ddProxy->title, sizeof(g_ddProxy->title));

        g_ddProxy->isredalert = strcmp(g_ddProxy->title, "Red Alert") == 0;
        g_ddProxy->iscnc1 = strcmp(g_ddProxy->title, "Command & Conquer") == 0;

        if (g_ddProxy->vhack && !g_ddProxy->isredalert && !g_ddProxy->iscnc1)
        {
            g_ddProxy->vhack = 0;
        }
    }

    return DD_OK;
}

HRESULT dd_WaitForVerticalBlank(DWORD dwFlags, HANDLE h)
{
    if (!g_ddProxy->flip_limiter.tick_length)
        return DD_OK;

    if (g_ddProxy->flip_limiter.htimer)
    {
        FILETIME last_flip_ft = { 0 };
        GetSystemTimeAsFileTime(&last_flip_ft);

        if (!g_ddProxy->flip_limiter.due_time.QuadPart)
        {
            memcpy(&g_ddProxy->flip_limiter.due_time, &last_flip_ft, sizeof(LARGE_INTEGER));
        }
        else
        {
            while (CompareFileTime((FILETIME*)&g_ddProxy->flip_limiter.due_time, &last_flip_ft) == -1)
                g_ddProxy->flip_limiter.due_time.QuadPart += g_ddProxy->flip_limiter.tick_length_ns;

            SetWaitableTimer(g_ddProxy->flip_limiter.htimer, &g_ddProxy->flip_limiter.due_time, 0, NULL, NULL, FALSE);
            WaitForSingleObject(g_ddProxy->flip_limiter.htimer, g_ddProxy->flip_limiter.tick_length * 2);
        }
    }
    else
    {
        static DWORD next_game_tick;

        if (!next_game_tick)
        {
            next_game_tick = timeGetTime();
            return DD_OK;
        }

        next_game_tick += g_ddProxy->flip_limiter.tick_length;
        DWORD tick_count = timeGetTime();

        int sleep_time = next_game_tick - tick_count;

        if (sleep_time <= 0 || sleep_time > g_ddProxy->flip_limiter.tick_length)
        {
            next_game_tick = tick_count;
        }
        else
        {
            Sleep(sleep_time);
        }
    }

    return DD_OK;
}

ULONG dd_AddRef()
{
    return ++g_ddProxy->ref;
}

ULONG dd_Release()
{
    g_ddProxy->ref--;

    if (g_ddProxy->dk2hack)
    {
        static BOOL once; 
        if (!once) once = g_ddProxy->ref-- + 1;
    }

    if (g_ddProxy->ref == 0)
    {
        if (g_ddProxy->bpp)
        {
            // save cfg
        }

        if (g_ddProxy->render.run)
        {
            EnterCriticalSection(&g_ddProxy->cs);
            g_ddProxy->render.run = FALSE;
            ReleaseSemaphore(g_ddProxy->render.sem, 1, NULL);
            LeaveCriticalSection(&g_ddProxy->cs);

            if (g_ddProxy->render.thread)
            {
                WaitForSingleObject(g_ddProxy->render.thread, INFINITE);
                g_ddProxy->render.thread = NULL;
            }

            if (g_ddProxy->renderer == d3d9_render_main)
            {
                d3d9_release();
            }
            else if (!g_ddProxy->windowed)
            {
                ChangeDisplaySettings(NULL, 0);
            }
        }

        if (g_ddProxy->render.hdc)
        {
            ReleaseDC(g_ddProxy->hwnd, g_ddProxy->render.hdc);
            g_ddProxy->render.hdc = NULL;
        }

        if (g_ddProxy->ticks_limiter.htimer)
        {
            CancelWaitableTimer(g_ddProxy->ticks_limiter.htimer);
            CloseHandle(g_ddProxy->ticks_limiter.htimer);
            g_ddProxy->ticks_limiter.htimer = NULL;
        }

        if (g_ddProxy->flip_limiter.htimer)
        {
            CancelWaitableTimer(g_ddProxy->flip_limiter.htimer);
            CloseHandle(g_ddProxy->flip_limiter.htimer);
            g_ddProxy->flip_limiter.htimer = NULL;
        }

        if (g_ddProxy->fps_limiter.htimer)
        {
            CancelWaitableTimer(g_ddProxy->fps_limiter.htimer);
            CloseHandle(g_ddProxy->fps_limiter.htimer);
            g_ddProxy->fps_limiter.htimer = NULL;
        }

        DeleteCriticalSection(&g_ddProxy->cs);

        /* restore old wndproc, subsequent ddraw creation will otherwise fail */
        real_SetWindowLongA(g_ddProxy->hwnd, GWL_WNDPROC, (LONG)g_ddProxy->wndproc);
        g_ddProxy->hwndInitialized = FALSE;

        HeapFree(GetProcessHeap(), 0, g_ddProxy);
        g_ddProxy = NULL;

        return 0;
    }

    return g_ddProxy->ref;
}

HRESULT dd_GetAvailableVidMem(void* lpDDCaps, LPDWORD lpdwTotal, LPDWORD lpdwFree)
{
    *lpdwTotal = 16777216;
    *lpdwFree = 16777216;
    return DD_OK;
}

HRESULT dd_GetVerticalBlankStatus(LPBOOL lpbIsInVB)
{
    *lpbIsInVB = TRUE;
    return DD_OK;
}

HRESULT dd_CreateEx(GUID* lpGuid, LPVOID* lplpDD, REFIID iid, IUnknown* pUnkOuter)
{
    if (g_ddProxy)
    {
        return DDERR_DIRECTDRAWALREADYCREATED;
    }

    g_ddProxy = (dd_proxy*)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(dd_proxy));

    IDirectDrawImpl* dd = (IDirectDrawImpl*)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(IDirectDrawImpl));
    
    if (iid && IsEqualGUID(&IID_IDirectDraw, iid))
    {
        dd_dprintf("     GUID = %08X (IID_IDirectDraw), ddraw = %p\n", ((GUID*)iid)->Data1, dd);

        dd->lpVtbl = &g_dd_vtbl1;
    }
    else
    {
        dd_dprintf("     GUID = %08X (IID_IDirectDrawX), ddraw = %p\n", iid ? ((GUID*)iid)->Data1 : NULL, dd);

        dd->lpVtbl = &g_dd_vtblx;
    }

    IDirectDraw_AddRef(dd);

    *lplpDD = (LPVOID)dd;

    InitializeCriticalSection(&g_ddProxy->cs);

    g_ddProxy->render.sem = CreateSemaphore(NULL, 0, 1, NULL);
    g_ddProxy->wine = GetProcAddress(GetModuleHandleA("ntdll.dll"), "wine_get_version") != 0;

    dprintf("Creating rendering device.\n");
    init_config();

    return DD_OK;
}
