#include "windows.h"
#include <stdio.h>
#include "dd.h"
#include "ddsurface.h"
#include "opengl_utils.h"
#include "ddraw_utils.h"
#include "wndproc.h"
#include "debug.h"


DWORD WINAPI gdi_render_main(void)
{
    Sleep(500);

    DWORD tick_start = 0;
    DWORD tick_end = 0;

    int max_fps = g_ddProxy->render.maxfps;

    g_ddProxy->fps_limiter.tick_length_ns = 0;
    g_ddProxy->fps_limiter.tick_length = 0;

    if (max_fps < 0)
        max_fps = g_ddProxy->mode.dmDisplayFrequency;

    if (max_fps > 1000)
        max_fps = 0;

    if (max_fps > 0)
    {
        float len = 1000.0f / max_fps;
        g_ddProxy->fps_limiter.tick_length_ns = len * 10000;
        g_ddProxy->fps_limiter.tick_length = len + (g_ddProxy->accurate_timers ? 0.5f : 0.0f);
    }

    DWORD timeout = g_ddProxy->render.minfps > 0 ? g_ddProxy->render.minfps_tick_len : INFINITE;

    while (g_ddProxy->render.run &&
        (g_ddProxy->render.minfps < 0 || WaitForSingleObject(g_ddProxy->render.sem, timeout) != WAIT_FAILED))
    {
#if _DEBUG
        dbg_draw_frame_info_start();
#endif

        draw_warning_message();

        if (g_ddProxy->fps_limiter.tick_length > 0)
            tick_start = timeGetTime();

        EnterCriticalSection(&g_ddProxy->cs);

        if (g_ddProxy->primary && (g_ddProxy->bpp == 16 || (g_ddProxy->primary->palette && g_ddProxy->primary->palette->data_rgb)))
        {
            BOOL scale_cutscene = g_ddProxy->vhack && util_detect_cutscene();

            if (g_ddProxy->vhack)
                InterlockedExchange(&g_ddProxy->incutscene, scale_cutscene);

            if (!g_ddProxy->handlemouse)
            {
                g_ddProxy->child_window_exists = FALSE;
                EnumChildWindows(g_ddProxy->hwnd, util_enum_child_proc, (LPARAM)g_ddProxy->primary);
            }

            if (g_ddProxy->bnet_active)
            {
                RECT rc = { 0, 0, g_ddProxy->render.width, g_ddProxy->render.height };
                FillRect(g_ddProxy->render.hdc, &rc, (HBRUSH)GetStockObject(BLACK_BRUSH));
            }
            else if (scale_cutscene)
            {
                StretchDIBits(g_ddProxy->render.hdc, g_ddProxy->render.viewport.x, g_ddProxy->render.viewport.y,
                              g_ddProxy->render.viewport.width, g_ddProxy->render.viewport.height,
                    0,
                              g_ddProxy->height - 400,
                    CUTSCENE_WIDTH, 
                    CUTSCENE_HEIGHT, g_ddProxy->primary->surface,
                              g_ddProxy->primary->bmi,
                    DIB_RGB_COLORS, 
                    SRCCOPY);
            }
            else if (!g_ddProxy->child_window_exists &&
                     (g_ddProxy->render.width != g_ddProxy->width || g_ddProxy->render.height != g_ddProxy->height))
            {
                StretchDIBits(g_ddProxy->render.hdc, g_ddProxy->render.viewport.x, g_ddProxy->render.viewport.y,
                              g_ddProxy->render.viewport.width, g_ddProxy->render.viewport.height,
                    0, 
                    0,
                              g_ddProxy->width, g_ddProxy->height, g_ddProxy->primary->surface, g_ddProxy->primary->bmi,
                    DIB_RGB_COLORS, 
                    SRCCOPY);
            }
            else
            {
                SetDIBitsToDevice(g_ddProxy->render.hdc,
                    0, 
                    0, g_ddProxy->width, g_ddProxy->height,
                    0, 
                    0, 
                    0,
                                  g_ddProxy->height, g_ddProxy->primary->surface, g_ddProxy->primary->bmi,
                    DIB_RGB_COLORS);
            } 
        }

        LeaveCriticalSection(&g_ddProxy->cs);

#if _DEBUG
        dbg_draw_frame_info_end();
#endif

        if (g_ddProxy->fps_limiter.tick_length > 0)
        {
            if (g_ddProxy->fps_limiter.htimer)
            {
                FILETIME ft = { 0 };
                GetSystemTimeAsFileTime(&ft);

                if (CompareFileTime((FILETIME*)&g_ddProxy->fps_limiter.due_time, &ft) == -1)
                {
                    memcpy(&g_ddProxy->fps_limiter.due_time, &ft, sizeof(LARGE_INTEGER));
                }
                else
                {
                    WaitForSingleObject(g_ddProxy->fps_limiter.htimer, g_ddProxy->fps_limiter.tick_length * 2);
                }

                g_ddProxy->fps_limiter.due_time.QuadPart += g_ddProxy->fps_limiter.tick_length_ns;
                SetWaitableTimer(g_ddProxy->fps_limiter.htimer, &g_ddProxy->fps_limiter.due_time, 0, NULL, NULL, FALSE);
            }
            else
            {
                tick_end = timeGetTime();

                if (tick_end - tick_start < g_ddProxy->fps_limiter.tick_length)
                {
                    Sleep(g_ddProxy->fps_limiter.tick_length - (tick_end - tick_start));
                }
            }
        }
    }

    return TRUE;
}
