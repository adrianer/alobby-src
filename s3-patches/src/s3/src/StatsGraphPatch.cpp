#include <algorithm>
#include <debug_console.h>
#include "StatsGraphPatch.h"
#include "s3.h"
#include <implot.h>
#include <implot_internal.h>
#include <imgui.h>
#include "imgui_utils.h"

#define SNAPSHOT_INTERVAL_SECONDS 5

// Uncomment to enable extended debug output
//#define STATS_GRAPH_DEBUG 1

typedef struct stats_chunk : file_chunk_header {
    unsigned int numEntries;
    unsigned int numPlayers;
    unsigned int statsEntrySize;
    unsigned int lastGameTick;
    unsigned int nextSnapshotSeconds;
    unsigned int nextSnapshotTick;
} stats_chunk;

typedef struct loaded_stats_chunk : stats_chunk {
    stats_player snapshots[];
} loaded_stats_chunk;

typedef struct marker_chunk : file_chunk_header {
    unsigned int numMarkers;
    unsigned int markerSize;
} marker_chunk;

typedef struct loaded_marker_chunk : marker_chunk {
    event_marker markers[];
} loaded_marker_chunk;

typedef struct current_stats_chunk : file_chunk_header {
    bool isMultiplayerGame;
    uint32_t initialGameVersion;
    stats_player stats[20];
} current_stats_chunk;

static stats_player tmpStatsHolder[20];

void reset_stats() {
    auto stats = s3_get_player_stats();
    for (int i = 0; i < 20; i++) {
        stats[i].pointScore = 0;
        stats[i].settlerScore = 0;
        stats[i].buildingScore = 0;
        stats[i].mineScore = 0;
        stats[i].foodScore = 0;
        stats[i].soldierScore = 0;
        stats[i].goldScore = 0;
        stats[i].manaScore = 0;
        stats[i].battleScore = 0;
    }
}

void copy_stats(stats_player* targetStats, global_player_stats* sourceStats) {
    for (int i = 0; i < 20; i++) {
        targetStats[i].settlerScore = sourceStats[i].settlerScore;
        targetStats[i].buildingScore = sourceStats[i].buildingScore;
        targetStats[i].foodScore = sourceStats[i].foodScore;
        targetStats[i].mineScore = sourceStats[i].mineScore;
        targetStats[i].goldScore = sourceStats[i].goldScore;
        targetStats[i].manaScore = sourceStats[i].manaScore;
        targetStats[i].soldierScore = sourceStats[i].soldierScore;
        targetStats[i].battleScore = sourceStats[i].battleScore;
    }
}

void copy_stats(global_player_stats* targetStats, stats_player* sourceStats) {
    for (int i = 0; i < 20; i++) {
        targetStats[i].settlerScore = sourceStats[i].settlerScore;
        targetStats[i].buildingScore = sourceStats[i].buildingScore;
        targetStats[i].foodScore = sourceStats[i].foodScore;
        targetStats[i].mineScore = sourceStats[i].mineScore;
        targetStats[i].goldScore = sourceStats[i].goldScore;
        targetStats[i].manaScore = sourceStats[i].manaScore;
        targetStats[i].soldierScore = sourceStats[i].soldierScore;
        targetStats[i].battleScore = sourceStats[i].battleScore;
    }
}

void save_stats_start(hl::CpuContext* cpuContext) {
    copy_stats(tmpStatsHolder, s3_get_player_stats());
    reset_stats();
}

void save_stats_end(hl::CpuContext* cpuContext) {
    copy_stats(s3_get_player_stats(), tmpStatsHolder);
}

void StatsGraphPatch::enable() {
    m_hooker.hookDetour(Constants::S3_EXE + 0x10A153, 8, save_stats_start);
    m_hooker.hookDetour(Constants::S3_EXE + 0x10A1F6, 10, save_stats_end);
}

void StatsGraphPatch::disable() {
    m_hooker.revert();
}

void StatsGraphPatch::OnSave(logic_thread* logicThread, void* saveFile) {
    // Save the chunk containing all the stats snapshots
    stats_chunk snapshotChunk;
    snapshotChunk.type = FILE_CHUNK_STATS;
    snapshotChunk.metaInfo = 0;
    snapshotChunk.numEntries = m_statsSnapshots.size();
    snapshotChunk.lastGameTick = m_lastGameTick;
    snapshotChunk.nextSnapshotSeconds = m_nextSnapshotSecond;
    snapshotChunk.nextSnapshotTick = m_nextSnapshotTick;
    snapshotChunk.numPlayers = m_numPlayers;
    snapshotChunk.statsEntrySize = sizeof(stats_player);

    unsigned int dataSize = snapshotChunk.numEntries * sizeof(stats_player);
    snapshotChunk.size = sizeof(stats_chunk) + dataSize;

    dprintf("Saving stats snapshots (numEntries=%d, vectorCapacity=%d, dataSize=%d, chunkSize=%lu)\n", m_statsSnapshots.size(),
            m_statsSnapshots.capacity(), dataSize, snapshotChunk.size);
    s3_file_chunk_write(saveFile, FILE_CHUNK_STATS, &snapshotChunk, dataSize, m_statsSnapshots.data());

    // Save the chunk containing event markers like when a player died
    marker_chunk markerChunk;
    markerChunk.type = FILE_CHUNK_MARKERS;
    markerChunk.metaInfo = 0;
    markerChunk.numMarkers = m_eventMarkers.size();
    markerChunk.markerSize = sizeof(event_marker);

    if (markerChunk.numMarkers > 0) {
        dataSize = markerChunk.numMarkers * sizeof(event_marker);
        markerChunk.size = sizeof(marker_chunk) + dataSize;
        dprintf("Saving event markers (numMarkers=%d)\n", markerChunk.numMarkers);
        s3_file_chunk_write(saveFile, FILE_CHUNK_MARKERS, &markerChunk, dataSize, m_eventMarkers.data());
    }

    // Save the chunk containing the most recent stats (we move it to a different location)
    current_stats_chunk currentStats;
    currentStats.type = FILE_CHUNK_CURRENT_STATS;
    currentStats.metaInfo = 0;
    currentStats.size = sizeof(current_stats_chunk);
    currentStats.isMultiplayerGame = m_isMultiplayerGame;
    currentStats.initialGameVersion = m_initialGameVersion;

    auto stats = s3_get_player_stats();
    copy_stats(currentStats.stats, stats);
    s3_file_chunk_write(saveFile, FILE_CHUNK_CURRENT_STATS, &currentStats);
}

void StatsGraphPatch::OnLoadSave(logic_thread* logicThread, void* saveFile) {
    bool settingsLoaded = false;
    auto chunk = static_cast<loaded_stats_chunk*>(s3_file_chunk_read(saveFile, FILE_CHUNK_STATS));
    if (!chunk) {
        dprintf("Stats chunk not found in save\n");
    } else {
        if (chunk->statsEntrySize != sizeof(stats_player)) {
            dprintf("Stats size changed, cannot load saved stats data (%d != %d)\n", chunk->statsEntrySize, sizeof(stats_player));
        } else {
            m_lastGameTick = chunk->lastGameTick;
            m_nextSnapshotSecond = chunk->nextSnapshotSeconds;
            m_nextSnapshotTick = chunk->nextSnapshotTick;
            m_numPlayers = chunk->numPlayers;

            m_statsSnapshots.clear();
            m_statsSnapshots.reserve(chunk->numEntries);
            for (int i = 0; i < chunk->numEntries; i++) {
                m_statsSnapshots.push_back(chunk->snapshots[i]);
            }

            dprintf("Loaded %d snapshots from save file\n", m_statsSnapshots.size());
            settingsLoaded = true;
        }
    }

    s3_file_chunk_free(saveFile, FILE_CHUNK_STATS);
    if (!settingsLoaded) {
        Initialize(logicThread);
    }

    m_eventMarkers.clear();
    auto markerChunk = static_cast<loaded_marker_chunk*>(s3_file_chunk_read(saveFile, FILE_CHUNK_MARKERS));
    if (markerChunk) {
        if (markerChunk->markerSize != sizeof(event_marker)) {
            dprintf("Marker size changed, cannot load saved markers (%d != %d)\n", markerChunk->markerSize, sizeof(event_marker));
        } else {
            m_eventMarkers.reserve(markerChunk->numMarkers);
            for (int i = 0; i < markerChunk->numMarkers; i++) {
                m_eventMarkers.push_back(markerChunk->markers[i]);
            }

            dprintf("Loaded %d event markers from save file\n", m_eventMarkers.size());
        }
    }
    s3_file_chunk_free(saveFile, FILE_CHUNK_MARKERS);

    auto currentStatsChunk = static_cast<current_stats_chunk*>(s3_file_chunk_read(saveFile, FILE_CHUNK_CURRENT_STATS));
    if (currentStatsChunk) {
        auto stats = s3_get_player_stats();
        copy_stats(stats, currentStatsChunk->stats);
        m_isMultiplayerGame = currentStatsChunk->isMultiplayerGame;
        m_initialGameVersion = currentStatsChunk->initialGameVersion;
    }
    s3_file_chunk_free(saveFile, FILE_CHUNK_CURRENT_STATS);
}

void StatsGraphPatch::OnBeforeGameLoop(logic_thread* logicThread) {
    auto tick = logicThread->currentGameTick;
    if (m_lastGameTick == tick) {
        return;
    }
    m_lastGameTick = tick;

    if (tick < m_nextSnapshotTick) {
        return;
    }

    m_nextSnapshotSecond += SNAPSHOT_INTERVAL_SECONDS;
    m_nextSnapshotTick = ceil((double)m_nextSnapshotSecond * 937 / 60);

#ifdef STATS_GRAPH_DEBUG
    dprintf("=============== Tick: %d ===================\n", tick);
#endif

    stats_player snapshot;
    auto currentStats = s3_get_player_stats();
    for (short i = 0; i < m_numPlayers; i++) {
        snapshot.tick = tick;
        snapshot.settlerScore = currentStats[i].settlerScore;
        snapshot.buildingScore = currentStats[i].buildingScore;
        snapshot.foodScore = currentStats[i].foodScore;
        snapshot.mineScore = currentStats[i].mineScore;
        snapshot.goldScore = currentStats[i].goldScore;
        snapshot.manaScore = currentStats[i].manaScore;
        snapshot.soldierScore = currentStats[i].soldierScore;
        snapshot.battleScore = currentStats[i].battleScore;

        // Normally the game only computes this at the very end when someone leaves, lets compute it now.
        // We could also consider not storing this at all, since it can be computed at any time using the other values.
        snapshot.pointScore = snapshot.goldScore + snapshot.buildingScore + snapshot.foodScore + snapshot.mineScore + snapshot.manaScore +
                              5 * snapshot.battleScore + 2 * snapshot.soldierScore + 2 * snapshot.settlerScore;

        m_statsSnapshots.push_back(snapshot);

#ifdef STATS_GRAPH_DEBUG
        dprintf("Player #%d - Points: %lu, Settlers:%lu, Buildings: %lu, Food: %lu, Mines: %lu, Gold: %lu, Mana: %lu, Soldiers: %lu, "
                "Battles: %lu\n",
                i, snapshot.pointScore, snapshot.settlerScore, snapshot.buildingScore, snapshot.foodScore, snapshot.mineScore,
                snapshot.goldScore, snapshot.manaScore, snapshot.soldierScore, snapshot.battleScore);
#endif
    }

#ifdef STATS_GRAPH_DEBUG
    dprintf("=============================================\n");
#endif
}

void StatsGraphPatch::OnAfterGameInit(logic_thread* logicThread) {
    if (s3_get_game_settings()->isSaveGame) {
        return;
    }

    Initialize(logicThread);
}

void StatsGraphPatch::Initialize(logic_thread* logicThread) {
    m_lastGameTick = -1;
    m_nextSnapshotTick = logicThread->currentGameTick;
    m_nextSnapshotSecond = floor((double)logicThread->currentGameTick / 937 * 60);
    m_statsSnapshots.clear();
    m_eventMarkers.clear();
    m_isMultiplayerGame = s3_get_game_settings()->isMultiplayerGame;
    m_initialGameVersion = *m_settlersVersion;

    for (short i = 20; i >= 0; i--) {
        if (logicThread->gameData->players[i - 1].type != PLAYER_TYPE_EMPTY) {
            m_numPlayers = i;
            break;
        }
    }
}

void StatsGraphPatch::initializeGuiData() {
    if (m_isGuiInitialized) {
        return;
    }

    m_isGuiInitialized = true;
    m_showExtendedStats = false;
    m_xValueCount = m_statsSnapshots.size() / m_numPlayers;
    m_xAxisValues = new int[m_xValueCount];

    auto statsEntries = m_statsSnapshots.data();
    for (int j = 0; j < m_xValueCount; j++) {
        auto timeStats = statsEntries[j * m_numPlayers];

        int gameSeconds = static_cast<int>(floorf(static_cast<float>(timeStats.tick) / 937 * 60));
        m_xAxisValues[j] = gameSeconds;
    }

    Category category;
    std::string translatedName;

    translatedName = m_i18n->__("points", "Points");
    strncpy(category.name, translatedName.c_str(), sizeof(category.name));
    category.offset = offsetof(stats_player, pointScore);
    m_categories.push_back(category);

    translatedName = m_i18n->__("settlers", "Settlers");
    strncpy(category.name, translatedName.c_str(), sizeof(category.name));
    category.offset = offsetof(stats_player, settlerScore);
    m_categories.push_back(category);

    translatedName = m_i18n->__("buildings", "Buildings");
    strncpy(category.name, translatedName.c_str(), sizeof(category.name));
    category.offset = offsetof(stats_player, buildingScore);
    m_categories.push_back(category);

    translatedName = m_i18n->__("food", "Food");
    strncpy(category.name, translatedName.c_str(), sizeof(category.name));
    category.offset = offsetof(stats_player, foodScore);
    m_categories.push_back(category);

    translatedName = m_i18n->__("mine", "Mine");
    strncpy(category.name, translatedName.c_str(), sizeof(category.name));
    category.offset = offsetof(stats_player, mineScore);
    m_categories.push_back(category);

    translatedName = m_i18n->__("gold", "Gold");
    strncpy(category.name, translatedName.c_str(), sizeof(category.name));
    category.offset = offsetof(stats_player, goldScore);
    m_categories.push_back(category);

    translatedName = m_i18n->__("mana", "Mana");
    strncpy(category.name, translatedName.c_str(), sizeof(category.name));
    category.offset = offsetof(stats_player, manaScore);
    m_categories.push_back(category);

    translatedName = m_i18n->__("soldiers", "Soldiers");
    strncpy(category.name, translatedName.c_str(), sizeof(category.name));
    category.offset = offsetof(stats_player, soldierScore);
    m_categories.push_back(category);

    translatedName = m_i18n->__("battles", "Battles");
    strncpy(category.name, translatedName.c_str(), sizeof(category.name));
    category.offset = offsetof(stats_player, battleScore);
    m_categories.push_back(category);
}

void StatsGraphPatch::cleanUpGuiData() {
    if (!m_isGuiInitialized) {
        return;
    }

    delete[] m_xAxisValues;
    m_categories.clear();
    m_isGuiInitialized = false;
}

void StatsGraphPatch::OnRenderFrame(RenderContext* context) {
    if (context->activeDialogId != STATS_SCREEN_DIALOG_ID) {
        cleanUpGuiData();
        return;
    }

    if (m_isStatsHidden) {
        return;
    }

    initializeGuiData();

    ImGui::SetNextWindowCollapsed(false, ImGuiCond_Appearing);
    ImGui::SetNextWindowPos(ImScaledVec2(35, 25), ImGuiCond_Appearing);
    if (ImGui::Begin("#gamestats-open", NULL,
                     ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoBackground |
                         ImGuiWindowFlags_NoMove | ImGuiWindowFlags_AlwaysAutoResize)) {
        if (ImGui::Button(m_i18n->__("view_extended_game_stats", "View Extended Game Stats").c_str())) {
            m_showExtendedStats = !m_showExtendedStats;
        }

        if (m_showExtendedStats) {
            ImGui::SetNextWindowBgAlpha(1.0f);
            if (ImGui::BeginChildFrame(ImGui::GetID("#gamestats-container"), ImScaledVec2(700, 370), ImGuiWindowFlags_NoMove)) {
                auto stats = s3_get_player_stats();
                auto statsEntries = m_statsSnapshots.data();

                std::vector<int> playerIndices;
                for (int i = 0; i < m_numPlayers; i++) {
                    if (stats[i].rank == 0 || !stats[i].playerName.sText) {
                        continue;
                    }

                    playerIndices.push_back(i);
                }

                if (ImGui::BeginTabBar("Stats")) {
                    static ImVec4 colors[] = {
                        ImColor(255, 30, 0),    ImColor(30, 150, 255),  ImColor(255, 255, 0),   ImColor(52, 191, 91),
                        ImColor(255, 150, 0),   ImColor(0, 255, 255),   ImColor(255, 0, 255),   ImColor(50, 50, 50),
                        ImColor(255, 255, 255), ImColor(12, 30, 255),   ImColor(220, 85, 30),   ImColor(150, 150, 150),
                        ImColor(186, 19, 234),  ImColor(0, 120, 0),     ImColor(255, 200, 200), ImColor(147, 251, 177),
                        ImColor(170, 20, 60),   ImColor(220, 150, 240), ImColor(255, 220, 159), ImColor(150, 200, 250),
                    };

                    for (auto category : m_categories) {
                        if (ImGui::BeginTabItem(category.name)) {
                            int** counts = new int*[20];
                            int maxValue = 0;

                            for (int i : playerIndices) {
                                counts[i] = new int[m_xValueCount];

                                for (int j = 0; j < m_xValueCount; j++) {
                                    auto timeStats = statsEntries[i + j * m_numPlayers];
                                    int value = *(reinterpret_cast<DWORD*>(reinterpret_cast<BYTE*>(&(timeStats.tick)) + category.offset));
                                    counts[i][j] = value;
                                    if (value > maxValue) {
                                        maxValue = value;
                                    }
                                }
                            }

                            ImPlot::SetNextPlotLimitsX(0, std::max(m_nextSnapshotSecond, 600));
                            ImPlot::SetNextPlotLimitsY(0, (maxValue / 10 + 2) * 10);
                            if (ImPlot::BeginPlot(category.name, m_i18n->__("game_time", "Game Time").c_str(),
                                                  m_i18n->__("count", "Count").c_str(), ImScaledVec2(700, 330), ImPlotFlags_None,
                                                  ImPlotAxisFlags_Duration, ImPlotAxisFlags_None)) {
                                for (int i : playerIndices) {
                                    auto color = colors[s3_get_player_color(i)];
                                    ImPlot::SetNextFillStyle(color);
                                    ImPlot::SetNextLineStyle(color);
                                    ImPlot::PlotLine(stats[i].playerName.sText, m_xAxisValues, counts[i], m_xValueCount);
                                }

                                ImPlot::EndPlot();
                            }

                            for (int i : playerIndices) {
                                delete[] counts[i];
                            }
                            delete[] counts;

                            ImGui::EndTabItem();
                        }
                    }

                    ImGui::EndTabBar();
                }
            }

            ImGui::EndChildFrame();
        }
    }

    ImGui::End();
}

void StatsGraphPatch::OnGameLeave(logic_thread* logicThread, bool isGameOver) {
    // If it is not a multiplayer game, always show the statistics.
    if (!m_isMultiplayerGame) {
        m_isStatsHidden = false;

        return;
    }

    // If the game has not completed, we hide some stats that might be abused.
    m_isStatsHidden = !isGameOver && shouldHideStats(logicThread);
    if (m_isStatsHidden) {
        dprintf("Resetting some stats as game is not completed yet\n");
        reset_stats();
    }
}

/**
 * Returns whether we should still hide the statistics.
 *
 * This is intended to hide the stats if the game has to be left early even though it has not finished yet. Then, ideally
 * we would like to not show the stats just yet to prevent an abuse of that information.
 *
 * If the game is decided, i.e. one team won, then the stats are always shown. Besides that, we use time-based conditions or a measure of
 * how much fighting has been going on already to decide whether to show stats.
 *
 * @param logicThread pointer to the game's logic thread
 * @return true if stats should be hidden
 */
bool StatsGraphPatch::shouldHideStats(logic_thread* logicThread) {
    // If the save was created by a newer launcher version, we do not support showing stats with an older client. The reasoning is that we
    // may have improved the logic of this method, and someone may try to bypass the new logic by using an older client to load the save.
    if (*m_settlersVersion < m_initialGameVersion) {
        return true;
    }

    // If we are playing with Vanilla clients, always show the stats.
    if (*m_isVanillaVersion) {
        return false;
    }

    // Something fishy is going on.
    if (s3_get_game_settings()->isMultiplayerGame != m_isMultiplayerGame) {
        return true;
    }

    // Release stats once we reach a certain in-game time regardless of what has happened in the game so far.
    int minutes = logicThread->currentGameTick / 937;
    int statsReleaseTime = 60;
    if (s3_get_game_settings()->goodsSetting == GOODS_LOW) {
        statsReleaseTime = 105;
    } else if (s3_is_random_game()) {
        statsReleaseTime = 90;
    }
    if (minutes > statsReleaseTime) {
        return false;
    }

    auto stats = s3_get_player_stats();
    int numSoldiers = 0, numBattles = 0;
    for (int i = 0; i < 20; i++) {
        numSoldiers += stats[i].soldierScore;
        numBattles += stats[i].battleScore;
    }
    if (numSoldiers > 20 && static_cast<double>(numBattles) / static_cast<double>(numSoldiers) > 0.3) {
        return false;
    }

    return true;
}
