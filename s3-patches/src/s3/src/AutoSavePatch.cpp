#include "AutoSavePatch.h"

#define AUTO_SAVE_INTERVAL_TICKS 937 * 2 // this is the minimum imposed by the game (1 minute) multiplied by 2 to save only every two minutes
#define MAX_RANDOM_DELAY_TICKS 80    // up to about 5 seconds random delay

void AutoSavePatch::enable() {}

void AutoSavePatch::disable() {}

AutoSavePatch::AutoSavePatch(I18n* i18n, bool* isVanillaVersion, bool isEnabled)
    : m_i18n(i18n), m_isVanillaVersion(isVanillaVersion), m_enabled(isEnabled) {
    std::random_device rd;
    gen = std::mt19937(rd());
    random_delay = std::uniform_int_distribution<>(0, MAX_RANDOM_DELAY_TICKS);
}

void AutoSavePatch::OnSave(logic_thread* logicThread, void* saveFile) {
    updateNextSaveTick(logicThread, 100);
}

void AutoSavePatch::OnAfterGameInit(logic_thread* logicThread) {
    auto gameSettings = s3_get_game_settings();

    // Auto-Save is always enabled for the host when the launcher is used. However, if playing with Vanilla clients, we fallback to the
    // m_enabled setting that is coming from the launch options/configuration.
    bool isEnabled = *m_isVanillaVersion ? m_enabled : gameSettings->isHost;
    if (!isEnabled) {
        isActive = false;
        return;
    }

    isActive = gameSettings->isMultiplayerGame;

    // We do a first save right after the game is initialized.
    if (!gameSettings->isSaveGame) {
        updateNextSaveTick(logicThread);
    } else {
        updateNextSaveTick(logicThread, AUTO_SAVE_INTERVAL_TICKS);
    }
    lastKnownSaveOnTick = logicThread->lastSavedAtGameTick;

    str_struct_wchar msg;
    if (isActive) {
        s3_str_init(&msg, utf8ToWindows(m_i18n->__("auto_save_on", "Auto-Save: ON")).c_str());
    } else {
        s3_str_init(&msg, utf8ToWindows(m_i18n->__("auto_unavailable_in_single_player", "Auto-Save: Unavailable in single player mode.")).c_str());
    }

    s3_add_chat_message(logicThread, &msg, s3_get_player_color(logicThread->gameData->actingPlayer));
}

void AutoSavePatch::OnBeforeGameLoop(logic_thread* logicThread) {
    if (!isActive) {
        return;
    }

    int currentGameTick = logicThread->currentGameTick;
    if (nextSaveOnTick == 0) {
        OnAfterGameInit(logicThread);
    } else if (lastKnownSaveOnTick != logicThread->lastSavedAtGameTick) {
        // We keep this here in case the game resets the game tick at some point if it gets too large.
        // Then, we will just trigger a save once the roll over happens.
        if (lastKnownSaveOnTick > logicThread->lastSavedAtGameTick) {
            updateNextSaveTick(logicThread, 10);
        } else {
            updateNextSaveTick(logicThread, AUTO_SAVE_INTERVAL_TICKS);
        }

        lastKnownSaveOnTick = logicThread->lastSavedAtGameTick;
    }

    if (nextSaveOnTick <= currentGameTick || currentGameTick < logicThread->lastSavedAtGameTick ||
        nextSaveOnTick - currentGameTick > 5000) {
        net_event_trigger_save triggerSaveEvent;
        triggerSaveEvent.eventType = -1503;
        triggerSaveEvent.eventSize = 8;
        triggerSaveEvent.actingPlayer = logicThread->gameData->actingPlayer;

        s3_queue_event(logicThread->netToQueue, &triggerSaveEvent, 2);
        updateNextSaveTick(logicThread, AUTO_SAVE_INTERVAL_TICKS);
    }
}

void AutoSavePatch::updateNextSaveTick(logic_thread* logicThread, int baseTick) {
    auto randomDelay = random_delay(gen);
    nextSaveOnTick = logicThread->currentGameTick + baseTick + randomDelay;
}
