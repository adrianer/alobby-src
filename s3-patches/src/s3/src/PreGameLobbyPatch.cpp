#include <string>
#include <hacklib/ConsoleEx.h>
#include <debug_console.h>
#include "PreGameLobbyPatch.h"
#include <imgui.h>
#include <imgui_internal.h>
#include <imgui_utils.h>
#include <asmjit/x86.h>

using namespace asmjit;

//#define DEBUG_PREGAME_LOBBY 1

typedef struct {
    BYTE gap1[0xC4];
    menu_shared_data* menuSharedData;
    BYTE gap2[0xACC];
    DWORD gameTypeText;
} game_launch_dialog;

typedef void (__thiscall *ON_JOIN_CALLBACK_PROC)(game_launch_dialog* launchDialog, int a2);
ON_JOIN_CALLBACK_PROC s3_pregame_lobby_update = reinterpret_cast<ON_JOIN_CALLBACK_PROC>(Constants::S3_EXE + 0x8E180);

typedef signed int (__thiscall *NET_SESSION_UPDATE_PROC)(net_session* netSession);
NET_SESSION_UPDATE_PROC s3_net_session_update = reinterpret_cast<NET_SESSION_UPDATE_PROC>(Constants::S3_EXE + 0x70E10);

typedef void (__thiscall *DPLAY_SEND_PROC)(void *sendingPlayer, void *targetPlayer, int messageType, BYTE* data, int dataSize);
DPLAY_SEND_PROC s3_dplay_send = reinterpret_cast<DPLAY_SEND_PROC>(Constants::S3_EXE + 0x1C13F0);

#define S3_GAME_DATA_SIZE 315

// Whether to render a single option for random balancing. If defined, it combines the map and other balancing changes into one option.
static bool useSingleRandomBalancingOption = true;

static PreGameLobbyPatch* g_activePatch = nullptr;

// For sending net messages
static bool g_hasOriginalSettings = false;
static BYTE g_gameSettingsData[1024];

// For receiving net messages
static bool g_hasReceivedNetMessage = false;
static BYTE* g_receivedNetMessage; // points to local instance variable (only valid during method invocation)
static HANDLE g_syncNetTo_hEvent = nullptr;

static std::string g_randomHelpText("This improves all but the Roman race on Random maps.\n\nAll Races:\n- build time for small towers equalized (or almost)\n- time for reaching level 3 about equal for all races\n\nAsia:\n- some more wood and tools at start\n\nEgypt:\n- some more tools at start\n\nAmazon:\n- some more wood and tools at start");
static std::string g_thievesHelpText("Thieves cannot pick up goods anymore if in range of a tower manned with bows.\n\nAlso, thieves can now pick up goods in own territory (fixing an inconsistency in game code and providing more micro options).");
static std::string g_sameRandomRacesHelpText("Random races will be assigned in a way to ensure that each team has the same races.\n\nFor example, each team has 2 Egypt players and one Asian player, or each team has 3 Egypt players and so on.");
static std::string g_optimizeStartLocationsText("This optimizes start locations to better utilize the entire map and provide every player/team with spots that offer similar opportunities.\n\nIncreases initial game start time for a few seconds.");
static std::string g_increasedResourceYieldText("This increases the yield of resources on the map (affects mountain, fish, and stone).\n\nMountain changes mainly focus on allowing more soldier production - fighting power is mostly unaffected.");
static std::string g_increaseTransportLimitHelpText("This increases the settler transport limit considerably.\n\nFor practical purposes, you will be able to run as many buildings as you want in a single economic sector.");

void __fastcall pregame_lobby_update(game_launch_dialog* launchDialog, void* notUsed, int a2) {
    s3_pregame_lobby_update(launchDialog, a2);

    str_struct emptyText;
    if (g_activePatch && !g_activePatch->IsGuiActive()) {
        auto i18n = g_activePatch->GetI18n();
        PatchGameSettings gameSettings = g_activePatch->GetGameSettings();
        std::string rulesText(i18n->__("rules", "Rules"));
        rulesText.append(": ");
        rulesText.append(gameSettings.GetDescription(i18n, s3_is_random_game(), g_activePatch->GetRandomMirrorType() == 0 && !useSingleRandomBalancingOption));
        s3_str_init(&emptyText, utf8ToWindows(rulesText).c_str());
    } else {
        s3_str_init(&emptyText, "");
    }

    s3_set_static_text_value(reinterpret_cast<void*>(&launchDialog->gameTypeText), &emptyText);
}

signed int __fastcall net_session_update(net_session* netSession) {
    g_hasReceivedNetMessage = false;
    auto result = s3_net_session_update(netSession);
    g_activePatch->OnNetSessionUpdate(netSession);

    return result;
}

void __fastcall dplay_send(void *sendingPlayer, void *notUsed, void *targetPlayer, int a3, BYTE* data, int dataSize) {
    // We store the original game settings so that we do not have to re-create them by hand when we add our data at the end.
    if (data[0] == 25 && dataSize == S3_GAME_DATA_SIZE) {
        memset(g_gameSettingsData, 0, sizeof(g_gameSettingsData));
        memcpy(g_gameSettingsData, data, S3_GAME_DATA_SIZE);
        g_hasOriginalSettings = true;

        // send our custom game settings too
        g_activePatch->SendGameSettings(sendingPlayer, targetPlayer);
    }

    dprintf("Sending message (type=%d)\n", data[0]);
    s3_dplay_send(sendingPlayer, targetPlayer, a3, data, dataSize);
}

void on_before_message_processing(hl::CpuContext* cpuContext) {
    g_hasReceivedNetMessage = true;
    g_receivedNetMessage = reinterpret_cast<BYTE*>(cpuContext->ESP + 88);
}

void on_after_message_processing(hl::CpuContext* cpuContext) {
    if (g_hasReceivedNetMessage) {
        if (g_receivedNetMessage[0] == 30) {
            PatchGameSettings newSettings(0);
            memcpy(&newSettings, &g_receivedNetMessage[1], sizeof(PatchGameSettings));
            g_activePatch->OnNewGameSettings(&newSettings);
        }
    }
}

void PreGameLobbyPatch::enable() {
    if (g_activePatch) {
        g_activePatch->disable();
    }
    g_activePatch = this;

    m_hooker.hookDetour(Constants::S3_EXE + 0x70E61, 9, on_before_message_processing);
    m_hooker.hookDetour(Constants::S3_EXE + 0x7138B, 6, on_after_message_processing);

    m_onJoinHook.hook(reinterpret_cast<void**>(&s3_pregame_lobby_update),
                      reinterpret_cast<void*>(pregame_lobby_update));
    m_netSessionHook.hook(reinterpret_cast<void**>(&s3_net_session_update), reinterpret_cast<void*>(net_session_update));
    m_dplaySendHook.hook(reinterpret_cast<void**>(&s3_dplay_send), reinterpret_cast<void*>(dplay_send));

    patchGameTypeText();
}

void PreGameLobbyPatch::OnNewGameSettings(PatchGameSettings* newGameSettings) {
    m_gameSettingsManager->Update(*newGameSettings);
}

void PreGameLobbyPatch::SendGameSettings(void* sendingPlayer, void* targetPlayer) {
    BYTE settingsMessage[sizeof(PatchGameSettings) + 1];
    settingsMessage[0] = 30;
    static_assert(sizeof(settingsMessage) <= 1024, "settings cannot be greater than 1024 bytes");

    auto settings = m_gameSettingsManager->getGameSettings();
    memcpy(&settingsMessage[1], &settings, sizeof(PatchGameSettings));
    s3_dplay_send(sendingPlayer, targetPlayer, 1, settingsMessage, sizeof(settingsMessage));
}

void PreGameLobbyPatch::disable() {
    m_hooker.revert();
    m_onJoinHook.revert();
    m_gameTypeTextPatch.revert();

    if (g_activePatch == this) {
        g_activePatch = nullptr;
    }
}

void PreGameLobbyPatch::OnAfterMenuDrawLoop(lp_menu_draw_thread* menuDrawThread) {
    if (menuDrawThread->activeDialogId != PRE_GAME_LOBBY_DIALOG_ID) {
        g_hasOriginalSettings = false;
        m_isJoined = false;

        return;
    }

    auto settings = s3_get_game_settings();
    if (settings->isSaveGame) {
        return;
    }

    bool isJoined = settings->ownPlayerIndex >= 0 && settings->ownPlayerIndex < 20;
    if (isJoined && !m_isJoined) {
        typedef struct {
            BYTE gap[0x8];
            HANDLE hEvent;
        } eventContainer;

        auto netToEvt = reinterpret_cast<eventContainer*>(menuDrawThread->menuSharedData->syncEvent_netTo);
        g_syncNetTo_hEvent = netToEvt->hEvent;

        typedef struct random_info_chunk : file_chunk_header {
            DWORD mapSize;
            BYTE gap_1[4];
            DWORD mirrorType;
            DWORD seedValue;
        } random_info_chunk;

        auto mapFile = s3_get_game_settings()->mapFile;
        auto chunk = reinterpret_cast<random_info_chunk*>(s3_file_chunk_read(mapFile, 5));
        if (chunk) {
            m_mirrorType = chunk->mirrorType;
        } else {
            m_mirrorType = -1;
        }
        s3_file_chunk_free(mapFile, 5);

        if (settings->isHost && !s3_is_random_game()) {
            auto nonRandomSettings = m_gameSettingsManager->getGameSettings();
            nonRandomSettings.setUseRevisedRandomBalancing(false);
            nonRandomSettings.setIncreaseRandomResourceYields(false);
            m_gameSettingsManager->Update(nonRandomSettings);
        }
    }
    m_isJoined = isJoined;
}

void PreGameLobbyPatch::OnRenderFrame(RenderContext* context) {
    if (context->activeDialogId != PRE_GAME_LOBBY_DIALOG_ID) {
        return;
    }

    if (!m_config->isClassicOnly()) {
        renderSettingsActions();
    }
    renderSettingsDescription();
}

void PreGameLobbyPatch::renderSettingsActions() {
    if (!m_isJoined) {
        return;
    }

    auto s3Settings = s3_get_game_settings();
    if (!s3Settings->isSaveGame) {
        if (ImGui::Begin("#gameSettingButtonContainer", NULL, ImGuiWindowFlags_NoBackground | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoSavedSettings)) {
            auto windowSize = ImGui::GetWindowSize();
            auto targetRight = ImScaledVec2(780, 540);

            ImGui::SetWindowPos(ImVec2(targetRight.x - windowSize.x, targetRight.y - windowSize.y));

            renderChangeSettingsPopup();
        }
        ImGui::End();
    }
}

void PreGameLobbyPatch::renderChangeSettingsPopup() {
    auto s3Settings = s3_get_game_settings();
    if (!s3Settings->isHost) {
        return;
    }

    if (ImGui::Button(m_i18n->__("change_settings", "Change Settings").c_str())) {
        ImGui::OpenPopup("settings", ImGuiPopupFlags_AnyPopup);
    }

    if (ImGui::BeginPopup("settings", ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove)) {
        auto gameSettings = m_gameSettingsManager->getGameSettings();

        bool hasChanged = false;
        if (s3_is_random_game()) {
            bool useRandomBalancing = gameSettings.useRevisedRandomBalancing();
            if (ImGui::Checkbox(m_i18n->__("random_balancing_2_0", "Random Balancing 2.0").c_str(), &useRandomBalancing)) {
                gameSettings.setUseRevisedRandomBalancing(useRandomBalancing);
                if (useSingleRandomBalancingOption) {
                    gameSettings.setOptimizeRandomStartLocations(useRandomBalancing);
                }
                hasChanged = true;
            }

            ImGui::SameLine();
            ImGuiWidgets::HelpMarker(m_i18n->__("new_random_balancing_help_text", g_randomHelpText).c_str());

            if (!useSingleRandomBalancingOption && m_mirrorType == 0) {
                bool optimizeStartLocations = gameSettings.shouldOptimizeRandomStartLocations();
                if (ImGui::Checkbox(m_i18n->__("optimize_start_locations", "Optimize Start Locations").c_str(), &optimizeStartLocations)) {
                    gameSettings.setOptimizeRandomStartLocations(optimizeStartLocations);
                    hasChanged = true;
                }

                ImGui::SameLine();
                ImGuiWidgets::HelpMarker(m_i18n->__("optimized_start_locations_help_text", g_optimizeStartLocationsText).c_str());
            }

            bool increaseResourceYields = gameSettings.shouldIncreaseRandomResourceYield();
            if (ImGui::Checkbox(m_i18n->__("more_resources", "More Resources").c_str(), &increaseResourceYields)) {
                gameSettings.setIncreaseRandomResourceYields(increaseResourceYields);
                hasChanged = true;
            }
            ImGui::SameLine();
            ImGuiWidgets::HelpMarker(m_i18n->__("more_resources_help_text", g_increasedResourceYieldText).c_str());
        }

        bool sameRacesPerTeam = gameSettings.useSameRacesPerTeam();
        if (ImGui::Checkbox(m_i18n->__("same_random_races_per_team", "Same Random Races per Team").c_str(), &sameRacesPerTeam)) {
            gameSettings.setUseSameRacesPerTeam(sameRacesPerTeam);
            hasChanged = true;
        }
        ImGui::SameLine();
        ImGuiWidgets::HelpMarker(m_i18n->__("same_random_races_per_team_help_text", g_sameRandomRacesHelpText).c_str());

        bool warMachineDisabled = gameSettings.isWarMachineDisabled();
        if (ImGui::Checkbox(m_i18n->__("disable_war_machines", "Disable War Machines").c_str(), &warMachineDisabled)) {
            gameSettings.setWarMachineDisabled(warMachineDisabled);
            hasChanged = true;
        }

        bool useRevisedThief = gameSettings.useRevisedThief();
        if (ImGui::Checkbox(m_i18n->__("revised_thieves", "Revised Thieves").c_str(), &useRevisedThief)) {
            gameSettings.setUseRevisedThief(useRevisedThief);
            hasChanged = true;
        }
        ImGui::SameLine();
        ImGuiWidgets::HelpMarker(m_i18n->__("new_thieves_help_text", g_thievesHelpText).c_str());

        if (!useRevisedThief) {
            ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
            ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
        }
        bool disallowStealing = useRevisedThief && gameSettings.isStealingDisallowed();
        if (ImGui::Checkbox(m_i18n->__("disallow_stealing", "Disallow Stealing").c_str(), &disallowStealing)) {
            gameSettings.setStealingDisabled(disallowStealing);
            hasChanged = true;
        }
        if (!useRevisedThief) {
            ImGui::PopItemFlag();
            ImGui::PopStyleVar();
        }

        bool increaseTransportLimit = gameSettings.useIncreasedTransportLimit();
        if (ImGui::Checkbox(m_i18n->__("increase_transport_limit", "Increase Transport Limit").c_str(), &increaseTransportLimit)) {
            gameSettings.setIncreaseTransportLimit(increaseTransportLimit);
            hasChanged = true;
        }
        ImGui::SameLine();
        ImGuiWidgets::HelpMarker(m_i18n->__("increase_transport_limit_help_text", g_increaseTransportLimitHelpText).c_str());

        if (hasChanged) {
            m_config->updateDefaultSettings(gameSettings);
            m_gameSettingsManager->Update(gameSettings);
            m_sendGameSettings = true;
            if (g_syncNetTo_hEvent) {
                if (!PulseEvent(g_syncNetTo_hEvent)) {
                    dprintf("failed to signal menu net thread\n");
                }
            }
        }

        ImGui::EndPopup();
    }
}

void PreGameLobbyPatch::renderSettingsDescription() {
    if (!m_isJoined) {
        return;
    }

    ImGui::SetNextWindowPos(ImScaledVec2(18, 513), ImGuiCond_Always);
    if (ImGui::Begin("#gameTypeDescription", NULL, ImGuiWindowFlags_NoBackground | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings)) {
        auto gameSettings = m_gameSettingsManager->getGameSettings();
        bool isRandom = s3_is_random_game();
        bool showRandomDescription = isRandom && gameSettings.useRevisedRandomBalancing();
        bool showIncreasedResourceYields = isRandom && gameSettings.shouldIncreaseRandomResourceYield();
        bool increaseTransportLimit = gameSettings.useIncreasedTransportLimit();
        bool showOptimizeDescription = isRandom && !useSingleRandomBalancingOption && gameSettings.shouldOptimizeRandomStartLocations() && m_mirrorType == 0;

        std::string rulesText(m_i18n->__("rules", "Rules"));
        rulesText.append(": ");
        rulesText.append(gameSettings.GetDescription(m_i18n, isRandom, showOptimizeDescription));

        ImGui::PushStyleColor(ImGuiCol_Text, S3_YELLOW);
        ImGui::Text("%s", rulesText.c_str());
        ImGui::PopStyleColor();

        if (showRandomDescription || showOptimizeDescription || showIncreasedResourceYields || gameSettings.useSameRacesPerTeam() || gameSettings.useRevisedThief() || increaseTransportLimit) {
            ImGui::SameLine();
            ImGui::TextDisabled("(?)");
            if (ImGui::IsItemHovered()) {
                ImGui::BeginTooltip();
                ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);

                if (showRandomDescription && ImGui::CollapsingHeader(m_i18n->__("new_random_balancing", "New Random Balancing").c_str(), ImGuiTreeNodeFlags_DefaultOpen)) {
                    ImGui::TextUnformatted(m_i18n->__("new_random_balancing_help_text", g_randomHelpText).c_str());
                }
                if (gameSettings.useSameRacesPerTeam() && ImGui::CollapsingHeader(m_i18n->__("same_random_races_per_team", "Same Random Races per Team").c_str(), ImGuiTreeNodeFlags_DefaultOpen)) {
                    ImGui::TextUnformatted(m_i18n->__("same_random_races_per_team_help_text", g_sameRandomRacesHelpText).c_str());
                }
                if (gameSettings.useRevisedThief() && ImGui::CollapsingHeader(m_i18n->__("new_thieves", "New Thieves").c_str(), ImGuiTreeNodeFlags_DefaultOpen)) {
                    ImGui::TextUnformatted(m_i18n->__("new_thieves_help_text", g_thievesHelpText).c_str());
                }
                if (showOptimizeDescription && ImGui::CollapsingHeader(m_i18n->__("optimized_start_locations", "Optimized Start Locations").c_str(), ImGuiTreeNodeFlags_DefaultOpen)) {
                    ImGui::TextUnformatted(m_i18n->__("optimized_start_locations_help_text", g_optimizeStartLocationsText).c_str());
                }
                if (showIncreasedResourceYields && ImGui::CollapsingHeader(m_i18n->__("more_resources", "More Resources").c_str(), ImGuiTreeNodeFlags_DefaultOpen)) {
                    ImGui::TextUnformatted(m_i18n->__("more_resources_help_text", g_increasedResourceYieldText).c_str());
                }
                if (increaseTransportLimit && ImGui::CollapsingHeader(m_i18n->__("increased_transport_limit", "Increased Transport Limit").c_str(), ImGuiTreeNodeFlags_DefaultOpen)) {
                    ImGui::TextUnformatted(m_i18n->__("increase_transport_limit_help_text", g_increaseTransportLimitHelpText).c_str());
                }

                ImGui::PopTextWrapPos();
                ImGui::EndTooltip();
            }
        }
    }
    ImGui::End();
}

void PreGameLobbyPatch::OnNetSessionUpdate(net_session* netSession) {
    if (m_sendGameSettings.exchange(false)) {
        SendGameSettings(netSession->localPlayer, NULL);
    }
}

void __declspec(naked) setup_call_args() {
    __asm {
        pop eax
        push 36
        push 590185
        push 135
        push 180     // width (changed)
        push 129
        push eax
        ret
    }
}

void PreGameLobbyPatch::patchGameTypeText() {
    Environment env;
    env.setArch(Environment::kArchX86);

    uintptr_t patchPos = Constants::S3_EXE + 0x8BEC9;
    uintptr_t endPos = Constants::S3_EXE + 0x8BEDC;
    CodeHolder code;
    code.init(env, patchPos);
    x86::Assembler a(&code);
    a.call(&setup_call_args);
    while (code.textSection()->bufferSize() < endPos - patchPos) {
        a.nop();
    }

    CodeBuffer& buf = code.textSection()->buffer();
    m_gameTypeTextPatch.apply(patchPos, (char*)buf.data(), buf.size());
}
