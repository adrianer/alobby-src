#include <debug_console.h>
#include <hacklib/MessageBox.h>
#include "MachineCodePatchBuilder.h"
#include "DisableAiPatch.h"

typedef struct disable_ai_patch_chunk : file_chunk_header {
    bool isAiPresentAtGameStart;
} disable_ai_patch_chunk;

void DisableAiPatch::enable() {
    disableAi();
}

void DisableAiPatch::disable() {
    m_patch.revert();
}

void DisableAiPatch::OnAfterGameInit(logic_thread* logicThread) {
    if (!s3_get_game_settings()->isSaveGame) {
        m_isAiPresentAtGameStart = hasGameAi(logicThread->gameData);
    }

    if (*m_isVanillaVersion || m_isAiPresentAtGameStart) {
        m_patch.revert();
    } else {
        disableAi();
    }
}

void DisableAiPatch::disableAi() {
    MachineCodePatchBuilder builder(Constants::S3_EXE + 0x13E611);
    builder.a->jmp((uint64_t*)(Constants::S3_EXE + 0x13E618));
    builder.apply(&m_patch);
}

bool DisableAiPatch::hasGameAi(game_data* gameData) {
    for (int i=0; i<20; i++) {
        if (gameData->players[i].type == PLAYER_TYPE_COMP) {
            return true;
        }
    }

    return false;
}

void DisableAiPatch::OnSave(logic_thread* logicThread, void* saveFile) {
    disable_ai_patch_chunk chunk;
    chunk.type = FILE_CHUNK_DISABLE_AI_PATCH;
    chunk.metaInfo = 0;
    chunk.size = sizeof(disable_ai_patch_chunk);
    chunk.isAiPresentAtGameStart = m_isAiPresentAtGameStart;
    s3_file_chunk_write(saveFile, chunk.type, &chunk);
}

void DisableAiPatch::OnLoadSave(logic_thread* logicThread, void* saveFile) {
    auto chunk = static_cast<disable_ai_patch_chunk*>(s3_file_chunk_read(saveFile, FILE_CHUNK_DISABLE_AI_PATCH));
    if (!chunk) {
        m_isAiPresentAtGameStart = true;
    } else {
        m_isAiPresentAtGameStart = chunk->isAiPresentAtGameStart;
    }

    s3_file_chunk_free(saveFile, FILE_CHUNK_DISABLE_AI_PATCH);
}
