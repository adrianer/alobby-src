#include "FreezeFix.h"

#include <asmjit/x86.h>
#include <debug_console.h>
#include "s3.h"

using namespace asmjit;

uintptr_t ifConditionPos = 0x004EB7C1;
uintptr_t ifFreezeStart = 0x004F922F;
uintptr_t ifNoFreezeStart = 0x004EB7C7;

void FreezeFix::triggerFreezeCondition() {
    Environment env;
    env.setArch(Environment::kArchX86);
    CodeHolder code;
    code.init(env, ifConditionPos);
    x86::Assembler a(&code);
    a.jmp((uint64_t*)ifFreezeStart);
    a.nop();

    triggerFreezePatch.apply(ifConditionPos, (char*)code.textSection()->data(), code.textSection()->bufferSize());
}

static void showFreezeWarning(game_data* gameData, int settlerIndex, int xCoord, int yCoord) {
    static int lastFreezeWarningGameTick = -1;
    static int lastFreezeWarningBuilding = -1;

    auto logicThread = s3_get_app()->logicThread;
    int buildingIndex = gameData->settlerData[settlerIndex].buildingIndex;
    auto diff = logicThread->currentGameTick - lastFreezeWarningGameTick;

    if (diff > 0 && diff < 240) { // Less than about 15 seconds in-game time
        return;
    }
    lastFreezeWarningGameTick = logicThread->currentGameTick;

    std::string message;
    if (lastFreezeWarningBuilding == buildingIndex) {
        message.append("Still ");
    }
    message.append("Digger Trouble (f/k/a FREEZE) at ");
    message.append(s3_get_building_name(gameData->buildingData[buildingIndex].type));
    message.append(" - crush its construction site");

    str_struct_wchar msg;
    s3_str_init(&msg, message.c_str());
    s3_add_chat_message(logicThread, &msg, s3_get_player_color(gameData->settlerData[settlerIndex].owningPlayer));
    lastFreezeWarningBuilding = buildingIndex;
}

void FreezeFix::applyPatch() {
    Environment env;
    env.setArch(Environment::kArchX86);
    CodeHolder code;
    code.init(env, ifFreezeStart);
    x86::Assembler a(&code);

    // Needed by the if-false branch, and we can also use them as arguments in our function.
    a.push(x86::edx); // y coord
    a.push(x86::ecx); // x coord
    a.push(x86::ebp); // settler index
    a.push(x86::esi); // game data

    a.call((uint64_t*)&showFreezeWarning);

    a.pop(x86::esi);
    a.pop(x86::ebp);
    a.pop(x86::ecx);
    a.pop(x86::edx);
    a.jmp((uint64_t*)ifNoFreezeStart);

    patch.apply(ifFreezeStart, (char*)code.textSection()->data(), code.textSection()->bufferSize());
}

void FreezeFix::enable() {
    applyPatch();

    // For debugging purposes only.
//    triggerFreezeCondition();
}

void FreezeFix::disable() {
    patch.revert();
    triggerFreezePatch.revert();
}
