#include "QuitSeFix.h"

#include <asmjit/x86.h>
#include <debug_console.h>
#include <constants.h>

using namespace asmjit;

void __declspec(naked) safety_check_x_coordinate() {
    __asm {
        lea     ebp, [eax+ecx]
        mov     [esp+40], ebp
        and     ebp, 0FFFFh
        cmp     ebp, 02FFh
        jle     RETURN
        and     ebp, 02FFh

        RETURN:
            ret
    }
}

void __declspec(naked) safety_check_y_coordinate() {
    __asm {
        lea     eax, [ebx+edi]
        mov     [esp+44], eax
        and     eax, 0FFFFh
        cmp     eax, 02FFh
        jle     RETURN
        and     eax, 02FFh

        RETURN:
            ret
    }
}

void QuitSeFix::enable() {
    applyPatchXCoordinate();
    applyPatchYCoordinate();
}

void QuitSeFix::applyPatchXCoordinate() {
    uintptr_t pos = Constants::S3_EXE + 0x154D9E;
    Environment env;
    env.setArch(Environment::kArchX86);
    CodeHolder code;
    code.init(env, pos);
    x86::Assembler a(&code);
    a.call(&safety_check_x_coordinate);
    while (code.textSection()->bufferSize() < 13) {
        a.nop();
    }

    patchXCoordinate.apply(pos, (char*)code.textSection()->data(), code.textSection()->bufferSize());
}

void QuitSeFix::applyPatchYCoordinate()
{
    uintptr_t pos = Constants::S3_EXE + 0x154DAB;
    Environment env;
    env.setArch(Environment::kArchX86);
    CodeHolder code;
    code.init(env, pos);
    x86::Assembler a(&code);
    a.call(&safety_check_y_coordinate);
    while (code.textSection()->bufferSize() < 12) {
        a.nop();
    }

    patchYCoordinate.apply(pos, (char*)code.textSection()->data(), code.textSection()->bufferSize());
}
void QuitSeFix::disable() {
    patchXCoordinate.revert();
    patchYCoordinate.revert();
}
