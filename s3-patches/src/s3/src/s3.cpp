#include <debug_console.h>
#include <set>
#include <string>
#include "s3.h"
#include "constants.h"

#define MAX_TEXT_LENGTH 0xFFFFFFFF

typedef str_struct* (__thiscall *S3_STR_INIT_PROC)(str_struct *This, const char *textValue, unsigned int maxTextCapacity);
S3_STR_INIT_PROC real_s3_str_init = reinterpret_cast<S3_STR_INIT_PROC>(Constants::S3_EXE + 0x1A10);

typedef void* (__fastcall *S3_STR_INIT_WCHAR_PROC)(str_struct_wchar* targetWStr, str_struct* sourceCStr);
S3_STR_INIT_WCHAR_PROC real_s3_str_init_wchar = reinterpret_cast<S3_STR_INIT_WCHAR_PROC>(Constants::S3_EXE + 0x20B840);

typedef void (__thiscall *S3_QUEUE_EVENT_PROC)(void *This, void *eventPointer, int eventSize, char arg4);
S3_QUEUE_EVENT_PROC real_s3_queue_event = reinterpret_cast<S3_QUEUE_EVENT_PROC>(Constants::S3_EXE + 0x309C0);

typedef void (__thiscall *S3_ADD_CHAT_MESSAGE_PROC)(logic_thread *This, str_struct_wchar message, short playerColor);
S3_ADD_CHAT_MESSAGE_PROC real_s3_add_chat_message = reinterpret_cast<S3_ADD_CHAT_MESSAGE_PROC>(Constants::S3_EXE + 0x11E440);

typedef void (__thiscall *S3_WSTR_COPY_PROC)(str_struct_wchar *targetWstr, str_struct_wchar* sourceWstr, unsigned int startPos, unsigned int endPos);
S3_WSTR_COPY_PROC real_s3_str_copy = reinterpret_cast<S3_WSTR_COPY_PROC>(Constants::S3_EXE + 0x28400);

typedef void (__thiscall *S3_WSTR_CLEAR_PROC)(str_struct_wchar *This, char deallocateWchar);
S3_WSTR_CLEAR_PROC  real_s3_str_clear = reinterpret_cast<S3_WSTR_CLEAR_PROC>(Constants::S3_EXE + 0x286D0);

typedef tagRECT* (__thiscall *DRAW_TEXT_PROC)(CTextSurface *textSurface, tagRECT *drawnTextRect, char fontId, tagRECT *targetRect, char drawMode, str_struct *text);
DRAW_TEXT_PROC real_s3_draw_text = reinterpret_cast<DRAW_TEXT_PROC>(Constants::S3_EXE + 0xA8090);

typedef void (__thiscall *COUNT_TOWERS_PROC)(logic_thread *thisLogicThread, int numberOfTowersGroupedByPlayer[20]);
COUNT_TOWERS_PROC real_s3_count_towers = reinterpret_cast<COUNT_TOWERS_PROC>(Constants::S3_EXE + 0x114A00);

typedef settings_player* (__thiscall *FIND_SETTINGS_PLAYER_PROC)(global_game_settings *gameSettings, int index);
FIND_SETTINGS_PLAYER_PROC real_s3_find_player_in_game_settings =
    reinterpret_cast<FIND_SETTINGS_PLAYER_PROC>(Constants::S3_EXE + 0x35390);

typedef settings_player* (__thiscall *GET_OR_CREATE_SETTINGS_PLAYER_PROC)(global_game_settings *gameSettings, int index);
GET_OR_CREATE_SETTINGS_PLAYER_PROC real_s3_get_or_create_player_in_game_settings =
    reinterpret_cast<GET_OR_CREATE_SETTINGS_PLAYER_PROC>(Constants::S3_EXE + 0x351E0);

typedef void (__thiscall *SHOW_ERROR_MESSAGE_PROC)(lp_menu_draw_thread *This, str_struct *messageText, int buttonBitmask);
SHOW_ERROR_MESSAGE_PROC real_s3_show_message_box = reinterpret_cast<SHOW_ERROR_MESSAGE_PROC>(Constants::S3_EXE + 0x96D50);

typedef char (__thiscall *UPDATE_SCREEN_PROC)(lp_menu_draw_thread *This, int notUsed2, int notUsed3, int notUsed4, int dialogId, int notUsed5);
UPDATE_SCREEN_PROC real_s3_update_menu_screen = reinterpret_cast<UPDATE_SCREEN_PROC>(Constants::S3_EXE + 0x94990);

typedef void (__thiscall *SET_DROPDOWN_VALUE_PROC)(void* dropdown, int value);
SET_DROPDOWN_VALUE_PROC real_s3_set_dropdown_value = reinterpret_cast<SET_DROPDOWN_VALUE_PROC>(Constants::S3_EXE + 0x22120);

typedef void (__thiscall *SET_STATIC_TEXT_VALUE_PROC)(void *staticText, str_struct newValue);
SET_STATIC_TEXT_VALUE_PROC real_s3_set_static_text_value = reinterpret_cast<SET_STATIC_TEXT_VALUE_PROC>(Constants::S3_EXE + 0x1C810);

typedef void (__thiscall *WRITE_FILE_CHUNK_WITH_DATA_PROC)(void *This, unsigned short chunkType, int a3, void *chunkHeader, unsigned int additionalDataSourceSize, const void *additionalDataSource);
WRITE_FILE_CHUNK_WITH_DATA_PROC real_s3_write_file_chunk = reinterpret_cast<WRITE_FILE_CHUNK_WITH_DATA_PROC>(Constants::S3_EXE + 0xA6600);

typedef void (__thiscall *WRITE_FILE_CHUNK_PROC)(void *This, unsigned __int8 chunkType, int unusedParam, void *saveData);
WRITE_FILE_CHUNK_PROC real_s3_write_file_chunk_without_data = reinterpret_cast<WRITE_FILE_CHUNK_PROC>(Constants::S3_EXE + 0xA6540);

typedef file_chunk_header* (__thiscall *READ_FILE_CHUNK_PROC)(void *This, unsigned short searchedChunkType, unsigned int notUsed);
READ_FILE_CHUNK_PROC real_s3_read_file_chunk = reinterpret_cast<READ_FILE_CHUNK_PROC>(Constants::S3_EXE + 0xA60F0);

typedef void (__thiscall *CLEAR_FILE_CHUNK_PROC)(void *This, unsigned short chunkType, unsigned __int16 arg);
CLEAR_FILE_CHUNK_PROC real_s3_clear_file_chunk = reinterpret_cast<CLEAR_FILE_CHUNK_PROC>(Constants::S3_EXE + 0xA6310);

typedef s3_application* (*GET_APP_PROC)();
GET_APP_PROC real_s3_get_app = reinterpret_cast<GET_APP_PROC>(Constants::S3_EXE + 0x1B8C00);

typedef unsigned __int16 (__thiscall *GET_NEXT_RANDOM_VALUE_PROC)(game_data* This);
GET_NEXT_RANDOM_VALUE_PROC real_s3_get_next_value = reinterpret_cast<GET_NEXT_RANDOM_VALUE_PROC>(Constants::S3_EXE + 0xFEB70);

typedef unsigned int (__stdcall *CALCULATE_DISTANCE_PROC)(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2);
CALCULATE_DISTANCE_PROC real_calculate_distance = reinterpret_cast<CALCULATE_DISTANCE_PROC>(Constants::S3_EXE + 0xFF320);

typedef int (__thiscall *GET_SELECTED_LIST_ITEM_VALUE_PROC)(void *list);
GET_SELECTED_LIST_ITEM_VALUE_PROC real_get_selected_list_item_value = reinterpret_cast<GET_SELECTED_LIST_ITEM_VALUE_PROC>(Constants::S3_EXE + 0x1F3A0);

typedef void (__thiscall *ADD_ITEM_PILE_NEAR_PROC)(game_data *This, int itemType, int itemCount, int xCoord, int yCoord);
ADD_ITEM_PILE_NEAR_PROC real_s3_add_item_pile_near = reinterpret_cast<ADD_ITEM_PILE_NEAR_PROC>(Constants::S3_EXE + 0x1046B0);

typedef struct player_colors {
    short colors[20];
} player_colors;
static player_colors* s3PlayerColors = reinterpret_cast<player_colors*>(Constants::S3_EXE + 0x3ACE64);

static global_game_settings* s3GameSettings = reinterpret_cast<global_game_settings*>(Constants::S3_EXE + 0x3A7A18);
static bool* s3EntireMapVisible = reinterpret_cast<bool*>(Constants::S3_EXE + 0x3DFD84);
static global_player_stats* s3PlayerStats = reinterpret_cast<global_player_stats*>(Constants::S3_EXE + 0x3ACFAC);

void s3_str_init(str_struct* s, const char* textValue) {
    real_s3_str_init(s, textValue, 0xFFFFFFFF);
}

void s3_str_init(str_struct_wchar* target, const char* textValue) {
    str_struct tmp;
    s3_str_init(&tmp, textValue);
    s3_str_init(target, &tmp);
}

void s3_str_init(str_struct_wchar* target, str_struct* source) {
    real_s3_str_init_wchar(target, source);
}

void s3_str_copy(str_struct_wchar* target, str_struct_wchar* source) {
    real_s3_str_copy(target, source, 0, MAX_TEXT_LENGTH);
}

void s3_str_clear(str_struct_wchar* s, bool deAllocateWchar) {
    real_s3_str_clear(s, deAllocateWchar);
}

void s3_queue_event(void* queue, void* eventPtr, int eventSize_inNumPackets) {
    real_s3_queue_event(queue, eventPtr, eventSize_inNumPackets, 1);
}

void s3_draw_text(CTextSurface* textSurface, RECT* drawnRect, char fontId, RECT* targetRect, char drawMode, str_struct* text) {
    real_s3_draw_text(textSurface, drawnRect, fontId, targetRect, drawMode, text);
}

int s3_get_current_game_tick() {
    return s3_read_int(0x3DFD48);
}

unsigned int s3_get_display_height() {
    return *(unsigned int*)(Constants::S3_EXE + 0x330F10);
}

unsigned int s3_get_display_size_unit() {
    return *(unsigned int*)(Constants::S3_EXE + 0x330F38);
}

int s3_read_int(int offset) {
    int value;
    if (ReadProcessMemory(GetCurrentProcess(), (LPVOID)(Constants::S3_EXE + offset), &value, sizeof(int), NULL)) {
        return value;
    }

    return 0;
}

short s3_get_player_color(int playerIndex) {
    return s3PlayerColors->colors[playerIndex];
}

void s3_add_chat_message(logic_thread* logicThread, str_struct_wchar* message, short playerColor) {
    real_s3_add_chat_message(logicThread, *message, playerColor);
}

global_game_settings* s3_get_game_settings() {
    return s3GameSettings;
}

settings_player* s3_get_settings_player(int playerSettingsIndex) {
    return real_s3_find_player_in_game_settings(s3GameSettings, playerSettingsIndex);
}

settings_player* s3_get_or_create_settings_player(int playerSettingsIndex) {
    return real_s3_get_or_create_player_in_game_settings(s3GameSettings, playerSettingsIndex);
}

void s3_set_map_visibility(bool isVisible) {
    *s3EntireMapVisible = isVisible;
}

void s3_count_towers_by_player(logic_thread* logicThread, int numTowersByPlayer[20]) {
    real_s3_count_towers(logicThread, numTowersByPlayer);
}

void s3_update_alive_teams(logic_thread* logicThread, std::set<int>* aliveTeams, bool includeCpus) {
    aliveTeams->clear();

    int numTowers[20];
    s3_count_towers_by_player(logicThread, numTowers);

    for (short i=0; i<20; i++) {
        auto player = &logicThread->gameData->players[i];
        if (!player->type) {
            continue;
        }

        if (!includeCpus && player->type != PLAYER_TYPE_HUMAN) {
            continue;
        }

        if (numTowers[i] > 0) {
            aliveTeams->insert(player->team);
        }
    }
}

void s3_show_message_box(lp_menu_draw_thread* menuDrawThread, str_struct* message, int buttonStyle) {
    return real_s3_show_message_box(menuDrawThread, message, buttonStyle);
}

void s3_update_menu_screen(lp_menu_draw_thread* menuDrawThread, int buttonId) {
    real_s3_update_menu_screen(menuDrawThread, 0, 0, 0, buttonId, 0);
}

IDirectDrawSurface* s3_get_dd_surface(CSurface* surface) {
    // This replicates the memory operations the game is performing, but there is likely a simpler way to express all this in C++.
    typedef struct {
        BYTE gap[8];
        IDirectDrawSurface* ddSurface;
    } tmp_struct;

    tmp_struct* tmp = reinterpret_cast<tmp_struct*>((DWORD)surface + *reinterpret_cast<DWORD*>(reinterpret_cast<DWORD>(surface->vtable) + 4));

    return tmp->ddSurface;
}

void s3_set_dropdown_value(void* dropdown, int value) {
    real_s3_set_dropdown_value(dropdown, value);
}

void s3_set_static_text_value(void* staticText, str_struct* value) {
    real_s3_set_static_text_value(staticText, *value);
}

global_player_stats* s3_get_player_stats() {
    return s3PlayerStats;
}

void s3_file_chunk_write(void* file, unsigned short chunkType, file_chunk_header* chunkHeader, unsigned int dataSize, const void* data) {
    real_s3_write_file_chunk(file, chunkType, 0, chunkHeader, dataSize, data);
}

void s3_file_chunk_write(void* file, unsigned short chunkType, file_chunk_header* chunk) {
    real_s3_write_file_chunk_without_data(file, chunkType, 0, chunk);
}

file_chunk_header* s3_file_chunk_read(void* file, unsigned short chunkType) {
    return real_s3_read_file_chunk(file, chunkType, 0);
}

void s3_file_chunk_free(void* file, unsigned short chunkType) {
    return real_s3_clear_file_chunk(file, chunkType, 0);
}

bool s3_is_random_game() {
    if (s3GameSettings->mapName.actualTextLength <= 0) {
        return false;
    }

    return strcmp(s3GameSettings->mapName.sText, "\\Random") == 0;
}

std::string s3_get_building_name(int buildingType) {
    switch (buildingType) {
    case BUILDING_STORAGE:
        return "Storage";
    case BUILDING_WOOD_CUTTER:
        return "Wood Cutter";

    case BUILDING_STONE_CUTTER:
        return "Stone Cutter";

    case BUILDING_SAW_MILL:
        return "Saw Mill";

    case BUILDING_FORESTER:
        return "Forester";

    case BUILDING_WATCH_TOWER:
        return "Watch Tower";

    case BUILDING_COAL_MINE:
        return "Coal Mine";

    case BUILDING_GOLD_MINE:
        return "Gold Mine";

    case BUILDING_IRON_MINE:
        return "Iron Mine";

    case BUILDING_GOLD_SMELTER:
        return "Gold Smelter";

    case BUILDING_IRON_SMELTER:
        return "Iron Smelter";

    case BUILDING_TOOL_SMITH:
        return "Tool Smith";

    case BUILDING_WEAPON_SMITH:
        return "Weapon Smith";

    case BUILDING_WINERY:
        return "Winery";

    case BUILDING_SMALL_TOWER:
        return "Small Tower";

    case BUILDING_BIG_TOWER:
        return "Big Tower";

    case BUILDING_WIND_MILL:
        return "Wind Mill";

    case BUILDING_FORTRESS:
        return "Fortress";

    case BUILDING_BARRACKS:
        return "Barracks";

    case BUILDING_BAKERY:
        return "Bakery";

    case BUILDING_BUTCHER:
        return "Butcher";

    case BUILDING_DISTILLERY:
        return "Distillery";

    case BUILDING_PIG_FARM:
        return "Pig Farm";

    case BUILDING_FARM:
        return "Farm";

    case BUILDING_FISHERY:
        return "Fishery";

    case BUILDING_SMALL_HOUSE:
        return "Small House";

    case BUILDING_MIDDLE_HOUSE:
        return "Middle House";

    case BUILDING_LARGE_HOUSE:
        return "Large House";

    case BUILDING_SULFUR_MINE:
    case BUILDING_SULFUR_MINE_2:
        return "Sulfur Mine";

    case BUILDING_WATERWORKS:
        return "Waterworks";

    case BUILDING_CATAPULT_FACTORY:
        return "Catapult Factory";

    case BUILDING_SHIPYARD:
        return "Shipyard";

    case BUILDING_LANDING_PLACE:
        return "Landing Place";

    case BUILDING_MARKET:
        return "Market";

    case BUILDING_HEALER:
        return "Healer";

    case BUILDING_RICE_FARM:
        return "Rice Farm";

    case BUILDING_GEM_MINE:
        return "Gem Mine";

    case BUILDING_BREWERY:
        return "Brewery";

    case BUILDING_CHARBURNER:
        return "Charburner";

    case BUILDING_GUN_POWDER_MAKER:
        return "Gun Powder Maker";

    case BUILDING_PYRAMID:
        return "Pyramid";

    case BUILDING_SPHINX:
        return "Sphinx";

    case BUILDING_BIG_TEMPLE:
    case BUILDING_BIG_TEMPLE_2:
    case BUILDING_BIG_TEMPLE_3:
        return "Big Temple";

    case BUILDING_SMALL_TEMPLE:
    case BUILDING_SMALL_TEMPLE_2:
    case BUILDING_SMALL_TEMPLE_3:
        return "Small Temple";

    case BUILDING_BALLISTA_FACTORY:
        return "Ballista Factory";

    case BUILDING_CANNON_FACTORY:
        return "Cannon Factory";

    case BUILDING_DONKEY_FARM:
        return "Donkey Farm";

    case BUILDING_GONG_FACTORY:
        return "Gong Factory";

    case BUILDING_APIARY:
        return "Apiary";

    case BUILDING_MEAD_MAKER:
        return "Mead Maker";

    case BUILDING_LAB:
        return "Lab";

    default:
        return "Building-Type " + std::to_string(buildingType);
    }
}

s3_application* s3_get_app() {
    static_assert(offsetof(s3_application, logicThread) == 0x90, "Logic Thread is not at offset 0x90.");

    return real_s3_get_app();
}

std::string s3_get_player_name(int playerIndex) {
    auto app = s3_get_app();
    auto settingsIndex = app->logicThread->gameData->players[playerIndex].playerSettingIndex;
    if (settingsIndex >= 0 && settingsIndex < 20) {
        auto settingsPlayer = s3_get_settings_player(settingsIndex);
        if (settingsPlayer->playerName.actualTextLength > 0) {
            return settingsPlayer->playerName.sText;
        }
    }

    return "";
}

unsigned short s3_next_random_value(game_data* gameData) {
    return real_s3_get_next_value(gameData);
}

unsigned short s3_next_random_value(game_data* gameData, unsigned short maxValue) {
    if (maxValue == 0) {
        return 0;
    }

    unsigned short randomValue = real_s3_get_next_value(gameData);
    if (maxValue < UINT16_MAX) {
        return randomValue % (maxValue + 1);
    }

    return randomValue;
}

unsigned int s3_calculate_distance(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2) {
    return real_calculate_distance(x1, y1, x2, y2);
}

int s3_get_selected_list_item_value(void* list) {
    return real_get_selected_list_item_value(list);
}

void s3_add_item_pile_near(game_data* gameData, int itemType, unsigned int itemCount, int x, int y) {
    unsigned int addedCount = 0;
    while (addedCount < itemCount) {
        int toAdd = itemCount - addedCount;
        if (toAdd > 8) {
            toAdd = 8;
        }

        real_s3_add_item_pile_near(gameData, itemType, toAdd, x, y);
        addedCount += toAdd;
    }
}
