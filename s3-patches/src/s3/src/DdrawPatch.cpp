#include "stdafx.h"
#include "DdrawPatch.h"
#include <algorithm>
#include <asmjit/x86.h>
#include <ddraw.h>
#include <MachineCodePatchBuilder.h>
#include <DetourHooker.h>
#include "dll_config.h"
#include "constants.h"

extern "C"
{
#include "ddraw_proxy.h"
}

using namespace asmjit;

static DdrawPatch* g_activePatch;
static unsigned int* g_displayWidth = reinterpret_cast<unsigned int*>(Constants::S3_EXE + 0x330F0C);
static unsigned int* g_displayHeight = reinterpret_cast<unsigned int*>(Constants::S3_EXE + 0x330F10);
static unsigned int* g_gameAreaWidth = reinterpret_cast<unsigned int*>(Constants::S3_EXE + 0x330F18);
static unsigned int* g_gameAreaHeight = reinterpret_cast<unsigned int*>(Constants::S3_EXE + 0x330F1C);
static unsigned int* g_miniMapSize = reinterpret_cast<unsigned int*>(Constants::S3_EXE + 0x330F28);
static unsigned int* g_gameArea_paddingHorizontal = reinterpret_cast<unsigned int*>(Constants::S3_EXE + 0x330F44);
static unsigned int* g_gameArea_paddingVertical = reinterpret_cast<unsigned int*>(Constants::S3_EXE + 0x330F48);
static unsigned int* g_activeDisplayMode = reinterpret_cast<unsigned int*>(Constants::S3_EXE + 0x330F08);

typedef DWORD (__thiscall *HANDLE_DD_ERROR_PROC)(DWORD*, DWORD, char *);
HANDLE_DD_ERROR_PROC original_handle_dd_result = reinterpret_cast<HANDLE_DD_ERROR_PROC>(Constants::S3_EXE + 0x1BF2E0);

typedef void (__stdcall *INIT_DISPLAY_SETTINGS_PROC)(signed int displayMode);
INIT_DISPLAY_SETTINGS_PROC s3_init_display_settings = reinterpret_cast<INIT_DISPLAY_SETTINGS_PROC>(Constants::S3_EXE + 0xC6D70);

typedef bool (__thiscall *HANDLE_RESOLUTION_CHANGE)(void *This, int newDisplayMode);
HANDLE_RESOLUTION_CHANGE s3_handle_resolution_change = reinterpret_cast<HANDLE_RESOLUTION_CHANGE>(Constants::S3_EXE + 0x5C80);

typedef void (__thiscall *INIT_DRAWING_SURFACES_PROC)(void *drawer, void *a2);
INIT_DRAWING_SURFACES_PROC s3_init_drawing_surfaces = reinterpret_cast<INIT_DRAWING_SURFACES_PROC>(Constants::S3_EXE + 0x2E430);

typedef DWORD* (__thiscall *INIT_SURFACE)(void* surface);
INIT_SURFACE s3_init_surface = reinterpret_cast<INIT_SURFACE>(Constants::S3_EXE + 0x1BFAB0);

static int g_customWidth = 1366;
static int g_customHeight = 768;
static IDirectDrawSurface* menuSurface = nullptr;
static IDirectDrawSurface* gameAreaSurface = nullptr;

bool __fastcall handle_resolution_change(void* This, void* notUsed, int newDisplayMode) {
    if (newDisplayMode == 0) {
        typedef struct {
            BYTE gap[4];
            IDirectDraw2* ddDraw;
        } dd_container;

        typedef struct {
            BYTE gap[0x58];
            dd_container* lpDdContainer;
        } application;
        auto app = reinterpret_cast<application*>(This);

        app->lpDdContainer->ddDraw->SetDisplayMode(g_customWidth, g_customHeight, 16, 0, 0);

        return true;
    }

    return s3_handle_resolution_change(This, newDisplayMode);
}

void __stdcall init_display_settings(signed int displayMode) {
    int width = g_customWidth;
    int height = g_customHeight;

    if (width == 1024 && height == 768) {
        s3_init_display_settings(2);
        return;
    }

    s3_init_display_settings(0);

    *g_displayWidth = width;
    *g_displayHeight = height;
    *g_gameAreaWidth = width - 216;
    *g_gameAreaHeight = height;
    dprintf("Setting width/height: %d,%d\n", *g_displayWidth, *g_displayHeight);
}

void __fastcall init_drawing_surfaces(void *This, void* notUsed, void *a2) {
    s3_init_drawing_surfaces(This, a2);
    return;

    // TODO: The below is only needed for adding zoom capabilities. In that case,
    //       we would render the game area on screen resolution size on one surface
    //       and then copy/scale (using DDSurface::Blt) part of that game area to the result surface.
    //
    //       The left menu would have to be scaled differently otherwise it would also change
    //       size while zooming. Hence we need two additional helper surfaces which we would then combine
    //       into a result surface. Currently, the game directly draws everything onto the result surface.
    //
    //       What is left to do is exchange the DDSurface pointer in the backgroundSurface to either
    //       point to our gameAreaSurface or to the menuSurface depending on what is drawn. Plus at the
    //       end of drawing, we need to combine both of our helper surfaces and draw onto the game's original
    //       background surface, scaling up the menu with a fixed size, and scaling the game area based on
    //       actual zoom level.
    //
    //       One further problem is that the game currently does not render some game objects (everything but the
    //       landscape towards the bottom of the screen. It is unclear if there is a general limit as to what is drawn
    //       or if the game stops drawing objects at a certain distance from the game center which is hard-coded somewhere.
    //       This would have to be removed so that all objects are drawn which should not be an issue for modern hardware.
    //
    //       Besides, we would likely need to apply fixes for the mini map so that the viewed area is displayed
    //       correctly. We might also need to adjust the divisor for the left menu so that the clicked button is
    //       determined correctly. We also may have to modify the center coordinates that are being viewed so that
    //       pioneer/spy/geo conversions are done with respect to the middle of the viewed area.

    typedef struct {
        BYTE gap[0x4];
        IDirectDraw2* directDraw;
    } drawer_t;
    auto drawer = reinterpret_cast<drawer_t*>(This);

    DDSURFACEDESC surfaceDesc;
    surfaceDesc.ddsCaps.dwCaps |= DDSCAPS_OFFSCREENPLAIN;
    surfaceDesc.dwSize = sizeof(DDSURFACEDESC);
    surfaceDesc.dwFlags = DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH;
    surfaceDesc.dwWidth = *g_displayWidth;
    surfaceDesc.dwHeight = *g_displayHeight;

    if (drawer->directDraw->CreateSurface(&surfaceDesc, &menuSurface, NULL) != DD_OK) {
        dprintf("Failed to create surface\n");
    } else {
        dprintf("created surface for left menu\n");
    }
    if (drawer->directDraw->CreateSurface(&surfaceDesc, &gameAreaSurface, NULL) != DD_OK) {
        dprintf("Failed to create game area surface\n");
    } else {
        dprintf("created surface for game area\n");
    }
}

void on_after_settings_parse(hl::CpuContext* ctx) {
    // Force compatible graphics settings
    bool* graphicsSettings = reinterpret_cast<bool*>(0x7a7b4a);
    graphicsSettings[0] = true;
    graphicsSettings[1] = true;
    graphicsSettings[2] = true;
    graphicsSettings[3] = true;
}

void DdrawPatch::enable() {
    if (m_config->getGraphicMode() == GRAPHIC_MODE_DISABLE) {
        patchOldGraphicMode();
    } else {
        m_canZoom = true;
        enableWindowMode();
    }

    g_customWidth = m_config->getDisplayWidth();
    g_customHeight = m_config->getDisplayHeight();

//    g_customWidth = GetSystemMetrics(SM_CXSCREEN);
//    g_customHeight = GetSystemMetrics(SM_CYSCREEN);
//    g_customHeight = 900;

    applyBoundaryCheckPatch();
    applyIgnoreDisplayModeChangePatch(&m_changeDisplayModePatch1, Constants::S3_EXE + 0x125A83);
    applyIgnoreDisplayModeChangePatch(&m_changeDisplayModePatch2, Constants::S3_EXE + 0x125B36);
    applyIgnoreDisplayModeChangePatch(&m_changeDisplayModePatch3, Constants::S3_EXE + 0x125BFB);
    handleResolutionChangeHook.hook(reinterpret_cast<void**>(&s3_handle_resolution_change), reinterpret_cast<void*>(handle_resolution_change));
    initDisplaySettingsHook.hook(reinterpret_cast<void**>(&s3_init_display_settings), reinterpret_cast<void*>(init_display_settings));
    initSurfacesHook.hook(reinterpret_cast<void**>(&s3_init_drawing_surfaces), reinterpret_cast<void*>(init_drawing_surfaces));
    m_hooker.hookDetour(Constants::S3_EXE + 0xB6D7, 6, on_after_settings_parse);
}

void DdrawPatch::applyIgnoreDisplayModeChangePatch(hl::Patch* patchHolder, uintptr_t startPos) {
    MachineCodePatchBuilder builder(startPos);
    builder.a->jmp(reinterpret_cast<uint64_t*>(Constants::S3_EXE + 0x125866));
    builder.apply(patchHolder);
}

/**
 * Fixes game area boundary check when game area width >> game area height.
 *
 * The game is incorrectly using the game area width to limit the allowed scrolling height. This is not noticeable as long
 * as the game area width and height are close enough together, however if game area width is much larger than the game area
 * height scrolling to the bottom of the map is not possible anymore. This corrects the check to use the game area height instead.
 */
void DdrawPatch::applyBoundaryCheckPatch() {
    MachineCodePatchBuilder builder(Constants::S3_EXE + 0x11B528);
    builder.a->mov(x86::edi, x86::ptr(0x730F1C, 4));
    builder.apply(&m_boundaryCheckPatch);
}

DWORD __fastcall handle_dd_result(DWORD* thisRef, void* notUsed, DWORD ddResult, char * currentOperation) {
    if (ddResult == DD_OK) {
        return DD_OK;
    }

    if (ddResult == DDERR_NOTLOCKED && strcmp(currentOperation, "CSurface::DDUnlock") == 0) {
        // Let's just ignore this error. If it is not locked while attempting to unlock, we are already
        // in the desired target state.
        return DD_OK;
    }

    return original_handle_dd_result(thisRef, ddResult, currentOperation);
}

void DdrawPatch::patchOldGraphicMode() {
    ddResultHandlingHook.hook((void**)&original_handle_dd_result, (void*)handle_dd_result);
}

void DdrawPatch::enableWindowMode() {
    if (g_activePatch) {
        g_activePatch->disable();
    }
    g_activePatch = this;

    bool shouldApplyTextureFilter = m_config->shouldApplyTextureFilter();

    // In auto-mode, enable linear scaling if the screen is much larger than the game display size.
    if (m_config->getGraphicMode() == -1 && !m_config->shouldForceDisplayResolution()) {
        int screenWidth = GetSystemMetrics(SM_CXSCREEN);
        int screenHeight = GetSystemMetrics(SM_CYSCREEN);

        if (screenWidth >= 1.2 * m_config->getDisplayWidth() || screenHeight >= 1.2 * m_config->getDisplayHeight()) {
            dprintf("Automatically enable texture filter\n");
            shouldApplyTextureFilter = true;
        }
    }

    uintptr_t coCreateInstanceAddr;
    install_ddraw_compat((void**)&coCreateInstanceAddr, m_config->getGraphicMode(), shouldApplyTextureFilter, m_config->shouldReduceMaxFps(),
                         m_config->shouldForceDisplayResolution(), m_config->getDisplayWidth(), m_config->getDisplayHeight(), m_config->shouldForceWindowMode());

    Environment env;
    env.setArch(Environment::kArchX86);

    uintptr_t firstCoCreate = Constants::S3_EXE + 0x1BEC16;
    CodeHolder code;
    code.init(env, firstCoCreate);
    x86::Assembler a(&code);
    a.call(coCreateInstanceAddr);
    a.nop();

    CodeBuffer& buf = code.textSection()->buffer();
    m_ddPatch.apply(Constants::S3_EXE + 0x1BEC16, (char*)buf.data(), buf.size());
}

void DdrawPatch::disable() {
    // The window mode patch cannot be reverted sanely, but we can revert the DD Error handling for the old graphic mode.
    ddResultHandlingHook.revert();
    m_hooker.revert();

    if (g_activePatch == this) {
        g_activePatch = nullptr;
    }
}

bool DdrawPatch::UpdateDisplaySettings(void* drawer, current_draw_data* drawData) {
    int requestedLevel = m_requestedZoomLevel;
    if (requestedLevel == m_activeZoomLevel) {
        return false;
    }

    dprintf("Changing zoom level (%d -> %d, game area width: %d, game area height: %d)\n", m_activeZoomLevel, requestedLevel, *g_gameAreaWidth, *g_gameAreaHeight);
    m_activeZoomLevel = requestedLevel;

    if (requestedLevel == 10) {

    } else if (requestedLevel == 0) {

    }

    return true;
}

bool DdrawPatch::OnWindowMessage(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    if (!m_isInGame) {
        return false;
    }

    short movement = HIWORD(wParam);
    m_requestedZoomLevel = std::min(10, std::max(0, m_requestedZoomLevel + movement));

    return false;
}

void DdrawPatch::OnAfterGameInit(logic_thread* logicThread) {
    m_isInGame = true;
}

void DdrawPatch::OnGameLeave(logic_thread* logicThread, bool isGameOver) {
    m_isInGame = false;
}
