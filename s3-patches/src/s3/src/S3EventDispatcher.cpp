#include <debug_console.h>
#include "constants.h"
#include "S3EventDispatcher.h"
#include "s3.h"

static S3EventDispatcher*g_instance;
static boolean dispatchedGameOver = false;

typedef void (__thiscall *GAME_TICK_PROC)(logic_thread *This);
GAME_TICK_PROC s3_process_game_tick = reinterpret_cast<GAME_TICK_PROC>(Constants::S3_EXE + 0x13DB30);

typedef void (__thiscall *INIT_GAME_PROC)(logic_thread *This);
INIT_GAME_PROC s3_init_game = reinterpret_cast<INIT_GAME_PROC>(Constants::S3_EXE + 0x13C9E0);

typedef void (__thiscall *DRAW_GAME_DATA_PROC)(game_drawer *gameDrawer, current_draw_data *currentDrawData, unsigned int a4);
DRAW_GAME_DATA_PROC  s3_draw_game_data = reinterpret_cast<DRAW_GAME_DATA_PROC>(Constants::S3_EXE + 0xB26C0);

typedef void (__thiscall *UPDATE_DRAW_DATA_PROC)(logic_thread *thisLogicThread, char arg);
UPDATE_DRAW_DATA_PROC s3_update_draw_data = reinterpret_cast<UPDATE_DRAW_DATA_PROC>(Constants::S3_EXE + 0x11F110);

typedef int (__thiscall *MENU_DRAW_THREAD_LOOP_PROC)(lp_menu_draw_thread *This, int a2);
MENU_DRAW_THREAD_LOOP_PROC s3_menu_draw_thread_loop =
    reinterpret_cast<MENU_DRAW_THREAD_LOOP_PROC>(Constants::S3_EXE + 0x93880);

typedef game_data* (__thiscall *LOAD_SAVE_GAME_PROC)(logic_thread *This, void *saveFile);
LOAD_SAVE_GAME_PROC s3_load_save_game = reinterpret_cast<LOAD_SAVE_GAME_PROC>(Constants::S3_EXE + 0x108FA0);

typedef unsigned int (__thiscall *INIT_BUILDING_PROC)(game_data *This, int xCoord, int yCoord, int playerIndex, int buildingType);
INIT_BUILDING_PROC s3_init_building = reinterpret_cast<INIT_BUILDING_PROC>(Constants::S3_EXE + 0xDD280);

typedef signed int (__thiscall *CREATE_BUILDING_PROC)(game_data *This, unsigned int xCoord, unsigned int yCoord, int buildingType, signed int playerIndex, unsigned int initialState);
CREATE_BUILDING_PROC s3_create_building = reinterpret_cast<CREATE_BUILDING_PROC>(Constants::S3_EXE + 0xD99E0);

static unsigned int lastInitializedBuildingIndex = 0;

unsigned int __fastcall init_building(game_data *gameData, void *notUsed, int xCoord, int yCoord, int playerIndex, int buildingType) {
    auto newBuildingIndex = s3_init_building(gameData, xCoord, yCoord, playerIndex, buildingType);
    lastInitializedBuildingIndex = newBuildingIndex;

    return newBuildingIndex;
}

signed int __fastcall create_building(game_data * gameData, void *notUsed, unsigned int xCoord, unsigned int yCoord, int buildingType, signed int playerIndex, unsigned int initialState) {
    auto result = s3_create_building(gameData, xCoord, yCoord, buildingType, playerIndex, initialState);
    if (result && lastInitializedBuildingIndex && initialState == 1) {
        for (auto listener : g_instance->GetBuildingCreatedListener()) {
            listener->OnBuildingCreated(gameData, lastInitializedBuildingIndex);
        }
    }

    return result;
}

void __fastcall process_game_tick(logic_thread *logicThread) {
    for (auto listener : g_instance->GetBeforeGameLoopListeners()) {
        listener->OnBeforeGameLoop(logicThread);
    }

    s3_process_game_tick(logicThread);
}

void __fastcall init_game(logic_thread *logicThread) {
    dispatchedGameOver = false;
    s3_init_game(logicThread);

    for (auto listener : g_instance->GetAfterGameInitListener()) {
        listener->OnAfterGameInit(logicThread);
    }
}

void __fastcall draw_game_data(game_drawer *gameDrawer, void* notUsed, current_draw_data *currentDrawData, unsigned int a4) {
    s3_draw_game_data(gameDrawer, currentDrawData, a4);

    for (auto listener : g_instance->GetAfterDrawListener()) {
        listener->OnAfterDraw(gameDrawer, currentDrawData);
    }
}

void __fastcall update_draw_data(logic_thread* logicThread, void* notUsed, char arg) {
    for (auto listener : g_instance->GetBeforeUpdateDrawDataListener()) {
        listener->OnBeforeUpdateDrawData(logicThread);
    }

    s3_update_draw_data(logicThread, arg);
}

game_data* __fastcall load_save_game(logic_thread *This, void* notUsed, void *saveFile) {
    auto gameData = s3_load_save_game(This, saveFile);

    // Some listeners may affect other listeners, those have to run first when loading the save.
    for (auto listener : g_instance->GetLoadSaveStage1Listener()) {
        listener->OnLoadSave(This, saveFile);
    }
    for (auto listener : g_instance->GetLoadSaveListener()) {
        listener->OnLoadSave(This, saveFile);
    }

    return gameData;
}

int __fastcall menu_loop_callback(lp_menu_draw_thread* menuDrawThread, void* notUsed, int arg2) {
    for (auto listener : g_instance->GetBeforeMenuDrawLoopListener()) {
        listener->OnBeforeMenuDrawLoop(menuDrawThread);
    }

    int result = s3_menu_draw_thread_loop(menuDrawThread, arg2);

    for (auto listener : g_instance->GetAfterMenuDrawLoopListener()) {
        listener->OnAfterMenuDrawLoop(menuDrawThread);
    }

    return result;
}

void on_game_over(hl::CpuContext* ctx) {
    if (dispatchedGameOver) {
        return;
    }
    dispatchedGameOver = true;

    logic_thread* logicThread = reinterpret_cast<logic_thread*>(ctx->EBX);
    for (auto listener : g_instance->GetGameOverListener()) {
        listener->OnGameOver(logicThread);
    }
}

void on_save(hl::CpuContext* ctx) {
    void* saveFile = reinterpret_cast<void*>(ctx->ESP + 56);
    logic_thread* logicThread = reinterpret_cast<logic_thread*>(ctx->EBP);

    for (auto listener : g_instance->GetSaveListener()) {
        listener->OnSave(logicThread, saveFile);
    }
}

void on_game_leave(hl::CpuContext* ctx) {
    logic_thread* logicThread = reinterpret_cast<logic_thread*>(ctx->EBP);
    int playerIndex = *reinterpret_cast<int*>(ctx->ESP + 16);
    if (playerIndex == 19) {
        for (auto listener : g_instance->GetGameQuitListener()) {
            listener->OnGameLeave(logicThread, dispatchedGameOver);
        }
    }
}

S3EventDispatcher::S3EventDispatcher()
{
    g_instance = this;

    gameInitHook.hook((void**)&s3_init_game, (void*)init_game);
    logicLoopHook.hook((void**)&s3_process_game_tick, (void*)process_game_tick);
    drawHook.hook((void**)&s3_draw_game_data, (void*)draw_game_data);
    updateDrawDataHook.hook((void**)&s3_update_draw_data, (void*)update_draw_data);
    menuDrawLoopHook.hook((void**)&s3_menu_draw_thread_loop, (void*)menu_loop_callback);
    loadSaveHook.hook((void**)&s3_load_save_game, (void*)load_save_game);
    createBuildingHook.hook((void**)&s3_create_building, (void*)create_building);
    initBuildingHook.hook((void**)&s3_init_building, (void*)init_building);
    activeHooks.push_back(hooker.hookDetour(Constants::S3_EXE + 0x10ABA1, 5, on_save));
    activeHooks.push_back(hooker.hookDetour(Constants::S3_EXE + 0x13B34E, 10, on_game_over));
    activeHooks.push_back(hooker.hookDetour(Constants::S3_EXE + 0x13E7EB, 5, on_game_leave));
}

std::vector<BeforeGameLoopListener*> S3EventDispatcher::GetBeforeGameLoopListeners()
{
    return beforeGameLoopListeners;
}

std::vector<AfterGameInitListener*> S3EventDispatcher::GetAfterGameInitListener()
{
    return afterGameInitListeners;
}

std::vector<AfterDrawListener*> S3EventDispatcher::GetAfterDrawListener()
{
    return afterDrawListeners;
}

std::vector<BeforeUpdateDrawDataListener*> S3EventDispatcher::GetBeforeUpdateDrawDataListener()
{
    return beforeUpdateDrawDataListener;
}

std::vector<BeforeMenuDrawLoopListener*> S3EventDispatcher::GetBeforeMenuDrawLoopListener()
{
    return beforeMenuDrawLoopListener;
}

std::vector<AfterMenuDrawLoopListener*> S3EventDispatcher::GetAfterMenuDrawLoopListener()
{
    return afterMenuDrawLoopListener;
}

std::vector<GameOverListener*> S3EventDispatcher::GetGameOverListener()
{
    return gameOverListener;
}

std::vector<SaveListener*> S3EventDispatcher::GetSaveListener()
{
    return saveListener;
}

std::vector<LoadSaveListener*> S3EventDispatcher::GetLoadSaveListener()
{
    return loadSaveListener;
}

std::vector<BuildingCreatedListener*> S3EventDispatcher::GetBuildingCreatedListener()
{
    return buildingCreatedListener;
}

std::vector<GameLeaveListener*> S3EventDispatcher::GetGameQuitListener()
{
    return gameQuitListener;
}

std::vector<GuiListener*> S3EventDispatcher::GetGuiListener()
{
    return guiListener;
}

std::vector<LoadSaveStage1Listener*> S3EventDispatcher::GetLoadSaveStage1Listener()
{
    return loadSaveStage1Listener;
}
std::vector<WindowMessageListener*> S3EventDispatcher::GetWmListener()
{
    return wmListener;
}
