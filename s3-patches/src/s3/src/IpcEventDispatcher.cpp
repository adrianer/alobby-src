#include <debug_console.h>
#include "IpcEventDispatcher.h"
#include "ipc_events.h"

void IpcEventDispatcher::OnAfterMenuDrawLoop(lp_menu_draw_thread* menuDrawThread) {
    if (menuDrawThread->activeDialogId != m_lastDialogId) {
        ipc_event_dialog_changed eventData = {m_lastDialogId, menuDrawThread->activeDialogId};
        m_ipcServer->SendEvent(IPC_EVENT_DIALOG_CHANGED, reinterpret_cast<BYTE*>(&eventData), sizeof(ipc_event_dialog_changed));
        m_lastDialogId = menuDrawThread->activeDialogId;

        if (menuDrawThread->activeDialogId == PRE_GAME_LOBBY_DIALOG_ID) {
            m_firstLobbyUpdate = true;
            memset(&m_preGameLobbyData, 0, sizeof(ipc_event_pre_game_lobby_event_data));
        }
    }

    if (menuDrawThread->activeDialogId == PRE_GAME_LOBBY_DIALOG_ID) {
        auto settings = s3_get_game_settings();

        if (!settings->isMultiplayerGame) {
            return;
        }

        bool hasChanged = m_firstLobbyUpdate;
        if (hasChanged) {
            dprintf("Sending first lobby update\n");
            m_preGameLobbyData.isSaveGame = settings->isSaveGame;
        }

        m_firstLobbyUpdate = false;
        bool isJoined = settings->ownPlayerIndex >= 0 && settings->ownPlayerIndex < 20;
        if (m_preGameLobbyData.isJoined != isJoined) {
            m_preGameLobbyData.isJoined = isJoined;
            hasChanged = true;
        }
        if (m_preGameLobbyData.isHost != settings->isHost) {
            m_preGameLobbyData.isHost = settings->isHost;
            hasChanged = true;
        }
        if (m_preGameLobbyData.isEconomyMode != settings->isEconomyMode) {
            m_preGameLobbyData.isEconomyMode = settings->isEconomyMode;
            hasChanged = true;
        }
        if (m_preGameLobbyData.isAmazonAllowed != settings->hasAmazons) {
            m_preGameLobbyData.isAmazonAllowed = settings->hasAmazons;
            hasChanged = true;
        }
        if (m_preGameLobbyData.goodsSetting != settings->goodsSetting) {
            m_preGameLobbyData.goodsSetting = settings->goodsSetting;
            hasChanged = true;
        }

        if (settings->mapName.actualTextLength > 0 && strcmp(m_preGameLobbyData.mapName, settings->mapName.sText) != 0) {
            strncpy(m_preGameLobbyData.mapName, settings->mapName.sText, sizeof(m_preGameLobbyData.mapName));
            typedef struct map_file {
                BYTE gap[0x1C];
                str_struct fileName;
            } map_file;
            auto mapFile = reinterpret_cast<map_file*>(settings->mapFile);

            if (mapFile->fileName.actualTextLength > 0) {
                strncpy(m_preGameLobbyData.mapFileName, mapFile->fileName.sText, sizeof(m_preGameLobbyData.mapFileName));
            } else {
                memset(m_preGameLobbyData.mapFileName, 0, sizeof(m_preGameLobbyData.mapFileName));
            }

            hasChanged = true;
        }

        auto currentGameSettings = m_gameSettingsManager->getGameSettings();
        if (m_processedGameSettings != currentGameSettings) {
            unsigned int gameModes = 0;
            if (s3_is_random_game()) {
                if (currentGameSettings.useRevisedRandomBalancing()) {
                    gameModes |= GAME_MODE_RANDOM_BALANCING;
                }
                if (currentGameSettings.shouldIncreaseRandomResourceYield()) {
                    gameModes |= GAME_MODE_MORE_RESOURCES;
                }
            } else {
                if (currentGameSettings.useIncreasedTransportLimit()) {
                    gameModes |= GAME_MODE_INCREASED_TRANSPORT_LIMIT;
                }
            }
            if (currentGameSettings.useRevisedThief()) {
                gameModes |= GAME_MODE_NEW_THIEVES;

                if (!currentGameSettings.isStealingDisallowed()) {
                    gameModes |= GAME_MODE_STEALING_ALLOWED;
                }
            }
            m_preGameLobbyData.gameModes = gameModes;
            m_processedGameSettings = currentGameSettings;
            hasChanged = true;
        }

        if (isJoined) {
            if (settings->numPlayers != m_preGameLobbyData.numPlayers) {
                m_preGameLobbyData.numPlayers = settings->numPlayers;
                hasChanged = true;
            }
            for (int i=0;i<settings->numPlayers;i++) {
                auto player = &settings->players.players[i];

                if (m_preGameLobbyData.players[i].slotStatus != player->slotStatus) {
                    m_preGameLobbyData.players[i].slotStatus = player->slotStatus;

                    if (player->playerName.actualTextLength > 0) {
                        strncpy(m_preGameLobbyData.players[i].name, player->playerName.sText, sizeof(m_preGameLobbyData.players[i].name));
                    } else {
                        memset(m_preGameLobbyData.players[i].name, 0, sizeof(m_preGameLobbyData.players[i].name));
                    }
                    hasChanged = true;
                }

                if (m_preGameLobbyData.players[i].team != player->team) {
                    m_preGameLobbyData.players[i].team = player->team;
                    hasChanged = true;
                }
            }
        }

        if (hasChanged) {
#ifdef LOBBY_DEBUG
            dprintf("Sending lobby update (is-joined: %d, is-host: %d)\n", m_preGameLobbyData.isJoined, m_preGameLobbyData.isHost);
            for (int i=0; i<m_preGameLobbyData.numPlayers; i++) {
                auto player = &m_preGameLobbyData.players[i];
                dprintf("Slot-Status: %d - Name: %s - Team: %d\n", player->slotStatus, player->name, player->team);
            }
#endif

            m_ipcServer->SendEvent(IPC_EVENT_TYPE_PRE_GAME_LOBBY_UPDATE, reinterpret_cast<BYTE*>(&m_preGameLobbyData), sizeof(ipc_event_pre_game_lobby_event_data));
        }
    }
}
