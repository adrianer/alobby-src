#include "ZoomFunctionPatch.h"

#include <ddraw.h>
#include <asmjit/x86.h>
#include <constants.h>

using namespace asmjit;

typedef void*(__cdecl* _newObject)(int size);
_newObject newObject = (_newObject)(Constants::S3_EXE + 0x21EFB9);

typedef void*(__thiscall* _newCOffscreenSurface)(DWORD* thisPtr);
_newCOffscreenSurface newCOffscreenSurface = (_newCOffscreenSurface)(Constants::S3_EXE + 0x1BFAB0);

typedef int(__thiscall* _createDDSurface)(int width, int height);
_createDDSurface createDDSurface = (_createDDSurface)(Constants::S3_EXE + 0x1BFAF0);

DWORD __fastcall fChangeCursorPosZoom(DWORD);
void fZoomBlt(LPDIRECTDRAWSURFACE, LPDIRECTDRAWSURFACE);

DWORD bInitZoomSurface = 0;
DWORD bZoomBlt = 0;
DWORD bInitNewOffscreenSurface = 0;
DWORD bDeleteNewSurfaceOnError = 0;
DWORD bDeleteNewSurfaceOnExit = 0;
DWORD bSetZoomAsBackForLandscape = 0;
DWORD bUnlockDDSurface = 0;
DWORD bSetZoomLevel = Constants::S3_EXE + 0x4A4C;
DWORD bChangeCursorPosZoom = Constants::S3_EXE + 0x125097;
DWORD bSelectionBoxReorder = Constants::S3_EXE + 0xB366F;

int* gameWindowWidthAddress = (int*)(Constants::S3_EXE + 0x330F0C);
int* gameWindowHeightAddress = (int*)(Constants::S3_EXE + 0x330F10);
int* gameAreaWidthAddress = (int*)(Constants::S3_EXE + 0x330F18);
int* gameAreaHeightAddress = (int*)(Constants::S3_EXE + 0x330F1C);
int* sideBarWidthAddress = (int*)(Constants::S3_EXE + 0x330F24);

static int zoomLevel = 100;
static void* lpZoomSurface = 0;

_declspec(naked) void fSetZoomLevel() {
    __asm {
        mov eax, 5BA290h
        call eax
        lea ecx, [ebp-128h]
        cmp dword ptr[ecx], 8
        jne skip
        mov ecx, [ecx+4]
        test ecx, ecx
        je skip
        cmp ecx, 0
        jg increase
        sub [zoomLevel], 19h
        jmp clampmin
     increase:
        add [zoomLevel], 19h
        jmp clampmax
     clampmin:
        cmp [zoomLevel], 64h
        jge skip
        mov [zoomLevel], 64h
        jmp skip
     clampmax:
        cmp [zoomLevel], 12Ch
        jna skip
        mov [zoomLevel], 12Ch
     skip:
        jmp [bSetZoomLevel]
    }
}

__declspec(naked) void fUnlockDDSurface() {
    __asm {
        mov esi, [ebx+5F6C8h]
        mov edi, [ebx+0A4h]
        mov eax, [esi+8]
        push edi
        push eax
        mov ecx, [eax]
        call dword ptr [ecx+80h]
        push 697E7Ch
        push eax
        mov ecx, esi
        mov eax, 5BF2E0h
        call eax
        cmp eax, 887601C2h
        jne finish
        mov eax, [esi+8]
        push edi
        push eax
        mov edx, [eax]
        call dword ptr [edx+80h]
        push 697E7Ch
        push eax
        mov ecx, esi
        mov eax, 5BF2E0h
        call eax
        finish:
        ret
    }
}

__declspec(naked) void tZoomBlt() {
    __asm
    {
        sub esp, 20h
        mov eax, [lpZoomSurface]
        ;mov eax, [eax + 8]
        ;push eax
        push 0
        mov eax, [esi + 8]
        push eax
        call fZoomBlt
        ; mov eax, [ebx + 5F6B8h]
        ; mov eax, [eax + 0Ch]
        ; push eax
        ; ush eax
        ; call fBltZoomTest
        ; mov ecx, ebx
        ; mov eax, 0x42CD70
        ; call eax
        add esp, 28h
        ret
    }
}

__declspec(naked) void tUnlockDDSurface() {
    __asm
    {
        call fUnlockDDSurface
        mov esi, [ebx + 5F6C8h]
        call tZoomBlt
        push 0
        lea edx, [ebx + 80h]
        push edx
        push 0
        mov ecx, [ebx + 5F6C8h]
        mov eax, 5BF050h
        call eax
        ; mov ecx, [7DFD64h]
        ; mov ecx, [ecx]
        ; mov eax, [730F28h]
        ; mov eax, [eax]
        ; jmp fSelectionBox2
        mov eax, 730F28h
        mov eax, [eax]
        jmp[bSelectionBoxReorder]
    }
}

_declspec(naked) void tChangeCursorPosZoom() {
    __asm {
        mov ecx, [esp+134h]
        test [esp+12Ch], 17h
        je skip
        call fChangeCursorPosZoom
        mov [esp+134h], eax
    skip:
        mov eax, 7DEB55h
        mov al, byte ptr [eax]
        jmp [bChangeCursorPosZoom]
    }
}

_declspec(naked) void fSelectionBox1() {
    __asm
    {
        mov eax, 4B3CF3h
        jmp eax
    }
}

_declspec(naked) void fSelectionBox2() {
    __asm
    {
        ; jmp tUnlockDDSurface
        mov eax, 730F28h
        mov eax, [eax]
        jmp [bSelectionBoxReorder]
    }
}

_declspec(naked) void fSelectionBox3() {
    __asm
    {
        mov [ebx+5F6A0h], eax
        mov ecx, [ebp + 8]
        or edi, -1
        mov eax, 4B3DE7h
        jmp eax
    }
}

_declspec(naked) void fSelectionBox4() {
    __asm
    {
        ; jmp fSelectionBox2
        jmp tUnlockDDSurface
    }
}

void fZoomBlt(LPDIRECTDRAWSURFACE backSurface, LPDIRECTDRAWSURFACE zoomSurface) {
    DDSURFACEDESC ddsd;
    RECT srcRect;
    RECT destRect;
    HRESULT hRet;
    DDBLTFX ddbltfx;
    ZeroMemory(&ddsd, 0x6C);
    ddsd.dwSize = 0x6C;

    ZeroMemory(&ddbltfx, sizeof(ddbltfx));
    ddbltfx.dwSize = sizeof(ddbltfx);
    // ddbltfx.dwDDFX = DDBLTFX_MIRRORLEFTRIGHT;

    hRet = backSurface->GetSurfaceDesc(&ddsd);

    if (hRet == DD_OK) {
        int scaleFactorWidth = *gameAreaWidthAddress * 100 / zoomLevel;
        int scaleFactorHeight = *gameAreaHeightAddress * 100 / zoomLevel;

        if (!IsNewGraphicMode) {
            // doesn't work in new graphics mode wtf
            // blt broken in wrapper? or me being stupid
            srcRect.top = (*gameAreaHeightAddress - scaleFactorHeight) / 2;
            srcRect.bottom = scaleFactorHeight + srcRect.top;
            srcRect.left = (*gameAreaWidthAddress - scaleFactorWidth) / 2 + *sideBarWidthAddress;
            srcRect.right = scaleFactorWidth + srcRect.left;
        } else {
            // zoom into corner instead lol
            srcRect.right = *gameWindowWidthAddress;
            srcRect.bottom = *gameWindowHeightAddress;
            srcRect.top = *gameAreaHeightAddress - scaleFactorHeight;
            srcRect.left = *gameAreaWidthAddress - scaleFactorWidth + *sideBarWidthAddress;
        }

        destRect.right = *gameWindowWidthAddress;
        destRect.bottom = *gameWindowHeightAddress;
        destRect.left = *sideBarWidthAddress;
        destRect.top = 0;

        do {
            Sleep(0);
            // hRet = backSurface->BltFast(0, 0, zoomSurface, 0, DDBLTFAST_SRCCOLORKEY);
            hRet = backSurface->Blt(&destRect, backSurface, &srcRect, 0, 0);
        } while (hRet == DDERR_WASSTILLDRAWING);

        if (hRet == DDERR_SURFACELOST) {
            backSurface->Restore();
        }
    }
}

DWORD __fastcall fChangeCursorPosZoom(DWORD cursorPos) {
    short xPos = cursorPos & 0xFFFF;
    short yPos = cursorPos >> 16;

    if (zoomLevel > 100 && xPos > *sideBarWidthAddress) {
        
        if (IsNewGraphicMode) {
            xPos = (xPos - *sideBarWidthAddress - (*gameAreaWidthAddress / 2)) * 100 / zoomLevel + (*gameAreaWidthAddress / 2) + *sideBarWidthAddress;
            yPos = (yPos - (*gameWindowHeightAddress / 2)) * 100 / zoomLevel + (*gameWindowHeightAddress / 2);

            xPos += (*gameAreaWidthAddress - *gameAreaWidthAddress * 100 / zoomLevel) / 2;
            yPos += (*gameAreaHeightAddress - *gameAreaHeightAddress * 100 / zoomLevel) / 2;
        } else {
            xPos = (xPos - *sideBarWidthAddress - (*gameAreaWidthAddress / 2)) * 100 / zoomLevel + (*gameAreaWidthAddress / 2) + *sideBarWidthAddress;
            yPos = (yPos - (*gameWindowHeightAddress / 2)) * 100 / zoomLevel + (*gameWindowHeightAddress / 2);
        }
        
    }

    return xPos & 0xFFFF | (yPos << 16);
}

//don't really need this anymore, but if there's ever need for an extra surface..
__declspec(naked) void tInitNewOffscreenSurface() {
    __asm
    {
        ; sub esp, 10h
        push 50h
        call newObject
        add esp, 4
        mov ecx, eax
        call newCOffscreenSurface
        mov[lpZoomSurface], eax
        mov[eax + 0Ch], ebx
        mov dword ptr[eax + 14h], 800h
        mov ecx, [gameWindowHeightAddress]
        mov ecx, [ecx]
        push ecx
        mov ecx, [gameWindowWidthAddress]
        mov ecx, [ecx]
        push ecx
        mov ecx, eax
        call createDDSurface
        mov byte ptr[ebx + 5F6E4h], 0
        ; add esp, 10h
        jmp [bInitNewOffscreenSurface]
    }
}

_declspec(naked) void fDeleteNewSurfaceOnError() {
    __asm {
        mov ecx, [lpZoomSurface]
        cmp ecx, edi
        je skip
        mov edx, [ecx]
        push 1
        call dword ptr [edx]
    skip:
        mov ecx, [esi+5F6E0h]
        jmp [bDeleteNewSurfaceOnError]
    }
}

_declspec(naked) void fDeleteNewSurfaceOnExit() {
    __asm {
        mov ecx, [lpZoomSurface]
        test ecx, ecx
        je skip
        mov edx, [ecx]
        push 1
        call dword ptr [edx]
    skip:
        mov ecx, [esi+5F6B8h]
        jmp [bDeleteNewSurfaceOnExit]
    }
}



void ZoomFunctionPatch::applyPatch() {
    long long hookAddress = Constants::S3_EXE + 0x4A47;
    setZoomLevelHook.hook(reinterpret_cast<void**>(&hookAddress), reinterpret_cast<void*>(&fSetZoomLevel));
    hookAddress = Constants::S3_EXE + 0x125092;
    changeCursorPosHook.hook(reinterpret_cast<void**>(&hookAddress), reinterpret_cast<void*>(&tChangeCursorPosZoom));
    hookAddress = Constants::S3_EXE + 0xB366A;
    selectionBoxHook1.hook(reinterpret_cast<void**>(&hookAddress), reinterpret_cast<void*>(&fSelectionBox1));
    hookAddress = Constants::S3_EXE + 0xB3CED;
    selectionBoxHook2.hook(reinterpret_cast<void**>(&hookAddress), reinterpret_cast<void*>(&fSelectionBox3));
    hookAddress = Constants::S3_EXE + 0xB3DE1;
    selectionBoxHook3.hook(reinterpret_cast<void**>(&hookAddress), reinterpret_cast<void*>(&fSelectionBox4));
}

void ZoomFunctionPatch::enable() {
    applyPatch();
}

void ZoomFunctionPatch::disable() {
    setZoomLevelHook.revert();
    changeCursorPosHook.revert();
    selectionBoxHook1.revert();
    selectionBoxHook2.revert();
    selectionBoxHook3.revert();
}