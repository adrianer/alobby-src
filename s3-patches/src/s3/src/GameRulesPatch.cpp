#include "GameRulesPatch.h"
#include <algorithm>
#include <map>

typedef void(__thiscall* PROCESS_LOCAL_MESSAGES_PROC)(logic_thread* logicThread);
PROCESS_LOCAL_MESSAGES_PROC static s3_process_local_messages = reinterpret_cast<PROCESS_LOCAL_MESSAGES_PROC>(Constants::S3_EXE + 0x124FA0);

static GameRulesPatch* g_activePatch = nullptr;

void __fastcall process_local_messages(logic_thread* logicThread) {
    s3_process_local_messages(logicThread);

    auto patch = g_activePatch;
    if (patch) {
        patch->OnAfterProcessLocalMessages(logicThread);
    }
}

void assign_same_races_for_each_team(hl::CpuContext* cpuContext) {
    if (!g_activePatch->GetGameSettingsManager()->getGameSettings().useSameRacesPerTeam()) {
        return;
    }

    auto settings = s3_get_game_settings();
    auto gameData = s3_get_app()->logicThread->gameData;
    if (s3_is_random_game() && gameData->randomMirrorType != 0) {
        int pickedRacesPerSlot[20];
        std::fill(pickedRacesPerSlot, pickedRacesPerSlot + 20, -1);

        int curSlot = 0;
        int curTeam = -1;
        for (int i=0; i<settings->numPlayers; i++) {
            auto player = s3_get_or_create_settings_player(i);
            if (player->team != curTeam) {
                curSlot = 0;
                curTeam = player->team;
            } else {
                curSlot++;
            }

            if (player->race >= 0 && player->race <= 3) {
                pickedRacesPerSlot[curSlot] = player->race;
            }
        }

        curTeam = -1;
        for (int i=0; i<settings->numPlayers; i++) {
            auto player = s3_get_or_create_settings_player(i);
            if (player->team != curTeam) {
                curSlot = 0;
                curTeam = player->team;
            } else {
                curSlot++;
            }

            if (pickedRacesPerSlot[curSlot] < 0) {
                pickedRacesPerSlot[curSlot] = s3_next_random_value(gameData, 3);
            }

            if (player->race == 0xFF) {
                player->race = pickedRacesPerSlot[curSlot];
            }
        }

        return;
    }

    int raceCount[4] = { 0, 0, 0, 0 };
    int curRaceCount[4] = { 0, 0, 0, 0 };
    int prePickedRacesPerTeam[20][4];

    int curTeam = -1;
    int curPlayersPerTeam = 0;
    int maxPlayersPerTeam = 0;
    for (int i = 0; i < settings->numPlayers; i++) {
        auto player = s3_get_or_create_settings_player(i);

        if (curTeam != player->team) {
            curTeam = player->team;
            curPlayersPerTeam = 1;
            std::fill(curRaceCount, curRaceCount + 4, 0);
            std::fill(prePickedRacesPerTeam[curTeam], prePickedRacesPerTeam[curTeam] + 4, 0);
        } else {
            curPlayersPerTeam++;
        }
        if (curPlayersPerTeam > maxPlayersPerTeam) {
            maxPlayersPerTeam = curPlayersPerTeam;
        }

        if (player->race >= 0 && player->race <= 3) {
            curRaceCount[player->race]++;
            prePickedRacesPerTeam[curTeam][player->race]++;
        }

        for (char j = 0; j < 4; j++) {
            if (curRaceCount[j] > raceCount[j]) {
                raceCount[j] = curRaceCount[j];
            }
        }
    }

    std::vector<char> allRaces;
    int nbPickedRaces = 0;
    for (char i = 0; i < 4; i++) {
        while (raceCount[i]) {
            allRaces.push_back(i);
            raceCount[i]--;
            nbPickedRaces++;
        }
    }

    while (nbPickedRaces < maxPlayersPerTeam) {
        char pickedRace = s3_next_random_value(gameData, 3);
        allRaces.push_back(pickedRace);
        nbPickedRaces++;
    }

    std::vector<short> availableRaces;
    curTeam = -1;
    for (short i = 0; i < settings->numPlayers; i++) {
        auto player = s3_get_or_create_settings_player(i);
        if (curTeam != player->team) {
            curTeam = player->team;
            availableRaces.clear();
            for (auto race : allRaces) {
                if (prePickedRacesPerTeam[player->team][race] > 0) {
                    prePickedRacesPerTeam[player->team][race]--;
                } else {
                    availableRaces.push_back(race);
                }
            }
        }

        if (player->race != 0xFF) {
            continue;
        }

        int pickedPosition = s3_next_random_value(gameData, availableRaces.size() - 1);
        short pickedRace = availableRaces[pickedPosition];
        availableRaces.erase(availableRaces.begin() + pickedPosition);
        player->race = pickedRace;
    }
}

void GameRulesPatch::enable() {
    m_processLocalMessageHook.hook(reinterpret_cast<void**>(&s3_process_local_messages), reinterpret_cast<void*>(process_local_messages));
    m_hooker.hookDetour(Constants::S3_EXE + 0x13CE64, 6, assign_same_races_for_each_team);

    if (g_activePatch) {
        g_activePatch->disable();
    }
    g_activePatch = this;
}

void GameRulesPatch::disable() {
    m_processLocalMessageHook.revert();
    m_hooker.revert();

    if (g_activePatch == this) {
        g_activePatch = nullptr;
    }
}

void GameRulesPatch::OnAfterProcessLocalMessages(logic_thread* logicThread) {
    auto gameData = logicThread->gameData;
    if (gameData->buildingMode_selectedBuildingIndex && m_gameSettings.isWarMachineDisabled()) {
        auto buildingType = gameData->buildingMode_selectedBuildingType;
        if (buildingType == BUILDING_BALLISTA_FACTORY || buildingType == BUILDING_CANNON_FACTORY ||
            buildingType == BUILDING_CATAPULT_FACTORY || buildingType == BUILDING_GONG_FACTORY) {
            str_struct_wchar errorMessage;
            s3_str_init(&errorMessage, "War machines are prohibited as per host rules.");
            s3_add_chat_message(logicThread, &errorMessage, s3_get_player_color(gameData->actingPlayer));

            gameData->buildingMode_selectedBuildingIndex = 0;
            gameData->buildingMode_selectedBuildingType = 0;
        }
    }
}

void GameRulesPatch::OnBeforeGameLoop(logic_thread* logicThread) {}

void GameRulesPatch::OnAfterGameInit(logic_thread* logicThread) {
    // We create a local copy of the game settings.
    auto currentSettings = m_gameSettingsManager->getGameSettings();
    m_gameSettings = currentSettings;
}
