#include <debug_console.h>
#include "FunctionHook.h"
#include "HostGameTask.h"
#include "constants.h"
#include "s3.h"
#include "S3EventDispatcher.h"
#include "dll_config.h"
#include <imgui.h>
#include <imgui_utils.h>
#include <fmt/core.h>

static HostGameTask* g_activeTask;
static boolean isMapDialog = false;

typedef map_select_dialog* (__thiscall *INIT_MAP_SELECT_DIALOG_PROC)(map_select_dialog *This, void* graphicsData, int a3, char a4, int a5);
INIT_MAP_SELECT_DIALOG_PROC s3_init_map_select_dialog =
    reinterpret_cast<INIT_MAP_SELECT_DIALOG_PROC>(Constants::S3_EXE + 0x7fEA0);

typedef int (__thiscall *SHOW_DIALOG_PROC)(void *This, void* dialog);
SHOW_DIALOG_PROC s3_show_dialog = reinterpret_cast<SHOW_DIALOG_PROC>(Constants::S3_EXE + 0x96CD0);

typedef void (__thiscall *UPDATE_TEAM_SETUP)(game_setup_dialog* This);
UPDATE_TEAM_SETUP s3_update_team_setup = reinterpret_cast<UPDATE_TEAM_SETUP>(Constants::S3_EXE + 0x8A150);

typedef int (__thiscall *UPDATE_MAP_FILE_PROC)(map_select_dialog *This);
UPDATE_MAP_FILE_PROC s3_update_map_file = reinterpret_cast<UPDATE_MAP_FILE_PROC>(Constants::S3_EXE + 0x83760);

map_select_dialog* __fastcall init_map_select_dialog(map_select_dialog *This, void* notUsed, void* graphicsData, int a3, char a4, int a5) {
    isMapDialog = true;

    return s3_init_map_select_dialog(This, graphicsData, a3, a4, a5);
}

void __fastcall update_setup_dialog(game_setup_dialog* setupDialog) {
    auto task = g_activeTask;
    if (task && !task->isTeamSetupChanged) {
        task->isTeamSetupChanged = true;

        dprintf("Updating setup dialog (random-position: %d, goods-setting: %d)\n", task->m_randomPositions, task->m_goodsSetting);

        if (task->m_randomPositions) {
            // We run an update once to fully load teams as per selected map defaults,
            // and then a second update after we change the game type settings.
            s3_update_team_setup(setupDialog);
            setupDialog->gameType1 = 2;
            setupDialog->gameType2 = 4;
        }

        setupDialog->goodsSetting = task->m_goodsSetting;

        // User has to take care of changing teams if different teams are desired, and
        // then click continue to actually create the game.
        task->onCompleted();
    }

    s3_update_team_setup(setupDialog);
}

int __fastcall show_dialog(void *This, void* notUsed, void *dialog) {
    if (!isMapDialog) {
        return s3_show_dialog(This, dialog);
    }

    isMapDialog = false;
    map_select_dialog* mapDialog = static_cast<map_select_dialog*>(dialog);
    auto task = g_activeTask;
    if (task && !task->isMapSelected) {
        task->isMapSelected = true;

        // For saves, no changes to team setup are needed
        task->isTeamSetupChanged = task->m_mapCategory == MAP_CATEGORY_SAVE;

        dprintf("Selecting map (category=%d, mapSize=%d, mirrorType=%d, mapName=%s)\n", task->m_mapCategory, task->m_mapSize, task->m_mirrorType, task->m_mapName.c_str());
        mapDialog->mapSubCategory = task->m_mapCategory;

        if (mapDialog->mapSubCategory == MAP_CATEGORY_SINGLE_PLAYER) {
            s3_str_init(&mapDialog->mapFolderName, "Single");
        } else if (mapDialog->mapSubCategory == MAP_CATEGORY_MULTI_PLAYER) {
            s3_str_init(&mapDialog->mapFolderName, "Multi");
        } else if (mapDialog->mapSubCategory == MAP_CATEGORY_USER) {
            s3_str_init(&mapDialog->mapFolderName, "User");
        } else if (mapDialog->mapSubCategory == MAP_CATEGORY_SAVE) {
            s3_str_init(&mapDialog->mapFolderName, "Save\\Multi");
        }

        bool hasError = false;
        if (mapDialog->mapSubCategory != MAP_CATEGORY_RANDOM) {
            s3_str_init(&mapDialog->mapFileName, task->m_mapName.c_str());
            if (s3_update_map_file(mapDialog)) {
                task->m_errorMessage = fmt::format(
                    g_activeTask->GetI18n()->__("map_could_not_be_opened", "The map {mapName} could not be opened."),
                    fmt::arg("mapName", task->m_mapName)
                );
                task->m_hasError = true;
            }
        } else {
            mapDialog->mapType = 2;
            s3_set_dropdown_value(&mapDialog->mapSizeDropdownStart, task->m_mapSize);
            s3_set_dropdown_value(&mapDialog->mirrorDropdownStart, task->m_mirrorType);
        }

        if (task->m_mapCategory == MAP_CATEGORY_SAVE) {
            // For saves, we will immediately go to the pre game lobby after this, so there
            // is nothing else to do.
            task->onCompleted();
        }

        if (!task->m_hasError) {
            // We do not show the dialog, but return the result code immediately.
            return 2;
        }

        // In case of map loading error, close the task (nothing we can do anymore, user has to take over completely).
        task->onCompleted();
    }

    auto result = s3_show_dialog(This, dialog);
    dprintf("Selected map (map name=%s, folder name=%s)\n", mapDialog->mapFileName.sText, mapDialog->mapFolderName.sText);

    return result;
}

void HostGameTask::OnRenderFrame(RenderContext* context) {
    if (isCompleted) {
        if (m_hasError) {
            auto popupTitle = m_i18n->__("setup_error", "Setup Error");
            ImGui::OpenPopup(popupTitle.c_str());
            if (ImGui::BeginPopupModal(popupTitle.c_str(), &m_hasError, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_AlwaysAutoResize)) {
                ImGui::Text("%s", m_errorMessage.c_str());
                ImGui::Text("%s", m_i18n->__("setup_error_how_to_fix", "You can continue with running all set-up manually or return to the aLobby and try again.").c_str());
                if (ImGui::Button("OK")) {
                    m_hasError = false;
                }
                ImGui::EndPopup();
            }
        }

        return;
    }

    ImGuiWidgets::LoadingOverlay(m_i18n->__("setting_up_game", "Setting Up Game...").c_str());
}

void HostGameTask::install() {
    if (g_activeTask) {
        g_activeTask->onCompleted();
    }
    g_activeTask = this;

    initMapDialogHook.hook(reinterpret_cast<void**>(&s3_init_map_select_dialog), reinterpret_cast<void*>(init_map_select_dialog));
    showDialogHook.hook(reinterpret_cast<void**>(&s3_show_dialog), reinterpret_cast<void*>(show_dialog));
    updateTeamSetupHook.hook(reinterpret_cast<void**>(&s3_update_team_setup),
                             reinterpret_cast<void*>(update_setup_dialog));
}

void HostGameTask::onCompleted() {
    if (isCompleted) {
        return;
    }
    isCompleted = true;

    if (g_activeTask == this) {
        g_activeTask = nullptr;
    }
}

HostGameTask::~HostGameTask() {
    onCompleted();
}

void HostGameTask::OnBeforeMenuDrawLoop(lp_menu_draw_thread* menuDrawThread) {
    if (isCompleted) {
        return;
    }

    if (menuDrawThread->activeDialogId == START_SCREEN_DIALOG_ID) {
        s3_update_menu_screen(menuDrawThread, START_MENU_MULTIPLAYER_LAN_BTN);
    } else if (menuDrawThread->activeDialog) {
        if (menuDrawThread->activeDialogId == CONNECTION_DIALOG_ID)
        {
            if (!menuDrawThread->menuSharedData->isDirectPlayActionRunning)
            {
                connection_dialog* connectionDialog = (connection_dialog*)menuDrawThread->activeDialog;
                menu_shared_data* menuData = menuDrawThread->menuSharedData;

                if (menuData->numConnectionShortcuts > 0)
                {
                    if (connectionDialog->directplayProviderList->selectedValue == -1)
                    {
                        connectionDialog->directplayProviderList->selectedValue = 0;
                        s3_select_conn_provider(connectionDialog, 0);
                    }

                    // The provider change is not active immediately as it has to be processed by the menu net thread first, so we have to check again for the value here until it becomes selected.
                    if (connectionDialog->directplayProviderList->selectedValue != -1)
                    {
                        if (!m_nickname.empty())
                        {
                            str_struct nickname;
                            s3_str_init(&nickname, m_nickname.c_str());
                            s3_str_init(&connectionDialog->nicknameInput->inputValue, &nickname);
                        }

                        s3_host_new_game(connectionDialog, 0);
                    }
                }
            }
        }
    }
}