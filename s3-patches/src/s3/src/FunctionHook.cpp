#include "FunctionHook.h"
#include "stdafx.h"

extern "C" {
#include "detours.h"
}

void FunctionHook::hook(void** ppPointer, void* pDetour) {
    revert();

    m_ppPointer = ppPointer;
    m_pDetour = pDetour;

    DetourTransactionBegin();
    DetourAttach(ppPointer, pDetour);
    DetourTransactionCommit();
}

void FunctionHook::revert() {
    if (!isActive()) {
        return;
    }

    DetourTransactionBegin();
    DetourDetach(m_ppPointer, m_pDetour);
    DetourTransactionCommit();
}

bool FunctionHook::isActive() {
    return m_pDetour != nullptr || m_ppPointer != nullptr;
}

FunctionHook::~FunctionHook() {
    revert();
}
