#ifndef SIEDLER3_FIXES_PREGAMELOBBYPATCH_H
#define SIEDLER3_FIXES_PREGAMELOBBYPATCH_H

#include "Patch.h"
#include "S3EventDispatcher.h"
#include "PatchGameSettings.h"
#include "GameSettingsManager.h"
#include "DetourHooker.h"
#include "I18n.h"
#include "dll_config.h"
#include <hacklib/WindowOverlay.h>
#include <hacklib/DrawerD3D.h>
#include <hacklib/Patch.h>

class PreGameLobbyPatch : public Patch, public AfterMenuDrawLoopListener, public GuiListener {
public:
    PreGameLobbyPatch(GameSettingsManager* gameSettingsManager, I18n* i18n, Config* config, bool* guiActive)
        : m_gameSettingsManager(gameSettingsManager), m_i18n(i18n), m_config(config), m_guiActive(guiActive) {}

    void enable();
    void disable();
    void OnAfterMenuDrawLoop(lp_menu_draw_thread* menuDrawThread);
    void OnRenderFrame(RenderContext* context);
    void OnNetSessionUpdate(net_session* pSession);
    void SendGameSettings(void* sendingPlayer, void* targetPlayer);
    void OnNewGameSettings(PatchGameSettings* newGameSettings);
    bool IsGuiActive() { return *m_guiActive; }
    PatchGameSettings GetGameSettings() { return m_gameSettingsManager->getGameSettings(); }
    I18n* GetI18n() { return m_i18n; }
    int GetRandomMirrorType() { return m_mirrorType; }

private:
    Config* m_config;
    bool* m_guiActive;
    bool m_isJoined = false;
    bool m_showSettingsDialog = false;
    int m_mirrorType = -1;
    GameSettingsManager* m_gameSettingsManager;
    I18n* m_i18n;
    std::atomic<bool> m_sendGameSettings = false;
    FunctionHook m_onJoinHook, m_netSessionHook, m_dplaySendHook;
    DetourHooker m_hooker;
    hl::Patch m_gameTypeTextPatch;
    void renderSettingsActions();
    void renderSettingsDescription();
    void renderChangeSettingsPopup();
    void patchGameTypeText();
};


#endif // SIEDLER3_FIXES_PREGAMELOBBYPATCH_H
