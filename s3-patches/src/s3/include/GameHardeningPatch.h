#ifndef SIEDLER3_FIXES_GAMEHARDENINGPATCH_H
#define SIEDLER3_FIXES_GAMEHARDENINGPATCH_H

#include "Patch.h"
#include "DetourHooker.h"
#include "s3_listeners.h"

/**
 * This fixes errors that typically occur later on in the game when memory areas are re-used.
 *
 * The patch clears parts of memory before they are used again to ensure stable behavior just as if it was used for the first time.
 *
 * One notable error fixed by this patch is a mine related SE that occurs when selecting a mine during construction,
 * but it likely addresses a whole lot of these types of errors.
 */
class GameHardeningPatch : public Patch, public AfterGameInitListener {
    bool* m_isVanillaVersion;
    bool m_isApplied = false;
    DetourHooker m_hooker;

public:
    GameHardeningPatch(bool* isVanillaVersion): m_isVanillaVersion(isVanillaVersion) { }
    void enable() override;
    void disable() override;
    void OnAfterGameInit(logic_thread* logicThread) override;

private:
    void applyPatches();
};


#endif // SIEDLER3_FIXES_GAMEHARDENINGPATCH_H
