#ifndef SIEDLER3_FIXES_IPCSERVER_H
#define SIEDLER3_FIXES_IPCSERVER_H

#include <string>
#include <queue>
#include <mutex>
#include <windows.h>

typedef struct ipc_event {
    unsigned short eventType;
    unsigned short dataLength;
} ipc_event;

class IpcServer
{
public:
    IpcServer(std::string pipeName): m_pipeName(pipeName) {
        m_outgoingEventHandle = CreateEvent(NULL, TRUE, FALSE, NULL);
        if (!m_outgoingEventHandle) {
            throw std::exception("failed to create outgoing event handle");
        }
    }
    ~IpcServer();
    void Run();
    void SendEvent(unsigned char eventType, BYTE* eventData, unsigned short eventSize);
    bool IsTerminated();
    void RequestTermination();
private:
    std::string m_pipeName;
    std::queue<BYTE*> m_events;
    std::mutex m_eventLock;
    bool m_isTerminated = false;
    bool m_shouldTerminate = false;
    bool m_isStarted = false;
    HANDLE m_outgoingEventHandle;
    HANDLE m_hPipe;
    OVERLAPPED m_pipeActivity;

    void FlushEvents();
};

#endif // SIEDLER3_FIXES_IPCSERVER_H
