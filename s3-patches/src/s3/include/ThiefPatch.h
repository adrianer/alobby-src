#ifndef SIEDLER3_FIXES_THIEFPATCH_H
#define SIEDLER3_FIXES_THIEFPATCH_H

#include <hacklib/Patch.h>
#include "Patch.h"
#include "S3EventDispatcher.h"
#include "I18n.h"

class ThiefPatch : public Patch, public AfterGameInitListener
{
public:
    ThiefPatch(I18n* i18n, bool alwaysDisabled):
        m_alwaysDisabled(alwaysDisabled), m_i18n(i18n) {
    }

    void enable();
    void disable();
    void OnAfterGameInit(logic_thread* logicThread);
    I18n* GetI18n() { return m_i18n; }

private:
    I18n* m_i18n;
    bool m_alwaysDisabled;
    hl::Patch lookingForGoodPatch, issueCommandPatch;
    void applyLookingForGoodPatch();
    void applyIssueCommandPatch();
};


#endif // SIEDLER3_FIXES_THIEFPATCH_H
