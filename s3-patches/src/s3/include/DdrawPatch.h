#ifndef SIEDLER3_FIXES_DDRAWPATCH_H
#define SIEDLER3_FIXES_DDRAWPATCH_H

#include <hacklib/Patch.h>
#include <hacklib/Hooker.h>
#include <atomic>
#include "Patch.h"
#include "FunctionHook.h"
#include "s3_listeners.h"
#include "dll_config.h"
#include "DetourHooker.h"

extern "C" {
#include "ddraw_proxy.h"
};

class DdrawPatch : public Patch, public WindowMessageListener, public AfterGameInitListener, public GameLeaveListener
{
private:
    Config* m_config;
    hl::Patch m_ddPatch, m_boundaryCheckPatch, m_changeDisplayModePatch1, m_changeDisplayModePatch2, m_changeDisplayModePatch3;
    FunctionHook ddResultHandlingHook, initSurfacesHook, transferDataHook, initDisplaySettingsHook, handleResolutionChangeHook;
    DetourHooker m_hooker;
    bool m_isInGame = false;
    bool m_canZoom = false;
    std::atomic<int> m_requestedZoomLevel = 0;
    int m_activeZoomLevel = 0;

public:
    DdrawPatch(Config* config): m_config(config) {
    }

    void enable();
    void disable();
    bool OnWindowMessage(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);
    void OnAfterGameInit(logic_thread* logicThread);
    void OnGameLeave(logic_thread* logicThread, bool isGameOver);

    void enableWindowMode();
    void patchOldGraphicMode();
    bool UpdateDisplaySettings(void* drawer, current_draw_data* drawData);
    void applyBoundaryCheckPatch();
    void applyIgnoreDisplayModeChangePatch(hl::Patch* patchHolder, uintptr_t startPos);
};

#endif
