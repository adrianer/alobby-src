#ifndef SIEDLER3_FIXES_IMGUI_UTILS_H
#define SIEDLER3_FIXES_IMGUI_UTILS_H

#include <imgui.h>

#define S3_YELLOW IM_COL32(248, 217, 1, 255)
#define S3_YELLOW_VEC4 ImVec4(248, 217, 1, 1)

#define ImScaledVec2(arg1, arg2) ImVec2(static_cast<int>(ImGui::GetIO().DisplaySize.x * static_cast<float>(arg1) / 800.0f), \
                                        static_cast<int>(ImGui::GetIO().DisplaySize.y * static_cast<float>(arg2) / 600.0f))

namespace ImGuiWidgets {
void LoadingOverlay(const char* label);
void HelpMarker(const char* description);
}


#endif // SIEDLER3_FIXES_IMGUI_UTILS_H
