#ifndef SIEDLER3_FIXES_ZOOMFUNCTIONPATCH_H
#define SIEDLER3_FIXES_ZOOMFUNCTIONPATCH_H

#include <hacklib/Patch.h>
#include "Patch.h"
#include "FunctionHook.h"

extern bool IsNewGraphicMode;

class ZoomFunctionPatch : public Patch {
public:
    ZoomFunctionPatch() {}
    void enable();
    void disable();

private:
    //Read mouse wheel input value to adjust zoom level
    FunctionHook setZoomLevelHook;

    //Rearrange when the selection box is drawn so it is affected by the zoom
    //Includes call to actual zoom
    FunctionHook selectionBoxHook1;
    FunctionHook selectionBoxHook2;
    FunctionHook selectionBoxHook3;

    //Adjust cursor position according to zoom level
    FunctionHook changeCursorPosHook;

    void applyPatch();
};

#endif