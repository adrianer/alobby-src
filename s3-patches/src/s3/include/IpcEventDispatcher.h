#ifndef SIEDLER3_FIXES_IPCEVENTDISPATCHER_H
#define SIEDLER3_FIXES_IPCEVENTDISPATCHER_H

#include "IpcServer.h"
#include "s3_listeners.h"
#include "ipc_events.h"
#include "PatchGameSettings.h"
#include "GameSettingsManager.h"

class IpcEventDispatcher : public AfterMenuDrawLoopListener
{
public:
    IpcEventDispatcher(IpcServer* server, GameSettingsManager* manager): m_ipcServer(server), m_gameSettingsManager(manager) {
        memset(&m_preGameLobbyData, 0, sizeof(ipc_event_pre_game_lobby_event_data));
    }

    void OnAfterMenuDrawLoop(lp_menu_draw_thread* menuDrawThread);

private:
    IpcServer* m_ipcServer;
    int m_lastDialogId = -1;
    bool m_firstLobbyUpdate = false;
    ipc_event_pre_game_lobby_event_data m_preGameLobbyData;
    GameSettingsManager* m_gameSettingsManager;
    PatchGameSettings m_processedGameSettings;
};

#endif // SIEDLER3_FIXES_IPCEVENTDISPATCHER_H
