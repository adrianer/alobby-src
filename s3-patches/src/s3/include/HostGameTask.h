#ifndef SIEDLER3_FIXES_HOSTGAMETASK_H
#define SIEDLER3_FIXES_HOSTGAMETASK_H

#include <chrono>
#include <hacklib/WindowOverlay.h>
#include <hacklib/DrawerD3D.h>
#include "s3.h"
#include "S3EventDispatcher.h"
#include "I18n.h"

using namespace std::chrono;

class HostGameTask : public BeforeMenuDrawLoopListener, public GuiListener {
public:
    std::string m_nickname, m_mapName, m_errorMessage;
    unsigned char m_mapCategory, m_mapSize, m_mirrorType, m_goodsSetting;
    bool isCompleted = false, isMapSelected = false, isTeamSetupChanged = true, m_randomPositions, m_hasError = false;
    FunctionHook initMapDialogHook, showDialogHook, updateTeamSetupHook;
    I18n* m_i18n;

    HostGameTask(I18n* i18n, std::string nickname, std::string mapName, unsigned char mapCategory, unsigned char mapSize,
                 unsigned char mirrorType, bool randomPositions, unsigned char goodsSetting)
        : m_i18n(i18n)
        , m_nickname(nickname)
        , m_mapName(mapName)
        , m_mapCategory(mapCategory)
        , m_mapSize(mapSize)
        , m_mirrorType(mirrorType)
        , m_randomPositions(randomPositions)
        , m_goodsSetting(goodsSetting) {
        install();
    };
    ~HostGameTask();

    void OnBeforeMenuDrawLoop(lp_menu_draw_thread* menuDrawThread);
    void OnRenderFrame(RenderContext* context);
    void onCompleted();
    I18n* GetI18n() { return m_i18n; }

private:
    void install();
};


#endif // SIEDLER3_FIXES_HOSTGAMETASK_H
