#ifndef SIEDLER3_FIXES_COALBUGFIX_H
#define SIEDLER3_FIXES_COALBUGFIX_H

#include "Patch.h"
#include <hacklib/Patch.h>

class CoalBugFix : public Patch {
private:
    hl::Patch m_processingExitConditionPatch;

public:
    void enable() override;
    void disable() override;
};


#endif // SIEDLER3_FIXES_COALBUGFIX_H
