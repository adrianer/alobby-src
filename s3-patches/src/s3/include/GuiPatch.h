#ifndef SIEDLER3_FIXES_GUIPATCH_H
#define SIEDLER3_FIXES_GUIPATCH_H

#include <hacklib/WindowOverlay.h>
#include <hacklib/DrawerD3D.h>
#include "Patch.h"
#include "S3EventDispatcher.h"
#include "GameSettingsManager.h"
#include <thread>

class GuiPatch : public Patch, public BeforeMenuDrawLoopListener, public AfterGameInitListener {
private:
    bool m_isInitialized = false;
    hl::WindowOverlay m_guiOverlay;
    FunctionHook m_wmHook;
    std::thread m_renderThread;
    bool m_isActive = false;
    bool m_isStarted = false;
    bool m_needsRestore = false;
    bool m_scaleSet = false;
    bool* m_guiActive;
    RenderContext m_renderContext;
    S3EventDispatcher* m_dispatcher;
    GameSettingsManager* m_gameSettingsManager;

public:
    GuiPatch(S3EventDispatcher* dispatcher, bool* guiActive, GameSettingsManager* gameSettingsManager)
        : m_dispatcher(dispatcher), m_guiActive(guiActive), m_gameSettingsManager(gameSettingsManager) {}
    ~GuiPatch();
    void enable();
    void disable();
    void OnBeforeMenuDrawLoop(lp_menu_draw_thread* menuDrawThread);
    void OnAfterGameInit(logic_thread* logicThread);
    bool init();
    void render();
    void shutdown();
    void terminateRenderer();
};


#endif // SIEDLER3_FIXES_GUIPATCH_H
