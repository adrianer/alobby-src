#ifndef SIEDLER3_FIXES_PATCHMANAGER_H
#define SIEDLER3_FIXES_PATCHMANAGER_H

#include <vector>
#include "Patch.h"
#include "S3EventDispatcher.h"

class PatchManager
{
private:
    std::vector<Patch*> m_patches;
    S3EventDispatcher* m_s3EventDispatcher;

public:
    PatchManager(S3EventDispatcher* dispatcher): m_s3EventDispatcher(dispatcher) {
    }

    void enable(Patch* patch) {
        patch->enable();
        m_s3EventDispatcher->AddListener(patch);
        m_patches.push_back(patch);
    }

    template<typename T>
    void disable() {
        m_patches.erase(
            std::remove_if(m_patches.begin(), m_patches.end(), [&](Patch* p) {
                auto searchedPatch = dynamic_cast<T*>(p);
                if (searchedPatch != nullptr) {
                    p->disable();
                    m_s3EventDispatcher->RemoveListener(p);
                    delete searchedPatch;

                    return true;
                }

                return false;
            }),
            m_patches.end());
    }
};


#endif // SIEDLER3_FIXES_PATCHMANAGER_H
