#ifndef SIEDLER3_FIXES_DEBUG_CONSOLE_H
#define SIEDLER3_FIXES_DEBUG_CONSOLE_H

#ifndef NDEBUG
#include <cstdio>

#define dprintf(format, ...) printf(format, ##__VA_ARGS__)
#else
#define dprintf(...)
#endif

#endif // SIEDLER3_FIXES_DEBUG_CONSOLE_H
