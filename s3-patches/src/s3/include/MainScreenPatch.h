#ifndef SIEDLER3_FIXES_MAINSCREENPATCH_H
#define SIEDLER3_FIXES_MAINSCREENPATCH_H

#include "Patch.h"
#include "s3_listeners.h"
#include "dll_config.h"
#include "MachineCodePatchBuilder.h"

class MainScreenPatch : public Patch, public GuiListener {
private:
    Config* m_config;
    hl::Patch m_hideMultiplayerInternetButtonPatch;

public:
    MainScreenPatch(Config* config): m_config(config) { }
    void enable() override;
    void disable() override;
    void OnRenderFrame(RenderContext* context) override;
};


#endif // SIEDLER3_FIXES_MAINSCREENPATCH_H
