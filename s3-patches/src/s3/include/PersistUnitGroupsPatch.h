#ifndef SIEDLER3_FIXES_PERSISTUNITGROUPSPATCH_H
#define SIEDLER3_FIXES_PERSISTUNITGROUPSPATCH_H

#include "Patch.h"
#include "s3_listeners.h"
#include "DetourHooker.h"

class PersistUnitGroupsPatch : public Patch, public SaveListener, public LoadSaveListener {
    DetourHooker m_hooker;

public:
    void enable() override;
    void disable() override;
    void OnSave(logic_thread* logicThread, void* saveFile) override;
    void OnLoadSave(logic_thread* logicThread, void* saveFile) override;
};


#endif // SIEDLER3_FIXES_PERSISTUNITGROUPSPATCH_H
