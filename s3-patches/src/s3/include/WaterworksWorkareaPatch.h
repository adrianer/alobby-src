#ifndef SIEDLER3_FIXES_WATERWORKSWORKAREAPATCH_H
#define SIEDLER3_FIXES_WATERWORKSWORKAREAPATCH_H

#include <hacklib/Patch.h>
#include <hacklib/Hooker.h>
#include "Patch.h"

class WaterworksWorkareaPatch : public Patch
{
public:
    void enable();
    void disable();

private:
    hl::Hooker hooker;
    hl::Patch maintainWorkLocation;
};


#endif // SIEDLER3_FIXES_WATERWORKSWORKAREAPATCH_H
