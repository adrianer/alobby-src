#ifndef SIEDLER3_FIXES_BUILDERPATHFINDING_H
#define SIEDLER3_FIXES_BUILDERPATHFINDING_H

#include "Patch.h"
#include <hacklib/Patch.h>
#include "FunctionHook.h"

class BuilderPathFinding: public Patch
{
private:
    hl::Patch dataPatch1, dataPatch2, dataPatch3;
    FunctionHook pathFindingHook;

public:
    void enable();
    void disable();
};

#endif // SIEDLER3_FIXES_BUILDERPATHFINDING_H
