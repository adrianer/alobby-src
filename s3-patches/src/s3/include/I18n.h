#ifndef SIEDLER3_FIXES_I18N_H
#define SIEDLER3_FIXES_I18N_H

#include <string>
#include <map>
#include <spirit_po/catalog.hpp>
#include <windows.h>

enum Language {
    GERMAN,
    ENGLISH,
    ITALIAN,
    FRENCH,
    POLISH,
    // TODO: add the remaining languages
};

#define CP_WINDOWS 1252

/**
 * Converts a UTF-8 encoded string to Windows 1252 codepage encoding.
 */
std::string utf8ToWindows(std::string utf8Str);

class I18n {
    Language m_activeLanguage;
    spirit_po::default_catalog m_catalog;

public:
    I18n();
    void setActiveLanguage(Language language);
    std::string __(std::string translationId, std::string englishText);

private:
    spirit_po::default_catalog loadTranslations();
    std::string findTranslationFile();
};

#endif // SIEDLER3_FIXES_I18N_H
