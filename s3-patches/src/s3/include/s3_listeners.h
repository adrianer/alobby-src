#ifndef SIEDLER3_FIXES_S3_LISTENERS_H
#define SIEDLER3_FIXES_S3_LISTENERS_H

#include "s3.h"

typedef struct RenderContext {
    int activeDialogId;
} GuiData;

class Listener {
public:
    virtual ~Listener() {};
};

class GameOverListener: public Listener {
public:
    virtual void OnGameOver(logic_thread* logicThread) = 0;
};

class BeforeGameLoopListener: public Listener {
public:
    virtual void OnBeforeGameLoop(logic_thread* logicThread) = 0;
};

class AfterGameInitListener : public Listener {
public:
    virtual void OnAfterGameInit(logic_thread* logicThread) = 0;
};

class WindowMessageListener : public Listener {
public:
    virtual bool OnWindowMessage(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam) = 0;
};


class AfterDrawListener : public Listener {
public:
    virtual void OnAfterDraw(game_drawer* drawer, current_draw_data* currentDrawData) = 0;
};

class BeforeUpdateDrawDataListener : public Listener {
public:
    virtual void OnBeforeUpdateDrawData(logic_thread* logicThread) = 0;
};

class BeforeMenuDrawLoopListener : public Listener {
public:
    virtual void OnBeforeMenuDrawLoop(lp_menu_draw_thread* menuDrawThread) = 0;
};

class AfterMenuDrawLoopListener : public Listener {
public:
    virtual void OnAfterMenuDrawLoop(lp_menu_draw_thread* menuDrawThread) = 0;
};

class SaveListener : public Listener {
public:
    virtual void OnSave(logic_thread* logicThread, void* saveFile) = 0;
};

class LoadSaveListener : public Listener {
public:
    virtual void OnLoadSave(logic_thread* logicThread, void* saveFile) = 0;
};

class LoadSaveStage1Listener : public Listener {
public:
    virtual void OnLoadSave(logic_thread* logicThread, void* saveFile) = 0;
};

class GameLeaveListener : public Listener {
public:
    virtual void OnGameLeave(logic_thread* logicThread, bool isGameOver) = 0;
};

class BuildingCreatedListener : public Listener {
public:
    virtual void OnBuildingCreated(game_data* gameData, unsigned int newBuildingIndex) = 0;
};

class GuiListener : public Listener {
public:
    virtual void OnRenderFrame(RenderContext* context) = 0;
};

#endif // SIEDLER3_FIXES_S3_LISTENERS_H
