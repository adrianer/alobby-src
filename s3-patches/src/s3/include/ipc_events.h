#ifndef SIEDLER3_FIXES_IPC_EVENTS_H
#define SIEDLER3_FIXES_IPC_EVENTS_H

#include "s3.h"

#define IPC_EVENT_DIALOG_CHANGED 1
#define IPC_EVENT_TYPE_PRE_GAME_LOBBY_UPDATE 2
#define IPC_EVENT_TYPE_STATS_SCREEN_AVAILABLE 3

#define SLOT_STATUS_EMPTY 0
#define SLOT_STATUS_HUMAN_PLAYER 1
#define SLOT_STATUS_COMP 2
#define SLOT_STATUS_CLOSED 4

#define GAME_MODE_RANDOM_BALANCING (1 << 0)
#define GAME_MODE_MORE_RESOURCES (1 << 1)
#define GAME_MODE_NEW_THIEVES (1 << 2)
#define GAME_MODE_STEALING_ALLOWED (1 << 3)
#define GAME_MODE_INCREASED_TRANSPORT_LIMIT (1 << 4)

typedef struct ipc_event_stats_player {
} ipc_event_stats_player;

typedef struct ipc_event_stats_update_data {
    unsigned int gameTick;
    ipc_event_stats_player playerStats[20];
} ipc_event_stats_update_data;

typedef struct ipc_event_pre_game_lobby_player {
    char name[20];
    unsigned short slotStatus;
    unsigned short team;
} ipc_event_pre_game_lobby_player;

typedef struct ipc_event_pre_game_lobby_event_data {
    unsigned char isJoined;
    unsigned char isHost;
    bool isEconomyMode;
    bool isAmazonAllowed;
    unsigned char goodsSetting;
    bool isSaveGame;
    unsigned short numPlayers;
    ipc_event_pre_game_lobby_player players[20];
    unsigned int gameModes;
    char mapName[260];
    char mapFileName[260];
} ipc_event_pre_game_lobby_event_data;

typedef struct ipc_event_dialog_changed {
    int oldDialogId;
    int newDialogId;
} ipc_event_dialog_changed;

typedef struct ipc_event_stats_screen_available_data {
    char screenPath[260];
} ipc_event_stats_screen_available;

#endif // SIEDLER3_FIXES_IPC_EVENTS_H
