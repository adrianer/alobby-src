#ifndef SIEDLER3_FIXES_ENHANCEDWIDESCREENFIX_H
#define SIEDLER3_FIXES_ENHANCEDWIDESCREENFIX_H

#include <hacklib/Patch.h>
#include "Patch.h"

class EnhancedWidescreenFix : public Patch {

public:
    EnhancedWidescreenFix() {}
    void enable();
    void disable();

private:
    hl::Patch patches[67];

    void applyPatch();
};

#endif