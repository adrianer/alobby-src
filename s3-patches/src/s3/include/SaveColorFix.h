#ifndef SIEDLER3_FIXES_SAVECOLORFIX_H
#define SIEDLER3_FIXES_SAVECOLORFIX_H

#include "Patch.h"
#include <hacklib/Patch.h>

class SaveColorFix : public Patch
{
    hl::Patch patch;

public:
    void enable();
    void disable();
};

#endif // SIEDLER3_FIXES_SAVECOLORFIX_H
