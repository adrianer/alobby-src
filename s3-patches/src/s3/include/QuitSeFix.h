#ifndef SIEDLER3_FIXES_QUITSEFIX_H
#define SIEDLER3_FIXES_QUITSEFIX_H

#include <hacklib/Patch.h>
#include "Patch.h"

class QuitSeFix: public Patch
{
private:
    hl::Patch patchXCoordinate, patchYCoordinate;

public:
    QuitSeFix() {
    }
    void enable();
    void disable();

private:
    void applyPatchXCoordinate();
    void applyPatchYCoordinate();
};

#endif // SIEDLER3_FIXES_QUITSEFIX_H
