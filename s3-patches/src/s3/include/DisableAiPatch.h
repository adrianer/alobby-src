#ifndef SIEDLER3_FIXES_DISABLEAIPATCH_H
#define SIEDLER3_FIXES_DISABLEAIPATCH_H

#include "Patch.h"
#include "s3_listeners.h"

/**
 * Disables the AI for games where it has not been present from the beginning of the game.
 *
 * This is intended for multiplayer games where a human player leaves the game at some point
 * for whatever reason. Then, that player will essentially act like a player that is AFK.
 *
 * The main reason for this is to prevent AI related game de-syncs.
 */
class DisableAiPatch : public Patch, public AfterGameInitListener, public SaveListener, public LoadSaveListener {
    bool* m_isVanillaVersion;
    bool m_isAiPresentAtGameStart;
    hl::Patch m_patch;

public:
    DisableAiPatch(bool* isVanillaVersion): m_isVanillaVersion(isVanillaVersion) {}
    void enable() override;
    void disable() override;
    void OnAfterGameInit(logic_thread* logicThread) override;
    void OnSave(logic_thread* logicThread, void* saveFile) override;
    void OnLoadSave(logic_thread* logicThread, void* saveFile) override;
    bool hasGameAi(game_data* gameData);
    void disableAi();
};


#endif // SIEDLER3_FIXES_DISABLEAIPATCH_H
