#ifndef SIEDLER3_FIXES_FREEZEFIX_H
#define SIEDLER3_FIXES_FREEZEFIX_H

#include <hacklib/Patch.h>
#include <hacklib/Hooker.h>
#include "Patch.h"

class FreezeFix: public Patch
{
private:
    hl::Patch patch, triggerFreezePatch;

public:
    FreezeFix() {}
    void enable();
    void disable();

private:
    void triggerFreezeCondition();
    void applyPatch();
};

#endif // SIEDLER3_FIXES_FREEZEFIX_H
