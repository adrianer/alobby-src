#ifndef SIEDLER3_FIXES_S3_H
#define SIEDLER3_FIXES_S3_H

#include <IDirectDrawSurface.h>
#include "stdafx.h"
#include "constants.h"
#include <set>
#include <string>

#define SETTLER_TYPE_BUILDER 2

#define SETTLER_IS_HEADING_TO_CONSTRUCTION_SITE 17

#define SETTLER_BITMASK_HAS_NOT_MOVED_SINCE_LAST_TICK 1

#define START_MENU_MULTIPLAYER_LAN_BTN 7

#define START_SCREEN_DIALOG_ID 0
#define CONNECTION_DIALOG_ID 2
#define PRE_GAME_LOBBY_DIALOG_ID 3
#define GAME_SETUP_DIALOG_ID 9
#define STATS_SCREEN_DIALOG_ID 15

#define MESSAGE_BOX_OK_BUTTON 1
#define MESSAGE_BOX_CANCEL_BUTTON 2
#define MESSAGE_BOX_EDIT_CONTROL_BUTTON 4

#define GAME_RESULT_UNKNOWN -1
#define GAME_RESULT_LOST 0
#define GAME_RESULT_WON 1
#define GAME_RESULT_DRAW 2

#define SETTING_TYPE_SETTLER_USAGE 11
#define SETTING_TYPE_TOOL_AND_WEAPON_PRODUCTION 8
#define SETTING_TYPE_GOODS_DISTRIBUTION 17
#define SETTING_TYPE_STORAGE 10

#define SETTING_ID_CARRIER_PERCENTAGE 1
#define SETTING_ID_DIGGER_PERCENTAGE 2
#define SETTING_ID_BUILDER_PERCENTAGE 3
#define SETTING_ID_RECRUITING 10
#define SETTING_UP 1
#define SETTING_DOWN 0

#define SETTING_ID_WEAPON_PRODUCTION_SWORDS 18
#define SETTING_ID_WEAPON_PRODUCTION_BOWS 19
#define SETTING_ID_WEAPON_PRODUCTION_SPEARS 20

#define SETTING_SCALE_MIN 0
#define SETTING_SCALE_MAX 16

#define SETTING_ID_COAL_TO_GOLD_SMELTER 2
#define SETTING_ID_COAL_TO_WEAPON_SMITH 3
#define SETTING_ID_COAL_TO_TOOL_SMITH 4
#define SETTING_ID_COAL_TO_DISTILLERY 5
#define SETTING_ID_GRAIN_TO_BREWERY 25
#define SETTING_ID_WATER_TO_BREWERY 29
#define SETTING_ID_FISH_TO_COAL 40
#define SETTING_ID_FISH_TO_IRON 41
#define SETTING_ID_FISH_TO_GEM_MINE 42

#define ITEM_PLANKS 1
#define ITEM_STONES 2
#define ITEM_TRUNKS 3
#define ITEM_BREAD 4
#define ITEM_COAL 5
#define ITEM_GOLD_IRON 6
#define ITEM_IRON 7
#define ITEM_FISH 8
#define ITEM_GRAIN 9
#define ITEM_GOLD_BARS 10
#define ITEM_IRON_BARS 11
#define ITEM_SHOVEL 12
#define ITEM_HAMMER 13
#define ITEM_AX 14
#define ITEM_PICK 15
#define ITEM_SAW 16
#define ITEM_FISHING_ROD 17
#define ITEM_SWORDS 18
#define ITEM_BOWS 19
#define ITEM_SPEARS 20
#define ITEM_WINE 21
#define ITEM_FLOUR 22
#define ITEM_PIGS 23
#define ITEM_MEAT 24
#define ITEM_SULFUR 25
#define ITEM_WATER 26
#define ITEM_RICE 27
#define ITEM_GEMS 28
#define ITEM_BEER 29
#define ITEM_GUN_POWDER 30
// 31 is an unused item, looks like an empty bucket.
#define ITEM_SCYTHE 32
#define ITEM_RICE_WINE 33
#define ITEM_MEAD 34
#define ITEM_HONEY 35

#define BUILDING_STORAGE 1
#define BUILDING_WOOD_CUTTER 2
#define BUILDING_STONE_CUTTER 3
#define BUILDING_SAW_MILL 4
#define BUILDING_FORESTER 5
#define BUILDING_WATCH_TOWER 6
#define BUILDING_COAL_MINE 7
#define BUILDING_GOLD_MINE 8
#define BUILDING_IRON_MINE 9
#define BUILDING_GOLD_SMELTER 10
#define BUILDING_IRON_SMELTER 11
#define BUILDING_TOOL_SMITH 12
#define BUILDING_WEAPON_SMITH 13
#define BUILDING_WINERY 14
#define BUILDING_SMALL_TOWER 15
#define BUILDING_BIG_TOWER 16
#define BUILDING_WIND_MILL 17
#define BUILDING_FORTRESS 18
#define BUILDING_BARRACKS 19
#define BUILDING_BAKERY 20
#define BUILDING_BUTCHER 21
#define BUILDING_DISTILLERY 22
#define BUILDING_PIG_FARM 23
#define BUILDING_FARM 24
#define BUILDING_FISHERY 25
#define BUILDING_SMALL_HOUSE 26
#define BUILDING_MIDDLE_HOUSE 27
#define BUILDING_LARGE_HOUSE 28
#define BUILDING_SULFUR_MINE_2 29
#define BUILDING_WATERWORKS 30
#define BUILDING_CATAPULT_FACTORY 31
#define BUILDING_SHIPYARD 32
#define BUILDING_LANDING_PLACE 33
#define BUILDING_MARKET 34
#define BUILDING_HEALER 35
#define BUILDING_RICE_FARM 36
#define BUILDING_GEM_MINE 37
#define BUILDING_BREWERY 38
#define BUILDING_CHARBURNER 39
#define BUILDING_GUN_POWDER_MAKER 40
#define BUILDING_PYRAMID 41
#define BUILDING_SPHINX 42
#define BUILDING_BIG_TEMPLE_2 43
#define BUILDING_SMALL_TEMPLE 44
#define BUILDING_BIG_TEMPLE_3 45
#define BUILDING_SMALL_TEMPLE_2 46
#define BUILDING_BALLISTA_FACTORY 47
#define BUILDING_CANNON_FACTORY 48
#define BUILDING_DONKEY_FARM 49
#define BUILDING_GONG_FACTORY 50
#define BUILDING_APIARY 51
#define BUILDING_MEAD_MAKER 52
#define BUILDING_LAB 53
#define BUILDING_SMALL_TEMPLE_3 54
#define BUILDING_BIG_TEMPLE 55
#define BUILDING_SULFUR_MINE 56

#define SETTLER_TYPE_SWORD_LEVEL_1 5
#define SETTLER_TYPE_SWORD_LEVEL_2 32
#define SETTLER_TYPE_SWORD_LEVEL_3 35
#define SETTLER_TYPE_BOW_LEVEL_1 8
#define SETTLER_TYPE_BOW_LEVEL_2 30
#define SETTLER_TYPE_BOW_LEVEL_3 33
#define SETTLER_TYPE_SPEAR_LEVEL_1 15
#define SETTLER_TYPE_SPEAR_LEVEL_2 31
#define SETTLER_TYPE_SPEAR_LEVEL_3 34

#define RESOURCE_TYPE_FISH 0x0
#define RESOURCE_TYPE_COAL 0x10
#define RESOURCE_TYPE_IRON 0x20
#define RESOURCE_TYPE_GOLD 0x30
#define RESOURCE_TYPE_GEMS 0x40
#define RESOURCE_TYPE_SULFUR 0x50

#define get_resource_type(resource) (resource & 0xF0)
#define get_resource_count(resource) (resource & 0xF)
#define create_resource(type, count) (type + count)

#define TERRAIN_WATER_LEVEL_1 0
#define TERRAIN_WATER_LEVEL_2 1
#define TERRAIN_LAND 16
#define TERRAIN_SWAMP 21
#define TERRAIN_MOUNTAIN_1 32
#define TERRAIN_MOUNTAIN_2 33
#define TERRAIN_SHORE 48
#define TERRAIN_STREAM 96
#define TERRAIN_STREAM_2 97
#define TERRAIN_STREAM_3 98
#define TERRAIN_STREAM_4 99

#define OBJECTS_TREES_START 68
// End of trees (included; i.e. this one is still a tree)
#define OBJECTS_TREES_END 83

#define OBJECTS_STONES_START 115
#define OBJECTS_STONES_END 126
// Just crumbs, i.e. no more stones to cut.
#define OBJECTS_STONE_CRUMBS 127

#define GOODS_MAP_DEFAULT 0
#define GOODS_LOW 1
#define GOODS_MEDIUM 2
#define GOODS_HIGH 3

#define SETTING_STORE 1
#define SETTING_DO_NOT_STORE 2

#define RACE_ROMAN 0
#define RACE_EGYPT 1
#define RACE_ASIA 2
#define RACE_AMAZON 3

#define PLAYER_TYPE_EMPTY 0
#define PLAYER_TYPE_HUMAN 1
#define PLAYER_TYPE_COMP 2

// Custom Patch Chunks starting at 50 (not in the Vanilla game)
#define FILE_CHUNK_PATCH_GAME_SETTINGS 50
#define FILE_CHUNK_STATS 51
#define FILE_CHUNK_MARKERS 52
#define FILE_CHUNK_DISABLE_AI_PATCH 54
#define FILE_CHUNK_UNIT_GROUPS 55
#define FILE_CHUNK_CURRENT_STATS 56

typedef struct str_struct
{
    DWORD field0;
    const char* sText;
    signed int actualTextLength;
    signed int textCapacity;
} str_struct;


typedef struct str_struct_wchar
{
    DWORD field0;
    const wchar_t* sText;
    signed int actualTextLength;
    signed int textCapacity;
} str_struct_wchar;

typedef struct CTextSurface
{
    void* vtable;
    BYTE dict[120];
    BYTE gap_7C[8];
    HGDIOBJ hObject;
    signed int field_88;
    signed int field_8C;
    char field_90;
} CTextSurface;

typedef struct game_data_active_message
{
    BYTE gap_1[120];
} game_data_active_message;

typedef struct game_data_timer
{
    char defined;
    BYTE gap[19];
} game_data_timer;

typedef struct __declspec(align(2)) game_data_map_tile
{
    WORD settlerIndex;
    unsigned __int16 buildingIndex_or_pileIndex;
    unsigned __int8 height;
    unsigned __int8 field2;
    unsigned __int8 terrain;
    unsigned __int8 objects;
    __int8 owningPlayer;
    unsigned __int8 field3;
    unsigned __int8 bitmask;
    BYTE gap_3[5];
    unsigned __int8 ecoSectorIndex;
    unsigned __int8 resource;
    BYTE gap_4[6];
} game_data_map_tile;
static_assert(offsetof(game_data_map_tile, owningPlayer) == 0x8, "owningPlayer at offset 0x8");

typedef struct game_data_war_map_entry
{
    BYTE gap_1[960];
} game_data_war_map_entry;

typedef struct game_data_eco_sector
{
    unsigned __int8 owningPlayer;
    BYTE gap_1[117];
    BYTE statsRelated[108];
    BYTE gap_2[510];
} game_data_eco_sector;

typedef struct __declspec(align(4)) game_data_player
{
    DWORD type;
    DWORD race;
    DWORD field_1;
    DWORD team;
    signed int startX;
    signed int startY;
    DWORD field_18;
    DWORD playerSettingIndex;
    BYTE gap_1[30];
    WORD field_54;
    BYTE gap_2[30];
    WORD field_86;
    WORD field_88;
    WORD field_90;
    WORD field_92;
    BYTE gap_4_1[26];
    DWORD offensiveStrengthMultiplier;
    BYTE gap_4_2[4];
    DWORD chatOn;
    DWORD availableMana;
    DWORD availableLevelUpPoints;
    DWORD currentSpellCosts[8];
    signed int nextLevelRequiredPoints;
    DWORD madeSwordUpgrades;
    DWORD madeBowUpgrades;
    DWORD madeSpearUpgrades;
    DWORD neededSettlerCapacity;
    DWORD settlerCapacity;
    DWORD count4;
    DWORD carrierCount;
    BYTE gap_6[116];
} game_data_player;
static_assert(offsetof(game_data_player, nextLevelRequiredPoints) == 0xB4, "nextLevelRequiredPoints must be at offset 0xB4");

typedef struct construction_site
{
    char defined;
    BYTE gap_1[961];
} construction_site;

typedef struct __declspec(align(2))
{
    BYTE gap_1[6];
    WORD xCoord;
    WORD yCoord;
    BYTE gap_2[4];
} game_data_pile;

typedef struct __declspec(align(4))
{
    unsigned __int8 owningPlayer;
    unsigned __int8 type;
    unsigned __int8 race;
    unsigned __int8 someInfo;
    unsigned __int16 xCoord;
    unsigned __int16 yCoord;
    DWORD randomValue1;
    WORD randomValue2;
    WORD currentState;
    WORD workLocationIndex;
    WORD linkedSettlerIndex;
    BYTE gap_3[2];
    char field_10;
    BYTE gap_4[2];
    char field_42;
    BYTE gap_42[8];
    unsigned __int16 buildingIndex;
    unsigned __int16 targetXCoord;
    unsigned __int16 targetYCoord;
    unsigned __int16 prevXCoord;
    unsigned __int16 prevYCoord;
    char bitmask;
    BYTE gap_6[3];
    unsigned __int16 field_58;
    unsigned __int16 field_59;
    unsigned __int16 field_60;
    unsigned __int16 field_61;
    unsigned __int16 field_62;
    unsigned __int16 field_63;
    BYTE gap_7[4];
} game_data_settler;

typedef struct game_data_building
{
    unsigned __int8 owningPlayer;
    char field_1;
    const WORD centerX;
    const WORD centerY;
    const unsigned __int8 type;
    const unsigned __int8 race;
    unsigned __int16 settlerIndex;
    BYTE gap_1[23];
    unsigned __int8 constructionState;
    BYTE gap_2[0x3A - 0x22];
} game_data_building;
static_assert(offsetof(game_data_building, type) == 0x6, "Type is at offset 0x6");
static_assert(offsetof(game_data_building, constructionState) == 0x21, "constructionState is at 0x21");
static_assert(sizeof(game_data_building) == 0x3A, "game_data_building size is 0x3A");

typedef struct game_data_finished_military_building {
    BYTE gap_1[0x19];
    unsigned __int8 numComingSwords;
    unsigned __int8 numRequestedSwords;
    unsigned __int8 numComingBows;
    unsigned __int8 numRequestedBows;
    BYTE gap_2[2];
    unsigned __int8 numComingSpears;
    unsigned __int8 numRequestedSpears;
} game_data_finished_military_building;
static_assert(offsetof(game_data_finished_military_building, numComingBows) == 0x1B, "numComingBows is at 0x1B");
static_assert(offsetof(game_data_finished_military_building, numRequestedBows) == 0x1C, "numRequestedBows is at 0x1C");

typedef struct game_data_building_in_construction {
    BYTE gap_1[0x1C];
    unsigned char numAllowedBuilders;
} game_data_building_in_construction;
static_assert(offsetof(game_data_building_in_construction, numAllowedBuilders) == 0x1C, "numAllowedBuilders is at 0x1C");

typedef struct game_data_effect
{
    DWORD field_0;
    signed __int16 field_4;
    signed __int16 field_6;
    signed int field_8;
    signed int field_C;
    signed int field_10;
    signed int field_14;
    signed int field_18;
    signed int field_1C;
    signed int field_20;
    signed int field_24;
    signed __int16 field_28;
    signed __int16 field_2A;
    signed int field_2C;
    signed __int16 field_30;
    signed __int16 field_32;
    signed int field_34;
    char field_38;
    char field_39;
    char field_3A;
    BYTE gap_3B;
    signed __int16 field_3C;
    signed __int16 field_3E;
    signed int field_40;
    signed int field_44;
    signed int otherEffectIndex;
} game_data_effect;

typedef struct __declspec(align(4)) game_data_effects_data
{
    game_data_effect effects[5000];
    signed int indexLookupMap[10000];
    signed int numEffects;
    signed int field_668A4;
    signed int field_668A8;
    signed int field_668AC;
    signed int field_668B0;
    signed int field_668B4;
    signed int field_668B8;
} game_data_effects_data;

typedef struct game_data_radius_offset {
    signed int x;
    signed int y;
    BYTE gap[8];
} game_data_radius_offset;

typedef struct __declspec(align(4)) game_data
{
    DWORD* __vftable;
    signed int field_4;
    signed int field_8;
    signed int field_C;
    signed int field_10;
    signed int field_14;
    signed int field_18;
    signed int field_1C;
    signed int field_20;
    signed int field_24;
    signed int field_28;
    signed int field_2C;
    signed int field_30;
    signed int field_34;
    BYTE productionStats[184320];
    BYTE gap_38[512];
    signed __int16 field_2D238;
    BYTE gap_2D23A[10];
    signed int field_2D244;
    int field_2D248;
    signed int remainingGameMinutes;
    DWORD field_2D250;
    DWORD field_2D254;
    DWORD field_2D258;
    DWORD fieldWhichCausesHostSE;
    DWORD field_2D25F;
    signed int field_2D264;
    signed int field_2D268;
    signed int field_2D26C;
    signed int field_2D270;
    DWORD randomMirrorType;
    signed int field_2D278;
    signed int isLeftMenuChanged;
    signed int selectedProductionGood;
    signed int selectedTopLevelMenu;
    signed int selectedSecondLevelMenu;
    signed int buildingMode_selectedBuildingIndex;
    signed int field_2D290;
    signed int field_2D294;
    signed int saveState;
    signed int selectedSpellId;
    signed int selectedSettlerSubmenu;
    signed int selectedSettingId;
    signed int field_2D2A8;
    signed int field_2D2AC;
    signed int selectedSettingId_real;
    signed int field_2D2B4;
    signed int field_2D2B8;
    signed int field_2D2BC;
    signed int field_2D2C0;
    signed int field_2D2C4;
    signed int field_2D2C8;
    BYTE gap_2D2CC[4];
    DWORD mapSize;
    DWORD actingPlayer;
    DWORD field_2D2D4_1;
    DWORD randomValue;
    DWORD randomValue2;
    signed int clickXCoord;
    signed int clickYCoord;
    BYTE gap_2D2EC[4];
    signed int buildingMode_selectedBuildingType;
    signed int field_2D2F4;
    signed int field_2D2F8;
    signed int field_2D2FC;
    char field_2D300;
    char field_2D301;
    BYTE gap_2D302[2];
    signed int field_2D304;
    signed int field_2D308;
    BYTE gap_2D30C[8];
    signed int field_2D314;
    signed int field_2D318;
    signed int field_2D31C;
    BYTE activeMessageData1[4800];
    game_data_active_message activeMessageData2[42];
    BYTE eventData1[12000];
    signed int eventData1_related1;
    DWORD eventData1_related2;
    BYTE eventData2[16000];
    signed int eventData2_related1;
    DWORD eventData2_related2;
    game_data_timer activeTimers[10000];
    DWORD numActiveTimers;
    game_data_map_tile mapTiles[768][768];
    BYTE warMapIndividualEntryResetRelated[4];
    game_data_war_map_entry warMap1[48];
    game_data_war_map_entry warMap2[48];
    int currentMapCoord;
    BYTE gap_DFDC4C[39996];
    DWORD field_E07888;
    DWORD field_E0788C;
    BYTE gap_E07890[40008];
    WORD numEcoSectors;
    BYTE usedEcoSectors[256];
    DWORD numSectors;
    DWORD sectorRelated2;
    BYTE ecoSectorData2[4096];
    DWORD sectorRelated3;
    BYTE ecoSectorData3[8192];
    game_data_eco_sector ecoSectors[4096];
    BYTE activeTradeData[90000];
    DWORD activeTradesSize;
    DWORD activeIndex;
    DWORD field_110A57C_2;
    DWORD field_110A57C_3;
    game_data_radius_offset radiusOffsets[20000];
    BYTE field_1158788[204020];
    BYTE activeSettlersData1[40000];
    BYTE activeSettlersData2[800];
    game_data_player players[20];
    DWORD field_1195D7C;
    BYTE gap_1195D80[76];
    char isDemo;
    BYTE demoRelatedRandomValues[7];
    BYTE field_1195DD4[512];
    BYTE playerData1[80];
    construction_site constructionSites[600];
    DWORD diggerRelatedSize;
    BYTE gap_1222ED8[28];
    int field_1222EF4;
    BYTE gap_1222EF8[925];
    BYTE diggerRelatedSize_2[4];
    BYTE gap_1223299[27];
    DWORD field_12232B4;
    BYTE gap_12232B8[926];
    BYTE field_1223656[86528];
    BYTE gap_1223656_1[2];
    DWORD pileDataSize;
    game_data_pile pileData[32000];
    DWORD numSettlers;
    game_data_settler settlerData[32000];
    DWORD numBuildings;
    game_data_building buildingData[4000];
    int field_14D28A4;
    char field_14D28A8[800];
    game_data_effects_data effects;
    char some_array_field[40];
    int effectRelatedCounter1;
    char hasCpu;
    char victoryCondition[116];
    char field_1539525_1;
    char field_1539525_2;
    char field_1539527;
    char gap_1539528[4];
    int field_153952C;
    char gap_153952C[3064];
} game_data;
static_assert(sizeof(game_data) == 0x153A128);

typedef struct game_data_settler_carrier {
    BYTE gap[0x15];
    unsigned char itemType;
} game_data_settler_carrier;

#pragma pack(push, 1)
typedef struct CStaticText
{
    void* __vftable;
    BYTE gap_4[27];
    char field_1F;
    char field_20;
    BYTE gap_21[7];
    str_struct textValue;
    char field_38;
    BYTE gap_39[3];
    int field_3C;
    signed int field_40;
} CStaticText;
#pragma pack(pop)

typedef struct CMessageBox
{
    void* __vftable;
    DWORD field_4;
    BYTE gap_8[48];
    void* __vftable_38;
    BYTE gap_3C[120];
    int field_B4;
    BYTE gap_B8[8];
    DWORD* field_C0;
    CStaticText* staticText;
    DWORD* okButton;
    DWORD* cancelButton;
    void* editControl;
    BYTE gap_D4[12];
    DWORD field_E0;
} CMessageBox;

typedef struct menu_shared_data
{
    signed int syncEvent_netTo;
    signed int syncEvent_netFrom;
    signed int numConnectionShortcuts;
    signed int sessionSelectionState;
    signed int field_10;
    char isDirectPlayActionRunning;
    BYTE gap_15[3];
    void* connectionShortcuts;
    BYTE gap_1C[24];
    void* __vftable_34;
    BYTE gap_38[24];
    void* __vftable_50;
    BYTE gap_54[24];
    signed int field_6C;
    char field_70;
    BYTE gap_71[3];
    signed int field_74;
    signed int field_78;
    int field_7C;
    BYTE gap_80[60];
    str_struct ipAddress;
    int selectedSession;
    BYTE gap_D0[92];
    str_struct field_12C;
    str_struct playerNickname;
    str_struct field_14C;
    char field_15C;
    char field_15D;
    BYTE gap_15E[2];
    void* lobbyClient;
    void* lobbyManager;
    DWORD field_168;
    BYTE gap_16C[280];
    void* __vftable_284;
    BYTE gap_288[16];
    signed int field_298;
    signed int field_29C;
    void* __vftable_2A0;
    BYTE gap_2A4[16];
    signed int field_2B4;
    signed int field_2B8;
    void* __vftable_2BC;
} menu_shared_data;

typedef struct CSurface
{
    void* vtable;
} CSurface;

typedef struct dd_drawer
{
    void* __vftable;
    DWORD drawingInterface;
    bool isNotInitialized;
    BYTE gap_9[3];
    HWND windowRef;
    void* __vftable_10;
    signed int field_14;
    BYTE gap_18[16];
    void* __vftable_28;
    signed int field_2C;
    BYTE gap_30[16];
    void* __vftable_40;
    signed int field_44;
    BYTE gap_48[20];
    void* __vftable_5C;
    BYTE gap_60[24];
    signed int field_78;
    char field_7C;
    BYTE gap_7D[3];
    DWORD width;
    DWORD height;
    int bitRate;
    CSurface* primarySurface;
    signed int backgroundSurface;
    signed int mouseSurface;
    void* graphicsData;
    void* soundData;
    signed int field_A0;
} dd_drawer;

typedef struct __declspec(align(4)) lp_menu_draw_thread
{
    void* __vftable;
    void* __vftable_4;
    BYTE gap_8[33];
    char field_29;
    BYTE gap_2A[26];
    dd_drawer* drawer;
    BYTE gap_2A_1[12];
    DWORD field_54;
    DWORD field_58;
    BYTE gap_5C[140];
    void* consoleQueue;
    void* directDraw;
    menu_shared_data* menuSharedData;
    void* graphicsData;
    void* soundData;
    void* timer;
    signed int activeDialog;
    signed int activeDialogId;
    BYTE gap_108[8];
    DWORD isDirectplayConnected;
    void* __vftable_114;
    void* __vftable_118;
    signed int field_11C;
    signed int field_120;
    signed int field_124;
    signed int field_128;
    signed int field_12C;
    CMessageBox* activeMessageBox;
    char field_134;
    BYTE gap_135[7];
    signed int field_13C;
    char isTerminated;
    BYTE gap_141[3];
    int field_144;
    char field_148;
    BYTE gap_149[7];
    void* field_150;
} lp_menu_draw_thread;

typedef struct WCValOrderedVector_t_String
{
    void* __vftable;
    signed int field_4;
    DWORD* field_8;
    int capacity;
    signed int field_10;
    signed int field_14;
    int field_18;
} WCValOrderedVector_t_String;

typedef struct CListBox
{
    void* __vftable;
    DWORD field_4;
    BYTE gap_8[4];
    unsigned int field_C;
    BYTE gap_10[40];
    void* __vftable_38;
    void* field_3C;
    BYTE gap_40[4];
    DWORD field_44;
    BYTE gap_48[16];
    int field_58;
    BYTE gap_5C[88];
    signed int field_B4;
    signed int field_B8;
    signed int field_BC;
    DWORD scrollbar;
    DWORD field_C4;
    BYTE gap_C8[172];
    int field_174;
    BYTE gap_178[608];
    DWORD field_3D8;
    BYTE gap_3DC[4];
    DWORD field_3E0;
    BYTE gap_3E4[12];
    char field_3F0;
    char field_3F1;
    BYTE gap_3F2[2];
    DWORD field_3F4;
    int field_3F8;
    int field_3FC;
    int field_400;
    int field_404;
    int field_408;
    int field_40C;
    int field_410;
    DWORD field_414;
    signed int selectedValue;
    signed int field_41C;
    WCValOrderedVector_t_String* strVector;
    BYTE gap_424[16];
    DWORD field_434;
    signed int field_438;
    BYTE gap_43C[8];
    void* field_444;
} CListBox;

typedef struct CEditControl
{
    void* __vftable;
    DWORD field_4;
    BYTE gap_8[48];
    signed int field_38;
    signed int field_3C;
    signed int field_40;
    char field_44;
    char field_45;
    char field_46;
    char field_47;
    char field_48;
    BYTE gap_49[3];
    str_struct_wchar inputValue;
    signed int field_5C;
    int field_60;
    BYTE gap_64[4];
    signed int field_68;
} CEditControl;

typedef struct running_session
{
    BYTE gap_1[64];
    uint64_t patchLevel;
    DWORD settlersVersion;
    BYTE gap_4[4];
    str_struct sessionName;
} running_session;
static_assert(sizeof(running_session) == 0x60, "running session has size of 0x60");

typedef struct WCBareVectorBase_t_DirectX_CRunningSession_t_
{
    void* __vftable;
    signed int field_4;
    running_session* sessions;
    signed int capacity;
    signed int numSessions;
} WCBareVectorBase_t_DirectX_CRunningSession_t_;

typedef struct connection_dialog
{
    DWORD field_0;
    DWORD field_4;
    BYTE gap_8[48];
    DWORD field_38;
    void* field_3C;
    BYTE gap_40[128];
    menu_shared_data* menuSharedData;
    lp_menu_draw_thread* menuDrawThread;
    void* graphicsData;
    void* backgroundImage;
    BYTE gap_D0[32];
    DWORD localNetworkGameText;
    BYTE gap_F4[64];
    DWORD searchGamesBtn;
    DWORD newGameButton;
    DWORD joinGameButton;
    DWORD* backButton;
    CListBox* directplayProviderList;
    void* gamesList;
    int listWithScrollbar3;
    CEditControl* nicknameInput;
    CEditControl* ipInput;
    void* screenElement1;
    void* screenElement2;
    void* screenElement3;
    void* screenElement4;
    CStaticText* ipAddressLabel;
    CStaticText* statusText;
    void* __vftable_170;
    DWORD* field_174;
    DWORD field_178;
    BYTE gap_17C[12];
    void* __vftable_188;
    DWORD* field_18C;
    DWORD field_190;
    BYTE gap_194[12];
    void* __vftable_1A0;
    DWORD* field_1A4;
    DWORD field_1A8;
    BYTE gap_1AC[12];
    void* __vftable_1B8;
    DWORD* field_1BC;
    DWORD field_1C0;
    BYTE gap_1C4[12];
    void* __vftable_1D0;
    DWORD* field_1D4;
    DWORD field_1D8;
    BYTE gap_1DC[12];
    void* __vftable_1E8;
    DWORD* field_1EC;
    DWORD field_1F0;
    BYTE gap_1F4[12];
    WCBareVectorBase_t_DirectX_CRunningSession_t_ runningSessions;
    signed int field_214;
    signed int field_218;
    BYTE gap_21C[8];
    void* field_224;
} connection_dialog;
static_assert(offsetof(connection_dialog, gamesList) == 0x148, "gamesList is at offset 0x148");

typedef struct logic_thread_chat_message
{
    str_struct message;
    BYTE gap_1[12];
} logic_thread_chat_message;

typedef struct logic_thread
{
    void* vtable;
    BYTE gap_4[60];
    DWORD generic_thread_field;
    DWORD field_4;
    DWORD field_5;
    DWORD field_4C;
    DWORD field_50;
    DWORD field_54;
    DWORD field_58;
    DWORD field_5C;
    DWORD field_60;
    DWORD field_64;
    DWORD field_68;
    DWORD field_6C;
    int currentGameTick;
    DWORD field_74;
    DWORD field_78;
    DWORD field_7C;
    DWORD field_80;
    DWORD field_84;
    DWORD field_88;
    DWORD field_8B;
    DWORD field_90;
    DWORD field_94;
    DWORD field_98;
    DWORD field_9C;
    int field_A0;
    int field_A4;
    int field_A8;
    int field_AC;
    int field_B0;
    int field_B4;
    int field_B8;
    DWORD field_BC;
    int field_C0;
    int field_C4;
    int field_C8;
    int field_CC;
    int field_D0;
    DWORD field_D4;
    unsigned int field_D8;
    unsigned __int8 field_DC;
    BYTE gap_DD[3];
    int field_E0;
    BYTE gap_E4[24];
    int field_FC;
    int field_100;
    BYTE gap_104[20];
    int field_118;
    BYTE gap_11C[8];
    DWORD field_124;
    BYTE gap_128[80];
    signed int field_178;
    BYTE gap_17C[156];
    DWORD field_218;
    DWORD field_21C;
    DWORD field_221;
    int lastSavedAtGameTick;
    DWORD field_228;
    DWORD field_22C;
    DWORD field_230;
    char field_23E;
    BYTE gap_234[23];
    int field_24C;
    BYTE gap_250[4];
    int field_254;
    BYTE gap_258[248];
    DWORD computerPlayers;
    BYTE gap_354[76];
    game_data* gameData;
    BYTE gap_3A4[12];
    int field_3B0;
    unsigned int field_3B4;
    DWORD field_3B8;
    void* lpTimer;
    void* lpConsoleQueue;
    void* netToQueue;
    void* netFromQueue;
    volatile LONG* exchangeValueTargetPtr;
    void* currentDrawData;
    HANDLE* syncEvent1;
    HANDLE* syncEvent2;
    DWORD cObject;
    DWORD win32Waitable;
    BYTE gap_3DC[20];
    str_struct_wchar pendingChatMessage;
    char isChatOpen;
    BYTE gap_chat[3];
    unsigned int pendingChatMessagePrefixLength;
    bool hasChatMessageChanged;
    BYTE gap_40C[3];
    logic_thread_chat_message chatMessages[20];
    DWORD maxChatMessages;
    DWORD chatMessagePointer;
    DWORD previousTime;
    DWORD currentTime;
    DWORD field_654;
    DWORD field_658;
    DWORD field_65B;
    DWORD field_65F;
    DWORD field_664;
    BYTE gap_668[4];
    BYTE* lpLobby;
    DWORD lpLobbyManager;
    char field_674;
    char isDesynced;
} logic_thread;

typedef struct net_event_trigger_save
{
    WORD eventType;
    WORD eventSize;
    DWORD actingPlayer;
} net_event_trigger_save;

typedef struct __declspec(align(2)) net_setting_change_event
{
    char eventCls;
    char settingType;
    unsigned __int8 eventSize;
    unsigned __int8 field3;
    unsigned __int8 settingId;
    unsigned __int8 settingValue;
    unsigned __int8 actingPlayer;
    unsigned __int8 field4;
    unsigned __int16 sectorIndex;
} net_setting_change_event;

typedef struct settings_player
{
    DWORD field_0;
    signed int field_4;
    signed int field_8;
    char lobbyRelated_1;
    char field_C_1;
    char field_C_2;
    char field_C_3;
    signed int uuid;
    signed int team;
    str_struct playerName;
    str_struct field_28;
    BYTE field_38[20]; // a vector of shorts
    DWORD field_3B;
    DWORD field_3F;
    char field_54;
    BYTE gap_55[3];
    signed int field_58;
    unsigned int slotStatus;
    char field_60;
    BYTE gap_61[3];
    signed int race;
    char racePredefined;
    BYTE gap_69[3];
    signed int field_6B;
    signed int field_70;
    signed int field_74;
    signed int field_78;
    DWORD gap_7C[10];
    signed int field_A4;
    signed int field_A8;
    signed int startX;
    signed int startY;
    signed int field_B4;
    signed int field_B8;
    str_struct field_BC;
} settings_player;

typedef struct __declspec(align(4)) settings_player_vector
{
    void* __vftable;
    signed int field_4;
    settings_player* players;
    int size;
    signed int numPlayers;
} settings_player_vector;

typedef struct global_game_settings
{
    BYTE gap_1[24];
    str_struct field_1_24;
    bool isSaveGame;
    bool isMultiplayerGame;
    bool isLeagueGame;
    bool isRankedClanGame;
    str_struct mapName;
    BYTE vector1[20];
    DWORD numTeams;
    DWORD field_7_2;
    settings_player_vector players;
    DWORD numPlayers;
    DWORD field_7_5;
    DWORD goodsSetting;
    signed int ownPlayerIndex;
    DWORD isHost;
    DWORD field_6;
    void* mapFile;
    int randomValue;
    BYTE gap_4[4];
    BYTE field_6_1[20];
    DWORD field_6_2;
    BYTE gap_4_2[4];
    DWORD field_15;
    GUID* guid;
    BYTE gap9[12];
    DWORD field_10;
    DWORD field_11;
    DWORD field_12;
    bool hasAmazons;
    bool isEconomyMode;
    WORD field_13_3;
    DWORD economyModeMinutes;
} global_game_settings;
static_assert(offsetof(global_game_settings, isEconomyMode) == 0xCD, "isEconomyMode is at 0xCD");

typedef struct __declspec(align(4)) current_draw_data
{
    DWORD field_0;
    DWORD field_4;
    DWORD field_8;
    DWORD field_1B;
    DWORD field_1F;
    BYTE gap_24[36];
    DWORD field_36;
    DWORD field_40;
    DWORD isLeftMenuChanged;
    DWORD field_48;
    DWORD field_52;
    BYTE gap_4[72];
    DWORD playerRace;
    DWORD playerIndex;
    BYTE gap_4_new[4];
    DWORD displayMode;
    BYTE gap_4_1[584];
    signed int field_2EC;
    BYTE gap_2F0[50356944];
    str_struct field_30065C0[16];
    str_struct pendingChatMessage;
    DWORD chatWithNonTeamM8s;
    bool hasChatMessageChanged;
    BYTE gap_30005c[3];
    BYTE chatMessages[564];
    DWORD field_300690C;
    BYTE gap_3006910[3600];
} current_draw_data;

typedef struct game_drawer
{
    DWORD field_0;
    BYTE gap_4[8];
    HWND windowHandle;
    DWORD field_10;
    BYTE gap_14[4];
    DWORD* field_18;
    BYTE gap_1C[92];
    int field_78;
    char field_7C;
    BYTE gap_7D[390615];
    int field_5F654;
    int field_5F658;
    int field_5F65C;
    int field_5F660;
    int field_5F664;
    signed int field_5F668;
    int displayWidth;
    int displayHeight;
    BYTE gap_5F674[32];
    signed int field_5F694;
    signed int field_5F698;
    signed int field_5F69C;
    signed int field_5F6A0;
    signed int field_5F6A4;
    signed int field_5F6A8;
    signed int field_5F6AC;
    signed int field_5F6B0;
    BYTE gap_5F6B4[4];
    void* graphicsData;
    void* field_5F6BC;
    void* primaryFlipSurface;
    char* field_5F6C4;
    void* backgroundSurface;
    void* landscapeSurface;
    void* drawThread;
    BYTE gap_5F6D4;
    bool field_5F6D5;
    BYTE gap_5F6D6[2];
    LPARAM mouseSurface;
    DWORD* field_5F6DC;
    CTextSurface* textSurface;
    DWORD field_5F6E4;
    int field_5F6E8;
    int field_5F6EC;
    int field_5F6F0;
    int field_5F6F4;
    void* field_5F6F8;
    void* field_5F6FC;
} game_drawer;

typedef struct __declspec(align(4)) net_event_chat_message
{
    char eventCls;
    char field0;
    char eventSize;
    char field2;
    char playerSettingsIndex;
    char messageLength;
    BYTE gap1[2];
    unsigned int recipients;
    wchar_t message[256];
} net_event_chat_message;

typedef struct window_container {
    BYTE gap_1[4];
    HWND hWnd;
} window_container;

typedef struct s3_application
{
    BYTE gap_1[0x3C];
    window_container* mainWindow;
    BYTE gap_2[0x50];
    logic_thread* logicThread;
} s3_application;
static_assert(offsetof(s3_application, logicThread) == 0x90);
static_assert(offsetof(s3_application, mainWindow) == 0x3C);

typedef struct map_select_dialog
{
    BYTE gap_1[0xA20];
    void* mapSizeDropdownStart;
    BYTE gap_2[3484];
    void* mirrorDropdownStart;
    BYTE gap_3[3668];
    unsigned int mapSubCategory;
    str_struct mapFolderName;
    str_struct mapFileName;
    BYTE gap_4[36];
    unsigned int mapType;
} map_select_dialog;

typedef struct game_setup_dialog
{
    BYTE gap_1[12728];
    int gameType1;
    int gameType2;
    BYTE gap_2[24];
    int goodsSetting;
} game_setup_dialog;

typedef struct global_player_stats
{
    DWORD rank; // 0 if undefined for this player, i.e. no player in this slot. Rank is only populated shortly before the stats screen is viewed.
    str_struct playerName;
    DWORD team;
    BYTE gap_2[4];
    DWORD pointScore;
    DWORD settlerScore;
    DWORD buildingScore;
    DWORD foodScore;
    DWORD mineScore;
    DWORD goldScore;
    DWORD manaScore;
    DWORD soldierScore;
    DWORD battleScore;
    BYTE gap_3[4];
} global_player_stats;
static_assert(sizeof(global_player_stats) == 68, "player stats must be 68 in size");

typedef struct file_chunk_header
{
    WORD type;
    WORD metaInfo;
    DWORD size;
} file_chunk_header;

typedef struct {
    BYTE gap1[0x40];
    uint64_t patchLevel;
    BYTE gap2[8];
} game_session_desc;
static_assert(sizeof(game_session_desc) == 0x50, "size of game_session_desc must be 0x50.");

typedef struct net_session {
    BYTE gap[0xF8];
    void* localPlayer;
} net_session;
static_assert(offsetof(net_session, localPlayer) == 0xF8);

typedef void(__thiscall* SELECT_CONN_PROVIDER_PROC)(connection_dialog* This, int providerIndex);
static SELECT_CONN_PROVIDER_PROC s3_select_conn_provider =
    reinterpret_cast<SELECT_CONN_PROVIDER_PROC>(Constants::S3_EXE + 0x7E270);

typedef void(__thiscall* SEARCH_GAMES_PROC)(connection_dialog* This, int notUsed);
static SEARCH_GAMES_PROC s3_search_games = reinterpret_cast<SEARCH_GAMES_PROC>(Constants::S3_EXE + 0x7DF70);

typedef void(__thiscall* JOIN_SESSION_PROC)(connection_dialog* This, int notUsed);
static JOIN_SESSION_PROC s3_join_session = reinterpret_cast<JOIN_SESSION_PROC>(Constants::S3_EXE + 0x7F480);

typedef void(__thiscall* SELECT_SESSION_PROC)(connection_dialog* This, int notUsed, char sessionIndex);
static SELECT_SESSION_PROC s3_select_session = reinterpret_cast<SELECT_SESSION_PROC>(Constants::S3_EXE + 0x7ECC0);

typedef void (__thiscall* INIT_STRUCTURED_EXCEPTION_MESSAGE_PROC)(void* exception, str_struct *message);
static INIT_STRUCTURED_EXCEPTION_MESSAGE_PROC s3_init_structured_exception_message = reinterpret_cast<INIT_STRUCTURED_EXCEPTION_MESSAGE_PROC>(Constants::S3_EXE + 0x214870);

typedef void* (__thiscall *CREATE_MAIN_MENU_DIALOG)(void *mainMenuDialog, void *graphicsData, lp_menu_draw_thread *menuDrawThread, int a4);
static CREATE_MAIN_MENU_DIALOG s3_create_main_menu_dialog = reinterpret_cast<CREATE_MAIN_MENU_DIALOG>(Constants::S3_EXE + 0x74010);

typedef void(__thiscall* HOST_NEW_GAME_PROC)(connection_dialog* This, int a2);
static HOST_NEW_GAME_PROC s3_host_new_game = reinterpret_cast<HOST_NEW_GAME_PROC>(Constants::S3_EXE + 0x7EFD0);

// Initializes a str_struct which uses a char for storing text (usually this is used everywhere).
void s3_str_init(str_struct* s, const char* textValue);

// Initializes a str_struct which uses a wchar for storing the text (used in UI related classes).
void s3_str_init(str_struct_wchar* target, str_struct* source);

void s3_str_init(str_struct_wchar* target, const char* textValue);

void s3_str_copy(str_struct_wchar* target, str_struct_wchar* source);

void s3_str_clear(str_struct_wchar* target, bool deAllocate = false);

unsigned int s3_get_display_height();
unsigned int s3_get_display_size_unit();

settings_player* s3_get_settings_player(int playerSettingsIndex);
settings_player* s3_get_or_create_settings_player(int playerSettingsIndex);
int s3_get_current_game_tick();
int s3_read_int(int offset);
void s3_queue_event(void* queue, void* eventPtr, int eventSize_inNumDwords);
global_game_settings* s3_get_game_settings();
short s3_get_player_color(int playerIndex);
std::string s3_get_player_name(int playerIndex);
void s3_add_chat_message(logic_thread* logicThread, str_struct_wchar* message, short playerColor);
void s3_draw_text(CTextSurface* textSurface, RECT* drawnRect, char fontId, RECT* targetRect, char drawMode,
                  str_struct* text);
void s3_set_map_visibility(bool isVisible);
void s3_count_towers_by_player(logic_thread* logicThread, int numTowersByPlayer[20]);
void s3_update_alive_teams(logic_thread* logicThread, std::set<int>* aliveTeams, bool includeCpus = true);

// This call blocks until the message box is closed.
void s3_show_message_box(lp_menu_draw_thread* menuDrawThread, str_struct* message, int buttonStyle);
void s3_update_menu_screen(lp_menu_draw_thread* menuDrawThread, int buttonId);
IDirectDrawSurface* s3_get_dd_surface(CSurface* surface);
void s3_set_dropdown_value(void* dropdown, int value);
void s3_set_static_text_value(void* staticText, str_struct* newValue);
int s3_get_selected_list_item_value(void* list);
global_player_stats* s3_get_player_stats();
void s3_file_chunk_write(void* file, unsigned short chunkType, file_chunk_header* chunkHeader, unsigned int dataSize,
                         const void* data);
void s3_file_chunk_write(void* file, unsigned short chunkType, file_chunk_header* chunk);
file_chunk_header* s3_file_chunk_read(void* file, unsigned short chunkType);
void s3_file_chunk_free(void* file, unsigned short chunkType);
bool s3_is_random_game();
std::string s3_get_building_name(int buildingType);
s3_application* s3_get_app();
unsigned short s3_next_random_value(game_data* gameData);
unsigned short s3_next_random_value(game_data* gameData, unsigned short maxValue);
unsigned int s3_calculate_distance(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2);
void s3_add_item_pile_near(game_data* gameData, int itemType, unsigned int itemCount, int x, int y);

typedef void (__thiscall *DIRECTPLAY_GET_SESSION_DESC_PROC)(void *netSession, game_session_desc* desc, int timeoutMs);
static DIRECTPLAY_GET_SESSION_DESC_PROC s3_dp_get_session_desc = reinterpret_cast<DIRECTPLAY_GET_SESSION_DESC_PROC>(Constants::S3_EXE + 0x1C0900);

typedef void (__thiscall *DIRECTPLAY_SET_SESSION_DESC_PROC)(void *netSession, game_session_desc* desc, int timeoutMs);
static DIRECTPLAY_SET_SESSION_DESC_PROC s3_dp_set_session_desc = reinterpret_cast<DIRECTPLAY_SET_SESSION_DESC_PROC>(Constants::S3_EXE + 0x1C0930);

#endif // SIEDLER3_FIXES_S3_H
