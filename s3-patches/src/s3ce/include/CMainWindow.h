#ifndef SIEDLER3_FIXES_CMAINWINDOW_H
#define SIEDLER3_FIXES_CMAINWINDOW_H

#include <vector>
#include <string>
#include "stdafx.h"

class GraphicMode {
public:
    unsigned int m_id;
    LPCTSTR m_name;

    GraphicMode(unsigned int id, LPCTSTR name) {
        this->m_name = name;
        this->m_id = id;
    }
};

class DisplayResolution {
public:
    unsigned short m_width;
    unsigned short m_height;

    DisplayResolution(unsigned short width, unsigned short height): m_width(width), m_height(height) {
    }

    std::string GetDescription() {
        return std::to_string(m_width) + "x" + std::to_string(m_height);
    }
};

class CMainWindow: public CFrameWnd
{
public:
    CComboBox* graphicModeDropdown = new CComboBox;
    CComboBox* displayResolutionDropdown = new CComboBox;
    CButton* textureFilterCheckbox = new CButton;
    CButton* reduceFpsCheckbox = new CButton;
    CButton* setupGameSettingsCheckbox = new CButton;
    CButton* autoSaveCheckbox = new CButton;
    CButton* improvedChat = new CButton;
    CButton* launchButton = new CButton;

    CStatic* graphicSettingsTitle = new CStatic;
    CStatic* gameSettingsTitle = new CStatic;
    CStatic *joinGameTitle = new CStatic;
    CStatic *nicknameLabel = new CStatic;
    CEdit *nicknameInput = new CEdit;
    CStatic *ipLabel = new CStatic;
    CEdit *hostIpInput = new CEdit;

    HBRUSH m_whiteBrush = CreateSolidBrush(RGB(255, 255, 255));
    std::vector<GraphicMode*> graphicModes;
    std::vector<DisplayResolution*> displayResolutions;

    CMainWindow();
    BOOL DestroyWindow();

protected:
    void OnGraphicModeSelect();
    void OnDisplayResolutionSelect();
    void OnResolutionChangeClicked();
    void OnReduceFpsClicked();
    void OnAutoSaveClicked();
    void OnSetupGameSettingsClicked();
    void OnLaunchClicked();
    void OnImprovedChatClicked();
    HBRUSH OnCtlColor(CDC*, CWnd*, UINT);
    void OnShowWindow(BOOL bShow, UINT status);
    DECLARE_MESSAGE_MAP();

private:
    void UpdateControlStates();
    int FindGraphicModePosition(unsigned int graphicMode);
    void NicknameInputLooseFocus();
};


#endif // SIEDLER3_FIXES_CMAINWINDOW_H
