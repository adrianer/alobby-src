#ifndef SIEDLER3_FIXES_UTILS_H
#define SIEDLER3_FIXES_UTILS_H

#include "stdafx.h"
#include <string>

std::string WidestringToString(std::wstring wstr);
bool is_elevated();

#endif // SIEDLER3_FIXES_UTILS_H
