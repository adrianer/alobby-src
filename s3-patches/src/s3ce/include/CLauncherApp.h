#ifndef SIEDLER3_FIXES_CLAUNCHERAPP_H
#define SIEDLER3_FIXES_CLAUNCHERAPP_H

#include <string>
#include "stdafx.h"

class CLauncherApp: public CWinApp
{
public:
    BOOL InitInstance();

private:
    std::string ParseStringCliFlag(std::string cmdLine, std::string flagName, std::string defaultValue = "");
    bool ParseBoolCliFlag(std::string cmdLine, std::string flagName, bool defaultValue);
    BOOL LaunchGame(std::string cmdLine, std::string hostIpAddr, bool shouldWaitForProcessExit);
    long ParseLongCliFlag(std::string cmdLine, std::string flagName, long defaultValue);
    bool HasCliFlag(std::string cmdLine, std::string flagName);
};


#endif // SIEDLER3_FIXES_CLAUNCHERAPP_H
