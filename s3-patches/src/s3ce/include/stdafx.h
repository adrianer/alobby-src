#ifndef SIEDLER3_FIXES_STDAFX_H
#define SIEDLER3_FIXES_STDAFX_H

// see https://docs.microsoft.com/de-de/cpp/porting/modifying-winver-and-win32-winnt?view=msvc-160
#define _WIN32_WINNT 0x0601                    // Windows 7
#define WINVER 0x0601                          // Windows 7
#define _CRT_STDIO_ARBITRARY_WIDE_SPECIFIERS 1

#include <afxwin.h>

#define NOMINMAX 1
#include <algorithm>
namespace Gdiplus
{
using std::min;
using std::max;
};
#include <afxbutton.h>

#endif
