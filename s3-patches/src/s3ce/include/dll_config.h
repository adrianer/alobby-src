#ifndef SIEDLER3_FIXES_DLL_CONFIG_H
#define SIEDLER3_FIXES_DLL_CONFIG_H

#include "stdafx.h"

#define MAX_NICKNAME_LEN 19
#define MAX_IP_ADDR_LEN 15
#define MAX_MAP_NAME_LEN 99
#define MAX_PIPE_NAME_LEN 19

#define MAP_CATEGORY_RANDOM 0
#define MAP_CATEGORY_SINGLE_PLAYER 1
#define MAP_CATEGORY_MULTI_PLAYER 2
#define MAP_CATEGORY_USER 3
#define MAP_CATEGORY_SAVE 4

#define MAP_SIZE_384 0
#define MAP_SIZE_448 1
#define MAP_SIZE_512 2
#define MAP_SIZE_576 3
#define MAP_SIZE_640 4
#define MAP_SIZE_704 5
#define MAP_SIZE_768 6

#define MIRROR_TYPE_NONE 0
#define MIRROR_TYPE_SHORT_AXIS 1
#define MIRROR_TYPE_LONG_AXIS 2
#define MIRROR_TYPE_BOTH_AXES 3

#define GOODS_SETTING_MAP_DEFAULT 0
#define GOODS_SETTING_LOW 1
#define GOODS_SETTING_MEDIUM 2
#define GOODS_SETTING_HIGH 3

typedef struct dll_config_join_game {
    bool enabled;
    char nickname[MAX_NICKNAME_LEN + 1];
    char ipAddr[MAX_IP_ADDR_LEN + 1];
} dll_config_join_game;

typedef struct dll_config_host_game {
    bool enabled;
    unsigned char mapCategory;
    unsigned char mapSize;
    unsigned char mirrorType;
    bool useRandomPositions;
    unsigned char goodsSetting;
    char nickname[MAX_NICKNAME_LEN + 1];
    char mapName[MAX_MAP_NAME_LEN + 1];
} dll_config_host_game;

typedef struct dll_config {
    short graphicMode;
    unsigned short displayWidth;
    unsigned short displayHeight;
    bool applyTextureFilter;
    bool reduceMaxFps;
    char pipeName[MAX_PIPE_NAME_LEN + 1];
    dll_config_join_game joinGameData;
    dll_config_host_game hostGameData;
    bool autoSave;
    bool balancingPatchesDisabled;
    bool classicOnly;
    bool improvedChat;
    char screenDirectory[MAX_PATH];
    bool disableGui;
    bool enableGui;
    bool errorTracking;
    bool forceDisplayResolution;
    bool forceWindowMode;
} dll_config;

#endif // SIEDLER3_FIXES_DLL_CONFIG_H
