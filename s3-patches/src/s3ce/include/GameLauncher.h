#ifndef SIEDLER3_FIXES_GAMELAUNCHER_H
#define SIEDLER3_FIXES_GAMELAUNCHER_H

#include <vector>
#include "dll_config.h"

class GameLauncher
{
public:
    GameLauncher(bool waitForProcessExit = false);
    unsigned long launch(dll_config *dllConfig);
private:
    bool applyFixes(std::vector<int>& pids, dll_config *dllConfig);

    bool m_waitForProcessExit;
};


#endif // SIEDLER3_FIXES_GAMELAUNCHER_H
