
#include "AutoUpdater.h"
#include "utils.h"
#include "constants.h"
#include "stdafx.h"
#include <winhttp.h>
#include <Wincrypt.h>
#include <hacklib/MessageBox.h>

#define BUFSIZE 1024
#define ALGLEN 16

bool AutoUpdater::checkForUpdates()
{
    try
    {
        DeleteFileA(LAUNCHER_OLD_FILE);
        DeleteFileA(LAUNCHER_TMP_FILE);

        bool showUpdateCompletedMessage = false;

        if (m_parentLauncherProcessId != 0) {
            MoveFile(LAUNCHER_REGULAR_FILE, LAUNCHER_OLD_FILE);
            if (!MoveFile(LAUNCHER_TMP_FILE, LAUNCHER_REGULAR_FILE)) {
                // Something went wrong, let the parent process take over again.
                MoveFile(LAUNCHER_OLD_FILE, LAUNCHER_REGULAR_FILE);
                exit(0);
            }

            HANDLE parentProcess = OpenProcess(PROCESS_TERMINATE, TRUE, m_parentLauncherProcessId);
            TerminateProcess(parentProcess, 1);
            WaitForSingleObject(parentProcess, 1000);
            CloseHandle(parentProcess);
            DeleteFileA(LAUNCHER_OLD_FILE);

            showUpdateCompletedMessage = true;
        } else {
            TCHAR szExeFileName[MAX_PATH];
            GetModuleFileName(NULL, szExeFileName, MAX_PATH);

            auto launcherLocalTag = computeLocalETag(szExeFileName);
            auto launcherRemoteTag = fetchRemoteTag(LAUNCHER_REMOTE_PATH);
            if (launcherLocalTag.compare(launcherRemoteTag) != 0) {
                if (!shouldUpdate()) {
                    return true;
                }

                DeleteFileA(LAUNCHER_TMP_FILE);
                downloadFile(LAUNCHER_REMOTE_PATH, LAUNCHER_TMP_FILE, launcherRemoteTag);

                STARTUPINFOA startupInfo;
                ZeroMemory(&startupInfo, sizeof(startupInfo));
                PROCESS_INFORMATION processInfo;
                ZeroMemory(&processInfo, sizeof(processInfo));
                char cmdLine[100];
                sprintf(cmdLine, "%s -update %lu", LAUNCHER_TMP_FILE, GetCurrentProcessId());
                if (CreateProcess(NULL, cmdLine, NULL, NULL, TRUE, 0, NULL, NULL, &startupInfo, &processInfo)) {
                    // Waiting for the new launcher to terminate our process.
                    WaitForSingleObject(processInfo.hProcess, INFINITE);

                    throw UpdaterException("Launcher Update failed", "The launcher could not be updated. Please close any running launcher processes and try again.");
                }
            }
        }

        DeleteFileA(DLL_OLD_FILE);
        DeleteFileA(DLL_TMP_FILE);

        auto dllRemoteTag = fetchRemoteTag(DLL_REMOTE_PATH);
        auto dllLocalTag = computeLocalETag(DLL_REGULAR_FILE);
        if (dllRemoteTag.compare(dllLocalTag) != 0) {
            if (!shouldUpdate()) {
                return true;
            }

            DeleteFileA(DLL_TMP_FILE);
            downloadFile(DLL_REMOTE_PATH, DLL_TMP_FILE, dllRemoteTag);
            MoveFileA(DLL_REGULAR_FILE, DLL_OLD_FILE);
            if (!MoveFileA(DLL_TMP_FILE, DLL_REGULAR_FILE)) {
                MoveFileA(DLL_OLD_FILE, DLL_REGULAR_FILE);

                throw UpdaterException("DLL Update failed", "Failed to update DLL file. Please close any running game instances and try again.");
            }

            DeleteFileA(DLL_OLD_FILE);
            showUpdateCompletedMessage = true;
        }

        if (showUpdateCompletedMessage) {
            hl::MsgBox("Update applied", "The update was successful.");
        }

        return true;
    } catch (UpdaterException& e) {
        hl::MsgBox("Auto-Updater: " + e.title, e.message);
    } catch (std::exception& e) {
        hl::MsgBox("Auto-Updater: Generic Error", e.what());
    }

    return false;
}

void AutoUpdater::downloadFile(LPCWSTR remotePath, LPCSTR localPath, std::string expectedETag) {
    HINTERNET hSession = NULL,
        hConnect = NULL,
        hRequest = NULL;

    DWORD dwSize = sizeof(DWORD);
    hSession = WinHttpOpen(L"S3 Community Edition/1.0", WINHTTP_ACCESS_TYPE_DEFAULT_PROXY, WINHTTP_NO_PROXY_NAME,
                           WINHTTP_NO_PROXY_BYPASS, 0);

    if (!hSession) {
        throw UpdaterException("Failed to open http session", "The HTTP session could not be created.");
    }

    hConnect = WinHttpConnect( hSession, SERVER_NAME, INTERNET_DEFAULT_HTTPS_PORT, 0);
    if (!hConnect) {
        throw UpdaterException("Failed to connect", "A connection could not be established.");
    }

    hRequest = WinHttpOpenRequest(hConnect, L"GET", remotePath, NULL, WINHTTP_NO_REFERER, WINHTTP_DEFAULT_ACCEPT_TYPES, WINHTTP_FLAG_SECURE);
    if (!hRequest) {
        throw UpdaterException("Failed to open download request", "Download HTTP-Request could not be opened.");
    }
    if (!WinHttpSendRequest(hRequest, WINHTTP_NO_ADDITIONAL_HEADERS, 0, WINHTTP_NO_REQUEST_DATA, 0, 0, 0)) {
        throw UpdaterException("Download Request failed", "Could not retrieve latest version.");
    }
    if (!WinHttpReceiveResponse( hRequest, NULL)) {
        throw UpdaterException("Download Request incomplete", "Failed to complete the request.");
    }

    HANDLE hDownloadedFile = CreateFile(localPath, GENERIC_WRITE, NULL, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
    if (!hDownloadedFile) {
        throw UpdaterException("File Creation Failed", "Download file could not be created.");
    }

    LPSTR pszOutBuffer;
    DWORD dwDownloaded = 0;
    do
    {
        // Check for available data.
        dwSize = 0;
        if (!WinHttpQueryDataAvailable( hRequest, &dwSize))
        {
            throw UpdaterException("Download failed", "Data could not be retrieved.");
        }

        // No more available data.
        if (!dwSize)
            break;

        // Allocate space for the buffer.
        pszOutBuffer = new char[dwSize+1];
        if (!pszOutBuffer) {
            throw UpdaterException("Memory allocation failed", "Could not allocate memory for downloaded data.");
        }

        // Read the Data.
        ZeroMemory(pszOutBuffer, dwSize+1);

        if (!WinHttpReadData( hRequest, (LPVOID)pszOutBuffer, dwSize, &dwDownloaded)) {
            throw UpdaterException("Chunk Read Error", "Failed to read chunk into memory.");
        }

        DWORD bytesWritten;
        if (!WriteFile(hDownloadedFile, pszOutBuffer, dwSize, &bytesWritten, NULL)) {
            throw UpdaterException("Disk write failed", "Could not write downloaded data to disk.");
        }

        // Free the memory allocated to the buffer.
        delete [] pszOutBuffer;

        // This condition should never be reached since WinHttpQueryDataAvailable
        // reported that there are bits to read.
        if (!dwDownloaded)
            break;
    } while (dwSize > 0);

    CloseHandle(hDownloadedFile);
    WinHttpCloseHandle(hRequest);
    WinHttpCloseHandle(hConnect);
    WinHttpCloseHandle(hSession);

    auto downloadedETag = computeLocalETag(localPath);
    if (downloadedETag.compare(expectedETag) != 0) {
        DeleteFileA(localPath);

        char message[300];
        sprintf(message, "The downloaded file '%s' is incomplete (%s != %s). Please check that you have enough disk space and an active Internet connection.", localPath, downloadedETag.c_str(), expectedETag.c_str());

        throw UpdaterException("Download Corrupt", message);
    }
}

bool AutoUpdater::shouldUpdate() {
    if (m_parentLauncherProcessId != 0) {
        return true;
    }

    return MessageBoxA(NULL, "An update is available. Download now?", "S3 Community Edition Update available", MB_OKCANCEL) == IDOK;
}

std::string AutoUpdater::fetchRemoteTag(LPCWSTR remotePath) {
    HINTERNET hSession = NULL,
        hConnect = NULL,
        hRequest = NULL;

    DWORD dwSize = sizeof(DWORD);
    hSession = WinHttpOpen(L"S3 Community Edition/1.0", WINHTTP_ACCESS_TYPE_DEFAULT_PROXY, WINHTTP_NO_PROXY_NAME,
                           WINHTTP_NO_PROXY_BYPASS, 0);

    if (!hSession) {
        throw UpdaterException("Failed to open http session", "The HTTP session could not be created.");
    }

    hConnect = WinHttpConnect( hSession, SERVER_NAME, INTERNET_DEFAULT_HTTPS_PORT, 0);
    if (!hConnect) {
        throw UpdaterException("Failed to connect", "A connection could not be established.");
    }

    hRequest = WinHttpOpenRequest(hConnect, L"HEAD", remotePath, NULL, WINHTTP_NO_REFERER, WINHTTP_DEFAULT_ACCEPT_TYPES, WINHTTP_FLAG_SECURE);
    if (!hRequest) {
        throw UpdaterException("Failed to open request", "HTTP-Request could not be opened.");
    }

    if (!WinHttpSendRequest(hRequest, WINHTTP_NO_ADDITIONAL_HEADERS, 0, WINHTTP_NO_REQUEST_DATA, 0, 0, 0)) {
        throw UpdaterException("Version Request failed", "Could not retrieve latest version information.");
    }
    if (!WinHttpReceiveResponse( hRequest, NULL)) {
        throw UpdaterException("Version Request incomplete", "Failed to complete the request.");
    }

    LPVOID lpOutBuffer;
    BOOL headerRes = WinHttpQueryHeaders( hRequest, WINHTTP_QUERY_CUSTOM, L"ETag", WINHTTP_NO_OUTPUT_BUFFER, &dwSize, WINHTTP_NO_HEADER_INDEX);
    if (GetLastError() == ERROR_INSUFFICIENT_BUFFER) {
        lpOutBuffer = new WCHAR[dwSize/sizeof(WCHAR)];
        headerRes = WinHttpQueryHeaders(hRequest, WINHTTP_QUERY_CUSTOM, L"ETag", lpOutBuffer, &dwSize, WINHTTP_NO_HEADER_INDEX);
    }
    if (!headerRes) {
        throw UpdaterException("Version Information Invalid",
                               "Could not extract version information from request.");
    }

    std::string tagValue(WidestringToString((WCHAR*)lpOutBuffer));

    WinHttpCloseHandle(hRequest);
    WinHttpCloseHandle(hConnect);
    WinHttpCloseHandle(hSession);

    return tagValue.substr(1, tagValue.length()-2);
}

std::string AutoUpdater::computeLocalETag(const char* filename)
{
    HCRYPTPROV hProv = 0;
    HCRYPTHASH hHash = 0;
    BYTE rgbFile[BUFSIZE];
    DWORD cbRead = 0;
    BOOL bResult = FALSE;
    HANDLE hFile = CreateFile(filename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_FLAG_SEQUENTIAL_SCAN, NULL);

    if (GetLastError() == ERROR_FILE_NOT_FOUND) {
        return "";
    }

    if (!hFile) {
        return "";
    }

    try
    {
        DWORD dwStatus = 0;

        if (!CryptAcquireContext(&hProv, NULL, NULL, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT)) {
            throw UpdaterException("Crypt", "AcquireContext failed");
        }

        if (!CryptCreateHash(hProv, CALG_MD5, 0, 0, &hHash)) {
            throw UpdaterException("Crypt", "SHA unavailable");
        }

        while ((bResult = ReadFile(hFile, rgbFile, BUFSIZE, &cbRead, NULL))) {
            if (0 == cbRead) {
                break;
            }

            if (!CryptHashData(hHash, rgbFile, cbRead, 0))
            {
                throw UpdaterException("MD5 Calculation failed", "Failed to create checksum.");
            }
        }

        if (!bResult) {
            throw UpdaterException("MD5 Calculation aborted", "Failed to read the entire file.");
        }

        CHAR rgbDigits[] = "0123456789abcdef";
        BYTE rgbHash[ALGLEN];
        DWORD cbHash = ALGLEN;
        if (CryptGetHashParam(hHash, HP_HASHVAL, rgbHash, &cbHash, 0))
        {
            std::string hash;

            for (DWORD i = 0; i < cbHash; i++)
            {
                hash += rgbDigits[rgbHash[i] >> 4];
                hash += rgbDigits[rgbHash[i] & 0xf];
            }

            CloseHandle(hFile);
            CryptReleaseContext(hProv, 0);
            CryptDestroyHash(hHash);

            return hash;
        }

        throw UpdaterException("Hash failed", "Hash could not be retrieved.");
    } catch (std::exception& e) {
        CloseHandle(hFile);
        if (hProv) {
            CryptReleaseContext(hProv, 0);
        }
        if (hHash) {
            CryptDestroyHash(hHash);
        }

        throw e;
    }

    return "";
}
