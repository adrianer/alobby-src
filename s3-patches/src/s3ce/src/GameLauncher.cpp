#include "GameLauncher.h"
#include "stdafx.h"
#include <hacklib/MessageBox.h>
#include <hacklib/Injector.h>
#include <iostream>
#include <vector>
#include "constants.h"
#include <chrono>

DWORD GameLauncher::launch(dll_config* dllConfig) {
    auto exePaths = {
        "s3_alobby.exe",
    };
    for (auto exePath : exePaths) {
        struct stat buffer {};
        auto result = stat(exePath, &buffer);
        if (result == 0) {
            STARTUPINFOA startupInfo;
            ZeroMemory(&startupInfo, sizeof(startupInfo));
            PROCESS_INFORMATION processInfo;
            ZeroMemory(&processInfo, sizeof(processInfo));
            auto procResult =
                CreateProcess(exePath, nullptr, nullptr, nullptr, false, CREATE_SUSPENDED, nullptr, nullptr, &startupInfo, &processInfo);
            if (FAILED(procResult)) {
                hl::MsgBox("S3 Launch Failed", "Could not launch the S3 process.");
            } else {
                std::cout << "Launch succeeded. Process: " << processInfo.dwProcessId << std::endl;
                std::cout << "Waiting for initialization to complete..." << std::endl;

                auto pids = std::vector<int>{ (int)processInfo.dwProcessId };
                if (applyFixes(pids, dllConfig)) {
                    if (m_waitForProcessExit) {
                        std::cout << "Waiting until S3 exits." << std::endl;
                        WaitForSingleObject(processInfo.hProcess, INFINITE);
                    }

                    CloseHandle(processInfo.hProcess);
                    CloseHandle(processInfo.hThread);

                    return processInfo.dwProcessId;
                }

                TerminateProcess(processInfo.hProcess, 0);

                return 0;
            }
        }
    }

    hl::MsgBox("Settler3 not found", "No s3_alobby.exe found. Copy the launcher + s3.dll to the directory with s3_alobby.exe.");

    return 0;
}

bool GameLauncher::applyFixes(std::vector<int>& pids, dll_config* dllConfig) {
    struct stat buffer {};
    auto statDllResult = stat(DLL_REGULAR_FILE, &buffer);
    if (statDllResult != 0) {
        std::string msg;
        msg.append(DLL_REGULAR_FILE);
        msg.append(" file not found. Make sure it is placed in the same directory like the launcher.");
        hl::MsgBox("DLL Missing", msg);

        return false;
    }

    std::string errorMsg;
    for (auto p : pids) {
        auto dllConfigSize = sizeof(dll_config);
        bool result = hl::Inject(p, DLL_REGULAR_FILE, dllConfig, dllConfigSize, &errorMsg);

        if (result) {
            return true;
        } else {
            std::string message;
            message.append("Fixes could not be applied.\n\n");
            message.append(errorMsg);
            hl::MsgBox("S3 Community Edition", message);

            return false;
        }
    }

    hl::MsgBox("Process Launch failed", "The settler process could not be created.");

    return false;
}

GameLauncher::GameLauncher(bool waitForProcessExit) {
    m_waitForProcessExit = waitForProcessExit;
}
